﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleAiManager
{
	public enum eAiMode
	{
		PVP,
		NONE
	}
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleAiManager()
	{
		mBattleGrid = null;
		mCommanderDictionary = new Dictionary<int, IBattleAiCommanderData>();
		mAllyCommanderDictionary = new Dictionary<int, IBattleAiCommanderData>();
		mEnemyCommanderDictionary = new Dictionary<int, IBattleAiCommanderData>();

		mAiMode = eAiMode.NONE;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 AI Mode가 어떤 Mode를 사용하는지 처리
	public bool IsAiMode(BattleAiManager.eAiMode pAiMode)
	{
		return pAiMode == mAiMode;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public virtual void Init(
		BattleUser pUser, 
		BattleGrid pBattleGrid, 
		BattleAiCollector pBattleAiCollector)
	{
		mUser = pUser;
		mBattleGrid = pBattleGrid;
		mMatchInfo = pBattleGrid.aMatchInfo;
		mProgressInfo = pBattleGrid.aProgressInfo;
		mBattleAiCollector = pBattleAiCollector;
	}
	//-------------------------------------------------------------------------------------------------------
	// 재사용을 위한 리셋
	public virtual void Reset()
	{
		mCommanderDictionary.Clear();
		mAllyCommanderDictionary.Clear();
		mEnemyCommanderDictionary.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 입력
	public virtual void AddCommander(IBattleAiCommanderData pCommander)
	{
		if (IsMyCommander(pCommander))
			mCommanderDictionary.Add(pCommander.aId, pCommander);
		else if (IsAllyCommander(pCommander))
			mAllyCommanderDictionary.Add(pCommander.aId, pCommander);
		else /*if (IsEnemyCommander(pCommander))/**/
			mEnemyCommanderDictionary.Add(pCommander.aId, pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 삭제
	public virtual void RemoveCommander(IBattleAiCommanderData pCommander)
	{
		if (IsMyCommander(pCommander))
			mCommanderDictionary.Remove(pCommander.aId);
		else if (IsAllyCommander(pCommander))
			mAllyCommanderDictionary.Remove(pCommander.aId);
		else /*if (IsEnemyCommander(pCommander))/**/
			mEnemyCommanderDictionary.Remove(pCommander.aId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관이 적 발견시 AI 매니저에게 다음 행동을 요청합니다.
	public virtual void ReqFindEnemy(int pCommanderId, Coord2 pMoveCoord, int pEnemyCommanderId) { }
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관이 전투를 시작합니다.
	public virtual void ReqBattleStart(int pCommanderId) { }
	//-------------------------------------------------------------------------------------------------------
	// 타겟이 되는 지휘관의 부대가 사라졌을 때 다음 행동을 요청합니다.
	public virtual void ReqBattleEnd(int pCommanderId) { }
	//-------------------------------------------------------------------------------------------------------
	// 타겟 지휘관을 추가합니다.
	public virtual void AddTargetCommander(int pTargetCommanderId) { }
	//-------------------------------------------------------------------------------------------------------
	// 타겟 지휘관을 제거합니다.
	public virtual void RemoveTargetCommander(int pTargetCommanderId) { }
	
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 부대가 전멸했을 경우 호출
	//public virtual void ReqDestoryCommander(int pCommanderId) { }


	////-------------------------------------------------------------------------------------------------------
	//// 피격 당한 지휘관이 AI 매니저에게 다음행동을 요청합니다.
	//public virtual void ReqHitCommander(int pCommanderId){}

	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public virtual void UpdateFrame(){}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 아군 지휘관
	protected bool IsMyCommander(IBattleAiCommanderData pCommander)
	{
		if (pCommander.aTeamIdx == mUser.aTeamIdx && pCommander.aAiUserIdx == mUser.aUserIdx && pCommander.aAiUserIdx >= 0)
			return true;
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 동맹 지휘관
	protected bool IsAllyCommander(IBattleAiCommanderData pCommander)
	{
		if (pCommander.aTeamIdx == mUser.aTeamIdx && pCommander.aAiUserIdx != mUser.aUserIdx && pCommander.aAiUserIdx >= 0)
		{
			return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 적군 지휘관
	protected bool IsEnemyCommander(IBattleAiCommanderData pCommander)
	{
		if (pCommander.aTeamIdx != mUser.aTeamIdx)
		{
			return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	// 공통 사용 변수
	protected BattleUser mUser;
	protected BattleGrid mBattleGrid;
	protected BattleMatchInfo mMatchInfo;
	protected BattleProgressInfo mProgressInfo;
	protected Dictionary<int, IBattleAiCommanderData> mCommanderDictionary; // 본인 지휘관 리스트(조작 가능한 지휘관)
	protected Dictionary<int, IBattleAiCommanderData> mAllyCommanderDictionary; // 동맹 지휘관 리스트]
	protected Dictionary<int, IBattleAiCommanderData> mEnemyCommanderDictionary; // 적군 지휘관 리스트
	protected eAiMode mAiMode;
	protected BattleAiCollector mBattleAiCollector;
}
