﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;

public class BattleDataDump
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 전투 결과를 덤프합니다.
	public static void DumpBattleResult(String pDumpFilePath, BattleProgressInfo pProgressInfo, BattleGrid pBattleGrid)
	{
		Debug.Log("DumpBattleResult() pDumpFilePath=" + pDumpFilePath);

		using (FileStream lFileStream = new FileStream(pDumpFilePath, FileMode.Create))
		{
			StreamWriter lStreamWriter = new StreamWriter(lFileStream);
			StringBuilder lStringBuilder = new StringBuilder();

			lStringBuilder.AppendLine("// " + pDumpFilePath);
			lStringBuilder.AppendLine();

			DumpProgressInfo(lStringBuilder, pProgressInfo);
			lStringBuilder.AppendLine();
			DumpBattleGrid(lStringBuilder, pBattleGrid);

			lStreamWriter.WriteLine(lStringBuilder.ToString());
			lStreamWriter.Close();
			lFileStream.Close();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 결과를 덤프합니다.
	public static void DumpProgressInfo(String pDumpFilePath, BattleProgressInfo pProgressInfo)
	{
		Debug.Log("DumpProgressInfo() pDumpFilePath=" + pDumpFilePath);

		using (FileStream lFileStream = new FileStream(pDumpFilePath, FileMode.Create))
		{
			StreamWriter lStreamWriter = new StreamWriter(lFileStream);
			StringBuilder lStringBuilder = new StringBuilder();

			lStringBuilder.AppendLine("// " + pDumpFilePath);
			lStringBuilder.AppendLine();

			DumpProgressInfo(lStringBuilder, pProgressInfo);

			lStreamWriter.WriteLine(lStringBuilder.ToString());
			lStreamWriter.Close();
			lFileStream.Close();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 결과를 덤프합니다.
	public static void DumpBattleGrid(String pDumpFilePath, BattleGrid pBattleGrid)
	{
		Debug.Log("DumpBattleGrid() pDumpFilePath=" + pDumpFilePath);

		using (FileStream lFileStream = new FileStream(pDumpFilePath, FileMode.Create))
		{
			StreamWriter lStreamWriter = new StreamWriter(lFileStream);
			StringBuilder lStringBuilder = new StringBuilder();

			lStringBuilder.AppendLine("// " + pDumpFilePath);
			lStringBuilder.AppendLine();

			DumpBattleGrid(lStringBuilder, pBattleGrid);

			lStreamWriter.WriteLine(lStringBuilder.ToString());
			lStreamWriter.Close();
			lFileStream.Close();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 진행 정보를 덤프합니다.
	public static void DumpProgressInfo(StringBuilder pStringBuilder, BattleProgressInfo pProgressInfo)
	{
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine("// [START] BattleProgressInfo");
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine();

		// 유저 정보를 덤프합니다.
		for (int iUser = 0; iUser < pProgressInfo.aMatchInfo.mUserCount; ++iUser)
			pStringBuilder.AppendFormat("aUserStates[{0}]={1}\n", iUser, pProgressInfo.aUserStates[iUser]);
		pStringBuilder.AppendLine();

		for (int iUser = 0; iUser < pProgressInfo.aMatchInfo.mUserCount; ++iUser)
			pStringBuilder.AppendFormat("aUserMps[{0}]={1}\n", iUser, pProgressInfo.aUserMps[iUser]);
		pStringBuilder.AppendLine();

		for (int iUser = 0; iUser < pProgressInfo.aMatchInfo.mUserCount; ++iUser)
			pStringBuilder.AppendFormat("aUserExtraMilliMps[{0}]={1}\n", iUser, pProgressInfo.aUserExtraMilliMps[iUser]);
		pStringBuilder.AppendLine();

		// 팀 정보를 덤프합니다.
		for (int iTeam = 0; iTeam < pProgressInfo.aMatchInfo.aTeamCount; ++iTeam)
			pStringBuilder.AppendFormat("aTeamMilliVps[{0}]={1}\n", iTeam, pProgressInfo.aTeamMilliVps[iTeam]);
		pStringBuilder.AppendLine();

		for (int iTeam = 0; iTeam < pProgressInfo.aMatchInfo.aTeamCount; ++iTeam)
			pStringBuilder.AppendFormat("aLastTeamMilliVps[{0}]={1}\n", iTeam, pProgressInfo.aLastTeamMilliVps[iTeam]);
		pStringBuilder.AppendLine();

		for (int iTeam = 0; iTeam < pProgressInfo.aMatchInfo.aTeamCount; ++iTeam)
			pStringBuilder.AppendFormat("aTeamBattleEndCodes[{0}]={1}\n", iTeam, pProgressInfo.aTeamBattleEndCodes[iTeam]);
		pStringBuilder.AppendLine();

		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine("// [End] BattleProgressInfo");
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 상태를 덤프합니다.
	public static void DumpBattleGrid(StringBuilder pStringBuilder, BattleGrid pBattleGrid)
	{
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine("// [START] BattleGrid");
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine();

		// 지휘관 정보를 덤프합니다.
		for (int iCommander = 0; iCommander < pBattleGrid.aCommanders.Length; ++iCommander)
		{
			BattleCommander lCommander = pBattleGrid.aCommanders[iCommander];
			if (lCommander == null)
				continue;

			pStringBuilder.AppendFormat("Commander[{0}] aCoord={1} aDirectionY={2}\n", iCommander, lCommander.aCoord, lCommander.aDirectionY);
		
			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			{
				BattleTroop lTroop = lCommander.aTroops[iTroop];
				if (lTroop == null)
					continue;

				pStringBuilder.AppendFormat("\tTroop[{0}] aCoord={1} aDirectionY={2} CurHp={3}\n", iTroop, lTroop.aCoord, lTroop.aDirectionY, lTroop.aStat.aCurMilliHp / 1000);
			}
		}
		pStringBuilder.AppendLine();


		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
		pStringBuilder.AppendLine("// [END] BattleGrid");
		pStringBuilder.AppendLine("//------------------------------------------------------------------------");
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
}
