using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum BattleProgressEventType
{
	MpBuff,
}

public class BattleProgressEvent : PoolingObject
{
	public delegate void OnMpBuffEventDelegate(int pMpBuffMilliRatio);

	internal class MpBuffInfo
	{
		internal int mMpBuffMilliRatio;
		internal OnMpBuffEventDelegate mOnMpBuffEventDelegate;
	}

	internal LinkedListNode<BattleProgressEvent> mNode;

	internal BattleProgressEventType mEventType;
	internal int mEventStartTime;

	internal MpBuffInfo mMpBuffInfo;

	public BattleProgressEvent()
	{
		mNode = new LinkedListNode<BattleProgressEvent>(this);
	}
	public override void Reset()
	{
	}

	internal void InitMpBuff(int pEventStartTime, int pMpBuffMilliRatio, OnMpBuffEventDelegate pOnMpBuffEventDelegate)
	{
		mEventType = BattleProgressEventType.MpBuff;
		mEventStartTime = pEventStartTime;

		if (mMpBuffInfo == null)
			mMpBuffInfo = new MpBuffInfo();
		mMpBuffInfo.mMpBuffMilliRatio = pMpBuffMilliRatio;
		mMpBuffInfo.mOnMpBuffEventDelegate = pOnMpBuffEventDelegate;
	}

	internal void PlayEvent()
	{
		switch (mEventType)
		{
		case BattleProgressEventType.MpBuff:
			if (mMpBuffInfo.mOnMpBuffEventDelegate != null)
				mMpBuffInfo.mOnMpBuffEventDelegate(mMpBuffInfo.mMpBuffMilliRatio);
			break;
		}
	}
}

public class BattleProgressEventScheduler
{
	public readonly bool cIsEnableEventPooling = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleProgressEventScheduler()
	{
		mEventPool = new ObjectPool<BattleProgressEvent>(cIsEnableEventPooling);
		mEventList = new List<BattleProgressEvent>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Reset()
	{
		foreach (BattleProgressEvent iEvent in mEventList)
			iEvent.Dispose();
		mEventList.Clear();
		mEventCheckIdx = -1;
		mEventCheckTime = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트를 추가합니다.
	public BattleProgressEvent AddMpBuffEvent(int pEventStartTime, int pMpBuffMilliRatio, BattleProgressEvent.OnMpBuffEventDelegate pOnMpBuffEventDelegate)
	{
		BattleProgressEvent lAddEvent = mEventPool.Allocate();
		lAddEvent.InitMpBuff(pEventStartTime, pMpBuffMilliRatio, pOnMpBuffEventDelegate);

		bool lIsAdded = false;
		for (int iEvent = (mEventList.Count - 1); iEvent >= 0; --iEvent)
		{
			if (pEventStartTime >= mEventList[iEvent].mEventStartTime)
			{
				for (int iAddEvent = (iEvent + 1); iAddEvent < mEventList.Count; ++iAddEvent)
					mEventList[iAddEvent] = mEventList[iAddEvent - 1];
				mEventList.Add(mEventList[mEventList.Count - 1]);
				mEventList[iEvent] = lAddEvent;

				lIsAdded = true;
				if (mEventCheckIdx > (iEvent + 1))
					++mEventCheckIdx;
				break;				
			}
		}
		if (!lIsAdded)
			mEventList.Add(lAddEvent);

		if (mEventCheckIdx < 0)
			mEventCheckIdx = 0;

		return lAddEvent;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트를 처리합니다.
	public void CheckEvent(int pFrameDelta)
	{
		if ((mEventCheckIdx < 0) ||
			(mEventCheckIdx >= mEventList.Count))
			return;

		mEventCheckTime += pFrameDelta;

		if (mEventList[mEventCheckIdx].mEventStartTime <= mEventCheckTime)
		{
			mEventList[mEventCheckIdx].PlayEvent();
			++mEventCheckIdx;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ObjectPool<BattleProgressEvent> mEventPool;
	private List<BattleProgressEvent> mEventList;
	private int mEventCheckTime;
	private int mEventCheckIdx;
}
