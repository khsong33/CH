//#define BATTLE_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUpdateScheduler
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int aBattleProgressFrameSliceIdx
	{
		get { return mBattleProgressFrameSliceIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int aAiCollectorFrameSliceIdx
	{
		get { return mAiCollectorFrameSliceIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int[,] aFrameSliceUpdateProcessUserIdxs
	{
		get { return mFrameSliceUpdateProcessUserIdxs; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int[] aFrameSliceUpdateUserProcessCount
	{
		get { return mFrameSliceUpdateUserProcessCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int aBattleFrameTickScale
	{
		get { return mBattleFrameTickScale; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int aBattleFrameTickNo
	{
		get { return mBattleFrameTickNo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스케쥴 정보
	public int aFrameIdxUpdateLimit
	{
		get { return mFrameIdxUpdateLimit; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleUpdateScheduler()
	{
		mFrameSliceUpdateProcessUserIdxs = new int[BattleConfig.get.mFrameSliceCount, BattleMatchInfo.cMaxUserCount];
		mFrameSliceUpdateUserProcessCount = new int[BattleConfig.get.mFrameSliceCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(BattleMatchInfo pMatchInfo, FrameUpdater pFrameUpdater)
	{
		if (pFrameUpdater.aFrameSliceCount > BattleConfig.get.mFrameSliceCount)
			Debug.LogError("pFrameUpdater.aFrameSliceCount overflow");

		// 여러 프레임에 고르게 분산되도록 갱신 스케쥴을 잡습니다.
		for (int iSlice = 0; iSlice < pFrameUpdater.aFrameSliceCount; ++iSlice)
			mFrameSliceUpdateUserProcessCount[iSlice] = 0;

		int lProcessCount = 1 + pMatchInfo.mUserCount + 1; // 전투 진행(1) + 유저별 진행(n) + AI콜렉터 진행(1)
		float lProcessSliceSize = (float)pFrameUpdater.aFrameSliceCount / lProcessCount;

		mBattleProgressFrameSliceIdx = 0;
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + mBattleProgressFrameSliceIdx + "] BattleProgress");
	#endif
		for (int iUser = 0; iUser < pMatchInfo.mUserCount; ++iUser)
		{
			int lFrameSliceIdx = Mathf.RoundToInt((iUser + 1) * lProcessSliceSize);
			if (lFrameSliceIdx >= pFrameUpdater.aFrameSliceCount)
				lFrameSliceIdx = pFrameUpdater.aFrameSliceCount - 1;
			mFrameSliceUpdateProcessUserIdxs[lFrameSliceIdx, mFrameSliceUpdateUserProcessCount[lFrameSliceIdx]++] = iUser;
		#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
			Logger.Log("[" + lFrameSliceIdx + "] User:" + iUser);
		#endif
		}
		mAiCollectorFrameSliceIdx = Mathf.RoundToInt((pMatchInfo.mUserCount + 1) * lProcessSliceSize);
		if (mAiCollectorFrameSliceIdx >= pFrameUpdater.aFrameSliceCount)
			mAiCollectorFrameSliceIdx = pFrameUpdater.aFrameSliceCount - 1;
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + mAiCollectorFrameSliceIdx + "] AiCollector");
	#endif

		// 갱신 초기화를 합니다.
		mBattleFrameTickScale = BattleConfig.get.mTickDelay / pFrameUpdater.aFrameDelta;
		mBattleFrameTickNo = 1;
		mFrameIdxUpdateLimit = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 틱을 갱신합니다.
	public void UpdateTick()
	{
		// 개체 갱신자의 프레임 제한을 갱신합니다.
		mFrameIdxUpdateLimit = mBattleFrameTickNo * mBattleFrameTickScale;
		//Debug.Log("mFrameIdxUpdateLimit=" + mFrameIdxUpdateLimit);

		// 다음 프레임 틱 번호를 증가시킵니다.
		++mBattleFrameTickNo;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mBattleProgressFrameSliceIdx;
	private int mAiCollectorFrameSliceIdx;
	private int[,] mFrameSliceUpdateProcessUserIdxs;
	private int[] mFrameSliceUpdateUserProcessCount;

	private int mBattleFrameTickScale;
	private int mBattleFrameTickNo;
	private int mFrameIdxUpdateLimit;
}
