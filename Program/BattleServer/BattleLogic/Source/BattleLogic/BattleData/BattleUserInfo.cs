using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public enum DeckNation
{
	None = 0, // 중립국으로 사용
	US,
	GE,
	USSR,

	Count
}

public class BattleUserInfo
{
	public const int cMaxCommanderCount = 10;
	public const int cMaxCommanderItemCount = 10;
	public int mUserIdx;
	public String mUserName;
	public int mRating;
	public int mArea;
	public int mUseDeckId;
	public uint mClientGuid;
	public bool mIsAi;
	public uint mHeadquarterGuid; // 맵에서의 본부 GUID
	public int mTeamIdx; // 0은 중립 NPC, 1부터 8까지가 유저 선택 가능한 팀, 9는 예비

	public Dictionary<DeckNation, NationData> mNationData;
	//public int[] mNationLevels;

	public int mWin;
	public int mLose;
	public int mWinStreak;
	public int mLoseStreak;

	public int mMaxMp;
	public int mStartMp;

	public DeckNation mBattleNation;

	public CommanderItem[] mCommanderItems;
	public int mFirstCommanderItemIdx; // 처음에 자동으로 등장할 지휘관 인덱스

	public bool[] mAliveCommanderSlotIdx; // 이미 나와있는 지휘관인지 확인

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 유효한 지휘관 아이템 개수
	public int aValidCommanderItemCount
	{
		get { return mValidCommanderItemCount; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleUserInfo()
	{
		mCommanderItems = new CommanderItem[cMaxCommanderItemCount];
		for (int iItem = 0; iItem < cMaxCommanderItemCount; ++iItem)
			mCommanderItems[iItem] = new CommanderItem(iItem);

		mFirstCommanderItemIdx = 0; // 현재는 0으로 가정하고 나중에 서버로부터 받기로

		mAliveCommanderSlotIdx = new bool[cMaxCommanderCount];
		for (int iSlot = 0; iSlot < cMaxCommanderCount; iSlot++)
			mAliveCommanderSlotIdx[iSlot] = false;

		mValidCommanderItemCount = 0;
		mNationData = new Dictionary<DeckNation, NationData>();
		for (int iNation = 1; iNation < (int)DeckNation.Count; iNation++)
		{
			mNationData.Add((DeckNation)iNation, new NationData());
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 재사용을 위한 리셋
	public void Reset()
	{
		for (int iItem = 0; iItem < cMaxCommanderItemCount; ++iItem)
			mCommanderItems[iItem].Reset();
		mValidCommanderItemCount = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON으로 초기화합니다.
	public void InitFromJson(JSONObject pUserInfoJson)
	{
		Reset();

		uint lRecvClientGuid = (uint)pUserInfoJson.GetField("UniqueId").n;
		//Debug.Log("lRecv Client Id On Match Info - " + lRecvClientGuid);
		int lUserIdx = (int)pUserInfoJson.GetField("UserIdx").n;

		String lRecvAiUser = pUserInfoJson.GetField("Ai").str;
		if (lRecvAiUser.ToUpper().Equals("TRUE"))
			mIsAi = true;
		else
			mIsAi = false;

		mUserName = pUserInfoJson.GetField("UserName").str;
		mUserIdx = lUserIdx;
		mUseDeckId = (int)pUserInfoJson.GetField("UseDeckId").n;
		mRating = (int)pUserInfoJson.GetField("Rating").n;
		mArea = (int)pUserInfoJson.GetField("Area").n;
		mClientGuid = lRecvClientGuid;
		mHeadquarterGuid = (uint)(mUserIdx + 1);
		mTeamIdx = mUserIdx + 1; // 아직 팀 인덱를 지정하는 개념이 없었기에 임시로 여기서 지정했습니다.

		if (pUserInfoJson.GetField("NationInfo") != null)
		{
			JSONObject lNationData = pUserInfoJson.GetField("NationInfo");
			for (int iNation = 1; iNation < (int)DeckNation.Count; iNation++)
			{
				JSONObject lNationInfo = lNationData.GetField(iNation.ToString());
				DeckNation lNation = (DeckNation)lNationInfo.GetField("Nation").n;
				if (!mNationData.ContainsKey(lNation))
				{
					mNationData.Add(lNation, new NationData());
				}
				mNationData[lNation].mNation = lNation;
				mNationData[lNation].mNationLevel = (int)lNationInfo.GetField("NationLevel").n;
				mNationData[lNation].mNationExp = (int)lNationInfo.GetField("NationExp").n;
			}
		}

		mWin = (int)pUserInfoJson.GetField("Win").n;
		mLose = (int)pUserInfoJson.GetField("Lose").n;
		mWinStreak = (int)pUserInfoJson.GetField("WinStreak").n;
		JSONObject lLoseStreakObject = pUserInfoJson.GetField("LoseStreak"); // LoseStreak은 나중에 추가된 것이라 없을 수 있음
		mLoseStreak = (lLoseStreakObject != null) ? (int)lLoseStreakObject.n : 0;

		mMaxMp = BattleConfig.get.mTestMaxMp;
		mStartMp = BattleConfig.get.mTestStartMp;

		mBattleNation = (DeckNation)pUserInfoJson.GetField("UseDeckNation").n;

		JSONObject lDeckJson = pUserInfoJson.GetField("Decks"); // 현재 서버에서는 덱 리스트가 아닌 덱 하나를 받아오는 상태임,'Decks'라는 필드명 때문에 혼동하지 말 것
		JSONObject lCommanderItemListJson = lDeckJson.GetField("Items");
		int lCommanderItemCount = (int)lDeckJson.GetField("Count").n;
		for (int iItem = 0; iItem < lCommanderItemCount; iItem++)
		{
			JSONObject lCommanderItemJson = lCommanderItemListJson.list[iItem];
			int lSlotIdx = (int)lCommanderItemJson.GetField("SlotId").n;
			CommanderItemData lCommanderItemData = new CommanderItemData();
			lCommanderItemData.InitFromJson(lCommanderItemJson);
			AddCommanderItem(lSlotIdx, lCommanderItemData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 객체를 만들어서 반환합니다.
	public JSONObject ToJson()
	{
		JSONObject lUserInfoJson = new JSONObject();

		lUserInfoJson.AddField("UniqueId", mClientGuid);
		lUserInfoJson.AddField("UserName", mUserName);
		lUserInfoJson.AddField("GuildId", 0);
		lUserInfoJson.AddField("Win", mWin);
		lUserInfoJson.AddField("Lose", mLose);
		lUserInfoJson.AddField("WinStreak", mWinStreak);
		lUserInfoJson.AddField("LoseStreak", mLoseStreak);
		lUserInfoJson.AddField("Area", mArea);
		lUserInfoJson.AddField("Rating", mRating);
		lUserInfoJson.AddField("UseDeckId", mUseDeckId);
		lUserInfoJson.AddField("UseDeckNation", (int)mBattleNation);

		JSONObject lNationInfoJson = new JSONObject();
		for (int iNation = 1; iNation < (int)DeckNation.Count; iNation++)
		{
			JSONObject lNationData = new JSONObject();
			lNationData.AddField("Nation", iNation);
			lNationData.AddField("NationLevel", mNationData[(DeckNation)iNation].mNationLevel);
			lNationData.AddField("NationExp", mNationData[(DeckNation)iNation].mNationExp);
			lNationInfoJson.AddField(iNation.ToString(), lNationData);
		}
		lUserInfoJson.AddField("NationInfo", lNationInfoJson);

		lUserInfoJson.AddField("ClearTutorial", -1);
		lUserInfoJson.AddField("UserKey", String.Empty);

		JSONObject lCommanderItemArrayJson = new JSONObject(JSONObject.Type.ARRAY);
		int lCommanderItemCount = 0;
		CommanderItemData lCommanderItemData = new CommanderItemData();
		for (int iItem = 0; iItem < mCommanderItems.Length; iItem++)
		{
			CommanderItem lCommanderItem = mCommanderItems[iItem];
			if (lCommanderItem != null)
				++lCommanderItemCount;
			else
				continue;

			lCommanderItemData.InitFromItem(lCommanderItem);
			JSONObject lCommanderItemJson = lCommanderItemData.ToJson();
			lCommanderItemJson.AddField("SlotId", iItem);
			lCommanderItemArrayJson.Add(lCommanderItemJson);
		}

		JSONObject lDeckNode = new JSONObject();
		lDeckNode.AddField("Items", lCommanderItemArrayJson);
		lDeckNode.AddField("Count", lCommanderItemCount);

		lUserInfoJson.AddField("Decks", lDeckNode); // 현재 서버에서는 덱 리스트가 아닌 덱 하나를 받아오는 상태임,'Decks'라는 필드명 때문에 혼동하지 말 것

		lUserInfoJson.AddField("Ai", mIsAi ? "TRUE" : "FALSE");
		lUserInfoJson.AddField("UserIdx", mUserIdx);

		return lUserInfoJson;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환 되어야할 지휘관 SlotIdx를 얻어옵니다.
	public int GetSpawnCommanderSlotIdx()
	{
		for (int iSlot = 0; iSlot < cMaxCommanderCount; iSlot++)
		{
			if (!mAliveCommanderSlotIdx[iSlot])
			{
				mAliveCommanderSlotIdx[iSlot] = true;
				return iSlot;
			}
		}
		return -1;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소멸 된 지휘관의 SlotIdx를 반환합니다.
	public void RestoreCommanderSlotIdx(int pSlotIdx)
	{
		mAliveCommanderSlotIdx[pSlotIdx] = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환할 Slot이 있는지 확인합니다.
	public bool IsSpawnAvailableSlot()
	{
		for (int iSlot = 0; iSlot < cMaxCommanderCount; iSlot++)
		{
			if (!mAliveCommanderSlotIdx[iSlot])
				return true;
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 넣습니다.
	public void AddCommanderItem(CommanderItem pCloneCommanderItem)
	{
		mCommanderItems[pCloneCommanderItem.mSlotIdx].Init(pCloneCommanderItem);

		if (mCommanderItems[pCloneCommanderItem.mSlotIdx].mCount > 0)
			++mValidCommanderItemCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 넣습니다.
	public void AddCommanderItem(int pSlotIdx, CommanderItemData pCommanderItemData)
	{
		mCommanderItems[pSlotIdx].Init(CommanderItemType.UserCommander, pCommanderItemData);

		if (mCommanderItems[pSlotIdx].mCount > 0)
			++mValidCommanderItemCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 빼냅니다.
	public void RemoveCommanderItem(int pSlot)
	{
		if (mCommanderItems[pSlot].mCommanderItemState == CommanderItemState.Retreat)
			return;

		mCommanderItems[pSlot].OnDestoryCommander();

		if (mCommanderItems[pSlot].mCommanderItemState == CommanderItemState.Retreat)
			--mValidCommanderItemCount;
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mValidCommanderItemCount;
}
