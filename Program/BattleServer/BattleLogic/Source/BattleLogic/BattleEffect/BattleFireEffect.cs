using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleFireEffect : BattleEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 공격 팀 인덱스
	public int aAttackTeamIdx
	{
		get { return mAttackTeamIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 좌표
	public Coord2 aAttackCoord
	{
		get { return mAttackCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 부대
	public BattleTroop aAttackTroop
	{
		get { return mAttackTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 부대 스탯
	public BattleTroopStat aAttackTroopStat
	{
		get { return mAttackTroopStat; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 발사 무기 데이터
	public BattleTroopStat.WeaponData aFireWeaponData
	{
		get { return mFireWeaponData; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대
	public BattleTroop aTargetTroop
	{
		get { return mTargetTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 착탄 결과
	public TroopHitResult aHitResult
	{
		get { return mHitResult; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 착탄 좌표
	public Coord2 aHitCoord
	{
		get { return mHitCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 착탄 딜레이
	public int aHitDelay
	{
		get { return mHitDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageFireEffect aStageFireEffect
	{
		get { return mStageFireEffect; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 속성
	public override IStageEffect aStageEffect
	{
		get { return mStageFireEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleFireEffect(aGuid:{0} aAttackTeamIdx:{1})", aGuid, mAttackTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleFireEffect(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mAttackTroopLink = new GuidLink<BattleTroop>();
		mTargetTroopLink = new GuidLink<BattleTroop>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateFireEffect(BattleTroopTargetingInfo pTroopTargetingInfo)
	{
		BattleTroop lAttackTroop = pTroopTargetingInfo.aAttackTroop;

		mAttackTeamIdx = lAttackTroop.aTeamIdx;

		mAttackCoord = lAttackTroop.aCoord;
		mAttackTroopLink.Set(lAttackTroop);

		mAttackTroopStat = lAttackTroop.aStat;
		mAttackTroopStat.AddRef(); // 스탯 참조 증가

		mFireWeaponData = pTroopTargetingInfo.aSelectedWeaponData;

		mTargetTroopLink.Set(pTroopTargetingInfo.aTargetTroop);

		mWeaponRangeLevel = pTroopTargetingInfo.aSelectedWeaponRangeLevel;

		mHitResult = pTroopTargetingInfo.RollHitResult(out mHitCoord);
		//Debug.Log("mHitResult=" + mHitResult + " mHitCoord=" + mHitCoord);

		mHitDirectionY = (mHitCoord - mAttackCoord).RoughDirectionY();

		int lBulletVelocity = mFireWeaponData.aWeaponStatTable.Get(WeaponStat.BulletVelocity);
		mHitDelay = pTroopTargetingInfo.aTargetDistance * 1000 / lBulletVelocity; // lBulletVelocity가 1초당 거리이므로 밀리초 딜레이가 되도록 1000을 곱함

		// 상위 버전을 호출합니다.
		OnCreateEffect(
			lAttackTroop.aBattleGrid,
			mHitDelay);

		// 스테이지 이펙트를 생성합니다.
		if (mHitResult == TroopHitResult.GroundHit)
			mStageFireEffect = aBattleGrid.aStage.CreateFireEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		mAttackTroopStat.ReleaseRef(); // 스탯 참조 감소

		// 스테이지 이펙트를 제거합니다.
		if (mStageFireEffect != null)
		{
			mStageFireEffect.DestroyEffect();
			mStageFireEffect = null;
		}

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyFireEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 콜백
	public override void OnActivateEffect()
	{
		//Debug.Log("OnActivateEffect() - " + this);

		BattleTroop lTargetTroop = mTargetTroopLink.Get();
		switch (mHitResult)
		{
		case TroopHitResult.Penetrate:
			{
				if (lTargetTroop != null)
					_AddDirectDamage(lTargetTroop);
			}
			break;
		case TroopHitResult.GroundHit:
			{
				if (mFireWeaponData.aSqrMaxAoeRadius > 0)
					aBattleGrid.CallTroopHandler(
						mHitCoord,
						mFireWeaponData.aMaxAoeRadius,
						mAttackTeamIdx,
						BattleGrid.TeamSelection.NonAllyTeam,
						_OnAddRangeDamageAndSuppress);
			}
			break;
		default:
			{
				if (lTargetTroop != null)
				{
					lTargetTroop.aStageTroop.ShowHit(mHitResult);

					lTargetTroop.OnAttackedByEnemy(this, 0);
				}
			}
			break;
		}

		if (mFireWeaponData.aWeaponStatTable.Get(WeaponStat.SuppressionRange) > 0)
			aBattleGrid.CallTroopHandler(
				(lTargetTroop != null) ? lTargetTroop.aCoord : mHitCoord,
				mFireWeaponData.aWeaponStatTable.Get(WeaponStat.SuppressionRange),
				mAttackTeamIdx,
				BattleGrid.TeamSelection.NonAllyTeam,
				_OnAddDirectionalSuppress);
		else if (lTargetTroop != null)
			_OnAddDirectionalSuppress(lTargetTroop, 0);

		// 스테이지에 표시합니다.
		if (mStageFireEffect != null)
			mStageFireEffect.Activate(mHitCoord);

		// 효과를 처리했으므로 파괴합니다.
		Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대에 직격 데미지를 줍니다.
	private void _AddDirectDamage(BattleTroop pDamageTroop)
	{
		if (pDamageTroop.aIsNoDamage)
			return; // 데미지 안 받음

		int lDirectMilliDamage = mFireWeaponData.aWeaponRangeStatTables[mWeaponRangeLevel].Get(WeaponRangeStat.MilliDamage);

		int lActualMilliDamage = -pDamageTroop.UpdateHp(TroopHpChangeType.DirectDamage, -lDirectMilliDamage);

		pDamageTroop.aStageTroop.ShowHit(TroopHitResult.Penetrate);

		pDamageTroop.OnAttackedByEnemy(this, lActualMilliDamage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대를 중심으로한 방향성 위협을 줍니다.
	private bool _OnAddDirectionalSuppress(BattleTroop pDamageTroop, int lSqrDamageTroopDistance)
	{
		if (pDamageTroop.aIsNoDamage)
			return true; // 데미지 안 받음

		int lDegreeVector = MathUtil.DegreeVector(mHitDirectionY, pDamageTroop.aDirectionY);
		int lAbsDegreeVector = Mathf.Abs(lDegreeVector);
		int lSuppressionMultiplyRatio;
		TroopStatTable lTargetTroopStatTable = pDamageTroop.aStat.aTroopStatTable;
		int lDamageTroopArmor;

		int lGroundArmorAdd;
		int lSupperssionRatioValue;
		_ComputeGroundArmorAddAndSuppretionRatio(pDamageTroop, out lGroundArmorAdd, out lSupperssionRatioValue);

		if (lAbsDegreeVector >= BattleConfig.get.mSideSuppressionAngleDistance) // 정면 위협
		{
			lSuppressionMultiplyRatio = (int)((Int64)BattleConfig.get.mFrontSuppressionMultiplyRatio * lSupperssionRatioValue / 10000); // 만분율
			lDamageTroopArmor = lTargetTroopStatTable.Get(TroopStat.FrontArmor) + lGroundArmorAdd;
		}
		else if (lAbsDegreeVector > BattleConfig.get.mRearSuppressionAngleDistance) // 측면 위협 (측면 값은 경계 수치 제외)
		{
			lSuppressionMultiplyRatio = (int)((Int64)BattleConfig.get.mSideSuppressionMultiplyRatio * lSupperssionRatioValue / 10000); // 만분율
			lDamageTroopArmor = lTargetTroopStatTable.Get(TroopStat.SideArmor) + lGroundArmorAdd;
		}
		else // 후면 위협
		{
			lSuppressionMultiplyRatio = (int)((Int64)BattleConfig.get.mRearSuppressionAngleDistance * lSupperssionRatioValue / 10000); // 만분율
			lDamageTroopArmor = lTargetTroopStatTable.Get(TroopStat.RearArmor) + lGroundArmorAdd;
		}
		//Debug.Log("lDamageTroopArmor - " + lDamageTroopArmor);
		int lSuppressionMilliPoint
			= (int)((Int64)mFireWeaponData.aWeaponRangeStatTables[mWeaponRangeLevel].Get(WeaponRangeStat.RangeSuppressionMilliPoint)
			* lSuppressionMultiplyRatio
			/ 10000); // 만분율
		if (lDamageTroopArmor > 0)
		{
			int lPenetration = mFireWeaponData.aWeaponRangeStatTables[mWeaponRangeLevel].Get(WeaponRangeStat.Penetration);
			lSuppressionMilliPoint = lSuppressionMilliPoint * lPenetration / lDamageTroopArmor;
		}

		pDamageTroop.aStat.AddSuppression(lSuppressionMilliPoint);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnObjectHandlingDelegate<BattleTroop>
	private bool _OnAddRangeDamageAndSuppress(BattleTroop pDamageTroop, int lSqrDamageTroopDistance)
	{
		if (pDamageTroop.aIsNoDamage)
			return true; // 데미지 안 받음

		int lGroundArmorAdd;
		int lSupperssionRatioValue;
		_ComputeGroundArmorAddAndSuppretionRatio(pDamageTroop, out lGroundArmorAdd, out lSupperssionRatioValue);

		for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
		{
			WeaponAoeStatTable lWeaponAoeStatTable = mFireWeaponData.aWeaponAoeStatTables[iAoe];

			int lCheckRange = lWeaponAoeStatTable.aRadius + pDamageTroop.aTroopSpec.mUnitRadius;
			int lSqrCheckRange = lCheckRange * lCheckRange;

			if (lSqrDamageTroopDistance <= lSqrCheckRange)
			{
				// 범위 데미지를 줍니다.
				int lDamageTroopArmor = pDamageTroop.aStat.aTroopStatTable.Get(TroopStat.FrontArmor);
				if ((iAoe > 0) || pDamageTroop.aAffectedGroundSpec.mIsCeiling) // 직격 폭발이 아니거나 천장이 있다면
					lDamageTroopArmor += lGroundArmorAdd; // 지형 효과 아머 추가 
				int lPenetrationProb = 100;
				if (lDamageTroopArmor > 0)
				{
					lPenetrationProb
						= (int)((Int64)mFireWeaponData.aWeaponRangeStatTables[0].Get(WeaponRangeStat.Penetration)
						* lWeaponAoeStatTable.Get(WeaponAoeStat.PenetrationMultiplyRatio) / 100 / lDamageTroopArmor); // 만분율이므로 확률값이 되려면 * 100 
				}

				if (aBattleGrid.aSyncRandom.CheckProb(lPenetrationProb))
				{
					int lAoeMilliDamage
						= (int)((Int64)mFireWeaponData.aWeaponRangeStatTables[mWeaponRangeLevel].Get(WeaponRangeStat.MilliDamage)
						* lWeaponAoeStatTable.Get(WeaponAoeStat.DamageMultiplyRatio)
						/ 10000); // 만분율

					int lActualMilliDamage = -pDamageTroop.UpdateHp(
						(iAoe == 0) ? TroopHpChangeType.DirectDamage : TroopHpChangeType.RangeDamage, // 중앙에서의 최근접 히트는 범위 타격이라도 직격 데미지로 간주
						-lAoeMilliDamage);

					pDamageTroop.aStageTroop.ShowHit((iAoe == 0) ? TroopHitResult.Penetrate : TroopHitResult.GroundHit); // 중앙에서의 최근접 히트는 범위 타격이라도 직격 데미지로 간주
#if BATTLE_VERIFY_TEST
                    BattleLogicTestStatistics.Log(pDamageTroop.aGuid, "GroundHitResult", (iAoe == 0) ? TroopHitResult.Penetrate : TroopHitResult.GroundHit);
#endif
					pDamageTroop.OnAttackedByEnemy(this, lActualMilliDamage);
				}
                else
                {
#if BATTLE_VERIFY_TEST
                    BattleLogicTestStatistics.Log(pDamageTroop.aGuid, "GroundHitResult", TroopHitResult.Block);
#endif
                }

				// 범위 위협을 줍니다.
				int lSuppressionMilliPoint
					= ((iAoe > 0) || pDamageTroop.aAffectedGroundSpec.mIsCeiling) // 직격 폭발이 아니거나 천장이 있다면
					? (lWeaponAoeStatTable.Get(WeaponAoeStat.AoeSuppressionMilliPoint) * lSupperssionRatioValue / 10000) // 지형 효과 위협 수치 보정
					: lWeaponAoeStatTable.Get(WeaponAoeStat.AoeSuppressionMilliPoint); // 아니라면 위협 수치 그대로 받음

				pDamageTroop.aStat.AddSuppression(lSuppressionMilliPoint);

				// 데미지를 주고 안주고에 관계 없이 범위에 있었으므로 공격 받음을 알립니다.
				pDamageTroop.OnAttackedByEnemy(this, 0);

				return true;
			}			
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형에 의한 장갑과 위협 보정 수치를 계산합니다.
	private void _ComputeGroundArmorAddAndSuppretionRatio(BattleTroop pDamageTroop, out int oGroundArmorAdd, out int oSupperssionRatioValue)
	{
		oGroundArmorAdd = 0;
		oSupperssionRatioValue = 10000; // 만분율

		GroundStatTable lGroundStatTable = pDamageTroop.aAffectedGroundStatTable;
		if (!lGroundStatTable.CheckEffectAvailableType(pDamageTroop.aTroopSpec.mType))
			return; // 영향을 받지 않음

		int lSupperssionRatioOffsetSum = 0;
		int lSupperssionRatioOffsetCount = 0;

		// 지형 자체의 장갑을 반영합니다.
		if (lGroundStatTable.aTileArmorSpec != null)
		{
			int lTileArmorDegreeVector = MathUtil.DegreeVector(mHitDirectionY + 180, lGroundStatTable.aTileArmorDirectionY);
			if (Mathf.Abs(lTileArmorDegreeVector) <= lGroundStatTable.aTileArmorEffectAngleHalf)
			{
				oGroundArmorAdd = lGroundStatTable.aTileArmorSpec.mArmorAdd; // 장갑 보정은 합
				lSupperssionRatioOffsetSum += lGroundStatTable.aTileArmorSpec.mSuppressionRatio - oSupperssionRatioValue; // 위협 보정은 평균
				++lSupperssionRatioOffsetCount;
			}
		}

		// 스킬에 의한 장갑을 반영합니다.
		if (lGroundStatTable.aSkillArmorSpec != null)
		{
			int lSkillArmorDegreeVector = MathUtil.DegreeVector(mHitDirectionY + 180, lGroundStatTable.aSkillArmorDirectionY);
			if (Mathf.Abs(lSkillArmorDegreeVector) <= lGroundStatTable.aSkillArmorEffectAngleHalf)
			{
				oGroundArmorAdd = lGroundStatTable.aSkillArmorSpec.mArmorAdd; // 장갑 보정은 합
				lSupperssionRatioOffsetSum += lGroundStatTable.aSkillArmorSpec.mSuppressionRatio - oSupperssionRatioValue; // 위협 보정은 평균
				++lSupperssionRatioOffsetCount;
			}
		}

		if (lSupperssionRatioOffsetCount <= 1)
			oSupperssionRatioValue += lSupperssionRatioOffsetSum;
		else
			oSupperssionRatioValue += lSupperssionRatioOffsetSum / lSupperssionRatioOffsetCount;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mAttackTeamIdx;
	private Coord2 mAttackCoord;
	private GuidLink<BattleTroop> mAttackTroopLink;
	private BattleTroopStat mAttackTroopStat;
	private BattleTroopStat.WeaponData mFireWeaponData;

	private GuidLink<BattleTroop> mTargetTroopLink;
	private int mWeaponRangeLevel;

	private TroopHitResult mHitResult;
	private Coord2 mHitCoord;
	private int mHitDirectionY;
	private int mHitDelay;

	private IStageFireEffect mStageFireEffect;
}
