using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopSkillEffect : BattleEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 효과
	public TroopEffectSpec aTroopEffectSpec
	{
		get { return mTroopEffectSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과를 받는 부대
	public BattleTroop aEffectTroop
	{
		get { return mEffectTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageTroopSkillEffect aStageTroopSkillEffect
	{
		get { return mStageTroopSkillEffect; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 속성
	public override IStageEffect aStageEffect
	{
		get { return mStageTroopSkillEffect; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경자
	public StatTableModifier<TroopStat> aTroopStatTableModifier
	{
		get { return mTroopStatTableModifier; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경자
	public StatTableModifier<WeaponStat>[] aWeaponStatTableModifiers
	{
		get { return mWeaponStatTableModifiers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경자
	public StatTableModifier<WeaponRangeStat>[] aWeaponRangeStatTableModifiers
	{
		get { return mWeaponRangeStatTableModifiers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경자
	public StatTableModifier<WeaponAoeStat>[] aWeaponAoeStatTableModifiers
	{
		get { return mWeaponAoeStatTableModifiers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지속 시간
	public int aDurationDelay
	{
		get { return mDurationDelay; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleTroopSkillEffect(aGuid:{0} aEffectTroop:{1})", aGuid, aEffectTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopSkillEffect(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mEffectTroopLink = new GuidLink<BattleTroop>();

		mWeaponStatTableModifiers = new StatTableModifier<WeaponStat>[TroopSpec.cWeaponCount];
		mWeaponRangeStatTableModifiers = new StatTableModifier<WeaponRangeStat>[TroopSpec.cWeaponCount];
		mWeaponAoeStatTableModifiers = new StatTableModifier<WeaponAoeStat>[TroopSpec.cWeaponCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateTroopSkillEffect(BattleTroop pEffectTroop, TroopEffectSpec pTroopEffectSpec, int pActivationDelay, int pDurationDelay)
	{
		// 초기화를 합니다.
		mTroopEffectSpec = pTroopEffectSpec;
		mEffectTroopLink.Set(pEffectTroop);
		mDurationDelay = pDurationDelay;

		// 상위 버전을 호출합니다.
		OnCreateEffect(pEffectTroop.aBattleGrid, pActivationDelay);

		// 스테이지 이펙트를 생성합니다.
		mStageTroopSkillEffect = aBattleGrid.aStage.CreateTroopSkillEffect(this);

		// 스탯 변경자를 초기화합니다.
		mTroopStatTableModifier = null;
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			mWeaponStatTableModifiers[iWeapon] = null;
			mWeaponRangeStatTableModifiers[iWeapon] = null;
			mWeaponAoeStatTableModifiers[iWeapon] = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지 이펙트를 제거합니다.
		mStageTroopSkillEffect.DestroyEffect();
		mStageTroopSkillEffect = null;

		// 부대 스탯에서 효과를 제거합니다.
		if (mChainItem != null)
		{
			BattleTroop lEffectTroop = mEffectTroopLink.Get();
			if (lEffectTroop != null)
				lEffectTroop.aStat.RemoveSkillEffect(mChainItem);

			mChainItem = null;
		}

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyTroopSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 콜백
	public override void OnActivateEffect()
	{
		//Debug.Log("OnActivateEffect() - " + this);

		// 효과를 적용할 부대를 얻습니다.
		BattleTroop lEffectTroop = mEffectTroopLink.Get();
		if (lEffectTroop == null)
		{
			Destroy();
			return;
		}

		// 스탯 변경자를 지정합니다.
		mTroopStatTableModifier = mTroopEffectSpec.mTroopStatTableModifier;
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			mWeaponStatTableModifiers[iWeapon] = mTroopEffectSpec.mWeaponStatTableModifiers[iWeapon];
			mWeaponRangeStatTableModifiers[iWeapon] = mTroopEffectSpec.mWeaponRangeStatTableModifiers[iWeapon];
			mWeaponAoeStatTableModifiers[iWeapon] = mTroopEffectSpec.mWeaponAoeStatTableModifiers[iWeapon];
		}

		// 부대 스탯에 효과를 적용합니다.
		mChainItem = lEffectTroop.aStat.AddSkillEffect(this);

		// 스테이지에 표시합니다.
		if (mStageTroopSkillEffect != null)
			mStageTroopSkillEffect.Activate();

		// 지속 시간 제약이 있는 경우 비활성화를 예약합니다(토클 스킬의 경우는 Destroy()를 바로 호출).
		if (mDurationDelay > 0)
			ReserveTask(_OnDeActivated, mDurationDelay);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnDeActivated(FrameTask pTask, int pFrameDelta)
	{
		Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private GuidLink<BattleTroop> mEffectTroopLink;
	private TroopEffectSpec mTroopEffectSpec;
	private GuidObjectChainItem<BattleTroopSkillEffect> mChainItem;

	private IStageTroopSkillEffect mStageTroopSkillEffect;

	private StatTableModifier<TroopStat> mTroopStatTableModifier;
	private StatTableModifier<WeaponStat>[] mWeaponStatTableModifiers;
	private StatTableModifier<WeaponRangeStat>[] mWeaponRangeStatTableModifiers;
	private StatTableModifier<WeaponAoeStat>[] mWeaponAoeStatTableModifiers;

	private int mDurationDelay;
}
