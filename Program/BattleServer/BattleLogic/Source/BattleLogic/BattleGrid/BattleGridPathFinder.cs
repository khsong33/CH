using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGridPathFinder
{
	public const int cMaxWeight = AStarPathFinder<BattleGridTile>.cMaxWeight;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(BattleGrid pBattleGrid, int pAStarHeuristic)
	{
		mTiles = pBattleGrid.aTiles;

		mPathFinder = new AStarPathFinder<BattleGridTile>();
		mPathFinder.InitNode(
			pBattleGrid.aTiles,
			pBattleGrid.aTileRowCount,
			pBattleGrid.aTileColCount,
			pBattleGrid.aTileScaleX,
			pBattleGrid.aTileScaleZ,
			pBattleGrid.aTileBlockScaleX,
			pBattleGrid.aTileBlockScaleZ,
			pAStarHeuristic);

		mStartPathTiles = new BattleGridTile[Mathf.Max(pBattleGrid.aTileRowCount, pBattleGrid.aTileColCount)];
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 이동 경로를 찾습니다.
	public void SearchTilePath(BattleGridTile pStartTile, BattleGridTile pEndTile, OnGetNodeWeightDelegate pOnGetNodeWeightDelegate, BattleGridTile[] pPathTiles, out int oPathTileCount)
	{
		OnGetNodeWeightDelegate lOnGetNodeWeightDelegate
			= (pOnGetNodeWeightDelegate != null) // 노드 비중을 얻는 델리게이트가 제공된다면
			? pOnGetNodeWeightDelegate // 그 델리게이트를 사용하고
			: AStarPathFinder <BattleGridTile>.OnGetNodeWeight; // 아니라면 노드의 aWeight를 그대로 사용하는 기본 델리게이트를 사용

		BattleGridTile lStartPathTile;
		int lStartPathTileCount;
		if (lOnGetNodeWeightDelegate(pStartTile) < cMaxWeight)
		{
			lStartPathTile = pStartTile;
			lStartPathTileCount = 0;
		}
		else
		{
			lStartPathTile = GetNearestMovableTile(pStartTile, pEndTile, pOnGetNodeWeightDelegate, mStartPathTiles, out lStartPathTileCount);
		}

		BattleGridTile lEndPathTile;
		int lEndPathTileCount;
		if (lOnGetNodeWeightDelegate(pEndTile) < cMaxWeight)
		{
			lEndPathTile = pEndTile;
			lEndPathTileCount = 0;
		}
		else
		{
			lEndPathTile = GetNearestMovableTile(pEndTile, pStartTile, pOnGetNodeWeightDelegate, null, out lEndPathTileCount);
		}

		int lSearchCount = mPathFinder.Search(
			lEndPathTile, // pPathTiles의 길이가 모자라면 앞부분이 잘려서 들어오므로 목표 지점에서 시작 지점을 찾는 방식을 사용
			lStartPathTile, // 그렇게 되면 시작 지점에서 (목표에는 도달 못하더라도) 목표에 근접한 지점까지의 좌표를 얻게 됨
			pOnGetNodeWeightDelegate,
			pPathTiles,
			out oPathTileCount);

	#if UNITY_EDITOR
		if (BattleConfig.get.aDebugSetting.mIsShowSearchCount)
			Debug.Log("SearchTilePath() lSearchCount=" + lSearchCount);
	#endif

		// 앞 부분의 탈출 경로를 붙입니다.
		if (lStartPathTileCount > 0)
		{
			//Debug.Log("SearchTilePath() added " + lStartPathTileCount + " block tiles");
			for (int iTile = (oPathTileCount - 1); iTile >= 0; --iTile)
				pPathTiles[iTile + lStartPathTileCount] = pPathTiles[iTile]; // 뒤로 lStartPathTileCount만큼 옮기고
			for (int iTile = 0; iTile < lStartPathTileCount; ++iTile)
				pPathTiles[iTile] = mStartPathTiles[iTile]; // 앞에 mStartPathTiles를 붙여 넣습니다.
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 방향 타일에 근접하는 블럭이 아닌 타일을 얻습니다.
	public BattleGridTile GetNearestMovableTile(BattleGridTile pStartTile, BattleGridTile pEndTile, OnGetNodeWeightDelegate pOnGetNodeWeightDelegate, BattleGridTile[] pPathTiles, out int oPathTileCount)
	{
		OnGetNodeWeightDelegate lOnGetNodeWeightDelegate
			= (pOnGetNodeWeightDelegate != null) // 노드 비중을 얻는 델리게이트가 제공된다면
			? pOnGetNodeWeightDelegate // 그 델리게이트를 사용하고
			: AStarPathFinder<BattleGridTile>.OnGetNodeWeight; // 아니라면 노드의 aWeight를 그대로 사용하는 기본 델리게이트를 사용

		BattleGridTile lNearestTile = pStartTile;
		oPathTileCount = 0;

		while (lOnGetNodeWeightDelegate(lNearestTile) >= cMaxWeight)
		{
			if (pPathTiles != null)
				pPathTiles[oPathTileCount++] = lNearestTile; // 경로를 저장

			if (lNearestTile == pEndTile)
				break;

			int lRowVector = pEndTile.aRow - lNearestTile.aRow;
			int lColVector = pEndTile.aCol - lNearestTile.aCol;

			if (lRowVector >= 0) // 위쪽 방향
			{
				int lRowDistance = lRowVector;

				if (lColVector >= 0) // 오른쪽 방향
				{
					int lColDistance = lColVector;

					if (lRowDistance >= (lColDistance * 2))
						lNearestTile = mTiles[lNearestTile.aRow + 1, lNearestTile.aCol]; // 위로
					else
						lNearestTile = mTiles[lNearestTile.aRow + (((lNearestTile.aCol & 0x01) == 0) ? 1 : 0), lNearestTile.aCol + 1]; // 오른쪽 위로
				}
				else // 왼쪽 방향
				{
					int lColDistance = -lColVector;

					if (lRowDistance >= (lColDistance * 2))
						lNearestTile = mTiles[lNearestTile.aRow + 1, lNearestTile.aCol]; // 위로
					else
						lNearestTile = mTiles[lNearestTile.aRow + (((lNearestTile.aCol & 0x01) == 0) ? 1 : 0), lNearestTile.aCol - 1]; // 왼쪽 위로
				}
			}
			else // 아래쪽 방향
			{
				int lRowDistance = -lRowVector;

				if (lColVector >= 0) // 오른쪽 방향
				{
					int lColDistance = lColVector;

					if (lRowDistance >= (lColDistance * 2))
						lNearestTile = mTiles[lNearestTile.aRow - 1, lNearestTile.aCol]; // 아래로
					else
						lNearestTile = mTiles[lNearestTile.aRow - (((lNearestTile.aCol & 0x01) == 0) ? 0 : 1), lNearestTile.aCol + 1]; // 오른쪽 아래로
				}
				else // 왼쪽 방향
				{
					int lColDistance = -lColVector;

					if (lRowDistance >= (lColDistance * 2))
						lNearestTile = mTiles[lNearestTile.aRow - 1, lNearestTile.aCol]; // 아래로
					else
						lNearestTile = mTiles[lNearestTile.aRow - (((lNearestTile.aCol & 0x01) == 0) ? 0 : 1), lNearestTile.aCol - 1]; // 왼쪽 아래로
				}
			}
		}

		//Debug.Log("GetNearestMovableTile() pStartTile=" + pStartTile + " pEndTile=" + pEndTile + " lNearestTile=" + lNearestTile);
		return lNearestTile;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGridTile[,] mTiles;

	private AStarPathFinder<BattleGridTile> mPathFinder;
	private BattleGridTile[] mStartPathTiles;
}
