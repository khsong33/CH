using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum BattleGridTileType
{
	Way,
	Headquarter,
	CheckPoint,
	EventPoint,

	Block, // [마지막] 무조건 갈수 없는 게임 외 공간
}

public enum BattleGridTileAttributeType
{
	Tile, // 타일 타입
	Param, // 파라미터
	Ground, // 지형 타입
	Weight, // 경로 비중
	DirectionY, // 방향각

	None,
}

public class BattleGridTile : AStarNode
{
	public delegate void OnHandleTileDelegate(BattleGridTile pTile);

	public const int cTileTypeCount = (int)(BattleGridTileType.Block + 1);
	public const int cAttributeTypeCount = (int)BattleGridTileAttributeType.None;

	public const char cLineSplitChar = '%';
	public const char cAttributeTokenSplitChar = ':';
	public const char cParamTokenSplitChar = '^';

	public const int cParam1Index = 0;
	public const int cParam2Index = 1;
	public const int cParam3Index = 2;
	public const int cParam4Index = 3;
	public const int cExtraParamIndex = 4;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 인덱스
	public RowCol aTileIndex
	{
		get { return mTileIndex; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AStarNode 속성
	public int aRow
	{
		get { return mTileIndex.row; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AStarNode 속성
	public int aCol
	{
		get { return mTileIndex.col; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 중심 좌표
	public Coord2 aCenterCoord
	{
		get { return mCenterCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 타입
	public BattleGridTileType aTileType
	{
		get { return mTileType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 타입 스펙
	public GroundSpec aGroundSpec
	{
		get { return mGroundSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 스탯 테이블
	public GroundStatTable aGroundStatTable
	{
		get { return mGroundStatTable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 연결된 이벤트 지점
	public BattleEventPoint aLinkEventPoint
	{
		get { return mLinkEventPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AStarNode 속성
	public int aWeight
	{
		get { return mMapWeight; }
		set { mMapWeight = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AStarNode 속성
	public bool aIsBlocked
	{
		get { return (mMapWeight >= BattleGridPathFinder.cMaxWeight); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각
	public int aDirectionY
	{
		get { return mDirectionY; }
		set
		{
			mDirectionY = value;
			mGroundStatTable.SetTileArmorSpec(mGroundSpec.mArmorSpec, mDirectionY); // 지형 장갑 방향도 같이 갱신
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 방향각
	public int aSkillDirectionY
	{
		get { return mSkillDirectionY; }
		set { mSkillDirectionY = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 타일을 점유하고 있는 부대 리스트
	public LinkedList<BattleTroop> aOccupyingTroopList
	{
		get { return mOccupyingTroopList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 가장 최근의 스킬 효과 번호
	public uint aLastSkillEffectGuid
	{
		get { return mLastSkillEffectGuid; }
		set { mLastSkillEffectGuid = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("BattleGridTile<{0},{1}>", mTileIndex.row, mTileIndex.col);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleGridTile(BattleGrid pBattleGrid, int lTileRowIndex, int lTileColIndex)
	{
		mBattleGrid = pBattleGrid;
		mTileIndex = new RowCol(lTileRowIndex, lTileColIndex);
		mCenterCoord = _GetCenterCoord();
		mGroundStatTable = new GroundStatTable();
		mOccupyingTroopList = new LinkedList<BattleTroop>();

		mLastSkillEffectGuid = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 타입을 얻습니다.
	public static BattleGridTileType ParseTileType(String pTileToken)
	{
		for (int iToken = 0; iToken < cTileTypeCount; ++iToken)
			if (String.Compare(pTileToken, ((BattleGridTileType)iToken).ToString()) == 0)
				return (BattleGridTileType)iToken;
		return BattleGridTileType.Block;
	}
	//-------------------------------------------------------------------------------------------------------
	// 속성 타입을 얻습니다.
	public static BattleGridTileAttributeType ParseAttributeType(String pAttributeToken)
	{
		for (int iToken = 0; iToken < cAttributeTypeCount; ++iToken)
			if (String.Compare(pAttributeToken, ((BattleGridTileAttributeType)iToken).ToString()) == 0)
				return (BattleGridTileAttributeType)iToken;
		return BattleGridTileAttributeType.None;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 타입을 지정합니다.
	public void SetTileType(BattleGridTileType pTileType)
	{
		mTileType = pTileType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 스펙을 초기화합니다.
	public void InitGroundSpec(GroundSpec pGroundSpec)
	{
		mGroundSpec = pGroundSpec;

		mGroundStatTable.Copy(pGroundSpec.mStatTable);

		mGroundStatTable.SetTileArmorSpec(pGroundSpec.mArmorSpec, mDirectionY); // 지형 장갑으로 초기화
		mGroundStatTable.SetSkillArmorSpec(null, 0); // 효과 장갑은 리셋 초기화

		if (mSkillEffectChain != null)
			mSkillEffectChain.Enumerate(_OnModifyGroundStatTable);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 지점을 연결합니다.
	public void SetLinkEventPoint(BattleEventPoint pEventPoint)
	{
		mLinkEventPoint = pEventPoint;

		CallTileHandler(mBattleGrid.aEventCheckTileDistance, _OnFindEventCheckTile);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 타일 위에 있는 부대들에게 지형 스탯 변경을 반영합니다.
	public void UpdateGroundStat()
	{
		foreach (BattleTroop iOccupationTroop in mOccupyingTroopList)
			iOccupationTroop.aStat.UpdateStat();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 타일로 들어오려는 이동 벡터를 타일 경계에서 잘라내고 그 수정된 도착점을 반환합니다.
	public Coord2 CutInTileMoveVector(Coord2 pMoveStartCoord, Coord2 pMoveEndCoord)
	{
		Coord2 lTileMinCoord = new Coord2(mCenterCoord.x - mBattleGrid.aTileBlockScaleX, mCenterCoord.z - mBattleGrid.aTileBlockScaleZ);
		Coord2 lTileMaxCoord = new Coord2(mCenterCoord.x + mBattleGrid.aTileBlockScaleX, mCenterCoord.z + mBattleGrid.aTileBlockScaleZ);

		if ((pMoveEndCoord.x < lTileMinCoord.x) ||
			(pMoveEndCoord.x >= lTileMaxCoord.x) ||
			(pMoveEndCoord.z < lTileMinCoord.z) ||
			(pMoveEndCoord.z >= lTileMaxCoord.z))
			return pMoveEndCoord; // 타일 밖으로 가는 벡터는 통과합니다.

		Coord2 lCutEndCoord = pMoveEndCoord;
		if (pMoveStartCoord.x < lTileMinCoord.x)
			lCutEndCoord.x = lTileMinCoord.x - 1;
		else if (pMoveStartCoord.x >= lTileMaxCoord.x)
			lCutEndCoord.x = lTileMaxCoord.x;
		if (pMoveStartCoord.z < lTileMinCoord.z)
			lCutEndCoord.z = lTileMinCoord.z - 1;
		else if (pMoveStartCoord.z >= lTileMaxCoord.z)
			lCutEndCoord.z = lTileMaxCoord.z;
		return lCutEndCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과를 추가합니다.
	public GuidObjectChainItem<BattleGridSkillEffect> AddSkillEffect(BattleGridSkillEffect pSkillEffect)
	{
		// 격자 타일은 너무 많기 때문에 타일들이 스킬 효과 체인을 모두 가지고 있기 보다는,
		// 체인을 필요한 경우에만 생성하고 체인이 비면 재활용 풀로 돌려보내는 정책을 사용합니다.

		// 체인이 아직 없다면 생성합니다.
		if (mSkillEffectChain == null)
			mSkillEffectChain = mBattleGrid.aBattleLogic.CreateGridSkillEffectChain();

		// 체인에 스킬 효과를 넣습니다.
		GuidObjectChainItem<BattleGridSkillEffect> lSkillEffectChainItem = mSkillEffectChain.AddLast(pSkillEffect);

		// 스탯 변경을 적용합니다.
		_ModifySkillEffectStat(pSkillEffect);
		if ((pSkillEffect.aGroundEffectSpec != null) &&
			(pSkillEffect.aGroundEffectSpec.mGroundArmorSpec != null))
			mGroundStatTable.SetSkillArmorSpec(pSkillEffect.aGroundEffectSpec.mGroundArmorSpec, pSkillEffect.aDirectionY);
		UpdateGroundStat();

		return lSkillEffectChainItem;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과를 제거합니다.
	public void RemoveSkillEffect(GuidObjectChainItem<BattleGridSkillEffect> pSkillEffectChainItem)
	{
		BattleGridSkillEffect lSkillEffect = pSkillEffectChainItem.Value;
		if (lSkillEffect.aEffectGridTile != this)
		{
			Debug.LogError("incorrect skill effect for this BattleGridTile - " + this);
			return; // 다른 타일의 것이 들어옴
		}

		// 체인 아이템을 비웁니다.
		pSkillEffectChainItem.Empty();

		// 체인이 비었다면 체인 자체를 파괴합니다(재활용 풀로 돌려보냅니다).
		if (!mSkillEffectChain.CheckValidItem())
		{
			mBattleGrid.aBattleLogic.DestroyGridSkillEffectChain(mSkillEffectChain);
			mSkillEffectChain = null;
		}

		// 스탯 변경을 적용합니다.
		_ModifySkillEffectStat(lSkillEffect);
		if ((lSkillEffect.aGroundEffectSpec != null) &&
			(lSkillEffect.aGroundEffectSpec.mGroundArmorSpec == mGroundStatTable.aSkillArmorSpec))
			mGroundStatTable.SetSkillArmorSpec(null, mDirectionY);
		UpdateGroundStat();
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 핸들러를 호출합니다.
	public void CallTileHandler(int pCheckTileDistance, OnHandleTileDelegate pOnHandleTileDelegate)
	{
		pOnHandleTileDelegate(this);

		if (pCheckTileDistance >= 1)
		{
			if ((mTileIndex.col & 0x01) == 0)
				_CallTileHandler(BattleGrid.sEvenColNearOffsetsInDistanceOne, pOnHandleTileDelegate);
			else
				_CallTileHandler(BattleGrid.sOddColNearOffsetsInDistanceOne, pOnHandleTileDelegate);

			if (pCheckTileDistance >= 2)
			{
				if ((mTileIndex.col & 0x01) == 0)
					_CallTileHandler(BattleGrid.sEvenColNearOffsetsInDistanceTwo, pOnHandleTileDelegate);
				else
					_CallTileHandler(BattleGrid.sOddColNearOffsetsInDistanceTwo, pOnHandleTileDelegate);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 주변에 비어있는 엄폐 가능 타일을 찾습니다.
	public BattleGridTile FindSurroundingHidingTile(int pCheckTileDistance, int pHidingDirectionY)
	{
		mFoundHidingTile = null;
		mFoundArmorAdd = 0;
		mHidingDirectionY = pHidingDirectionY;
		CallTileHandler(pCheckTileDistance, _OnFindSurroundingHidingTile);
		return mFoundHidingTile;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대가 이 타일을 점유하도록 합니다.
	public void AddOccupation(BattleTroop pTroop)
	{
		if (pTroop.aOccupationNode.List == mOccupyingTroopList)
			return;

		mOccupyingTroopList.AddLast(pTroop.aOccupationNode);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대가 이 타일을 점유하던 것을 해제합니다.
	public void RemoveOccupation(BattleTroop pTroop)
	{
		if (pTroop.aOccupationNode.List != mOccupyingTroopList)
			return;

		mOccupyingTroopList.Remove(pTroop.aOccupationNode);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 중심 좌표를 구합니다.
	private Coord2 _GetCenterCoord()
	{
		int lTileCoordX = mTileIndex.col * mBattleGrid.aTileScaleX + mBattleGrid.aTileBlockScaleX;
		int lTileCoordZ;
		if ((mTileIndex.col & 0x01) == 0) // 짝수열이라면
			lTileCoordZ = mTileIndex.row * mBattleGrid.aTileScaleZ + mBattleGrid.aTileBlockScaleZ;
		else // 홀수열이라면
			lTileCoordZ = mTileIndex.row * mBattleGrid.aTileScaleZ;

		return new Coord2(lTileCoordX, lTileCoordZ);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifyGroundStatTable(BattleGridSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aGroundStatTableModifier != null)
			pSkillEffect.aGroundStatTableModifier.ModifyStatTable(mGroundStatTable);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경을 적용합니다.
	private void _ModifySkillEffectStat(BattleGridSkillEffect pApplySkillEffect)
	{
		StatTableModifier<GroundStat> lApplyStatTableModifier = pApplySkillEffect.aGroundStatTableModifier;
		if (pApplySkillEffect.aGroundStatTableModifier != null)
		{
			for (int iStat = 0; iStat < lApplyStatTableModifier.aModifyStatCount; ++iStat)
			{
				mModifyGroundStatIdx = lApplyStatTableModifier.aModifyStatIdxs[iStat];
				mGroundStatTable.Set(mModifyGroundStatIdx, mGroundSpec.mStatTable.Get(mModifyGroundStatIdx)); // 초기화

				if (mSkillEffectChain != null)
					mSkillEffectChain.Enumerate(_OnModifySkillEffectGroundStat);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifySkillEffectGroundStat(BattleGridSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aGroundStatTableModifier != null)
			pSkillEffect.aGroundStatTableModifier.ModifyStat(mGroundStatTable, mModifyGroundStatIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 핸들러를 호출합니다.
	private void _CallTileHandler(int[,] pTileOffsets, OnHandleTileDelegate pOnHandleTileDelegate)
	{
		int lTileCount = pTileOffsets.GetLength(0);
		for (int iTile = 0; iTile < lTileCount; ++iTile)
		{
			int lTileRowIndex = mTileIndex.row + pTileOffsets[iTile, 0];
			if ((lTileRowIndex < 0) ||
				(lTileRowIndex >= mBattleGrid.aTileRowCount))
				continue;
			int lTileColIndex = mTileIndex.col + pTileOffsets[iTile, 1];
			if ((lTileColIndex < 0) ||
				(lTileColIndex >= mBattleGrid.aTileColCount))
				continue;

			pOnHandleTileDelegate(mBattleGrid.aTiles[lTileRowIndex, lTileColIndex]);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnHandleTileDelegate
	private void _OnFindEventCheckTile(BattleGridTile pTile)
	{
		pTile.mLinkEventPoint = mLinkEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnHandleTileDelegate
	private void _OnFindSurroundingHidingTile(BattleGridTile pTile)
	{
		if (!pTile.aGroundStatTable.aIsArmorSpec)
			return; // 엄폐 효과 없음
		if (pTile.mOccupyingTroopList.Count > 0)
			return; // 다른 부대 있음

		int lArmorAdd = 0;
		if (mGroundStatTable.aTileArmorSpec != null) // 지형 자체의 장갑을 반영
		{
			int lTileArmorDegreeVector = MathUtil.DegreeVector(mHidingDirectionY, mGroundStatTable.aTileArmorDirectionY);
			if (Mathf.Abs(lTileArmorDegreeVector) <= mGroundStatTable.aTileArmorEffectAngleHalf)
				lArmorAdd = mGroundStatTable.aTileArmorSpec.mArmorAdd; // 장갑 보정은 합
		}
		if (mGroundStatTable.aSkillArmorSpec != null) // 스킬에 의한 장갑을 반영
		{
			int lSkillArmorDegreeVector = MathUtil.DegreeVector(mHidingDirectionY, mGroundStatTable.aSkillArmorDirectionY);
			if (Mathf.Abs(lSkillArmorDegreeVector) <= mGroundStatTable.aSkillArmorEffectAngleHalf)
				lArmorAdd = mGroundStatTable.aSkillArmorSpec.mArmorAdd; // 장갑 보정은 합
		}
		if (lArmorAdd <= 0)
			return; // 엄폐 효과 없음

		if ((mFoundHidingTile != null) &&
			(mFoundArmorAdd < lArmorAdd))
			return; // 엄폐 효과가 더 큰 타일을 이미 찾았음

		mFoundHidingTile = pTile; // 엄폐 타일 찾음
		mFoundArmorAdd = lArmorAdd;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGrid mBattleGrid;
	private RowCol mTileIndex;
	private Coord2 mCenterCoord;
	private BattleGridTileType mTileType;
	private GroundSpec mGroundSpec;
	private GroundStatTable mGroundStatTable;
	private BattleEventPoint mLinkEventPoint;
	private int mMapWeight;
	private int mDirectionY;
	private int mSkillDirectionY;
	private LinkedList<BattleTroop> mOccupyingTroopList;

	private GuidObjectChain<BattleGridSkillEffect> mSkillEffectChain;
	private int mModifyGroundStatIdx;
	private uint mLastSkillEffectGuid;

	private BattleGridTile mFoundHidingTile;
	private int mFoundArmorAdd;
	private int mHidingDirectionY;
}
