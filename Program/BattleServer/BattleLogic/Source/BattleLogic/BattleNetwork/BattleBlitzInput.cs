using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleBlitzInput
{
	public int mClientId;
	public int mBlitzTroopId;
	public Coord2 mPointingCoord;
	public int mGaugeCount;
	public int mTargetTroopCount;
	public uint[] mTargetTroopGuids;

	public bool aIsValid
	{
		get { return (mBlitzTroopId > 0); }
	}

	public BattleBlitzInput()
	{
		mTargetTroopGuids = new uint[BattleProtocol.cTargetTroopMaxCount];
		mBlitzTroopId = 0;
	}

	public void Reset()
	{
		mBlitzTroopId = 0;
	}

	public void Set(int pClientId, int pBlitzTroopId, Coord2 pPointingCoord, int pGaugeCount, int pTargetTroopCount, GuidLink<BattleTroop>[] pTargetTroopLinks)
	{
		mClientId = pClientId;
		mBlitzTroopId = pBlitzTroopId;
		mPointingCoord = pPointingCoord;
		mGaugeCount = pGaugeCount;
		mTargetTroopCount = pTargetTroopCount;
		for (int iTroop = 0; iTroop < pTargetTroopCount; ++iTroop)
		{
			BattleTroop lTargetTroop = pTargetTroopLinks[iTroop].Get();
			mTargetTroopGuids[iTroop] = (lTargetTroop != null) ? lTargetTroop.aGuid : 0;
		}
	}
	
	public static void Copy(BattleBlitzInput pFrom, BattleBlitzInput pTo)
	{
		if (!pFrom.aIsValid)
			pTo.Reset();

		pTo.mClientId = pFrom.mClientId;
		pTo.mBlitzTroopId = pFrom.mBlitzTroopId;
		pTo.mPointingCoord = pFrom.mPointingCoord;
		pTo.mGaugeCount = pFrom.mGaugeCount;
		pTo.mTargetTroopCount = pFrom.mTargetTroopCount;
		for (int iTroop = 0; iTroop < pFrom.mTargetTroopCount; ++iTroop)
			pTo.mTargetTroopGuids[iTroop] = pFrom.mTargetTroopGuids[iTroop];
	}
	public override string ToString()
	{
		if (mBlitzTroopId > 0)
			return String.Format("BlitzInput<mBlitzTroopId:{0} mPointingCoord:{1} mGaugeCount:{2} mTargetTroopCount:{3}>", mBlitzTroopId, mPointingCoord, mGaugeCount, mTargetTroopCount);
		else
			return "BlitzInput<None>";
	}
}
