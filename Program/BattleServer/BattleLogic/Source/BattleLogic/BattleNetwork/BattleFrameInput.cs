using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public enum BattleFrameInputType
{
	SpawnCommander,
	SpawnCommanderAndAttackTroop,
	SpawnCommanderAndGrabCheckPoint,
	MoveTroops,
	AttackTroop,
	GrabCheckPoint,
	RetreatCommander,
	CommanderSkill,
	GoodGame,
	ForceAi,

	DebugDestroyCommander,
	CheatWin,
	CheatLose,
}

public class BattleFrameInput : IComparable<BattleFrameInput>
{
	public int mTickNo;

	// 입력 값
	public int mType;
	public int mInputSerialNo;
	public int mX;
	public int mY;
	public int mZ;
	public int mA;
	public int mB;
	public int mC;

	// 입력을 보낸 클라이언트 정보
	public uint mClientGuid;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에서 생성합니다.
	public static BattleFrameInput Create(int pTickNo)
	{
		BattleFrameInput lFrameInput;
		if (sPoolingQueue.Count <= 0)
			lFrameInput = new BattleFrameInput();
		else
			lFrameInput = sPoolingQueue.Dequeue();

		lFrameInput.mTickNo = pTickNo;

		return lFrameInput;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에서 생성합니다.
	public static BattleFrameInput Create(BattleFrameInput pCloneInput)
	{
		BattleFrameInput lFrameInput = BattleFrameInput.Create(pCloneInput.mTickNo);
		BattleFrameInput.Copy(pCloneInput, lFrameInput);

		return lFrameInput;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에서 생성합니다.
	public static BattleFrameInput Create(String pFrameInputJsonString)
	{
		int lInputFieldStringOffset = pFrameInputJsonString.IndexOf(":");
		if (lInputFieldStringOffset < 0)
		{
			Debug.LogError("missing input token ':'");
			return null;
		}
		String lTickNoString = pFrameInputJsonString.Substring(0, lInputFieldStringOffset); // ':' 전까지
		String lInputFieldString = pFrameInputJsonString.Substring(lInputFieldStringOffset + 1); // ':' 다음부터(+1) 끝까지

		int lTickNo = int.Parse(lTickNoString);

		BattleFrameInput lFrameInput;
		if (sPoolingQueue.Count <= 0)
			lFrameInput = new BattleFrameInput();
		else
			lFrameInput = sPoolingQueue.Dequeue();

		lFrameInput.mTickNo = lTickNo;
		lFrameInput.FromJsonString(lInputFieldString);

		return lFrameInput;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에 돌려놓습니다.
	public void Destroy()
	{
		sPoolingQueue.Enqueue(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("FrameInput<mTickNo:{0} mType:{1} mInputSerialNo:{2} mX:{3} mY:{4} mZ:{5} mA:{6} mB:{7} mC:{8} mClientGuid:{9}>", mTickNo, mType, mInputSerialNo, mX, mY, mZ, mA, mB, mC, mClientGuid);
	}
	//-------------------------------------------------------------------------------------------------------
	// IComparable 메서드
	public int CompareTo(BattleFrameInput pOther)
	{
		// mTickNo -> mClientGuid -> mInputSerialNo 순서로 정렬합니다.
		if (mTickNo < pOther.mTickNo)
			return -1;
		else if (mTickNo > pOther.mTickNo)
			return 1;
		else
		{
			if (mClientGuid < pOther.mClientGuid)
				return -1;
			else if (mClientGuid > pOther.mClientGuid)
				return 1;
			else
			{
				if (mInputSerialNo < pOther.mInputSerialNo)
					return -1;
				else if (mInputSerialNo > pOther.mInputSerialNo)
					return 1;
				else
					return 0;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 복사합니다.
	public static void Copy(BattleFrameInput pFrom, BattleFrameInput pTo)
	{
		pTo.mType = pFrom.mType;
		pTo.mInputSerialNo = pFrom.mInputSerialNo;
		pTo.mX = pFrom.mX;
		pTo.mY = pFrom.mY;
		pTo.mZ = pFrom.mZ;
		pTo.mA = pFrom.mA;
		pTo.mB = pFrom.mB;
		pTo.mC = pFrom.mC;

		pTo.mClientGuid = pFrom.mClientGuid;
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 문자열을 만듭니다.
	public void AppendVerifyDataJsonString(StringBuilder pStringBuilder)
	{
		pStringBuilder.Append(mTickNo);
		pStringBuilder.Append(":");
		AppendInputFieldJsonString(pStringBuilder);
		AppendClientGuidJsonString(pStringBuilder);
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력 값 부분에 대한 JSON 문자열을 만듭니다.
	public void AppendInputFieldJsonString(StringBuilder pStringBuilder)
	{
		pStringBuilder.AppendFormat("{0}/{1}/{2}/{3}/{4}/{5}/{6}/{7}",
			mType,
			mInputSerialNo,
			mX,
			mY,
			mZ,
			mA,
			mB,
			mC);
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 보낸 클라이언트 정보에 대한 JSON 문자열을 만듭니다.
	public void AppendClientGuidJsonString(StringBuilder pStringBuilder)
	{
		pStringBuilder.Append("/" + mClientGuid);
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 문자열로 초기화합니다.
	public void FromJsonString(String pJsonString)
	{
		String[] lInputTokens = pJsonString.Split('/');
		if (lInputTokens.Length < 9)
		{
			Debug.LogError("missing input token:" + pJsonString);
			return;
		}

		mType = Convert.ToInt32(lInputTokens[0]);
		mInputSerialNo = Convert.ToInt32(lInputTokens[1]);
		mX = Convert.ToInt32(lInputTokens[2]);
		mY = Convert.ToInt32(lInputTokens[3]);
		mZ = Convert.ToInt32(lInputTokens[4]);
		mA = Convert.ToInt32(lInputTokens[5]);
		mB = Convert.ToInt32(lInputTokens[6]);
		mC = Convert.ToInt32(lInputTokens[7]);

		mClientGuid = Convert.ToUInt32(lInputTokens[8]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력 값 부분 JSON 문자열로 초기화합니다.
	public void FromInputFieldJsonString(String pJsonString)
	{
		String[] lInputTokens = pJsonString.Split('/');
		if (lInputTokens.Length < 8) // 마지막에 있는 mClientGuid 제외
		{
			Debug.LogError("missing input token:" + pJsonString);
			return;
		}

		mType = Convert.ToInt32(lInputTokens[0]);
		mInputSerialNo = Convert.ToInt32(lInputTokens[1]);
		mX = Convert.ToInt32(lInputTokens[2]);
		mY = Convert.ToInt32(lInputTokens[3]);
		mZ = Convert.ToInt32(lInputTokens[4]);
		mA = Convert.ToInt32(lInputTokens[5]);
		mB = Convert.ToInt32(lInputTokens[6]);
		mC = Convert.ToInt32(lInputTokens[7]);

		mClientGuid = 0; // 0으로 초기화
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	internal BattleFrameInput()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	internal static Queue<BattleFrameInput> sPoolingQueue = new Queue<BattleFrameInput>();
}
