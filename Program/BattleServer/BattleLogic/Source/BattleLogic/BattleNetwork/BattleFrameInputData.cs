using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleFrameInputData
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 틱 번호
	public int aTickNo
	{
		get { return mTickNo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력 리스트
	public List<BattleFrameInput> aFrameInputList
	{
		get { return mFrameInputList; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	protected BattleFrameInputData()
	{
		mFrameInputList = new List<BattleFrameInput>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다.
	protected void OnCreate(int pTickNo)
	{
		mTickNo = pTickNo;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴할 때 호출됩니다.
	protected void OnDestroy()
	{
		for (int iInput = 0; iInput < mFrameInputList.Count; ++iInput)
		{
			mFrameInputList[iInput].Destroy();
			mFrameInputList[iInput] = null;
		}
		mFrameInputList.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 복사합니다.
	public static void Copy(BattleFrameInputData pFrom, BattleFrameInputData pTo, int pToTickNo)
	{
		pTo.mTickNo = pToTickNo;

		for (int iInput = 0; iInput < pFrom.mFrameInputList.Count; ++iInput)
		{
			BattleFrameInput lFrameInput = BattleFrameInput.Create(pTo.mTickNo);
			BattleFrameInput.Copy(pFrom.mFrameInputList[iInput], lFrameInput);
			pTo.mFrameInputList.Add(lFrameInput);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 추가합니다.
	public void AddFrameInput(BattleFrameInput pInput)
	{
		BattleFrameInput lFrameInput = BattleFrameInput.Create(pInput);
		if (pInput.mTickNo != mTickNo)
			Debug.LogError("invalid pInput.mTickNo:" + pInput.mTickNo);
		mFrameInputList.Add(lFrameInput);
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 추가합니다.
	public void AddFrameInput(int pOverwriteInputIdx, uint pClientGuid, int pInputType, int pInputX, int pInputY, int pInputZ, int pInputA, int pInputB, int pInputC)
	{
		BattleFrameInput lFrameInput = BattleFrameInput.Create(mTickNo);

		lFrameInput.mType = pInputType;
		lFrameInput.mInputSerialNo = mFrameInputList.Count;
		lFrameInput.mX = pInputX;
		lFrameInput.mY = pInputY;
		lFrameInput.mZ = pInputZ;
		lFrameInput.mA = pInputA;
		lFrameInput.mB = pInputB;
		lFrameInput.mC = pInputC;

		lFrameInput.mClientGuid = pClientGuid;

		mFrameInputList.Add(lFrameInput);
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 입력 일련 번호가 작은 순서대로 정렬합니다.
	public void SortFrameInputSerialNo()
	{
		if (mFrameInputList.Count <= 1)
			return;

		for (int iInput = 0; iInput < mFrameInputList.Count; ++iInput)
		{
			int lMinInputSerialNo = mFrameInputList[iInput].mInputSerialNo;
			int lSwapIdx = -1;

			for (int iCheck = (iInput + 1); iCheck < mFrameInputList.Count; ++iCheck)
			{
				if (lMinInputSerialNo > mFrameInputList[iCheck].mInputSerialNo)
				{
					lMinInputSerialNo = mFrameInputList[iCheck].mInputSerialNo;
					lSwapIdx = iCheck;
				}
			}

			if (lSwapIdx >= 0)
			{
				BattleFrameInput lTempInput = mFrameInputList[lSwapIdx];
				mFrameInputList[iInput] = mFrameInputList[lSwapIdx];
				mFrameInputList[lSwapIdx] = lTempInput;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mTickNo;
	private List<BattleFrameInput> mFrameInputList;
}
