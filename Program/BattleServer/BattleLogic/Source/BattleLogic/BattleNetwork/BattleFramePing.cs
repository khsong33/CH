using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleFramePing : BattleFrameInputData
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 프레임 핑을 팩토리에서 생성합니다.
	public static BattleFramePing Create(int pTickNo)
	{
		BattleFramePing lFramePing;
		if (sPoolingQueue.Count <= 0)
			lFramePing = new BattleFramePing();
		else
			lFramePing = sPoolingQueue.Dequeue();

		lFramePing.OnCreate(pTickNo);

		return lFramePing;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 핑을 팩토리에 돌려놓습니다.
	public void Destroy()
	{
		OnDestroy();

		sPoolingQueue.Enqueue(this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	private BattleFramePing()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Queue<BattleFramePing> sPoolingQueue = new Queue<BattleFramePing>();
}
