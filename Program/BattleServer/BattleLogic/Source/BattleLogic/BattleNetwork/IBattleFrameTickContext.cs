﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IBattleFrameTickContext
{
	BattleMatchInfo aMatchInfo { get; }
	BattleGrid aBattleGrid { get; }
	BattleProgressInfo aProgressInfo { get; }
	BattleUser[] aUsers { get; }

	void OnFrameInputSpawnCommander(BattleCommander pSpawnCommander, int pLastTroopCount);
	void OnFrameInputGoodGame(BattleUser pInputUser);
}
