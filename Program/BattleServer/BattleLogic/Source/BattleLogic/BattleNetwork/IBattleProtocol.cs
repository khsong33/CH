using UnityEngine;
using System.Collections;
using System;

public interface IBattleProtocol
{
	// [SendPacket] 클라가 서버에 전투 방 입장을 요청합니다.
	void SendPacket_EnterBattleRoom();

	// [OnReceivePacket] 서버는 전투 방에 입장했음을 알립니다.
	BattleProtocol.OnBattleRoomEnteredDelegate aOnBattleRoomEnteredDelegate { get; set; }

	// [SendPacket] 클라가 대전 정보를 바탕으로 전투맵을 로딩한 다음에 서버에 플레이 준비가 끝났음을 알립니다.
	void SendPacket_ReadyToPlay();

	// [OnReceivePacket] 서버는 대전에 참가할 클라들이 모두 준비가 되었다면 전투 시작을 알립니다.
	BattleProtocol.OnBattleStartedDelegate aOnBattleStartedDelegate { get; set; }

	// 해당 프레임의 틱 입력을 얻습니다. 아직 틱을 받지 않았다면 null을 반환합니다.
	BattleFrameTick PopFrameTick(int pTickNo);

	// 틱을 받았을 때 호출됩니다.
	BattleProtocol.OnFrameTickDelegate aOnFrameTickDelegate { get; set; }

	// [SendPacket] 클라가 매 프레임마다 해당 프레임을 처리할 준비가 되었음을 알리는 핑을 보냅니다. 핑에는 유저 입력이 추가로 들어있을 수 있습니다.
	void SendPacket_FramePing(BattleFramePing pBattleFramePing);

	// [SendPacket] 클라가 전투가 끝나면 전투가 끝났음을 서버에 알립니다.
	void SendPacket_BattleEnd(BattleProtocol.BattleEndCode pBattleEndCode);

	// [OnReceivePacket] 전투를 종료합니다.
	BattleProtocol.OnBattleEndedDelegate aOnBattleEndedDelegate { get; set; }
}
