using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleBarricade : BattleObject
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 인덱스
	public int aBarricadeIdx
	{
		get { return mBarricadeIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// GUID
	public uint aBarricadeGuid
	{
		get { return mBarricadeGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 대상 병종
	public TroopType aCoveringTroopType
	{
		get { return mCoveringTroopType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방어 타입
	public TroopDefenseType aTroopDefenseType
	{
		get { return mTroopDefenseType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드 진입 반경의 제곱
	public int aSqrEnterRadius
	{
		get { return mSqrEnterRadius; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageBarricade aStageBarricade
	{
		get { return mStageBarricade; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageBarricade; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주둔 부대
	public BattleTroop aCoveringTroop
	{
		get { return mCoveringTroop; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("{0}{1}<{3},{4}>(aTeamIdx:{4})", mCoveringTroopType, mTroopDefenseType, aGridCell.aCellIndex.row, aGridCell.aCellIndex.col, aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleBarricade(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateBarricade(
		BattleGridTile pGridTile,
		int pBarricadeIdx,
		uint pBarricadeGuid,
		TroopType pCoveringTroopType,
		TroopDefenseType pTroopDefenseType,
		int pEnterRadius)
	{
		OnCreateObject(
			pGridTile.aBattleGrid,
			false, // bool pIsCheckDetection
			false, // bool pIsDetecting
			pGridTile.aBattleGrid.aIsNoneDetectables, // bool pIsDetectables
			pGridTile.aCenterCoord,
			0, // int pDirectionY
			0); // int pTeamIdx

		mBarricadeIdx = pBarricadeIdx;
		mBarricadeGuid = pBarricadeGuid;

		mCoveringTroopType = pCoveringTroopType;
		mTroopDefenseType = pTroopDefenseType;
		mSqrEnterRadius = pEnterRadius * pEnterRadius;

		mStageBarricade = aBattleGrid.aStage.CreateBarricade(this);

		mCoveringTroop = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지에서 제거합니다.
		mStageBarricade.DestroyObject();
		mStageBarricade = null;

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
//		aBattleLogic.DestroyBarricade(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 주둔 부대를 지정합니다.
	public void SetCoveringTroop(BattleTroop pCoveringTroop)
	{
		if (mCoveringTroop != null)
			mCoveringTroop.ResetDefenseType();
		mCoveringTroop = pCoveringTroop;
		if (mCoveringTroop != null)
			mCoveringTroop.SetDefenseType(mTroopDefenseType);

		UpdateTeam((pCoveringTroop != null) ? pCoveringTroop.aTeamIdx : -1);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지점이 바리케이드 주둔 영역 안인지를 검사합니다.
	public bool CheckInEnterRadius(Coord2 pCoord)
	{
		Coord2 lEnterVector = pCoord - aCoord;
		return (lEnterVector.SqrMagnitude() <= mSqrEnterRadius);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mBarricadeIdx;
	private uint mBarricadeGuid;

	private TroopType mCoveringTroopType;
	private TroopDefenseType mTroopDefenseType;
	private int mSqrEnterRadius;

	private IStageBarricade mStageBarricade;

	private BattleTroop mCoveringTroop;
}
