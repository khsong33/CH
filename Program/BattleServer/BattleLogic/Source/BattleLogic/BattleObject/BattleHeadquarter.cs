using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleHeadquarter : BattleObject
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 인덱스
	public int aHeadquarterIdx
	{
		get { return mHeadquarterIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// GUID
	public uint aHeadquarterGuid
	{
		get { return mHeadquarterGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 본부에서 스폰하면 나오는 부대의 시작 좌표
	public Coord2 aSpawnCoord
	{
		get { return mSpawnCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageHeadquarter aStageHeadquarter
	{
		get { return mStageHeadquarter; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageHeadquarter; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 연결 유저
	public BattleUser aLinkUser
	{
		get { return mLinkUser; }
		set
		{
			if (mLinkUser == value)
				return;

			mLinkUser = value;
			if (mLinkUser != null)
				UpdateTeam(mLinkUser.aTeamIdx); // 연결 유저의 팀으로 자신의 팀을 변경
			else
				UpdateTeam(0);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 연결 거점
	public BattleCheckPoint aLinkCheckPoint
	{
		get { return mLinkCheckPoint; }
		set
		{
			if (mLinkCheckPoint == value)
				return;

			mLinkCheckPoint = value;
			if (mLinkCheckPoint != null)
				mLinkCheckPoint.aLinkHeadquarter = this; // 서로 연결
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("HQ{0}<{1},{2}>(aGuid:{3} aTeamIdx:{4})", aTeamIdx, aGridCell.aCellIndex.row, aGridCell.aCellIndex.col, aGuid, aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleHeadquarter(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateHeadquarter(
		BattleGridTile pGridTile,
		int pHeadquarterIdx,
		uint pHeadquarterGuid,
		Coord2 pSpawnCoord)
	{
		OnCreateObject(
			pGridTile.aBattleGrid,
			false, // bool pIsCheckDetection
			false, // bool pIsDetecting
			pGridTile.aBattleGrid.aIsNoneDetectables, // bool pIsDetectables
			pGridTile.aCenterCoord,
			pGridTile.aDirectionY,
			0); // int pTeamIdx

		mHeadquarterIdx = pHeadquarterIdx;
		mHeadquarterGuid = pHeadquarterGuid;
		mSpawnCoord = pSpawnCoord;
		//Debug.Log("mSpawnCoord=" + mSpawnCoord);

		mStageHeadquarter = aBattleGrid.aStage.CreateHeadquarter(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지에서 제거합니다.
		mStageHeadquarter.DestroyObject();
		mStageHeadquarter = null;

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyHeadquarter(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateTeam(int pTeamIdx)
	{
		if (aTeamIdx == pTeamIdx)
			return;

		base.UpdateTeam(pTeamIdx);

		if (mLinkCheckPoint != null)
			mLinkCheckPoint.UpdateTeam(aTeamIdx); // 자신의 팀으로 본부 거점의 팀을 변경
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mHeadquarterIdx;
	private uint mHeadquarterGuid;
	private Coord2 mSpawnCoord;

	private IStageHeadquarter mStageHeadquarter;

	private BattleUser mLinkUser;
	private BattleCheckPoint mLinkCheckPoint;
}
