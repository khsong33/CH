using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class BattleObject : FrameObject
{
	public const int cDirectionVectorSize = 1000;

	internal const int cNonDetectionTime = int.MaxValue;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 다른 객체를 탐지 가능 여부
	public virtual bool aIsDetecting
	{
		get { return mIsDetecting; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 객체의 탐지 가능 여부
	public bool[] aIsDetectables
	{
		get { return mIsDetectables; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 탐지 여부
	public bool[] aIsDetecteds
	{
		get { return mIsDetecteds; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표
	public Coord2 aCoord
	{
		get { return mCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각
	public int aDirectionY
	{
		get { return mDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀 인덱스
	public int aTeamIdx
	{
		get { return mTeamIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태 문자열
	public String aStateText
	{
		get { return mStateText; }
		set
		{
			mStateText = value;
			aStageObject.aStateText = value;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// HP 문자열
	public String aHpText
	{
		get { return mHpText; }
		set
		{
			mHpText = value;
			aStageObject.aHpText = value;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 타일
	public BattleGridTile aGridTile
	{
		get { return mGridTile; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 셀
	public BattleGridCell aGridCell
	{
		get { return mGridCell; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 체인 아이템
	public GuidObjectChainItem<BattleObject> aChainItem
	{
		get { return mChainItem; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 오버랩 할 수 있는 오브젝트
	public BattleObject aOverlapObject
	{
		get { return mOverlapObject; }
		set { mOverlapObject = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 은폐 여부
	public virtual bool aIsHiding
	{
		get { return false; }
		set { }
	}
	//-------------------------------------------------------------------------------------------------------
	// 적을 탐지하는 거리
	public virtual int aDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 적을 탐지하는 거리(제곱)
	public virtual int aSqrDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 엄폐된 대상을 탐지하는 거리
	public virtual int aHidingDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 엄폐된 대상을 탐지하는 거리(제곱)
	public virtual int aSqrHidingDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 오브젝트 인터페이스
	public abstract IStageObject aStageObject { get; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleObject(BattleLogic pBattleLogic)
	{
		mBattleLogic = pBattleLogic;

		mIsDetectables = new bool[BattleMatchInfo.cMaxTeamCount];
		mIsDetecteds = new bool[BattleMatchInfo.cMaxTeamCount];
		mDetectionChangeTimes = new int[BattleMatchInfo.cMaxTeamCount];
		mDetectingObjectLinks = new GuidLink<BattleObject>[BattleMatchInfo.cMaxTeamCount];
		for (int iLink = 0; iLink < BattleMatchInfo.cMaxTeamCount; ++iLink)
			mDetectingObjectLinks[iLink] = new GuidLink<BattleObject>();
		mIsToDetecteds = new bool[BattleMatchInfo.cMaxTeamCount];
		mUndetectedCheckTimers = new int[BattleMatchInfo.cMaxTeamCount];
		mIsUpdateUndetecteds = new bool[BattleMatchInfo.cMaxTeamCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateObject(
		BattleGrid pBattleGrid,
		bool pIsCheckDetection,
		bool pIsDetecting,
		bool[] pIsDetectables,
		Coord2 pCoord,
		int pDirectionY,
		int pTeamIdx)
	{
		//Debug.Log("OnCreate() - " + this);

		base.OnCreateObject(pBattleGrid.aFrameUpdater);

		mBattleGrid = pBattleGrid;

		mIsCheckDetection = pIsCheckDetection;
		mIsDetecting = mIsCheckDetection && pIsDetecting;
		for (int iTeam = 0; iTeam < pBattleGrid.aTeamCount; ++iTeam)
			mIsDetectables[iTeam]
				= (mIsCheckDetection && (pIsDetectables != null) && pIsDetectables[iTeam])
				| (iTeam == mTeamIdx);

		mCoord = pCoord;
		if (mCoord.x < mBattleGrid.aMinCoordX)
			mCoord.x = mBattleGrid.aMinCoordX;
		if (mCoord.x > mBattleGrid.aMaxCoordX)
			mCoord.x = mBattleGrid.aMaxCoordX;
		if (mCoord.z < mBattleGrid.aMinCoordZ)
			mCoord.z = mBattleGrid.aMinCoordZ;
		if (mCoord.z > mBattleGrid.aMaxCoordZ)
			mCoord.z = mBattleGrid.aMaxCoordZ;

		mGridTile = pBattleGrid.GetTile(mCoord);

		mDirectionY = pDirectionY;

		mTeamIdx = pTeamIdx;
		mStateText = String.Empty;

		mGridCell = pBattleGrid.GetCell(mCoord);
		if (mGridCell != null)
			mChainItem = mGridCell.aObjectChain.AddLast(this); // 오브젝트를 새로운 셀에 넣습니다.
		else
			mChainItem = null;

		mOverlapObject = null;

		_ResetDetectionFlags();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDisableObject() - " + this);

		UpdateGridTile(null);
		UpdateGridCell(null);

		base.OnDestroy();

		// 연결된 태스크를 취소합니다.
		if (mUndetectedCheckTask != null)
		{
			mUndetectedCheckTask.Cancel();
			mUndetectedCheckTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표를 갱신합니다.
	public virtual void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		mCoord = pCoord;

		if (mCoord.x < mBattleGrid.aMinCoordX)
			mCoord.x = mBattleGrid.aMinCoordX;
		if (mCoord.x > mBattleGrid.aMaxCoordX)
			mCoord.x = mBattleGrid.aMaxCoordX;
		if (mCoord.z < mBattleGrid.aMinCoordZ)
			mCoord.z = mBattleGrid.aMinCoordZ;
		if (mCoord.z > mBattleGrid.aMaxCoordZ)
			mCoord.z = mBattleGrid.aMaxCoordZ;

		// 타일 변화를 갱신합니다.
		int lNewTileCol = mCoord.x / mBattleGrid.aTileScaleX;
		int lNewTileRow;
		if ((lNewTileCol & 0x01) == 0) // 짝수라면
			lNewTileRow = mCoord.z / mBattleGrid.aTileScaleZ;
		else
			lNewTileRow = (mCoord.z + mBattleGrid.aTileBlockScaleZ) / mBattleGrid.aTileScaleZ;
		BattleGridTile lNewGridTile = mBattleGrid.aTiles[lNewTileRow, lNewTileCol];
		if (mGridTile != lNewGridTile)
			UpdateGridTile(lNewGridTile); // 타일 변화 반영

		// 셀 변화를 갱신합니다.
		int lNewCellCol = mCoord.x / mBattleGrid.aCellScaleX;
		int lNewCellRow;
		if ((lNewCellCol & 0x01) == 0) // 짝수라면
			lNewCellRow = mCoord.z / mBattleGrid.aCellScaleZ;
		else
			lNewCellRow = (mCoord.z + mBattleGrid.aCellBlockScaleZ) / mBattleGrid.aCellScaleZ;
		BattleGridCell lNewGridCell = mBattleGrid.aCells[lNewCellRow, lNewCellCol];
		if (mGridCell != lNewGridCell)
			UpdateGridCell(lNewGridCell); // 셀 변화 반영

		// 탐지 좌표를 갱신합니다.
		Coord2 lMoveVector = mCoord - mLastDetectionCoord;
		if (lMoveVector.SqrMagnitude() > BattleConfig.get.mSqrDetectionUpdateDegree)
			UpdateDetection();
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 타일을 갱신합니다.
	protected virtual void UpdateGridTile(BattleGridTile pUpdateGridTile)
	{
		//Debug.Log("UpdateGridTile() pUpdateGridTile=" + pUpdateGridTile + " - " + this);

		mGridTile = pUpdateGridTile;
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 셀을 갱신합니다.
	protected virtual void UpdateGridCell(BattleGridCell pUpdateGridCell)
	{
		//Debug.Log("UpdateGridCell() pUpdateGridCell=" + pUpdateGridCell + " - " + this);

		if (mChainItem != null)
			mChainItem.Empty();
		mGridCell = pUpdateGridCell;
		if (mGridCell != null)
			mChainItem = pUpdateGridCell.aObjectChain.AddLast(this); // 오브젝트를 새로운 셀에 넣습니다.
		else
			mChainItem = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향을 갱신합니다.
	public virtual void UpdateDirectionY(int pDirectionY)
	{
		mDirectionY = pDirectionY;
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀을 갱신합니다.
	public virtual void UpdateTeam(int pUpdateTeamIdx)
	{
		mTeamIdx = pUpdateTeamIdx;

		_ResetDetectionFlags();
		UpdateDetection();
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 이벤트의 처리가 가능한지 검사합니다.
	public virtual bool CheckTouch(BattleGrid.Touch pGridTouch)
	{
		return aStageObject.CheckTouch(pGridTouch);
	}
	//-------------------------------------------------------------------------------------------------------
	// 한시적으로 보이게 합니다.
	public void SetTemporaryDetection(int pDetectingTeamIdx, int pDetectionDelay)
	{
		if (!mIsDetecteds[pDetectingTeamIdx])
		{
			mIsDetecteds[pDetectingTeamIdx] = true;
			OnDetected(pDetectingTeamIdx, true, null);
		}
		_SetUndetectedCheckTimers(pDetectingTeamIdx, pDetectionDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 색적 변화 이벤트를 처리합니다.
	public virtual void OnDetected(int pDetectingTeamIdx, bool pIsDetected, BattleObject pDetectingObject)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐지 상태를 지정합니다.
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
		if (!mIsDetectables[pDetectingTeamIdx])
			return; // 탐지 처리 제외 대상

		if (mIsToDetecteds[pDetectingTeamIdx] == pIsToDetected)
			return;

		mIsToDetecteds[pDetectingTeamIdx] = pIsToDetected;
		mDetectionChangeTimes[pDetectingTeamIdx] = aFrameUpdater.aElapsedTime; // 탐지된 지금 시점의 시간을 저장
		mDetectingObjectLinks[pDetectingTeamIdx].Set(null);
		//Debug.Log("ReserveTask(_OnDetected) _SetDetected() pDetectingTeamIdx=" + pDetectingTeamIdx + " pIsToDetected=" + pIsToDetected + " - " + this);
		if (BattleConfig.get.mDetectionChangeDelay > 0)
			ReserveTask(_OnDetected, BattleConfig.get.mDetectionChangeDelay);
		else
			_OnDetected(null, 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐지 상태를 초기화합니다.
	public void ResetDetection()
	{
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			aIsDetecteds[iTeam] = (iTeam == mTeamIdx); // 같은 팀에게만 보이고
			mUndetectedCheckTimers[iTeam] = 0; // 탐지 타이머 초기화
			OnDetected(iTeam, aIsDetecteds[iTeam], null);
		}

		if (mUndetectedCheckTask != null)
		{
			mUndetectedCheckTask.Cancel();
			mUndetectedCheckTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐지 상태를 갱신합니다.
	public void UpdateDetection()
	{
		if (!mIsCheckDetection)
			return;

		// 팀별로 탐지 변화를 처리합니다.
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			if (iTeam == mTeamIdx)
				continue; // 같은 팀은 변화 없음

			_UpdateTeamDetection(iTeam);
		}

		// 탐지 처리를 했던 좌표를 기억합니다.
		mLastDetectionCoord = aCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대의 탐지 기능이 꺼지는 갱신을 합니다.
	public void UpdateDetectionOff()
	{
		if (!mIsDetecting) // [주의] 이 객체의 다른 객체에 대한 탐지 기능이 꺼질 때의 처리이므로 mIsDetectables가 아닌 mIsDetecting 임
			return; // 탐지 처리 제외 대상

		// 팀별로 탐지 변화를 처리합니다.
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			if (iTeam == mTeamIdx)
				continue; // 같은 팀은 변화 없음

			using (GuidObjectChainEnumerator<BattleObject> lEnemyObjectEnumerator = aBattleGrid.aDetectionObjectChains[iTeam].CreateEnumerator())
			{
				while (lEnemyObjectEnumerator.MoveNext())
				{
					// 적군이 아군 팀에게 탐지가 안되는 검사를 요청합니다.
					BattleObject lEnemyObject = lEnemyObjectEnumerator.GetCurrent();
					if (lEnemyObject.mIsDetecteds[mTeamIdx] &&
						!lEnemyObject.mIsUpdateUndetecteds[mTeamIdx])
						lEnemyObject._SetUndetectedCheckTimers(mTeamIdx, BattleConfig.get.mDetectionOffDelay);
				}
			}

			// 탐지 꺼짐 처리를 합니다.
			OnDetected(iTeam, false, null);
		}

		// 탐지 처리를 했던 좌표를 기억합니다.
		mLastDetectionCoord = aCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 다른 객체를 탐지 가능한지를 지정합니다.
	public void EnableDetecting(bool pIsDetecting)
	{
		if (mIsDetecting == pIsDetecting)
			return;
		mIsDetecting = pIsDetecting;

		if (pIsDetecting)
			UpdateDetection();
		else
			UpdateDetectionOff();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 객체를 다른 객체가 탐지 가능한지를 지정합니다.
	public void EnableDetectables(bool pIsDetectableAll)
	{
		bool lIsChanged = false;
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			bool lIsDetectable = (mIsCheckDetection && pIsDetectableAll) || (iTeam == mTeamIdx);
			if (mIsDetectables[iTeam] == lIsDetectable)
				continue;
			lIsChanged = true;
			mIsDetectables[iTeam] = lIsDetectable;
		}

		if (lIsChanged)
			UpdateDetection();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 탐지 관련 플래그를 초기화합니다.
	private void _ResetDetectionFlags()
	{
		if (mIsCheckDetection)
		{
			mLastDetectionCoord = new Coord2(int.MinValue, int.MaxValue);
			for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
			{
				mDetectionChangeTimes[iTeam] = (iTeam == mTeamIdx) ? 0 : cNonDetectionTime; // 아군끼리는 등장한 상태에서 시작
				mDetectingObjectLinks[iTeam].Set(null);
				mIsToDetecteds[iTeam] = (iTeam == mTeamIdx);
				mUndetectedCheckTimers[iTeam] = 0;
				mIsUpdateUndetecteds[iTeam] = false;
				mIsDetecteds[iTeam] = mIsToDetecteds[iTeam];
			}
		}
		else
		{
			mLastDetectionCoord = mCoord;
			for (int iTeam = 0; iTeam < BattleMatchInfo.cMaxTeamCount; ++iTeam) // 비탐지 오브젝트는 팀 생성 이전에 존재할 수 있으므로 BattleMatchInfo.cMaxTeamCount를 사용
			{
				mDetectionChangeTimes[iTeam] = 0;
				mDetectingObjectLinks[iTeam].Set(null);
				mIsToDetecteds[iTeam] = true;
				mUndetectedCheckTimers[iTeam] = 0;
				mIsUpdateUndetecteds[iTeam] = false;
				mIsDetecteds[iTeam] = true;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 탐지 상태를 갱신합니다.
	public void _UpdateTeamDetection(int pDetectingTeamIdx)
	{
		//Debug.Log("\n_UpdateDetection() mLastDetectionCoord=" + mLastDetectionCoord + " aCoord=" + aCoord + " - " + this);

		mIsToDetecteds[pDetectingTeamIdx] = false; // 안보이는 것으로 간주하고 탐지 검사를 시작

		// 탐지가 되는 변화를 처리합니다.
		using (GuidObjectChainEnumerator<BattleObject> lEnemyObjectEnumerator = aBattleGrid.aDetectionObjectChains[pDetectingTeamIdx].CreateEnumerator())
		{
			while (lEnemyObjectEnumerator.MoveNext())
			{
				BattleObject lEnemyObject = lEnemyObjectEnumerator.GetCurrent();
				int lDistanceX = aCoord.x - lEnemyObject.aCoord.x;
				int lDistanceZ = aCoord.z - lEnemyObject.aCoord.z;

				bool lIsDetectedToEnemy = (lEnemyObject.mIsDetecting && !mIsToDetecteds[pDetectingTeamIdx] && mIsDetectables[pDetectingTeamIdx]);// && (lEnemyObject.aDetectionRange > 0));
				if (lIsDetectedToEnemy) // 아군이 적군에게 탐지되려는 상태에서의 조건 검사
				{
					int lDetectionToEnemyRange = !aIsHiding ? lEnemyObject.aDetectionRange : lEnemyObject.aHidingDetectionRange;
					if ((lDistanceX > lDetectionToEnemyRange) ||
						(-lDistanceX > lDetectionToEnemyRange) ||
						(lDistanceZ > lDetectionToEnemyRange) ||
						(-lDistanceZ > lDetectionToEnemyRange))
						lIsDetectedToEnemy = false;
				}

				bool lIsDetectedToAlly = (mIsDetecting && !lEnemyObject.mIsToDetecteds[mTeamIdx] && lEnemyObject.mIsDetectables[mTeamIdx]);// && (aDetectionRange > 0));
				if (lIsDetectedToAlly) // 적군이 아군에게 탐지되려는 상태에서의 조건 검사
				{
					int lDetectionToAllyRange = !lEnemyObject.aIsHiding ? aDetectionRange : aHidingDetectionRange;
					if ((lDistanceX > lDetectionToAllyRange) ||
						(-lDistanceX > lDetectionToAllyRange) ||
						(lDistanceZ > lDetectionToAllyRange) ||
						(-lDistanceZ > lDetectionToAllyRange))
						lIsDetectedToAlly = false;
				}

				int lSqrDistance;
				bool lIsEnemyObjectUndetectedChecked = false;

				if (lIsDetectedToEnemy ||
					lIsDetectedToAlly ||
					lEnemyObject.mIsDetecteds[mTeamIdx])
				{
					lSqrDistance = lDistanceX * lDistanceX + lDistanceZ * lDistanceZ;

					// 아군이 적군 팀에 탐지된 경우를 처리합니다.
					if (lIsDetectedToEnemy)
					{
						if (lSqrDistance <= (!aIsHiding ? lEnemyObject.aSqrDetectionRange : lEnemyObject.aSqrHidingDetectionRange))
						{
							mIsToDetecteds[pDetectingTeamIdx] = true;
							if (mIsToDetecteds[pDetectingTeamIdx] != mIsDetecteds[pDetectingTeamIdx])
							{
								mDetectionChangeTimes[pDetectingTeamIdx] = aFrameUpdater.aElapsedTime; // 탐지가 바뀌는 지금 시점의 시간을 저장
								mDetectingObjectLinks[pDetectingTeamIdx].Set(lEnemyObject);
								_ResetUndetectedCheckTimers(pDetectingTeamIdx); // 탐지 해제 검사 불필요
								if (BattleConfig.get.mDetectionChangeDelay > 0)
									ReserveTask(_OnDetected, BattleConfig.get.mDetectionChangeDelay);
								else
									_OnDetected(null, 0);
							}
						}
						//else
						//	lIsDetectedToEnemy = false;
					}

					// 적군이 아군 팀에 탐지된 경우를 처리합니다.
					if (lIsDetectedToAlly)
					{
						if (lSqrDistance <= (!lEnemyObject.aIsHiding ? aSqrDetectionRange : aSqrHidingDetectionRange))
						{
							lEnemyObject.mIsToDetecteds[mTeamIdx] = true;
							if (lEnemyObject.mIsToDetecteds[mTeamIdx] != lEnemyObject.mIsDetecteds[mTeamIdx])
							{
								lEnemyObject.mDetectionChangeTimes[mTeamIdx] = lEnemyObject.aFrameUpdater.aElapsedTime; // 탐지가 바뀌는 지금 시점의 시간을 저장
								lEnemyObject.mDetectingObjectLinks[mTeamIdx].Set(this);
								lEnemyObject._ResetUndetectedCheckTimers(mTeamIdx); // 탐지 해제 검사 불필요
								if (BattleConfig.get.mDetectionChangeDelay > 0)
									lEnemyObject.ReserveTask(lEnemyObject._OnDetected, BattleConfig.get.mDetectionChangeDelay);
								else
									lEnemyObject._OnDetected(null, 0);
							}
						}
						else
						{
							lIsDetectedToAlly = false;
							lIsEnemyObjectUndetectedChecked = true;
						}
					}

					// 적군이 아군 팀에게 탐지가 안되는 검사를 예약합니다.
					if (!lIsDetectedToAlly && // 아군에게 탐지되는 처리를 하지 않고
						!lEnemyObject.mIsUpdateUndetecteds[mTeamIdx] && // 탐지 해제 검사가 없는데
						lEnemyObject.mIsDetecteds[mTeamIdx]) // 탐지된 적이라면
					{
						// 탐지 해제 검사를 예약합니다.
						if (!lIsEnemyObjectUndetectedChecked)
						{
							if (lSqrDistance > (!lEnemyObject.aIsHiding ? aSqrDetectionRange : aSqrHidingDetectionRange))
								lIsEnemyObjectUndetectedChecked = true;
						}
						if (lIsEnemyObjectUndetectedChecked)
							lEnemyObject._SetUndetectedCheckTimers(mTeamIdx, BattleConfig.get.mDetectionOffDelay);
					}
				}
			}
		}

		// 아군이 적 팀에게 탐지가 안되는 변화를 처리합니다.
		if (!mIsToDetecteds[pDetectingTeamIdx] &&
			mIsDetecteds[pDetectingTeamIdx])
		{
			//Debug.Log("OnDetected() when mUndetectedCheckTimers iTeam=" + iTeam);
			mIsDetecteds[pDetectingTeamIdx] = false;
			OnDetected(pDetectingTeamIdx, false, null);
		}

		mIsUpdateUndetecteds[pDetectingTeamIdx] = false; // 이제 이 팀에 대해서는 갱신 했음
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnDetected(FrameTask pTask, int pFrameDelta)
	{
		//Debug.Log("_OnDetected() - " + this);

// 		OnDetected(pDetectingTeamIdx, pIsDetected); // 이벤트를 호출합니다.
// 		OnDetected(iTeam, lIsDetected); // 이벤트를 호출합니다.

		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			//Debug.Log("iTeam=" + iTeam + " mIsDetecteds[iTeam]=" + mIsDetecteds[iTeam] + " mIsToDetecteds[iTeam]=" + mIsToDetecteds[iTeam]);
			if (mIsDetecteds[iTeam] == mIsToDetecteds[iTeam])
				continue;

			if (!mIsDetecteds[iTeam])
			{
				// 탐지 켜짐을 처리합니다.
				int lDetectionElapsedTime = aFrameUpdater.aElapsedTime - mDetectionChangeTimes[iTeam];
				//Debug.Log("lDetectionElapsedTime=" + lDetectionElapsedTime + " BattleConfig.get.mDetectionChangeDelay=" + BattleConfig.get.mDetectionChangeDelay);
				if (lDetectionElapsedTime >= BattleConfig.get.mDetectionChangeDelay)
				{
					mIsDetecteds[iTeam] = true;
					OnDetected(iTeam, true, mDetectingObjectLinks[iTeam].Get()); // 이벤트를 호출합니다.
				}
			}
			else
			{
				// 탐지 꺼짐을 처리합니다.
				int lDetectionElapsedTime = aFrameUpdater.aElapsedTime - mDetectionChangeTimes[iTeam];
				if (lDetectionElapsedTime >= BattleConfig.get.mDetectionChangeDelay)
				{
					mIsDetecteds[iTeam] = false;
					OnDetected(iTeam, false, null); // 이벤트를 호출합니다.
				}
			}
			//Debug.Log("iTeam=" + iTeam + " mIsDetecteds[iTeam]=" + mIsDetecteds[iTeam]);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 팀에 대한 탐지 해제 검사를 예약합니다.
	private void _SetUndetectedCheckTimers(int pDetectingTeamIdx, int pCheckDelay)
	{
		mUndetectedCheckTimers[pDetectingTeamIdx] = pCheckDelay;
		mIsUpdateUndetecteds[pDetectingTeamIdx] = true; // 적군이 아군에 대해서는 스스로 탐지 갱신 필요

		if (mUndetectedCheckTask == null)
			mUndetectedCheckTask = aFrameUpdater.RepeatTask(this, _CheckUndetected, FrameUpdateLayer.Layer1); // 탐지 갱신 태스크가 없다면 태스크 갱신 시작
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 팀에 대한 탐지 해제 검사를 해제합니다.
	private void _ResetUndetectedCheckTimers(int pDetectingTeamIdx)
	{
		mUndetectedCheckTimers[pDetectingTeamIdx] = 0;
		mIsUpdateUndetecteds[pDetectingTeamIdx] = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTask] 콜백
	private void _CheckUndetected(FrameTask pTask, int pFrameDelta)
	{
		//Debug.Log("_CheckUndetected() - " + this);

		// 탐지 변경을 처리합니다.
		bool lIsChecked = false;
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
		{
			if (mIsUpdateUndetecteds[iTeam] &&
				(mUndetectedCheckTimers[iTeam] >= 0)) // 0(즉시) 포함
			{
				//Debug.Log("_CheckUndetected() detecting team=" + iTeam + " timer=" + mUndetectedCheckTimers[iTeam] + " - " + this);

				mUndetectedCheckTimers[iTeam] -= pFrameDelta;
				if (mUndetectedCheckTimers[iTeam] <= 0)
					_UpdateTeamDetection(iTeam);
				else
					lIsChecked = true;
			}
		}

		if (!lIsChecked)
		{
			mUndetectedCheckTask.Cancel(); // 더이상 처리할 것이 없다면 취소
			mUndetectedCheckTask = null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;

	private BattleGrid mBattleGrid;
	private Coord2 mCoord;
	private int mDirectionY;
	private int mTeamIdx;
	private String mStateText;
	private String mHpText;

	private BattleGridTile mGridTile;

	private BattleGridCell mGridCell;
	private GuidObjectChainItem<BattleObject> mChainItem;

	private bool mIsCheckDetection;
	private bool mIsDetecting;
	private bool[] mIsDetectables;
	private bool[] mIsDetecteds;
	private int[] mDetectionChangeTimes;
	private GuidLink<BattleObject>[] mDetectingObjectLinks;
	private bool[] mIsToDetecteds;
	private Coord2 mLastDetectionCoord;
	private int[] mUndetectedCheckTimers;
	private bool[] mIsUpdateUndetecteds;
	private FrameTask mUndetectedCheckTask;

	private BattleObject mOverlapObject;
}
