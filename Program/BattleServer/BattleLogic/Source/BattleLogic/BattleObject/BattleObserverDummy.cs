using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleObserverDummy : BattleObject
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageObserverDummy aStageObserverDummy
	{
		get { return mStageObserverDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageObserverDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aDetectionRange
	{
		get { return mDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aSqrDetectionRange
	{
		get { return mSqrDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aHidingDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aSqrHidingDetectionRange
	{
		get { return 0; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 그리드 안에서의 관리 노드
	public LinkedListNode<BattleObserverDummy> aGridNode
	{
		get { return mGridNode; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleObserverDummy<{0},{1}>(aGuid:{2} aTeamIdx:{3})", aCoord.x, aCoord.z, aGuid, aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleObserverDummy(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mGridNode = new LinkedListNode<BattleObserverDummy>(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateObserverDummy(
		BattleGrid pBattleGrid,
		Coord2 pCoord,
		int pDirectionY,
		int pObserverTeamIdx,
		int pDetectionRange,
		int pShowDelay)
	{
		OnCreateObject(
			pBattleGrid,
			true, // bool pIsCheckDetection
			true, // bool pIsDetecting
			null, // bool pIsDetectables
			pCoord,
			pDirectionY,
			pObserverTeamIdx);

		// 초기화를 합니다.
		mDetectionObjectChainItem = pBattleGrid.aDetectionObjectChains[aTeamIdx].AddLast(this);

		mDetectionRange = pDetectionRange;
		mSqrDetectionRange = mDetectionRange * mDetectionRange;

		mStageObserverDummy = aBattleGrid.aStage.CreateObserverDummy(this);

		OnDetected(aTeamIdx, true, null); // 자신의 팀에게는 보이는 상태로 시작합니다.
		UpdateDetection(); // 탐지 상태를 갱신합니다.

		mShowEndTask = ReserveTask(_OnShowEnd, pShowDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		if (mGridNode.List != null)
			mGridNode.List.Remove(mGridNode);

		// 이 부대의 탐지 범위가 없어지는 갱신을 합니다.
		UpdateDetectionOff();

		// 스테이지에서 제거합니다.
		mStageObserverDummy.DestroyObject();
		mStageObserverDummy = null;

		mDetectionObjectChainItem.Empty();

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyObserverDummy(this);

		// 연결된 태스크를 취소합니다.
		if (mShowEndTask != null)
		{
			mShowEndTask.Cancel();
			mShowEndTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void OnDetected(int pDetectingTeamIdx, bool pIsDetected, BattleObject pDetectingObject)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 보이는 시간이 다 했을 때 호출됩니다.
	private void _OnShowEnd(FrameTask pTask, int pFrameDelta)
	{
		if (mShowEndTask.aGuid != pTask.aGuid)
			return; // 다른 예약이 새로 걸렸음

		Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private LinkedListNode<BattleObserverDummy> mGridNode;
	private GuidObjectChainItem<BattleObject> mDetectionObjectChainItem;

	private int mDetectionRange;
	private int mSqrDetectionRange;

	private IStageObserverDummy mStageObserverDummy;

	private FrameTask mShowEndTask;
}
