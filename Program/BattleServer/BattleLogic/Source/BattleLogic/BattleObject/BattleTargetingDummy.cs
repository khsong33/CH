using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTargetingDummy : BattleObject, IBattleTarget
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 부대 스펙
	public TroopSpec aTroopSpec
	{
		get { return mTroopSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageTargetingDummy aStageTargetingDummy
	{
		get { return mStageTargetingDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageTargetingDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public BattleTroop aTargetTroop
	{
		get { return null; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public BattleTargetingDummy aTargetingDummy
	{
		get { return this; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public Coord2 aTargetCoord
	{
		get { return aCoord; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleTargetingDummy<{0},{1}>(aGuid:{2} aTeamIdx:{3} aSpec:{4})", aCoord.x, aCoord.z, aGuid, aTeamIdx, aTroopSpec.mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTargetingDummy(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mIsTargetDetectables = new bool[BattleMatchInfo.cMaxTeamCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateTargetingDummy(
		BattleGrid pBattleGrid,
		BattleTroop pTroop,
		int pTargetTeamIdx,
		int pShowDelay)
	{
		if (pTargetTeamIdx >= 0) // 타겟 팀이 지정되어 있다면
			for (int iTeam = 0; iTeam < pBattleGrid.aTeamCount; ++iTeam)
				mIsTargetDetectables[iTeam] = (pTargetTeamIdx == iTeam); // 타겟 팀에게만 보임
		else // 타겟 팀이 지정되어 있지 않다면
			for (int iTeam = 0; iTeam < pBattleGrid.aTeamCount; ++iTeam)
				mIsTargetDetectables[iTeam] = true; // 모든 팀에게 보임

		OnCreateObject(
			pBattleGrid,
			true, // bool pIsCheckDetection
			false, // bool pIsDetecting
			mIsTargetDetectables, // bool pIsDetectables (탐지되는 것만 가능)
			pTroop.aCoord,
			pTroop.aDirectionY,
			pTroop.aTeamIdx);

		// 초기화를 합니다.
		mTroopSpec = pTroop.aTroopSpec;

		mTargetingDummyChainItem = pBattleGrid.aTargetingDummyChains[aTeamIdx].AddLast(this);
		mDetectionObjectChainItem = pBattleGrid.aDetectionObjectChains[aTeamIdx].AddLast(this);

		mStageTargetingDummy = aBattleGrid.aStage.CreateTargetingDummy(this);

		OnDetected(aTeamIdx, true, null); // 자신의 팀에게는 보이는 상태로 시작합니다.
		UpdateDetection(); // 탐지 상태를 갱신합니다.

		mShowEndTask = ReserveTask(_OnShowEnd, pShowDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지에서 제거합니다.
		mStageTargetingDummy.DestroyObject();
		mStageTargetingDummy = null;

		mTargetingDummyChainItem.Empty();
		mDetectionObjectChainItem.Empty();

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyTargetingDummy(this);

		// 연결된 태스크를 취소합니다.
		if (mShowEndTask != null)
		{
			mShowEndTask.Cancel();
			mShowEndTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 위치에 보입니다.
	public void Show(BattleTroop pTroop, int pShowDelay)
	{
		UpdateCoord(pTroop.aCoord, true);
		UpdateDirectionY(pTroop.aDirectionY);

		UpdateDetection(); // 탐지 상태를 갱신합니다.

		// 스테이지에 갱신합니다.
		mStageTargetingDummy.UpdateCoord(pTroop.aCoord, true);
		mStageTargetingDummy.UpdateDirectionY(pTroop.aDirectionY);

		// 다시 예약합니다.
		if (mShowEndTask != null)
			mShowEndTask.Cancel();
		mShowEndTask = ReserveTask(_OnShowEnd, pShowDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void OnDetected(int pDetectingTeamIdx, bool pIsDetected, BattleObject pDetectingObject)
	{
		//Debug.Log("OnDetected() pDetectingTeamIdx=" + pDetectingTeamIdx + " pIsDetected=" + pIsDetected + " - " + this);

		// 스테이지의 색적 변화를 갱신합니다.
		mStageTargetingDummy.SetDetected(pDetectingTeamIdx, pIsDetected);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 보이는 시간이 다 했을 때 호출됩니다.
	private void _OnShowEnd(FrameTask pTask, int pFrameDelta)
	{
		Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private TroopSpec mTroopSpec;

	private GuidObjectChainItem<BattleTargetingDummy> mTargetingDummyChainItem;
	private GuidObjectChainItem<BattleObject> mDetectionObjectChainItem;

	private bool[] mIsTargetDetectables;

	private IStageTargetingDummy mStageTargetingDummy;

	private FrameTask mShowEndTask;
}
