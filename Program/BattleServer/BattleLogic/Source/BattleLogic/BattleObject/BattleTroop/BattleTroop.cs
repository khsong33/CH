using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroop : BattleObject, IBattleTroopRotater, IBattleTarget
{
	public enum StartState
	{
		Spawn,
		Observer,
		Npc,
		Hold,
	}
	public enum TroopCommand
	{
		None,
		SpawnTroops,
		MoveTroops,
		AttackTargetTroop,
		AttackTargetCoord,
	}

	public class ControlState
	{
		public bool mIsInFormation; // 포메이션 유지 여부
		public bool mIsWaitingFolloweeTroop; // 선행 부대를 기다리고 있는지 여부
		public bool mIsAttackMoving; // 공격 이동 여부
		public bool mIsSetUpMode; // 셋업 모드 여부
	}

	internal const int cBattleTimerDelay = 3000;

	public delegate void OnDestroyingDelegate(BattleTroop pTroop);
	public delegate void OnDestroyedDelegate(BattleCommander pCommander, int pTroopSlotIdx);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 그리드 (등록) 인덱스
	public int aTroopGridIdx
	{
		get { return mTroopGridIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 스펙
	public TroopSpec aTroopSpec
	{
		get { return mTroopSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨 스펙
	public TroopLevelSpec aLevelSpec
	{
		get { return mLevelSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨
	public int aLevel
	{
		get { return mLevel; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 스펙
	public VeterancySpec aVeterancySpec
	{
		get { return mVeterancySpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 기본 방향각
	public int aDefaultDirectionY
	{
		get { return mDefaultDirectionY; }
		set { mDefaultDirectionY = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 커맨더
	public BattleCommander aCommander
	{
		get { return mCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// (지휘관에서의) 부대 슬롯 인덱스
	public int aTroopSlotIdx
	{
		get { return mTroopSlotIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 선행 부대
	public BattleTroop aPrecedingTroop
	{
		get { return mPrecedingTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저
	public BattleUser aUser
	{
		get { return mUser; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯
	public BattleTroopStat aStat
	{
		get { return mStat; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 기록
	public BattleTroopRecord aRecord
	{
		get { return mRecord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 블리츠 모드 여부
	public bool aIsBlitzMode
	{
		get { return (mStat.aBlitzTimer > 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸통 회전 갱신자
	public BattleTroopRotationUpdater aBodyRotationUpdater
	{
		get { return mBodyRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸통 무기 회전 갱신자
	public BattleTroopRotationUpdater aBodyWeaponRotationUpdater
	{
		get { return mBodyWeaponRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 터렛 회전 갱신자
	public BattleTroopRotationUpdater aTurretRotationUpdater
	{
		get { return mTurretRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 터렛 방향각
	public int aTurretDirectionY
	{
		get { return mTurretAttackControl.aRotationDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 갱신자
	public BattleTroopMoveUpdater aMoveUpdater
	{
		get { return mMoveUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 점유 노드
	public LinkedListNode<BattleTroop> aOccupationNode
	{
		get { return mOccupationNode; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태 변화 콜백
	public OnDestroyingDelegate aOnDestroyingDelegate { get; set; } // 부대가 파괴되려 할 때 호출되는 콜백
	public OnDestroyedDelegate aOnDestroyedDelegate { get; set; } // 부대가 파괴되고서 호출되는 콜백
	//-------------------------------------------------------------------------------------------------------
	// 방어 타입
	public TroopDefenseType aDefenseType
	{
		get { return mDefenseType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 살아 있는지 여부
	public bool aIsAlive
	{
		get { return (mStat.aCurMilliHp > 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각 벡터 (크기는 cDirectionVectorSize)
	public Coord2 aDirectionVector
	{
		get { return mDirectionVector; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 영향을 받고 있는 지형 타입 스펙
	public GroundSpec aAffectedGroundSpec
	{
		get { return mAffectedGroundSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 영향을 받는 지형 스탯 테이블
	public GroundStatTable aAffectedGroundStatTable
	{
		get { return mAffectedGroundStatTable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드
	public BattleBarricade aCoveringBarricade
	{
		get { return mCoveringBarricade; }
		set { mCoveringBarricade = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 로그를 남길지 여부
	public bool aIsLogControl
	{
		get { return mIsLogControl; }
		set { mIsLogControl = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageTroop aStageTroop
	{
		get { return mStageTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTroopRotater 속성
	public int aRotationDirectionY
	{
		get { return aDirectionY; }
		set
		{
			UpdateDirectionY(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동중인지 여부
	public bool aIsMoving
	{
		get { return mMoveUpdater.aIsMoving; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격중인지 여부
	public bool aIsAttacking
	{
		get { return mController.aIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주 무기가 공격중인지 여부
	public bool aIsPrimaryAttacking
	{
		get { return mController.aMainAttackControl.aIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸통이 주 공격 컨트롤인지 여부
	public bool aIsPrimaryAttackBody
	{
		get { return (mController.aMainAttackControl == mBodyAttackControl); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 터렛이 공격중인지 여부
	public bool aIsTurretAttacking
	{
		get { return mTurretAttackControl.aIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 공격 중인 적 부대
	public BattleTroop aAttackingTargetTroop
	{
		get { return mController.aAttackingTargetTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 탐색 비교자
	public BattleTroopTargetSearchComparator aTargetSearchComparator
	{
		get { return mTargetSearchComparator; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대를 명시적으로 공격하고 있는 지휘관들의 리스트
	public LinkedList<BattleCommander> aExplicitTargetingCommanderList
	{
		get { return mExplicitTargetingCommanderList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 상태
	public ControlState aControlState
	{
		get { return mControlState; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 낙오 여부
	public bool aIsFallBehind
	{
		get { return mIsFallBehind; }
		set
		{
			if (mIsFallBehind == value)
				return;

			mIsFallBehind = value;
			mStageTroop.UpdateReturnState(value);

			mCommander.UpdateFormationOrder(); // 포메이션 순번 갱신
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 관찰자 여부
	public bool aIsObserver
	{
		get { return mIsObserver; }
	}
	//-------------------------------------------------------------------------------------------------------
	// NPC 여부
	public bool aIsNpc
	{
		get { return mIsNpc; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 무적 여부
	public bool aIsNoDamage
	{
		get { return mIsNoDamage; }
		set { mIsNoDamage = value || (mIsObserver || mIsNpc); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 가능 여부
	public bool aIsTargetable
	{
		get { return mIsTargetable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override bool aIsHiding
	{
		get { return mIsHiding; }
		set
		{
			if (mIsHiding == value)
				return;

			mIsHiding = value;
			if (mIsHiding) // 은폐 상태가 되면
				ResetDetection(); // 탐지 상태 초기화
			else // 은폐 상태가 해제되면
				UpdateDetection(); // 탐지 상태 갱신
			mStageTroop.UpdateHiding(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aDetectionRange
	{
		get { return mStat.aSightRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aSqrDetectionRange
	{
		get { return mStat.aSqrSightRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aHidingDetectionRange
	{
		get { return mStat.aHidingDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override int aSqrHidingDetectionRange
	{
		get { return mStat.aSqrHidingDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// HP 비율(%)
	public int aHpPercent
	{
		get { return mHpPercent; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부상 여부
	public bool aIsWounded
	{
		get { return mIsWounded; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 중 여부
	public bool aIsInBattle
	{
		get { return (mBattleTimer > 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 명령
	public BattleTroopCommand aCommand
	{
		get { return mCommand; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 처리
	public BattleTroopSkillProcess aSkillProcess
	{
		get { return mSkillProcess; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호 작용
	public BattleTroopInteraction aInteraction
	{
		get { return mInteraction; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 큐
	public BattleTroopActionQueue aActionQueue
	{
		get { return mActionQueue; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 가장 최근의 스킬 효과 번호
	public uint aLastSkillEffectGuid
	{
		get { return mLastSkillEffectGuid; }
		set { mLastSkillEffectGuid = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대별 경험치 제공
	public int aExperience
	{
		get { return mTroopSpec.mStatTable.Get(TroopStat.Experience); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화된 컨트롤 이름
	public String aActiveControlName
	{
		get { return mController.aActiveControl.ToString(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public BattleTroop aTargetTroop
	{
		get { return this; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public BattleTargetingDummy aTargetingDummy
	{
		get { return null; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleTarget 속성
	public Coord2 aTargetCoord
	{
		get { return aCoord; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleTroop<{0},{1}>(aGuid:{2} aTeamIdx:{3} aTroopSlotIdx:{4} aSpec:{5})", aCoord.x, aCoord.z, aGuid, aTeamIdx, aTroopSlotIdx, aTroopSpec.mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroop(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mPrecedingTroopLink = new GuidLink<BattleTroop>();

		mRecord = new BattleTroopRecord();

		mMoveUpdater = new BattleTroopMoveUpdater();
		mOccupationNode = new LinkedListNode<BattleTroop>(this);
		mController = new BattleTroopController();
		mBodyAttackControl = mController.aBodyAttackControl;
		mTurretAttackControl = mController.aTurretAttackControl;
		mBodyRotationUpdater = new BattleTroopRotationUpdater();
		mBodyWeaponRotationUpdater = new BattleTroopRotationUpdater();
		mTurretRotationUpdater = new BattleTroopRotationUpdater();

		mExplicitTargetingCommanderList = new LinkedList<BattleCommander>();

		mTargetSearchComparator = new BattleTroopTargetSearchComparator();

		mControlState = new ControlState();

		mCommand = new BattleTroopCommand();
		mSkillProcess = new BattleTroopSkillProcess();
		mInteraction = new BattleTroopInteraction();
		mActionQueue = new BattleTroopActionQueue();

		mTargetingDummyLink = new GuidLink<BattleTargetingDummy>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateTroop(
		BattleGrid pBattleGrid,
		int pTroopGridIdx,
		TroopSpec pTroopSpec,
		Coord2 pSpawnCoord,
		int pSpawnDirectionY,
		BattleCommander pCommander,
		int pTroopSlotIdx,
		StartState pStartState)
	{
		int lCreationDirectionY
			= (pStartState == StartState.Spawn)
			? pSpawnDirectionY // 스폰인 경우는 주어진 방향으로
			: pBattleGrid.GetClampedTile(pSpawnCoord).aDirectionY; // 그 외의 경우는 스폰되는 타일의 방향으로

		OnCreateObject(
			pBattleGrid,
			true, // bool pIsCheckDetection
			!pCommander.aCommanderItem.mIsTeamUndefined, // bool pIsDetecting
			!pCommander.aCommanderItem.mIsTeamUndefined // bool pIsDetectable
				? ((pStartState != StartState.Observer) ? pBattleGrid.aIsAllDetectables : pBattleGrid.aIsNoneDetectables) // 팀이 정해져 있다면 일반 상태로 시작
				: pBattleGrid.aIsNoneDetectables, // 팀이 정해져 있지 않다면 비탐지 상태로 시작
			pSpawnCoord,
			lCreationDirectionY,
			pCommander.aTeamIdx);

		// 초기화를 합니다.
		mTroopGridIdx = pTroopGridIdx;
		mTroopSpec = pTroopSpec;
		mLevelSpec = mTroopSpec.mRankLevelSpecs[pCommander.aCommanderSpec.mRank];
		mLevel = pCommander.aCommanderItem.mCommanderLevel;
		mVeterancySpec = mTroopSpec.mVeterancySpecs[pCommander.aVeterancyRank];
		mCommander = pCommander;
		mTroopSlotIdx = pTroopSlotIdx;
		mPrecedingTroopLink.Set(null);
		mUser = pCommander.aUser;
		mCommander.aTroops[mTroopSlotIdx] = this;
		mDefaultDirectionY = aDirectionY;

		if (aBattleGrid.aTroops[mTroopGridIdx] != null)
			Debug.LogError("duplicate TroopGridIdx :" + mTroopGridIdx + " - " + this);
		aBattleGrid.aTroops[mTroopGridIdx] = this;

		UpdateKind();

		if (aGridTile.aTileType != BattleGridTileType.Block)
		{
			mAffectedGroundSpec = aGridTile.aGroundSpec;
			mAffectedGroundStatTable = aGridTile.aGroundStatTable;
		}
		else
		{
			mAffectedGroundSpec = GroundTable.get.aDefaultSpec;
			mAffectedGroundStatTable = mAffectedGroundSpec.mStatTable;
		}
		mCoveringBarricade = null;

		mStat = BattleTroopStat.Create(this);

		mIsLogControl = false;

		mTroopChainItem = pBattleGrid.aTroopChains[aTeamIdx].AddLast(this);
		mDetectionObjectChainItem = pBattleGrid.aDetectionObjectChains[aTeamIdx].AddLast(this);

		mControlState.mIsInFormation = true;
		mControlState.mIsWaitingFolloweeTroop = false;
		mControlState.mIsAttackMoving = false;
		mControlState.mIsSetUpMode = false;

		mIsFallBehind = false;
		mIsObserver = (pStartState == StartState.Observer);
		mIsNpc = (mIsObserver || (pStartState == StartState.Npc));
		mIsNoDamage = (mIsObserver || mIsNpc);
		mIsTargetable = (!mIsObserver && !mIsNpc);
		mIsHiding = mStat.aTroopStatTable.aIsHiding || mAffectedGroundStatTable.aIsHiding; // 은폐 동기화
		mLastSkillEffectGuid = 0;
		mBattleTimer = 0;

		if (!mIsObserver &&
			(aGridTile.aTileType != BattleGridTileType.Block))
			aGridTile.AddOccupation(this); // 생성하면서 지형 점유

		mRecord.Init(this);
		mMoveUpdater.Init(this);
		mController.Init(this);
		mBodyRotationUpdater.Init(this, this, BattleTroopRotationUpdater.UpdaterType.Body);
		mBodyWeaponRotationUpdater.Init(this, mBodyAttackControl.aTargetingInfo.aIsValid ? mBodyAttackControl : null, BattleTroopRotationUpdater.UpdaterType.BodyWeapon);
		mTurretRotationUpdater.Init(this, mTurretAttackControl.aTargetingInfo.aIsValid ? mTurretAttackControl : null, BattleTroopRotationUpdater.UpdaterType.Turret);

		mCommand.Init(mController);
		mSkillProcess.Init(mController);
		mInteraction.Init(mController);
		mActionQueue.Init(mController);

		mTargetSearchComparator.Set(this);

		mStageTroop = aBattleGrid.aStage.CreateTroop(this);

		ResetDefenseType();
		UpdateHp(TroopHpChangeType.Reset, 0);

		OnDetected(aTeamIdx, !mIsObserver, null); // 자신의 팀에게는 보이는 상태로 시작합니다.
		UpdateDetection(); // 탐지 상태를 갱신합니다.

		mStageTroop.UpdateSightRange();
		mStageTroop.UpdateAttackRange();
		mStageTroop.UpdateHiding(mIsHiding);
		mStageTroop.UpdateMoraleState(mStat.aMoraleState);

// [테스트] 특정 부대의 컨트롤을 강제 지정합니다.
// 		if (aCommander.aUser == null)
// 			pStartState = StartState.Hold;

		mController.StartControl(pStartState); // 컨트롤을 시작합니다.
		RepeatUpdate(); // OnFrameUpdate()
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 상호 작용을 중단합니다.
		if (mInteraction.aIsValid)
			mInteraction.StopInteraction();

		// 액션 큐를 비웁니다.
		mActionQueue.Empty();

		// 파괴 전 콜백을 호출합니다.
		if (aOnDestroyingDelegate != null)
		{
			aOnDestroyingDelegate(this);
			aOnDestroyingDelegate = null;
		}

		// 지휘관에서의 슬롯을 비웁니다.
		mCommander.aTroops[mTroopSlotIdx] = null;

		// 바리케이드에 있었다면 바리케이드를 벗어납니다.
		if (mCoveringBarricade != null)
			LeaveBarricade();

		// 이 부대의 탐지 범위가 없어지는 갱신을 합니다.
		UpdateDetectionOff();

		// 이 부대를 명시적으로 공격하고 있던 지휘관들에게 알립니다.
		foreach (BattleCommander iCommander in mExplicitTargetingCommanderList)
			iCommander.OnExplicitTargetTroopDestroyed(this);
		while (mExplicitTargetingCommanderList.Last != null)
			mExplicitTargetingCommanderList.RemoveLast(); // List.Clear()를 사용하면 LinkedListNode.List가 null이 안되고 그대로 있습니다. 그래서 하나씩 제거합니다.

		// 컨트롤을 멈춥니다.
		mController.StopControl();

		// 스테이지에서 제거합니다.
		mStageTroop.DestroyObject();
		mStageTroop = null;

		// 부대 등록을 해제합니다.
		mTroopChainItem.Empty();
		mDetectionObjectChainItem.Empty();

		aBattleGrid.aTroops[mTroopGridIdx] = null;

		// 부대 파괴 효과로 MP를 받습니다.
//		aBattleGrid.AddDestroyMilliMp(pTargetTroop.aTeamIdx, pTargetTroop.aTroopSpec);

		// 기록을 초기화합니다.
		mRecord.UpdateCommanderExp();
		mRecord.Reset();

		// 스탯을 파괴합니다.
		mStat.Destroy();

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 파괴 후 콜백을 호출합니다.
		if (aOnDestroyedDelegate != null)
		{
			aOnDestroyedDelegate(mCommander, mTroopSlotIdx);
			aOnDestroyedDelegate = null;
		}

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyTroop(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameObject 콜백
	public override void OnFrameUpdate(FrameTask pTask, int pFrameDelta)
	{
	#if UNITY_EDITOR
		if (aBattleGrid.aDebugTroopGuid == aGuid)
			aBattleGrid.aDebugTroopGuid = aGuid;
	#endif

		// 스킬 처리를 갱신합니다.
		if (mSkillProcess.aIsUpdating)
			mSkillProcess.UpdateFrame(pFrameDelta);

		// 상호 작용을 갱신합니다.
		if (mInteraction.aIsValid)
			mInteraction.UpdateFrame(pFrameDelta);

		// 액션 큐를 갱신합니다.
		if (mActionQueue.aIsValid)
			mActionQueue.UpdateFrame(pFrameDelta);

		// 컨트롤러를 갱신합니다.
		if (!mController.UpdateFrame(pFrameDelta))
		{
			Destroy();
			return; // 컨트롤 불가
		}

		// 부대 HP를 검사합니다.
		if (mStat.aCurMilliHp <= 0)
		{
			Destroy();
			return; // 부대 HP가 0이 됨
		}

		// 스탯을 갱신합니다.
		mStat.OnFrameUpdate(pFrameDelta);

		// 전투 타이머를 갱신합니다.
		if (mBattleTimer > 0)
			mBattleTimer -= pFrameDelta;

		// IStageTroop을 갱신합니다.
		mStageTroop.OnFrameUpdate(pFrameDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		base.UpdateCoord(pCoord, pIsArrived);

		// 스테이지 좌표를 갱신합니다.
		mStageTroop.MoveTo(pCoord, pIsArrived);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	protected override void UpdateGridTile(BattleGridTile pUpdateGridTile)
	{
		//Debug.Log("UpdateGridTile() pUpdateGridTile=" + pUpdateGridTile + " - " + this);

		if (!mIsObserver)
		{
			if ((aGridTile != null) &&
				(aGridTile.aTileType != BattleGridTileType.Block))
				aGridTile.RemoveOccupation(this);
			base.UpdateGridTile(pUpdateGridTile);
			if ((aGridTile != null) &&
				(aGridTile.aTileType != BattleGridTileType.Block))
				aGridTile.AddOccupation(this);
		}

		// 지형 효과를 갱신합니다.
		if ((aGridTile != null) &&
			(aGridTile.aTileType != BattleGridTileType.Block)) // 이동 과정에서 아주 잠시 Block 타일에 갈 수 있는데 이럴 때에는 이전 지형 효과를 유지합니다.
		{
			mAffectedGroundSpec = aGridTile.aGroundSpec;
			mAffectedGroundStatTable = aGridTile.aGroundStatTable;
			mStat.UpdateStat();
		}

		// 이벤트를 검사합니다.
		if ((aGridTile != null) &&
			(aGridTile.aLinkEventPoint != null) && // 연결된 이벤트 지점이 있는데
			aGridTile.aLinkEventPoint.aIsTriggerReady && // 이벤트가 준비되 있고
			aGridTile.aLinkEventPoint.CheckEvent(this)) // 이 부대가 발동시킬 수 있다면
			aGridTile.aLinkEventPoint.ActivateTrigger(this); // 이벤트 발동
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateDirectionY(int pDirectionY)
	{
		int lBodyRotationVector = pDirectionY - aDirectionY;
		base.UpdateDirectionY(pDirectionY);

		mDirectionVector = Coord2.RoughDirectionVector(pDirectionY, cDirectionVectorSize);

		// 스테이지 방향각을 갱신합니다.
		mStageTroop.RotateBodyToDirectionY(pDirectionY);

		// 붙어있는 로테이터도 같이 회전하게 됩니다.
		if (mBodyWeaponRotationUpdater.aIsValid)
		{
			mBodyWeaponRotationUpdater.aRotater.aRotationDirectionY += lBodyRotationVector;
			if (mBodyWeaponRotationUpdater.aIsRotating)
				mBodyWeaponRotationUpdater.CorrectRotation();
		}
		if (mTurretRotationUpdater.aIsValid)
		{
			//Debug.Log("mTurretRotationUpdater.aRotater.aRotationDirectionY=" + mTurretRotationUpdater.aRotater.aRotationDirectionY + " lBodyRotationVector=" + lBodyRotationVector);
			mTurretRotationUpdater.aRotater.aRotationDirectionY += lBodyRotationVector;
			if (mTurretRotationUpdater.aIsRotating)
				mTurretRotationUpdater.CorrectRotation();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표와 방향각을 초기화합니다.
	public void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		base.UpdateCoord(pCoord, true);
		base.UpdateDirectionY(pDirectionY);

		mDirectionVector = Coord2.RoughDirectionVector(pDirectionY, cDirectionVectorSize);

		mDefaultDirectionY = pDirectionY;

		mMoveUpdater.ResetMove();
		mBodyRotationUpdater.ResetRotation();
		mBodyWeaponRotationUpdater.ResetRotation();
		mTurretRotationUpdater.ResetRotation();
		mInteraction.StopInteraction();
		mController.ResetControl();

		// 스테이지에 보입니다.
		mStageTroop.SetCoordAndDirectionY(pCoord, pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateTeam(int pTeamIdx)
	{
		if (aTeamIdx == pTeamIdx)
			return;

		// 팀을 갱신합니다.
		base.UpdateTeam(pTeamIdx);

		// 팀별 체인 아이템을 갱신합니다.
		mTroopChainItem.Empty(); // 기존 아이템은 비움
		mTroopChainItem = aBattleGrid.aTroopChains[aTeamIdx].AddLast(this);
		mDetectionObjectChainItem.Empty(); // 기존 아이템은 비움
		mDetectionObjectChainItem = aBattleGrid.aDetectionObjectChains[aTeamIdx].AddLast(this);

		// 스테이지에 보입니다.
		mStageTroop.UpdateTeam(aTeamIdx);

		// 공격중이었다면 팀 변경에 의해 중단되어야 할 수도 있으므로 탐색 컨트롤을 시작합니다.
		if (mController.aIsAttacking)
			mController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.AdvanceWhenNoEnemy,
				0);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void OnDetected(int pDetectingTeamIdx, bool pIsDetected, BattleObject pDetectingObject)
	{
		//Debug.Log("OnDetected() pDetectingTeamIdx=" + pDetectingTeamIdx + " pIsDetected=" + pIsDetected + " - " + this);

		// 탐지가 되는 경우, 탐지하는 팀이 적이라면 대응을 할 수 있도록 합니다.
		if (pIsDetected &&
			(pDetectingTeamIdx != aTeamIdx))
		{
			// 적 발견 이벤트를 처리합니다.
			if (pDetectingObject != null)
			{
				BattleTroop lDetectingTroop = pDetectingObject as BattleTroop;
				if (lDetectingTroop != null)
				{
					//Debug.Log("lDetectingTroop=" + lDetectingTroop);
					if ((mCommander.aLinkEventPoint != null) && // 이 부대와 연결된 이벤트 지점이 있는데
						mCommander.aLinkEventPoint.aIsTriggerReady) // 이벤트가 준비되어 있다면
						mCommander.aLinkEventPoint.OnDetectedByTroop(lDetectingTroop); // 적 부대 발견을 처리합니다.
					if ((lDetectingTroop.mCommander.aLinkEventPoint != null) && // 적 부대와 연결된 이벤트 지점이 있는데
						lDetectingTroop.mCommander.aLinkEventPoint.aIsTriggerReady) // 이벤트가 준비되어 있다면
						lDetectingTroop.mCommander.aLinkEventPoint.OnDetectedByTroop(this); // 아군 부대 발견을 처리합니다.
				}
			}

			using (GuidObjectChainEnumerator<BattleTroop> lEnemyTroopEnumerator = aBattleGrid.aTroopChains[pDetectingTeamIdx].CreateEnumerator())
			{
				while (lEnemyTroopEnumerator.MoveNext())
				{
					BattleTroop lEnemyTroop = lEnemyTroopEnumerator.GetCurrent();
					if (lEnemyTroop.CheckInAttackRange(aCoord))
						lEnemyTroop.mCommander.ReportNewEnemy(lEnemyTroop.aTroopSlotIdx, this);
				}
			}
		}

		// 스테이지에 보이는 은폐 상태 갱신을 처리합니다.
		if (mIsHiding)
			mStageTroop.UpdateHiding(mIsHiding);

		// 지휘관의 색적 변화를 갱신합니다.
		mCommander.UpdateDetection(pDetectingTeamIdx);

		// 스테이지의 색적 변화를 갱신합니다.
		mStageTroop.UpdateDetection(pDetectingTeamIdx, pIsDetected);
	}
	//-------------------------------------------------------------------------------------------------------
	// 적에게 공격을 받았을 때의 처리를 합니다.
	public void OnAttackedByEnemy(BattleFireEffect pFireEffect, int pMilliDamage)
	{
// [테스트] 특정 부대의 공격 무시
// 		if (mCommander.aUser == null)
// 			return;

		if (pFireEffect.aAttackTeamIdx == aTeamIdx)
			return; // 같은 팀이면 무시

		// 공격한 적에 대한 대응을 합니다.
		BattleTroop lAttackTroop = pFireEffect.aAttackTroop;
		if (lAttackTroop != null)
		{
// 공격을 한 부대는 한시적으로 색적에 관계없이 탐지가 됩니다.
// 			if (!lAttackTroop.aIsDetecteds[aTeamIdx])
// 				lAttackTroop.SetTemporaryDetecting(aTeamIdx, BattleConfig.get.mDetectionOffDelay);
// 			if (lAttackTroop.aCheckUndetectedTimers[aTeamIdx] < BattleConfig.get.mDetectionOffDelay)
// 				lAttackTroop.aCheckUndetectedTimers[aTeamIdx] = BattleConfig.get.mDetectionOffDelay; // 탐지 꺼지는 타이머가 작동중이었다면 최대로 늘려 놓음

			// 지휘관 능력에 따라 주변에 엄폐 가능한 타일이 있을 경우 이동하는 처리를 합니다.
			if (mCommander.aTroopSet.mIsHidingMoveOnHit &&
				!mAffectedGroundStatTable.aIsArmorSpec &&
				!mIsFallBehind &&
				(mStat.aAdvanceSpeed > 0) &&
				mInteraction.aIsAdvancable)
			{
				Coord2 lEnemyVector = lAttackTroop.aCoord - aCoord;
				int lHidingDirectionY = lEnemyVector.RoughDirectionY();
				BattleGridTile lHidingTile = aGridTile.FindSurroundingHidingTile(1, lHidingDirectionY);
				if (lHidingTile != null)
				{
					//mControlState.mIsSetUpMode = (mStat.aTroopStatTable.Get(TroopStat.SetUpDelay) > 0); // 타겟 좌표 지정이라면 설치 딜레이가 있을 경우 셋업 모드로
					mController.StartDstCoordAdvanceControl(lHidingTile.aCenterCoord, 0);
				}
			}

			// 데미지를 준 지휘관을 기록합니다.
			if (pMilliDamage > 0)
				mRecord.RecordCommanderDamage(lAttackTroop.aCommander, pMilliDamage);
		}

		// 거점을 점령중이었다면 중단시킵니다.
		if ((mCommander.aTryCheckPoint != null) &&
			(mCommander.aLeaderTroop == this))
			mCommander.UngrabCheckPoint();

		mCommander.aStageCommander.ShowState(StageCommanderState.Hit);
	}
	//-------------------------------------------------------------------------------------------------------
	// HP를 갱신합니다.
	public int UpdateHp(TroopHpChangeType pHpChangeType, int pChangeValue)
	{
// [테스트] 전투 빨리 진행
//		pChangeValue *= 10;
// [테스트] 특정 팀 데미지 입지 않음
// 		if ((aTeamIdx == 1) && (pHpChangeType != TroopHpChangeType.Reset))
// 			return 0;
// [테스트] 치료 안 되게
// 		if ((pChangeValue > 0) && (pHpChangeType != TroopHpChangeType.Reset))
// 			return 0;

		int lActualChangeValue = pChangeValue;

		mStat.aCurMilliHp += pChangeValue;
		if (pChangeValue >= 0)
		{
			// 치료를 처리합니다.
			if (mStat.aCurMilliHp > mStat.aMaxMilliHp)
			{
				lActualChangeValue -= mStat.aCurMilliHp - mStat.aMaxMilliHp;
				mStat.aCurMilliHp = mStat.aMaxMilliHp;
			}
		}
		else
		{
			// 데미지를 처리합니다.
			if (mStat.aCurMilliHp < 0)
			{
				lActualChangeValue -= mStat.aCurMilliHp;
				mStat.aCurMilliHp = 0;
			}
		}

		mHpPercent = mStat.aCurMilliHp * 100 / mStat.aMaxMilliHp;
		mIsWounded = (mHpPercent < BattleConfig.get.mRecoveryStartHpPercents[(int)mTroopSpec.mBody]);

		aHpText = "HP " + (mStat.aCurMilliHp / 1000);
// 		if (mStat.mDefenseScale != 100)
// 			aHpText += "\nDEF " + (mStat.mDefenseScale - 100) + "%";

		if (pHpChangeType != TroopHpChangeType.Reset)
			mCommander.UpdateTotalHp(); // 지휘관의 전체 HP 갱신

		mStageTroop.UpdateHp(mStat.aCurMilliHp / 1000, mStat.aMaxMilliHp / 1000, pHpChangeType);

		return lActualChangeValue;
	}
	//-------------------------------------------------------------------------------------------------------
	// 병종을 갱신합니다.
	public void UpdateKind()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 방어 상태를 초기화합니다.
	public void ResetDefenseType()
	{
		mDefenseType = aTroopSpec.mDefaultDefenseType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 선행 부대를 지정합니다.
	public void SetPrecedingTroop(BattleTroop pPrecedingTroop)
	{
		mPrecedingTroopLink.Set(pPrecedingTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 방어 상태를 변경합니다.
	public void SetDefenseType(TroopDefenseType pDefenseType)
	{
		mDefenseType = pDefenseType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟을 찾습니다.
	public IBattleTarget SearchTarget(BattleTroop pLastTargetTroop, BattleTroopTargetingInfo pTargetingInfo, bool pIsTargetingRotatable)
	{
// [테스트] 특정 부대 무시
// 		if (mCommander.aUser == null)
// 			return null;

		// 지휘관 능력에 따라 집중 공격 처리를 합니다.
		if (mCommander.aTroopSet.mIsConcentratedFire &&
			(mCommander.aLeaderTroop != this))
		{
			BattleTroop lMainTargetTroop = mCommander.aLeaderTroop.mController.aAttackingTargetTroop;
			if (lMainTargetTroop != null)
				return lMainTargetTroop;
		}

		// 탐색으로 적 부대를 찾습니다.
		if (pTargetingInfo.aMainWeaponData == null)
		{
			Debug.LogError("pTargetingInfo.aMainWeaponData=null - " + this);
			return null;
		}
		WeaponSpec lSearchWeaponSpec = pTargetingInfo.aMainWeaponData.mWeaponSpec;
		BattleGrid.TeamSelection lTeamSelection
			= lSearchWeaponSpec.mIsAllyTargeting
			? BattleGrid.TeamSelection.AllyTeam : BattleGrid.TeamSelection.NonAllyTeam;

		mTargetSearchComparator.StartSearch(pTargetingInfo, pLastTargetTroop, pIsTargetingRotatable);
		BattleTroop lNewTargetTroop = aBattleGrid.SearchTroop(
			aCoord,
			mStat.aMaxAttackRange,
			aTeamIdx,
			lTeamSelection,
			mTargetSearchComparator);

		// 그룹 타게팅 중이었다면 정렬합니다.		
		if (pTargetingInfo.aIsGroupTargeting)
		{
			pTargetingInfo.SortGroupTargetingInfo(pLastTargetTroop);
			lNewTargetTroop = pTargetingInfo.GetNextTargetTroop();
		}

		// 적 부대가 있다면 반환하고 끝냅니다.
		if (lNewTargetTroop != null)
			return lNewTargetTroop;

		// 적 부대가 없고 곡사 무기라면 다시 타게팅 더미를 찾습니다.
		if (lSearchWeaponSpec.mAttackType == WeaponAttackType.HighAngleGun)
		{
			BattleTargetingDummy lNewTargetingDummy = aBattleGrid.SearchNearestTargetingDummy(aCoord, mStat.aMaxAttackRange, aTeamIdx);
			//Debug.Log("lNewTargetingDummy=" + lNewTargetingDummy + " - " + this);
			return lNewTargetingDummy;
		}

		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 어떤 적이든 찾습니다.
	public IBattleTarget SearchAnyTarget()
	{
		IBattleTarget lTarget;
		if (mBodyAttackControl.aTargetingInfo.aIsValid)
		{
			lTarget = SearchTarget(null, mBodyAttackControl.aTargetingInfo, aIsPrimaryAttackBody);
			if (lTarget != null)
				return lTarget;
		}
		if (mTurretAttackControl.aTargetingInfo.aIsValid)
		{
			lTarget = SearchTarget(null, mTurretAttackControl.aTargetingInfo, true);
			if (lTarget != null)
				return lTarget;
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 대해서 공격이 가능한지를 검사합니다.
	public bool CheckInAttackRange(Coord2 pTargetCoord)
	{
		Coord2 lTargetVector = pTargetCoord - aCoord;

		// 사거리를 검사합니다.
		int lSqrTargetDistance = lTargetVector.SqrMagnitude();
		if ((lSqrTargetDistance < mStat.aSqrMinAttackRange) ||
			(lSqrTargetDistance > mStat.aSqrMaxAttackRange))
			return false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 교전 타이머를 초기화합니다.
	public void SetBattleTimer()
	{
		mBattleTimer = cBattleTimerDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// 블리츠 명령을 내립니다.
	public void CommandBlitz(BattleBlitzInput pBlitzInput)
	{
		//Debug.Log("CommandBlitz() pBlitzInput=" + pBlitzInput + " - " + this);

// 		if (mStat.mBlitzCoolTimer > 0)
// 			return; // 블리츠 쿨타임 적용

// 		if (!BattlePlay.main.vBattleUIControlPanel.UseMp(aTeamIdx, pBlitzInput.mGaugeCount))
// 			return; // MP 부족

//		StartBlitzControl(pBlitzInput); // 블리츠 컨트롤을 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드에 들어가게 합니다.
	public void EnterBarricade(BattleBarricade pBarricade)
	{
		//Debug.Log("EnterBarricade() - " + this);

// 		if (!BattlePlay.main.vBattleUIControlPanel.UseMp(aTeamIdx, pBarricade.vMpUsage)) // 바리케이드에 들어가려는 시도를 하는 순간 MP를 소모합니다.
// 			return; // MP 부족

		mController.StartSkillPositioningAdvanceControl(pBarricade.aCoord, 0); // 바리케이드 컨트롤을 시작합니다.

		mCoveringBarricade = pBarricade;
// 		aBattleGrid.UpdateBarricadeButton();
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드에서 나가게 합니다.
	public void LeaveBarricade()
	{
		//Debug.Log("LeaveBarricade() - " + this);

		mController.StartBarricadeControl(null, 0);

		mCoveringBarricade.SetCoveringTroop(null);
		mCoveringBarricade = null;
// 		aBattleGrid.UpdateBarricadeButton();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 퇴각 정보를 표시합니다.
	public void ShowRetreat(bool pIsShow)
	{
		mStageTroop.ShowRetreat(pIsShow);
	}
	//-------------------------------------------------------------------------------------------------------
	// 경험치를 업데이트합니다.
	public void UpdateExperience(int pExperience)
	{
		mCommander.UpdateExperience(pExperience);
	}
	//-------------------------------------------------------------------------------------------------------
	// 랭크업 처리를 진행합니다.
	public void OnVeteranRankup(int pRank)
	{
		mVeterancySpec = mTroopSpec.mVeterancySpecs[pRank];

		if (pRank > 0) // 0일 경우 초기화 단계에서 이미 Setup을 완료한 상태임
		{
			mStat.SetUp(this, false);
			mStat.UpdateStat();
			mController.UpdateWeaponControlInfo();
		}

		if (mBodyAttackControl.aTargetingInfo.aIsValid)
			mBodyAttackControl.aTargetingInfo.UpdateStat();
		if (mTurretAttackControl.aTargetingInfo.aIsValid)
			mTurretAttackControl.aTargetingInfo.UpdateStat();

		OnVeteranExperience();

		mStageTroop.UpdateVeteranRank(pRank);
	}
	//-------------------------------------------------------------------------------------------------------
	// 얻은 경험치를 처리합니다.
	public void OnVeteranExperience()
	{
		aStageTroop.UpdateExp();
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 된 적의 관찰자 더미를 잠시 생성합니다.
	public void FlashTargetingEnemyObserverDummy(BattleTroopTargetingInfo pTargetingInfo)
	{
		mFlashTargetingDetectionRange = mTroopSpec.mUnitRadius + BattleConfig.get.mAttackDetectionRangeMargin;

		if (pTargetingInfo.aTargetTroop != null)
		{
			if (!aIsDetecteds[pTargetingInfo.aTargetTroop.aTeamIdx])
				aBattleGrid.SpawnObserverDummy(aCoord, aDirectionY, mFlashTargetingDetectionRange, pTargetingInfo.aTargetTroop.aTeamIdx);
		}
		else
		{
			bool lIsDetectedToAllEnemys = true;
			for (int iTeam = 1; iTeam < aBattleGrid.aTeamCount; ++iTeam) // 중립팀 제외
				if (!aIsDetecteds[iTeam])
					lIsDetectedToAllEnemys = false;

			if (!lIsDetectedToAllEnemys)
			{
				BattleGridCell aGridCell = aBattleGrid.GetCell(pTargetingInfo.aTargetCoord);
				aGridCell.CallTroopHandler(BattleGridCell.cNearCellMaxDistance, _OnFlashTargetingEnemyObserverDummy);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 더미를 잠시 보입니다.
	public void FlashTargetingDummy(BattleTroop pTargetTroop)
	{
		int lTargetTeamIdx = (pTargetTroop != null) ? pTargetTroop.aTeamIdx : -1;

		BattleTargetingDummy lTargetingDummy = mTargetingDummyLink.Get();
		if (lTargetingDummy == null)
		{
			lTargetingDummy = aBattleLogic.CreateTargetingDummy(aBattleGrid, this, lTargetTeamIdx, BattleConfig.get.mAttackDetectionDelay);
			mTargetingDummyLink.Set(lTargetingDummy);
		}
		else
		{
			lTargetingDummy.Show(this, BattleConfig.get.mAttackDetectionDelay);
		}

		// 지휘관의 타게팅 더미로 지정합니다.
		mCommander.SetTargetingDummy(lTargetingDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// 대화창을 보여줍니다.
	public void ShowTroopScript(String pMessageKey, float pDuration)
	{
		mStageTroop.ShowTroopScript(pMessageKey, pDuration);
	}

#if BATTLE_VERIFY_TEST
    public void Test_ModifyStat(TroopStat stat, int value)
    {
        mStat.aTroopStatTable.Test_ModifyStat(stat, value);
    }

    public BattleTroopController Test_aController
    {
        get { return mController; }
    }
#endif

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnObjectHandlingDelegate<BattleTroop>
	private bool _OnFlashTargetingEnemyObserverDummy(BattleTroop pTroop, int pSqrObjectDistance)
	{
		if ((pTroop.aTeamIdx > 0) && // 중립팀 제외
			!aIsDetecteds[pTroop.aTeamIdx])
		{
			aBattleGrid.SpawnObserverDummy(aCoord, aDirectionY, mFlashTargetingDetectionRange, pTroop.aTeamIdx);

			bool lIsDetectedToAllEnemys = true;
			for (int iTeam = 1; iTeam < aBattleGrid.aTeamCount; ++iTeam) // 중립팀 제외
				if (!aIsDetecteds[iTeam])
					lIsDetectedToAllEnemys = false;

			if (lIsDetectedToAllEnemys)
				return false; // 처리 중단
		}

		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mTroopGridIdx;
	private TroopSpec mTroopSpec;
	private TroopLevelSpec mLevelSpec;
	private int mLevel;
	private VeterancySpec mVeterancySpec;
	private int mDefaultDirectionY;
	private BattleCommander mCommander;
	private int mTroopSlotIdx;
	private GuidLink<BattleTroop> mPrecedingTroopLink;
	private BattleUser mUser;

	private BattleTroopStat mStat;
	private BattleTroopRecord mRecord;
	private BattleTroopMoveUpdater mMoveUpdater;
	private LinkedListNode<BattleTroop> mOccupationNode;
	private BattleTroopController mController;
	private BattleTroopAttackControl mBodyAttackControl;
	private BattleTroopAttackControl mTurretAttackControl;
	private BattleTroopRotationUpdater mBodyRotationUpdater;
	private BattleTroopRotationUpdater mBodyWeaponRotationUpdater;
	private BattleTroopRotationUpdater mTurretRotationUpdater;

	private TroopDefenseType mDefenseType;
	private GroundSpec mAffectedGroundSpec;
	private GroundStatTable mAffectedGroundStatTable;
	private BattleBarricade mCoveringBarricade;

	private GuidObjectChainItem<BattleTroop> mTroopChainItem;
	private GuidObjectChainItem<BattleObject> mDetectionObjectChainItem;

	private LinkedList<BattleCommander> mExplicitTargetingCommanderList;
	private BattleTroopTargetSearchComparator mTargetSearchComparator;

	private bool mIsLogControl;

	private Coord2 mDirectionVector;

	private ControlState mControlState;
	private bool mIsFallBehind;
	private bool mIsObserver;
	private bool mIsNpc;
	private bool mIsNoDamage;
	private bool mIsTargetable;
	private bool mIsHiding;
	private bool mIsWounded;
	private int mHpPercent;
	private uint mLastSkillEffectGuid;
	private int mBattleTimer;

	private BattleTroopCommand mCommand;
	private BattleTroopSkillProcess mSkillProcess;
	private BattleTroopInteraction mInteraction;
	private BattleTroopActionQueue mActionQueue;

	private IStageTroop mStageTroop;

	private GuidLink<BattleTargetingDummy> mTargetingDummyLink;
	private int mFlashTargetingDetectionRange;
}
