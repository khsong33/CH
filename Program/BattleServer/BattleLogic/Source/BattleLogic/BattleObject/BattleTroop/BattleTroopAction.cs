using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopAction : PoolingObject
{
	private delegate void OnStartActionDelegate(BattleTroopAction pThis, int pStartDelta);
	private delegate void OnStopActionDelegate(BattleTroopAction pThis);
	private delegate bool OnUpdateActionDelegate(BattleTroopAction pThis, int pUpdateDelta, out int oExtraUpdateDelta);

	private class SelfRecoveryInfo
	{
		public int mRecoveryEffectDelay;
		public int mRecoveryActionDelay;
		public int mRecoveryRatio;

		public int mRecoveryEffectTimer;
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 액션을 생성한 스킬 효과의 번호
	public uint aActionSkillEffectGuid
	{
		get { return mActionSkillEffectGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 타입
	public TroopActionType aActionType
	{
		get { return mActionType; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleTroopAction<{0}> of {1}", mActionType, mControlTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopAction()
	{
		if (!sIsSetUpProcessFunctionDelegates)
		{
			_SetUpProcessFunctionDelegates();
			sIsSetUpProcessFunctionDelegates = true;
		}

		mActionType = TroopActionType.None;
	}
	//-------------------------------------------------------------------------------------------------------
	// PoolingObject 메서드
	public override void Reset()
	{
		if (sOnStopActionDelegates[(int)mActionType] != null)
			sOnStopActionDelegates[(int)mActionType](this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 자가 회복을 초기화합니다.
	public void InitSelfRecovery(BattleTroopController pController, uint pActionSkillEffectGuid, int pRecoveryEffectDelay, int pRecoveryActionDelay, int pRecoveryRatio)
	{
		mController = pController;
		mControlTroop = pController.aControlTroop;

		mActionSkillEffectGuid = pActionSkillEffectGuid;
		mActionType = TroopActionType.SelfRecovery;

		if (mSelfRecoveryInfo == null)
			mSelfRecoveryInfo = new SelfRecoveryInfo();

		mSelfRecoveryInfo.mRecoveryEffectDelay = pRecoveryEffectDelay;
		mSelfRecoveryInfo.mRecoveryActionDelay = pRecoveryActionDelay;
		mSelfRecoveryInfo.mRecoveryRatio = pRecoveryRatio;
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션을 시작합니다.
	public void StartAction(int pStartDelta)
	{
		//Debug.Log("ProcessAction() mActionType=" + mActionType + " - " + pTroopController.aControlTroop);

		if (sOnStartActionDelegates[(int)mActionType] != null)
			sOnStartActionDelegates[(int)mActionType](this, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션을 갱신합니다.
	public bool UpdateAction(int pUpdateDelta, out int lExtraUpdateDelta)
	{
		if (sOnUpdateActionDelegates[(int)mActionType] != null)
		{
			if (!sOnUpdateActionDelegates[(int)mActionType](this, pUpdateDelta, out lExtraUpdateDelta))
			{
				if (sOnStopActionDelegates[(int)mActionType] != null)
					sOnStopActionDelegates[(int)mActionType](this);
				return false; // 갱신 완료
			}
		}
		else
		{
			lExtraUpdateDelta = pUpdateDelta;

			if (sOnStopActionDelegates[(int)mActionType] != null)
				sOnStopActionDelegates[(int)mActionType](this);
			return false; // 갱신 불필요
		}

		return true; // 갱신중
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 처리 함수 델리게이트 테이블을 초기화합니다.
	private static void _SetUpProcessFunctionDelegates()
	{
		DebugUtil.Assert(TroopSpec.cTroopActionTypeCount == Enum.GetValues(typeof(TroopActionType)).Length, "cTroopActionTypeCount mismatch");

		sOnStartActionDelegates = new OnStartActionDelegate[TroopSpec.cTroopActionTypeCount];
		sOnStartActionDelegates[(int)TroopActionType.None] = null;
		sOnStartActionDelegates[(int)TroopActionType.SelfRecovery] = _OnStartAction_SelfRecovery;

		sOnStopActionDelegates = new OnStopActionDelegate[TroopSpec.cTroopActionTypeCount];
		sOnStopActionDelegates[(int)TroopActionType.None] = null;
		sOnStopActionDelegates[(int)TroopActionType.SelfRecovery] = null;//_OnStopAction_SelfRecovery;

		sOnUpdateActionDelegates = new OnUpdateActionDelegate[TroopSpec.cTroopActionTypeCount];
		sOnUpdateActionDelegates[(int)TroopActionType.None] = null;
		sOnUpdateActionDelegates[(int)TroopActionType.SelfRecovery] = _OnUpdateAction_SelfRecovery;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartActionDelegate
	private static void _OnStartAction_SelfRecovery(BattleTroopAction pThis, int pStartDelta)
	{
		//Debug.Log("_OnStartAction_SelfRecovery() - " + pThis);

		if (pThis.aControlTroop.aHpPercent >= 100)
			return; // 자가 회복 불필요

		SelfRecoveryInfo lSelfRecoveryInfo = pThis.mSelfRecoveryInfo;

		pThis.mController.StartActionControl(TroopActionType.SelfRecovery, lSelfRecoveryInfo.mRecoveryActionDelay, pStartDelta);

		lSelfRecoveryInfo.mRecoveryEffectTimer = lSelfRecoveryInfo.mRecoveryEffectDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopActionDelegate
// 	private static void _OnStopAction_SelfRecovery(BattleTroopAction pThis)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateActionDelegate
	private static bool _OnUpdateAction_SelfRecovery(BattleTroopAction pThis, int pUpdateDelta, out int oExtraUpdateDelta)
	{
		//Debug.Log("_OnUpdateAction_SelfRecovery() - " + pThis);

		SelfRecoveryInfo lSelfRecoveryInfo = pThis.mSelfRecoveryInfo;

		if (lSelfRecoveryInfo.mRecoveryEffectTimer > 0)
		{
			lSelfRecoveryInfo.mRecoveryEffectTimer -= pUpdateDelta;
			if (lSelfRecoveryInfo.mRecoveryEffectTimer <= 0)
				oExtraUpdateDelta = -lSelfRecoveryInfo.mRecoveryEffectTimer;
			else
				oExtraUpdateDelta = 0;

			int lRecoveryDelta = pUpdateDelta - oExtraUpdateDelta;
			int lRecoveryMilliHp = (int)((Int64)pThis.mControlTroop.aStat.aMaxMilliHp * lSelfRecoveryInfo.mRecoveryRatio * lRecoveryDelta / (1000 * 10000));
			//Debug.Log("lRecoveryMilliHp=" + lRecoveryMilliHp + " - " + pThis.mControlTroop);
			pThis.mControlTroop.UpdateHp(TroopHpChangeType.Recover, lRecoveryMilliHp);
		}
		else
		{
			oExtraUpdateDelta = 0;
		}

		return (oExtraUpdateDelta <= 0);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static bool sIsSetUpProcessFunctionDelegates = false;
	private static OnStartActionDelegate[] sOnStartActionDelegates;
	private static OnStopActionDelegate[] sOnStopActionDelegates;
	private static OnUpdateActionDelegate[] sOnUpdateActionDelegates;

	private BattleTroopController mController;
	private BattleTroop mControlTroop;

	private uint mActionSkillEffectGuid;
	private TroopActionType mActionType;
	private SelfRecoveryInfo mSelfRecoveryInfo;
}
