using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopActionQueue
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션중 여부
	public bool aIsValid
	{
		get { return ((mCurrentAction != null) || (mTroopActionPoolingQueue.Count > 0)); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 액션
	public BattleTroopAction aCurrentAction
	{
		get { return mCurrentAction; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopActionQueue()
	{
		mTroopActionPoolingQueue = new Queue<BattleTroopAction>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		mController = pController;
		mControlTroop = pController.aControlTroop;

		mCurrentAction = null;
		Empty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 큐를 비웁니다.
	public void Empty()
	{
		if (mCurrentAction != null)
		{
			mCurrentAction.Dispose();
			mCurrentAction = null;
		}
			
		mTroopActionPoolingQueue.Clear();	
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션을 추가합니다.
	public void PushSelfRecoveryAction(int pRecoveryEffectDelay, int pRecoveryActionDelay, int pRecoveryRatio, uint pActionSkillEffectGuid)
	{
		if ((mCurrentAction != null) &&
			(mCurrentAction.aActionSkillEffectGuid == pActionSkillEffectGuid))
			return; // 이미 수행중인 액션
		foreach (BattleTroopAction iAction in mTroopActionPoolingQueue)
		{
			if (iAction.aActionSkillEffectGuid == pActionSkillEffectGuid)
				return; // 이미 예약된 액션
		}

		BattleTroopAction lTroopAction = mControlTroop.aBattleLogic.AllocateTroopAction();
		lTroopAction.InitSelfRecovery(mController, pActionSkillEffectGuid, pRecoveryEffectDelay, pRecoveryActionDelay, pRecoveryRatio);

		mTroopActionPoolingQueue.Enqueue(lTroopAction);
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 큐를 갱신합니다.
	public void UpdateFrame(int pUpdateDelta)
	{
		int lExtraUpdateDelta;
		if (mCurrentAction != null)
		{
			if (!mCurrentAction.UpdateAction(pUpdateDelta, out lExtraUpdateDelta))
				mCurrentAction = null;
		}
		else
			lExtraUpdateDelta = pUpdateDelta;

		if ((mCurrentAction == null) &&
			(mTroopActionPoolingQueue.Count > 0) &&
			mController.aActiveControl.aIsActionAvailable)
		{
			mCurrentAction = mTroopActionPoolingQueue.Dequeue();
			mCurrentAction.StartAction(lExtraUpdateDelta);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopController mController;
	private BattleTroop mControlTroop;

	private Queue<BattleTroopAction> mTroopActionPoolingQueue;
	private BattleTroopAction mCurrentAction;
}
