using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopActionControl : BattleTroopControl
{
	public class StartParam
	{
		internal TroopActionType mActionType;
		internal int mActionDelay;

		internal StartParam()
		{
		}

		public void Set(TroopActionType pActionType, int pActionDelay)
		{
			mActionType = pActionType;
			mActionDelay = pActionDelay;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Action"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 타입
	public TroopActionType aTroopActionType
	{
		get { return mActionType; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopActionControl()
	{
		mStartParam = new StartParam();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		SetController(pController);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (aControlTroopCommander == null)
			Debug.LogError("can't start control : aControlTroopCommander is null - " + this);

		if (!base.Start())
			return false;

		mActionType = mStartParam.mActionType;
		mActionTimer = mStartParam.mActionDelay;
		switch (mActionType)
		{
		case TroopActionType.SelfRecovery:
			_SetOnFrameUpdateDelegate(_OnStart_SelfRecovery(), 0);
			break;
		}

		if (mOnFrameUpdateDelegate == null)
			return false; // 액션 없음

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool Stop(int pStopTime, int pStopDelta)
	{
		if (!base.Stop(pStopTime, pStopDelta))
			return false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		// 액션 시간이 끝나기를 기다립니다.
		mActionTimer -= pUpdateDelta;
		if (mActionTimer <= 0)
		{
			// 스테이지에 보입니다.
			aControlTroop.aStageTroop.StopAction();

			// 지휘관이 가장 최근에 내린 컨트롤을 시작합니다.
			aControlTroop.aCommand.StartLastCommand(-mActionTimer);
			return false;
		}

		// 액션 중이면 액션 갱신을 합니다.
		return mOnFrameUpdateDelegate(pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수를 지정하고 갱신합니다.
	private bool _SetOnFrameUpdateDelegate(OnFrameUpdateDelegate pOnFrameUpdateDelegate, int pUpdateDelta)
	{
		mOnFrameUpdateDelegate = pOnFrameUpdateDelegate;

		if (pUpdateDelta > 0)
			return mOnFrameUpdateDelegate(pUpdateDelta); // 갱신 진행

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_SelfRecovery()
	{
		// 스테이지에 보입니다.
		aControlTroop.aStageTroop.StartAction(TroopActionType.SelfRecovery);

		return _OnFrameUpdate_SelfRecovery;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private bool _OnFrameUpdate_SelfRecovery(int pUpdateDelta)
	{
		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;

	private OnFrameUpdateDelegate mOnFrameUpdateDelegate;

	private TroopActionType mActionType;
	private int mActionTimer;
}
