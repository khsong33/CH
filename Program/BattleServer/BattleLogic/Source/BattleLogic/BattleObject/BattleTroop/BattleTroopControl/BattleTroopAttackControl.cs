using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopAttackControl : BattleTroopControl, IBattleTroopRotater
{
	public enum ControlWeapon
	{
		BodyWeapon,
		TurretWeapon,
	}

	public class StartParam
	{
		internal BattleTroopTargetingInfo.TargetingType mTargetingType;
		internal GuidLink<BattleTroop> mTargetTroopLink;
		internal Coord2 mTargetCoord;
		internal bool mIsIgnoreRange;

		internal StartParam()
		{
			mTargetTroopLink = new GuidLink<BattleTroop>();
		}

		public void SetTargetTroopAttack(BattleTroop pTargetTroop, bool pIsIgnoreRange)
		{
			mTargetingType = BattleTroopTargetingInfo.TargetingType.Troop;
			mTargetTroopLink.Set(pTargetTroop);
			mTargetCoord = pTargetTroop.aCoord;
			mIsIgnoreRange = pIsIgnoreRange;
		}
		public void SetTargetCoordAttack(Coord2 pTargetCoord, bool pIsIgnoreRange)
		{
			mTargetingType = BattleTroopTargetingInfo.TargetingType.Coord;
			mTargetTroopLink.Set(null);
			mTargetCoord = pTargetCoord;
			mIsIgnoreRange = pIsIgnoreRange;
		}
	}

	public const int cIdleSearchDelay = 1000;
	public const int cSetOnFrameUpdateCallWarningCount = 8;
	public const int cSetOnFrameUpdateCallBreakCount = 12;
	
	public const String cIdleText = "Idle";
	public const String cRotateText = "Rotate";
	public const String cSetUpRotationText = "SetUpRotation";
	public const String cSetUpInstallText = "SetUpInstall";
	public const String cInstallText = "Install";
	public const String cUnInstallText = "UnInstall";
	public const String cTrackingText = "Tracking";
	public const String cWindupText = "Windup";
	public const String cFireText = "Fire";
	public const String cCooldownText = "Cooldown";
	public const String cTargetResetText = "TargetReset";

	public const String cAttackHoldPrefix = "Attack Hold ";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IBattleTroopRotater 속성
	public int aRotationDirectionY
	{
		get { return mAttackDirectionY; }
		set
		{
			mAttackDirectionY = value;

			// 터렛일 경우 회전 상태를 보입니다.
			if (mControlWeapon == ControlWeapon.TurretWeapon)
				aControlTroop.aStageTroop.RotateTurretToDirectionY(value);
			else
				aControlTroop.aStageTroop.RotateBodyWeaponToDirectionY(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Attack"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return (aControlTroop.aIsWounded || !mIsAttacking); }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return !mIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 컨트롤의 타게팅 정보
	public BattleTroopTargetingInfo aTargetingInfo
	{
		get { return mTargetingInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 공격을 진행중인지 여부
	public bool aIsAttacking
	{
		get { return mIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표 타격일 경우의 반복 공격 회수
	public int aTargetCoordAttackCounter
	{
		get { return mTargetCoordAttackCounter; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopAttackControl()
	{
		mStartParam = new StartParam();

		mNextTargetTroopLink = new GuidLink<BattleTroop>();
		mInteractionTargetTroopLink = new GuidLink<BattleTroop>();

		mTargetingInfo = new BattleTroopTargetingInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController, ControlWeapon pControlWeapon)
	{
		mNextTargetTroopLink.Set(null);
		mInteractionTargetTroopLink.Set(null);
		mIsGoToInteraction = false;

		SetController(pController);

		mControlWeapon = pControlWeapon;

		switch (pControlWeapon)
		{
		case ControlWeapon.BodyWeapon:
			{
				mFacingRotationUpdater = aControlTroop.aBodyRotationUpdater; // 아이들 상태에서는 몸통 자체의 회전 갱신자를
				mTrackingRotationUpdater = aControlTroop.aBodyWeaponRotationUpdater; // 기본은 몸통 무기 회전 갱신자를
				mOnStartIdleDelegate = _OnStart_Idle;
				mOnFrameUpdateIdleDelegate = _OnFrameUpdate_Idle;
			}
			break;
		case ControlWeapon.TurretWeapon:
			{
				mFacingRotationUpdater = aControlTroop.aTurretRotationUpdater;
				mTrackingRotationUpdater = mFacingRotationUpdater;
				mOnStartIdleDelegate = _OnStart_Idle;
				mOnFrameUpdateIdleDelegate = _OnFrameUpdate_Idle;
			}
			break;
		}
		mAttackDirectionY = aControlTroop.aDirectionY;

		mTargetingInfo.Init(aControlTroop, mControlWeapon);

		mIsCooldownActionWeapon = (mControlWeapon == ControlWeapon.TurretWeapon); // 터렛은 쿨다운 동안에 움직일 수 있음
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		//Debug.Log("Start() pTargetTroop=" + pTargetTroop + " - " + this);

		if (!base.Start())
			return false;

		return _OnStart(0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 처리 함수(private 함수지만 흐름 파악상 이곳에 배치)
	private bool _OnStart(int pUpdateDelta)
	{
		//Debug.Log("_OnStart()" + this);

		if (mControlWeapon == ControlWeapon.BodyWeapon) // 몸체라면
			aControlTroop.aControlState.mIsAttackMoving = true; // 현재 상태를 공격 이동으로 간주

		BattleTroop lTargetTroop = mStartParam.mTargetTroopLink.Get();
		mTargetLockFrameTime = 0;

		mOnFrameUpdateDelegate = null;

		mTargetCoordAttackCounter = 0;
		mIsSettingUp = false;
		mIsAttacking = false;
		mIsBurstFiring = false;
		mCooldownTimer = 0;

		mSetOnFrameUpdateCallCounter = 0;

// 		aControlTroopCommander.aStatus.OnAttackEnemy(lTargetTroop);

		if (mTargetingInfo.aIsValid &&
			!aControlTroopStat.aTroopStatTable.aIsAttackDisabled) // 공격 가능하다면
		{
			// 공격을 초기화합니다.
			_SetTarget(mStartParam.mTargetingType, mStartParam.mTargetCoord, lTargetTroop, true);

			switch (mControlWeapon)
			{
			case ControlWeapon.BodyWeapon:
				mIsTargetingRotatable = aControlTroop.aIsPrimaryAttackBody && !aControlTroop.aControlState.mIsSetUpMode; // 터렛 몸통이 아니고 셋업 모드가 아니라면 타겟을 향해 회전 가능
				break;
			case ControlWeapon.TurretWeapon:
				mIsTargetingRotatable = true; // 터렛은 항상 타겟을 향해 회전 가능
				break;
			}

			if (!aControlTroop.aControlState.mIsSetUpMode)
				return _SetOnFrameUpdateDelegate(_InitAttack(pUpdateDelta), pUpdateDelta);
			else
				return _SetOnFrameUpdateDelegate(_OnStart_SetUpRotation(), pUpdateDelta); // 셋업 방향으로 회전부터 시작
		}
		else // 무기가 없는 몸통처럼 공격이 불가능한데 공격 컨트롤을 시작하는 경우라면
		{
			// 아이들 상태를 시작합니다.
			return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool Stop(int pStopTime, int pStopDelta)
	{
		if (!base.Stop(pStopTime, pStopDelta))
			return false;

		aControlTroop.aControlState.mIsAttackMoving = false; // 공격 이동 상태 해제

		return _SetOnFrameUpdateDelegate(mOnFrameUpdateIdleDelegate, 0); // 이후로 aIsAttacking가 항상 false가 됨
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		// 프레임 초기화를 합니다.
		mFrameUpdateTime = pUpdateTime;
		mFrameUpdateDelta = pUpdateDelta;
		mSetOnFrameUpdateCallCounter = 0;

		// 공격 상태에서의 전환 검사를 합니다.
		if (mOnFrameUpdateDelegate != mOnFrameUpdateIdleDelegate)
		{
			// 공격 불가거나 핀 다운 상태에서는 아이들 상태가 됩니다.
			if ((aControlTroopStat.aMoraleState == TroopMoraleState.PinDown) ||
				aControlTroopStat.aTroopStatTable.aIsAttackDisabled)
				return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta);

			// 타게팅 중에 적이 사라질 때의 처리를 합니다.
			if (mTargetingInfo.aTargetingType == BattleTroopTargetingInfo.TargetingType.Troop)
			{
				if (((mTargetingInfo.aTargetTroop == null) || !mTargetingInfo.aTargetTroop.aIsDetecteds[aControlTroop.aTeamIdx]) && // 적이 사라졌거나 안보이고
					!mIsSettingUp) // 세팅 중이 아니라면
				{
					if (!aControlTroop.aControlState.mIsSetUpMode)
						return _SetOnFrameUpdateDelegate(_OnStart_TargetTroopReset(_InitNextTargetTroop(pUpdateTime)), pUpdateDelta); // 타게팅 초기화를 시작합니다.
					else
						return _SetOnFrameUpdateDelegate(_OnStart_SetUpRotation(), pUpdateDelta); // 셋업 방향으로 회전부터 시작
				}
			}
		}

		// 매 프레임마다 지휘관의 교전 타이머를 초기화합니다.
		if (aIsAttacking)
			aControlTroop.SetBattleTimer();

// 		aControlTroopCommander.aStatus.OnAttackEnemy(null);

		// 현재 상태의 갱신 델리게이트를 실행합니다.
		return mOnFrameUpdateDelegate(pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnStopUpdate(int pStopTime, int pStopDelta, out int oExtraUpdateDelta)
	{
		if (aController.aIsForcedReservedControl)
		{
			oExtraUpdateDelta = pStopDelta;
			return true; // 공격 즉시 중단
		}

		// 쿨다운이 끝났는지 검사합니다.
		if (mCooldownTimer > 0)
		{
			if (!_ProceedCooldown(pStopDelta, out pStopDelta, false)) // 터렛도 멈춰서 쿨다운을 기다림
			{
				oExtraUpdateDelta = 0;
				return false; // 중단 유보
			}
		}

		// 설치 해제를 검사합니다.
		if (mTargetingInfo.aIsInstalled)
		{
			aControlTroop.aStageTroop.DebugLog(cUnInstallText);

			mTargetingInfo.aIsInstalled = false;

			mSetUpTimer = aControlTroopStat.aTroopStatTable.Get(TroopStat.SetUpDelay);

			// 철거 상태를 보입니다.
			if (mTargetingInfo.aSelectedWeaponData != null)
				aControlTroop.aStageTroop.UnInstallWeapon(
					mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
					mSetUpTimer);
			else
				Debug.LogWarning("mTargetingInfo.aSelectedWeaponData is null - " + this);

			if (mSetUpTimer <= 0)
			{
				oExtraUpdateDelta = -mSetUpTimer;
				return true; // 해체 바로 끝남
			}
			// else mSetUpTimer > 0인 경우의 처리는 아래로 넘어감
		}
		if (mSetUpTimer > 0)
		{
			mSetUpTimer -= pStopDelta;
			if (mSetUpTimer <= 0)
			{
				oExtraUpdateDelta = -mSetUpTimer;
				return true; // 해체 끝(혹은 불필요)
			}
			else
			{
				oExtraUpdateDelta = 0;
				return false; // 해체 중
			}
		}
		else
		{
			oExtraUpdateDelta = pStopDelta;
			return true; // 공격 중단
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 재지정합니다.
	public void ResetTargetTroop(BattleTroop pTargetTroop, bool pIsIgnoreRange, int pUpdateDelta)
	{
		mStartParam.SetTargetTroopAttack(pTargetTroop, pIsIgnoreRange);
		_OnStart(pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표를 재지정합니다.
	public void ResetTargetCoord(Coord2 pTargetCoord, bool pIsIgnoreRange, int pUpdateDelta)
	{
		mStartParam.SetTargetCoordAttack(pTargetCoord, pIsIgnoreRange);
		_OnStart(pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수를 지정하고 갱신합니다.
	private bool _SetOnFrameUpdateDelegate(OnFrameUpdateDelegate pOnFrameUpdateDelegate, int pUpdateDelta)
	{
		//if (mControlWeapon == ControlWeapon.TurretWeapon)
		//	mControlWeapon = ControlWeapon.TurretWeapon;

		if (pOnFrameUpdateDelegate == null) // 정의되지 않은 델리게이트의 경우
		{
			if (mOnFrameUpdateDelegate != mOnFrameUpdateIdleDelegate)
				pOnFrameUpdateDelegate = mOnStartIdleDelegate(); // 아이들 상태가 아니었다면 아이들 상태를 시작
			else
				pOnFrameUpdateDelegate = mOnFrameUpdateIdleDelegate; // 혹은 기존 아이들 상태 지속
		}

		bool lIsToAttacking = (pOnFrameUpdateDelegate != mOnFrameUpdateIdleDelegate); // 지정 상태가 아이들 상태가 아니라면 공격으로의 전환으로 간주
		if (mIsAttacking != lIsToAttacking) // 공격 상태 전환이 있다면
		{
			if (lIsToAttacking)
			{
				if (!mIsSettingUp) // 세팅 상태가 아닐 경우에
				{
					mIsAttacking = true; // 공격 상태 켬
					aController.UpdateAttackingState();
				}
			}
			else
			{
				mIsAttacking = false; // 공격 상태 끔
				_ResetTarget();
				aController.UpdateAttackingState();
			}
		}

		mOnFrameUpdateDelegate = pOnFrameUpdateDelegate;

		if (++mSetOnFrameUpdateCallCounter >= cSetOnFrameUpdateCallWarningCount)
		{
			if (mSetOnFrameUpdateCallCounter < cSetOnFrameUpdateCallBreakCount)
			{
				Debug.LogWarning("mSetOnFrameUpdateCallCounter:" + mSetOnFrameUpdateCallCounter + " overhead detected - " + this);
			}
			else
			{
				Debug.LogError("mSetOnFrameUpdateCallCounter:" + mSetOnFrameUpdateCallCounter + " overhead detected and stopped - " + this);
				return true; // 과도한 재귀 호출로 중단
			}
		}

		if (pUpdateDelta > 0)
			return mOnFrameUpdateDelegate(pUpdateDelta); // 갱신 진행

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 상태를 초기화합니다.
	private OnFrameUpdateDelegate _InitAttack(int pUpdateDelta)
	{
		//aControlTroop.aDefaultDirectionY = mTargetingInfo.aTargetDirectionY;

		if (!mIsBurstFiring) // 연속 공격에 의한 회전이 없다면
		{
			if (!aControlTroop.aControlState.mIsSetUpMode)
				mTargetingInfo.UpdateAimingDirectionY(mAttackDirectionY); // 현재 위치로 방향각을 갱신합니다.
			else
				mTargetingInfo.UpdateAimingDirectionY(aControlTroop.aDefaultDirectionY); // 기본 방향으로 방향각을 갱신합니다.
		}

		if (!aControlTroop.aInteraction.aIsAttackable) // 공격 불가 상호 작용 상태라면
			return null; // 아이들 상태로
		if (!mStartParam.mIsIgnoreRange &&
			!mTargetingInfo.CheckInAttackRange()) // 사거리 밖이라면
			return null; // 아이들 상태로

		if (!mIsBurstFiring)
		{
			int lSelectionTime = mFrameUpdateTime + (mFrameUpdateDelta - pUpdateDelta);

			// 일반 공격이라면 타겟과의 각도 등을 고려해서 최적의 무기를 찾아 상태를 변경합니다.
			if (mTargetingInfo.SelectBestWeapon(BattleTroopTargetingInfo.TargetingAngle.FireConeAngle, mStartParam.mIsIgnoreRange, lSelectionTime)) // 발사각 안에 적이 있다면
			{
				if (!mIsTargetingRotatable && // 타겟을 향해 회전이 가능하지 않은데
					!MathUtil.CheckEqualDegree(aControlTroop.aDirectionY, aControlTroop.aDefaultDirectionY)) // 기본 방향을 향하고 있지 않다면
				{
					return null; // 일단은 아이들 상태로(아이들 상태에서 기본 방향을 향하게 됨)
				}

				if (!mTargetingInfo.aIsInstalled) // 설치가 안 된 상태라면
				{
					return _OnStart_Install(); // 설치합니다.
				}
				else // 설치된 상태라면
				{
					mAttackDirectionY = mTargetingInfo.aTargetDirectionY; // 타겟 방향각을 바로 공격 회전각으로 지정
					return _OnStart_Windup(); // 바로 와인드업으로 넘어갑니다.
				}
			}
			else // 발사각 안에 적이 없다면
			{
				if (mTargetingInfo.SelectBestWeapon(BattleTroopTargetingInfo.TargetingAngle.TrackingAngle, mStartParam.mIsIgnoreRange, lSelectionTime)) // 시야각 안에 적이 있다면
				{
					if (!mTargetingInfo.aIsInstalled) // 설치가 안 된 상태라면
					{
						if (mIsTargetingRotatable) // 회전 가능하다면
							return _OnStart_Rotate(); // 회전 이동을 합니다(설치 전에 최대한 적을 향하고 나서 설치합니다).
						else // 회전 불가라면
							return _OnStart_Install(); // 현재 방향에서 설치합니다.
					}
					else
					{
						return _OnStart_Tracking(); // 트래킹을 합니다.
					}
				}
				else // 시야각 안에 적이 없고
				{
					if (!mIsTargetingRotatable) // 타겟을 향해 회전이 가능하지 않다면
						return null; // 아이들 상태로

					if (mTargetingInfo.SelectBestWeapon(BattleTroopTargetingInfo.TargetingAngle.None, mStartParam.mIsIgnoreRange, lSelectionTime)) // 시야각 관계 없이 쏠 수 있는 적이 있다면
					{
						if (mTargetingInfo.aIsInstalled) // 설치된 상태라면
							return _OnStart_UnInstall(); // 회전 이동을 위해 철거합니다.
						else // 설치가 안 된 상태라면
							return _OnStart_Rotate(); // 회전 이동을 합니다.
					}
					else // 아무 적도 없다면
					{
						return null; // 아이들 상태로
					}
				}
			}
		}
		else
		{
			// 연속 공격중이라면 현재 선택된 무기를 바꾸지 않고 상태만 변경합니다.
			if (!mTargetingInfo.aIsAllyTargeting) // 적 대상이라면
			{
				if (mTargetingInfo.aTargetAngleDistance <= mTargetingInfo.aSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.FireConeAngleHalf))
				{
					return _OnStart_Windup(); // 바로 와인드업으로 넘어갑니다.
				}
				else
				{
					return _OnStart_Tracking(); // 트래킹을 합니다.
				}
			}
			else // 아군 대상이라면
			{
				return _OnStart_Windup(); // 바로 와인드업으로 넘어갑니다.
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Idle()
	{
		aControlTroop.aStageTroop.DebugLog(cIdleText);

		mIdleSearchTimer = 0;

		int lIdleDirectionY;
		switch (mControlWeapon)
		{
		case ControlWeapon.BodyWeapon:
			lIdleDirectionY = aControlTroop.aDefaultDirectionY;
			break;
		case ControlWeapon.TurretWeapon:
		default:
			lIdleDirectionY = aControlTroop.aDirectionY;
			break;
		}

		mFacingRotationUpdater.SetDstRotation(
			lIdleDirectionY,
			mTargetingInfo.aMaxRotationDelay,
			false); // 적을 공격 못한다면 기본 방향을 향함

		return _OnFrameUpdate_Idle;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Idle(int pUpdateDelta)
	{
		// 쿨다운이 끝났는지 검사합니다.
		if (mCooldownTimer > 0)
		{
			if (!_ProceedCooldown(pUpdateDelta, out pUpdateDelta, mIsCooldownActionWeapon)) // 터렛은 동시 진행 가능
				return true;
		}

		// 무기별 상태 변화 처리를 합니다.
		switch (mControlWeapon)
		{
		case ControlWeapon.BodyWeapon:
			{
				if (!mFacingRotationUpdater.aIsRotating)
				{
					if (!MathUtil.CheckEqualDegree(mFacingRotationUpdater.aRotater.aRotationDirectionY, aControlTroop.aDefaultDirectionY))
					{
						mFacingRotationUpdater.SetDstRotation(
							aControlTroop.aDefaultDirectionY,
							mTargetingInfo.aMaxRotationDelay,
							false); // 적을 공격 못한다면 기본 방향을 향함
					}
					else
					{
						if (!aControlTroop.aIsTurretAttacking && // 터렛이 공격중이 아니라면 아무 적도 없는 상태이므로
							!aControlTroop.aControlState.mIsSetUpMode) // 셋업 모드가 아니라면
						{
							aController.StartSearchControl(
								BattleTroopSearchControl.RetryOption.RetrySomeTimes,
								BattleTroopSearchControl.MoveOption.HoldPosition,
								pUpdateDelta); // 탐색으로 변경
							return false;
						}
					}
				}
			}
			break;
		case ControlWeapon.TurretWeapon:
			{
				if (!mFacingRotationUpdater.aIsRotating &&
					!MathUtil.CheckEqualDegree(mFacingRotationUpdater.aRotater.aRotationDirectionY, aControlTroop.aDirectionY))
				{
					mFacingRotationUpdater.SetTroopDirection(
						mTargetingInfo.aMaxRotationDelay,
						false); // 몸통 방향을 향함
				}
			}
			break;
		}

		// 회전을 하는 중이라면 갱신합니다.
		if (mFacingRotationUpdater.aIsRotating)
			mFacingRotationUpdater.UpdateRotation(pUpdateDelta);

		// 공격 가능하다면 주기적으로 적을 찾습니다.
		if (mTargetingInfo.aIsValid &&
			!aControlTroopStat.aTroopStatTable.aIsAttackDisabled)
		{
			// 탐색 주기가 끝나기를 기다립니다.
			if (mIdleSearchTimer > 0)
			{
				mIdleSearchTimer -= pUpdateDelta;
				return true;
			}
			mIdleSearchTimer += cIdleSearchDelay; // 타이머 오차 손실을 반영(+=)

			// 타겟을 찾습니다.
			IBattleTarget lTarget = aControlTroop.SearchTarget(null, mTargetingInfo, mIsTargetingRotatable);
			//Debug.Log("lEnemyTroop=" + lEnemyTroop + " - " + this);
			if (lTarget != null) // 타겟이 있다면
			{
				// 공격을 준비합니다.
				_SetTarget(BattleTroopTargetingInfo.TargetingType.Troop, lTarget.aTargetCoord, lTarget.aTargetTroop, true);
				return _SetOnFrameUpdateDelegate(_InitAttack(-mIdleSearchTimer), -mIdleSearchTimer);
			}
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Rotate()
	{
		aControlTroop.aStageTroop.DebugLog(cRotateText);

		// 방향 회전 세팅을 합니다.
		mFacingRotationUpdater.SetDstRotation(
			mTargetingInfo.aTargetDirectionY,
			mTargetingInfo.aMaxRotationDelay,
			false);

		return _OnFrameUpdate_Rotate;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Rotate(int pUpdateDelta)
	{
		// 쿨다운이 끝나기를 기다립니다.
		if (mCooldownTimer > 0)
		{
			if (!_ProceedCooldown(pUpdateDelta, out pUpdateDelta, mIsCooldownActionWeapon)) // 터렛이라면 동시 진행 가능
				return true;
		}

		// 방향 회전이 끝나기를 기다립니다.
		if (mTargetingInfo.CheckTargetUpdate() ||
			!MathUtil.CheckEqualDegree(mFacingRotationUpdater.aRotationEndDirectionY, mTargetingInfo.aTargetDirectionY))
			mFacingRotationUpdater.SetDstRotation(
				mTargetingInfo.aTargetDirectionY,
				mTargetingInfo.aMaxRotationDelay,
				false);
		mFacingRotationUpdater.UpdateRotation(pUpdateDelta);

		// 방향각을 갱신합니다.
		mTargetingInfo.UpdateAimingDirectionY(mAttackDirectionY);

		// 회전이 끝나기를 기다립니다.
		if (mFacingRotationUpdater.aIsRotating)
			return true;

		// 연사 공격이라서 쿨다운이 남아 있어도 이곳까지 올 수 있으므로 더이상은 진행시키지 않습니다.
		if (mCooldownTimer > 0)
			return true;

		// 회전 이동이 끝나면 설치로 넘어갑니다.
		return _SetOnFrameUpdateDelegate(_OnStart_Install(), mFacingRotationUpdater.aExtraUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_SetUpRotation()
	{
		aControlTroop.aStageTroop.DebugLog(cSetUpRotationText);

		mFacingRotationUpdater.SetDstRotation(
			aControlTroop.aDefaultDirectionY,
			aControlTroopStat.aTroopStatTable.Get(TroopStat.MaxRotationDelay),
			false); // 기본 방향을 향함

		mIsSettingUp = true; // 세팅 시작

		return _OnFrameUpdate_SetUpRotation; // 셋업 회전을 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_SetUpRotation(int pUpdateDelta)
	{
		mFacingRotationUpdater.UpdateRotation(pUpdateDelta);
		if (mFacingRotationUpdater.aIsRotating)
			return true;

		return _SetOnFrameUpdateDelegate(_OnStart_SetUpInstall(), mFacingRotationUpdater.aExtraUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_SetUpInstall()
	{
		aControlTroop.aStageTroop.DebugLog(cSetUpInstallText);

		mTargetingInfo.aIsInstalled = true;

		mSetUpTimer = aControlTroopStat.aTroopStatTable.Get(TroopStat.SetUpDelay);

		// 설치 상태를 보입니다.
		aControlTroop.aStageTroop.InstallWeapon(
			mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
			mTargetingInfo.aTargetCoord,
			mTargetingInfo.aTargetTroop,
			mSetUpTimer);

		return _OnFrameUpdate_SetUpInstall; // 설치를 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_SetUpInstall(int pUpdateDelta)
	{
		// 설치가 끝나기를 기다립니다.
		mSetUpTimer -= pUpdateDelta;
		if (mSetUpTimer > 0)
			return true;

		mIsSettingUp = false; // 세팅 완료

		// 셋업 설치가 끝나면 아이들 상태로 시작합니다.
		return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), -mSetUpTimer);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Install()
	{
		aControlTroop.aStageTroop.DebugLog(cInstallText);

		mTargetingInfo.aIsInstalled = true;

		mSetUpTimer = aControlTroopStat.aTroopStatTable.Get(TroopStat.SetUpDelay);

		// 설치 상태를 보입니다.
		aControlTroop.aStageTroop.InstallWeapon(
			mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
			mTargetingInfo.aTargetCoord,
			mTargetingInfo.aTargetTroop,
			mSetUpTimer);

		return _OnFrameUpdate_Install; // 설치를 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Install(int pUpdateDelta)
	{
		// 설치가 끝나기를 기다립니다.
		mSetUpTimer -= pUpdateDelta;
		if (mSetUpTimer > 0)
			return true;

		// 설치가 끝나면 조준으로 넘어갑니다.
		return _SetOnFrameUpdateDelegate(_OnStart_Tracking(), -mSetUpTimer);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_UnInstall()
	{
		aControlTroop.aStageTroop.DebugLog(cUnInstallText);

		mTargetingInfo.aIsInstalled = false;

		mSetUpTimer = aControlTroopStat.aTroopStatTable.Get(TroopStat.SetUpDelay);

		// 철거 상태를 보입니다.
		aControlTroop.aStageTroop.UnInstallWeapon(
			mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
			mSetUpTimer);

		return _OnFrameUpdate_UnInstall; // 철거를 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_UnInstall(int pUpdateDelta)
	{
		// 쿨다운이 끝났는지 검사합니다.
		if (mCooldownTimer > 0)
		{
			if (!_ProceedCooldown(pUpdateDelta, out pUpdateDelta, mIsCooldownActionWeapon)) // 터렛은 동시 진행 가능
				return true;
		}

		// 철거가 끝나기를 기다립니다.
		mSetUpTimer -= pUpdateDelta;
		if (mSetUpTimer > 0)
			return true;

		// 철거가 끝나면 회전 이동으로 넘어갑니다.
		return _SetOnFrameUpdateDelegate(_OnStart_Rotate(), -mSetUpTimer);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Tracking()
	{
		aControlTroop.aStageTroop.DebugLog(cTrackingText);

		// 트래킹 세팅을 합니다.
		mTrackingRotationUpdater.SetDstRotation(
			mTargetingInfo.aTargetDirectionY,
			mTargetingInfo.aMaxTrackingDelay,
			false);

		// 트래킹 상태를 보입니다.
		aControlTroop.aStageTroop.TrackWeapon(
			mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
			mTargetingInfo.aTargetCoord,
			mTargetingInfo.aTargetTroop,
			mTrackingRotationUpdater.aRotationDelay);

		return _OnFrameUpdate_Tracking;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Tracking(int pUpdateDelta)
	{
		// 쿨다운이 끝났는지 검사합니다.
		int lCooldownExtraUpdateDelta;
		if (mCooldownTimer > 0)
		{
			if (!mIsBurstFiring)
			{
				if (!_ProceedCooldown(pUpdateDelta, out pUpdateDelta, mIsCooldownActionWeapon)) // 터렛이라면 동시 진행 가능
					return true;
				lCooldownExtraUpdateDelta = pUpdateDelta;
			}
			else
			{
				_ProceedCooldown(pUpdateDelta, out lCooldownExtraUpdateDelta, false);
			}
		}
		else
		{
			lCooldownExtraUpdateDelta = pUpdateDelta;
		}

		// 회전이 끝나기를 기다립니다.
		int lFacingRotationExtraUpdateDelta;
		if (mFacingRotationUpdater != mTrackingRotationUpdater)
		{
			mFacingRotationUpdater.UpdateRotation(pUpdateDelta); // 방향 회전이 남아있다면 마저 합니다.
			lFacingRotationExtraUpdateDelta = mFacingRotationUpdater.aExtraUpdateDelta;
		}
		else
		{
			lFacingRotationExtraUpdateDelta = pUpdateDelta;
		}

		if (mTargetingInfo.CheckTargetUpdate() ||
			!MathUtil.CheckEqualDegree(mTrackingRotationUpdater.aRotationEndDirectionY, mTargetingInfo.aTargetDirectionY))
			mTrackingRotationUpdater.SetDstRotation(
				mTargetingInfo.aTargetDirectionY,
				mTargetingInfo.aMaxTrackingDelay,
				false);
		mTrackingRotationUpdater.UpdateRotation(pUpdateDelta);

		// 방향각을 갱신합니다.
		mTargetingInfo.UpdateAimingDirectionY(mAttackDirectionY);

		// 회전이 끝나기를 기다립니다.
		if (mFacingRotationUpdater.aIsRotating ||
			mTrackingRotationUpdater.aIsRotating)
		{
			// 연사 공격중이라면 쿨다운이 끝났을 때 발사합니다.
			if (mIsBurstFiring &&
				(mCooldownTimer <= 0))
			{
				//Debug.Log("mIsBurstFiring skips _OnFrameUpdate_Tracking()");
				int lTargetingRatio
					= mFacingRotationUpdater.aIsRotating
					? mFacingRotationUpdater.GetRotationCompletionRatio()
					: mTrackingRotationUpdater.GetRotationCompletionRatio();
				return _SetOnFrameUpdateDelegate(_OnStart_Fire(lTargetingRatio), lCooldownExtraUpdateDelta);
			}

			return true;
		}

		// 연사 공격이라서 쿨다운이 남아 있어도 이곳까지 올 수 있으므로 더이상은 진행시키지 않습니다.
		if (mCooldownTimer > 0)
			return true;

		// 조준이 끝나면 와인드업으로 넘어갑니다.
		int lRotationExtraUpdateDelta = Mathf.Min(lFacingRotationExtraUpdateDelta, mTrackingRotationUpdater.aExtraUpdateDelta);
		int lExtraUpdateDelta = Mathf.Min(lCooldownExtraUpdateDelta, lRotationExtraUpdateDelta);
		return _SetOnFrameUpdateDelegate(_OnStart_Windup(), lExtraUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Windup()
	{
		//Debug.Log("_OnStart_Windup()");

		// 연사 공격중이라면 와인드업을 건너뜁니다.
		if (mIsBurstFiring)
		{
			//Debug.Log("mIsBurstFiring skips _OnStart_Windup()");
			return _OnStart_Fire(10000); // 100% 조준(만분율)
		}

		// 와인드업 시작을 처리합니다.
		aControlTroop.aStageTroop.DebugLog(cWindupText);

		mWindupTimer = mTargetingInfo.GetWindupDelay();

		// 와인드업 상태를 보입니다.
		aControlTroop.aStageTroop.WindupWeapon(
			mTargetingInfo.aSelectedWeaponData.mWeaponSpec,
			mTargetingInfo.aTargetCoord,
			mTargetingInfo.aTargetTroop,
			mWindupTimer);

		return _OnFrameUpdate_Windup; // 와인드업을 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Windup(int pUpdateDelta)
	{
		// 쿨다운을 진행시키고 추가 갱신 시간을 얻습니다.
		int lCooldownExtraUpdateDelta;
		if (mCooldownTimer > 0)
			_ProceedCooldown(pUpdateDelta, out lCooldownExtraUpdateDelta, false); // 여기서는 터렛도 쿨다운을 기다립니다.
		else
			lCooldownExtraUpdateDelta = pUpdateDelta;

		// 회전을 진행시키고 추가 갱신 시간을 얻습니다.
		int lFacingRotationExtraUpdateDelta;
		if ((mFacingRotationUpdater != mTrackingRotationUpdater) &&
			mFacingRotationUpdater.aIsRotating)
		{
			mFacingRotationUpdater.UpdateRotation(pUpdateDelta); // 방향 회전이 남아있다면 마저 합니다.
			lFacingRotationExtraUpdateDelta = mFacingRotationUpdater.aExtraUpdateDelta;
		}
		else
		{
			lFacingRotationExtraUpdateDelta = pUpdateDelta;
		}

		if (mTargetingInfo.CheckTargetUpdate() ||
			!MathUtil.CheckEqualDegree(mTrackingRotationUpdater.aRotationEndDirectionY, mTargetingInfo.aTargetDirectionY))
			mTrackingRotationUpdater.SetDstRotation(
				mTargetingInfo.aTargetDirectionY,
				mTargetingInfo.aMaxTrackingDelay, // 트래킹 속도를 사용
				false);
		int lTrackingRotationExtraUpdateDelta;
		if (mTrackingRotationUpdater.aIsRotating)
		{
			mTrackingRotationUpdater.UpdateRotation(pUpdateDelta);
			lTrackingRotationExtraUpdateDelta = mTrackingRotationUpdater.aExtraUpdateDelta;
		}
		else
		{
			lTrackingRotationExtraUpdateDelta = pUpdateDelta;
		}

		// 와인드업을 진행시키고 추가 갱신 시간을 얻습니다.
		int lWindupExtraUpdateDelta;
		if (mWindupTimer > 0)
		{
			mWindupTimer -= pUpdateDelta;
			lWindupExtraUpdateDelta = -mWindupTimer;
		}
		else
			lWindupExtraUpdateDelta = pUpdateDelta;

		// 모든 동시 진행이 끝나기를 기다립니다.
		if ((mCooldownTimer > 0) ||
			mFacingRotationUpdater.aIsRotating ||
			mTrackingRotationUpdater.aIsRotating ||
			(mWindupTimer > 0))
			return true; // 아직 끝나지 않은 작업 있음

		// TOT 대기 여부를 검사합니다.
		if ((aController.aMainAttackControl == this) &&
			(aControlTroopCommander.aTimeOnTargetState != BattleCommander.TimeOnTargetState.None))
		{
			switch (aControlTroopCommander.aTimeOnTargetState)
			{
			case BattleCommander.TimeOnTargetState.Wait:
				{
					aControlTroopCommander.ReportTimeOnTargetReady(aControlTroop.aTroopSlotIdx, mTargetingInfo);
					return true; // BattleCommander.TimeOnTargetState.Ready 상태에서의 처리로 넘어감
				}
			case BattleCommander.TimeOnTargetState.Ready:
				{
					if (!aControlTroopCommander.ReportTimeOnTargetFire(aControlTroop.aTroopSlotIdx))
						return true; // 발사 타이밍을 기다림
				}
				break;
			}
		}

		// 와인드업을 비롯한 모든 작업이 끝나면 발사로 넘어갑니다.
		int lRotationExtraUpdateDelta = Mathf.Min(lFacingRotationExtraUpdateDelta, lTrackingRotationExtraUpdateDelta);
		int lExtraUpdateDelta = MathUtil.Min(lCooldownExtraUpdateDelta, lRotationExtraUpdateDelta, lWindupExtraUpdateDelta);
		return _SetOnFrameUpdateDelegate(_OnStart_Fire(10000), lExtraUpdateDelta); // 100% 조준(만분율)
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_Fire(int pTargetingRatio)
	{
		aControlTroop.aStageTroop.DebugLog(cFireText);

		mTargetingInfo.SetTargetingRatio(pTargetingRatio);

		mAimTimer = mTargetingInfo.GetAimDelay();

		// 조준 상태를 보입니다.
		aControlTroop.aStageTroop.AimWeapon(mAimTimer);

		return _OnFrameUpdate_Fire;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_Fire(int pUpdateDelta)
	{
// [테스트] 아군 공격 테스트 용도
// 		if (aControlTroop.aTeamIdx != 1)
// 			return true;

		// 쿨다운을 진행시키고 추가 갱신 시간을 얻습니다.
		int lCooldownExtraUpdateDelta;
		if (mCooldownTimer > 0)
			_ProceedCooldown(pUpdateDelta, out lCooldownExtraUpdateDelta, false); // 여기서는 터렛도 쿨다운을 기다립니다.
		else
			lCooldownExtraUpdateDelta = pUpdateDelta;

		// 조준을 진행시키고 추가 갱신 시간을 얻습니다.
		int lAimExtraUpdateDelta;
		if (mAimTimer > 0)
		{
			mAimTimer -= pUpdateDelta;
			lAimExtraUpdateDelta = -mAimTimer;
		}
		else
			lAimExtraUpdateDelta = pUpdateDelta;

		// 타겟이 움직일 때의 회전을 처리합니다.
		int lTrackingRotationExtraUpdateDelta = pUpdateDelta;
		if (mTargetingInfo.CheckTargetUpdate() ||
			!MathUtil.CheckEqualDegree(mTrackingRotationUpdater.aRotationEndDirectionY, mTargetingInfo.aTargetDirectionY))
		{
			mTrackingRotationUpdater.SetDstRotation(
				mTargetingInfo.aTargetDirectionY,
				mTargetingInfo.aMaxTrackingDelay, // 트래킹 속도를 사용
				false);		
			if (mTrackingRotationUpdater.aIsRotating)
			{
				mTrackingRotationUpdater.UpdateRotation(pUpdateDelta);
				lTrackingRotationExtraUpdateDelta = mTrackingRotationUpdater.aExtraUpdateDelta;
			}
		}

		// 모든 동시 진행이 끝나기를 기다립니다.
		if ((mCooldownTimer > 0) ||
			(mAimTimer > 0) ||
			mTrackingRotationUpdater.aIsRotating)
			return true; // 아직 끝나지 않은 작업 있음

		// 보정한 갱신 시간을 얻습니다.
		pUpdateDelta = MathUtil.Min(lCooldownExtraUpdateDelta, lAimExtraUpdateDelta, lTrackingRotationExtraUpdateDelta);

		// 곡사 무기의 경우 타게팅 더미를 보입니다. -> 아래의 FlashEnemyObserverDummy()로 대체됨
// 		for (int iTeam = 1; iTeam < aBattleGrid.aTeamCount; ++iTeam) // 이 부대가 0번 중립 팀을 제외하고
// 			if (!aControlTroop.aIsDetecteds[iTeam] && // 공격 받는 쪽 입장에서 탐지가 안 된 부대인데
// 				(mTargetingInfo.aSelectedWeaponData.aWeaponSpec.mAttackType == WeaponAttackType.HighAngleGun)) // 곡사 무기로 공격하는 경우라면
// 			{
// 				aControlTroop.FlashTargetingDummy(mTargetingInfo.aTargetTroop); // 타게팅 더미를 보여줍니다.
// 				break;
// 			}

		// 공격 순간에 이 공격 부대를 다른 모든 팀이 탐지할 수 있도록 옵저버 더미를 생성합니다.
		aControlTroop.FlashTargetingEnemyObserverDummy(mTargetingInfo);

		// 공격 효과를 생성합니다.
		BattleFireEffect lFireEffect = aBattleGrid.SpawnFireEffect(mTargetingInfo);

		// 쿨다운 타이머를 초기화합니다.
		mCooldownTimer = mTargetingInfo.GetCooldownDelay();

		// 사용 딜레이 검사를 위한 시간을 지정합니다.
		int lFireTime = mFrameUpdateTime + (mFrameUpdateDelta - pUpdateDelta);
		mTargetingInfo.aSelectedWeaponData.SetUseDelay(lFireTime);

		// 타겟 타입에 따라...
		switch (mTargetingInfo.aTargetingType)
		{
		case BattleTroopTargetingInfo.TargetingType.Troop:
			{
				// 다음 타겟을 미리 얻습니다.
				BattleTroop lLastTargetTroop = mTargetingInfo.aTargetTroop;
				BattleTroop lNextTargetTroop = _InitNextTargetTroop(lFireTime);
				bool lIsResumeAim;
				if ((lNextTargetTroop != null) &&
					(lLastTargetTroop == lNextTargetTroop))
					lIsResumeAim = true;
				else
					lIsResumeAim = false;

				// 공격 상태를 보입니다.
				aControlTroop.aStageTroop.FireWeapon(lFireEffect, mCooldownTimer, lIsResumeAim);
				aControlTroop.aStageTroop.DebugLog(lFireEffect.aHitResult.ToString());

				// 다음 상태로 넘어갑니다.
				return _SetOnFrameUpdateDelegate(_OnStart_TargetTroopReset(lNextTargetTroop), pUpdateDelta);
			}
			//break;
		case BattleTroopTargetingInfo.TargetingType.Coord:
		default:
			{
				// 지면 타격일 경우 공격 반복 회수를 증가시킵니다.
				++mTargetCoordAttackCounter;
				//Debug.Log("mTargetCoordAttackCounter=" + mTargetCoordAttackCounter + " - " + this);

				bool lIsResumeAim;
				lIsResumeAim = (mTargetCoordAttackCounter < aController.aTargetCoordAttackRepeatCount); // 타겟 공격 반복 회수가 남았다면 조준 지속

				// 공격 상태를 보입니다.
				aControlTroop.aStageTroop.FireWeapon(lFireEffect, mCooldownTimer, lIsResumeAim);
				aControlTroop.aStageTroop.DebugLog(lFireEffect.aHitResult.ToString());

				// 다음 상태로 넘어갑니다.
				return _SetOnFrameUpdateDelegate(_OnStart_TargetCoordReset(), pUpdateDelta);
			}
			//break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_TargetTroopReset(BattleTroop pNextTargetTroop)
	{
		//Debug.Log("_OnStart_TargetTroopReset()");

		aControlTroop.aStageTroop.DebugLog(cTargetResetText);

		return _OnFrameUpdate_TargetTroopReset; // 타겟 초기화를 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_TargetTroopReset(int pUpdateDelta)
	{
		BattleTroop lNextTargetTroop = mNextTargetTroopLink.Get();

		// 적 부대 유무에 대하여...
		if (lNextTargetTroop != null) // 적이 있다면
		{
			return _SetOnFrameUpdateDelegate(_InitAttack(pUpdateDelta), pUpdateDelta); // 공격 상태를 초기화합니다.
		}
		else // 적이 없다면
		{
			if (mControlWeapon == ControlWeapon.BodyWeapon) // 몸통일 때에는
			{
				if (!aControlTroop.aIsTurretAttacking && // 몸통이고 터렛도 공격중이 아니라면
					!aControlTroop.aControlState.mIsSetUpMode) // 셋업 모드가 아닐 경우
				{
					aController.StartSearchControl(
						BattleTroopSearchControl.RetryOption.RetrySomeTimes,
						BattleTroopSearchControl.MoveOption.HoldPosition,
						pUpdateDelta); // 탐색으로 변경
					return false;
				}
				else
					return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta); // 아이들 상태로 돌아갑니다.
			}
			else if (mControlWeapon == ControlWeapon.TurretWeapon) // 터렛일 때에는
			{
				return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta); // 아이들 상태로 돌아갑니다.
			}
			else
				return true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_TargetCoordReset()
	{
		//Debug.Log("_OnStart_TargetCoordReset()");

		aControlTroop.aStageTroop.DebugLog(cTargetResetText);

		return _OnFrameUpdate_TargetCoordReset; // 타겟 초기화를 시작합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_TargetCoordReset(int pUpdateDelta)
	{
		if (mTargetCoordAttackCounter < aController.aTargetCoordAttackRepeatCount) // 타겟 공격 반복 회수가 남았다면
		{
			return _SetOnFrameUpdateDelegate(_InitAttack(pUpdateDelta), pUpdateDelta); // 공격 상태를 초기화합니다.
		}
		else // 타겟 공격 반복 회수가 끝났다면
		{
			mTargetCoordAttackCounter = 0; // 카운터 초기화
			//Debug.Log("mTargetCoordAttackCounter=" + mTargetCoordAttackCounter + " - " + this);

			if (mControlWeapon == ControlWeapon.BodyWeapon) // 몸통일 때에는
			{
				if (!aControlTroop.aIsTurretAttacking && // 몸통이고 터렛도 공격중이 아니라면
					!aControlTroop.aControlState.mIsSetUpMode) // 셋업 모드가 아닐 경우
				{
					aController.StartSearchControl(
						BattleTroopSearchControl.RetryOption.RetrySomeTimes,
						BattleTroopSearchControl.MoveOption.HoldPosition,
						pUpdateDelta); // 탐색으로 변경
					return false;
				}
				else
					return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta); // 아이들 상태로 돌아갑니다.
			}
			else if (mControlWeapon == ControlWeapon.TurretWeapon) // 터렛일 때에는
			{
				return _SetOnFrameUpdateDelegate(mOnStartIdleDelegate(), pUpdateDelta); // 아이들 상태로 돌아갑니다.
			}
			else
				return true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟을 지정합니다.
	private void _SetTarget(BattleTroopTargetingInfo.TargetingType pTargetingType, Coord2 pTargetCoord, BattleTroop pTargetTroop, bool pIsUpdateInteraction)
	{
		if ((aController.aTargetCoordAttackRepeatCount > 1) && // 반복 사격인데
			(mTargetingInfo.aMainWeaponData.aWeaponSpec.mAttackType == WeaponAttackType.HighAngleGun)) // 포병이라면
			pTargetingType = BattleTroopTargetingInfo.TargetingType.Coord; // 적 부대 공격을 좌표 공격으로 강제

		mTargetingInfo.SetTarget(pTargetingType, pTargetCoord, pTargetTroop);
		aControlTroopCommander.OnTroopTargetChange(aControlTroop.aTroopSlotIdx, pTargetTroop);

		// 이 타게팅이 유발하는 부대 상호 작용을 지정합니다.
		if (pIsUpdateInteraction)
		{
			BattleTroop lCurrentInteractionTargetTroop = mInteractionTargetTroopLink.Get();
			BattleTroop lNewInteractionTargetTroop
				= (mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType != TroopInteractionType.None)
				? pTargetTroop : null;
			if (lNewInteractionTargetTroop != lCurrentInteractionTargetTroop) // 상호 작용 대상이 바뀌었다면
			{
				if (lCurrentInteractionTargetTroop != null)
					lCurrentInteractionTargetTroop.aInteraction.StopInteraction();

				mInteractionTargetTroopLink.Set(lNewInteractionTargetTroop);

				if (lNewInteractionTargetTroop != null)
				{
					lNewInteractionTargetTroop.aInteraction.StartInteraction(
						mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType,
						aControlTroop,
						0);

					// 스테이지에 보입니다.
					mIsGoToInteraction = true;
					aControlTroop.aStageTroop.GoToInteraction(
						mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType,
						lNewInteractionTargetTroop);
				}
				else
				{
					// 스테이지에 보입니다.
					mIsGoToInteraction = false;
					aControlTroop.aStageTroop.ComeBackInteraction();
				}
			}
			else // 상호 작용 대상이 같다면(없는 것 포함)
			{
				if ((lCurrentInteractionTargetTroop != null) && // 상호 작용 대상은 그대로인데
					(lCurrentInteractionTargetTroop.aInteraction.aTroopInteractionType != mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType)) // 상호 작용 타입이 바뀌었다면
				{
					lCurrentInteractionTargetTroop.aInteraction.StopInteraction(); // 기존 상호 작용은 중단
					lCurrentInteractionTargetTroop.aInteraction.StartInteraction(
						mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType,
						aControlTroop,
						0); // 새 상호 작용 시작

					// 스테이지에 보입니다.
					mIsGoToInteraction = true;
					aControlTroop.aStageTroop.GoToInteraction(
						mTargetingInfo.aSelectedWeaponData.mWeaponSpec.mTroopInteractionType,
						lCurrentInteractionTargetTroop);
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟을 초기화합니다.
	private void _ResetTarget()
	{
		BattleTroop lInteractionTroop = mInteractionTargetTroopLink.Get();
		if (lInteractionTroop != null)
		{
			if (lInteractionTroop.aInteraction.aInteractionTroop == aControlTroop)
				lInteractionTroop.aInteraction.StopInteraction();

			mInteractionTargetTroopLink.Set(null);
		}

		// 상호작용 부대가 이미 사라졌더라도 상호작용 동작 중이었을 수 있으므로 중단 처리를 검사합니다.
		if (mIsGoToInteraction)
		{
			// 스테이지에 보입니다.
			mIsGoToInteraction = false;
			aControlTroop.aStageTroop.ComeBackInteraction();
		}

		aControlTroopCommander.OnTroopTargetChange(aControlTroop.aTroopSlotIdx, null);
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 타겟 부대에 대한 공격을 초기화합니다.
	private BattleTroop _InitNextTargetTroop(int pInitTime)
	{
		if (aControlTroopStat.aTroopStatTable.aIsAttackDisabled) // 공격 불가 상태라면
		{
			mNextTargetTroopLink.Set(null);
			return null; // 다음 타겟 없음
		}

		BattleTroop lCurrentTargetTroop = mTargetingInfo.aTargetTroop;
		if ((lCurrentTargetTroop != null) &&
			!lCurrentTargetTroop.aIsDetecteds[aControlTroop.aTeamIdx])
			lCurrentTargetTroop = null; // 적이 탐지 범위에서 사라짐

		IBattleTarget lNextTarget;
		if (!mTargetingInfo.aIsGroupTargeting) // 그룹 타게팅이 아니라면
		{
			if (lCurrentTargetTroop == mInteractionTargetTroopLink.Get())
				lCurrentTargetTroop = null; // 상호 작용의 경우는 계속 타겟 갱신

			if ((lCurrentTargetTroop != null) && // 기존 적이 남아 있고
				((lCurrentTargetTroop == aControlTroopCommander.aExplicitTargetTroop) || // 명시적 타겟이거나
					((pInitTime - mTargetLockFrameTime) < BattleConfig.get.mTargetResearchDelay))) // 타겟 재탐색 시간이 안 지났다면
			{
				lNextTarget = lCurrentTargetTroop; // 기존 타겟을 계속 공격
				//Debug.Log("lNextTarget = lCurrentTargetTroop - " + this);
			}
			else // 새로운 적 탐색을 해야 한다면
			{
				lNextTarget = aControlTroop.SearchTarget(lCurrentTargetTroop, mTargetingInfo, mIsTargetingRotatable);
				if (lNextTarget != null) // 적이 있다면
				{
					// 새로운 공격을 준비합니다.
					//Debug.Log("_SetTarget() - " + aControlTroop);
					_SetTarget(BattleTroopTargetingInfo.TargetingType.Troop, lNextTarget.aTargetCoord, lNextTarget.aTargetTroop, true);

					mTargetLockFrameTime = pInitTime;
				}
			}

			mIsBurstFiring = false;
		}
		else // 그룹 타게팅이라면
		{
			mIsBurstFiring = false;

			if ((pInitTime - mTargetLockFrameTime) >= BattleConfig.get.mTargetResearchDelay) // 타겟 재탐색 시간이 지났다면
			{
				// 현재 타겟이 화각을 벗어났다면 초기화합니다.
				if (lCurrentTargetTroop != null)
				{
					Coord2 lTargetVector = lCurrentTargetTroop.aCoord - aControlTroop.aCoord;
					int lTargetDirectionY = lTargetVector.RoughDirectionY();
					int lTargetAngleDistance = Mathf.Abs(MathUtil.DegreeVector(lTargetDirectionY, mFacingRotationUpdater.aRotationEndDirectionY));
					if (lTargetAngleDistance > mTargetingInfo.aSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.TrackingAngleHalf))
						lCurrentTargetTroop = null;
				}

				// 새로 탐색을 합니다.
				BattleTroop lLastTargetTroop = mTargetingInfo.aTargetTroop;
				lNextTarget = aControlTroop.SearchTarget(lCurrentTargetTroop, mTargetingInfo, mIsTargetingRotatable);
				if (lNextTarget != null) // 적이 있다면
				{
					BattleTroopStat.WeaponData lNextBestWeaponData = mTargetingInfo.GetBestWeaponData(
						BattleTroopTargetingInfo.TargetingAngle.TrackingAngle,
						mStartParam.mIsIgnoreRange,
						pInitTime);

					// 첫 타겟이 이전과 같은 타겟이라면 다음 타겟으로 바로 넘어갑니다.
					if ((lNextTarget.aTargetTroop == lLastTargetTroop) && // 같은 타겟
						(lNextBestWeaponData != null) &&
						(mTargetingInfo.aSelectedWeaponData == lNextBestWeaponData)) // 같은 무기
					{
						lNextTarget = mTargetingInfo.GetNextTargetTroop();
						mIsBurstFiring = true; // 연사 공격중
						if (lNextTarget == null)
						{
							lNextTarget = lLastTargetTroop;
							Debug.LogWarning("lNextTarget is null - " + this);
						}
					}

					mTargetLockFrameTime = pInitTime;
				}
			}
			else // 아직 한 주기 공격이 안 끝났거나 재탐색 시간 이내라면
			{
				// 다음 타겟을 공격합니다.
				lNextTarget = mTargetingInfo.GetNextTargetTroop();
				mIsBurstFiring = true; // 연사 공격중
			}

			if (lNextTarget != null)
			{
				// 중간 발사가 아닌 새로운 타겟에 대한 공격이라면 새로운 공격을 준비합니다.
				if (mTargetingInfo.aTargetingRatio >= 10000) // 100% 조준일 경우에만(만분율)
					_SetTarget(
						BattleTroopTargetingInfo.TargetingType.Troop,
						lNextTarget.aTargetCoord,
						lNextTarget.aTargetTroop,
						!mIsBurstFiring); // 연사 공격 중에는 상호 작용 갱신 안 함
			}
		}

		BattleTroop lNextTargetTroop = (lNextTarget != null) ? lNextTarget.aTargetTroop : null;
		mNextTargetTroopLink.Set(lNextTargetTroop); // 다음 타겟 지정
		return lNextTargetTroop; // 다음 타겟 반환
	}
	//-------------------------------------------------------------------------------------------------------
	// 쿨다운 시간을 보냅니다.
	private bool _ProceedCooldown(int pUpdateDelta, out int oProceedUpdateDelta, bool pIsCooldownAction)
	{
		// 쿨다운이 끝나기를 기다립니다.
		mCooldownTimer -= pUpdateDelta;
		if (mCooldownTimer > 0)
		{
			if (pIsCooldownAction)
				oProceedUpdateDelta = pUpdateDelta;
			else
				oProceedUpdateDelta = 0;
			return pIsCooldownAction; // 쿨다운 남음
		}

		// 쿨다운 해제를 보입니다.
		aControlTroop.aStageTroop.EndCooldown();

		if (pIsCooldownAction)
			oProceedUpdateDelta = pUpdateDelta;
		else
			oProceedUpdateDelta = -mCooldownTimer;
		return true; // 쿨다운 끝남
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;

	private ControlWeapon mControlWeapon;
	private BattleTroopRotationUpdater mFacingRotationUpdater;
	private BattleTroopRotationUpdater mTrackingRotationUpdater;
	private int mAttackDirectionY;
	private bool mIsTargetingRotatable;

	private BattleTroopTargetingInfo mTargetingInfo;
	private bool mIsCooldownActionWeapon;

	private OnFrameUpdateDelegate mOnFrameUpdateDelegate;
	private OnStartDelegate mOnStartIdleDelegate;
	private OnFrameUpdateDelegate mOnFrameUpdateIdleDelegate;
	private int mTargetCoordAttackCounter;
	private bool mIsSettingUp;
	private bool mIsAttacking;
	private bool mIsBurstFiring;
	private int mSetUpTimer;
	private int mWindupTimer;
	private int mAimTimer;
	private int mCooldownTimer;
	private GuidLink<BattleTroop> mNextTargetTroopLink;
	private GuidLink<BattleTroop> mInteractionTargetTroopLink;
	private bool mIsGoToInteraction;
	private int mTargetLockFrameTime;

	private int mIdleSearchTimer;
	private int mFrameUpdateTime;
	private int mFrameUpdateDelta;
	private int mSetOnFrameUpdateCallCounter;
}
