using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopBarricadeControl : BattleTroopControl
{
	public class StartParam
	{
		internal BattleBarricade mBarricade;

		public void Set(BattleBarricade pBarricade)
		{
			mBarricade = pBarricade;
		}
	}
	
	public const String cStatePrefix = "Barricade ";
	
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Barricade"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopBarricadeControl()
	{
		mStartParam = new StartParam();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		SetController(pController);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (!base.Start())
			return false;
		
		mBarricade = mStartParam.mBarricade;
		if (mBarricade != null)
		{
			if (!mBarricade.CheckInEnterRadius(aControlTroop.aCoord))
			{
				aController.StartSkillPositioningAdvanceControl(mBarricade.aCoord, aStartDelta);
				return false; // 먼저 바리케이드로 이동할 필요 있음
			}			
			if (mBarricade.aCoveringTroop == aControlTroop)
			{
				aController.StartSearchControl(
					BattleTroopSearchControl.RetryOption.RetrySomeTimes,
					BattleTroopSearchControl.MoveOption.HoldPosition,
					aStartDelta); // 멈춤 탐색
				return false; // 이미 주둔 중
			}

			//Debug.Log("BarricadeMove");
			bool lIsMovableBackward
				= (aControlTroop.aTroopSpec.mType == TroopType.Armored)
				&& aControlTroop.aIsInBattle;
			aControlTroop.aMoveUpdater.SetMove(
				mBarricade.aCoord,
				BattleTroopMoveUpdater.MoveType.PositioningMove,
				0, // pSqrApproachRange
				BattleTroopMoveUpdater.RotationType.RotateAndMove,
				lIsMovableBackward);
		}
		else
		{
			if ((aControlTroop.aCoveringBarricade != null) &&
				(aControlTroop.aCoveringBarricade.aCoveringTroop == aControlTroop))
				aControlTroop.aCoveringBarricade.SetCoveringTroop(null); // 주둔 해제를 하자마자 방어 효과 해제

//			aControlTroopStat.mPreMoveTimer = aControlTroopStat.GetValue(TroopStat.SetUpDelay); // 이동 전 타이머 초기화

			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.RetrySomeTimes,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				aStartDelta); // 탐색으로 전환하고 대기
			return false; // 바리케이드를 바로 빠져나옴
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
// 	public override bool Stop()
// 	{
// 		if (!base.Stop())
// 			return false;
// 
// 		return true;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		// 핀 다운 상태에서는 탐색으로 돌아갑니다.
		if (aControlTroopStat.aMoraleState == TroopMoraleState.PinDown)
		{
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				0); // 진군을 안한다면 탐색으로 전환
			return false;
		}

		return _OnFrameUpdate(pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수
	private bool _OnFrameUpdate(int pUpdateDelta)
	{
		// 이동 중 여부를 판단해서...
		if (aControlTroop.aMoveUpdater.aIsMoving)
		{
			// 이동 중이라면 현재 이동을 갱신합니다.
			aControlTroop.aMoveUpdater.UpdateMove(pUpdateDelta);
		}
		else
		{
			// 이동이 끝났다면 주둔 효과를 발생시키고 탐색을 시작합니다.
			mBarricade.SetCoveringTroop(aControlTroop); // 주둔이 완료 되었을 때 방어 효과 생김
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.RetrySomeTimes,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 멈춤 탐색
			return false;
		}
		
		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;
//	private BattleTroop mAttackingEnemyTroop;
	private BattleBarricade mBarricade;
//	private int mConveringDelay;
//	private int mCoveringTimer;
//	private int mCoveringSec;
}
