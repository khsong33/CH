using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public interface IBattleTroopRotater
{
	int aRotationDirectionY { get; set; }
}

public class BattleTroopRotationUpdater
{
	public enum UpdaterType
	{
		Body,
		BodyWeapon,
		Turret,
	}
	private enum RotationType
	{
		DstRotation,
		TroopDirection,
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IBattleTroopRotater
	public IBattleTroopRotater aRotater
	{
		get { return mRotater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유효 여부
	public bool aIsValid
	{
		get { return (mRotater != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 갱신 여부
	public bool aIsRotating
	{
		get { return (mRotationTimer < mRotationDelay); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 시작 각도
	public int aRotationStartDirectionY
	{
		get { return mRotationStartDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 목표 각도
	public int aRotationEndDirectionY
	{
		get { return mRotationEndDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 회전 딜레이
	public int aMaxRotationDelay
	{
		get { return mMaxRotationDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 딜레이
	public int aRotationDelay
	{
		get { return mRotationDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 경과 시간
	public int aRotationTimer
	{
		get { return mRotationTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 추과 갱신 델타
	public int aExtraUpdateDelta
	{
		get { return mExtraUpdateDelta; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopRotationUpdater()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroop pUpdateTroop, IBattleTroopRotater pRotater, UpdaterType pUpdaterType)
	{
		mUpdateTroop = pUpdateTroop;
		mRotater = pRotater;
		mUpdaterType = pUpdaterType;
		if (mRotater != null)
		{
			mRotationStartDirectionY = mRotater.aRotationDirectionY;
			mRotationEndDirectionY = mRotationStartDirectionY;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전을 초기화합니다.
	public void ResetRotation()
	{
		mRotationDelay = 0;
		mRotationTimer = 0;
		mExtraUpdateDelta = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전을 지정합니다.
	public bool SetDstRotation(int pDstDirectionY, int pMaxRotationDelay, bool pIsMovableBackward)
	{
		mRotationType = RotationType.DstRotation;
		return _SetRotation(pDstDirectionY, pMaxRotationDelay, pIsMovableBackward);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 방향으로의 회전을 지정합니다.
	public bool SetTroopDirection(int pMaxRotationDelay, bool pIsMovableBackward)
	{
		mRotationType = RotationType.TroopDirection;
		return _SetRotation(mUpdateTroop.aDirectionY, pMaxRotationDelay, pIsMovableBackward);
	}
	//-------------------------------------------------------------------------------------------------------
	// 같은 목표각에 대해서 회전을 다시 수정합니다.
	public void CorrectRotation()
	{
		//Debug.Log("CorrectRotation()");

		mRotationStartDirectionY = mRotater.aRotationDirectionY;

		switch (mRotationType)
		{
		case RotationType.TroopDirection:
			mRotationEndDirectionY = mUpdateTroop.aDirectionY;
			break;
		}

		int lDstRotationVector = MathUtil.DegreeVector(mRotationStartDirectionY, mRotationEndDirectionY);
		if (lDstRotationVector == 0)
		{
			ResetRotation();
			return;
		}
		mRotationDelay = mMaxRotationDelay * Mathf.Abs(lDstRotationVector) / 180;
		mRotationTimer = 0;
		mExtraUpdateDelta = 0;

		// 회전 딜레이는 0인데 시작과 종료 방향각이 다르다면 다음 UpdateRotation()에서 무조건 완료가 되도록 처리합니다.
		if ((mRotationDelay <= 0) &&
			(mRotationStartDirectionY != mRotationEndDirectionY))
			mRotationDelay = 1; // UpdateRotation()에서 무조건 완료가 되도록
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전을 갱신합니다.
	public void UpdateRotation(int pUpdateDelta)
	{
		if (!aIsRotating)
		{
			mExtraUpdateDelta = pUpdateDelta;
			return;
		}

		mRotationTimer += pUpdateDelta;
		if (mRotationTimer > mRotationDelay)
		{
			mExtraUpdateDelta = mRotationTimer - mRotationDelay;
			mRotationTimer = mRotationDelay;
		}

		mRotater.aRotationDirectionY = MathUtil.RoughLerpDegree(mRotationStartDirectionY, mRotationEndDirectionY, mRotationTimer, mRotationDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 완료 비율(만분율)을 얻습니다.
	public int GetRotationCompletionRatio()
	{
		if (mRotationTimer < mRotationDelay)
			return (mRotationTimer * 10000 / mRotationDelay);
		else
			return 10000;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 회전을 지정합니다.
	private bool _SetRotation(int pDstDirectionY, int pMaxRotationDelay, bool pIsMovableBackward)
	{
		mRotationStartDirectionY = mRotater.aRotationDirectionY;
		mRotationEndDirectionY = pDstDirectionY;

		int lDstRotationVector = MathUtil.DegreeVector(mRotationStartDirectionY, mRotationEndDirectionY);
		if (lDstRotationVector == 0)
		{
			ResetRotation();
			return false; // 이미 같은 방향각
		}
		if (pIsMovableBackward)
		{
			if (lDstRotationVector < -90)
			{
				lDstRotationVector += 180;
				mRotationEndDirectionY = MathUtil.AbsDegree(mRotationEndDirectionY + 180);
			}
			else if (lDstRotationVector > 90)
			{
				lDstRotationVector -= 180;
				mRotationEndDirectionY = MathUtil.AbsDegree(mRotationEndDirectionY + 180);
			}
		}

		mMaxRotationDelay = pMaxRotationDelay;
		mRotationDelay = mMaxRotationDelay * Mathf.Abs(lDstRotationVector) / 180;
		mRotationTimer = 0;
		mExtraUpdateDelta = 0;

		// 회전 딜레이는 0인데 시작과 종료 방향각이 다르다면 다음 UpdateRotation()에서 무조건 완료가 되도록 처리합니다.
		if ((mRotationDelay <= 0) &&
			(mRotationStartDirectionY != mRotationEndDirectionY))
			mRotationDelay = 1; // UpdateRotation()에서 무조건 완료가 되도록

		// 회전 지정을 보입니다.
		if (aIsRotating)
		{
			switch (mUpdaterType)
			{
			case UpdaterType.Body:
				mUpdateTroop.aStageTroop.OnSetDstBodyDirectionY(mRotationEndDirectionY, mRotationDelay);
				break;
			case UpdaterType.BodyWeapon:
				mUpdateTroop.aStageTroop.OnSetDstBodyWeaponDirectionY(mRotationEndDirectionY, mRotationDelay);
				break;
			case UpdaterType.Turret:
				mUpdateTroop.aStageTroop.OnSetDstTurretDirectionY(mRotationEndDirectionY, mRotationDelay);
				break;
			}
		}

		return aIsRotating;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mUpdateTroop;
	private IBattleTroopRotater mRotater;
	private UpdaterType mUpdaterType;

	private RotationType mRotationType;
	private int mRotationStartDirectionY;
	private int mRotationEndDirectionY;
	private int mMaxRotationDelay;
	private int mRotationDelay;
	private int mRotationTimer;
	private int mExtraUpdateDelta;
}
