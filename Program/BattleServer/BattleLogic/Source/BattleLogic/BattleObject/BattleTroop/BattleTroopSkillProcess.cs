using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopSkillProcess
{
	private delegate void OnStartSkillFunctionDelegate(BattleTroopSkillProcess pThis, int pStartDelta);
	private delegate void OnStopSkillFunctionDelegate(BattleTroopSkillProcess pThis, int pStopDelta);
	private delegate void OnUpdateSkillFunctionDelegate(BattleTroopSkillProcess pThis, int pUpdateDelta);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스킬 스펙
	public SkillSpec aSkillSpec
	{
		get { return mSkillSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 여부
	public bool aIsStarted
	{
		get { return mIsStarted; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 여부
	public bool aIsUpdating
	{
		get { return (mOnUpdateSkillFunctionDelegate != null); }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopSkillProcess()
	{
		if (!sIsSetUpSkillFunctionDelegates)
		{
			_SetUpSkillFunctionDelegates();
			sIsSetUpSkillFunctionDelegates = true;
		}

		mTroopSkillEffectLink = new GuidLink<BattleTroopSkillEffect>();
		mGridSkillEffectLinkBuffer = new GuidLink<BattleGridSkillEffect>[SkillSpec.cMaxSkillFunctionGridTileCount];
		for (int iTile = 0; iTile < SkillSpec.cMaxSkillFunctionGridTileCount; ++iTile)
			mGridSkillEffectLinkBuffer[iTile] = new GuidLink<BattleGridSkillEffect>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pTroopController)
	{
		mController = pTroopController;
		mTroop = mController.aControlTroop;
		mBattleLogic = mTroop.aBattleLogic; // 코딩 편의적인 중복 정보
//		mBattleGrid = mTroop.aBattleGrid; // 코딩 편의적인 중복 정보

		mCommanderSkill = mTroop.aCommander.aSkill;
		mSkillSpec = mCommanderSkill.aSkillSpec;

		mGridSkillEffectLinkCount = 0;

		mIsStarted = false;
		mIsInEffect = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리셋 함수
	public void Reset()
	{
		if (mIsInEffect)
			_OnDestroySkillEffect(null, 0);

		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 처리를 시작합니다.
	public void StartProcess(Coord2 pSkillTargetCoord, int pSkillDirectionY, int pSkillFormationIdx, int pStartDelta)
	{
		if (mIsStarted)
			Reset();
		mIsStarted = true;

		mSkillTargetCoord = pSkillTargetCoord;
		mSkillDirectionY = pSkillDirectionY;
		mSkillFormationIdx = pSkillFormationIdx;

		if (mSkillSpec.mIsFormationMove) // 스킬 포메이션 이동이 있다면
		{
			// 스킬 사용할 자리로 먼저 이동합니다.
			Coord2 lFormationCenter;
			switch (mSkillSpec.mUseType)
			{
			case SkillUseType.Position:
				lFormationCenter = mTroop.aCommander.aCoord;
				break;
			case SkillUseType.Default:
			default:
				lFormationCenter = pSkillTargetCoord;
				break;
			}
			Coord2 lFormationOffset = Coord2.RoughRotationVector(
				mSkillSpec.mFormationPositionOffsets[mSkillFormationIdx],
				mSkillDirectionY);
			Coord2 lFormationCoord = mTroop.aBattleGrid.ClampCoord(lFormationCenter + lFormationOffset);
			mController.StartSkillFormationAdvanceControl(lFormationCoord, pStartDelta); // 이후는 OnSkillFormationAdvanceEnd()에서 처리
		}
		else if (mSkillSpec.mIsGoToTileCenter) // 타일 중심으로의 자리 이동이 있다면
		{
			// 타일 중심으로의 자리 이동을 합니다.			
			mController.StartSkillPositioningAdvanceControl(mTroop.aGridTile.aCenterCoord, pStartDelta); // 이후는 OnSkillPositioningAdvanceEnd()에서 처리
		}
		else // 스킬 관련 이동이 따로 없다면
		{
			// 스킬 효과를 생성합니다.
			_OnCreateSkillEffect(null, pStartDelta);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 처리를 중단합니다.
	public void StopProcess(bool pIsHalting, int pStopDelta)
	{
		if (!mIsStarted)
			return;

		if ((mCommanderSkill.aReadyDelay > 0) && // 준비 시간이 필요한 스킬은
			!pIsHalting) // 즉각적인 중단이 아니라면
			mTroop.ReserveTask(
				_OnDestroySkillEffect,
				mCommanderSkill.aReadyDelay - pStopDelta); // 해제도 마찬가지로 준비 시간 이후에 효과 파괴
		else // 준비 시간이 없는 스킬이거나 즉각적인 중단이라면
			_OnDestroySkillEffect(null, pStopDelta); // 즉시 효과 파괴

		// 스테이지에 보입니다.
		mTroop.aStageTroop.StopSkill(mCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신을 합니다.
	public void UpdateFrame(int pUpdateDelta)
	{
		if (mOnUpdateSkillFunctionDelegate != null)
			mOnUpdateSkillFunctionDelegate(this, pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 포메이션 이동이 끝났을 때의 처리를 합니다.
	public void OnSkillFormationAdvanceEnd(int pEndDelta, out int pExtraUpdateDelta)
	{
		pExtraUpdateDelta = 0;

		if (mSkillSpec.mIsGoToTileCenter) // 타일 중심으로의 자리 이동이 있다면
		{
			// 타일 중심으로의 자리 이동을 합니다.			
			mController.StartSkillPositioningAdvanceControl(mTroop.aGridTile.aCenterCoord, pEndDelta); // 이후는 OnSkillPositioningAdvanceEnd()에서 처리
		}
		else // 스킬 관련 이동이 따로 없다면
		{
			// 스킬 효과를 생성합니다.
			_OnCreateSkillEffect(null, pEndDelta);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 자리잡기 이동이 끝났을 때의 처리를 합니다.
	public void OnSkillPositioningAdvanceEnd(int pElapsedTime, out int pExtraUpdateDelta)
	{
		pExtraUpdateDelta = 0;

		// 스킬 효과를 생성합니다.
		_OnCreateSkillEffect(null, pElapsedTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// 은폐 상태가 탐지될 때의 처리를 합니다. ==> 취소
// 	public void OnHidingDetected()
// 	{
// 		BattleTroopSkillEffect lTroopSkillEffect = mTroopSkillEffectLink.Get();
// 		if ((lTroopSkillEffect != null) &&
// 			lTroopSkillEffect.aTroopEffectSpec.mIsOffWhenHidingDetected) // 탐지되면 해제하는 스킬 효과라면
// 			lTroopSkillEffect.Destroy(); // 파괴합니다.
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 스킬 효과를 생성할 때 호출됩니다.
	private void _OnCreateSkillEffect(FrameTask pTask, int pStartDelta)
	{
		mIsInEffect = true;

		// 부대 스킬 효과를 생성합니다.
		if (mSkillSpec.mTroopEffectSpec != null)
		{
			int lActivationDelay = Mathf.Max(mCommanderSkill.aReadyDelay - pStartDelta, 0);
			BattleTroopSkillEffect lTroopSkillEffect = mBattleLogic.CreateTroopSkillEffect(
				mTroop,
				mSkillSpec.mTroopEffectSpec,
				lActivationDelay,
				0,
				mCommanderSkill.aSkillEffectGuid);
			mTroopSkillEffectLink.Set(lTroopSkillEffect);
		}

		// 스킬 함수를 시작합니다.
		if (sOnStartSkillFunctionDelegates[(int)mSkillSpec.mFunction] != null)
			sOnStartSkillFunctionDelegates[(int)mSkillSpec.mFunction](this, pStartDelta);

		// 스테이지에 보입니다.
		mTroop.aStageTroop.StartSkill(mCommanderSkill);

		// 스킬 갱신 함수를 지정합니다.
		mOnUpdateSkillFunctionDelegate = sOnUpdateSkillFunctionDelegates[(int)mSkillSpec.mFunction];
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 스킬 효과를 파괴할 때 호출됩니다.
	private void _OnDestroySkillEffect(FrameTask pTask, int pEndDelta)
	{
		mIsInEffect = false;
		mIsStarted = false; // 비로소 스킬 종료

		// 스킬 처리 중단 함수를 호출합니다.
		if (sOnStopSkillFunctionDelegates[(int)mSkillSpec.mFunction] != null)
			sOnStopSkillFunctionDelegates[(int)mSkillSpec.mFunction](this, 0);

		// 부대 스킬 효과를 파괴합니다.
		BattleTroopSkillEffect lTroopSkillEffect = mTroopSkillEffectLink.Get();
		if ((lTroopSkillEffect != null) &&
			(lTroopSkillEffect.aDurationDelay <= 0))
			lTroopSkillEffect.Destroy();

		// 그리드 스킬 효과를 파괴합니다.
		for (int iTile = 0; iTile < mGridSkillEffectLinkCount; ++iTile)
		{
			BattleGridSkillEffect lGridSkillEffect = mGridSkillEffectLinkBuffer[iTile].Get();
			if ((lGridSkillEffect != null) &&
				(lGridSkillEffect.aDurationDelay <= 0))
				lGridSkillEffect.Destroy();
		}
		mGridSkillEffectLinkCount = 0;

		// 기타 초기화를 합니다.
		mOnUpdateSkillFunctionDelegate = null;

		// 스킬 중지 후의 처리를 합니다.
		if (!mTroop.aIsFallBehind) // 낙오 상태가 아니라면
		{
			// 스킬 사용시 포메이션 이동이 강제였다면 포메이션을 복구합니다.
			if (mSkillSpec.mIsForceFormationMoveWhenSkill)
				mController.StartDstCoordAdvanceControl(
					mTroop.aCommander.GetTroopFormationCoord(mTroop.aTroopSlotIdx),
					pEndDelta);
		}
		else // 낙오 상태였다면
		{
			// 복귀합니다.
			mController.StartReturnAdvanceControl(pEndDelta);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 델리게이트 테이블을 초기화합니다.
	private static void _SetUpSkillFunctionDelegates()
	{
		sOnStartSkillFunctionDelegates = new OnStartSkillFunctionDelegate[SkillSpec.cSkillFunctionCount];
		sOnStartSkillFunctionDelegates[(int)SkillFunction.None] = null;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.GroundEffect] = _OnStartSkillFunction_GroundEffect;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.GroundAttack] = _OnStartSkillFunction_GroundAttack;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.TargetAttack] = _OnStartSkillFunction_TargetAttack;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.TroopSupplement] = _OnStartSkillFunction_TroopSupplement;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.FieldMedKit] = _OnStartSkillFunction_FieldRecovery;
		sOnStartSkillFunctionDelegates[(int)SkillFunction.Repair] = _OnStartSkillFunction_FieldRecovery;

		sOnStopSkillFunctionDelegates = new OnStopSkillFunctionDelegate[SkillSpec.cSkillFunctionCount];
		sOnStopSkillFunctionDelegates[(int)SkillFunction.None] = null;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.GroundEffect] = null;//_OnStopSkillFunction_GroundEffect;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.GroundAttack] = null;//_OnStopSkillFunction_GroundAttack;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.TargetAttack] = _OnStopSkillFunction_TargetAttack;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.TroopSupplement] = null;//_OnStopSkillFunction_TroopSupplement;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.FieldMedKit] = null;//_OnStopSkillFunction_FieldRecovery;
		sOnStopSkillFunctionDelegates[(int)SkillFunction.Repair] = null;//_OnStopSkillFunction_FieldRecovery;

		sOnUpdateSkillFunctionDelegates = new OnUpdateSkillFunctionDelegate[SkillSpec.cSkillFunctionCount];
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.None] = null;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.GroundEffect] = null;//_OnUpdateSkillFunction_GroundEffect;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.GroundAttack] = null;//_OnUpdateSkillFunction_GroundAttack;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.TargetAttack] = null;//_OnUpdateSkillFunction_TargetAttack;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.TroopSupplement] = null;//_OnUpdateSkillFunction_TroopSupplement;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.FieldMedKit] = null;//_OnUpdateSkillFunction_FieldRecovery;
		sOnUpdateSkillFunctionDelegates[(int)SkillFunction.Repair] = null;//_OnUpdateSkillFunction_FieldRecovery;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSkillFunctionDelegate
	private static void _OnStartSkillFunction_GroundEffect(BattleTroopSkillProcess pThis, int pStartDelta)
	{
		SkillSpec.GroundEffectParam lFunctionParam = pThis.mSkillSpec.mGroundEffectParam;

		// 부대 각각의 자리에 그리드 스킬 효과를 생성합니다.
		pThis.mStartDelta = pStartDelta;
		pThis.mTroop.aGridTile.CallTileHandler(lFunctionParam.mTileDistance, pThis._OnStartGridTileEffect_GroundEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGridTile.OnHandleTileDelegate
	private void _OnStartGridTileEffect_GroundEffect(BattleGridTile pGridTile)
	{
		int lActivationDelay = Mathf.Max(mCommanderSkill.aReadyDelay - mStartDelta, 0);
		BattleGridSkillEffect lGridSkillEffect = mBattleLogic.CreateGridSkillEffect(
			pGridTile,
			pGridTile.aCenterCoord,
			mSkillDirectionY,
			mSkillSpec.mGroundEffectParam.mGroundEffectSpec,
			lActivationDelay,
			mSkillSpec.mGroundEffectParam.mDurationDelay,
			mCommanderSkill.aSkillEffectGuid);

		if (lGridSkillEffect != null)
			mGridSkillEffectLinkBuffer[mGridSkillEffectLinkCount++].Set(lGridSkillEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopSkillFunctionDelegate
// 	private static void _OnStopSkillFunction_GroundEffect(BattleTroopSkillProcess pThis, int pStopDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateSkillFunctionDelegate
// 	private static void _OnUpdateSkillFunction_GroundEffect(BattleTroopSkillProcess pThis, int pUpdateDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSkillFunctionDelegate
	private static void _OnStartSkillFunction_GroundAttack(BattleTroopSkillProcess pThis, int pStartDelta)
	{
		SkillSpec.GroundAttackParam lFunctionParam = pThis.mSkillSpec.mGroundAttackParam;

		if (pThis.mTroop.aStat.aPrimaryWeaponAttackType == WeaponAttackType.HighAngleGun) // 포병만 해당
		{
			pThis.mTroop.aCommand.StartTargetCoordAttack(pThis.mSkillTargetCoord, true, pStartDelta); // 타겟 지점 공격 시작

			if (pThis.mTroop == pThis.mTroop.aCommander.aLeaderTroop) // 리더 부대인지 검사해서 한 번만
				pThis.mTroop.aCommander.SetTimeOnTargetCount(lFunctionParam.mAttackCount); // TOT 공격 회수를 지정

			pThis.mController.aTargetCoordAttackRepeatCount = lFunctionParam.mAttackCount; // 반복 공격 회수 지정
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopSkillFunctionDelegate
	private static void _OnStopSkillFunction_GroundAttack(BattleTroopSkillProcess pThis, int pStopDelta)
	{
		pThis.mController.aTargetCoordAttackRepeatCount = 1; // 반복 공격 회수 초기화
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateSkillFunctionDelegate
// 	private static void _OnUpdateSkillFunction_GroundAttack(BattleTroopSkillProcess pThis, int pUpdateDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSkillFunctionDelegate
	private static void _OnStartSkillFunction_TargetAttack(BattleTroopSkillProcess pThis, int pStartDelta)
	{
		SkillSpec.TargetAttackParam lFunctionParam = pThis.mSkillSpec.mTargetAttackParam;

		pThis.mController.aTargetCoordAttackRepeatCount = lFunctionParam.mAttackCount; // 반복 공격 회수 지정
		pThis.mTroop.aTargetSearchComparator.SetTargetBody(lFunctionParam.mAttackBody); // 해당 타입의 몸체만 공격
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopSkillFunctionDelegate
	private static void _OnStopSkillFunction_TargetAttack(BattleTroopSkillProcess pThis, int pStopDelta)
	{
		pThis.mController.aTargetCoordAttackRepeatCount = 1; // 반복 공격 회수 초기화
		pThis.mTroop.aTargetSearchComparator.ResetTargetBody(); // 모든 타입의 몸체를 공격
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateSkillFunctionDelegate
// 	private static void _OnUpdateSkillFunction_TargetAttack(BattleTroopSkillProcess pThis, int pUpdateDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSkillFunctionDelegate
	private static void _OnStartSkillFunction_TroopSupplement(BattleTroopSkillProcess pThis, int pStartDelta)
	{
		if (pThis.mTroop == pThis.mTroop.aCommander.aLeaderTroop) // 리더 부대인지 검사해서 한 번만
			pThis.mTroop.aCommander.OnRecruitTroop(); // 부대 충원
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopSkillFunctionDelegate
// 	private static void _OnStopSkillFunction_TroopSupplement(BattleTroopSkillProcess pThis, int pStopDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateSkillFunctionDelegate
// 	private static void _OnUpdateSkillFunction_TroopSupplement(BattleTroopSkillProcess pThis, int pUpdateDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSkillFunctionDelegate
	private static void _OnStartSkillFunction_FieldRecovery(BattleTroopSkillProcess pThis, int pStartDelta)
	{
		SkillSpec.FieldRecoveryParam lFunctionParam = pThis.mSkillSpec.mFieldRecoveryParam;

		// 반경 안의 아군 부대에게 회복을 실시합니다.
		if (lFunctionParam.mRecoveryDistance <= 0)
			pThis._OnFieldRecovery(pThis.mTroop, 0); // 거리가 0이면 자기 자신에게만
		else
			pThis.mTroop.aBattleGrid.CallTroopHandler(
				pThis.mTroop.aCoord,
				lFunctionParam.mRecoveryDistance,
				pThis.mTroop.aTeamIdx,
				BattleGrid.TeamSelection.AllyTeam,
				pThis._OnFieldRecovery);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnObjectHandlingDelegate<BattleTroop>
	private bool _OnFieldRecovery(BattleTroop pRecoveryTroop, int lSqrRecoveryTroopDistance)
	{
		//Debug.Log("_OnFieldRecovery() pRecoveryTroop=" + pRecoveryTroop);

		SkillSpec.FieldRecoveryParam lFunctionParam = mSkillSpec.mFieldRecoveryParam;

		if (pRecoveryTroop.aStat.aCurMilliHp < pRecoveryTroop.aStat.aMaxMilliHp)
			pRecoveryTroop.aActionQueue.PushSelfRecoveryAction(
				lFunctionParam.mRecoveryEffectDelay,
				lFunctionParam.mRecoveryActionDelay,
				lFunctionParam.mRecoveryRatio,
				mCommanderSkill.aSkillEffectGuid);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStopSkillFunctionDelegate
// 	private static void _OnStopSkillFunction_FieldRecovery(BattleTroopSkillProcess pThis, int pStopDelta)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateSkillFunctionDelegate
// 	private static void _OnUpdateSkillFunction_FieldRecovery(BattleTroopSkillProcess pThis, int pUpdateDelta)
// 	{
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static bool sIsSetUpSkillFunctionDelegates = false;
	private static OnStartSkillFunctionDelegate[] sOnStartSkillFunctionDelegates;
	private static OnStopSkillFunctionDelegate[] sOnStopSkillFunctionDelegates;
	private static OnUpdateSkillFunctionDelegate[] sOnUpdateSkillFunctionDelegates;

	private BattleTroopController mController;
	private BattleTroop mTroop;
	private BattleLogic mBattleLogic;
//	private BattleGrid mBattleGrid;
	private BattleCommanderSkill mCommanderSkill;
	private SkillSpec mSkillSpec;

	private bool mIsStarted;
	private bool mIsInEffect;
	private Coord2 mSkillTargetCoord;
	private int mSkillDirectionY;
	private int mSkillFormationIdx;
	private GuidLink<BattleTroopSkillEffect> mTroopSkillEffectLink;
	private GuidLink<BattleGridSkillEffect>[] mGridSkillEffectLinkBuffer;
	private int mGridSkillEffectLinkCount;

	private OnUpdateSkillFunctionDelegate mOnUpdateSkillFunctionDelegate;
	private int mStartDelta;
}
