using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopTargetSearchComparator : BattleGrid.ISearchComparator<BattleTroop>
{
	internal const int cInvalidTargetAngleDistance = int.MaxValue;

	internal delegate bool OnStartSearchDelegate(BattleTroopTargetSearchComparator pThis);
	internal delegate bool OnCompareDelegate(BattleTroopTargetSearchComparator pThis, BattleTroop pCompareTroop, int pSqrDistance);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleGridCell.ISearchComparator 속성
	public BattleTroop aResult
	{
		get { return mTargetTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 몸체 여부
	public bool[] aIsTargetBodys
	{
		get { return mIsTargetBodys; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopTargetSearchComparator()
	{
		if (!sIsSetUpDelegates)
		{
			sIsSetUpDelegates = true;
			_SetUpDelegates();
		}

		mIsTargetBodys = new bool[TroopSpec.cTroopTypeCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Set(BattleTroop pSearchTroop)
	{
		mSearchTroop = pSearchTroop;

		ResetTargetBody();
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐색 정보를 초기화합니다.
	public void StartSearch(BattleTroopTargetingInfo pTargetingInfo, BattleTroop pLastTargetTroop, bool pIsRotatable)
	{
		mTargetTroop = null;
		mLastTargetTroop = pLastTargetTroop;
		mSqrLastCompareDistance = int.MaxValue;
		mLastTroopMoraleState = TroopMoraleState.PinDown;
		mTargetingInfo = pTargetingInfo;

		mTargetingInfo.ResetGroupTargetingInfo();
		mIsGroupTargeting = mTargetingInfo.aIsGroupTargeting;

		mSearchWeaponData = mTargetingInfo.aMainWeaponData;
		mSearchWeaponSpec = mSearchWeaponData.mWeaponSpec;

		if (mSearchTroop.aControlState.mIsSetUpMode ||
			!pIsRotatable ||
			(mTargetingInfo.aMaxRotationDelay <= 0))
		{
			mIsCheckTrackingAngleLimit = true;

			mTrackingAngleHalfLimit = mSearchWeaponData.aWeaponStatTable.Get(WeaponStat.TrackingAngleHalf);

			BattleTroop lExplicitTargetTroop = mSearchTroop.aCommander.aExplicitTargetTroop;
			if ((lExplicitTargetTroop == null) || // 명시적 타겟이 없거나
				!pIsRotatable) // 회전 불가라면
				mTargetSearchDirectionY = mSearchTroop.aDefaultDirectionY; // 부대의 기본 방향을 기준으로 타겟을 찾습니다.
			else // 명시적 타겟이 있다면
				mTargetSearchDirectionY = (lExplicitTargetTroop.aCoord - mSearchTroop.aCoord).RoughDirectionY(); // 명시적 타겟 방향을 기준으로 타겟을 찾습니다.
		}
		else
		{
			mIsCheckTrackingAngleLimit = false;

			mTargetSearchDirectionY = mSearchTroop.aDefaultDirectionY;
		}

		// 탐색 시작 콜백을 호출합니다.
		if (sOnStartSearchDelegates[(int)mSearchWeaponSpec.mAttackType] != null)
		{
			if (sOnStartSearchDelegates[(int)mSearchWeaponSpec.mAttackType](this))
				return;
		}

		// 탐색 비교 콜백을 지정합니다.
		mOnCompareDelegate = sOnCompareDelegates[(int)mSearchWeaponSpec.mAttackType];
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.ISearchComparator 메서드
	public bool Compare(BattleTroop pCompareTroop, int pSqrDistance)
	{
		if (mSearchWeaponSpec.mTargeting != WeaponTargeting.All)
		{
			switch (mSearchWeaponSpec.mTargeting)
			{
			case WeaponTargeting.Soldier:
				if (pCompareTroop.aTroopSpec.mType != TroopType.Infantry)
					return true; // 보병만 가능
				break;
			case WeaponTargeting.Vehicle:
				if (pCompareTroop.aTroopSpec.mType == TroopType.Infantry)
					return true; // 차량만 가능
				break;
			}
		}
		if (!pCompareTroop.aIsDetecteds[mSearchTroop.aTeamIdx])
			return true; // 비교 계속(탐지 못함)
		if (!pCompareTroop.aIsTargetable)
			return true; // 비교 계속(타게팅 불가)
		if (!mIsTargetBodys[(int)pCompareTroop.aTroopSpec.mType])
			return true; // 비교 계속(타게팅 가능 바디 아님)
		if (!mOnCompareDelegate(this, pCompareTroop, pSqrDistance))
			return true; // 비교 계속(타입별 검사에서 제외)

		// 교체합니다.
		mTargetTroop = pCompareTroop;
		mSqrLastCompareDistance = pSqrDistance;
		mLastTroopMoraleState = mTargetTroop.aStat.aMoraleState;

		return true; // 비교 계속
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타입의 몸체만 공격하게 합니다.
	public void SetTargetBody(TroopType pTargetBody)
	{
		for (int iBody = 0; iBody < TroopSpec.cTroopTypeCount; ++iBody)
			mIsTargetBodys[iBody] = (pTargetBody == (TroopType)iBody);
	}
	//-------------------------------------------------------------------------------------------------------
	// 모든 타입의 몸체를 공격하게 합니다.
	public void ResetTargetBody()
	{
		for (int iBody = 0; iBody < TroopSpec.cTroopTypeCount; ++iBody)
			mIsTargetBodys[iBody] = true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 콜백 테이블을 초기화합니다.
	private static void _SetUpDelegates()
	{
		sOnStartSearchDelegates = new OnStartSearchDelegate[WeaponSpec.cAttackTypeEnumCount];
		for (int iType = 0; iType < WeaponSpec.cAttackTypeEnumCount; ++iType)
			sOnStartSearchDelegates[iType] = null;
		sOnStartSearchDelegates[(int)WeaponAttackType.MedKit] = _OnStartSearch_Recovery;
		sOnStartSearchDelegates[(int)WeaponAttackType.RepairKit] = _OnStartSearch_Recovery;

		sOnCompareDelegates = new OnCompareDelegate[WeaponSpec.cAttackTypeEnumCount];
		for (int iType = 0; iType < WeaponSpec.cAttackTypeEnumCount; ++iType)
			sOnCompareDelegates[iType] = _OnCompare_Default;
		sOnCompareDelegates[(int)WeaponAttackType.MedKit] = _OnCompare_Recovery;
		sOnCompareDelegates[(int)WeaponAttackType.RepairKit] = _OnCompare_Recovery;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSearchDelegate
	private static bool _OnStartSearch_Default(BattleTroopTargetSearchComparator pThis)
	{
		// 기존 타겟이 공격 가능할 경우 그 적을 기본 타겟으로 초기화합니다.
		if ((pThis.mLastTargetTroop != null) &&
			pThis.mLastTargetTroop.aIsDetecteds[pThis.mSearchTroop.aTeamIdx] &&
			pThis.mSearchTroop.CheckInAttackRange(pThis.mLastTargetTroop.aCoord))
			pThis.mTargetTroop = pThis.mLastTargetTroop;

		return false; // 탐색 완료 못함
	}
	//-------------------------------------------------------------------------------------------------------
	// OnCompareDelegate
	private static bool _OnCompare_Default(BattleTroopTargetSearchComparator pThis, BattleTroop pCompareTroop, int pSqrDistance)
	{
		if ((pSqrDistance < pThis.mTargetingInfo.aMainWeaponData.aSqrMinAttackRange) ||
			(pSqrDistance > pThis.mTargetingInfo.aMainWeaponData.aSqrMaxAttackRange))
			return false; // 타겟 부적격(사정 거리 밖)

		// 타겟 리스트를 반환할 필요가 없는 경우 교체 가능성을 미리 검사합니다.
		if (!pThis.mIsGroupTargeting)
		{
			bool lIsChanged;
			if ((pThis.mLastTroopMoraleState == TroopMoraleState.PinDown) && // 기존 부대가 핀다운 상태이고
				(pCompareTroop.aStat.aMoraleState != TroopMoraleState.PinDown)) // 새 부대가 아니라면
				lIsChanged = true; // 교체
			else
				lIsChanged = (pThis.mSqrLastCompareDistance > pSqrDistance); // 더 가까이 있는 부대라면 교체
			if (!lIsChanged)
				return false; // 타겟 부적격(교체 필요 없음)
		}

		// 타겟을 향한 각도 차이를 얻습니다.
		int lTargetAngleVector;
		if (pThis.mIsCheckTrackingAngleLimit ||
			pThis.mIsGroupTargeting)
		{
			Coord2 lTargetVector = pCompareTroop.aCoord - pThis.mSearchTroop.aCoord;
			int lTargetDirectionY = lTargetVector.RoughDirectionY();
			lTargetAngleVector = MathUtil.DegreeVector(lTargetDirectionY, pThis.mTargetSearchDirectionY);
		}
		else
			lTargetAngleVector = 0; // 단순 초기화

		// 회전할 수 없는 부대는 조준 범위 안에 있는 적만을 타겟으로 할 수 있습니다.
		if (pThis.mIsCheckTrackingAngleLimit &&
			((lTargetAngleVector < -pThis.mTrackingAngleHalfLimit) ||
				(lTargetAngleVector > pThis.mTrackingAngleHalfLimit)))
			return false; // 타겟 부적격(회전할 수 없는데 조준각 밖에 있음)

		// 타겟 리스트를 반환할 필요가 있는 경우 교체 가능성을 나중에 검사합니다.
		if (pThis.mIsGroupTargeting)
		{
			// 교체 가능성을 검사합니다.
			bool lIsChanged;
			if ((pThis.mLastTroopMoraleState == TroopMoraleState.PinDown) && // 기존 부대가 핀다운 상태이고
				(pCompareTroop.aStat.aMoraleState != TroopMoraleState.PinDown)) // 새 부대가 아니라면
				lIsChanged = true; // 교체
			else
				lIsChanged = (pThis.mSqrLastCompareDistance > pSqrDistance); // 더 가까이 있는 부대라면 교체

			// 타겟 부대 리스트에 추가합니다.
			pThis.mTargetingInfo.AddGroupTargetingInfo(pCompareTroop, pSqrDistance, lTargetAngleVector, lIsChanged);

			// 교체가 필요없다면 그룹에만 추가하고 마칩니다.
			if (!lIsChanged)
				return false; // 타겟 부적격(교체 필요 없음)
		}

		return true; // 타겟 선정(가능 대상)
	}
	//-------------------------------------------------------------------------------------------------------
	// OnStartSearchDelegate
	private static bool _OnStartSearch_Recovery(BattleTroopTargetSearchComparator pThis)
	{
		if ((pThis.mLastTargetTroop != null) && // 기존 부대가 있는데
			pThis.mLastTargetTroop.aInteraction.aIsRecoveryAvailable && // 치료가 가능하고
			(pThis.mLastTargetTroop.aHpPercent < BattleConfig.get.mRecoveryEndHpPercents[(int)pThis.mSearchTroop.aTroopSpec.mBody])) // 기본 치료량을 아직 덜 채웠다면
		{
			// 기존 타겟을 그대로 유지합니다.
			pThis.mTargetTroop = pThis.mLastTargetTroop;
			return true; // 탐색 완료
		}

		return false; // 탐색 완료 못함
	}
	//-------------------------------------------------------------------------------------------------------
	// OnCompareDelegate
	private static bool _OnCompare_Recovery(BattleTroopTargetSearchComparator pThis, BattleTroop pCompareTroop, int pSqrDistance)
	{
		if ((pSqrDistance < pThis.mSearchWeaponData.aSqrMinAttackRange) ||
			(pSqrDistance > pThis.mSearchWeaponData.aSqrMaxAttackRange))
			return false; // 타겟 부적격(사정 거리 밖)

		if (pCompareTroop == pThis.mSearchTroop)
			return false; // 타겟 부적격(자기 자신 치료 못함)
		if (pCompareTroop.aHpPercent >= 100)
			return false; // 타겟 부적격(HP 100%라서 치료 불필요)
		if (!pCompareTroop.aInteraction.aIsRecoveryAvailable)
			return false; // 타겟 부적격(치료 불가 상태)
		if (pCompareTroop.aInteraction.aIsValid)
		{
			BattleTroop lCompareTroopInteractionTroop = pCompareTroop.aInteraction.aInteractionTroop;
			if ((lCompareTroopInteractionTroop != null) &&
				(lCompareTroopInteractionTroop != pThis.mSearchTroop))
				return false; // 타겟 부적격(다른 부대와 상호 작용중)
		}

		if (pThis.mTargetTroop != null) // 기존 타겟이 있는데
		{
			if (pThis.mTargetTroop.aIsWounded) // 기존 타겟이 중상이라면
			{
				if (pCompareTroop.aIsWounded) // 새로운 타겟도 중상일 경우
				{
					if (pSqrDistance < pThis.mSqrLastCompareDistance) // 기존 타겟보다 가까이 있다면
						return true; // 타겟 선정(중상을 입은 더 가까운 부대 찾음)
					else // 기존 타겟이 더 가깝다면
						return false; // 타겟 부적격(중상을 입은 더 가까운 부대 있음)
				}
				else // 새로운 타겟이 중상이 아닐 경우
				{
					return false; // 타겟 부적격(중상을 입은 부대 우선)
				}
			}
			else // 기존 타겟이 중상은 아니라면
			{
				if (pCompareTroop.aIsWounded) // 새로운 타겟이 중상일 경우
				{
					return true; // 타겟 선정(중상을 입은 부대 우선)
				}
				else // 새로운 타겟도 중상이 아닐 경우
				{
					if (pCompareTroop.aHpPercent < pThis.mTargetTroop.aHpPercent) // 기존 타겟보다 HP가 적으면
						return true; // 타겟 선정(HP 적은 부대 우선)
					else // 기존 타겟보다 HP가 많다면
						return false; // 타겟 부적격(HP 적은 부대 우선)
				}
			}
		}
		else
			return true; // 타겟 선정(회복 필요한 부대 발견)
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static bool sIsSetUpDelegates = false;
	private static OnStartSearchDelegate[] sOnStartSearchDelegates;
	private static OnCompareDelegate[] sOnCompareDelegates;

	private BattleTroop mSearchTroop;
	private BattleTroop mTargetTroop;
	private BattleTroop mLastTargetTroop;
	private int mSqrLastCompareDistance;
	private TroopMoraleState mLastTroopMoraleState;
	private BattleTroopTargetingInfo mTargetingInfo;
	private bool mIsGroupTargeting;
	private BattleTroopStat.WeaponData mSearchWeaponData;
	private WeaponSpec mSearchWeaponSpec;
	private OnCompareDelegate mOnCompareDelegate;

	private bool mIsCheckTrackingAngleLimit;
	private int mTrackingAngleHalfLimit;
	private int mTargetSearchDirectionY;

	private bool[] mIsTargetBodys;
}
