using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopTargetingInfo
{
	public enum TargetingType
	{
		Troop,
		Coord,
	}
	public enum TargetingAngle
	{
		None,
		FireConeAngle,
		TrackingAngle,
	}
	internal enum TargetSelectionDirection
	{
		Right,
		Left,
	}

	internal class GroupTargetingInfo
	{
		public GuidLink<BattleTroop> mTargetTroopLink;
		public int mSqrDistance;
		public int mAngleVector;

		public GroupTargetingInfo()
		{
			mTargetTroopLink = new GuidLink<BattleTroop>();
		}
		public void Set(BattleTroop pTargetTroop, int pSqrDistance, int pAngleVector)
		{
			mTargetTroopLink.Set(pTargetTroop);
			mSqrDistance = pSqrDistance;
			mAngleVector = pAngleVector;
		}
	}

	public const int cMaxGroupTargetingCount = 16;
	private const int cMaxMillAccuracy = 100 * 1000;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 존재 여부
	public bool aIsValid
	{
		get { return (mAttackTroop != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 부대
	public BattleTroop aAttackTroop
	{
		get { return mAttackTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주 무기
	public BattleTroopStat.WeaponData aMainWeaponData
	{
		get { return mMainWeaponData; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 설치 여부
	public bool aIsInstalled
	{
		get { return mIsInstalled; }
		set { mIsInstalled = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 타입
	public TargetingType aTargetingType
	{
		get { return mTargetingType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표
	public Coord2 aTargetCoord
	{
		get { return mTargetCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대
	public BattleTroop aTargetTroop
	{
		get { return mTargetTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 그룹 타게팅 여부
	public bool aIsGroupTargeting
	{
		get { return mIsGroupTargeting; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 벡터
	public Coord2 aTargetVector
	{
		get { return mTargetVector; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟과의 거리의 제곱
	public int aSqrTargetDistance
	{
		get { return mSqrTargetDistance; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 회전 딜레이
	public int aMaxRotationDelay
	{
		get { return mMaxRotationDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 트래킹 딜레이
	public int aMaxTrackingDelay
	{
		get { return mMaxTrackingDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 조준 방향각
	public int aAimingDirectionY
	{
		get { return mAimingDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 거리
	public int aTargetDistance
	{
		get
		{
			if (!mIsValidTargetDistance)
			{
				mIsValidTargetDistance = true;

				mTargetDistance = MathUtil.RoughSqrt(mSqrTargetDistance);
			}
			return mTargetDistance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 각도 거리
	public int aTargetAngleDistance
	{
		get
		{
			if (!mIsValidTargetAngleDistance)
			{
				mIsValidTargetAngleDistance = true;

				mTargetAngleDistance = Mathf.Abs(MathUtil.DegreeVector(aTargetDirectionY, mAimingDirectionY));
				//Debug.Log("mTargetAngleDistance=" + mTargetAngleDistance + " mAttackTroop=" + mAttackTroop);
			}
			return mTargetAngleDistance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 방향각
	public int aTargetDirectionY
	{
		get
		{
			if (!mIsValidTargetDirectionY)
			{
				mIsValidTargetDirectionY = true;

				mTargetDirectionY = mTargetVector.RoughDirectionY();
			}
			return mTargetDirectionY;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 조준율
	public int aTargetingRatio
	{
		get { return mTargetingRatio; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 아군을 타게팅하는지 여부
	public bool aIsAllyTargeting
	{
		get { return mIsAllyTargeting; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 선택된 무기 데이터
	public BattleTroopStat.WeaponData aSelectedWeaponData
	{
		get { return mSelectedWeaponData; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 선택된 무기로 적을 공격할 때의 범위 레벨
	public int aSelectedWeaponRangeLevel
	{
		get { return mSelectedWeaponRangeLevel; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopTargetingInfo()
	{
		mTargetingWeaponDatas = new BattleTroopStat.WeaponData[TroopSpec.cWeaponCount];

		mTargetTroopLink = new GuidLink<BattleTroop>();

		mGroupTargetingInfos = new GroupTargetingInfo[cMaxGroupTargetingCount];
		for (int iInfo = 0; iInfo < cMaxGroupTargetingCount; ++iInfo)
			mGroupTargetingInfos[iInfo] = new GroupTargetingInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 리셋
	public void Reset()
	{
		mAttackTroop = null;
		mMainWeaponData = null;
		mWeaponDataCount = 0;
		mSelectedWeaponData = null;
		mTargetingRatio = 0;

		ResetGroupTargetingInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(BattleTroop pAttackTroop, BattleTroopAttackControl.ControlWeapon pControlWeapon)
	{
		mAttackTroop = pAttackTroop;
		mSyncRandom = pAttackTroop.aBattleGrid.aSyncRandom; // 코딩 편의적인 중복 정보
		mAttackTroopSpec = pAttackTroop.aTroopSpec; // 코딩 편의적인 중복 정보
		mAttackTroopStat = pAttackTroop.aStat; // 코딩 편의적인 중복 정보
		mAttackTroopStatTable = mAttackTroopStat.aTroopStatTable; // 코딩 편의적인 중복 정보
		mControlWeapon = pControlWeapon;

		_UpdateStat();
		_UpdateRotationDelay();
		if (mMainWeaponData == null)
		{
			Reset();
			return; // 아무 무기도 없다면 리셋하고 중지
		}

		mTargetingRatio = 0;

		mTargetTroopLink.Set(null);
		ResetGroupTargetingInfo();
		mTargetSelectionDirection = TargetSelectionDirection.Right;

		mAimingDirectionY = pAttackTroop.aDirectionY;

		_SetSelectedWeaponData(mMainWeaponData, WeaponSpec.cMaxRangeLevel);
		mIsInstalled = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 스탯 변화를 반영합니다.
	public void UpdateStat()
	{
		// 스탯 변경은 타겟이 바뀌는 SetTarget()에서 하도록 미루고 여기서는 갱신 플래그만 켜둡니다.
		mIsUpdateStat = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟을 지정합니다.
	public void SetTarget(TargetingType pTargetingType, Coord2 pTargetCoord, BattleTroop pTargetTroop)
	{
		if (mIsUpdateStat)
			_UpdateStat();

		_UpdateRotationDelay(); // 회전 속도는 항상 반영

		mTargetingType = pTargetingType;
		mLastTargetCoord = mTargetCoord;
		mTargetCoord = pTargetCoord;
		mTargetTroopLink.Set(pTargetTroop);
		mTargetingRatio = 10000; // 100% 조준 상태(만분율)

		mTargetVector = mTargetCoord - aAttackTroop.aCoord;
		mSqrTargetDistance = mTargetVector.SqrMagnitude();
		mIsInSightRange = (mSqrTargetDistance <= mAttackTroopStat.aSqrSightRange);

		mIsValidTargetDistance = false;
		mIsValidTargetAngleDistance = false;
		mIsValidTargetDirectionY = false;

		if ((pTargetingType == TargetingType.Troop) &&
			(pTargetTroop != null) &&
			(pTargetTroop.aTeamIdx == mAttackTroop.aTeamIdx))
			mIsAllyTargeting = true;
		else
			mIsAllyTargeting = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 비율을 지정합니다.
	public void SetTargetingRatio(int pTargetingRatio)
	{
		mTargetingRatio = pTargetingRatio;
	}
	//-------------------------------------------------------------------------------------------------------
	// 그룹 타게팅 정보를 초기화합니다.
	public void ResetGroupTargetingInfo()
	{
		mGroupTargetingInfoCount = 0;
		mMainTargetingInfoIdx = 0;
		mGroupTargetingInfoIdx = 0;
		mGroupTargetingCycleCounter = 0;
		mGroupTargetingCycleLimit = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 그룹 타게팅 정보를 추가합니다.
	public void AddGroupTargetingInfo(BattleTroop pTargetTroop, int pSqrDistance, int pAngleVector, bool pIsMainTarget)
	{
		if (mGroupTargetingInfoCount < cMaxGroupTargetingCount) // 제한수 미만이라면
		{
			// 새로 추가합니다.
			mGroupTargetingInfos[mGroupTargetingInfoCount].Set(pTargetTroop, pSqrDistance, pAngleVector);
			if (pIsMainTarget)
				mMainTargetingInfoIdx = mGroupTargetingInfoCount;
			++mGroupTargetingInfoCount;
		}
		else // 제한수에 도달했다면
		{
			// 기존 것 중에서 멀리 있는 것을 대체합니다.
			for (int iInfo = 0; iInfo < cMaxGroupTargetingCount; ++iInfo)
				if (mGroupTargetingInfos[iInfo].mSqrDistance > pSqrDistance)
				{
					mGroupTargetingInfos[iInfo].Set(pTargetTroop, pSqrDistance, pAngleVector);
					if (pIsMainTarget)
						mMainTargetingInfoIdx = iInfo;
					return;
				}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 그룹 타게팅 정보를 정렬합니다.
	public void SortGroupTargetingInfo(BattleTroop pLastTargetTroop)
	{
		//Debug.Log("SortGroupTargetingInfo()");

		if (mGroupTargetingInfoCount > 1)
		{
			GroupTargetingInfo lCurrentGroupTargetingInfo = null;
			if (pLastTargetTroop != null)
			{
				for (int iInfo = 0; iInfo < mGroupTargetingInfoCount; ++iInfo)
					if (mGroupTargetingInfos[iInfo].mTargetTroopLink.Get() == pLastTargetTroop)
					{
						lCurrentGroupTargetingInfo = mGroupTargetingInfos[iInfo];
						break;
					}
			}
			if (lCurrentGroupTargetingInfo == null)
			{
				int lStartTargetingInfoIdx = (mTargetSelectionDirection == TargetSelectionDirection.Right) ? mMainTargetingInfoIdx : 0; // 오른쪽으로 훑을 때에는 다음부터는 왼쪽으로 훑도록 맨 끝을 지정(그 반대의 경우도 마찬가지)
				lCurrentGroupTargetingInfo = mGroupTargetingInfos[lStartTargetingInfoIdx];
			}

			// 버블 정렬로 각 거리가 큰 순서로 정렬합니다.
			bool lIsSwapped = true;
			for (int iInfo = (mGroupTargetingInfoCount - 1); lIsSwapped; --iInfo)
			{
				lIsSwapped = false;
				for (int jInfo = 0; jInfo < iInfo; ++jInfo)
				{
					if (mGroupTargetingInfos[jInfo].mAngleVector > mGroupTargetingInfos[jInfo + 1].mAngleVector)
					{
						GroupTargetingInfo lGroupTargetingInfo = mGroupTargetingInfos[jInfo];
						mGroupTargetingInfos[jInfo] = mGroupTargetingInfos[jInfo + 1];
						mGroupTargetingInfos[jInfo + 1] = lGroupTargetingInfo;

						lIsSwapped = true;
					}
				}
			}

			// 바뀐 인덱스를 얻습니다.
			for (int iInfo = 0; iInfo < mGroupTargetingInfoCount; ++iInfo)
				if (mGroupTargetingInfos[iInfo] == lCurrentGroupTargetingInfo)
				{
					mMainTargetingInfoIdx = iInfo;
					break;
				}

			//for (int iInfo = 0; iInfo < mGroupTargetingInfoCount; ++iInfo)
			//{
			//	BattleTroop lTargetTroop = mGroupTargetingInfos[iInfo].mTargetTroopLink.Get();
			//	Debug.Log(String.Format("mGroupTargetingInfos[{0}].mAngleVector={1} coord=({2},{3})", iInfo, mGroupTargetingInfos[iInfo].mAngleVector, (lTargetTroop != null) ? lTargetTroop.aCoord.x : 0, (lTargetTroop != null) ? lTargetTroop.aCoord.z : 0));
			//}
			//Debug.Log("mGroupTargetingInfoCount=" + mGroupTargetingInfoCount);
		}
		else
		{
			mMainTargetingInfoIdx = 0;
			mTargetTroopLink.Set(null); // 최근 타겟이 없다면 기본 타겟 정보도 초기화
		}

		mGroupTargetingInfoIdx = mMainTargetingInfoIdx;
		mGroupTargetingCycleCounter = 0;
		mGroupTargetingCycleLimit = mGroupTargetingInfoCount * 2 - 3;
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 타겟 부대를 얻습니다.
	public BattleTroop GetNextTargetTroop()
	{
		if (mTargetingRatio < 10000) // 100% 조준 상태가 아니라면
		{
			// 현재 타겟이 있을 경우 아직 그 타겟을 공격 전이므로 이를 반환합니다.
			BattleTroop lTargetTroop = _ValidateDetection(mTargetTroopLink.Get());
			if (lTargetTroop != null)
				return lTargetTroop;
		}

		int lLastGroupTargetingInfoIdx = mGroupTargetingInfoIdx; // 저장

		if (mGroupTargetingInfoCount <= 0)
			return _ValidateDetection(mTargetTroopLink.Get()); // 그룹 타게팅 정보가 없으면 현재 타겟을 반환
		if (mGroupTargetingInfoCount == 1)
			return _ValidateDetection(mGroupTargetingInfos[mGroupTargetingInfoIdx].mTargetTroopLink.Get()); // 타겟 하나 뿐이면 계속 같은 타겟(mTargetTroopLink.Get()와 같음)

		bool lIsGroupTargetingCycleCounterReset = false;

		for (int iInfo = 0; iInfo < mGroupTargetingInfoCount; ++iInfo)
		{
			if (mTargetSelectionDirection == TargetSelectionDirection.Right)
			{
				if (mGroupTargetingInfoIdx < (mGroupTargetingInfoCount - 1))
				{
					++mGroupTargetingInfoIdx;
				}
				else
				{
					mTargetSelectionDirection = TargetSelectionDirection.Left;
				}
			}
			else // if (mTargetSelectionDirection == TargetSelectionDirection.Left)
			{
				if (mGroupTargetingInfoIdx > 0)
				{
					--mGroupTargetingInfoIdx;
				}
				else
				{
					mTargetSelectionDirection = TargetSelectionDirection.Right;
				}
			}

			if (!lIsGroupTargetingCycleCounterReset)
			{
				if (++mGroupTargetingCycleCounter >= mGroupTargetingCycleLimit)
				{
					mGroupTargetingCycleCounter = 0;
					lIsGroupTargetingCycleCounterReset = true;
				}
			}

			BattleTroop lNextTargetTroop = mGroupTargetingInfos[mGroupTargetingInfoIdx].mTargetTroopLink.Get();
			//Debug.Log(String.Format("{0} mGroupTargetingInfos[{1}].mAngleVector={2} coord=({3},{4})", mTargetSelectionDirection, mGroupTargetingInfoIdx, mGroupTargetingInfos[mGroupTargetingInfoIdx].mAngleVector, (lNextTargetTroop != null) ? lNextTargetTroop.aCoord.x : 0, (lNextTargetTroop != null) ? lNextTargetTroop.aCoord.z : 0));
			if ((lNextTargetTroop != null) &&
				lNextTargetTroop.aIsDetecteds[mAttackTroop.aTeamIdx])
			{
				mTargetTroopLink.Set(lNextTargetTroop); // 타겟 교체
				return lNextTargetTroop; // 다음 타겟 반환
			}
		}

		// 다음 타겟을 못 찾은 상황에서는 기존 타겟을 반환합니다.
		mGroupTargetingInfoIdx = lLastGroupTargetingInfoIdx; // 원래의 자리로 되돌림
		return _ValidateDetection(mTargetTroopLink.Get()); // 기본 타겟을 반환
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 갱신을 검사합니다.
	public bool CheckTargetUpdate()
	{
		if (mTargetingType == TargetingType.Troop)
		{
			BattleTroop lTargetTroop = mTargetTroopLink.Get();
			Coord2 lUpdateTargetCoord = (lTargetTroop != null) ? lTargetTroop.aCoord : mTargetCoord;
			Coord2 lUpdateTargetVector = lUpdateTargetCoord - aAttackTroop.aCoord;
			if (mTargetVector != lUpdateTargetVector)
			{
				SetTarget(mTargetingType, lUpdateTargetCoord, lTargetTroop);
				return true;
			}

			return false;
		}
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 대해서 공격이 가능한지를 검사합니다.
	public bool CheckInAttackRange()
	{
		if ((mSqrTargetDistance < mAttackTroopStat.aSqrMinAttackRange) ||
			(mSqrTargetDistance > mAttackTroopStat.aSqrMaxAttackRange))
			return false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 무기에 대해서 사격 각도 안에 있는지를 검사합니다.
	public bool CheckInFireConeAngle(bool pIsIgnoreRange)
	{
		for (int iWeapon = 0; iWeapon < mWeaponDataCount; ++iWeapon)
			if (pIsIgnoreRange ||
				((mSqrTargetDistance >= mTargetingWeaponDatas[iWeapon].aSqrMinAttackRange) &&
					(mSqrTargetDistance <= mTargetingWeaponDatas[iWeapon].aSqrMaxAttackRange)))
			{
				if (aTargetAngleDistance <= mTargetingWeaponDatas[iWeapon].aWeaponStatTable.Get(WeaponStat.FireConeAngleHalf))
					return true;
			}

		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 무기에 대해서 해당 좌표가 시야 각도 안에 있는지를 검사합니다.
	public bool CheckInTrackingAngle(bool pIsIgnoreRange)
	{
		for (int iWeapon = 0; iWeapon < mWeaponDataCount; ++iWeapon)
			if (pIsIgnoreRange ||
				((mSqrTargetDistance >= mTargetingWeaponDatas[iWeapon].aSqrMinAttackRange) &&
					(mSqrTargetDistance <= mTargetingWeaponDatas[iWeapon].aSqrMaxAttackRange)))
			{
				if (aTargetAngleDistance <= mTargetingWeaponDatas[iWeapon].aWeaponStatTable.Get(WeaponStat.TrackingAngleHalf))
					return true;
			}

		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 상태를 갱신합니다.
	public void UpdateAimingDirectionY(int pUpdateDirectionY)
	{
		mAimingDirectionY = pUpdateDirectionY;

		mIsValidTargetAngleDistance = false; // 타겟 각도를 다시 계산해야 함
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 대한 조준 딜레이를 구합니다.
	public int GetAimingDelay()
	{
		int lAimingDelay = mMainWeaponData.aWeaponStatTable.Get(WeaponStat.MaxTrackingDelay) * mTargetAngleDistance / 180;
		return lAimingDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟에 대한 명중률을 얻습니다.
	public static int GetHitProb(BattleTroopStat.WeaponData pWeaponData, int pMilliAccuracy, int pTargetHitSize, int pSqrTargetDistance)
	{
		int lSightWeaponAccuracyMultiplyRatio;
		if (pSqrTargetDistance <= pWeaponData.aTroopStat.aSqrSightRange) // 자신의 시야
			lSightWeaponAccuracyMultiplyRatio = 10000;
		else // 아군의 시야(직접 안보이면 잘 안 맞을지도)
			lSightWeaponAccuracyMultiplyRatio = pWeaponData.aAllySightWeaponAccuracyMultiplyRatio;

		int lHitProb
			= (int)((Int64)pMilliAccuracy
			* pTargetHitSize
			* lSightWeaponAccuracyMultiplyRatio // 자신의 시야인지 아군 시야인지에 따른 보정 반영(만분율)
			/ (1000 * 100 * 10000)); // 두 번째 100은 원래 만분율이라서 10000을 나누어야 할 것을 lHitProb이 백분율값이라서 100을 곱하게 되면서 나온 수치임
		if (lHitProb > 100)
			lHitProb = 100;

		return lHitProb;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟에 대한 데미지 기대값을 얻습니다.
	public static int GetExpectedMilliDamage(BattleTroopStat.WeaponData pWeaponData, BattleTroop pTargetTroop, int pSqrTargetDistance)
	{
		if (pTargetTroop != null)
		{
			TroopStatTable lTargetTroopStatTable = pTargetTroop.aStat.aTroopStatTable;
			for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
			{
				WeaponRangeStatTable lWeaponRangeStatTable = pWeaponData.aWeaponRangeStatTables[iRange];
				if (pSqrTargetDistance <= lWeaponRangeStatTable.aSqrRange) // 사정 거리 이내라면
				{
					int lHitProb = GetHitProb(
						pWeaponData,
						lWeaponRangeStatTable.Get(WeaponRangeStat.MilliAccuracy),
						pTargetTroop.aTroopSpec.mHitSize,
						pSqrTargetDistance);

					int lPenetration = lWeaponRangeStatTable.Get(WeaponRangeStat.Penetration);
					if (lPenetration < 1)
						lPenetration = 1;
					int lTargetArmor = lTargetTroopStatTable.Get(TroopStat.FrontArmor);
					if (lTargetArmor <= 0)
						lTargetArmor = 1;

					int lPenetrationProb = 200 - lTargetArmor * 100 / lPenetration;
					//if (lPenetrationProb < 0)
					//	lPenetrationProb = 0;
					//else if (lPenetrationProb > 100)
					//	lPenetrationProb = 100;

					// 데미지에 확률을 곱한 기대값을 얻습니다.
					int lWeaponMilliDamage = lWeaponRangeStatTable.Get(WeaponRangeStat.MilliDamage);
					int lExpectedMilliDamage = (int)((Int64)lWeaponMilliDamage * lHitProb * lPenetrationProb / (100 * 100)); // Prob * Prob
					int lWeaponCooldownDelay = lWeaponRangeStatTable.Get(WeaponRangeStat.CooldownDelay);
					if (lWeaponCooldownDelay > 0)
						lExpectedMilliDamage /= lWeaponCooldownDelay;
					if (lExpectedMilliDamage <= 0)
						lExpectedMilliDamage = 1; // 사정거리 이내라면 최소값을 보장

					// 가장 가까운 거리가 더 큰 기대값이라는 전제로 이후의 처리를 하지 않습니다.
					return lExpectedMilliDamage;
				}
			}
		}
		else
		{
			for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
			{
				WeaponRangeStatTable lWeaponRangeStatTable = pWeaponData.aWeaponRangeStatTables[iRange];
				if (pSqrTargetDistance <= lWeaponRangeStatTable.aSqrRange) // 사정 거리 이내라면
				{
					int lWeaponMilliDamage = lWeaponRangeStatTable.Get(WeaponRangeStat.MilliDamage);
					return lWeaponMilliDamage;
				}
			}
		}

		return 0; // 최대 사정거리 초과로 공격 불가
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타겟에 대해서 터렛은 제외하고 최적의 공격 가능한 무기를 찾습니다.
	public BattleTroopStat.WeaponData GetBestWeaponData(TargetingAngle pTargetingAngle, bool pIsIgnoreRange, int pSelectionTime)
	{
		int lBestMilliDamage = int.MinValue;
		BattleTroopStat.WeaponData lBestWeaponData = null;
		BattleTroop lTargetTroop = mTargetTroopLink.Get();

		for (int iWeapon = 0; iWeapon < mWeaponDataCount; ++iWeapon)
		{
			BattleTroopStat.WeaponData lWeaponData = mTargetingWeaponDatas[iWeapon];

			if (pSelectionTime < lWeaponData.aUseDelayEndTime)
				continue; // 아직 사용 딜레이 안 끝났음

			if (pIsIgnoreRange ||
				((mSqrTargetDistance >= lWeaponData.aSqrMinAttackRange) &&
					(mSqrTargetDistance <= lWeaponData.aSqrMaxAttackRange)))
			{
				if (!mIsAllyTargeting) // 적 대상이라면
				{
					if (((pTargetingAngle == TargetingAngle.FireConeAngle) && (aTargetAngleDistance <= lWeaponData.aWeaponStatTable.Get(WeaponStat.FireConeAngleHalf))) ||
						((pTargetingAngle == TargetingAngle.TrackingAngle) && (aTargetAngleDistance <= lWeaponData.aWeaponStatTable.Get(WeaponStat.TrackingAngleHalf))) ||
						(pTargetingAngle == TargetingAngle.None))
					{
						if (!lWeaponData.mWeaponSpec.mIsAllyTargeting) // 적 대상 무기에 한정
						{
							int lExpectedMilliDamage = GetExpectedMilliDamage(lWeaponData, lTargetTroop, mSqrTargetDistance);
							if (lBestMilliDamage < lExpectedMilliDamage)
							{
								lBestMilliDamage = lExpectedMilliDamage; // 제일 데미지를 많이 주는 무기를 선정
								lBestWeaponData = lWeaponData;
							}
						}
					}
				}
				else // 아군 대상이라면
				{
					if (lWeaponData.mWeaponSpec.mIsAllyTargeting) // 아군 대상 무기에 한정
					{
						lBestWeaponData = lWeaponData;
						break; // 처음 발견한 무기를 선택하고 더이상 비교 안함
					}
				}
			}
		}
		return lBestWeaponData;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타겟에 대해서 터렛은 제외하고 최적의 공격 가능한 무기를 선택합니다.
	public bool SelectBestWeapon(TargetingAngle pTargetingAngle, bool pIsIgnoreRange, int pSelectionTime)
	{
		BattleTroopStat.WeaponData lBestWeaponData = GetBestWeaponData(pTargetingAngle, pIsIgnoreRange, pSelectionTime);
		if (lBestWeaponData != null)
		{
			_SetSelectedWeaponData(lBestWeaponData, 0);
			return true;
		}
		else
			return false; // 선택 실패
	}
	//-------------------------------------------------------------------------------------------------------
	// 착탄 결과를 처리합니다.
	public TroopHitResult RollHitResult(out Coord2 oHitCoord)
	{
		if (mSelectedWeaponData.mWeaponSpec.mIsGroundAttack)
			return RollGroundHitResult(out oHitCoord); // 곡사포 같은 지면 타격 무기는 바로 산탄 계산으로 처리합니다.

		if (mTargetingRatio < 10000) // 100% 조준이 아니라면
			return RollGroundHitResult(out oHitCoord); // 산탄 계산으로 처리합니다.

		BattleTroop lTargetTroop = mTargetTroopLink.Get();
		if (lTargetTroop == null)
			return RollGroundHitResult(out oHitCoord); // 타겟 부대가 없다면 산탄 계산으로 처리합니다.

		TroopStatTable lTargetTroopStatTable = lTargetTroop.aStat.aTroopStatTable;

		int lUnitHitSize = lTargetTroop.aTroopSpec.mHitSize;

		if (mSqrTargetDistance <= mSelectedWeaponData.aSqrMaxAttackRange) // 사정 거리 이내라면
		{
			int lMilliAccuracy
				= mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.MilliAccuracy) // 무기의 조준률에
				+ lTargetTroop.aStat.aTroopStatTable.Get(TroopStat.HitRateMilliAdd) // 타겟의 회피율 적용하고
				+ BattleConfig.get.mMoraleHitRateMilliAdds[(int)lTargetTroop.aStat.aMoraleState] // 부대 사기에 따른 회피율 변화도 적용
				+ BattleConfig.get.mInteractionTypeHitRateMilliAdds[(int)lTargetTroop.aInteraction.aTroopInteractionType]; // 여기에 회복 같은 상호 작용에 따른 회피율 추가
			if (lMilliAccuracy < 0)
				lMilliAccuracy = 0;
			else if (lMilliAccuracy >= cMaxMillAccuracy)
				lMilliAccuracy = cMaxMillAccuracy;

			int lHitProb = GetHitProb(
				mSelectedWeaponData,
				lMilliAccuracy,
				lUnitHitSize,
				mSqrTargetDistance);
		#if BATTLE_VERIFY_TEST
            BattleLogicTestStatistics.Log(aAttackTroop.aGuid, "ResultAccuracy", lHitProb);
		#endif

			if (!mSyncRandom.CheckProb(lHitProb))
				return RollGroundHitResult(out oHitCoord); // 명중하지 않을 경우 산탄 계산으로 처리합니다.

			oHitCoord = mTargetCoord; // 히트하면 직격 여부와 관계없이 착탄 지점은 타겟 좌표가 됨

			int lHitDirectionY = (mTargetCoord - mAttackTroop.aCoord).RoughDirectionY();
			int lDegreeVector = MathUtil.DegreeVector(lHitDirectionY, lTargetTroop.aDirectionY);
			int lAbsDegreeVector = Mathf.Abs(lDegreeVector);

			// 영향을 주는 장갑의 크기와 두께, 개수를 얻습니다.
			int lArmorCount = 0;
			{
				// 피격 유닛의 장갑을 추가합니다.
				sArmorSizes[lArmorCount] = lTargetTroopStatTable.Get(TroopStat.ArmorSize);
				if (sArmorSizes[lArmorCount] > 0)
				{
					if (lAbsDegreeVector >= BattleConfig.get.mSideSuppressionAngleDistance) // 정면 공격
						sArmorAdds[lArmorCount] = lTargetTroopStatTable.Get(TroopStat.FrontArmor);
					else if (lAbsDegreeVector > BattleConfig.get.mRearSuppressionAngleDistance) // 측면 공격 (측면 값은 경계 수치 제외)
						sArmorAdds[lArmorCount] = lTargetTroopStatTable.Get(TroopStat.SideArmor);
					else // 후면 공격
						sArmorAdds[lArmorCount] = lTargetTroopStatTable.Get(TroopStat.RearArmor);

					if (sArmorAdds[lArmorCount] > 0)
						++lArmorCount;
				}

				GroundStatTable lGroundStatTable = lTargetTroop.aAffectedGroundStatTable;

				// 지형 타일의 장갑을 추가합니다.
				if (lGroundStatTable.aTileArmorSpec != null)
				{
					int lTileArmorDegreeVector = MathUtil.DegreeVector(lHitDirectionY + 180, lGroundStatTable.aTileArmorDirectionY);
					if (Mathf.Abs(lTileArmorDegreeVector) <= lGroundStatTable.aTileArmorEffectAngleHalf)
					{
						sArmorSizes[lArmorCount] = lGroundStatTable.aTileArmorSpec.mArmorSize;
						sArmorAdds[lArmorCount] = lGroundStatTable.aTileArmorSpec.mArmorAdd;

						if ((sArmorSizes[lArmorCount] > 0) &&
							(sArmorAdds[lArmorCount] > 0))
							++lArmorCount;
					}
				}

				// 지형 타일의 장갑을 추가합니다.
				if (lGroundStatTable.aSkillArmorSpec != null)
				{
					int lSkillArmorDegreeVector = MathUtil.DegreeVector(lHitDirectionY + 180, lGroundStatTable.aSkillArmorDirectionY);
					if (Mathf.Abs(lSkillArmorDegreeVector) <= lGroundStatTable.aSkillArmorEffectAngleHalf)
					{
						sArmorSizes[lArmorCount] = lGroundStatTable.aSkillArmorSpec.mArmorSize;
						sArmorAdds[lArmorCount] = lGroundStatTable.aSkillArmorSpec.mArmorAdd;

						if ((sArmorSizes[lArmorCount] > 0) &&
							(sArmorAdds[lArmorCount] > 0))
							++lArmorCount;
					}
				}

				// 유닛 크기보다 큰 장갑 영역은 제외합니다.
				for (int iArmor = 0; iArmor < lArmorCount; ++iArmor)
					if (sArmorSizes[iArmor] > lUnitHitSize)
						sArmorSizes[iArmor] = lUnitHitSize;

				// 영향을 주는 장갑을 크기가 큰 것을 맨 앞으로 해서 정렬합니다.
				if (lArmorCount >= 2)
					for (int iArmor = 0; iArmor < lArmorCount; ++iArmor)
					{
						for (int iCompareArmor = (iArmor + 1); iCompareArmor < lArmorCount; ++iCompareArmor)
						{
							if (sArmorSizes[iArmor] < sArmorSizes[iCompareArmor])
							{
								int lTempSize = sArmorSizes[iArmor];
								sArmorSizes[iArmor] = sArmorSizes[iCompareArmor];
								sArmorSizes[iCompareArmor] = lTempSize;

								int lTempAdd = sArmorAdds[iArmor];
								sArmorAdds[iArmor] = sArmorAdds[iCompareArmor];
								sArmorAdds[iCompareArmor] = lTempAdd;
							}
						}
					}
			}

			// 장갑에 의한 직격과 도탄을 판단합니다.
			if (lArmorCount > 0)
			{
				// 장갑을 단계적으로 들어가며 최종 장갑을 계산합니다.
				int lFinalTargetArmor = 0;
				int lHitSize = lUnitHitSize;
				for (int iArmor = 0; iArmor < lArmorCount; ++iArmor)
				{
					int lCheckSize = lHitSize - sArmorSizes[iArmor];
					if (lCheckSize > 0)
					{
						int lCheckProb = (int)((Int64)lCheckSize * lMilliAccuracy / (1000 * 100));
						if (mSyncRandom.CheckProb(lCheckProb)) // 해당 빈 영역을 맞추었다면
							break; // 이 장갑을 포함한 이후의 다른 장갑을 더하지 않음
					}

					lFinalTargetArmor += sArmorAdds[iArmor]; // 현재 장갑을 더하고
					lHitSize = sArmorSizes[iArmor]; // 다음 장갑을 검사
				}

				// 장갑 체크 이후의 최종 장갑(lFinalTargetArmor)를 가지고 장갑을 뚫고 직격할 지 아니면 도탄할 지를 판단합니다.
				int lPenetration = mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.Penetration);
				if (lPenetration < 1)
					lPenetration = 1;
				int lPenetrationProb = 200 - lFinalTargetArmor * 100 / lPenetration;
				if (lPenetrationProb < 0)
					lPenetrationProb = 0;
				if (mSyncRandom.CheckProb(lPenetrationProb))
					return TroopHitResult.Penetrate; // 직격(장갑을 뚫고)
				else
					return TroopHitResult.Block; // 도탄
			}
			else
			{
				return TroopHitResult.Penetrate; // 직격(장갑 없음)
			}
		}

		oHitCoord = mTargetCoord;
		return TroopHitResult.Miss; // 유실
	}
	//-------------------------------------------------------------------------------------------------------
	// 산탄 처리를 합니다.
	public TroopHitResult RollGroundHitResult(out Coord2 oHitCoord)
	{
		// 100% 조준이 아닐 경우에는 중간 탄착점을 반환합니다.
		if (mTargetingRatio < 10000)
		{
			oHitCoord = Coord2.RoughLerp(mLastTargetCoord, mTargetCoord, mTargetingRatio, 10000); // // 이전 타겟 지점과 현재 타겟 지점 사이의 중간 발사 지점

			return TroopHitResult.GroundHit; // 산탄
		}

		// 타겟 좌표에서 벗어난 착탄 지점을 발사 위치를 기준으로한 벡터 값으로 구합니다.
		int lScatterAngleHalf = mSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.ScatterAngleHalf);
		int lScatterOffset = mSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.ScatterOffset);

		if (!mIsInSightRange) // 아군의 시야(직접 안보이면 잘 안 맞을지도)
		{
			lScatterAngleHalf = (int)((Int64)lScatterAngleHalf * mSelectedWeaponData.aAllySightWeaponScatterMultiplyRatio / 10000);
			lScatterOffset = (int)((Int64)lScatterOffset * mSelectedWeaponData.aAllySightWeaponScatterMultiplyRatio / 10000);
		}

		int lTargetDistance = aTargetDistance;
		int lTargetDistanceRangeMin = lTargetDistance - lScatterOffset;
		if (lTargetDistanceRangeMin < 0)
			lTargetDistanceRangeMin = 0;

	#if BATTLE_VERIFY_TEST
        int randomedAngle = mSyncRandom.NormalDistributionRange(aTargetDirectionY - lScatterAngleHalf, aTargetDirectionY + lScatterAngleHalf + 1);
        int randomedOffset = mSyncRandom.NormalDistributionRange(-lScatterOffset, lScatterOffset);

        BattleLogicTestStatistics.Log(aAttackTroop.aGuid, "ResultScatterAngle", randomedAngle);
        BattleLogicTestStatistics.Log(aAttackTroop.aGuid, "ResultScatterDistance", randomedOffset);
	#endif

		Coord2 lHitVector = Coord2.RoughDirectionVector(
			mSyncRandom.NormalDistributionRange(aTargetDirectionY - lScatterAngleHalf, aTargetDirectionY + lScatterAngleHalf + 1),
			mSyncRandom.NormalDistributionRange(lTargetDistanceRangeMin, lTargetDistance + lScatterOffset));

		oHitCoord = mAttackTroop.aCoord + lHitVector; // 착탄 좌표

		return TroopHitResult.GroundHit; // 산탄
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 타겟을 공격할 때의 와인드업 딜레이를 얻습니다.
	public int GetWindupDelay()
	{
		int lWindupDelay
			= (int)((Int64)mSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.WindupDelay)
			* mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.AttackDelayMultiplyRatio)
			/ 10000); // 만분율
		return lWindupDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 타겟을 공격할 때의 조준 딜레이를 얻습니다.
	public int GetAimDelay()
	{
		int lAimDelay
			= (int)((Int64)mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.AimDelay)
			* mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.AttackDelayMultiplyRatio)
			/ 10000); // 만분율
		return lAimDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 타겟을 공격할 때의 쿨다운 딜레이를 얻습니다.
	public int GetCooldownDelay()
	{
		int lCooldownDelay
			= (int)((Int64)mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.CooldownDelay)
			* mSelectedWeaponRangeStatTable.Get(WeaponRangeStat.AttackDelayMultiplyRatio)
			/ 10000); // 만분율
		return lCooldownDelay;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 부대 스탯 변화를 반영합니다.
	private void _UpdateStat()
	{
		mIsUpdateStat = false; // 갱신 했음

		mWeaponDataCount = 0;
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			if (mAttackTroopStat.aWeaponDatas[iWeapon] == null)
				continue;

			if (((mControlWeapon == BattleTroopAttackControl.ControlWeapon.BodyWeapon) && mAttackTroopSpec.mIsBodyWeapons[iWeapon]) ||
				((mControlWeapon == BattleTroopAttackControl.ControlWeapon.TurretWeapon) && mAttackTroopSpec.mIsTurretWeapons[iWeapon]))
			{
				mTargetingWeaponDatas[mWeaponDataCount++] = mAttackTroopStat.aWeaponDatas[iWeapon];

				//if (iWeapon == TroopSpec.cVeteranWeaponIdx)
				//	Debug.Log("Veteran Weapon:" + mAttackTroopStat.mTargetingWeaponDatas[iWeapon].mWeaponSpec + " - " + this);
			}
		}
		for (int iWeapon = mWeaponDataCount; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			mTargetingWeaponDatas[iWeapon] = null;

		mMainWeaponData = mTargetingWeaponDatas[0];
		if (mMainWeaponData == null)
			return;

		mMaxTrackingDelay = mMainWeaponData.aWeaponStatTable.Get(WeaponStat.MaxTrackingDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 속도를 갱신합니다.
	private void _UpdateRotationDelay()
	{
		if (!mAttackTroop.aControlState.mIsSetUpMode && // 셋업 모드(먼저 설치하고 공격하는 모드)가 아니고
			(mMainWeaponData != null)) // 무기가 있다면
		{
			if (mAttackTroopSpec.mIsBodyWeapons[mMainWeaponData.mWeaponIdx])
				mMaxRotationDelay = mAttackTroopStatTable.Get(TroopStat.MaxRotationDelay); // 본체 무기의 회전 속도는 부대의 속성이고
			else if (mAttackTroopSpec.mIsTurretWeapons[mMainWeaponData.mWeaponIdx])
				mMaxRotationDelay = mMaxTrackingDelay; // 터렛 무기의 회전 속도는 무기의 속성이다.
			else
				mMaxRotationDelay = 0;
		}
		else // 셋업 모드거나 무기가 없다면
		{
			mMaxRotationDelay = 0; // 회전 없음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 선택합니다.
	private void _SetSelectedWeaponData(BattleTroopStat.WeaponData pSelectedWeaponData, int pRangeCheckStartLevel)
	{
		mSelectedWeaponData = pSelectedWeaponData;

		// 사정거리 레벨을 지정합니다.
		mSelectedWeaponRangeLevel = WeaponSpec.cMaxRangeLevel;
		for (int iRange = pRangeCheckStartLevel; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
		{
			mSelectedWeaponRangeStatTable = mSelectedWeaponData.aWeaponRangeStatTables[iRange];
			if (mSqrTargetDistance <= mSelectedWeaponRangeStatTable.aSqrRange) // 사정 거리 이내라면
			{
				mSelectedWeaponRangeLevel = iRange;
				break;
			}
		}

		// 타겟을 그룹으로 찾을지를 지정합니다.
		if (mSelectedWeaponData.mWeaponSpec.mAttackType == WeaponAttackType.MachineGun)
			mIsGroupTargeting = true;
		else
			mIsGroupTargeting = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐지된 경우에만 부대를 반환합니다.
	private BattleTroop _ValidateDetection(BattleTroop pTroop)
	{
		if (pTroop == null)
			return null;

		if (pTroop.aIsDetecteds[mAttackTroop.aTeamIdx])
			return pTroop;
		else
			return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mAttackTroop;
	private SyncRandom mSyncRandom;
	private TroopSpec mAttackTroopSpec;
	private BattleTroopStat mAttackTroopStat;
	private TroopStatTable mAttackTroopStatTable;
	private BattleTroopAttackControl.ControlWeapon mControlWeapon;
	private BattleTroopStat.WeaponData[] mTargetingWeaponDatas;
	private int mWeaponDataCount;
	private BattleTroopStat.WeaponData mMainWeaponData;
	private bool mIsUpdateStat;

	private BattleTroopStat.WeaponData mSelectedWeaponData;
	private int mSelectedWeaponRangeLevel;
	private WeaponRangeStatTable mSelectedWeaponRangeStatTable;
	private bool mIsInstalled;

	private int mMaxRotationDelay;
	private int mMaxTrackingDelay;
	private int mAimingDirectionY;

	private TargetingType mTargetingType;
	private Coord2 mTargetCoord;
	private Coord2 mLastTargetCoord;
	private GuidLink<BattleTroop> mTargetTroopLink;
	private Coord2 mTargetVector;
	private int mSqrTargetDistance;
	private bool mIsInSightRange;
	private int mTargetingRatio;

	private GroupTargetingInfo[] mGroupTargetingInfos;
	private int mGroupTargetingInfoCount;
	private TargetSelectionDirection mTargetSelectionDirection;
	private bool mIsGroupTargeting;
	private int mMainTargetingInfoIdx;
	private int mGroupTargetingInfoIdx;
	private int mGroupTargetingCycleCounter;
	private int mGroupTargetingCycleLimit;

	private bool mIsValidTargetDistance;
	private int mTargetDistance;

	private bool mIsValidTargetAngleDistance;
	private int mTargetAngleDistance;

	private bool mIsValidTargetDirectionY;
	private int mTargetDirectionY;

	private bool mIsAllyTargeting;

	private static int[] sArmorSizes = { 0, 0, 0 }; // 최대 3개(유닛, 타일, 스킬)의 장갑을 계산하기 위한 임시 공간
	private static int[] sArmorAdds = { 0, 0, 0 };
}
