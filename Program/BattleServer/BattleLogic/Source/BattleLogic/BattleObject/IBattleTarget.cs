using UnityEngine;
using System.Collections;
using System;

public interface IBattleTarget
{
	int aTeamIdx { get; }
	BattleTroop aTargetTroop { get; }
	BattleTargetingDummy aTargetingDummy { get; }
	Coord2 aTargetCoord { get; }
}
