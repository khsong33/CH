using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class BattleUser
{
	public enum ControlType
	{
		Manual,
		Auto,
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보
	public BattleUserInfo aUserInfo
	{
		get { return mUserInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 인덱스
	public int aUserIdx
	{
		get { return mUserIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀 인덱
	public int aTeamIdx
	{
		get { return mTeamIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 본부
	public BattleHeadquarter aHeadquarter
	{
		get { return mHeadquarter; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 타입
	public ControlType aControlType
	{
		get { return mControlType; }
		set
		{
			if (mControlType == value)
				return;

			mControlType = value;

			for (int iCommander = 0; iCommander < mValidCommanderCount; ++iCommander)
				if (mCommanders[iCommander] != null)
					mCommanders[iCommander].UpdateAiState(false); // AI 상태 갱신
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// AI
	public BattleAiManager aAiManager
	{
		get { return mAiManager; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관들
	public BattleCommander[] aCommanders
	{
		get { return mCommanders; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 MP
	public int aCurrentMp
	{
		get { return mProgressInfo.aUserMps[mUserIdx]; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 MP
	public int aCurrentExtraMilliMp
	{
		get { return mProgressInfo.aUserExtraMilliMps[mUserIdx]; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 원래 가지고 있는 지휘관 수
	public int aValidCommanderCount
	{
		get { return mValidCommanderCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 살아있는 지휘관 수
	public int aLiveCommanderCount
	{
		get { return mLiveCommanderCount; }
		set
		{
			mLiveCommanderCount = value;
			mProgressInfo.UpdateLiveCommanderCounts(mBattleGrid);
			Debug.Log("Live Commander Count - " + mLiveCommanderCount + "/UserIdx - " + mUserIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 소환 가능 부대수
	public int aMaxTroopCount
	{
		get { return mMaxTroopCount; }
		set { mMaxTroopCount = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 소환된 부대수
	public int aCurrentTroopCount
	{
		get { return mCurrentTroopCount; }
		set { mCurrentTroopCount = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AI 로그를 남길지 여부
	public bool aIsLogAi
	{
		get { return mIsLogAi; }
		set { mIsLogAi = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleUser(BattleLogic pBattleLogic)
	{
		mBattleLogic = pBattleLogic;

		mAiManager = new BattlePvpAiManager();

		mCommanders = new BattleCommander[BattleUserInfo.cMaxCommanderCount];

		mConquerCheckPoints = new Dictionary<int, BattleCheckPoint>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateUser(
		BattleGrid pBattleGrid,
		BattleUserInfo pUserInfo,
		ControlType pControlType)
	{
		//Debug.Log("OnCreateUser() - " + this);

		mBattleGrid = pBattleGrid;
		mProgressInfo = pBattleGrid.aProgressInfo;
		mUserInfo = pUserInfo;
		mUserIdx = pUserInfo.mUserIdx;
		mTeamIdx = pUserInfo.mTeamIdx;
		mHeadquarter = mBattleGrid.FindHeadquarterByGuid(pUserInfo.mHeadquarterGuid);
		if (mHeadquarter == null)
			Debug.LogError("can't find Headquarter Guid:" + pUserInfo.mHeadquarterGuid + " - " + this);
		mControlType = pControlType;

		mBattleGrid.SetUser(mUserIdx, this);

		for (int iCommander = 0; iCommander < BattleUserInfo.cMaxCommanderCount; ++iCommander)
			mCommanders[iCommander] = null;
		mValidCommanderCount = mUserInfo.aValidCommanderItemCount;
		mLiveCommanderCount = mValidCommanderCount;
		mCurrentTroopCount = 0;

		// 본부에 연결합니다.
		mHeadquarter.aLinkUser = this;

		mAiCollector = mBattleGrid.aAiCollector;
		mIsLogAi = false;

		mAiManager.Init(this, mBattleGrid, mAiCollector);

		mMaxTroopCount = BattleConfig.get.mMaxTroopNum;

		RefreshSpawnInfo();

		for (int iCheckPoint = 0; iCheckPoint < mBattleGrid.aCheckPointList.Count; iCheckPoint++)
			mBattleGrid.aCheckPointList[iCheckPoint].aOnTeamChangeDelegate += _OnTeamChangeDelegate;

		mConquerCheckPoints.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴합니다.
	public void Destroy()
	{
		for (int iCheckPoint = 0; iCheckPoint < mBattleGrid.aCheckPointList.Count; iCheckPoint++)
			mBattleGrid.aCheckPointList[iCheckPoint].aOnTeamChangeDelegate -= _OnTeamChangeDelegate;

		for (int iCommander = 0; iCommander < mValidCommanderCount; ++iCommander)
			if (mCommanders[iCommander] != null)
				mCommanders[iCommander].Destroy();

		mAiManager.Reset();

		mBattleGrid.ResetUser(mUserIdx);

		mBattleLogic.DestroyUser(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public void UpdateFrame()
	{
		mAiManager.UpdateFrame();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까이 있는 지휘관을 찾습니다.
	public BattleCommander SearchNearestCommander(Coord2 pSearchCoord)
	{
		int lSqrNearestCommanderDistance = int.MaxValue;
		BattleCommander lNearestCommander = null;

		for (int iCommander = 0; iCommander < mValidCommanderCount; ++iCommander)
			if ((mCommanders[iCommander] != null) &&
				mCommanders[iCommander].aIsAlive)
			{
				int lSqrCommanderDistance = (mCommanders[iCommander].aCoord - pSearchCoord).SqrMagnitude();
				if (lSqrNearestCommanderDistance > lSqrCommanderDistance)
				{
					lSqrNearestCommanderDistance = lSqrCommanderDistance;
					lNearestCommander = mCommanders[iCommander];
				}
			}
		
		return lNearestCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환이 가능한 지휘관이 있는지 확인
	public bool IsSpawnAvailable()
	{
		if (mMinSpawnAvaliableMp <= mProgressInfo.aUserMps[mUserIdx])
			return true;
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환할 병력의 수를 확인합니다.
	public bool IsSpawnAvailableTroopNum(CommanderItem pCommanderItem)
	{
		return ((mCurrentTroopCount + pCommanderItem.mTroopSet.mTroopCount) <= aMaxTroopCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환 후 처리를 진행합니다. 최소 소환 mp 계산을 위한 로직입니다.
	public void RefreshSpawnInfo()
	{
		mMinSpawnAvaliableMp = int.MaxValue;
		for (int iSlotIdx = 0; iSlotIdx < BattleUserInfo.cMaxCommanderItemCount; iSlotIdx++)
		{
			if (!mUserInfo.mCommanderItems[iSlotIdx].IsSpawnAvailable())
				continue;
			if (!IsSpawnAvailableTroopNum(mUserInfo.mCommanderItems[iSlotIdx]))
				continue;

			if (mMinSpawnAvaliableMp > mUserInfo.mCommanderItems[iSlotIdx].aSpawnMp)
				mMinSpawnAvaliableMp = mUserInfo.mCommanderItems[iSlotIdx].aSpawnMp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환 가능 여부를 검사합니다.
	public bool CheckSpawnable(CommanderItem pSpawnCommanderItem)
	{
		if (!pSpawnCommanderItem.IsSpawnAvailable())
			return false; // 재소환 불가
		if (!mUserInfo.IsSpawnAvailableSlot())
			return false; // 남는 지휘관 슬롯 없음
		if (!IsSpawnAvailableTroopNum(pSpawnCommanderItem))
			return false; // 최대 부대수 초과
		if (mProgressInfo.aUserMps[mUserIdx] < pSpawnCommanderItem.aSpawnMp)
			return false; // MP 부족

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 종료 후 경험치 계산 - Battle Verify에서 사용함. 각 BattleUser별로 이 함수를 한번씩 호출해 준 후 결과 내용을 redis에 저장합니다.
	// 현재는 Echo 서버에 사용할 내용을 저장합니다.
	public JSONObject CalcBattleEndPvPReward(BattleProtocol.BattleEndCode lBattleEndCode)
	{
		JSONObject lResultReward = new JSONObject(); 

		int lBattleWinExp = 0;
		if (lBattleEndCode == BattleProtocol.BattleEndCode.PlayWin)
		{
			lBattleWinExp = BattleConfig.get.mVitoryBonusExp;
			mUserInfo.mWin++;
			mUserInfo.mWinStreak++;
			mUserInfo.mLoseStreak = 0;
		}
		else if (lBattleEndCode == BattleProtocol.BattleEndCode.PlayLose)
		{
			mUserInfo.mLose++;
			mUserInfo.mLoseStreak++;
			mUserInfo.mWinStreak = 0;
		}

		for (int iCommander = 0; iCommander < mValidCommanderCount; iCommander++)
		{
			if (mCommanders[iCommander] == null)
				continue;
			// 전투 종료시 살아 있으면 생존 경험치 추가
			mCommanders[iCommander].aCommanderItem.AddItemExp(BattleConfig.get.mSuvivorBonusExp);
		}

		int lTotalUserUpdateExp = 0;
		JSONObject lCommanderReward = new JSONObject(JSONObject.Type.ARRAY);
		for (int iCommanderItem = 0; iCommanderItem < BattleUserInfo.cMaxCommanderItemCount; iCommanderItem++)
		{
			if (mUserInfo.mCommanderItems[iCommanderItem] == null)
				continue;

			CommanderItem lCommanderItem = mUserInfo.mCommanderItems[iCommanderItem];

			JSONObject lCommanderItemExpResult = new JSONObject();

			int lAccumExp = lCommanderItem.aAccumExp + lBattleWinExp;
			lAccumExp = 0; // 현재 경험치는 적용되지 않도록 수정
			int lResultCommanderExp = lCommanderItem.mCommanderExp + lAccumExp;
	
			int lResultRank = 0;
			for (var iRank = lCommanderItem.mCommanderExpRank; iRank < CommanderSpec.cRankCount; iRank++)
			{
				if (lResultCommanderExp >= lCommanderItem.mCommanderSpec.mRankNeedExps[iRank])
					lResultRank = iRank;
				else
					break;
			}
			lTotalUserUpdateExp += lAccumExp;

			lCommanderItemExpResult.AddField("ItemId", lCommanderItem.mCommanderItemIdx);
			lCommanderItemExpResult.AddField("Exp", lResultCommanderExp);
			lCommanderItemExpResult.AddField("Rank", lResultRank);
			lCommanderReward.Add(lCommanderItemExpResult);
		}
		lResultReward.AddField("CommanderExp", lCommanderReward);
		lResultReward.AddField("Nation", (int)mUserInfo.mBattleNation);

		NationLevelSpec lCurrentNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(mUserInfo.mNationData[mUserInfo.mBattleNation].mNationLevel);
		if (lTotalUserUpdateExp > lCurrentNationLevelSpec.mLimitGainExp)
			lTotalUserUpdateExp = lCurrentNationLevelSpec.mLimitGainExp;

		lTotalUserUpdateExp = 0; // 현재 경험치는 주어지지 않도록 처리
		int lResultNationExp = mUserInfo.mNationData[mUserInfo.mBattleNation].mNationExp + lTotalUserUpdateExp;

		lResultReward.AddField("NationExp", lResultNationExp);
		// User Level 처리를 추가합니다.
		int lStartNationLevel = mUserInfo.mNationData[mUserInfo.mBattleNation].mNationLevel;
		int lResultLevel = -1;
		for (int iLevel = lStartNationLevel; iLevel <= NationLevelTable.get.aMaxLevel; iLevel++)
		{
			NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(iLevel);
			if (lNationLevelSpec == null)
			{
				Debug.LogError("Not Exist Level - " + iLevel);
				lResultLevel = NationLevelTable.get.aMaxLevel;
				break;
			}
			if (lNationLevelSpec.mNeedExp <= lResultNationExp && lNationLevelSpec.mMaxExp > lResultNationExp)
			{
				lResultLevel = iLevel;
				break;
			}
		}
		lResultReward.AddField("NationLevel", lResultLevel); 

		// 레벨업에 따른 지휘관 아이템 보상 처리
		JSONObject lRewardCommanderItemSpecNo = new JSONObject();
		//if (lStartNationLevel != lResultLevel)
		//{
		//	for (int iLevel = lStartNationLevel; iLevel < lResultLevel; iLevel++)
		//	{
		//		NationLevelSpec lLevelData = NationLevelTable.get.GetNationLevelSpec(iLevel);
		//		if (lLevelData.mLevelUpNationRewardSpecNo[(int)mUserInfo.mBattleNation] > 0)
		//		{
		//			lRewardCommanderItemSpecNo.Add(lLevelData.mLevelUpNationRewardSpecNo[(int)mUserInfo.mBattleNation]);
		//		}
		//	}
		//}
		lResultReward.AddField("RewardCommander", lRewardCommanderItemSpecNo);
		lResultReward.AddField("Win", mUserInfo.mWin);
		lResultReward.AddField("WinStreak", mUserInfo.mWinStreak);
		lResultReward.AddField("Lose", mUserInfo.mLose);
		lResultReward.AddField("LoseStreak", mUserInfo.mLoseStreak);

		return lResultReward;
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 종료 후 경험치 계산 - Battle Verify에서 사용함. 각 BattleUser별로 이 함수를 한번씩 호출해 준 후 결과 내용을 redis에 저장합니다.
	// 현재는 Echo 서버에 사용할 내용을 저장합니다.
	public JSONObject CalcBattleEndTutorialReward(BattleProtocol.BattleEndCode lBattleEndCode)
	{
		JSONObject lResultReward = new JSONObject();

		lResultReward.AddField("Win", mUserInfo.mWin);
		lResultReward.AddField("WinStreak", mUserInfo.mWinStreak);
		lResultReward.AddField("Lose", mUserInfo.mLose);
		lResultReward.AddField("LoseStreak", mUserInfo.mLoseStreak);

		JSONObject lCommanderReward = new JSONObject(JSONObject.Type.ARRAY);
		int lTotalUserUpdateExp = 0;
		for (int iCommanderItem = 0; iCommanderItem < BattleUserInfo.cMaxCommanderItemCount; iCommanderItem++)
		{
			if (mUserInfo.mCommanderItems[iCommanderItem].mCommanderSpec == null)
				continue;

			CommanderItem lCommanderItem = mUserInfo.mCommanderItems[iCommanderItem];

			JSONObject lCommanderItemExpResult = new JSONObject();

			int lAccumExp = lCommanderItem.aAccumExp;
			int lResultCommanderExp = lCommanderItem.mCommanderExp + lAccumExp;

			int lResultRank = 0;
			for (var iRank = lCommanderItem.mCommanderExpRank; iRank < CommanderSpec.cRankCount; iRank++)
			{
				if (lResultCommanderExp >= lCommanderItem.mCommanderSpec.mRankNeedExps[iRank])
					lResultRank = iRank;
				else
					break;
			}
			lTotalUserUpdateExp += lAccumExp;

			int lItemIdx = mUserInfo.mCommanderItems[iCommanderItem].mCommanderItemIdx;
			lCommanderItemExpResult.AddField("ItemId", lItemIdx);
			lCommanderItemExpResult.AddField("Exp", lResultCommanderExp);
			lCommanderItemExpResult.AddField("Rank", lResultRank);
			lCommanderReward.Add(lCommanderItemExpResult);
		}
		lResultReward.AddField("CommanderExp", lCommanderReward);
		lResultReward.AddField("Nation", (int)mUserInfo.mBattleNation);

		int lResultNationExp = mUserInfo.mNationData[mUserInfo.mBattleNation].mNationExp + lTotalUserUpdateExp;
		lResultReward.AddField("NationExp", lResultNationExp);

		// User Level 처리를 추가합니다.
		int lStartNationLevel = mUserInfo.mNationData[mUserInfo.mBattleNation].mNationLevel;
		int lResultLevel = -1;
		for (int iLevel = lStartNationLevel; iLevel <= NationLevelTable.get.aMaxLevel; iLevel++)
		{
			NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(iLevel);
			if (lNationLevelSpec == null)
			{
				Debug.LogError("Not Exist Level - " + iLevel);
				lResultLevel = NationLevelTable.get.aMaxLevel;
				break;
			}
			if (lNationLevelSpec.mNeedExp <= lResultNationExp && lNationLevelSpec.mMaxExp > lResultNationExp)
			{
				lResultLevel = iLevel;
				break;
			}
		}
		lResultReward.AddField("NationLevel", lResultLevel);

		// 레벨업에 따른 지휘관 아이템 보상 처리
		JSONObject lRewardCommanderItemSpecNo = new JSONObject();
		if (lStartNationLevel != lResultLevel)
		{
			for (int iLevel = lStartNationLevel; iLevel < lResultLevel; iLevel++)
			{
				NationLevelSpec lLevelData = NationLevelTable.get.GetNationLevelSpec(iLevel);
				lRewardCommanderItemSpecNo.Add(lLevelData.mLevelUpNationRewardSpecNo[(int)mUserInfo.mBattleNation]);
			}
		}
		lResultReward.AddField("RewardCommander", lRewardCommanderItemSpecNo);

		return lResultReward;
	}
	//-------------------------------------------------------------------------------------------------------
	// 대전 종료 보상
	public JSONObject CalcBattleEndPvPReward2(BattleProtocol.BattleEndCode lBattleEndCode)
	{
		JSONObject lResultReward = new JSONObject();
		// 사전 팀 인원 확인
		int lTeamUserCount = 0;
		int lTeamTotalRating = 0;
		int lTeamLiveCommanderCount = 0;
		int lEnemyUserCount = 0;
		int lEnemyTotalRating = 0;
		int lEnemyLiveCommanderCount = 0;

		int lAveTeamRating = 0;
		int lAveEnemyRating = 0;
		int lMedalCount = 0;
		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; iUser++)
		{
			if (mBattleGrid.aUsers[iUser] == null)
				continue;
			// 같은 팀인 경우
			if (mBattleGrid.aUsers[iUser].aTeamIdx == mUserInfo.mTeamIdx)
			{
				lTeamTotalRating += mBattleGrid.aUsers[iUser].aUserInfo.mRating;
				lTeamLiveCommanderCount += mBattleGrid.aUsers[iUser].aLiveCommanderCount;
				lTeamUserCount++;
			}
			else
			{
				lEnemyTotalRating += mBattleGrid.aUsers[iUser].aUserInfo.mRating;
				lEnemyLiveCommanderCount += mBattleGrid.aUsers[iUser].aLiveCommanderCount;
				lEnemyUserCount++;
			}
		}
		lAveTeamRating = Mathf.CeilToInt(lTeamTotalRating / lTeamUserCount);
		lAveEnemyRating = Mathf.CeilToInt(lEnemyTotalRating / lEnemyUserCount);
		lMedalCount = Mathf.CeilToInt((float)((BattleUserInfo.cMaxCommanderCount * lEnemyUserCount) - lEnemyLiveCommanderCount) / lEnemyUserCount);

		int lGainRating = 0;
		// 승패 처리
		if (lBattleEndCode == BattleProtocol.BattleEndCode.PlayWin)
		{
			mUserInfo.mWin++;
			mUserInfo.mWinStreak++;
			mUserInfo.mLoseStreak = 0;
			lGainRating = Mathf.FloorToInt(-0.084f * (lAveTeamRating - lAveEnemyRating) + 29.4f);
		}
		else if (lBattleEndCode == BattleProtocol.BattleEndCode.PlayLose)
		{
			mUserInfo.mLose++;
			mUserInfo.mLoseStreak++;
			mUserInfo.mWinStreak = 0;
			lGainRating = Mathf.FloorToInt(-0.082f * (lAveTeamRating - lAveEnemyRating) - 29.4f);
		}
		mUserInfo.mRating += lGainRating;
		if (mUserInfo.mRating < 0)
			mUserInfo.mRating = 0;

		JSONObject lUserInfoUpdate = new JSONObject();
		lUserInfoUpdate.AddField("BattleEndCode", (int)lBattleEndCode);
		lUserInfoUpdate.AddField("Win", mUserInfo.mWin);
		lUserInfoUpdate.AddField("WinStreak", mUserInfo.mWinStreak);
		lUserInfoUpdate.AddField("Lose", mUserInfo.mLose);
		lUserInfoUpdate.AddField("LoseStreak", mUserInfo.mLoseStreak);
		lUserInfoUpdate.AddField("Medal", lMedalCount);
		lUserInfoUpdate.AddField("Rating", mUserInfo.mRating);

		lResultReward.AddField("UserInfo", lUserInfoUpdate);

		return lResultReward;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 팀이 바뀐 check point를 확인합니다.
	public void _OnTeamChangeDelegate(BattleCheckPoint pCheckPoint)
	{
		// 빼앗길 때 처리
		if (pCheckPoint.aTeamIdx != aTeamIdx)
		{
			if (mConquerCheckPoints.ContainsKey(pCheckPoint.aCheckPointIdx))
			{
				if (pCheckPoint.aCheckPointType == BattleCheckPoint.CheckPointType.BroadCastTower)
				{
					aMaxTroopCount -= BattleConfig.get.mRTPointAddTroopNum;
				}
				mConquerCheckPoints.Remove(pCheckPoint.aCheckPointIdx);
			}
		}
		else // 획득 시 처리
		{
			if (pCheckPoint.aCheckPointType == BattleCheckPoint.CheckPointType.BroadCastTower)
			{
				aMaxTroopCount += BattleConfig.get.mRTPointAddTroopNum;
			}
			mConquerCheckPoints.Add(pCheckPoint.aCheckPointIdx, pCheckPoint);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;

	private BattleGrid mBattleGrid;
	private BattleProgressInfo mProgressInfo;
	private BattleUserInfo mUserInfo;
	private int mUserIdx;
	private int mTeamIdx;
	private BattleHeadquarter mHeadquarter;
	private ControlType mControlType;

	private BattleAiManager mAiManager;
	private BattleCommander[] mCommanders;

	private BattleAiCollector mAiCollector;

	private int mValidCommanderCount;
	private int mLiveCommanderCount;

	private int mMaxTroopCount;
	private int mCurrentTroopCount;

	private int mMinSpawnAvaliableMp;

	private bool mIsLogAi;

	private Dictionary<int, BattleCheckPoint> mConquerCheckPoints;
}
