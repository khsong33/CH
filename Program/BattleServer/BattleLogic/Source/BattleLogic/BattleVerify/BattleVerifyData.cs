﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System;
using CombatJson;

public class BattleVerifyData
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 대전 정보
	public BattleMatchInfo aMatchInfo
	{
		get { return mMatchInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력 버퍼
	public List<BattleFrameInput> aFrameInputList
	{
		get { return mFrameInputList; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleVerifyData(BattleLogic pBattleLogic)
	{
		mBattleLogic = pBattleLogic;

		mMatchInfo = new BattleMatchInfo();

		mFrameInputList = new List<BattleFrameInput>();
		mInputDataStringBuilder = new StringBuilder();

		OnCreateVerifyData();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateVerifyData()
	{
		//Debug.Log("OnCreate() - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 객체를 파괴합니다.
	public void Destroy()
	{
		Reset();

		// 풀에 돌려놓습니다.
		mBattleLogic.DestroyVerifyData(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화합니다.
	public void Reset()
	{
		for (int iInput = 0; iInput < mFrameInputList.Count; ++iInput)
		{
			mFrameInputList[iInput].Destroy();
			mFrameInputList[iInput] = null;
		}
		mFrameInputList.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 추가합니다.
	public void RecordFrameTick(BattleFrameTick pFrameTick)
	{
		for (int iInput = 0; iInput < pFrameTick.aFrameInputList.Count; ++iInput)
		{
			BattleFrameInput lTickFrameInput = pFrameTick.aFrameInputList[iInput];
			BattleFrameInput lRecordFrameInput = BattleFrameInput.Create(lTickFrameInput);
			mFrameInputList.Add(lRecordFrameInput);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 추가합니다.
	public void AddFrameInput(String pFrameInputJsonString)
	{
		BattleFrameInput lFrameInput = BattleFrameInput.Create(pFrameInputJsonString);
		mFrameInputList.Add(lFrameInput);
	}
	//-------------------------------------------------------------------------------------------------------
	// 텍스트 파일로 저장합니다.
	public void SaveFile(String pSaveFilePath)
	{
		using (FileStream lFileStream = new FileStream(pSaveFilePath, FileMode.Create))
		{
			StreamWriter lStreamWriter = new StreamWriter(lFileStream);

			String lMatchInfoJsonString = mMatchInfo.ToJson().ToString();
			lStreamWriter.WriteLine(lMatchInfoJsonString);

			for (int iInput = 0; iInput < mFrameInputList.Count; ++iInput)
			{
				mInputDataStringBuilder.Length = 0;
				mFrameInputList[iInput].AppendVerifyDataJsonString(mInputDataStringBuilder);
				lStreamWriter.WriteLine(mInputDataStringBuilder.ToString());
			}

			lStreamWriter.Close();
			lFileStream.Close();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 텍스트 파일에서 읽어옵니다.
	public void LoadFile(String pLoadFilePath)
	{
		Reset();

		using (FileStream lFileStream = new FileStream(pLoadFilePath, FileMode.Open))
		{
			StreamReader lStreamReader = new StreamReader(lFileStream);

			String lMatchInfoJsonString = lStreamReader.ReadLine();
			JSONObject lMatchInfoJson = new JSONObject(lMatchInfoJsonString);
			mMatchInfo.InitFromJson(lMatchInfoJson);

			for (;;)
			{
				String lFrameInputJsonString = lStreamReader.ReadLine();
				if (lFrameInputJsonString == null)
					break;

				BattleFrameInput lFrameInput = BattleFrameInput.Create(lFrameInputJsonString);
				mFrameInputList.Add(lFrameInput);
			}

			lStreamReader.Close();
			lFileStream.Close();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [디버그] 해당 유저의 보상 정보를 계산합니다.
	public BattleRewardInfo DebugComputeRewardInfo(int pUserIdx)
	{
		BattleLogic lBattleLogic = new BattleLogic();

		BattleVerifier lVerifier = lBattleLogic.CreateVerifier();

		bool lIsValidResult = lVerifier.VerifyData(this);
		if (!lIsValidResult)
			Debug.LogError("lVerifier.VerifyData() failure - DebugComputeRewardInfo()");

		BattleUser lUser = lVerifier.aUsers[pUserIdx];
		BattleProtocol.BattleEndCode lBattleEndCode = lVerifier.aProgressInfo.aTeamBattleEndCodes[lUser.aTeamIdx];

		JSONObject lRewardInfoJson;
		if (TutorialTable.get.IsTutorial(mMatchInfo.aStageSpec.mNo))
			lRewardInfoJson = lUser.CalcBattleEndTutorialReward(lBattleEndCode);
		else
			lRewardInfoJson = lUser.CalcBattleEndPvPReward(lBattleEndCode);

		BattleRewardInfo lRewardInfo = new BattleRewardInfo();
		lRewardInfo.InitFromJson(lRewardInfoJson);

		lVerifier.Destroy();

		return lRewardInfo;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;

	private BattleMatchInfo mMatchInfo;

	private List<BattleFrameInput> mFrameInputList;
	private StringBuilder mInputDataStringBuilder;
}
