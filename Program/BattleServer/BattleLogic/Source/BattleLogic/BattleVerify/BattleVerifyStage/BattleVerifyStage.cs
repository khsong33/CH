﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStage : IStage
{
	public readonly bool cIsEnableBattleHeadquarterPooling = true;
	public readonly bool cIsEnableBattleCheckPointPooling = true;
	public readonly bool cIsEnableBattleEventPointPooling = true;
	public readonly bool cIsEnableBattleBarricadePooling = true;
	public readonly bool cIsEnableBattleCommanderPooling = true;
	public readonly bool cIsEnableBattleTroopPooling = true;
	public readonly bool cIsEnableBattleObserverDummyPooling = true;
	public readonly bool cIsEnableBattleTargetingDummyPooling = true;
	public readonly bool cIsEnableBattleFireEffectPooling = true;
	public readonly bool cIsEnableBattleTroopSkillEffectPooling = true;
	public readonly bool cIsEnableBattleGridSkillEffectPooling = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleVerifyStage()
	{
		mStageHeadquarterPoolingQueue = new Queue<BattleVerifyStageHeadquarter>();
		mStageCheckPointPoolingQueue = new Queue<BattleVerifyStageCheckPoint>();
		mStageEventPointPoolingQueue = new Queue<BattleVerifyStageEventPoint>();
		mStageBarricadePoolingQueue = new Queue<BattleVerifyStageBarricade>();
		mStageCommanderPoolingQueue = new Queue<BattleVerifyStageCommander>();
		mStageTroopPoolingQueue = new Queue<BattleVerifyStageTroop>();
		mStageObserverDummyPoolingQueue = new Queue<BattleVerifyStageObserverDummy>();
		mStageTargetingDummyPoolingQueue = new Queue<BattleVerifyStageTargetingDummy>();
		mStageFireEffectPoolingQueue = new Queue<BattleVerifyStageFireEffect>();
		mStageTroopSkillEffectPoolingQueue = new Queue<BattleVerifyStageTroopSkillEffect>();
		mStageGridSkillEffectPoolingQueue = new Queue<BattleVerifyStageGridSkillEffect>();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageHeadquarter CreateHeadquarter(BattleHeadquarter pBattleHeadquarter)
	{
		BattleVerifyStageHeadquarter lStageHeadquarter;
		if (mStageHeadquarterPoolingQueue.Count <= 0)
			lStageHeadquarter = new BattleVerifyStageHeadquarter();
		else
			lStageHeadquarter = mStageHeadquarterPoolingQueue.Dequeue();

		lStageHeadquarter.OnCreateHeadquarter(this, pBattleHeadquarter);

		return lStageHeadquarter;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyHeadquarter(BattleVerifyStageHeadquarter pStageHeadquarter)
	{
		if (cIsEnableBattleHeadquarterPooling)
			mStageHeadquarterPoolingQueue.Enqueue(pStageHeadquarter);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageCheckPoint CreateCheckPoint(BattleCheckPoint pBattleCheckPoint)
	{
		BattleVerifyStageCheckPoint lStageCheckPoint;
		if (mStageCheckPointPoolingQueue.Count <= 0)
			lStageCheckPoint = new BattleVerifyStageCheckPoint();
		else
			lStageCheckPoint = mStageCheckPointPoolingQueue.Dequeue();

		lStageCheckPoint.OnCreateCheckPoint(this, pBattleCheckPoint);

		return lStageCheckPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCheckPoint(BattleVerifyStageCheckPoint pStageCheckPoint)
	{
		if (cIsEnableBattleCheckPointPooling)
			mStageCheckPointPoolingQueue.Enqueue(pStageCheckPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageEventPoint CreateEventPoint(BattleEventPoint pBattleEventPoint)
	{
		BattleVerifyStageEventPoint lStageEventPoint;
		if (mStageEventPointPoolingQueue.Count <= 0)
			lStageEventPoint = new BattleVerifyStageEventPoint();
		else
			lStageEventPoint = mStageEventPointPoolingQueue.Dequeue();

		lStageEventPoint.OnCreateEventPoint(this, pBattleEventPoint);

		return lStageEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyEventPoint(BattleVerifyStageEventPoint pStageEventPoint)
	{
		if (cIsEnableBattleEventPointPooling)
			mStageEventPointPoolingQueue.Enqueue(pStageEventPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageBarricade CreateBarricade(BattleBarricade pBattleBarricade)
	{
		BattleVerifyStageBarricade lStageBarricade;
		if (mStageBarricadePoolingQueue.Count <= 0)
			lStageBarricade = new BattleVerifyStageBarricade();
		else
			lStageBarricade = mStageBarricadePoolingQueue.Dequeue();

		lStageBarricade.OnCreateBarricade(this, pBattleBarricade);

		return lStageBarricade;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyBarricade(BattleVerifyStageBarricade pStageBarricade)
	{
		if (cIsEnableBattleBarricadePooling)
			mStageBarricadePoolingQueue.Enqueue(pStageBarricade);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageCommander CreateCommander(BattleCommander pBattleCommander)
	{
		BattleVerifyStageCommander lStageCommander;
		if (mStageCommanderPoolingQueue.Count <= 0)
			lStageCommander = new BattleVerifyStageCommander();
		else
			lStageCommander = mStageCommanderPoolingQueue.Dequeue();

		lStageCommander.OnCreateCommander(this, pBattleCommander);

		return lStageCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCommander(BattleVerifyStageCommander pStageCommander)
	{
		if (cIsEnableBattleCommanderPooling)
			mStageCommanderPoolingQueue.Enqueue(pStageCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public virtual IStageTroop CreateTroop(BattleTroop pBattleTroop)
	{
		BattleVerifyStageTroop lStageTroop;
		if (mStageTroopPoolingQueue.Count <= 0)
			lStageTroop = new BattleVerifyStageTroop();
		else
			lStageTroop = mStageTroopPoolingQueue.Dequeue();

		lStageTroop.OnCreateTroop(this, pBattleTroop);

		return lStageTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroop(BattleVerifyStageTroop pStageTroop)
	{
		if (cIsEnableBattleTroopPooling)
			mStageTroopPoolingQueue.Enqueue(pStageTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageObserverDummy CreateObserverDummy(BattleObserverDummy pBattleObserverDummy)
	{
		BattleVerifyStageObserverDummy lStageObserverDummy;
		if (mStageObserverDummyPoolingQueue.Count <= 0)
			lStageObserverDummy = new BattleVerifyStageObserverDummy();
		else
			lStageObserverDummy = mStageObserverDummyPoolingQueue.Dequeue();

		lStageObserverDummy.OnCreateObserverDummy(this, pBattleObserverDummy);

		return lStageObserverDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyObserverDummy(BattleVerifyStageObserverDummy pStageObserverDummy)
	{
		if (cIsEnableBattleObserverDummyPooling)
			mStageObserverDummyPoolingQueue.Enqueue(pStageObserverDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageTargetingDummy CreateTargetingDummy(BattleTargetingDummy pBattleTargetingDummy)
	{
		BattleVerifyStageTargetingDummy lStageTargetingDummy;
		if (mStageTargetingDummyPoolingQueue.Count <= 0)
			lStageTargetingDummy = new BattleVerifyStageTargetingDummy();
		else
			lStageTargetingDummy = mStageTargetingDummyPoolingQueue.Dequeue();

		lStageTargetingDummy.OnCreateTargetingDummy(this, pBattleTargetingDummy);

		return lStageTargetingDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTargetingDummy(BattleVerifyStageTargetingDummy pStageTargetingDummy)
	{
		if (cIsEnableBattleTargetingDummyPooling)
			mStageTargetingDummyPoolingQueue.Enqueue(pStageTargetingDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageFireEffect CreateFireEffect(BattleFireEffect pBattleFireEffect)
	{
		BattleVerifyStageFireEffect lStageFireEffect;
		if (mStageFireEffectPoolingQueue.Count <= 0)
			lStageFireEffect = new BattleVerifyStageFireEffect();
		else
			lStageFireEffect = mStageFireEffectPoolingQueue.Dequeue();

		lStageFireEffect.OnCreateFireEffect(this, pBattleFireEffect);

		return lStageFireEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyFireEffect(BattleVerifyStageFireEffect pStageFireEffect)
	{
		if (cIsEnableBattleFireEffectPooling)
			mStageFireEffectPoolingQueue.Enqueue(pStageFireEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageTroopSkillEffect CreateTroopSkillEffect(BattleTroopSkillEffect pBattleTroopSkillEffect)
	{
		BattleVerifyStageTroopSkillEffect lStageTroopSkillEffect;
		if (mStageTroopSkillEffectPoolingQueue.Count <= 0)
			lStageTroopSkillEffect = new BattleVerifyStageTroopSkillEffect();
		else
			lStageTroopSkillEffect = mStageTroopSkillEffectPoolingQueue.Dequeue();

		lStageTroopSkillEffect.OnCreateTroopSkillEffect(this, pBattleTroopSkillEffect);

		return lStageTroopSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroopSkillEffect(BattleVerifyStageTroopSkillEffect pStageTroopSkillEffect)
	{
		if (cIsEnableBattleTroopSkillEffectPooling)
			mStageTroopSkillEffectPoolingQueue.Enqueue(pStageTroopSkillEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageGridSkillEffect CreateGridSkillEffect(BattleGridSkillEffect pBattleGridSkillEffect, int pDirectionY)
	{
		BattleVerifyStageGridSkillEffect lStageGridSkillEffect;
		if (mStageGridSkillEffectPoolingQueue.Count <= 0)
			lStageGridSkillEffect = new BattleVerifyStageGridSkillEffect();
		else
			lStageGridSkillEffect = mStageGridSkillEffectPoolingQueue.Dequeue();

		lStageGridSkillEffect.OnCreateGridSkillEffect(this, pBattleGridSkillEffect, pDirectionY);

		return lStageGridSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyGridSkillEffect(BattleVerifyStageGridSkillEffect pStageGridSkillEffect)
	{
		if (cIsEnableBattleGridSkillEffectPooling)
			mStageGridSkillEffectPoolingQueue.Enqueue(pStageGridSkillEffect);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	protected Queue<BattleVerifyStageHeadquarter> mStageHeadquarterPoolingQueue;
	protected Queue<BattleVerifyStageCheckPoint> mStageCheckPointPoolingQueue;
	protected Queue<BattleVerifyStageEventPoint> mStageEventPointPoolingQueue;
	protected Queue<BattleVerifyStageBarricade> mStageBarricadePoolingQueue;
	protected Queue<BattleVerifyStageCommander> mStageCommanderPoolingQueue;
	protected Queue<BattleVerifyStageTroop> mStageTroopPoolingQueue;
	protected Queue<BattleVerifyStageObserverDummy> mStageObserverDummyPoolingQueue;
	protected Queue<BattleVerifyStageTargetingDummy> mStageTargetingDummyPoolingQueue;
	protected Queue<BattleVerifyStageFireEffect> mStageFireEffectPoolingQueue;
	protected Queue<BattleVerifyStageTroopSkillEffect> mStageTroopSkillEffectPoolingQueue;
	protected Queue<BattleVerifyStageGridSkillEffect> mStageGridSkillEffectPoolingQueue;
}
