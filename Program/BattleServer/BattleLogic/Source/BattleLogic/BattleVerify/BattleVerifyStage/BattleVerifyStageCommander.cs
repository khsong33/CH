﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageCommander : BattleVerifyStageObject, IStageCommander
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleCommander aBattleCommander
	{
		get { return mBattleCommander; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	protected override void OnNewObject()
// 	{
// 		base.OnNewObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateCommander(BattleVerifyStage pStage, BattleCommander pBattleCommander)
	{
		OnCreateObject(
			pStage,
			pBattleCommander.aCoord,
			pBattleCommander.aDirectionY);

		mBattleCommander = pBattleCommander;
	}

	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyCommander(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void SetSelected(bool pIsSelected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void UpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void MoveTo(Coord2 pCoord, bool pIsArrived)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void RotateToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void ShowState(StageCommanderState pCommanderState)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCommander mBattleCommander;
}
