﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageFireEffect : BattleVerifyStageEffect, IStageFireEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleFireEffect aBattleFireEffect
	{
		get { return mBattleFireEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
// 	protected override void OnNewEffect()
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateFireEffect(BattleVerifyStage pStage, BattleFireEffect pBattleFireEffect)
	{
		OnCreateEffect(pStage);

		mBattleFireEffect = pBattleFireEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		aStage.DestroyFireEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate(Coord2 pActivationCoord)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleFireEffect mBattleFireEffect;
}
