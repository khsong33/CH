﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageGridSkillEffect : BattleVerifyStageEffect, IStageGridSkillEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleGridSkillEffect aBattleGridSkillEffect
	{
		get { return mBattleGridSkillEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
// 	public override void DestroyEffect()
// 	{
// 		base.DestroyEffect();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
// 	protected override void OnNewEffect()
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateGridSkillEffect(BattleVerifyStage pStage, BattleGridSkillEffect pBattleGridSkillEffect, int pDirectionY)
	{
		OnCreateEffect(pStage);

		mBattleGridSkillEffect = pBattleGridSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		aStage.DestroyGridSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate()
	{
		//Debug.Log("Activate() - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGridSkillEffect mBattleGridSkillEffect;
}
