﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageObserverDummy : BattleVerifyStageObject, IStageObserverDummy
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleObserverDummy aBattleObserverDummy
	{
		get { return mBattleObserverDummy; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateObserverDummy(BattleVerifyStage pStage, BattleObserverDummy pBattleObserverDummy)
	{
		OnCreateObject(
			pStage,
			pBattleObserverDummy.aCoord,
			pBattleObserverDummy.aDirectionY);

		mBattleObserverDummy = pBattleObserverDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyObserverDummy(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DebugLog(String pLog)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObserverDummy 메서드
	public void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObserverDummy 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleObserverDummy mBattleObserverDummy;
}
