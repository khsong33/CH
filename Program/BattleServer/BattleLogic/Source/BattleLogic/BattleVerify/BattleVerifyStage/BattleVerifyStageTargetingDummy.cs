﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageTargetingDummy : BattleVerifyStageObject, IStageTargetingDummy
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTargetingDummy aBattleTargetingDummy
	{
		get { return mBattleTargetingDummy; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateTargetingDummy(BattleVerifyStage pStage, BattleTargetingDummy pBattleTargetingDummy)
	{
		OnCreateObject(
			pStage,
			pBattleTargetingDummy.aCoord,
			pBattleTargetingDummy.aDirectionY);

		mBattleTargetingDummy = pBattleTargetingDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyTargetingDummy(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
// 	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
// 	{
// 		return false;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DebugLog(String pLog)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTargetingDummy mBattleTargetingDummy;
}
