﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageTroop : BattleVerifyStageObject, IStageTroop
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateTroop(BattleVerifyStage pStage, BattleTroop pBattleTroop)
	{
		OnCreateObject(
			pStage,
			pBattleTroop.aCoord,
			pBattleTroop.aDirectionY);

		mBattleTroop = pBattleTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyTroop(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
// 	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
// 	{
// 		return false;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DebugLog(String pLog)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public void SetSelected(bool pIsSelected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void SetTargetable(bool pIsTargetable)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateDetection(int pDetectingTeamIdx, bool pIsToDetected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateHp(int pCurHp, int pMaxHp, TroopHpChangeType pHpChangeType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateExp()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateVeteranRank(int pRank)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateSightRange()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateAttackRange()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateHiding(bool pIsHiding)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateReturnState(bool pIsFallBehind)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateLeaderState(bool pIsLeader)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateRecruitState()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnHoldMove(bool pIsHoldMove)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopMove(Coord2 pStopCoord)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void MoveTo(Coord2 pCoord, bool pIsArrived)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void RotateBodyToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void RotateBodyWeaponToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void RotateTurretToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void InstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void TrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void WindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void AimWeapon(int pAimDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void FireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void EndCooldown()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void GoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ComeBackInteraction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void StartSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void StopSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void StartAction(TroopActionType pActionType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void StopAction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowTroopScript(string pMessageKey, float pDuration)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mBattleTroop;
}
