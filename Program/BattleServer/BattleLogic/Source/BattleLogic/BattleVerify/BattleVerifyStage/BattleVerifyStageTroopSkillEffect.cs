﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageTroopSkillEffect : BattleVerifyStageEffect, IStageTroopSkillEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTroopSkillEffect aBattleTroopSkillEffect
	{
		get { return mBattleTroopSkillEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
// 	public override void DestroyEffect()
// 	{
// 		base.DestroyEffect();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
// 	protected override void OnNewEffect()
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateTroopSkillEffect(BattleVerifyStage pStage, BattleTroopSkillEffect pBattleTroopSkillEffect)
	{
		OnCreateEffect(pStage);

		mBattleTroopSkillEffect = pBattleTroopSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		aStage.DestroyTroopSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopSkillEffect mBattleTroopSkillEffect;
}
