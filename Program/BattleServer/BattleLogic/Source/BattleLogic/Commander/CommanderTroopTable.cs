//#define TEST_LIMIT_TROOP_COUNT // 테스트로 최대 부대수를 제한합니다.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderTroopTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인세트터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "CommanderTroopTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";

	public readonly String[] vTroopSpecColumnNames = { "Troop1", "Troop2", "Troop3", "Troop4", "Troop5", "Troop6" };
	public readonly String[] vTroopFormationGroupColumnNames = { "Troop1Group", "Troop2Group", "Troop3Group", "Troop4Group", "Troop5Group", "Troop6Group" };
	public readonly String[] vTroopFormationPositionOffsetColumnNames = { "Troop1Pos", "Troop2Pos", "Troop3Pos", "Troop4Pos", "Troop5Pos", "Troop6Pos" };
	public readonly String[] vTroopTypeColumnNames = { "Troop1Type", "Troop2Type", "Troop3Type", "Troop4Type", "Troop5Type", "Troop6Type" };

	public readonly String vIsFollowAdvanceColumnName = "AdvanceType";
	public readonly String vMaxMoraleMultiplierColumnName = "T_MaxMoraleMultiplier";
	public readonly String vSuppressionPointMultiplierColumnName = "C_SuppressionPointMultiplier";
	public readonly String vMinMoraleColumnName = "T_MinMoraleOverwrite";
	public readonly String vAllySightPrimaryWeaponAccuracyMultiplyRatioColumnName = "PW_SightEffectAccuracyMultiplier";
	public readonly String vAllySightSecondaryWeaponAccuracyMultiplyRatioColumnName = "SW_SightEffectAccuracyMultiplier";
	public readonly String vAllySightPrimaryWeaponScatterMultiplyRatioColumnName = "PW_SightEffectScatterMultiplier";
	public readonly String vAllySightSecondaryWeaponScatterMultiplyRatioColumnName = "SW_SightEffectScatterMultiplier";
	public readonly String vSkillReadyDelayMultiplyRatioColumnName = "S_ReadyDelayMultiplier";
	public readonly String vSkillUseDelayMultiplyRatioColumnName = "S_UseDelayMultiplier";
	public readonly String vSkillCooldownDelayMultiplyRatioColumnName = "S_CooldownDelayMultiplier";
	public readonly String vGainVeterancyMultiplyRatioColumnName = "CM_GainVeterancyMultiplier";
	public readonly String vStartVeterancyRankColumnName = "CM_StartVeterancyOverwrite";
	public readonly String vIsHidingMoveOnHitColumnName = "L_GroundMoveOverwrite";
	public readonly String vIsConcentratedFireColumnName = "L_ConcentratedFireOverwrite";
	public readonly String vIsSelectBestWeaponOnAttackColumnName = "L_AttackPositionOverwrite";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CommanderTroopTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CommanderTroopTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderTroopTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		CommanderTroopSet.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 세트 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lCommanderName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lCommanderName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 부대 세트 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("CommanderTroopTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lCommanderName))
			{
				Debug.LogError(String.Format("CommanderTroopTable[{0}] duplicate Name '{1}'", lSpecNo, lCommanderName));
				continue;
			}
			mNameToRowDictionary.Add(lCommanderName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mCommanderTroopTypeDictionary = new Dictionary<String, CommanderTroopType>();
		foreach (CommanderTroopType iEnum in Enum.GetValues(typeof(CommanderTroopType)))
			mCommanderTroopTypeDictionary.Add(iEnum.ToString(), iEnum);

		// 세트 테이블을 초기화합니다.
		mCommanderTroopSets = new CommanderTroopSet[mCSVObject.RowNum];

		mIsValidFormationPositions = new bool[CommanderTroopSet.cMaxTroopCount];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllCommanderTroopSet();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllCommanderTroopSet()
	{
		Debug.LogWarning("LoadAllCommanderTroopSet() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mCommanderTroopSets[iRow] == null) // _SetUpSpawnLinkCommanderInfo() 속에서 초기화되었을 수도 있습니다.
				mCommanderTroopSets[iRow] = _LoadCommanderTroopSet(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 세트을 얻습니다.
	public CommanderTroopSet GetCommanderTroopSetByIdx(int pCommanderIdx)
	{
		if (mCommanderTroopSets[pCommanderIdx] == null)
			mCommanderTroopSets[pCommanderIdx] = _LoadCommanderTroopSet(mCSVObject.GetRow(pCommanderIdx), pCommanderIdx);
		return mCommanderTroopSets[pCommanderIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 세트을 찾습니다.
	public CommanderTroopSet FindCommanderTroopSet(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (mCommanderTroopSets[lRowIdx] == null)
				mCommanderTroopSets[lRowIdx] = _LoadCommanderTroopSet(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mCommanderTroopSets[lRowIdx];
		}
		else
		{
			//Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 유니트 이름의 세트을 찾습니다.
	public CommanderTroopSet FindCommanderTroopSet(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mCommanderTroopSets[lRowIdx] == null)
				mCommanderTroopSets[lRowIdx] = _LoadCommanderTroopSet(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mCommanderTroopSets[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 에넘값을 찾습니다.
	public CommanderTroopType FindCommanderTroopType(String pCommanderTroopTypeString)
	{
		CommanderTroopType lCommanderTroopType;
		if (!mCommanderTroopTypeDictionary.TryGetValue(pCommanderTroopTypeString, out lCommanderTroopType))
		{
			Debug.LogError("invalid CommanderTroopType:" + pCommanderTroopTypeString);
			return CommanderTroopType.Default;
		}
		return lCommanderTroopType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 세트 개수를 얻습니다.
	public int GetSetCount()
	{
		return mCommanderTroopSets.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderTroopTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 세트을 로딩합니다.
	private CommanderTroopSet _LoadCommanderTroopSet(CSVRow pCSVRow, int pRowIdx)
	{
		CommanderTroopSet lCommanderTroopSet = new CommanderTroopSet();

		// 줄 인덱스
		lCommanderTroopSet.mIdx = pRowIdx;

		// Name
		lCommanderTroopSet.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lCommanderTroopSet.mName);

		// SpecNo
		lCommanderTroopSet.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// mTroopInfos
		lCommanderTroopSet.mTroopCount = CommanderTroopSet.cMaxTroopCount;
		lCommanderTroopSet.mTroopInfos = new CommanderTroopInfo[CommanderTroopSet.cMaxTroopCount];

		lCommanderTroopSet.mFormationPositionOffsets = new Coord2[CommanderTroopSet.cMaxTroopCount];
		lCommanderTroopSet.mFormationGroupIds = new int[CommanderTroopSet.cMaxTroopCount];
		lCommanderTroopSet.mFirstGroupIdxs = new int[CommanderTroopSet.cMaxTroopCount];
		lCommanderTroopSet.mSecondGroupIdxs = new int[CommanderTroopSet.cMaxTroopCount];

		lCommanderTroopSet.mTroopKindCountInfos = new CommanderTroopKindCountInfo[CommanderTroopSet.cMaxTroopKindCount];
		//lCommanderTroopSet.mSpawnMp = 0;
		lCommanderTroopSet.mSpawnCoolTime = 0;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			String lTroopSpecName = pCSVRow.GetStringValue(vTroopSpecColumnNames[iTroop]);
			if (String.IsNullOrEmpty(lTroopSpecName))
			{
				lCommanderTroopSet.mTroopCount = iTroop;
				mIsValidFormationPositions[iTroop] = false;
				break; // 빈 슬롯이 나오면 이후는 없는 것으로 간주
			}
			else
				mIsValidFormationPositions[iTroop] = true;

			CommanderTroopInfo lTroopInfo = new CommanderTroopInfo();
			lCommanderTroopSet.mTroopInfos[iTroop] = lTroopInfo;

			// TroopSpec
			lTroopInfo.mTroopSpec = TroopTable.get.FindTroopSpec(lTroopSpecName);

			// TroopType
			String lCommanderTroopTypeString;
			if (pCSVRow.TryStringValue(vTroopTypeColumnNames[iTroop], out lCommanderTroopTypeString, String.Empty)) // 기본값 빈 문자열
				lTroopInfo.mCommanderTroopType = FindCommanderTroopType(lCommanderTroopTypeString);
			else
				lTroopInfo.mCommanderTroopType = CommanderTroopType.Default;

			// TroopFormationGroupOffset
			lCommanderTroopSet.mFormationGroupIds[iTroop] = pCSVRow.GetIntValue(vTroopFormationGroupColumnNames[iTroop], 0);

			// TroopFormationPositionOffset
			Coord2 lFormationOffsetValue = pCSVRow.GetCoordValue(vTroopFormationPositionOffsetColumnNames[iTroop]);
			lCommanderTroopSet.mFormationPositionOffsets[iTroop] = new Coord2(-lFormationOffsetValue.x, -lFormationOffsetValue.z); // 0도 회전(-Z) 방향으로 오프셋을 저장

			// 이 부대의 소환 MP와 쿨타임을 더합니다.
			//lCommanderTroopSet.mSpawnMp += lTroopInfo.mTroopSpec.mStatTable.Get(TroopStat.SpawnMp);
			lCommanderTroopSet.mSpawnCoolTime += lTroopInfo.mTroopSpec.mStatTable.Get(TroopStat.SpawnCoolTime);

			// 종류별 부대 갯수를 저장합니다
			bool lIsSaveKindCount = false;
			for (int iKindTroop = 0; iKindTroop < CommanderTroopSet.cMaxTroopKindCount; iKindTroop++)
			{
				if (lCommanderTroopSet.mTroopKindCountInfos[iKindTroop] == null)
					lCommanderTroopSet.mTroopKindCountInfos[iKindTroop] = new CommanderTroopKindCountInfo();

				AssetKey lTroopIconAssetKey = lCommanderTroopSet.mTroopKindCountInfos[iKindTroop].mTroopIconAssetKey;
				if (lTroopIconAssetKey == null) // 저장된 값이 없으면
				{
					lCommanderTroopSet.mTroopKindCountInfos[iKindTroop].mTroopSpec = lTroopInfo.mTroopSpec;
					lCommanderTroopSet.mTroopKindCountInfos[iKindTroop].mTroopIconAssetKey = lTroopInfo.mTroopSpec.mFieldIconAssetKey;
					lCommanderTroopSet.mTroopKindCountInfos[iKindTroop].mTroopCount++;
					lCommanderTroopSet.mTroopKindCount++;
					lIsSaveKindCount = true;
					break;
				}
				else if (lTroopIconAssetKey.aAssetPath == lTroopInfo.mTroopSpec.mFieldIconAssetKey.aAssetPath)
				{
					lCommanderTroopSet.mTroopKindCountInfos[iKindTroop].mTroopCount++;
					lIsSaveKindCount = true; 
					break;
				}
			}
			if (!lIsSaveKindCount) 
			{
				Debug.LogError("Failed Kind of troop count :: Troop Spec Name - " + lTroopSpecName + " :: Troop Set Name - " + lCommanderTroopSet.mName);
			}

		#if TEST_LIMIT_TROOP_COUNT
			if ((iTroop + 1) == 1)
			{
				lCommanderTroopSet.mTroopCount = iTroop + 1;
				if (iTroop == 0)
					lCommanderTroopSet.mFormationPositionOffsets[0] = Coord2.zero;
				break;
			}
		#endif

// [테스트] 특정 부대를 하나만 스폰하는 처리
// 			if (lTroopInfo.mTroopSpec.mWeaponSpecs[lTroopInfo.mTroopSpec.mMainBodyWeaponIdx].mAttackType == WeaponAttackType.MachineGun)
// 			{
// 				lCommanderTroopSet.mTroopCount = iTroop + 1;
// 				break;
// 			}
		}

		int lFirstGroupCount = 0;
		int lSecondGroupCount = 0;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			if (!mIsValidFormationPositions[iTroop]) // 임시로 저장해 두었던 플래그 활용
				continue;

			if (lCommanderTroopSet.mFormationGroupIds[iTroop] == 0)
				lCommanderTroopSet.mFirstGroupIdxs[lFirstGroupCount++] = iTroop;
			else
				lCommanderTroopSet.mSecondGroupIdxs[lSecondGroupCount++] = iTroop;
		}

		// IsFollowAdvance
		lCommanderTroopSet.mIsFollowAdvance = pCSVRow.GetBoolValue(vIsFollowAdvanceColumnName, false);

		// MaxMoraleMultiplyRatio
		pCSVRow.TryRatioValue(vMaxMoraleMultiplierColumnName, out lCommanderTroopSet.mMaxMoraleMultiplyRatio, 10000); // 기본값 100%(만분율)

		// SuppressionMultiplyRatio
		pCSVRow.TryRatioValue(vSuppressionPointMultiplierColumnName, out lCommanderTroopSet.mSuppressionMultiplyRatio, 10000); // 기본값 100%(만분율)

		// MinMorale
		pCSVRow.TryRatioValue(vMinMoraleColumnName, out lCommanderTroopSet.mMinMorale, 0); // 기본값 0

		// AllySightWeaponAccuracyMultiplyRatios
		lCommanderTroopSet.mAllySightWeaponAccuracyMultiplyRatios = new int[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lCommanderTroopSet.mAllySightWeaponAccuracyMultiplyRatios[iWeapon] = 10000; // 기본값 100%(만분율)

		pCSVRow.TryRatioValue(vAllySightPrimaryWeaponAccuracyMultiplyRatioColumnName, out lCommanderTroopSet.mAllySightWeaponAccuracyMultiplyRatios[TroopSpec.cPrimaryWeaponIdx], 10000); // 기본값 100%(만분율)
		pCSVRow.TryRatioValue(vAllySightSecondaryWeaponAccuracyMultiplyRatioColumnName, out lCommanderTroopSet.mAllySightWeaponAccuracyMultiplyRatios[TroopSpec.cSecondaryWeaponIdx], 10000); // 기본값 100%(만분율)

		// AllySightWeaponScatterMultiplyRatios
		lCommanderTroopSet.mAllySightWeaponScatterMultiplyRatios = new int[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lCommanderTroopSet.mAllySightWeaponScatterMultiplyRatios[iWeapon] = 10000; // 기본값 100%(만분율)

		pCSVRow.TryRatioValue(vAllySightPrimaryWeaponScatterMultiplyRatioColumnName, out lCommanderTroopSet.mAllySightWeaponScatterMultiplyRatios[TroopSpec.cPrimaryWeaponIdx], 10000); // 기본값 100%(만분율)
		pCSVRow.TryRatioValue(vAllySightSecondaryWeaponScatterMultiplyRatioColumnName, out lCommanderTroopSet.mAllySightWeaponScatterMultiplyRatios[TroopSpec.cSecondaryWeaponIdx], 10000); // 기본값 100%(만분율)

		// SkillReadyDelayMultiplyRatio
		pCSVRow.TryRatioValue(vSkillReadyDelayMultiplyRatioColumnName, out lCommanderTroopSet.mSkillReadyDelayMultiplyRatio, 10000); // 기본값 100%(만분율)

		// SkillUseDelayMultiplyRatio
		pCSVRow.TryRatioValue(vSkillUseDelayMultiplyRatioColumnName, out lCommanderTroopSet.mSkillUseDelayMultiplyRatio, 10000); // 기본값 100%(만분율)

		// SkillCooldownDelayMultiplyRatio
		pCSVRow.TryRatioValue(vSkillCooldownDelayMultiplyRatioColumnName, out lCommanderTroopSet.mSkillCooldownDelayMultiplyRatio, 10000); // 기본값 100%(만분율)

		// GainVeterancyMultiplyRatio
		pCSVRow.TryRatioValue(vGainVeterancyMultiplyRatioColumnName, out lCommanderTroopSet.mGainVeterancyMultiplyRatio, 10000); // 기본값 100%(만분율)

		// StartVeterancyRank
		pCSVRow.TryIntValue(vStartVeterancyRankColumnName, out lCommanderTroopSet.mStartVeterancyRank, 0); // 기본값 숙련도 레벨 0

		// IsHidingMoveOnHit
		pCSVRow.TryBoolValue(vIsHidingMoveOnHitColumnName, out lCommanderTroopSet.mIsHidingMoveOnHit, false); // 기본값 false

		// IsConcentratedFire
		pCSVRow.TryBoolValue(vIsConcentratedFireColumnName, out lCommanderTroopSet.mIsConcentratedFire, false); // 기본값 false

		// IsSelectBestWeaponOnAttack
		pCSVRow.TryBoolValue(vIsSelectBestWeaponOnAttackColumnName, out lCommanderTroopSet.mIsSelectBestWeaponOnAttack, false); // 기본값 false

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lCommanderTroopSet);

		return lCommanderTroopSet;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(CommanderTroopSet pCommanderTroopSet)
	{
		// mSqrGroupingRange
		Coord2 lMaxDistanctCoord = Coord2.zero;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			if (pCommanderTroopSet.mTroopInfos[iTroop] == null)
				break;

			lMaxDistanctCoord.x = Mathf.Max(lMaxDistanctCoord.x, Mathf.Abs(pCommanderTroopSet.mFormationPositionOffsets[iTroop].x));
			lMaxDistanctCoord.z = Mathf.Max(lMaxDistanctCoord.z, Mathf.Abs(pCommanderTroopSet.mFormationPositionOffsets[iTroop].z));
		}
		int lMaxDistance = lMaxDistanctCoord.RoughMagnitude() + BattleConfig.get.mTroopGroupingRangeMargin;
		pCommanderTroopSet.mSqrGroupingRange = lMaxDistance * lMaxDistance;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CommanderTroopTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, CommanderTroopType> mCommanderTroopTypeDictionary;

	private CommanderTroopSet[] mCommanderTroopSets;

	private bool[] mIsValidFormationPositions;
}
