﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderLevelSpec
{
	public int mRank;
	public int mLevel;
	public int mNeedCard;
	public int mCost;
	public int mExp;

	internal const int cMaxLevelMarginCount = 10;
}
