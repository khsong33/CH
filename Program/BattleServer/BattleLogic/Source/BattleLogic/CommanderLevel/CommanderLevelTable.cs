﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderLevelTable
{
	public static readonly String cCSVTextAssetPath = "CommanderLevelTableCSV";

	public static readonly String vRankColumn = "Rank";
	public static readonly String vLevelColumn = "Level";
	public static readonly String vNeedCardColumn = "NeedCard";
	public static readonly String vCostColumn = "Cost";
	public static readonly String vExpColumn = "Exp";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CommanderLevelTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CommanderLevelTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderLevelTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mCommanderLevelSpecs = new Dictionary<int, Dictionary<int, CommanderLevelSpec>>();
		mRankMaxLevels = new Dictionary<int, int>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CommanderLevelSpec info = new CommanderLevelSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mRank = lCSVRow.GetIntValue(vRankColumn);
			info.mLevel = lCSVRow.GetIntValue(vLevelColumn);
			info.mNeedCard = lCSVRow.GetIntValue(vNeedCardColumn);
			info.mCost = lCSVRow.GetIntValue(vCostColumn);
			info.mExp = lCSVRow.GetIntValue(vExpColumn);

			if(!mCommanderLevelSpecs.ContainsKey(info.mRank))
			{
				mCommanderLevelSpecs.Add(info.mRank, new Dictionary<int,CommanderLevelSpec>());
			}
			mCommanderLevelSpecs[info.mRank].Add(info.mLevel, info);
		}
		// Max Level 저장
		Dictionary<int, Dictionary<int, CommanderLevelSpec>>.Enumerator iter = mCommanderLevelSpecs.GetEnumerator();
		while(iter.MoveNext())
		{
			mRankMaxLevels.Add(iter.Current.Key, iter.Current.Value.Count);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨 데이터를 가져옵니다.
	public CommanderLevelSpec GetCommanderLevelSpec(int pRank, int pLevel)
	{
		if (!mCommanderLevelSpecs.ContainsKey(pRank))
		{
			Debug.LogError("Not Exist Rank - " + pRank);
			return null;
		}
		else if (!mCommanderLevelSpecs[pRank].ContainsKey(pLevel))
		{
			Debug.LogError("Not Exist Level " + pLevel + " in Rank " + pRank);
			return null;
		}
		return mCommanderLevelSpecs[pRank][pLevel];
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 랭크 수를 가져옵니다.
	public int GetMaxRank()
	{
		return mCommanderLevelSpecs.Count;
	}
	//-------------------------------------------------------------------------------------------------------
	// 랭크별 최대 레벨을 가져옵니다.
	public int GetMaxLevel(int pRank)
	{
		if (!mRankMaxLevels.ContainsKey(pRank))
		{
			Debug.LogError("Not Exist Rank - " + pRank);
			return 0;
		}
		return mRankMaxLevels[pRank];
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 최대 레벨 여부를 확인합니다..
	public bool IsMaxLevel(CommanderItem pCommanderItem)
	{
		int lMaxLevel = GetMaxLevel(pCommanderItem.aRank);
		return lMaxLevel == pCommanderItem.mCommanderLevel;
	}

	//-------------------------------------------------------------------------------------------------------
	// 최대로 갖고 있을 수 있는 카드수
	public int GetMaxHaveItemCount(int pCurrentLevel, int pRank)
	{
		Dictionary<int, CommanderLevelSpec> lCommanderLevelSpecs = mCommanderLevelSpecs[pRank];
		Dictionary<int, CommanderLevelSpec>.Enumerator iter = lCommanderLevelSpecs.GetEnumerator();
		int lNeedCard = 0;
		while (iter.MoveNext())
		{
			CommanderLevelSpec lCurrentLevelSpec = iter.Current.Value;
			if (lCurrentLevelSpec.mLevel >= pCurrentLevel)
			{
				lNeedCard += lCurrentLevelSpec.mNeedCard;
			}
		}
		return lNeedCard + 10;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderLevelTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CommanderLevelTable sInstance = null;

	private CSVObject mCSVObject;
	private Dictionary<int, Dictionary<int, CommanderLevelSpec>> mCommanderLevelSpecs;
	private Dictionary<int, int> mRankMaxLevels;

}
