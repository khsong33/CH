using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum GroundEventFunction
{
	None,
	MedKit,
	Munition,
	Commander,
	RepairKit,
}

public class GroundEventSpec
{
	public class MedKitParam
	{
		public int mHpRecoveryRatio;
	}
	public class MunitionParam
	{
		public int mMpIncrement;
	}
	public class CommanderParam
	{
		public int mTeamIdx;
		public DeckNation mDeckNation;
		public CommanderSpec mSpawnCommanderSpec;
		public bool mIsAutoTeamUpdate;
		public bool mIsAi;
	}
	public class RepairKitParam
	{
		public int mHpRecoveryRatio;
	}

	public int mIndex;

	public String mName;
	public String mNameText;

	public AssetKey mStageEventObjectAssetKey;
	public AssetKey mMiniMapIconAssetKey;

	public GroundEventFunction mFunction;
	public MedKitParam mMedKitParam;
	public MunitionParam mMunitionParam;
	public CommanderParam mCommanderParam;
	public RepairKitParam mRepairKitParam;

	// 파생 정보
	public bool mIsCeiling;

	public const int cGroundEventFunctionCount = 5;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<GroundEventSpec[{0}]>", mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(GroundEventSpec pGroundEventSpec)
	{
		if (pGroundEventSpec != null)
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 미니맵 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetMiniMapIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mMiniMapIconAssetKey);
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
