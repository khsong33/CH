﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum GroundType
{
	Grass,
	Road,
	Rock,
	Trench,
	Fence,
	Bunker,
	Mud,
	Tree,
	Bush,
	Water,
	SandBag,

	None,
}

public enum GroundStat
{
	IsMoveAvailableInfantry,
	IsMoveAvailableArmored,
	IsMoveAvailableArtillery,

	IsEffectAvailableInfantry,
	IsEffectAvailableArmored,
	IsEffectAvailableArtillery,

	SightRangeAdd,
	AdvanceSpeedRatio,
	IsHiding,
	WeaponRangeRatio,
	AccuracyRatio,
	PenetrationRatio,
}

public class GroundArmorSpec
{
	public int mEffectAngleHalf;
	public int mArmorSize;
	public int mArmorAdd;
	public int mSuppressionRatio;
}

public class GroundStatTable : StatTable<GroundStat>
{
	public GroundArmorSpec aTileArmorSpec
	{
		get { return mTileArmorSpec; }
	}
	public int aTileArmorDirectionY
	{
		get { return mTileArmorDirectionY; }
	}
	public int aTileArmorEffectAngleHalf
	{
		get { return (mTileArmorSpec != null) ? mTileArmorSpec.mEffectAngleHalf : 0; }
	}
	public GroundArmorSpec aSkillArmorSpec
	{
		get { return mSkillArmorSpec; }
	}
	public int aSkillArmorDirectionY
	{
		get { return mSkillArmorDirectionY; }
	}
	public int aSkillArmorEffectAngleHalf
	{
		get { return (mSkillArmorSpec != null) ? mSkillArmorSpec.mEffectAngleHalf : 0; }
	}

	public int Get(GroundStat pStat)
	{
		return mStats[(int)pStat];
	}
	public bool aIsHiding
	{
		get { return (mStats[(int)GroundStat.IsHiding] > 0); }
	}

	public bool aIsArmorSpec
	{
		get { return mIsArmorSpec; }
	}

	public GroundStatTable()
	{
		mIsArmorSpec = false;
	}

	public bool CheckMoveAvailableType(TroopType pTroopType)
	{
		if (mStats[(int)GroundStat.IsMoveAvailableInfantry + (int)pTroopType] > 0)
			return true;
		else
			return false;
	}
	public bool CheckEffectAvailableType(TroopType pTroopType)
	{
		if (mStats[(int)GroundStat.IsEffectAvailableInfantry + (int)pTroopType] > 0)
			return true;
		else
			return false;
	}

	public void SetTileArmorSpec(GroundArmorSpec pTileArmorSpec, int pTileArmorDirectionY)
	{
		mTileArmorSpec = pTileArmorSpec;
		mTileArmorDirectionY = pTileArmorDirectionY;

		mIsArmorSpec = (mTileArmorSpec != null) || (mSkillArmorSpec != null);
	}
	public void SetSkillArmorSpec(GroundArmorSpec pSkillArmorSpec, int pSkillArmorDirectionY)
	{
		mSkillArmorSpec = pSkillArmorSpec;
		mSkillArmorDirectionY = pSkillArmorDirectionY;

		mIsArmorSpec = (mTileArmorSpec != null) || (mSkillArmorSpec != null);
	}

	private bool mIsArmorSpec;

	private GroundArmorSpec mTileArmorSpec;
	private int mTileArmorDirectionY;

	private GroundArmorSpec mSkillArmorSpec;
	private int mSkillArmorDirectionY;
}

public class GroundSpec
{
	public int mIndex;

	public GroundType mType;
	public String mName;
	public String mNameText;

	public GroundStatTable mStatTable;
	public GroundArmorSpec mArmorSpec;

	// 파생 정보
	public bool mIsCeiling;

	public const GroundType cDefaultType = GroundType.Grass;
	public const int cTypeCount = (int)GroundType.None;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<GroundSpec[{0}]>", mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(GroundSpec pGroundSpec)
	{
		if (pGroundSpec != null)
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
