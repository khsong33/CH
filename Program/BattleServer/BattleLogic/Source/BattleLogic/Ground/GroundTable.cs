﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GroundTable
{
	internal static readonly String cAllTroopTypeToken = "All";

	public static readonly String cCSVTextAssetPath = "GroundTypeTableCSV";

	public static readonly String vNameColumnName = "Name";
	public static readonly String vNameTextColumnName = "NameText";

	public static readonly String vMoveAvailableColumnName = "MoveAvailable";
	public static readonly String vEffectAvailableColumnName = "EffectAvailable";

	public static readonly String vEffectAngleColumnName = "EffectAngle";
	public static readonly String vTSightRangeAddColumnName = "T_SightRangeAdd";
	public static readonly String vTAdvanceSpeedMultiplierColumnName = "T_AdvanceSpeedMultiplier";
	public static readonly String vTArmorSizeColumnName = "T_ArmorSize";
	public static readonly String vTArmorAddColumnName = "T_ArmorAdd";
	public static readonly String vASuppressionMultiplierColumnName = "A_SuppressionMultiplier";
	public static readonly String vWDistanceMultiplierColumnName = "W_DistanceMultiplier";
	public static readonly String vWAccuracyMultiplierColumnName = "W_AccuracyMultiplier";
	public static readonly String vWPenetrationMultiplierColumnName = "W_PenetrationMultiplier";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static GroundTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 기본 지형 타입 스펙
	public GroundSpec aDefaultSpec
	{
		get { return mAccessingSpecs[(int)GroundSpec.cDefaultType]; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new GroundTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public GroundTable()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load() // 예외적으로 pIsPreLoadAll=true로 가정
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		TroopSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TypeTable[{0}] duplicate Name '{1}'", lSpecName, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mTypeDictionary = new Dictionary<String, GroundType>();
		foreach (GroundType iForm in Enum.GetValues(typeof(GroundType)))
			mTypeDictionary.Add(iForm.ToString(), iForm);

		// 스펙 테이블을 초기화합니다.
		mLoadingSpecs = new GroundSpec[mCSVObject.RowNum];
		mAccessingSpecs = new GroundSpec[GroundSpec.cTypeCount];

		// 타입이 빠짐없이 들어있는지 검사합니다.
		for (int iType = 0; iType < GroundSpec.cTypeCount; ++iType)
			if (!mTypeDictionary.ContainsKey(((GroundType)iType).ToString()))
				Debug.LogError(String.Format("can't find type info '{0}'", ((GroundType)iType).ToString()));

		// 미리 로딩합니다.
		LoadAllGroundType();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 정보를 모두 읽어옵니다.
	public void LoadAllGroundType()
	{
		// Debug.LogWarning("LoadAllGroundType() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mLoadingSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mLoadingSpecs[iRow] = _LoadGroundSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public GroundSpec FindGroundSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIndex;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIndex))
		{
			if (mLoadingSpecs[lRowIndex] == null)
				mLoadingSpecs[lRowIndex] = _LoadGroundSpec(mCSVObject.GetRow(lRowIndex), lRowIndex);
			return mLoadingSpecs[lRowIndex];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타입 값으로 스펙에 바로 접근합니다.
	public GroundSpec GetSpec(GroundType pGroundType)
	{
		return mAccessingSpecs[(int)pGroundType];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private GroundSpec _LoadGroundSpec(CSVRow pCSVRow, int pRowIndex)
	{
		GroundSpec lGroundSpec = new GroundSpec();

		// 줄 인덱스
		lGroundSpec.mIndex = pRowIndex;

		lGroundSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		lGroundSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// type
		if (!mTypeDictionary.TryGetValue(lGroundSpec.mName, out lGroundSpec.mType))
			Debug.LogError(String.Format("invalid type name '{0}'", lGroundSpec.mName));

		// StatTable
		lGroundSpec.mStatTable = new GroundStatTable();
		{
			GroundStatTable lGroundStatTable = lGroundSpec.mStatTable;

			// MoveAvailable
			int lMoveAvailableFlag = _GetAvailableTroopType(lGroundSpec.mName, pCSVRow, vMoveAvailableColumnName);
			lGroundStatTable.Set((int)GroundStat.IsMoveAvailableInfantry, ((lMoveAvailableFlag & (1 << (int)TroopType.Infantry)) == 0) ? 1 : 0);
			lGroundStatTable.Set((int)GroundStat.IsMoveAvailableArmored, ((lMoveAvailableFlag & (1 << (int)TroopType.Armored)) == 0) ? 1 : 0);
			lGroundStatTable.Set((int)GroundStat.IsMoveAvailableArtillery, ((lMoveAvailableFlag & (1 << (int)TroopType.Artillery)) == 0) ? 1 : 0);

			// EffectAvailable
			int lEffectAvailableFlag = _GetAvailableTroopType(lGroundSpec.mName, pCSVRow, vEffectAvailableColumnName);
			lGroundStatTable.Set((int)GroundStat.IsEffectAvailableInfantry, ((lEffectAvailableFlag & (1 << (int)TroopType.Infantry)) == 0) ? 1 : 0);
			lGroundStatTable.Set((int)GroundStat.IsEffectAvailableArmored, ((lEffectAvailableFlag & (1 << (int)TroopType.Armored)) == 0) ? 1 : 0);
			lGroundStatTable.Set((int)GroundStat.IsEffectAvailableArtillery, ((lEffectAvailableFlag & (1 << (int)TroopType.Artillery)) == 0) ? 1 : 0);

			// T_SightRangeAdd
			lGroundStatTable.Set((int)GroundStat.SightRangeAdd, pCSVRow.GetIntValue(vTSightRangeAddColumnName));

			// T_AdvanceSpeedMultiplier
			lGroundStatTable.Set((int)GroundStat.AdvanceSpeedRatio, pCSVRow.GetRatioValue(vTAdvanceSpeedMultiplierColumnName));

			// IsHiding
			lGroundStatTable.Set((int)GroundStat.IsHiding, 0); // pCSVRow.GetBoolValue(vIsHidingColumnName)

			// W_DistanceMultiplier
			lGroundStatTable.Set((int)GroundStat.WeaponRangeRatio, pCSVRow.GetRatioValue(vWDistanceMultiplierColumnName));

			// W_AccuracyMultiplier
			lGroundStatTable.Set((int)GroundStat.AccuracyRatio, pCSVRow.GetRatioValue(vWAccuracyMultiplierColumnName));

			// W_PenetrationMultiplier
			lGroundStatTable.Set((int)GroundStat.PenetrationRatio, pCSVRow.GetRatioValue(vWPenetrationMultiplierColumnName));
		}

		// ArmorSpec
		int lEffectAngleHalf = pCSVRow.GetIntValue(vEffectAngleColumnName) / 2; // Half
		int lArmorSize = pCSVRow.GetIntValue(vTArmorSizeColumnName);
		int lArmorAdd = pCSVRow.GetIntValue(vTArmorAddColumnName);
		int lSuppressionRatio = pCSVRow.GetRatioValue(vASuppressionMultiplierColumnName);

		bool lIsValidArmorAdd = (lArmorSize > 0) && (lArmorAdd > 0);
		if (!lIsValidArmorAdd &&
			((lArmorSize > 0) || (lArmorAdd > 0)))
			Debug.LogWarning("inconsistent armor values - " + lGroundSpec.mNameText);
		bool lIsValidArmor = lIsValidArmorAdd || (lSuppressionRatio != 10000);
		if ((lEffectAngleHalf <= 0) && lIsValidArmor)
			Debug.LogWarning("inconsistent armor values - " + lGroundSpec.mNameText);

		if ((lEffectAngleHalf > 0) &&
			lIsValidArmor) // 의미있는 경우에만 추가
		{
			lGroundSpec.mArmorSpec = new GroundArmorSpec();

			// mEffectAngleHalf
			lGroundSpec.mArmorSpec.mEffectAngleHalf = lEffectAngleHalf;

			// mArmorSize
			lGroundSpec.mArmorSpec.mArmorSize = lArmorSize;

			// mArmorAdd
			lGroundSpec.mArmorSpec.mArmorAdd = lArmorAdd;

			// mSuppressionRatio
			lGroundSpec.mArmorSpec.mSuppressionRatio = lSuppressionRatio;
		}

		// 테이블에 등록합니다.
		mAccessingSpecs[(int)lGroundSpec.mType] = lGroundSpec;

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lGroundSpec);

		return lGroundSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(GroundSpec pGroundSpec)
	{
		// IsCeiling
		pGroundSpec.mIsCeiling = (pGroundSpec.mType == GroundType.Bunker);
	}
	//-------------------------------------------------------------------------------------------------------
	// 영향 받을 부대 타입을 얻어옵니다.
	private int _GetAvailableTroopType(String pTypeName, CSVRow pCSVRow, String pAvailableColumnName)
	{
		String lAvailableTypeString = pCSVRow.GetStringValue(pAvailableColumnName);

		int lResultAvailableType = 0;
		String[] lAvailableTypes = lAvailableTypeString.Split(',');
		for (int iType = 0; iType < lAvailableTypes.Length; iType++)
		{
			int lTroopType;
			try
			{
				lTroopType = (int)Enum.Parse(typeof(TroopType), lAvailableTypes[iType]);
				lResultAvailableType |= (1 << lTroopType);
			}
			catch (ArgumentException)
			{
				if (String.Compare(lAvailableTypes[iType], cAllTroopTypeToken) == 0)
				{
					lResultAvailableType = int.MinValue; // 0xffffffff
					return lResultAvailableType;
				}
				else
				{
					Debug.LogError(String.Format("TypeTable[{0}] invalid available troop '{1}' in {2}", pTypeName, lAvailableTypeString, pAvailableColumnName));
					lResultAvailableType = 0;
					return lResultAvailableType;
				}
			}
		}
		return lResultAvailableType;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static GroundTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, GroundType> mTypeDictionary;

	private GroundSpec[] mLoadingSpecs;
	private GroundSpec[] mAccessingSpecs;
}
