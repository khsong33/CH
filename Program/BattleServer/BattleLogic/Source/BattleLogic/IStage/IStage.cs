using UnityEngine;
using System.Collections;
using System;

public interface IStage
{
	// 본부를 생성합니다.
	IStageHeadquarter CreateHeadquarter(BattleHeadquarter pBattleHeadquarter);

	// 거점을 생성합니다.
	IStageCheckPoint CreateCheckPoint(BattleCheckPoint pBattleCheckPoint);

	// 이벤트를 생성합니다.
	IStageEventPoint CreateEventPoint(BattleEventPoint pBattleEventPoint);

	// 바리케이드를 생성합니다.
	IStageBarricade CreateBarricade(BattleBarricade pBattleBarricade);

	// 지휘관을 생성합니다.
	IStageCommander CreateCommander(BattleCommander pBattleCommander);

	// 부대를 생성합니다.
	IStageTroop CreateTroop(BattleTroop pBattleTroop);

	// 관찰자 더미를 생성합니다.
	IStageObserverDummy CreateObserverDummy(BattleObserverDummy pBattleObserverDummy);

	// 타게팅 더미를 생성합니다.
	IStageTargetingDummy CreateTargetingDummy(BattleTargetingDummy pBattleTargetingDummy);

	// 발사 효과를 생성합니다.
	IStageFireEffect CreateFireEffect(BattleFireEffect pBattleFireEffect);

	// 부대 스킬 효과를 생성합니다.
	IStageTroopSkillEffect CreateTroopSkillEffect(BattleTroopSkillEffect pBattleTroopSkillEffect);

	// 격자 타일 스킬 효과를 생성합니다.
	IStageGridSkillEffect CreateGridSkillEffect(BattleGridSkillEffect pBattleGridSkillEffect, int pDirectionY);
}
