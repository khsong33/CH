using UnityEngine;
using System.Collections;
using System;

public interface IStageGridSkillEffect : IStageEffect
{
	void Activate();
}
