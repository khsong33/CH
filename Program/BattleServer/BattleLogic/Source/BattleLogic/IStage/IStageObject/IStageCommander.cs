using UnityEngine;
using System.Collections;
using System;

public enum StageCommanderState
{
	Attack,
	AttackStop,
	AttackComplete,
	Hit,
	ConquerCheckPoint,
	ConquerStop,
	ConquerComplete,
	Move,
	MoveStop,

	None,
}

public interface IStageCommander : IStageObject
{
	void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY);
	void SetSelected(bool pIsSelected);

	void OnFrameUpdate(int pDeltaTime);
	void UpdateTeam(int pTeamIdx);

	void MoveTo(Coord2 pCoord, bool pIsArrived);
	void RotateToDirectionY(int pDirectionY);

	void ShowState(StageCommanderState pCommanderState);
	void ShowRetreat(bool pIsRetreat);
}
