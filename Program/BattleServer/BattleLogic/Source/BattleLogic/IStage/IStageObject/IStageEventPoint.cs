using UnityEngine;
using System.Collections;
using System;

public interface IStageEventPoint : IStageObject
{
	void SetDetected(int pDetectingTeamIdx, bool pIsToDetected);

	void UpdateCoord(Coord2 pCoord);
	void UpdateDirectionY(int pDirectionY);

	void OnTriggerReady();
	void OnTriggerActivated();
}
