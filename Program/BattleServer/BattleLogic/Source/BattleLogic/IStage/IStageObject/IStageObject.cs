using UnityEngine;
using System.Collections;
using System;

public interface IStageObject
{
	// 스테이지 인터페이스
	IStage aIStage { get; }

	// 상태 문자열
	String aStateText { get; set; }

	// HP 문자열
	String aHpText { get; set; }

	// 터치 반경의 제곱
	int aSqrTouchRadius { get; }

	// 월드 좌표
	Vector3 aWorldPosition { get; }

	// 파괴합니다.
	void DestroyObject();

	// 터치에 반응할 수 있는지 검사합니다.
	bool CheckTouch(BattleGrid.Touch pGridTouch);

	// 디버그 로그
	void DebugLog(String pLog);
}
