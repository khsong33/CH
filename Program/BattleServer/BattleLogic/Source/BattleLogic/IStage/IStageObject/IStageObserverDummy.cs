using UnityEngine;
using System.Collections;
using System;

public interface IStageObserverDummy : IStageObject
{
	void UpdateCoord(Coord2 pCoord, bool pIsArrived);
	void UpdateDirectionY(int pDirectionY);
}
