using UnityEngine;
using System.Collections;
using System;

public interface IStageTroop : IStageObject
{
	void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY);
	void SetSelected(bool pIsSelected);
	void SetTargetable(bool pIsTargetable);

	void OnFrameUpdate(int pDeltaTime);
	void UpdateTeam(int pTeamIdx);
	void UpdateDetection(int pDetectingTeamIdx, bool pIsToDetected);
	void UpdateHp(int pCurHp, int pMaxHp, TroopHpChangeType pHpChangeType);
	void UpdateExp();
	void UpdateVeteranRank(int pRank);
	void UpdateSightRange();
	void UpdateAttackRange();
	void UpdateHiding(bool pIsHiding);
	void UpdateMoraleState(TroopMoraleState pTroopMoraleState);
	void UpdateReturnState(bool pIsFallBehind);
	void UpdateLeaderState(bool pIsLeader);
	void UpdateRecruitState();

	void ShowHit(TroopHitResult pTroopHitResult);
	void ShowRetreat(bool pIsRetreat);

	void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType);
	void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay);
	void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay);
	void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay);
	void OnHoldMove(bool pIsHoldMove);
	void OnStopMove(Coord2 pStopCoord);

	void MoveTo(Coord2 pCoord, bool pIsArrived);
	void RotateBodyToDirectionY(int pDirectionY);
	void RotateBodyWeaponToDirectionY(int pDirectionY);
	void RotateTurretToDirectionY(int pDirectionY);

	void InstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay);
	void UnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay);
	void TrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay);
	void WindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay);
	void AimWeapon(int pAimDelay);
	void FireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim);
	void EndCooldown();

	void GoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop);
	void ComeBackInteraction();

	void StartSkill(BattleCommanderSkill pCommanderSkill);
	void StopSkill(BattleCommanderSkill pCommanderSkill);

	void StartAction(TroopActionType pActionType);
	void StopAction();

	void ShowTroopScript(string pMessageKey, float pDuration);
}
