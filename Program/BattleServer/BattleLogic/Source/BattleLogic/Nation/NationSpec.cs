﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NationSpec
{
	public int mIndex;

	public DeckNation mNation;
	public String mName;
	public String mNameText;
	public String mNameKey;
	public AssetKey mBackgroundIcon;
	public AssetKey mMarkIcon;
	public String mDefaultDeckNameKey;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<NationSpec[{0}] Name={1}>", mNation, mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 백그라운드 아이콘을 가져옵니다.
#if !BATTLE_VERIFY
	public Texture GetNationBackgroundIcon()
	{
		return TextureResourceManager.get.LoadTexture(mBackgroundIcon);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 마크 아이콘을 가져옵니다.
#if !BATTLE_VERIFY
	public Texture GetNationMarkIcon()
	{
		return TextureResourceManager.get.LoadTexture(mMarkIcon);
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
public class NationData
{
	public DeckNation mNation;
	public int mNationLevel;
	public int mNationExp;
	public int mArea;
	public int mRating;
	public NationData()
	{
		mNation = DeckNation.None;
		mNationLevel = 1;
		mNationExp = 0;
		mArea = 0;
		mRating = 0;
	}
}