﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NationTable
{
	public readonly String cCSVTextAssetPath = "NationTableCSV";

	public static readonly String vNationIdColumnName = "Nation";
	public static readonly String vNameColumnName = "Name";
	public static readonly String vNameTextColumnName = "NameText";
	public static readonly String vNameKeyColumnName = "NameKey";
	public static readonly String vIconBundleColumnName = "IconBundle";
	public static readonly String vBackgroundIconPathColumnName = "BackgroundIconPath";
	public static readonly String vMarkIconPathColumnName = "MarkIconPath";
	public static readonly String vDefaultDeckNameKeyColumnName = "DefaultDeckNameKey";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static NationTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new NationTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public NationTable()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load() // 예외적으로 pIsPreLoadAll=true로 가정
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		TroopSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TypeTable[{0}] duplicate Name '{1}'", lSpecName, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mNationDictionary = new Dictionary<String, DeckNation>();
		foreach (DeckNation iForm in Enum.GetValues(typeof(DeckNation)))
			mNationDictionary.Add(iForm.ToString(), iForm);

		// 스펙 테이블을 초기화합니다.
		mLoadingSpecs = new NationSpec[mCSVObject.RowNum];
		mAccessingSpecs = new NationSpec[(int)DeckNation.Count]; // 0번 중립국 포함

		// 미리 로딩합니다.
		LoadAllDeckNation();

		// 빠짐없이 로딩했는지 검사합니다.
		for (int iNation = 1; iNation < (int)DeckNation.Count; ++iNation) // 0번 중립국 빼고 검사 시작
			if (mAccessingSpecs[iNation].mNation != (DeckNation)iNation)
				Debug.LogError(String.Format("can't find nation '{0}'", ((DeckNation)iNation).ToString()));
	}
	//-------------------------------------------------------------------------------------------------------
	// 정보를 모두 읽어옵니다.
	public void LoadAllDeckNation()
	{
		// Debug.LogWarning("LoadAllDeckNation() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mLoadingSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mLoadingSpecs[iRow] = _LoadNationSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public NationSpec FindNationSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIndex;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIndex))
		{
			if (mLoadingSpecs[lRowIndex] == null)
				mLoadingSpecs[lRowIndex] = _LoadNationSpec(mCSVObject.GetRow(lRowIndex), lRowIndex);
			return mLoadingSpecs[lRowIndex];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타입 값으로 스펙에 바로 접근합니다.
	public NationSpec GetNationSpec(DeckNation pDeckNation)
	{
		return mAccessingSpecs[(int)pDeckNation];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("NationTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private NationSpec _LoadNationSpec(CSVRow pCSVRow, int pRowIndex)
	{
		NationSpec lNationSpec = new NationSpec();

		// 줄 인덱스
		lNationSpec.mIndex = pRowIndex;
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, pRowIndex.ToString());

		// Nation
		int lNationId = pCSVRow.GetIntValue(vNationIdColumnName);
		lNationSpec.mNation = (DeckNation)lNationId;

		// Name, NameText, NameKey
		lNationSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		lNationSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);
		lNationSpec.mNameKey = pCSVRow.GetStringValue(vNameKeyColumnName);

		// BackgroundIcon, MarkIcon
		lNationSpec.mBackgroundIcon = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vBackgroundIconPathColumnName));
		lNationSpec.mMarkIcon = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vMarkIconPathColumnName));

		// DefaultDeckNameKey
		lNationSpec.mDefaultDeckNameKey = pCSVRow.GetStringValue(vDefaultDeckNameKeyColumnName);

		// 테이블에 등록합니다.
		mAccessingSpecs[(int)lNationSpec.mNation] = lNationSpec;

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lNationSpec);

		return lNationSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(NationSpec pNationSpec)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static NationTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, DeckNation> mNationDictionary;

	private NationSpec[] mLoadingSpecs;
	private NationSpec[] mAccessingSpecs;
}
