﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RankEffectTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "CommanderUpgradeTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";

	public readonly String vLocalNameKeyColumnName = "LocalNameKey";
	public readonly String vLocalDescKeyColumnName = "LocalDescKey";
	public readonly String vEffectiveKeyColumnName = "EffectiveKey";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static RankEffectTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new RankEffectTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public RankEffectTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 스펙 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lSkillName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSkillName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 이펙트 스펙 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("SkillTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 이펙트 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSkillName))
			{
				Debug.LogError(String.Format("SkillTable[{0}] duplicate Name '{1}'", lSpecNo, lSkillName));
				continue;
			}
			mNameToRowDictionary.Add(lSkillName, iRow);
		}

		// 스펙 테이블을 초기화합니다.
		pRankEffectSpecs = new RankEffectSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllRankEffectSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 정보를 모두 읽어옵니다.
	public void LoadAllRankEffectSpec()
	{
		Debug.LogWarning("LoadAllSkillSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (pRankEffectSpecs[iRow] == null) // _SetUpSpawnLinkSkillInfo() 속에서 초기화되었을 수도 있습니다.
				pRankEffectSpecs[iRow] = _LoadRankEffectSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public RankEffectSpec GetRankEffectByIdx(int pRankEffectIdx)
	{
		if (pRankEffectSpecs[pRankEffectIdx] == null)
			pRankEffectSpecs[pRankEffectIdx] = _LoadRankEffectSpec(mCSVObject.GetRow(pRankEffectIdx), pRankEffectIdx);
		return pRankEffectSpecs[pRankEffectIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public RankEffectSpec FindRankEffectSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (pRankEffectSpecs[lRowIdx] == null)
				pRankEffectSpecs[lRowIdx] = _LoadRankEffectSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return pRankEffectSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public RankEffectSpec FindRankEffectSpec(String pRankEffectName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pRankEffectName, out lRowIdx))
		{
			if (pRankEffectSpecs[lRowIdx] == null)
				pRankEffectSpecs[lRowIdx] = _LoadRankEffectSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return pRankEffectSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pRankEffectName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return pRankEffectSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private RankEffectSpec _LoadRankEffectSpec(CSVRow pCSVRow, int pRowIdx)
	{
		RankEffectSpec lRankEffectSpec = new RankEffectSpec();

		// 줄 인덱스
		lRankEffectSpec.mIdx = pRowIdx;

		// Name
		lRankEffectSpec.mName = pCSVRow.GetStringValue(vNameColumnName);

		// SpecNo
		lRankEffectSpec.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// NameText
		lRankEffectSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// LocalNameKey
		lRankEffectSpec.mLocalNameKey = pCSVRow.GetStringValue(vLocalNameKeyColumnName);

		// LocalDescKey
		lRankEffectSpec.mLocalDescKey = pCSVRow.GetStringValue(vLocalDescKeyColumnName);

		// EffectiveKey
		lRankEffectSpec.mEffectiveKey = pCSVRow.GetStringValue(vEffectiveKeyColumnName);

		return lRankEffectSpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static RankEffectTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private RankEffectSpec[] pRankEffectSpecs;
}


public class RankEffectSpec
{
	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameText;
	public String mLocalNameKey;
	public String mLocalDescKey;
	public String mEffectiveKey;
}
