using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleConfig
{
	public class DebugSetting
	{
		public bool mIsShowSearchCount = false;
	}

	public int mTickDelay;
	public int mFrameTime;
	public int mFrameSliceCount;
	public int mTaskFrameRange;

	public int mModeTime;
	public int mOccupationDelayTime;
	public int mTargetResearchDelay;
	public int mTestMaxMp;
	public int mTestStartMp;
	public int mTestMaxVp;
	public int mTestStartVp;
	public int mDestroyBonusMpRatio;
	public int mSpawnMpIncRatio;
	public int mDamageRatioTableMaxDistance;
	public int mDetectionChangeDelay;
	public int mDetectionOffDelay;
	public int mSqrFollowStartDistance;
	public int mSqrFollowStopDistance;
	public int mSqrFormationStartDistance;
	public int mAttackDetectionDelay;
	public int mAttackDetectionRangeMargin;

	public int mRTPointAddTroopNum;

	public int mMedicAIDistance;
	public int mRepairAIDistance;

	public int mTotalCommanderCount;

	public int mAircraftSpawnDelay;
	public int[] mRecoveryStartHpPercents;
	public int[] mRecoveryEndHpPercents;
	public int[] mMoraleHitRateMilliAdds;
	public int[] mInteractionTypeHitRateMilliAdds;

	public int mCheckPointCaptureRange;
	public int mSqrCheckPointCaptureRange;
	public int mCheckPointInterruptRange;
	public int mCheckPointTeamChangeDelay;
	public int mHeadquarterMpMilliGeneration;
	public int mCheckPointMpMilliGeneration;
	public int mCheckPointEnemyMilliVpDecrease;
	public int mHeadquarterEnemyMilliVpDecrease;
	public int mEventPointCheckRange;
	public int mSqrEventPointCheckRange;

	public Coord2 mGridTileBlockScale;
	public Coord2 mGridCellBlockScale;
	public int mMaxUnitRadius;
	public float mMinTouchRadius;
	public int mSqrDetectionUpdateDegree;
// 	public Vector3 mTroopPositioningScale;
	public int mTroopPathTileWeight;

	public int mSideSuppressionAngleDistance;
	public int mRearSuppressionAngleDistance;
	public int mFrontSuppressionMultiplyRatio;
	public int mSideSuppressionMultiplyRatio;
	public int mRearSuppressionMultiplyRatio;

	public int mMpBuffEventStartTime;
	public int mMpBuffEventMilliRatio;

	// AI 관련 데이터
	public int mVPValueCons1;
	public int mVPValueCons2;
	public int mVPCompareCons;
	public int mMPValueCons1;
	public int mMPValueCons2;
	public int mMPCompareCons;
	public float mInfluenceSlopeCons;
	public float mInfluenceActiveValue;
	public float mMPMultiplierCons;

	public int mTroopGroupingRangeMargin;

	public int mDefaultCommaderItemCount;
	public int mMaxTroopNum;
	public int mMaxVeterancyRank;
	public int mRetreatOrderDelay;
	public int mHidingCheckRangeRatio;

	//public int mFinalAttackBonusExp;
	public int mSuvivorBonusExp;
	public int mVitoryBonusExp;

	public int[] mMaxCommanderExpRank;

	public Color[] mRankColors;
	public Color[] mTroopMoraleStateColors;

	public int[] mRifleToHelmetDamageRatios;
	public int[] mRifleToArmorDamageRatios;
	public int[] mRifleToBunkerDamageRatios;
	public int[] mRifleToTrenchDamageRatios;
	public int[] mRifleToBarrierDamageRatios;

	public int[] mGrenadeToHelmetDamageRatios;
	public int[] mGrenadeToArmorDamageRatios;
	public int[] mGrenadeToBunkerDamageRatios;
	public int[] mGrenadeToTrenchDamageRatios;
	public int[] mGrenadeToBarrierDamageRatios;

	public int[] mMachineGunToHelmetDamageRatios;
	public int[] mMachineGunToArmorDamageRatios;
	public int[] mMachineGunToBunkerDamageRatios;
	public int[] mMachineGunToTrenchDamageRatios;
	public int[] mMachineGunToBarrierDamageRatios;

	public int[] mTankGunToHelmetDamageRatios;
	public int[] mTankGunToArmorDamageRatios;
	public int[] mTankGunToBunkerDamageRatios;
	public int[] mTankGunToTrenchDamageRatios;
	public int[] mTankGunToBarrierDamageRatios;

	public int[] mHighAngleGunToHelmetDamageRatios;
	public int[] mHighAngleGunToArmorDamageRatios;
	public int[] mHighAngleGunToBunkerDamageRatios;
	public int[] mHighAngleGunToTrenchDamageRatios;
	public int[] mHighAngleGunToBarrierDamageRatios;

	// 파생 정보
	public int mSqrFollowWaitDistance;

	private static readonly String cCSVTextAssetPath = "BattleConfigCSV";

	private static readonly String vNameColumnName = "Name";
	private static readonly String[] vValueColumnNames = { "Value0", "Value1", "Value2", "Value3", "Value4", "Value5", "Value6", "Value7", "Value8", "Value9" };

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static BattleConfig get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 세팅
	public DebugSetting aDebugSetting
	{
		get { return mDebugSetting; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤을 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new BattleConfig();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleConfig()
	{
		mDebugSetting = new DebugSetting();

	#if !BATTLE_VERIFY
		if (Application.isPlaying)
	#endif
			Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load()
	{
		//Debug.Log("Load() - " + this);

		_LoadTable();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 한 줄을 얻습니다.
	private void _GetRow(String pRowName)
	{
		mCurrentCSVRow = mCSVTable.GetRow(pRowName);
		if (mCurrentCSVRow == null)
			Debug.LogError("can't find row: " + pRowName + " - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 줄에서 해당 번호의 값을 얻습니다.
	private String _GetRowString(int pValueIndex)
	{
		return mCSVTable.GetStringValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 정수 값을 얻습니다.
	private int _GetIntValue(int pValueIndex)
	{
		return mCSVTable.GetIntValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 밀리 단위의 정수 값을 얻습니다.
	private int _GetMilliIntValue(int pValueIndex)
	{
		return mCSVTable.GetMilliIntValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 만분율 값을 얻습니다.
	private int _GetRatioValue(int pValueIndex)
	{
		return mCSVTable.GetRatioValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 정수 값을 얻습니다.
	private float _GetFloatValue(int pValueIndex)
	{
		return mCSVTable.GetFloatValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 컬러 값을 얻습니다.
	private Color _GetColorValue(int pValueIndex)
	{
		return mCSVTable.GetColorValue(mCurrentCSVRow, vValueColumnNames[pValueIndex]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 읽습니다.
	private void _LoadTable()
	{
		mCSVTable = new CSVTable();
		//Debug.Log("AssetManager.get=" + AssetManager.get + " Config.get=" + Config.get);
		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVTable.Load(lCSVText, vNameColumnName);

		_ReadRows();

		mCSVTable = null; // 해제
		mCurrentCSVRow = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄을 읽습니다.
	private void _ReadRows()
	{
// 		_GetRow("TickDelay");
// 		mTickDelay = _GetIntValue(0);
		mTickDelay = 300;

		_GetRow("FrameTime");
		mFrameTime = _GetIntValue(0);

		_GetRow("FrameSliceCount");
		mFrameSliceCount = _GetIntValue(0);

		_GetRow("TaskFrameRange");
		mTaskFrameRange = _GetIntValue(0);

		_GetRow("ModeTime");	
		mModeTime = _GetIntValue(0);
		
		_GetRow("OccupationDelayTime");
		mOccupationDelayTime = _GetIntValue(0);

		_GetRow("TargetResearchDelay");
		mTargetResearchDelay = _GetIntValue(0);

		_GetRow("TestMaxMp");
		mTestMaxMp = _GetIntValue(0);

		_GetRow("TestStartMp");
		mTestStartMp = _GetIntValue(0);

		_GetRow("TestMaxVp");
		mTestMaxVp = _GetIntValue(0);

		_GetRow("TestStartVp");
		mTestStartVp = _GetIntValue(0);

		_GetRow("DestroyBonusMpRatio");
		mDestroyBonusMpRatio = _GetIntValue(0);

		_GetRow("SpawnMpIncRatio");
		mSpawnMpIncRatio = _GetIntValue(0);
		
		_GetRow("DamageRatioTableMaxDistance");
		mDamageRatioTableMaxDistance = _GetIntValue(0);

		_GetRow("DetectionChangeDelay");
		mDetectionChangeDelay = _GetIntValue(0);

		_GetRow("DetectionOffDelay");
		mDetectionOffDelay = _GetIntValue(0);

		_GetRow("FollowStartDistance");
		int lFollowStartDistance = _GetIntValue(0);
		mSqrFollowStartDistance = lFollowStartDistance * lFollowStartDistance;

		_GetRow("FollowStopDistance");
		int lFollowStopDistance = _GetIntValue(0);
		mSqrFollowStopDistance = lFollowStopDistance * lFollowStopDistance;

		_GetRow("FormationStartDistance");
		int lFormationStartDistance = _GetIntValue(0);
		mSqrFormationStartDistance = lFormationStartDistance * lFormationStartDistance;

		_GetRow("AttackDiscoverDelay");
		mAttackDetectionDelay = _GetIntValue(0);

		_GetRow("AttackDiscoverRange");
		mAttackDetectionRangeMargin = _GetIntValue(0);

		_GetRow("AircraftSpawnAfterDelay");
		mAircraftSpawnDelay = _GetIntValue(0);

		DebugUtil.Assert(TroopSpec.cTroopBodyCount == Enum.GetValues(typeof(TroopBody)).Length, "cTroopBodyCount mismatch - " + this);
		mRecoveryStartHpPercents = new int[TroopSpec.cTroopBodyCount];
		mRecoveryEndHpPercents = new int[TroopSpec.cTroopBodyCount];

		_GetRow("TotalCommanderNum");
		mTotalCommanderCount = _GetIntValue(0);

		_GetRow("RTPointAddTroopNum");
		mRTPointAddTroopNum = _GetIntValue(0);

		_GetRow("MedicAIDistance");
		mMedicAIDistance = _GetIntValue(0);
		_GetRow("RepairAIDistance");
		mRepairAIDistance = _GetIntValue(0);

		_GetRow("MedKitHPLimitRatio");
		mRecoveryStartHpPercents[(int)TroopBody.Soldier] = _GetIntValue(0);
		_GetRow("RepairKitHPLimitRatio");
		mRecoveryStartHpPercents[(int)TroopBody.Vehicle] = _GetIntValue(0);
		_GetRow("MedKitHPMaxRatio");
		mRecoveryEndHpPercents[(int)TroopBody.Soldier] = _GetIntValue(0);
		_GetRow("RepairKitHPMaxRatio");
		mRecoveryEndHpPercents[(int)TroopBody.Vehicle] = _GetIntValue(0);

		DebugUtil.Assert(TroopSpec.cTroopMoraleStateCount == Enum.GetValues(typeof(TroopMoraleState)).Length, "cTroopMoraleStateCount mismatch - " + this);
		mMoraleHitRateMilliAdds = new int[TroopSpec.cTroopMoraleStateCount];

		_GetRow("Default_HitRateAdd");
		mMoraleHitRateMilliAdds[(int)TroopMoraleState.Good] = _GetMilliIntValue(0);
		_GetRow("Suppressed_HitRateAdd");
		mMoraleHitRateMilliAdds[(int)TroopMoraleState.Suppressed] = _GetMilliIntValue(0);
		_GetRow("PinDown_HitRateAdd");
		mMoraleHitRateMilliAdds[(int)TroopMoraleState.PinDown] = _GetMilliIntValue(0);

		DebugUtil.Assert(TroopSpec.cTroopInteractionTypeCount == Enum.GetValues(typeof(TroopInteractionType)).Length, "cTroopInteractionTypeCount mismatch - " + this);
		mInteractionTypeHitRateMilliAdds = new int[TroopSpec.cTroopInteractionTypeCount];
		for (int iType = 0; iType < TroopSpec.cTroopInteractionTypeCount; ++iType)
			mInteractionTypeHitRateMilliAdds[iType] = 0;

		_GetRow("FieldMedKit_HitRateAdd");
		mInteractionTypeHitRateMilliAdds[(int)TroopInteractionType.Recovery] = _GetMilliIntValue(0);

		_GetRow("CheckPointCaptureRange");
		mCheckPointCaptureRange = _GetIntValue(0);
		mSqrCheckPointCaptureRange = mCheckPointCaptureRange * mCheckPointCaptureRange;

		_GetRow("CheckPointInterruptRange");
		mCheckPointInterruptRange = _GetIntValue(0);
		if (mCheckPointInterruptRange < mCheckPointCaptureRange)
			Debug.LogError("mCheckPointInterruptRange < mCheckPointCaptureRange");

		_GetRow("CheckPointTeamChangeDelay");
		mCheckPointTeamChangeDelay = _GetIntValue(0);

		_GetRow("HeadquarterMpGeneration");
		mHeadquarterMpMilliGeneration = _GetIntValue(0);

		_GetRow("CheckPointMpGeneration");
		mCheckPointMpMilliGeneration = _GetIntValue(0);

		_GetRow("CheckPointEnemyVpDecrease");
		mCheckPointEnemyMilliVpDecrease = _GetMilliIntValue(0);

		_GetRow("HeadquarterEnemyVpDecrease");
		mHeadquarterEnemyMilliVpDecrease = _GetMilliIntValue(0);

		mEventPointCheckRange = 300;
		mSqrEventPointCheckRange = mEventPointCheckRange * mEventPointCheckRange;

		_GetRow("GridTileBlockScaleX");
		int lGridTileBlockScaleX = _GetIntValue(0);
		_GetRow("GridTileBlockScaleZ");
		int lGridTileBlockScaleZ = _GetIntValue(0);
		mGridTileBlockScale = new Coord2(lGridTileBlockScaleX, lGridTileBlockScaleZ);

		_GetRow("GridCellBlockScaleX");
		int lGridCellBlockScaleX = _GetIntValue(0);
		_GetRow("GridCellBlockScaleZ");
		int lGridCellBlockScaleZ = _GetIntValue(0);
		mGridCellBlockScale = new Coord2(lGridCellBlockScaleX, lGridCellBlockScaleZ);

		_GetRow("MaxUnitSize");
		mMaxUnitRadius = _GetIntValue(0) / 2; // 반지름

		mMinTouchRadius = mMaxUnitRadius * MathUtil.cCentiToMeter * 0.8f; // 일단 유닛 최대 크기의 80%로 정함

		mSqrDetectionUpdateDegree = 200 * 200; // 임시로 2미터 정밀도로 탐지를 갱신

// 		_GetRow("TroopPositioningScaleX");
// 		float lTroopPositioningScaleX = _GetFloatValue(0);
// 		_GetRow("TroopPositioningScaleZ");
// 		float lTroopPositioningScaleZ = _GetFloatValue(0);
// 		mTroopPositioningScale = new Vector3(lTroopPositioningScaleX, 1.0f, lTroopPositioningScaleZ);

		_GetRow("TroopPathTileWeight");
		mTroopPathTileWeight = _GetIntValue(0);

		_GetRow("SideSupAngle");
		mSideSuppressionAngleDistance = 180 - _GetIntValue(0);
		_GetRow("RearSupAngle");
		mRearSuppressionAngleDistance = 180 - _GetIntValue(0);
		_GetRow("FrontSupPointMultiplier");
		mFrontSuppressionMultiplyRatio = _GetRatioValue(0);
		_GetRow("SideSupPointMultiplier");
		mSideSuppressionMultiplyRatio = _GetRatioValue(0);
		_GetRow("RearSupPointMultiplier");
		mRearSuppressionMultiplyRatio = _GetRatioValue(0);

		_GetRow("MpBuffEventStartTime");
		mMpBuffEventStartTime = _GetIntValue(0);
		_GetRow("MpBuffEventRatio");
		mMpBuffEventMilliRatio = _GetRatioValue(0);

		_GetRow("VPValueCons1");
		mVPValueCons1 = _GetIntValue(0);
		_GetRow("VPValueCons2");
		mVPValueCons2 = _GetIntValue(0);
		_GetRow("VPCompareCons");
		mVPCompareCons = _GetIntValue(0);
		_GetRow("MPValueCons1");
		mMPValueCons1 = _GetIntValue(0);
		_GetRow("MPValueCons2");
		mMPValueCons2 = _GetIntValue(0);
		_GetRow("MPCompareCons");
		mMPCompareCons = _GetIntValue(0);
		_GetRow("InfluenceSlopeCons");
		mInfluenceSlopeCons = _GetFloatValue(0);
		_GetRow("InfluenceActiveValue");
		mInfluenceActiveValue = _GetFloatValue(0);
		_GetRow("MPMultiplierCons");
		mMPMultiplierCons = _GetFloatValue(0);

		_GetRow("TroopGroupingRangeMargin");
		mTroopGroupingRangeMargin = _GetIntValue(0);

		_GetRow("DefaultCommanderItemCount");
		mDefaultCommaderItemCount = _GetIntValue(0);

		_GetRow("MaxTroopNum");
		mMaxTroopNum = _GetIntValue(0);

		_GetRow("MaxVeterancyRank");
		mMaxVeterancyRank = _GetIntValue(0);

		_GetRow("RetreatOrderDelay");
		mRetreatOrderDelay = _GetIntValue(0);

		_GetRow("HidingCheckRangeRatio");
		mHidingCheckRangeRatio = _GetRatioValue(0);

		//_GetRow("FinalAttackBonusExp");
		//mFinalAttackBonusExp = _GetIntValue(0);

		_GetRow("SurvivorBonusExp");
		mSuvivorBonusExp = _GetIntValue(0);

		_GetRow("VictoryBonusExp");
		mVitoryBonusExp = _GetIntValue(0);

		_GetRow("MaxCommanderExpRank");
		mMaxCommanderExpRank = new int[CommanderSpec.cRankCount];
		mMaxCommanderExpRank[0] = 0;
		for (int iRank = 1; iRank < CommanderSpec.cRankCount; iRank++)
			mMaxCommanderExpRank[iRank] = _GetIntValue(iRank - 1);

		_GetRow("RankColors");
		mRankColors = new Color[CommanderSpec.cRankCount];
		mRankColors[0] = Color.white;
		for (int iRank = 1; iRank < CommanderSpec.cRankCount; ++iRank)
			mRankColors[iRank] = _GetColorValue(iRank - 1); // 표에서 0 랭크는 생략

		_GetRow("TroopMoraleStateColors");
		int lTroopMoraleStateEnumCount = Enum.GetValues(typeof(TroopMoraleState)).Length;
		mTroopMoraleStateColors = new Color[lTroopMoraleStateEnumCount];
		for (int iState = 0; iState < lTroopMoraleStateEnumCount; ++iState)
			mTroopMoraleStateColors[iState] = _GetColorValue(iState);

		_GetRow("RifleToHelmetDamageRatios");
		mRifleToHelmetDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mRifleToHelmetDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("RifleToArmorDamageRatios");
		mRifleToArmorDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mRifleToArmorDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("RifleToBunkerDamageRatios");
		mRifleToBunkerDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mRifleToBunkerDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("RifleToTrenchDamageRatios");
		mRifleToTrenchDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mRifleToTrenchDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("RifleToBarrierDamageRatios");
		mRifleToBarrierDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mRifleToBarrierDamageRatios[iDistance] = _GetIntValue(iDistance);

		_GetRow("GrenadeToHelmetDamageRatios");
		mGrenadeToHelmetDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mGrenadeToHelmetDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("GrenadeToArmorDamageRatios");
		mGrenadeToArmorDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mGrenadeToArmorDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("GrenadeToBunkerDamageRatios");
		mGrenadeToBunkerDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mGrenadeToBunkerDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("GrenadeToTrenchDamageRatios");
		mGrenadeToTrenchDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mGrenadeToTrenchDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("GrenadeToBarrierDamageRatios");
		mGrenadeToBarrierDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mGrenadeToBarrierDamageRatios[iDistance] = _GetIntValue(iDistance);

		_GetRow("MachineGunToHelmetDamageRatios");
		mMachineGunToHelmetDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mMachineGunToHelmetDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("MachineGunToArmorDamageRatios");
		mMachineGunToArmorDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mMachineGunToArmorDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("MachineGunToBunkerDamageRatios");
		mMachineGunToBunkerDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mMachineGunToBunkerDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("MachineGunToTrenchDamageRatios");
		mMachineGunToTrenchDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mMachineGunToTrenchDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("MachineGunToBarrierDamageRatios");
		mMachineGunToBarrierDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mMachineGunToBarrierDamageRatios[iDistance] = _GetIntValue(iDistance);

		_GetRow("TankGunToHelmetDamageRatios");
		mTankGunToHelmetDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mTankGunToHelmetDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("TankGunToArmorDamageRatios");
		mTankGunToArmorDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mTankGunToArmorDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("TankGunToBunkerDamageRatios");
		mTankGunToBunkerDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mTankGunToBunkerDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("TankGunToTrenchDamageRatios");
		mTankGunToTrenchDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mTankGunToTrenchDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("TankGunToBarrierDamageRatios");
		mTankGunToBarrierDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mTankGunToBarrierDamageRatios[iDistance] = _GetIntValue(iDistance);

		_GetRow("HighAngleGunToHelmetDamageRatios");
		mHighAngleGunToHelmetDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mHighAngleGunToHelmetDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("HighAngleGunToArmorDamageRatios");
		mHighAngleGunToArmorDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mHighAngleGunToArmorDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("HighAngleGunToBunkerDamageRatios");
		mHighAngleGunToBunkerDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mHighAngleGunToBunkerDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("HighAngleGunToTrenchDamageRatios");
		mHighAngleGunToTrenchDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mHighAngleGunToTrenchDamageRatios[iDistance] = _GetIntValue(iDistance);
		_GetRow("HighAngleGunToBarrierDamageRatios");
		mHighAngleGunToBarrierDamageRatios = new int[mDamageRatioTableMaxDistance + 1];
		for (int iDistance = 0; iDistance <= mDamageRatioTableMaxDistance; ++iDistance)
			mHighAngleGunToBarrierDamageRatios[iDistance] = _GetIntValue(iDistance);

		// 파생 정보를 초기화합니다.
		mSqrFollowWaitDistance = mGridTileBlockScale.x * mGridTileBlockScale.x + mGridTileBlockScale.z * mGridTileBlockScale.z;
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static BattleConfig sInstance = null;

	private DebugSetting mDebugSetting;

	private CSVTable mCSVTable;
	private CSVRow mCurrentCSVRow;
}
