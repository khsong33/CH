using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum SkillUseType
{
	Default,
	Position,
}

public enum SkillStartType
{
	OnActivation,
	AfterFormation,
}

public enum SkillFunction
{
	None,
	GroundEffect,
	GroundAttack,
	TargetAttack,
	TroopSupplement,
	FieldMedKit,
	Repair,
}

public class SkillSpec
{
	public class GroundEffectParam
	{
		public GroundEffectSpec mGroundEffectSpec;
		public int mTileDistance;
		public int mDurationDelay;
	}
	public class GroundAttackParam
	{
		public int mAttackCount;
	}
	public class TargetAttackParam
	{
		public int mAttackCount;
		public TroopType mAttackBody;
	}
	public class TroopSupplementParam
	{
	}
	public class FieldRecoveryParam
	{
		public int mRecoveryDistance;
		public int mRecoveryEffectDelay;
		public int mRecoveryActionDelay;
		public int mRecoveryRatio;
	}

	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameText;
	public String mNameKey;
	public String mDescKey;
	public String mControlKey;
	public string mTypeKey;

	public AssetKey mIconAssetKey;
	public AssetKey mStateIconAssetKey;

	public SkillUseType mUseType;
	public int mReadyDelay;
	public int mUseDelay;
	public int mCooldownDelay;

	public TroopEffectSpec mTroopEffectSpec;

	public Coord2[] mFormationPositionOffsets;
	public bool mIsFormationMove;
	public bool mIsGoToTileCenter;

	public SkillFunction mFunction;
	public GroundEffectParam mGroundEffectParam;
	public GroundAttackParam mGroundAttackParam;
	public TargetAttackParam mTargetAttackParam;
	public TroopSupplementParam mTroopSupplementParam;
	public FieldRecoveryParam mFieldRecoveryParam;

	// 파생 정보
	public SkillStartType mStartType; // 스킬 효과가 시작되는 타입
	public bool mIsMovable; // 스킬 효과 중에 움직일 수 있는가
	public bool mIsForceFormationMoveWhenSkill; // 스킬 사용 전에 포메이션 이동을 강제하는가

	public const int cIntValueCount = 5;
	public const int cRatioValueCount = 5;
	public const int cStringValueCount = 5;

	public const int cSkillFunctionCount = 7;
	public const int cMaxSkillFunctionGridTileDistance = 2; // [0]=1, [1]=6, [2]=12
	public const int cMaxSkillFunctionGridTileCount = (1 + 6 + 12);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// Active Skill 여부
	public bool aIsActiveSkill { get { return mUseDelay > 0; } }
	public bool aIsToggleSkill { get { return mUseDelay <= 0; } }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<SkillSpec[{0}] No={1} Name={2}>", mIdx, mNo, mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(SkillSpec pSkillSpec)
	{
		if ((pSkillSpec != null) &&
			(pSkillSpec.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 버튼 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 필드 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetStateIconTexture()
	{
		if (mStateIconAssetKey != null)
			return TextureResourceManager.get.LoadTexture(mStateIconAssetKey);
		else
			return null;
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
