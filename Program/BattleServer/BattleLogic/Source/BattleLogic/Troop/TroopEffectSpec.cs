using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TroopEffectSpec
{
	public int mIndex;

	public String mName;
	public String mNameText;

	public StatTableModifier<TroopStat> mTroopStatTableModifier;
	public StatTableModifier<WeaponStat>[] mWeaponStatTableModifiers;
	public StatTableModifier<WeaponRangeStat>[] mWeaponRangeStatTableModifiers;
	public StatTableModifier<WeaponAoeStat>[] mWeaponAoeStatTableModifiers;

	// 파생 정보

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<TroopEffectSpec[{0}]>", mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(TroopEffectSpec pTroopEffectSpec)
	{
		if (pTroopEffectSpec != null)
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
