using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TroopEffectTable
{
	public readonly String cCSVTextAssetPath = "TroopEffectTableCSV";

	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";

	public readonly String vMaxRotationDelayRatioColumnName = "T_MaxRotationDelayMultiplier";
	public readonly String vAdvanceSpeedRatioColumnName = "T_AdvanceSpeedMultiplier";
	public readonly String vArmorRatioColumnName = "T_ArmorMultiplier";
	public readonly String vSightRangeRatioColumnName = "T_SightRangeMultiplier";
	public readonly String vBaseHpRatioColumnName = "T_BaseHpMultiplier";
	public readonly String vSetUpDelayAddColumnName = "T_SetUpDelayAdd";
	public readonly String vHitRateAddColumnName = "C_HitRateAdd";
	public readonly String vAttackDisabledOverwriteColumnName = "AttackDisabledOverwrite";
	public readonly String vHidingOverwriteColumnName = "HidingOverwrite";
	public readonly String vAdvanceTypeOverwriteColumnName = "CM_AdvanceTypeOverwrite";
	public readonly String vMaxMilliMoraleRatioColumnName = "T_MaxMoraleMultiplier";

	public readonly String vPrimaryWeaponRangeRatioColumnName = "PW_DistanceMultiplier";
	public readonly String vSecondaryWeaponRangeRatioColumnName = "SW_DistanceMultiplier";
	public readonly String vPrimaryWeaponAccuracyRatioColumnName = "PW_AccuracyMultiplier";
	public readonly String vSecondaryWeaponAccuracyRatioColumnName = "SW_AccuracyMultiplier";
	public readonly String vPrimaryWeaponMaxTrackingDelayRatioColumnName = "PW_MaxAimDelayMultiplier";
	public readonly String vSecondaryWeaponMaxTrackingDelayRatioColumnName = "SW_MaxAimDelayMultiplier";
	public readonly String vPrimaryWeaponAttackDelayRatioColumnName = "PW_CooldownMultiplier";
	public readonly String vSecondaryWeaponAttackDelayRatioColumnName = "SW_CooldownMultiplier";
	public readonly String vPrimaryWeaponSuppressionPointRatioColumnName = "PW_SuppressionPointMultiplier";
	public readonly String vSecondaryWeaponSuppressionPointRatioColumnName = "SW_SuppressionPointMultiplier";
	public readonly String vPrimaryWeaponScatterAngleRatioColumnName = "PW_ScatterAngleMultiplier";
	public readonly String vSecondaryWeaponScatterAngleRatioColumnName = "SW_ScatterAngleMultiplier";
	public readonly String vPrimaryWeaponAttackDamageRatioColumnName = "PW_AttackDamageMultiplier";
	public readonly String vSecondaryWeaponAttackDamageRatioColumnName = "SW_AttackDamageMultiplier";
	public readonly String vPrimaryWeaponPenetrationRatioColumnName = "PW_PenetrationMultiplier";
	public readonly String vSecondaryWeaponPenetrationRatioColumnName = "SW_PenetrationMultiplier";

	public readonly String vPrimaryWeaponAoeDistanceRatioColumnName = "PW_AoeDistanceMultiplier";
	public readonly String vSecondaryWeaponAoeDistanceRatioColumnName = "SW_AoeDistanceMultiplier";
	public readonly String vPrimaryWeaponAoePenetrationMultiplyRatioAddColumnName = "PW_AoePenetrationMultiplierAdd";
	public readonly String vSecondaryWeaponAoePenetrationMultiplyRatioAddColumnName = "SW_AoePenetrationMultiplierAdd";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static TroopEffectTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new TroopEffectTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public TroopEffectTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		TroopEffectSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TypeTable[{0}] duplicate Name '{1}'", lSpecName, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.

		// 스펙 테이블을 초기화합니다.
		mTroopEffectSpecs = new TroopEffectSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllTroopEffectType();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 정보를 모두 읽어옵니다.
	public void LoadAllTroopEffectType()
	{
		// Debug.LogWarning("LoadAllTroopEffectType() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mTroopEffectSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mTroopEffectSpecs[iRow] = _LoadTroopEffectSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public TroopEffectSpec FindTroopEffectSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIndex;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIndex))
		{
			if (mTroopEffectSpecs[lRowIndex] == null)
				mTroopEffectSpecs[lRowIndex] = _LoadTroopEffectSpec(mCSVObject.GetRow(lRowIndex), lRowIndex);
			return mTroopEffectSpecs[lRowIndex];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName);
			return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("TroopEffectTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private TroopEffectSpec _LoadTroopEffectSpec(CSVRow pCSVRow, int pRowIndex)
	{
		TroopEffectSpec lTroopEffectSpec = new TroopEffectSpec();

		// 줄 인덱스
		lTroopEffectSpec.mIndex = pRowIndex;

		// Name
		lTroopEffectSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lTroopEffectSpec.mName);

		// NameText
		lTroopEffectSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// StatTableModifier<TroopStat>
		lTroopEffectSpec.mTroopStatTableModifier = new StatTableModifier<TroopStat>();

		StatTableModifier<TroopStat> lTroopStatTableModifier = lTroopEffectSpec.mTroopStatTableModifier;
		{
			int lModifyIntValue;
			bool lModifyBoolValue;

			// T_MaxRotationDelayMultiplier
			if (pCSVRow.TryRatioValue(vMaxRotationDelayRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MaxRotationDelay, StatModifyType.Ratio, lModifyIntValue);

			// T_AdvanceSpeedMultiplier
			if (pCSVRow.TryRatioValue(vAdvanceSpeedRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.AdvanceSpeed, StatModifyType.Ratio, lModifyIntValue);

			// T_ArmorMultiplier
			if (pCSVRow.TryRatioValue(vArmorRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
			{
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.FrontArmor, StatModifyType.Ratio, lModifyIntValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SideArmor, StatModifyType.Ratio, lModifyIntValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.RearArmor, StatModifyType.Ratio, lModifyIntValue);
			}

			// T_SightRangeMultiplier
			if (pCSVRow.TryRatioValue(vSightRangeRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SightRange, StatModifyType.Ratio, lModifyIntValue);

			// T_BaseHpMultiplier
			if (pCSVRow.TryRatioValue(vBaseHpRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.BaseMilliHp, StatModifyType.Ratio, lModifyIntValue);

			// T_SetUpDelayAdd
			if (pCSVRow.TryIntValue(vSetUpDelayAddColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SetUpDelay, StatModifyType.Add, lModifyIntValue);

			// C_HitRateAdd
			if (pCSVRow.TryMilliIntValue(vHitRateAddColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.HitRateMilliAdd, StatModifyType.Add, lModifyIntValue);

			// IsAttackDisabledOverwrite
			if (pCSVRow.TryBoolValue(vAttackDisabledOverwriteColumnName, out lModifyBoolValue, false)) // 기본값 false(의미 없음)
				lTroopStatTableModifier.AddModifyBoolStat(TroopStat.IsAttackDisabled, StatModifyType.Overwrite, lModifyBoolValue);

			// IsHidingOverwrite
			if (pCSVRow.TryBoolValue(vHidingOverwriteColumnName, out lModifyBoolValue, false)) // 기본값 false(의미 없음)
				lTroopStatTableModifier.AddModifyBoolStat(TroopStat.IsHiding, StatModifyType.Overwrite, lModifyBoolValue);

			// IsFollowAdvanceOverwrite
			if (pCSVRow.TryBoolValue(vAdvanceTypeOverwriteColumnName, out lModifyBoolValue, false)) // 기본값 false(의미 없음)
				lTroopStatTableModifier.AddModifyBoolStat(TroopStat.IsFollowAdvance, StatModifyType.Overwrite, lModifyBoolValue);

			// MaxMilliMoraleRatio
			if (pCSVRow.TryRatioValue(vMaxMilliMoraleRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MaxMilliMorale, StatModifyType.Ratio, lModifyIntValue);
		}

		// StatTableModifier<WeaponStat>
		lTroopEffectSpec.mWeaponStatTableModifiers = new StatTableModifier<WeaponStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopEffectSpec.mWeaponStatTableModifiers[iWeapon] = new StatTableModifier<WeaponStat>();

		StatTableModifier<WeaponStat> lPrimaryWeaponStatTableModifier = lTroopEffectSpec.mWeaponStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponStat> lSecondaryWeaponStatTableModifier = lTroopEffectSpec.mWeaponStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyIntValue;

			// PW_MaxTrackingDelayMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponMaxTrackingDelayRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyIntValue);
			//SW_MaxTrackingDelayMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponMaxTrackingDelayRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyIntValue);

			// PW_ScatterAngleMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponScatterAngleRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterAngleHalf, StatModifyType.Ratio, lModifyIntValue);
			// SW_ScatterAngleMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponScatterAngleRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterAngleHalf, StatModifyType.Ratio, lModifyIntValue);
		}

		// StatTableModifier<WeaponRangeStat>
		lTroopEffectSpec.mWeaponRangeStatTableModifiers = new StatTableModifier<WeaponRangeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopEffectSpec.mWeaponRangeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponRangeStat>();

		StatTableModifier<WeaponRangeStat> lPrimaryWeaponRangeStatTableModifier = lTroopEffectSpec.mWeaponRangeStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponRangeStat> lSecondaryWeaponRangeStatTableModifier = lTroopEffectSpec.mWeaponRangeStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyIntValue;

			// PW_DistanceMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponRangeRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Range, StatModifyType.Ratio, lModifyIntValue);
			// SW_DistanceMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponRangeRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Range, StatModifyType.Ratio, lModifyIntValue);

			// PW_AccuracyMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAccuracyRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyIntValue);
			// SW_AccuracyMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAccuracyRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyIntValue);

			// PrimaryWeaponAttackDelayRatio
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAttackDelayRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.AttackDelayMultiplyRatio, StatModifyType.Ratio, lModifyIntValue);
			// SecondaryWeaponAttackDelayRatio
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAttackDelayRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.AttackDelayMultiplyRatio, StatModifyType.Ratio, lModifyIntValue);

			// PW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponSuppressionPointRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.RangeSuppressionMilliPoint, StatModifyType.Ratio, lModifyIntValue);
			// SW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponSuppressionPointRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.RangeSuppressionMilliPoint, StatModifyType.Ratio, lModifyIntValue);

			// PW_AttackDamageMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAttackDamageRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliDamage, StatModifyType.Ratio, lModifyIntValue);
			// SW_AttackDamageMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAttackDamageRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliDamage, StatModifyType.Ratio, lModifyIntValue);

			// PW_PenetrationMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponPenetrationRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyIntValue);
			// SW_PenetrationMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponPenetrationRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyIntValue);
		}

		// StatTableModifier<WeaponAoeStat>
		lTroopEffectSpec.mWeaponAoeStatTableModifiers = new StatTableModifier<WeaponAoeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopEffectSpec.mWeaponAoeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponAoeStat>();

		StatTableModifier<WeaponAoeStat> lPrimaryWeaponAoeStatTableModifier = lTroopEffectSpec.mWeaponAoeStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponAoeStat> lSecondaryWeaponAoeStatTableModifier = lTroopEffectSpec.mWeaponAoeStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyIntValue;

			// PW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponSuppressionPointRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.AoeSuppressionMilliPoint, StatModifyType.Ratio, lModifyIntValue);
			// SW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponSuppressionPointRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.AoeSuppressionMilliPoint, StatModifyType.Ratio, lModifyIntValue);

			// PW_AoeDistanceMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAoeDistanceRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.Radius, StatModifyType.Ratio, lModifyIntValue);
			// SW_AoeDistanceMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAoeDistanceRatioColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.Radius, StatModifyType.Ratio, lModifyIntValue);

			// PW_AoePenetrationMultiplierAdd
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAoePenetrationMultiplyRatioAddColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.PenetrationMultiplyRatio, StatModifyType.Add, lModifyIntValue);
			// SW_AoePenetrationMultiplierAdd
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAoePenetrationMultiplyRatioAddColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.PenetrationMultiplyRatio, StatModifyType.Add, lModifyIntValue);
		}

		// 파생 정보를 초기화합니다.
// 		_InitExtraInfo(lTroopEffectSpec);

		return lTroopEffectSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
// 	private void _InitExtraInfo(TroopEffectSpec pTroopEffectSpec)
// 	{
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static TroopEffectTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private TroopEffectSpec[] mTroopEffectSpecs;
}
