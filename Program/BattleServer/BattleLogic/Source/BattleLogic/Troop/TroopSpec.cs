using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum TroopType
{
	Infantry,
	Armored,
	Artillery,
}

public enum TroopBody
{
	Soldier,
	Vehicle,
}

public enum TroopDefenseType
{
	Helmet, // 보병 보호구
	Armor, // 차량 장갑
	Bunker, // 천정까지 덮는 밀실 방어로 직사 화기와 곡사 화기를 모두 막음
	Trench, // 천정은 없고 땅 밑으로 들어가서 직사 화기를 확실히 막음
	Barrier, // 천정은 없고 직사 화기를 막음
}

public enum TroopHitResult
{
	GroundHit, // 지면에 착탄
	Penetrate, // 장갑을 뚫고 관통
	Block, // 명중했으나 튕겨나감
	Miss, // 아무런 영향을 주지 않는 유탄

	None,
}

public enum TroopHpChangeType
{
	Reset, // 초기화
	DirectDamage, // 직격에 의한 데미지
	RangeDamage, // 주변 폭발에 의한 간접 데미지
	Recover, // 회복이나 수리
}

public enum TroopMoraleState
{
	Good,
	Suppressed,
	PinDown,
}

public enum TroopInteractionType
{
	None,
	Recovery,
}

public enum TroopActionType
{
	None,
	SelfRecovery, // 자가 치료
}

public enum TroopStat
{
	IsAttackDisabled,
	IsHiding,
	IsFollowAdvance,

	SightRange,
	SpawnMp,
	SpawnCoolTime,
	BaseMilliHp,
	ArmorSize,
	FrontArmor,
	SideArmor,
	RearArmor,
	AdvanceSpeed,
	MaxRotationDelay,
	MoveStartDelay,
	SetUpDelay,

	MaxMilliMorale,
	MoraleMilliRecovery,
	SuppressedMilliActivate,
	SuppressedMilliRecover,
	PinDownMilliActivate,
	PinDownMilliRecover,
	SuppressedSpeedMultiplyRatio,
	CooldownDelayMultiplyRatio,
	AccuracyMultiplyRatio,
	NonBattleMultiplyRatio,
	NonBattleDelay,
	MoveOvercomeLevel,
	AttackOvercomeLevel,
	Experience,

	HitRateMilliAdd,
}

public class MetaTroopStatTable : StatTable<TroopStat>
{
	public int Get(TroopStat pStat)
	{
		return mStats[(int)pStat];
	}
	public void SetUp(MetaTroopStatTable pTroopStatTable, TroopLevelSpec pTroopLevelSpec, int pTroopLevel, VeterancySpec pVeterancySpec, CommanderTroopSet pCommanderTroopSet)
	{
		Copy(pTroopStatTable);

		pTroopLevelSpec.mTroopStatTableModifier.ModifyLevelStatTable(this, pTroopLevel);

		if (pVeterancySpec != null)
			pVeterancySpec.mTroopStatTableModifier.ModifyStatTable(this);

		mStats[(int)TroopStat.IsFollowAdvance] = pCommanderTroopSet.mIsFollowAdvance ? 1 : 0;
		mStats[(int)TroopStat.MaxMilliMorale] = (int)((Int64)mStats[(int)TroopStat.MaxMilliMorale] * pCommanderTroopSet.mMaxMoraleMultiplyRatio / 10000); // 만분율
	}
}

public class TroopStatTable : MetaTroopStatTable
{
	public int aSightRange { get { return mSightRange; } }
	public int aSqrSightRange { get { return mSqrSightRange; } }
	public int aHidingDetectionRange { get { return mHidingDetectionRange; } }
	public int aSqrHidingDetectionRange { get { return mSqrHidingDetectionRange; } }

	public TroopStatTable()
	{
		SetStatChangeDelegate(TroopStat.SightRange, _OnStatChange_SightRange);
	#if UNITY_EDITOR
		SetStatChangeDelegate(TroopStat.CooldownDelayMultiplyRatio, _OnStatChange_CooldownDelayMultiplyRatio); // 에러 검증 코드
	#endif
	}
	public bool aIsAttackDisabled
	{
		get { return (mStats[(int)TroopStat.IsAttackDisabled] > 0); }
	}
	public bool aIsHiding
	{
		get { return (mStats[(int)TroopStat.IsHiding] > 0); }
	}
	public bool aIsFollowAdvance
	{
		get { return (mStats[(int)TroopStat.IsFollowAdvance] > 0); }
	}

	private void _OnStatChange_SightRange(int pStatIdx)
	{
		mSightRange = mStats[pStatIdx];
		mSqrSightRange = mSightRange * mSightRange;
		mHidingDetectionRange = (int)((Int64)mSightRange * BattleConfig.get.mHidingCheckRangeRatio / 10000); // 만분율
		mSqrHidingDetectionRange = mHidingDetectionRange * mHidingDetectionRange;
	}
#if UNITY_EDITOR
	private void _OnStatChange_CooldownDelayMultiplyRatio(int pStatIdx)
	{
		if (mStats[pStatIdx] == 0)
			Debug.LogError("CooldownDelayMultiplyRatio is zero - " + this);
	}
#endif

#if BATTLE_VERIFY_TEST
    public void Test_ModifyStat(TroopStat stat, int value)
    {
        Set(stat, value);
        //mStats[(int)stat] = value;
    }
#endif

	private int mSightRange;
	private int mSqrSightRange;
	private int mHidingDetectionRange;
	private int mSqrHidingDetectionRange;
}

public class TroopSpec
{
	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameText;
	public String mNameKey;
	public String mDescText;
	public String mEffectiveText;
	public AssetKey mButtonIconAssetKey;
	public AssetKey mFieldIconAssetKey;
	public AssetKey mPortraitIconAssetKey;
	public String mStageTroopAssetKeyName;
	public AssetKey mStageTargetingDummyAssetKey;

	public TroopType mType;
	public TroopBody mBody;
	public int mUnitRadius;
	public int mHitSize;
	public WeaponSpec[] mWeaponSpecs;
	public bool[] mIsBodyWeapons;
	public bool[] mIsTurretWeapons;
	public int mMainBodyWeaponIdx;
	public int mMainTurretWeaponIdx;
	public TroopDefenseType mDefaultDefenseType;
	public int mUnitCount;
	public bool mIsCaptureType;

	public TroopStatTable mStatTable;

	public TroopLevelSpec[] mRankLevelSpecs;
	public VeterancySpec[] mVeterancySpecs;

	// 파생 정보

	public const int cTroopTypeCount = 3;
	public const int cTroopBodyCount = 2;
	public const int cTroopMoraleStateCount = 3;
	public const int cTroopInteractionTypeCount = 2;
	public const int cTroopActionTypeCount = 2;
	public const int cControlTeamCount = 2;
	public const int cPrimaryWeaponIdx = 0;
	public const int cSecondaryWeaponIdx = 1;
	public const int cVeteranWeaponIdx = 2;
	public const int cWeaponCount = 3;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<TroopSpec[{0}] No={1} AttackSpec={2}/{3}/{4} Name={5}>", mIdx, mNo, mWeaponSpecs[cPrimaryWeaponIdx], mWeaponSpecs[cSecondaryWeaponIdx], mWeaponSpecs[cVeteranWeaponIdx], mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(TroopSpec pTroopSpec)
	{
		if ((pTroopSpec != null) &&
			(pTroopSpec.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 버튼 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetButtonIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mButtonIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 부대 필드 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetFieldIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mFieldIconAssetKey);
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
