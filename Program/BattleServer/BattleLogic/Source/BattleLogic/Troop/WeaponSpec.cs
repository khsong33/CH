using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum WeaponAttackType
{
	None,
	Rifle, // 소총 공격
	Grenade, // 수류탄 공격
	MachineGun, // 기관총 공격
	TankGun, // 전차포
	HighAngleGun, // 곡사포
	MedKit, // 응급 치료
	RepairKit, // 차량 수리
}

public enum WeaponTargeting
{
	All,
	Soldier,
	Vehicle,
}

public enum WeaponAttackRange
{
	Near = 0,
	Middle,
	Far,
	Count
}

public enum WeaponStat
{
	MaxTrackingDelay,
	WindupDelay,
	UseDelay,
	BulletVelocity,
	MinRange,
	ScatterAngleHalf,
	ScatterOffset,
	FireConeAngleHalf,
	TrackingAngleHalf,
	SuppressionRange,
	SuppressionMultiplyRatio,

	MoveHoldLevel,
	MoveHoldDelay,
	AttackHoldLevel,
	AttackHoldDelay,
}

public enum WeaponRangeStat
{
	Range,
	AttackDelayMultiplyRatio,
	AimDelay,
	CooldownDelay,
	MilliAccuracy,
	Penetration,
	MilliDamage,
	RangeSuppressionMilliPoint,
}

public enum WeaponAoeStat
{
	Radius,
	DamageMultiplyRatio,
	AoeSuppressionMilliPoint,
	PenetrationMultiplyRatio,
}

public class MetaWeaponStatTable : StatTable<WeaponStat>
{
	public int 
	Get(WeaponStat pStat)
	{
		return mStats[(int)pStat];
	}

	public void SetUp(MetaWeaponStatTable pWeaponStatTable, int pWeaponIdx, TroopLevelSpec pTroopLevelSpec, int pTroopLevel, VeterancySpec pVeterancySpec)
	{
		Copy(pWeaponStatTable);

		pTroopLevelSpec.mWeaponStatTableModifiers[pWeaponIdx].ModifyLevelStatTable(this, pTroopLevel);

		if ((pVeterancySpec != null) &&
			(pWeaponIdx != TroopSpec.cVeteranWeaponIdx)) // 베테랑 무기는 영향 없음
			pVeterancySpec.mWeaponStatTableModifiers[pWeaponIdx].ModifyStatTable(this);
	}
}

public class WeaponStatTable : MetaWeaponStatTable
{
	public int aMinRange { get { return mMinRange; } }
	public int aSqrMinRange { get { return mSqrMinRange; } }

	public WeaponStatTable()
	{
		SetStatChangeDelegate(WeaponStat.MinRange, _OnStatChange_MinRange);
	}

	private void _OnStatChange_MinRange(int pStatIdx)
	{
		mMinRange = mStats[pStatIdx];
		mSqrMinRange = mMinRange * mMinRange;
	}

	private int mMinRange;
	private int mSqrMinRange;
}

public class MetaWeaponRangeStatTable : StatTable<WeaponRangeStat>
{
	public int Get(WeaponRangeStat pStat)
	{
		return mStats[(int)pStat];
	}

	public void SetUp(MetaWeaponRangeStatTable pWeaponRangeStatTable, int pWeaponIdx, TroopLevelSpec pTroopLevelSpec, int pTroopLevel, VeterancySpec pVeterancySpec)
	{
		Copy(pWeaponRangeStatTable);

		pTroopLevelSpec.mWeaponRangeStatTableModifiers[pWeaponIdx].ModifyLevelStatTable(this, pTroopLevel);

		if ((pVeterancySpec != null) &&
			(pWeaponIdx != TroopSpec.cVeteranWeaponIdx)) // 베테랑 무기는 영향 없음
			pVeterancySpec.mWeaponRangeStatTableModifiers[pWeaponIdx].ModifyStatTable(this);
	}
}

public class WeaponRangeStatTable : MetaWeaponRangeStatTable
{
	public const int cMinCooldownDelay = 100;

	public int aRange { get { return mRange; } }
	public int aSqrRange { get { return mSqrRange; } }

	public WeaponRangeStatTable()
	{
		SetStatChangeDelegate(WeaponRangeStat.Range, _OnStatChange_Range);
	#if UNITY_EDITOR
		SetStatChangeDelegate(WeaponRangeStat.CooldownDelay, _OnStatChange_CooldownDelay); // 에러 검증 코드
	#endif
	}

	private void _OnStatChange_Range(int pStatIdx)
	{
		mRange = mStats[pStatIdx];
		mSqrRange = mRange * mRange;
	}
#if UNITY_EDITOR
	private void _OnStatChange_CooldownDelay(int pStatIdx)
	{
		if (mStats[pStatIdx] < WeaponRangeStatTable.cMinCooldownDelay)
		{
			Debug.LogError("too small CooldownDelay:" + mStats[pStatIdx] + " - " + this);
			mStats[pStatIdx] = WeaponRangeStatTable.cMinCooldownDelay;
		}
	}
#endif

	private int mRange;
	private int mSqrRange;
}

public class MetaWeaponAoeStatTable : StatTable<WeaponAoeStat>
{
	public int Get(WeaponAoeStat pStat)
	{
		return mStats[(int)pStat];
	}
	public void SetUp(MetaWeaponAoeStatTable pWeaponAoeStatTable, int pWeaponIdx, TroopLevelSpec pTroopLevelSpec, int pTroopLevel, VeterancySpec pVeterancySpec)
	{
		Copy(pWeaponAoeStatTable);

		pTroopLevelSpec.mWeaponAoeStatTableModifiers[pWeaponIdx].ModifyLevelStatTable(this, pTroopLevel);

		if ((pVeterancySpec != null) &&
			(pWeaponIdx != TroopSpec.cVeteranWeaponIdx)) // 베테랑 무기는 영향 없음
			pVeterancySpec.mWeaponAoeStatTableModifiers[pWeaponIdx].ModifyStatTable(this);
	}
}

public class WeaponAoeStatTable : MetaWeaponAoeStatTable
{
	public int aRadius { get { return mRadius; } }
	public int aSqrRadius { get { return mSqrRadius; } }

	public WeaponAoeStatTable()
	{
		SetStatChangeDelegate(WeaponAoeStat.Radius, _OnStatChange_Radius);
	}

	private void _OnStatChange_Radius(int pStatIdx)
	{
		mRadius = mStats[pStatIdx];
		mSqrRadius = mRadius * mRadius;
	}

	private int mRadius;
	private int mSqrRadius;
}

public class WeaponSpec
{
	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameText;
	public String mNameKey;

	public WeaponAttackType mAttackType;
//	public bool mIsSetUpWeapon;
	public WeaponStatTable mStatTable;
	public WeaponRangeStatTable[] mRangeStatTables;
	public WeaponAoeStatTable[] mAoeStatTables;

	// 파생 정보
	public WeaponTargeting mTargeting;
	public bool mIsGroundAttack;
	public bool mIsAllyTargeting;
	public TroopInteractionType mTroopInteractionType;

	public const int cRangeLevelCount = 3;
	public const int cMaxRangeLevel = cRangeLevelCount - 1;
	public const int cAoeLevelCount = 3;
	public const int cMaxAoeLevel = cAoeLevelCount - 1;
	public const int cAttackTypeEnumCount = 8;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<WeaponSpec[{0}] Id={1} Name={2}>", mIdx, mNo, mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(WeaponSpec pWeaponSpec)
	{
		if ((pWeaponSpec != null) &&
			(pWeaponSpec.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
