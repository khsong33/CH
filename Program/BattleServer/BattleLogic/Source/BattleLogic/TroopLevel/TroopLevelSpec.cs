﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TroopLevelSpec
{
	public int mIdx;

	public String mName;

	public StatTableModifier<TroopStat> mTroopStatTableModifier;
	public StatTableModifier<WeaponStat>[] mWeaponStatTableModifiers;
	public StatTableModifier<WeaponRangeStat>[] mWeaponRangeStatTableModifiers;
	public StatTableModifier<WeaponAoeStat>[] mWeaponAoeStatTableModifiers;

	public const int cTroopMaxLevel = 12;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<TroopLevelSpec[{0}] Name={1}>", mIdx, mName);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
