﻿using UnityEngine;
using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class CSVObject : IEnumerable<CSVRow>
{
    private string[,] _grid;    // [column, row]. 얘는 헤더는 없고 데이터만 가지고 있음.
    private CSVRow[] _rowSet;
    private int _dataStartRow;
	private string[] header;
    private Dictionary<string, int> _headerIndex;


    public CSVObject()
    {
        _headerIndex = new Dictionary<string, int>();        
    }
	public bool LoadCSV(string csvText, int headerRow)
    {
        // 행, 열 모두 0부터 시작한다. 즉 csv의 첫번째 줄이 헤더라면, headerRow = 0 이다.
        // 1. headerRow < 0 이면, 헤더가 없는 것으로 간주하고 1행부터 데이터로 처리한다.
        // 2. headerRow >= 0 이면, 헤더를 따로 보관한다 (필드이름-인덱스 쌍의 dictionary)
        // 3. 데이터를 string[,]으로 저장한다.
        
        // 헤더 있으면, 헤더와 데이터를,
        // 헤더 없으면 데이터만 뽑아냄.
        header = null;
        if (headerRow >= 0)
        {
            _grid = CSVReader.SplitCsvGrid(csvText, headerRow, out header);
            // 헤더-인덱스 dictionary 만들기.
            _dataStartRow = headerRow + 1;
            for (int col = 0; col < this.ColNum; col++)
            {
                try
                {
                    _headerIndex[header[col]] = col;
                }
                catch
                {
                    Debug.Log("CSVObject.cs : header error");
                }
            }

            // Enumerator를 사용할 수 있도록 데이터셋을 따로 만들어 둔다.
			int lRowCount = this.RowNum;
			_rowSet = new CSVRow[lRowCount];
			for (int row = 0; row < lRowCount; row++)
            {
                _rowSet[row] = new CSVRow();
                foreach ( KeyValuePair<string, int> field in _headerIndex)
                {
					_rowSet[row].aValueDictionary[field.Key] = _grid[field.Value, row];
                }
            }
        }
        else
        {
            _grid = CSVReader.SplitCsvGrid(csvText);
        }

        return true;
    }
    public void UnloadCSV()
    {
        // unload가 필요한지는 모르겠음...
        _grid = null;
        _headerIndex.Clear();
        _headerIndex = null;
        _dataStartRow = 0;
    }
    public bool HaveHader
    {
        // 헤더를 가지고 있는지 여부를 반환하는 함수.
        get { return (_dataStartRow > 0);}
    }
    public int RowNum
    {
        // 헤더를 제외한 행(레코드) 개수. 데이터의 개수만 반환.
        get { return _grid.GetUpperBound(1); }
    }
    public int ColNum
    {
        // 필드 개수.
        get { return _grid.GetUpperBound(0); }
    }
	public string GetHeader(int column)
	{
		return header[column];
	}
	public CSVRow GetRow(int row)
	{
		return _rowSet[row];
	}
    public string Get(int row, string fieldname)
    {
        // 행, 필드 이름 지정해서 데이터 반환.
        return _grid[_headerIndex[fieldname], row];
    }
    public string Get(int row, int column)
    {
        // 행, 열 지정해서 데이터 반환.
        return _grid[column, row];
    }
    public string[,] DataSet
    {
        // 데이터 묶음 반환.
        get { return _grid; }
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }
    public IEnumerator<CSVRow> GetEnumerator()
    {
        return new CSVEnumerator(_rowSet);
    }
}
