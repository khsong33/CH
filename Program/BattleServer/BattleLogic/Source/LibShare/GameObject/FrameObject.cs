using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class FrameObject : IFrameObject
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IGuidObject 속성
	public uint aGuid
	{
		get { return mGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameObject 속성
	public int aUpdateFrameSliceIdx
	{
		get { return mUpdateFrameSliceIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameObject 속성
	public int aTaskCount
	{
		get { return mTaskCount; }
		set { mTaskCount = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신자
	public FrameUpdater aFrameUpdater
	{
		get { return mFrameUpdater; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이 오브젝트를 파괴합니다.
	public void Destroy()
	{
		OnDestroy();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateObject(FrameUpdater pFrameUpdater)
	{
		mGuid = pFrameUpdater.GenerateObjectGuid();

		mFrameUpdater = pFrameUpdater;
		mUpdateFrameSliceIdx = pFrameUpdater.GenerateFrameSliceIdx();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public virtual void OnDestroy()
	{
		mFrameUpdater.DeleteFrameSliceIdx(mUpdateFrameSliceIdx);
		mUpdateFrameSliceIdx = -1;
		mFrameUpdater = null;

		mGuid = 0;

		// 연결된 태스크를 취소합니다.
		if (mOnFrameUpdateTask != null)
		{
			mOnFrameUpdateTask.Cancel();
			mOnFrameUpdateTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IGuidObject 메서드
	public void ResetGuid()
	{
		mGuid = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTask] 갱신 콜백
	public virtual void OnFrameUpdate(FrameTask pTask, int pFrameDelta)
	{
		Debug.LogWarning("suspected idle task - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 리스트에 넣습니다.
	public void RepeatUpdate()
	{
		if (mOnFrameUpdateTask != null)
			mOnFrameUpdateTask.Cancel();
		mOnFrameUpdateTask = mFrameUpdater.RepeatTask(this, OnFrameUpdate, FrameUpdateLayer.Layer0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템을 갱신 리스트에서 뺍니다.
	public void StopUpdate()
	{
		if (mOnFrameUpdateTask == null)
			return;

		mOnFrameUpdateTask.Cancel();
		mOnFrameUpdateTask = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 태스크를 예약합니다.
	public FrameTask ReserveTask(FrameTaskDelegate pTaskDelegate, int pReservationDelay)
	{
		return mFrameUpdater.ReserveTask(this, pTaskDelegate, pReservationDelay);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private uint mGuid = 0;

	private FrameTask mOnFrameUpdateTask;
	private FrameUpdater mFrameUpdater;
	private int mUpdateFrameSliceIdx;
	private int mTaskCount;
}
