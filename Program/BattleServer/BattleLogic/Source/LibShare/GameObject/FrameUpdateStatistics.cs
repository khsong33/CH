using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FrameUpdaterStatistics
{
	public const int cHistoryLength = 100;

	public class UpdateInfo
	{
		public int mUpdateId;
		public int mItemUpdateCount;
		public int mRepeatTaskUpdateCount;
		public int mReservationTaskUpdateCount;

		internal void Reset(int pUpdateId)
		{
			mUpdateId = pUpdateId;
			mItemUpdateCount = 0;
			mRepeatTaskUpdateCount = 0;
			mReservationTaskUpdateCount = 0;
		}

		public override string ToString()
		{
			return String.Format("UpdateInfo[{0}](ItemUpdateCount:{1} RepeatTaskUpdateCount:{2}, ReservationTaskUpdateCount:{3})", mUpdateId, mItemUpdateCount, mRepeatTaskUpdateCount, mReservationTaskUpdateCount);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 갱신 정보 기록
	public UpdateInfo[] aUpdateInfoHistory
	{
		get { return mUpdateInfoHistory; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 기록 커서
	public int aHistoryIdx
	{
		get { return mHistoryIdx; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public FrameUpdaterStatistics()
	{
		mUpdateInfoHistory = new UpdateInfo[cHistoryLength];
		for (int iInfo = 0; iInfo < cHistoryLength; ++iInfo)
			mUpdateInfoHistory[iInfo] = new UpdateInfo();

		Reset();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화합니다.
	public void Reset()
	{
		mLastUdpateId = -1; // UpdateCountHistroy()에서 0부터 시작
		mHistoryIdx = -1; // UpdateCountHistroy()에서 0부터 시작
		UpdateHistroy();
	}
	//-------------------------------------------------------------------------------------------------------
	// 호출 회수를 증가시킵니다.
	public void IncrementItemUpdateCount()
	{
		++mUpdateInfoHistory[mHistoryIdx].mItemUpdateCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 호출 회수를 증가시킵니다.
	public void IncrementRepeatTaskUpdateCount()
	{
		++mUpdateInfoHistory[mHistoryIdx].mRepeatTaskUpdateCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 호출 회수를 증가시킵니다.
	public void IncrementReservationTaskUpdateCount()
	{
		++mUpdateInfoHistory[mHistoryIdx].mReservationTaskUpdateCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 기록을 갱신합니다.
	public void UpdateHistroy()
	{
		++mLastUdpateId;
		mHistoryIdx = (mHistoryIdx + 1) % cHistoryLength;
		mUpdateInfoHistory[mHistoryIdx].Reset(mLastUdpateId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 기록을 남깁니다.
	public void LogHistory()
	{
		int lItemUpdateCountSum = 0;
		int lItemUpdateCountMin = int.MaxValue;
		int lItemUpdateCountMax = int.MinValue;

		int lRepeatTaskUpdateCountSum = 0;
		int lRepeatTaskUpdateCountMin = int.MaxValue;
		int lRepeatTaskUpdateCountMax = int.MinValue;

		int lReservationTaskUpdateCountSum = 0;
		int lReservationTaskUpdateCountMin = int.MaxValue;
		int lReservationTaskUpdateCountMax = int.MinValue;
		int lUpdateInfoCount = 0;

		int lStartUpdateId = mLastUdpateId - cHistoryLength + 1;
		if (lStartUpdateId < 0)
			lStartUpdateId = 0;
		for (int iUpdate = lStartUpdateId; iUpdate < mLastUdpateId; ++iUpdate)
		{
			int lUpdateInfoIdx = iUpdate % cHistoryLength;
			UpdateInfo lUpdateInfo = mUpdateInfoHistory[lUpdateInfoIdx];
			Debug.Log(lUpdateInfo.ToString());

			lItemUpdateCountSum += lUpdateInfo.mItemUpdateCount;
			if (lItemUpdateCountMin > lUpdateInfo.mItemUpdateCount)
				lItemUpdateCountMin = lUpdateInfo.mItemUpdateCount;
			if (lItemUpdateCountMax < lUpdateInfo.mItemUpdateCount)
				lItemUpdateCountMax = lUpdateInfo.mItemUpdateCount;

			lRepeatTaskUpdateCountSum += lUpdateInfo.mRepeatTaskUpdateCount;
			if (lRepeatTaskUpdateCountMin > lUpdateInfo.mRepeatTaskUpdateCount)
				lRepeatTaskUpdateCountMin = lUpdateInfo.mRepeatTaskUpdateCount;
			if (lRepeatTaskUpdateCountMax < lUpdateInfo.mRepeatTaskUpdateCount)
				lRepeatTaskUpdateCountMax = lUpdateInfo.mRepeatTaskUpdateCount;

			lReservationTaskUpdateCountSum += lUpdateInfo.mReservationTaskUpdateCount;
			if (lReservationTaskUpdateCountMin > lUpdateInfo.mReservationTaskUpdateCount)
				lReservationTaskUpdateCountMin = lUpdateInfo.mReservationTaskUpdateCount;
			if (lReservationTaskUpdateCountMax < lUpdateInfo.mReservationTaskUpdateCount)
				lReservationTaskUpdateCountMax = lUpdateInfo.mReservationTaskUpdateCount;

			++lUpdateInfoCount;
		}

		if (lUpdateInfoCount > 0)
			Debug.Log(String.Format("average ItemUpdateCount:{0:F1}[{1}-{2}] RepeatTaskUpdateCount:{3:F1}[{4}-{5}] ReservationTaskUpdateCount:{6:F1}[{7}-{8}]",
				(float)lItemUpdateCountSum / lUpdateInfoCount,
				lItemUpdateCountMin,
				lItemUpdateCountMax,
				(float)lRepeatTaskUpdateCountSum / lUpdateInfoCount,
				lRepeatTaskUpdateCountMin,
				lRepeatTaskUpdateCountMax,
				(float)lReservationTaskUpdateCountSum / lUpdateInfoCount,
				lReservationTaskUpdateCountMin,
				lReservationTaskUpdateCountMax));
		else
			Debug.Log("no UpdateInfo");
	}
	//-------------------------------------------------------------------------------------------------------
	// 최근 기록을 남깁니다.
	public void LogLastHistory()
	{
		int lLastHistoryIdx = (mHistoryIdx - 1 + cHistoryLength) % cHistoryLength;
		Debug.Log(mUpdateInfoHistory[lLastHistoryIdx].ToString());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UpdateInfo[] mUpdateInfoHistory;
	private int mLastUdpateId;
	private int mHistoryIdx;
}
