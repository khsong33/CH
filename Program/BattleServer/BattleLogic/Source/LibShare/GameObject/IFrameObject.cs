using UnityEngine;
using System.Collections;
using System;

public interface IFrameObject : IGuidObject
{
	// 오브젝트에 할당된 갱신 프레임 조각 인덱스
	int aUpdateFrameSliceIdx { get; }

	// 이 오브젝트가 발생시킨 태스크 개수
	int aTaskCount { get; set; }
}
