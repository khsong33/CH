using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class FileLogger : ILogger, IDisposable
{
	public static readonly String cLogTag = "[Log] ";
	public static readonly String cLogWarningTag = "[LogWarning] ";
	public static readonly String cLogErrorTag = "[LogError] ";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public FileLogger(String pFilePath)
	{
		mFileStream = new FileStream(pFilePath, FileMode.Create);
		mStreamWriter = new StreamWriter(mFileStream);
	}
	//-------------------------------------------------------------------------------------------------------
	// IDisposable 메서드
	public void Dispose()
	{
		mStreamWriter.Close();
		mFileStream.Close();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void Log(String pLogMessage)
	{
	#if UNITY_EDITOR
		Debug.Log(pLogMessage);
	#endif

		mStreamWriter.WriteLine(cLogTag + pLogMessage);
		mStreamWriter.Flush();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogFormat(String pLogMessage, params object[] args)
	{
	#if UNITY_EDITOR
		Debug.LogFormat(pLogMessage, args);
	#endif

		mStreamWriter.WriteLine(cLogTag + String.Format(pLogMessage, args));
		mStreamWriter.Flush();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogWarning(String pLogMessage)
	{
	#if UNITY_EDITOR
		Debug.LogWarning(pLogMessage);
	#endif

		mStreamWriter.WriteLine(cLogWarningTag + pLogMessage);
		mStreamWriter.Flush();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogWarningFormat(String pLogMessage, params object[] args)
	{
	#if UNITY_EDITOR
		Debug.LogWarningFormat(pLogMessage, args);
	#endif

		mStreamWriter.WriteLine(cLogWarningTag + String.Format(pLogMessage, args));
		mStreamWriter.Flush();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogError(String pLogMessage)
	{
	#if UNITY_EDITOR
		Debug.LogError(pLogMessage);
	#endif

		mStreamWriter.WriteLine(cLogErrorTag + pLogMessage);
		mStreamWriter.Flush();
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogErrorFormat(String pLogMessage, params object[] args)
	{
	#if UNITY_EDITOR
		Debug.LogErrorFormat(pLogMessage, args);
	#endif

		mStreamWriter.WriteLine(cLogErrorTag + String.Format(pLogMessage, args));
		mStreamWriter.Flush();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private FileStream mFileStream;
	private StreamWriter mStreamWriter;
}
