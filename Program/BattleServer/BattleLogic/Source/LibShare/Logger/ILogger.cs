using UnityEngine;
using System.Collections;
using System;

public interface ILogger
{
	// 로그를 출력합니다.
	void Log(String pLogMessage);
	void LogFormat(string pLogMessage, params object[] args);

	void LogWarning(String pLogMessage);
	void LogWarningFormat(string pLogMessage, params object[] args);

	void LogError(String pLogMessage);
	void LogErrorFormat(string pLogMessage, params object[] args);
}
