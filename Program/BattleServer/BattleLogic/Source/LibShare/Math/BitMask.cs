using UnityEngine;
using System.Collections;
using System;

public class BitMask
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 비트를 1로 만듭니다.
	public static void SetBit(ref uint pBitMask, int pBitIndex)
	{
		pBitMask |= (uint)(0x00000001 << pBitIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 비트를 0으로 만듭니다.
	public static void ResetBit(ref uint pBitMask, int pBitIndex)
	{
		pBitMask &= ~(uint)(0x00000001 << pBitIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 비트가 1인지 검사합니다.
	public static bool CheckBit(uint pBitMask, int pBitIndex)
	{
		return ((pBitMask & (uint)(0x00000001 << pBitIndex)) != 0);
	}
}
