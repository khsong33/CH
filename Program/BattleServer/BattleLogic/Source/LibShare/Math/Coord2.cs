using UnityEngine;
using System.Collections;
using System.Text;
using System;

[Serializable]
public struct Coord2
{
	public const float cToCoord = 100.0f;
	public const float cToWorld = 0.01f;

	public int x;
	public int z;

	public Coord2(int pX, int pZ)
	{
		x = pX;
		z = pZ;
	}

	public static Coord2 operator -(Coord2 pA)
	{
		return new Coord2(-pA.x, -pA.z);
	}
	public static Coord2 operator -(Coord2 pA, Coord2 pB)
	{
		return new Coord2(pA.x - pB.x, pA.z - pB.z);
	}
	public static bool operator !=(Coord2 pLhs, Coord2 pRhs)
	{
		return ((pLhs.x != pRhs.x) || (pLhs.z != pRhs.z)) ? true : false;
	}
	public static Coord2 operator *(int pD, Coord2 pA)
	{
		return new Coord2(pD * pA.x, pD * pA.z);
	}
	public static Coord2 operator *(Coord2 pA, int pD)
	{
		return new Coord2(pA.x * pD, pA.z * pD);
	}
	public static Coord2 operator /(Coord2 pA, int pD)
	{
		return new Coord2(pA.x / pD, pA.z / pD);
	}
	public static Coord2 operator +(Coord2 pA, Coord2 pB)
	{
		return new Coord2(pA.x + pB.x, pA.z + pB.z);
	}
	public static bool operator ==(Coord2 pLhs, Coord2 pRhs)
	{
		return ((pLhs.x == pRhs.x) && (pLhs.z == pRhs.z)) ? true : false;
	}

	public override bool Equals(object other)
	{
		return other.GetHashCode() == GetHashCode();
	}
	public override int GetHashCode()
	{
		// bijective algorithm
		int tmp = (z + ((x + 1) / 2));
		return x + (tmp * tmp);
	}
	public static Coord2 Max(Coord2 pLhs, Coord2 pRhs)
	{
		return new Coord2(Mathf.Max(pLhs.x, pRhs.x), Mathf.Max(pLhs.z, pRhs.z));
	}
	public static Coord2 Min(Coord2 pLhs, Coord2 pRhs)
	{
		return new Coord2(Mathf.Min(pLhs.x, pRhs.x), Mathf.Min(pLhs.z, pRhs.z));
	}
	public static Coord2 FromVector2(Vector2 pVector2)
	{
		return new Coord2((int)pVector2.x, (int)pVector2.y);
	}
	public static Coord2 FromVectorXY(Vector3 pVector3)
	{
		return new Coord2((int)pVector3.x, (int)pVector3.y);
	}
	public static Coord2 FromVectorXZ(Vector3 pVector3)
	{
		return new Coord2((int)pVector3.x, (int)pVector3.z);
	}
	public static Vector2 ToVector2(Coord2 pCoord)
	{
		return new Vector2(pCoord.x, pCoord.z);
	}
	public static Vector3 ToVectorXY(Coord2 pCoord)
	{
		return new Vector3(pCoord.x, pCoord.z, 0.0f);
	}
	public static Vector3 ToVectorXZ(Coord2 pCoord)
	{
		return new Vector3(pCoord.x, 0.0f, pCoord.z);
	}
	public static Coord2 ToCoord(Vector3 pWorld)
	{
		return new Coord2((int)(pWorld.x * cToCoord), (int)(pWorld.z * cToCoord));
	}
	public static Vector3 ToWorld(Coord2 pCoord)
	{
		return new Vector3(pCoord.x * cToWorld, 0.0f, pCoord.z * cToWorld);
	}
	public static Vector3 ToWorld(Coord2 pCoord, float pWorldY)
	{
		return new Vector3(pCoord.x * cToWorld, pWorldY, pCoord.z * cToWorld);
	}
	public int SqrMagnitude()
	{
		return (x * x + z * z);
	}
	public int RoughMagnitude()
	{
		return MathUtil.RoughSqrt(x * x + z * z);
	}
	public int RoughDirectionY()
	{
// 		int lX, lZ;
// 
// 		lX = 0; lZ = 1000;
// 		int lDirectionZ = MathUtil.RoughAtan2(lX, lZ);
// 		Coord2 lVectorZ = RoughDirectionVector(lDirectionZ, 1000);
// 
// 		lX = 1000; lZ = 0;
// 		int lDirectionX = MathUtil.RoughAtan2(lX, lZ);
// 		Coord2 lVectorX = RoughDirectionVector(lDirectionX, 1000);
// 
// 		lX = 1000; lZ = 1000;
// 		int lDirectionXZ = MathUtil.RoughAtan2(lX, lZ);
// 		Coord2 lVectorXZ = RoughDirectionVector(lDirectionXZ, 1000);

		int lDirectionY = MathUtil.RoughAtan2(x, z);
		return lDirectionY;
	}
	public void RoughNormalize(int pSize)
	{
		int lDirectionY = MathUtil.RoughAtan2(x, z);
		x = pSize * MathUtil.MilliSin(lDirectionY) / 1000; // Sin 값이 milli임
		z = pSize * MathUtil.MilliCos(lDirectionY) / 1000; // Cos 값이 milli임
	}
	public static Coord2 RoughNormalize(Coord2 pVector, int pSize)
	{
		int lDirectionY = MathUtil.RoughAtan2(pVector.x, pVector.z);
		return new Coord2(
			pSize * MathUtil.MilliSin(lDirectionY) / 1000, // Sin 값이 milli임
			pSize * MathUtil.MilliCos(lDirectionY) / 1000); // Cos 값이 milli임
	}
	public static Coord2 RoughRotationVector(Coord2 pVector, int pRotationAngle)
	{
		return new Coord2(
			(pVector.z * MathUtil.MilliSin(pRotationAngle) + pVector.x * MathUtil.MilliCos(pRotationAngle)) / 1000, // Sin, Cos 값이 milli
			(pVector.z * MathUtil.MilliCos(pRotationAngle) - pVector.x * MathUtil.MilliSin(pRotationAngle)) / 1000); // Sin, Cos 값이 milli
	}
	public static Coord2 RoughDirectionVector(int pDirectionY, int pSize)
	{
		return new Coord2(
			pSize * MathUtil.MilliSin(pDirectionY) / 1000, // Sin 값이 milli임
			pSize * MathUtil.MilliCos(pDirectionY) / 1000); // Cos 값이 milli임
	}
	public static Coord2 RoughLerp(Coord2 pFrom, Coord2 pTo, int pFactor, int pBase)
	{
		return new Coord2(
			MathUtil.RoughLerp(pFrom.x, pTo.x, pFactor, pBase),
			MathUtil.RoughLerp(pFrom.z, pTo.z, pFactor, pBase));
	}
	public static Coord2 Lerp(Coord2 pFrom, Coord2 pTo, float t)
	{
		return new Coord2(
			(int)Mathf.Lerp(pFrom.x, pTo.x, t),
			(int)Mathf.Lerp(pFrom.z, pTo.z, t));
	}
	public override string ToString()
	{
		return String.Format("({0},{1})", x, z);
	}
	public string ToString(string pFormat)
	{
		return String.Empty;
	}
	
	public static Coord2 zero = new Coord2(0, 0);
	public static Coord2 MinValue = new Coord2(int.MinValue, int.MinValue);
	public static Coord2 MaxValue = new Coord2(int.MaxValue, int.MaxValue);
}
