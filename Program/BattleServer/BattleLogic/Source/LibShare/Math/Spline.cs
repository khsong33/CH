using UnityEngine;
using System.Collections;
using System;

#if !BATTLE_VERIFY
public class Spline
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 연결점(WeightPoint) 배열을 가지고, 곡선점(CurvePoint) 배열의 값을 수정합니다.
	public static void GetCurve(Vector2[] pWeightPoints, Vector2[] pCurvePoints)
	{
		float tt, _1_t, _2t, h00, h10, h01, h11;
		Vector2 m0;
		Vector2 m1;
		Vector2 m2;
		Vector2 m3;
		
		int lCurveCount = pCurvePoints.Length - 1;

		for (int iWeight = 0; iWeight < pWeightPoints.Length; ++iWeight)
		{
			for (int iCurve = 0; iCurve < pCurvePoints.Length; ++iCurve)
			{
				float t = (float)iCurve / lCurveCount;
			
				tt = t * t;
				_1_t = 1 - t;
				_2t = 2 * t;
				h00 =  (1 + _2t) * (_1_t) * (_1_t);
				h10 =  t  * (_1_t) * (_1_t);
				h01 =  tt * (3 - _2t);
				h11 =  tt * (t - 1);

				if (iWeight == 0)
				{
					m0 = _GetTangent(pWeightPoints[iWeight + 1], pWeightPoints[iWeight]);
					m1 = _GetTangent(pWeightPoints[iWeight + 2], pWeightPoints[iWeight]);
					pCurvePoints[iCurve] = new Vector2(
						h00 * pWeightPoints[iWeight].x + h10 * m0.x + h01 * pWeightPoints[iWeight + 1].x + h11 * m1.x,
						h00 * pWeightPoints[iWeight].y + h10 * m0.y + h01 * pWeightPoints[iWeight + 1].y + h11 * m1.y);
				}
				else if (iWeight < pWeightPoints.Length - 2)
				{
					m1 = _GetTangent(pWeightPoints[iWeight + 1], pWeightPoints[iWeight - 1]);
					m2 = _GetTangent(pWeightPoints[iWeight + 2], pWeightPoints[iWeight]);
					pCurvePoints[iCurve] = new Vector2(
						h00 * pWeightPoints[iWeight].x + h10 * m1.x + h01 * pWeightPoints[iWeight + 1].x + h11 * m2.x,
						h00 * pWeightPoints[iWeight].y + h10 * m1.y + h01 * pWeightPoints[iWeight + 1].y + h11 * m2.y);
				}
				else if (iWeight == pWeightPoints.Length - 1)
				{
					m2 = _GetTangent(pWeightPoints[iWeight], pWeightPoints[iWeight - 2]);
					m3 = _GetTangent(pWeightPoints[iWeight], pWeightPoints[iWeight - 1]);
					pCurvePoints[iCurve] = new Vector2(
						h00 * pWeightPoints[iWeight - 1].x + h10 * m2.x + h01 * pWeightPoints[iWeight].x + h11 * m3.x,
						h00 * pWeightPoints[iWeight - 1].y + h10 * m2.y + h01 * pWeightPoints[iWeight].y + h11 * m3.y);
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 접선 벡터를 구합니다.
	private static Vector2 _GetTangent(Vector2 p1, Vector2 p2)
	{
			return new Vector2((p1.x - p2.x) / 2, (p1.y - p2.y) / 2);
	}
	//-------------------------------------------------------------------------------------------------------
	//
// 	void __fastcall TForm1::Button1Click(TObject *Sender)
// 	{
// 		Vector2 p[7] = {Vector2(100,100), Vector2(120,180), Vector2(220,180), Vector2(250,250), Vector2(300,150), Vector2(350,200), Vector2(400,100)};
// 
// 		Curve(Form1->Canvas, p, 7, 0.005);
// 	}
}
#endif
