﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public delegate int OnGetNodeWeightDelegate(AStarNode pAStarNode);

public interface AStarNode
{
	int aRow { get; }
	int aCol { get; }
	int aWeight { get; }
}

public class AStarPathFinder<T_Tile> where T_Tile : AStarNode
{
	public const int cMaxWeight = Int32.MaxValue;
	public const int cMaxNode = 1000;

	public class NodeValue // 노드 비용 클래스
	{
		public int aRow;
		public int aCol;
		public int mG;
		public int mF;
//		public Coord2 mCoord;
		public NodeValue mParent;
		public NodeValue()
		{
			aRow = 0;
			aCol = 0;
			mG = cMaxWeight;
			mF = cMaxWeight;
			mParent = null;
		}
	}
	public class NodeMoveOffset // 노드 검색 클래스
	{
		public int mMoveRow = 0;
		public int mMoveCol = 0;
		public NodeMoveOffset(int pRow, int pCol)
		{
			mMoveRow = pRow;
			mMoveCol = pCol;
		}
	}

	// 디버깅용
	public List<NodeValue> mDebugNodePath;
	private NodeMoveOffset[] mMoveIndexsEven = new NodeMoveOffset[6]; // 6면으로 이동해서 확인할 경로 설정(col이 짝수)
	private NodeMoveOffset[] mMoveIndexsOdd = new NodeMoveOffset[6]; // 6면으로 이동해서 확인할 경로 설정(col이 홀수)

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void InitNode(T_Tile[,] pNodePaths, int pRow, int pCol, int pScaleX, int pScaleZ, int pBlockScaleX, int pBlockScaleZ, int pHeuristicValue)
	{
		mNodePaths = pNodePaths;
		mNodeRow = pRow;
		mNodeCol = pCol;
		mHeuristicValue = pHeuristicValue;

		mDebugNodePath = new List<NodeValue>();

		mMoveIndexsEven[0] = new NodeMoveOffset(1, 0);
		mMoveIndexsEven[1] = new NodeMoveOffset(1, 1);
		mMoveIndexsEven[2] = new NodeMoveOffset(0, 1);
		mMoveIndexsEven[3] = new NodeMoveOffset(-1, 0);
		mMoveIndexsEven[4] = new NodeMoveOffset(0, -1);
		mMoveIndexsEven[5] = new NodeMoveOffset(1, -1);

		mMoveIndexsOdd[0] = new NodeMoveOffset(1, 0);
		mMoveIndexsOdd[1] = new NodeMoveOffset(0, 1);
		mMoveIndexsOdd[2] = new NodeMoveOffset(-1, 1);
		mMoveIndexsOdd[3] = new NodeMoveOffset(-1, 0);
		mMoveIndexsOdd[4] = new NodeMoveOffset(-1, -1);
		mMoveIndexsOdd[5] = new NodeMoveOffset(0, -1);

		// 초기화
		mTaskId = 1;
		mNodeValue = new NodeValue[mNodeRow, mNodeCol];
		mNodeClosed = new int[mNodeRow, mNodeCol];
		mNodeOpen = new int[mNodeRow, mNodeCol];
		mNodeOpenList = new List<NodeValue>();

		mCellScaleX = pScaleX;
		mCellScaleZ = pScaleZ;
		mBlockScaleX = pBlockScaleX;
		mBlockScaleZ = pBlockScaleZ;

		for (int iRow = 0; iRow < mNodeRow; iRow++)
		{
			for (int iCol = 0; iCol < mNodeCol; iCol++)
			{
				mNodeValue[iRow, iCol] = new NodeValue();
				mNodeValue[iRow, iCol].aRow = iRow;
				mNodeValue[iRow, iCol].aCol = iCol;
				//mNodeValue[iRow, iCol].mCoord = _GetCellCenterCoord(iRow, iCol);

				mNodeClosed[iRow, iCol] = 0;
				mNodeOpen[iRow, iCol] = 0;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 휴리스틱 상수값 변경
	public void SetHeuristicValue(int pValue)
	{
		mHeuristicValue = pValue;
	}
	//-------------------------------------------------------------------------------------------------------
	// 길 찾기
	public int Search(T_Tile pStartNode, T_Tile pTargetNode, OnGetNodeWeightDelegate pOnGetNodeWeightDelegate, T_Tile[] pResultNodes, out int outPathCount)
	{
		//pResultNodes = new T_Tile[cMaxNode];
		OnGetNodeWeightDelegate lOnGetNodeWeightDelegate
			= (pOnGetNodeWeightDelegate != null) // 노드 비중을 얻는 델리게이트가 제공된다면
			? pOnGetNodeWeightDelegate // 그 델리게이트를 사용하고
			: OnGetNodeWeight; // 아니라면 노드의 aWeight를 그대로 사용하는 기본 델리게이트를 사용

		outPathCount = 0;

		//Coord2 lStartCoord = _GetCellCenterCoord(pStartNode.aCol, pStartNode.aRow);
		//Coord2 lTargetCoord = _GetCellCenterCoord(pTargetNode.aCol, pTargetNode.aRow);

		if (mNodePaths == null)
			return 0;
// 		if (lOnGetNodeWeightDelegate(pStartNode) == cMaxWeight || lOnGetNodeWeightDelegate(pTargetNode) == cMaxWeight)
// 			return 0;
		if (pStartNode.aRow == pTargetNode.aRow && pStartNode.aCol == pTargetNode.aCol)
			return 0;

		// 사용할 확인 변수 초기화
		mNodeOpenList.Clear();
		mDebugNodePath.Clear();
		// StartNode 처리
		{
			mNodeClosed[pStartNode.aRow, pStartNode.aCol] = mTaskId;
			mNodeValue[pStartNode.aRow, pStartNode.aCol].mParent = null;
		}
		// 시작점에서의 열린목록 추가
		for (int iMove = 0; iMove < 6; iMove++)
		{
			int lRowIndex = pStartNode.aRow + ((pStartNode.aCol % 2 == 0) ? mMoveIndexsEven[iMove].mMoveRow : mMoveIndexsOdd[iMove].mMoveRow);
			int lColIndex = pStartNode.aCol + ((pStartNode.aCol % 2 == 0) ? mMoveIndexsEven[iMove].mMoveCol : mMoveIndexsOdd[iMove].mMoveCol);

			if (lRowIndex < 0 || lRowIndex >= mNodeRow)
				continue;
			if (lColIndex < 0 || lColIndex >= mNodeCol)
				continue;
			if (lOnGetNodeWeightDelegate(mNodePaths[lRowIndex, lColIndex]) == cMaxWeight)
				continue;

			mNodeValue[lRowIndex, lColIndex].mG = lOnGetNodeWeightDelegate(mNodePaths[pStartNode.aRow, pStartNode.aCol]);

			//int lHeuristic = (Mathf.Abs(pTargetNode.aRow - lRowIndex) * mHeuristicValue) + (Mathf.Abs(pTargetNode.aCol - lColIndex) * mHeuristicValue);
			int lHeuristic = _GetHeuristic(mNodeValue[lRowIndex, lColIndex], mNodeValue[pTargetNode.aRow, pTargetNode.aCol]);

			mNodeValue[lRowIndex, lColIndex].mF = lHeuristic + mNodeValue[lRowIndex, lColIndex].mG;
			mNodeValue[lRowIndex, lColIndex].mParent = mNodeValue[pStartNode.aRow, pStartNode.aCol];

			//Debug.Log(String.Format("Row[{0}], Col[{1}], F-Value[{2}]", lRowIndex, lColIndex, mNodeValue[lRowIndex, lColIndex].mF));

			mNodeOpenList.Add(mNodeValue[lRowIndex, lColIndex]);

			// 열린 목록에 추가
			mNodeOpen[lRowIndex, lColIndex] = mTaskId;
		}
//		mNodeOpenList.Sort(_AStarComparer);
		SortUtil.SortList<NodeValue>(mNodeOpenList, _AStarComparer);
		int lCurrentCheckTask = 0;
		while (mNodeOpenList.Count > 0)
		{
			NodeValue lCurrentNodeValue = mNodeOpenList[0];
//			Debug.Log(String.Format("Get Node [{0}]/[{1}] - F[{2}]", lCurrentNodeValue.aRow, lCurrentNodeValue.aCol, lCurrentNodeValue.mF));
			mNodeOpenList.RemoveAt(0);
			mDebugNodePath.Add(mNodeValue[lCurrentNodeValue.aRow, lCurrentNodeValue.aCol]);
			mNodeClosed[lCurrentNodeValue.aRow, lCurrentNodeValue.aCol] = mTaskId;
			if (lCurrentNodeValue.aRow == pTargetNode.aRow && lCurrentNodeValue.aCol == pTargetNode.aCol)
			{
				// 검색한 길을 갖고 최적화된 길을 얻습니다.
				NodeValue lCurrentNodePath = mNodeValue[pTargetNode.aRow, pTargetNode.aCol];
				while (lCurrentNodePath != null)
				{
					pResultNodes[outPathCount] = mNodePaths[lCurrentNodePath.aRow, lCurrentNodePath.aCol];
					outPathCount++;
					if (outPathCount >= pResultNodes.Length)
					{
#if UNITY_EDITOR
						Debug.LogError("out Result nodes array over.");
#endif
						return lCurrentCheckTask;
					}
					lCurrentNodePath = lCurrentNodePath.mParent;
				}
				mTaskId++; // TaskID는 증가
#if UNITY_EDITOR
				//Debug.Log("Path finding loop count - " + lCurrentCheckTask);
#endif

				return lCurrentCheckTask;
			}
			for (int iMove = 0; iMove < 6; iMove++)
			{
				int lRowIndex = lCurrentNodeValue.aRow + ((lCurrentNodeValue.aCol % 2 == 0) ? mMoveIndexsEven[iMove].mMoveRow : mMoveIndexsOdd[iMove].mMoveRow);
				int lColIndex = lCurrentNodeValue.aCol + ((lCurrentNodeValue.aCol % 2 == 0) ? mMoveIndexsEven[iMove].mMoveCol : mMoveIndexsOdd[iMove].mMoveCol);

				if (lRowIndex < 0 || lRowIndex >= mNodeRow)
					continue;
				if (lColIndex < 0 || lColIndex >= mNodeCol)
					continue;
				if (lOnGetNodeWeightDelegate(mNodePaths[lRowIndex, lColIndex]) == cMaxWeight)
					continue;
				if (mNodeClosed[lRowIndex, lColIndex] == mTaskId)
					continue;
				if (mNodeOpen[lRowIndex, lColIndex] == mTaskId)
					continue;

				mNodeValue[lRowIndex, lColIndex].mG = lOnGetNodeWeightDelegate(mNodePaths[lRowIndex, lColIndex]) + mNodeValue[lCurrentNodeValue.aRow, lCurrentNodeValue.aCol].mG;

				//int lHeuristic = (Mathf.Abs(pTargetNode.aRow - lRowIndex) * mHeuristicValue) + (Mathf.Abs(pTargetNode.aCol - lColIndex) * mHeuristicValue);
				int lHeuristic = _GetHeuristic(mNodeValue[lRowIndex, lColIndex], mNodeValue[pTargetNode.aRow, pTargetNode.aCol]);

				mNodeValue[lRowIndex, lColIndex].mF = lHeuristic + mNodeValue[lRowIndex, lColIndex].mG;
				mNodeValue[lRowIndex, lColIndex].mParent = lCurrentNodeValue;

				//Debug.Log(String.Format("Row[{0}], Col[{1}], F-Value[{2}]", lRowIndex, lColIndex, mNodeValue[lRowIndex, lColIndex].mF));

				mNodeOpenList.Add(mNodeValue[lRowIndex, lColIndex]);
				// 열린 목록에 추가	
				mNodeOpen[lRowIndex, lColIndex] = mTaskId;
			}
//			mNodeOpenList.Sort(_AStarComparer);
			SortUtil.SortList<NodeValue>(mNodeOpenList, _AStarComparer);
			lCurrentCheckTask++;
		}
		// 못찾아도 TaskID는 변경
		mTaskId++;

		return lCurrentCheckTask;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnGetNodeWeightDelegate
	public static int OnGetNodeWeight(AStarNode pAStarNode)
	{
		return pAStarNode.aWeight;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 검색 우선 순위 정렬
	private int _AStarComparer(NodeValue lValue, NodeValue rValue)
	{
		if (lValue.mF < rValue.mF)
			return -1;
		else if (lValue.mF > rValue.mF)
			return 1;
		else
			return 0;
	}
	////-------------------------------------------------------------------------------------------------------
	//// 점과 직선의 거리를 구합니다.
	//private float _GetDistLineToPoint(Coord2 pStartCoord, Coord2 pTargetCoord, Coord2 pCurrentPointCoord)
	//{
	//	// 기울기를 구합니다.
	//	float lM = 0;
	//	if (pTargetCoord.x != pStartCoord.x) // exception divide zero
	//	{
	//		lM = (pTargetCoord.z - pStartCoord.z) / (pTargetCoord.x - pStartCoord.x);
	//	}
	//	// ax+by+c = 0의 일반식에서 C를 구합니다. b는 -1로 고정.
	//	float lC = pStartCoord.z - (lM * pStartCoord.x);

	//	// 점과 직선의 거리를 구합니다.
	//	float lDistance = Mathf.Abs(((lM * pCurrentPointCoord.x) - pCurrentPointCoord.z + lC)) / (Mathf.Sqrt((lM * lM) + 1));
		
	//	return lDistance / mCellScaleX;
	//}

	//-------------------------------------------------------------------------------------------------------
	// 점과 직선의 거리를 구합니다.
	private int _GetHeuristic(NodeValue pCurrentNode, NodeValue pTargetNode)
	{
		return (Mathf.Abs(pTargetNode.aRow - pCurrentNode.aRow) * mHeuristicValue) + (Mathf.Abs(pTargetNode.aCol - pCurrentNode.aCol) * mHeuristicValue);
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀의 중심 좌표를 구합니다.
	private Coord2 _GetCellCenterCoord(int pCellRowIndex, int pCellColIndex)
	{
		int lCellCoordX = pCellColIndex * mCellScaleX + mBlockScaleX;
		int lCellCoordZ;
		if ((pCellColIndex & 0x01) == 0) // 짝수열이라면
			lCellCoordZ = pCellRowIndex * mCellScaleZ + mBlockScaleZ;
		else // 홀수열이라면
			lCellCoordZ = pCellRowIndex * mCellScaleZ;

		return new Coord2(lCellCoordX, lCellCoordZ);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private T_Tile[,] mNodePaths;
	private int mNodeRow;
	private int mNodeCol;
	private int mHeuristicValue;

	private int mCellScaleX;
	private int mCellScaleZ;
	private int mBlockScaleX;
	private int mBlockScaleZ;

	// 계산에 필요한 영역
	int mTaskId;
	int[,] mNodeClosed;
	int[,] mNodeOpen;
	private NodeValue[,] mNodeValue;
	private List<NodeValue> mNodeOpenList;
}
