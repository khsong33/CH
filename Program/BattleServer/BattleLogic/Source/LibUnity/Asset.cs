﻿using System.Collections;
using System.Diagnostics;
using System.IO;
using System;

namespace UnityEngine
{
	public class Asset
	{
		public static readonly String cBundleResourceFolder = "BundleResources/";

		//-------------------------------------------------------------------------------------------------------
		// 함수
		//-------------------------------------------------------------------------------------------------------
		// 텍스트 에셋을 읽어옵니다.
		public static String LoadText(String pTextAssetBundle, String pTextAssetPath)
		{
			String lBundleResourcePath = String.Format("{0}{1}/{2}", cBundleResourceFolder, pTextAssetBundle, pTextAssetPath);
			String lFilePath = String.Empty;
			if (File.Exists(lBundleResourcePath))
			{
				//Debug.Log("Reading '" + lBundleResourcePath + "'.");
				lFilePath = lBundleResourcePath; //file found
			}
			else if (File.Exists(lBundleResourcePath + ".txt"))
			{
				lFilePath = lBundleResourcePath + ".txt";
			}
			else
			{
				Debug.LogError("can't find bundle file '" + lBundleResourcePath + "'");
				return null;
			}

			StreamReader lStreamReader;
			try
			{
				lStreamReader = new StreamReader(lFilePath);
			}
			catch (System.Exception e)
			{
				Debug.LogException(e);
				return null;
			}

			String lFileString = lStreamReader.ReadToEnd();
			lStreamReader.Close();

			return lFileString;
		}
	}
}
