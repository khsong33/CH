﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UnityEngine
{
	public class AssetInfo
	{
		public String vAssetPath;
		public String vBundleName;

		public AssetInfo()
		{
		}
		public AssetInfo(String pAssetPath, String pBundleName)
		{
			vAssetPath = pAssetPath;
			vBundleName = pBundleName;
		}
		public static bool IsValid(AssetInfo pAssetInfo)
		{
			if ((pAssetInfo != null) &&
				!String.IsNullOrEmpty(pAssetInfo.vAssetPath))
				return true;
			else
				return false;
		}
	}

	public class AssetKey : System.Object
	{
		internal AssetKey(int pBundleId, String pAssetPath)
		{
			mBundleId = pBundleId;
			mAssetPath = pAssetPath;
		}

		internal int mBundleId;
		internal String mAssetPath;

		public String aAssetPath
		{
			get { return mAssetPath; }
		}

		public override string ToString()
		{
			return String.Format("AssetKey<BundleId={0} AssetPath={1}>", mBundleId, mAssetPath);
		}

		public static bool IsValid(AssetKey pAssetKey)
		{
			if ((pAssetKey != null) &&
				!String.IsNullOrEmpty(pAssetKey.mAssetPath))
				return true;
			else
				return false;
		}

		//public static bool operator ==(AssetKey lhs, AssetKey rhs)
		//{
		//	bool status = false;
		//	if (lhs.mBundleId == rhs.mBundleId && lhs.mAssetPath == rhs.mAssetPath)
		//	{
		//		status = true;
		//	}
		//	return status;
		//}
		//public static bool operator !=(AssetKey lhs, AssetKey rhs)
		//{
		//	bool status = false;
		//	if (lhs.mBundleId != rhs.mBundleId || lhs.mAssetPath != rhs.mAssetPath)
		//	{
		//		status = true;
		//	}
		//	return status;
		//}

	}

	public class AssetManager
	{
		public readonly String[] vAssetBundleNames = { "Tables0", "GridLayouts0" };

		//-------------------------------------------------------------------------------------------------------
		// 속성
		//-------------------------------------------------------------------------------------------------------
		// 싱글톤 객체
		public static AssetManager get
		{
			get
			{
				if (sInstance == null)
					CreateSingleton();
				return sInstance;
			}
		}

		//-------------------------------------------------------------------------------------------------------
		// 메서드
		//-------------------------------------------------------------------------------------------------------
		// 싱글톤 객체를 생성합니다.
		public static void CreateSingleton()
		{
			if (sInstance == null)
			{
				sInstance = new AssetManager();
			}
		}
		//-------------------------------------------------------------------------------------------------------
		// 생성자
		public AssetManager()
		{
			mAssetBundleIdDictionary = new Dictionary<string, int>();

			for (int iBundle = 0; iBundle < vAssetBundleNames.Length; ++iBundle)
				mAssetBundleIdDictionary.Add(vAssetBundleNames[iBundle], iBundle);
		}
		//-------------------------------------------------------------------------------------------------------
		// 에셋 키를 생성합니다.
		public AssetKey CreateAssetKey(String pAssetBundleName, String pAssetPath)
		{
			int lAssetBundleId;
			if (!mAssetBundleIdDictionary.TryGetValue(pAssetBundleName, out lAssetBundleId))
				lAssetBundleId = -1;

			AssetKey lAssetKey = new AssetKey(lAssetBundleId, pAssetPath);
			return lAssetKey;
		}
		//-------------------------------------------------------------------------------------------------------
		// 에셋 키를 생성합니다.
		public AssetKey CreateAssetKey(AssetInfo pAssetInfo)
		{
			return CreateAssetKey(pAssetInfo.vBundleName, pAssetInfo.vAssetPath);
		}
		//-------------------------------------------------------------------------------------------------------
		// 에셋을 로딩한 다음에 반환합니다.
		public T_Object Load<T_Object>(AssetKey pAssetKey)
		{
			return default(T_Object);
		}
		//-------------------------------------------------------------------------------------------------------
		// 에셋을 로딩한 다음에 반환합니다.
		public String LoadText(AssetKey pAssetKey)
		{
			return Asset.LoadText(vAssetBundleNames[pAssetKey.mBundleId], pAssetKey.mAssetPath);
		}

		//-------------------------------------------------------------------------------------------------------
		// 구현
		//-------------------------------------------------------------------------------------------------------

		//-------------------------------------------------------------------------------------------------------
		// 데이터
		//-------------------------------------------------------------------------------------------------------
		private static AssetManager sInstance = null;

		private Dictionary<String, int> mAssetBundleIdDictionary;
	}
}
