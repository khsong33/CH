﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     Representation of 3D vectors and points.
	public struct Color
	{
		// 요약:
		//     Alpha component of the color.
		public float a;
		//
		// 요약:
		//     Blue component of the color.
		public float b;
		//
		// 요약:
		//     Green component of the color.
		public float g;
		//
		// 요약:
		//     Red component of the color.
		public float r;

		public Color(float init_r, float init_g, float init_b)
		{
			r = init_r;
			g = init_g;
			b = init_b;
			a = 1.0f;
		}
		public Color(float init_r, float init_g, float init_b, float init_a)
		{
			r = init_r;
			g = init_g;
			b = init_b;
			a = init_a;
		}

		// 요약:
		//     Solid black. RGBA is (0, 0, 0, 1).
		private static readonly Color _black = new Color(0, 0, 0, 1);
		public static Color black { get { return _black; } }
		//
		// 요약:
		//     Solid blue. RGBA is (0, 0, 1, 1).
		private static readonly Color _blue = new Color(0, 0, 1, 1);
		public static Color blue { get { return _blue; } }
		//
		// 요약:
		//     Completely transparent. RGBA is (0, 0, 0, 0).
		private static readonly Color _clear = new Color(0, 0, 0, 0);
		public static Color clear { get { return _clear; } }
		//
		// 요약:
		//     Cyan. RGBA is (0, 1, 1, 1).
		private static readonly Color _cyan = new Color(0, 1, 1, 1);
		public static Color cyan { get { return _cyan; } }
		//
		// 요약:
		//     Gray. RGBA is (0.5, 0.5, 0.5, 1).
		private static readonly Color _gray = new Color(0.5f, 0.5f, 0.5f, 1);
		public static Color gray { get { return _gray; } }
		//
		// 요약:
		//     Solid green. RGBA is (0, 1, 0, 1).
		private static readonly Color _green = new Color(0, 1, 0, 1);
		public static Color green { get { return _green; } }
		//
		// 요약:
		//     English spelling for gray. RGBA is the same (0.5, 0.5, 0.5, 1).
		private static readonly Color _grey = new Color(0.5f, 0.5f, 0.5f, 1);
		public static Color grey { get { return _grey; } }
		//
		// 요약:
		//     Magenta. RGBA is (1, 0, 1, 1).
		private static readonly Color _magenta = new Color(1, 0, 1, 1);
		public static Color magenta { get { return _magenta; } }
		//
		// 요약:
		//     Solid red. RGBA is (1, 0, 0, 1).
		private static readonly Color _red = new Color(1, 0, 0, 1);
		public static Color red { get { return _red; } }
		//
		// 요약:
		//     Solid white. RGBA is (1, 1, 1, 1).
		private static readonly Color _white = new Color(1, 1, 1, 1);
		public static Color white { get { return _white; } }
		//
		// 요약:
		//     Yellow. RGBA is (1, 0.92, 0.016, 1), but the color is nice to look at!
		private static readonly Color _yellow = new Color(1, 0.92f, 0.016f, 1);
		public static Color yellow { get { return _yellow; } }
	}
}
