﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	public sealed class Debug
	{
		public static void Log(object message)
		{
			Console.WriteLine("[Log] " + message);
		}
		public static void LogFormat(object message, params object[] args)
		{
			Console.WriteLine("[Log] " + message, args);
		}
		public static void Log(object message, Object context)
		{
			Console.WriteLine("[Log] " + message);
		}
		public static void LogError(object message)
		{
			Console.WriteLine("[LogError] " + message);
		}
		public static void LogErrorFormat(object message, params object[] args)
		{
			Console.WriteLine("[LogError] " + message, args);
		}
		public static void LogError(object message, Object context)
		{
			Console.WriteLine("[LogError] " + message);
		}
		public static void LogException(Exception exception)
		{
			Console.WriteLine("[LogException] " + exception);
		}
		public static void LogException(Exception exception, Object context)
		{
			Console.WriteLine("[LogException] " + exception);
		}
		public static void LogWarning(object message)
		{
			Console.WriteLine("[LogWarning] " + message);
		}
		public static void LogWarningFormat(object message, params object[] args)
		{
			Console.WriteLine("[LogWarning] " + message, args);
		}
		public static void LogWarning(object message, Object context)
		{
			Console.WriteLine("[LogWarning] " + message);
		}
	}
}
