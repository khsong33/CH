﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	public class DebugUtil
	{
		//-------------------------------------------------------------------------------------------------------
		// 함수
		//-------------------------------------------------------------------------------------------------------
		// Assert
		[Conditional("DEBUG")]
		public static void Assert(bool pCondition, String pMessage)
		{
			if (!pCondition)
				Debug.LogError("Assert: " + pMessage);
		}
	}
}
