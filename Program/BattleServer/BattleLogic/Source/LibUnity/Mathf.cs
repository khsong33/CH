﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     A collection of common math functions.
	public struct Mathf
	{
		public const float Deg2Rad = 0.0174533f;
		public const float Epsilon = 1.4013e-045f;
		public const float Infinity = 1.0f / 0.0f;
		public const float NegativeInfinity = -1.0f / 0.0f;
		public const float PI = 3.14159f;
		public const float Rad2Deg = 57.2958f;

		// 요약:
		//     Returns the absolute value of f.
		public static float Abs(float f)
		{
			return ((f >= 0.0f) ? f : -f);
		}
		//
		// 요약:
		//     Returns the absolute value of f.
		public static int Abs(int value)
		{
			return ((value >= 0) ? value : -value);
		}
		//
		// 요약:
		//     Returns the arc-cosine of f - the angle in radians whose cosine is f.
		public static float Acos(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Compares two floating point values if they are similar.
		public static bool Approximately(float a, float b)
		{
			Debugger.Break();
			return false;
		}
		//
		// 요약:
		//     Returns the arc-sine of f - the angle in radians whose sine is f.
		public static float Asin(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the arc-tangent of f - the angle in radians whose tangent is f.
		public static float Atan(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the angle in radians whose Tan is y/x.
		public static float Atan2(float y, float x)
		{
			//Debugger.Break();
			return (float)Math.Atan2(y, x);
		}
		//
		// 요약:
		//     Returns the smallest integer greater to or equal to f.
		public static float Ceil(float f)
		{
// 			float lIntValue = (int)f;
// 			if (f > 0.0f)
// 				return (f > lIntValue) ? (lIntValue + 1.0f) : lIntValue;
// 			else
// 				return lIntValue;

			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the smallest integer greater to or equal to f.
		public static int CeilToInt(float f)
		{
			int lIntValue = (int)f;
			if (f > 0.0f)
				return (f > (float)lIntValue) ? (lIntValue + 1) : lIntValue;
			else
				return lIntValue;

//			Debugger.Break();
			//return 0;
		}
		//
		// 요약:
		//     Clamps a value between a minimum float and maximum float value.
		public static float Clamp(float value, float min, float max)
		{
			if (value < min)
				return min;
			else if (value > max)
				return max;
			else
				return value;
		}
		//
		// 요약:
		//     Clamps a value between a minimum float and maximum float value.
		public static int Clamp(int value, int min, int max)
		{
			if (value < min)
				return min;
			else if (value > max)
				return max;
			else
				return value;
		}
		//
		// 요약:
		//     Clamps value between 0 and 1 and returns value.
		public static float Clamp01(float value)
		{
			if (value < 0.0f)
				return 0.0f;
			else if (value > 1.0f)
				return 1.0f;
			else
				return value;
		}
		//
		// 요약:
		//     Returns the closest power of two value.
		public static int ClosestPowerOfTwo(int value)
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Returns the cosine of angle f in radians.
		public static float Cos(float f)
		{
			//Debugger.Break();
			return (float)Math.Cos(f);
		}
		//
		// 요약:
		//     Calculates the shortest difference between two given angles.
		public static float DeltaAngle(float current, float target)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns e raised to the specified power.
		public static float Exp(float power)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the largest integer smaller to or equal to f.
		public static float Floor(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the largest integer smaller to or equal to f.
		public static int FloorToInt(float f)
		{
			int lIntValue = (int)f;
			if (f > 0.0f)
				return (f >= (float)lIntValue) ? (lIntValue) : lIntValue - 1;
			else
				return (f < (float)lIntValue) ? lIntValue - 1 : (lIntValue);
			//Debugger.Break();
			//return 0;
		}
		public static float Gamma(float value, float absmax, float gamma)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Converts the given value from gamma to linear color space.
		public static float GammaToLinearSpace(float value)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Calculates the Lerp parameter between of two values.
		public static float InverseLerp(float from, float to, float value)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns true if the value is power of two.
		public static bool IsPowerOfTwo(int value)
		{
			Debugger.Break();
			return false;
		}
		//
		// 요약:
		//     Interpolates between a and b by t. t is clamped between 0 and 1.
		public static float Lerp(float from, float to, float t)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Same as Lerp but makes sure the values interpolate correctly when they wrap
		//     around 360 degrees.
		public static float LerpAngle(float a, float b, float t)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Converts the given value from linear to gamma color space.
		public static float LinearToGammaSpace(float value)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the logarithm of a specified number in a specified base.
		public static float Log(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the logarithm of a specified number in a specified base.
		public static float Log(float f, float p)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the base 10 logarithm of a specified number.
		public static float Log10(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns largest of two or more values.
		public static float Max(params float[] values)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns largest of two or more values.
		public static int Max(params int[] values)
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Returns largest of two or more values.
		public static float Max(float a, float b)
		{
			return ((a > b) ? a : b);
		}
		//
		// 요약:
		//     Returns largest of two or more values.
		public static int Max(int a, int b)
		{
			return ((a > b) ? a : b);
		}
		//
		// 요약:
		//     Returns the smallest of two or more values.
		public static float Min(params float[] values)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the smallest of two or more values.
		public static int Min(params int[] values)
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Returns the smallest of two or more values.
		public static float Min(float a, float b)
		{
			return ((a < b) ? a : b);
		}
		//
		// 요약:
		//     Returns the smallest of two or more values.
		public static int Min(int a, int b)
		{
			return ((a < b) ? a : b);
		}
		//
		// 요약:
		//     Moves a value current towards target.
		public static float MoveTowards(float current, float target, float maxDelta)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Same as MoveTowards but makes sure the values interpolate correctly when
		//     they wrap around 360 degrees.
		public static float MoveTowardsAngle(float current, float target, float maxDelta)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the next power of two value.
		public static int NextPowerOfTwo(int value)
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Generate 2D Perlin noise.
		public static float PerlinNoise(float x, float y)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     PingPongs the value t, so that it is never larger than length and never smaller
		//     than 0.
		public static float PingPong(float t, float length)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns f raised to power p.
		public static float Pow(float f, float p)
		{
			//Debugger.Break();
			return (float)Math.Pow(f, p);
		}
		//
		// 요약:
		//     Loops the value t, so that it is never larger than length and never smaller
		//     than 0.
		public static float Repeat(float t, float length)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns f rounded to the nearest integer.
		public static float Round(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns f rounded to the nearest integer.
		public static int RoundToInt(float f)
		{
			int lIntValue = (int)f;
			float lFloatOffset = f - lIntValue;

			if (lFloatOffset < 0.5f)
			{
				return lIntValue;
			}
			else if (lFloatOffset > 0.5f)
			{
				return (lIntValue + 1);
			}
			else // 0.5일 경우 양측의 정수 중에서 짝수를 따라감
			{
				if ((lIntValue % 2) == 0)
					return lIntValue;
				else
					return (lIntValue + 1);
			}
		}
		//
		// 요약:
		//     Returns the sign of f.
		public static float Sign(float f)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns the sine of angle f in radians.
		public static float Sin(float f)
		{
			//Debugger.Break();
			return (float)Math.Sin(f);
		}
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime)
		{
			Debugger.Break();
			return 0.0f;
		}
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			Debugger.Break();
			return 0.0f;
		}
		public static float SmoothDamp(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
		{
			Debugger.Break();
			return 0.0f;
		}
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime)
		{
			Debugger.Break();
			return 0.0f;
		}
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed)
		{
			Debugger.Break();
			return 0.0f;
		}
		public static float SmoothDampAngle(float current, float target, ref float currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Interpolates between min and max with smoothing at the limits.
		public static float SmoothStep(float from, float to, float t)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns square root of f.
		public static float Sqrt(float f)
		{
			//Debugger.Break();
			return (float)Math.Sqrt(f);
		}
		//
		// 요약:
		//     Returns the tangent of angle f in radians.
		public static float Tan(float f)
		{
			//Debugger.Break();
			return (float)Math.Tan(f);
		}
	}
}
