﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     Quaternions are used to represent rotations.
	public struct Quaternion
	{
		public const float kEpsilon = 1e-006f;

		// 요약:
		//     W component of the Quaternion. Don't modify this directly unless you know
		//     quaternions inside out.
		public float w;
		//
		// 요약:
		//     X component of the Quaternion. Don't modify this directly unless you know
		//     quaternions inside out.
		public float x;
		//
		// 요약:
		//     Y component of the Quaternion. Don't modify this directly unless you know
		//     quaternions inside out.
		public float y;
		//
		// 요약:
		//     Z component of the Quaternion. Don't modify this directly unless you know
		//     quaternions inside out.
		public float z;

		public Quaternion(float init_x, float init_y, float init_z, float init_w)
		{
			x = init_x; y = init_y; z = init_z; w = init_w;
		}

		public static bool operator !=(Quaternion lhs, Quaternion rhs)
		{
			Debugger.Break();
			return false;
		}
		public static Quaternion operator *(Quaternion lhs, Quaternion rhs)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		public static Vector3 operator *(Quaternion rotation, Vector3 point)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public static bool operator ==(Quaternion lhs, Quaternion rhs)
		{
			Debugger.Break();
			return false;
		}

		// 요약:
		//     Returns the euler angle representation of the rotation.
		public Vector3 eulerAngles
		{
			get
			{
				Debugger.Break();
				return Vector3.zero;
			}
			set
			{
				Debugger.Break();
			}
		}
		//
		// 요약:
		//     The identity rotation (Read Only). This quaternion corresponds to "no rotation":
		//     the object.
		private static readonly Quaternion _identity = new Quaternion(0.0f, 0.0f, 0.0f, 1.0f);
		public static Quaternion identity { get { return _identity; } }

		public float this[int index]
		{
			get
			{
				switch (index)
				{
				case 0: return x;
				case 1: return y;
				case 2: return z;
				case 3: return w;
				default: return 0.0f;
				}
			}
			set
			{
				switch (index)
				{
				case 0: x = value; break;
				case 1: y = value; break;
				case 2: z = value; break;
				case 3: w = value; break;
				}
			}
		}

		// 요약:
		//     Returns the angle in degrees between two rotations a and b.
		public static float Angle(Quaternion a, Quaternion b)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Creates a rotation which rotates angle degrees around axis.
		public static Quaternion AngleAxis(float angle, Vector3 axis)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		[Obsolete("Use Quaternion.AngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion AxisAngle(Vector3 axis, float angle)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     The dot product between two rotations.
		public static float Dot(Quaternion a, Quaternion b)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		public override bool Equals(object other)
		{
			Debugger.Break();
			return false;
		}
		//
		// 요약:
		//     Returns a rotation that rotates z degrees around the z axis, x degrees around
		//     the x axis, and y degrees around the y axis (in that order).
		public static Quaternion Euler(Vector3 euler)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Returns a rotation that rotates z degrees around the z axis, x degrees around
		//     the x axis, and y degrees around the y axis (in that order).
		public static Quaternion Euler(float x, float y, float z)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerAngles(Vector3 euler)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerAngles(float x, float y, float z)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerRotation(Vector3 euler)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public static Quaternion EulerRotation(float x, float y, float z)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Creates a rotation which rotates from fromDirection to toDirection.
		public static Quaternion FromToRotation(Vector3 fromDirection, Vector3 toDirection)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		public override int GetHashCode()
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Returns the Inverse of rotation.
		public static Quaternion Inverse(Quaternion rotation)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Interpolates between from and to by t and normalizes the result afterwards.
		public static Quaternion Lerp(Quaternion from, Quaternion to, float t)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Creates a rotation with the specified forward and upwards directions.
		public static Quaternion LookRotation(Vector3 forward)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Creates a rotation with the specified forward and upwards directions.
		public static Quaternion LookRotation(Vector3 forward, Vector3 upwards)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Rotates a rotation from towards to.
		public static Quaternion RotateTowards(Quaternion from, Quaternion to, float maxDegreesDelta)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		//
		// 요약:
		//     Set x, y, z and w components of an existing Quaternion.
		public void Set(float new_x, float new_y, float new_z, float new_w)
		{
			x = new_x; y = new_y; z = new_z; w = new_w;
		}
		[Obsolete("Use Quaternion.AngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetAxisAngle(Vector3 axis, float angle)
		{
			Debugger.Break();
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerAngles(Vector3 euler)
		{
			Debugger.Break();
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerAngles(float x, float y, float z)
		{
			Debugger.Break();
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerRotation(Vector3 euler)
		{
			Debugger.Break();
		}
		[Obsolete("Use Quaternion.Euler instead. This function was deprecated because it uses radians instead of degrees")]
		public void SetEulerRotation(float x, float y, float z)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Creates a rotation which rotates from fromDirection to toDirection.
		public void SetFromToRotation(Vector3 fromDirection, Vector3 toDirection)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Creates a rotation with the specified forward and upwards directions.
		public void SetLookRotation(Vector3 view)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Creates a rotation with the specified forward and upwards directions.
		public void SetLookRotation(Vector3 view, Vector3 up)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Spherically interpolates between from and to by t.
		public static Quaternion Slerp(Quaternion from, Quaternion to, float t)
		{
			Debugger.Break();
			return Quaternion.identity;
		}
		public void ToAngleAxis(out float angle, out Vector3 axis)
		{
			Debugger.Break();
			angle = 0.0f;
			axis = Vector3.zero;
		}
		[Obsolete("Use Quaternion.ToAngleAxis instead. This function was deprecated because it uses radians instead of degrees")]
		public void ToAxisAngle(out Vector3 axis, out float angle)
		{
			Debugger.Break();
			angle = 0.0f;
			axis = Vector3.zero;
		}
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public Vector3 ToEuler()
		{
			Debugger.Break();
			return Vector3.zero;
		}
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public Vector3 ToEulerAngles()
		{
			Debugger.Break();
			return Vector3.zero;
		}
		[Obsolete("Use Quaternion.eulerAngles instead. This function was deprecated because it uses radians instead of degrees")]
		public static Vector3 ToEulerAngles(Quaternion rotation)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public override string ToString()
		{
			return String.Format("({0},{1},{2},{3})", x, y, z,w);
		}
		//
		// 요약:
		//     Returns a nicely formatted string of the Quaternion.
		public string ToString(string format)
		{
			return String.Format(format, x, y, z, w);
		}
	}
}
