﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     Class for generating random data.
	public sealed class Random
	{
		public Random()
		{
			Debugger.Break();
		}

		// 요약:
		//     Returns a random point inside a circle with radius 1 (Read Only).
		public static Vector2 insideUnitCircle
		{
			get
			{
				Debugger.Break();
				return Vector2.zero;
			}
		}
		//
		// 요약:
		//     Returns a random point inside a sphere with radius 1 (Read Only).
		public static Vector3 insideUnitSphere
		{
			get
			{
				Debugger.Break();
				return Vector3.zero;
			}
		}
		//
		// 요약:
		//     Returns a random point on the surface of a sphere with radius 1 (Read Only).
		public static Vector3 onUnitSphere
		{
			get
			{
				Debugger.Break();
				return Vector3.zero;
			}
		}
		//
		// 요약:
		//     Returns a random rotation (Read Only).
		public static Quaternion rotation
		{
			get
			{
				Debugger.Break();
				return Quaternion.identity;
			}
		}
		//
		// 요약:
		//     Returns a random rotation with uniform distribution (Read Only).
		public static Quaternion rotationUniform
		{
			get
			{
				Debugger.Break();
				return Quaternion.identity;
			}
		}
		//
		// 요약:
		//     Sets the seed for the random number generator.
		public static int seed
		{
			get
			{
				Debugger.Break();
				return 0;
			}
			set
			{
				Debugger.Break();
			}
		}
		//
		// 요약:
		//     Returns a random number between 0.0 [inclusive] and 1.0 [inclusive] (Read
		//     Only).
		public static float value
		{
			get
			{
				Debugger.Break();
				return 0.0f;
			}
		}

		[Obsolete("Use Random.Range instead")]
		public static float RandomRange(float min, float max)
		{
			Debugger.Break();
			return 0.0f;
		}
		[Obsolete("Use Random.Range instead")]
		public static int RandomRange(int min, int max)
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Returns a random float number between and min [inclusive] and max [inclusive]
		//     (Read Only).
		public static float Range(float min, float max)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns a random float number between and min [inclusive] and max [inclusive]
		//     (Read Only).
		public static int Range(int min, int max)
		{
			Debugger.Break();
			return 0;
		}
	}
}
