﻿using UnityEngine;
using System.Collections;
using System.IO;
using System;

public class ResourceUtil
{
	public static readonly String cResourceFolder = "Resources/";

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 경로의 문자열을 얻습니다.
	public static String LoadString(String pStringPath)
	{
		//Debug.Log("LoadString() pStringPath=" + pStringPath);

		String lResourcePath = cResourceFolder + pStringPath;
		String lFilePath = String.Empty;
		if (File.Exists(lResourcePath))
		{
			//Debug.Log("Reading '" + lResourcePath + "'.");
			lFilePath = lResourcePath; //file found
		}
		else if (File.Exists(lResourcePath + ".txt"))
		{
			lFilePath = lResourcePath + ".txt";
		}
		else
		{
			Debug.LogError("can't find file '" + lResourcePath + "'");
			return null;
		}

		StreamReader lStreamReader;
		try
		{
			lStreamReader = new StreamReader(lFilePath);
		}
		catch (System.Exception e)
		{
			Debug.LogException(e);
			return null;
		}

		String lFileString = lStreamReader.ReadToEnd();
		lStreamReader.Close();

		return lFileString;
	}
}
