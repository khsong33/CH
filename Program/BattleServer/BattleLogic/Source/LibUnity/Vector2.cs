﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     Representation of 2D vectors and points.
	public struct Vector2
	{
		public const float kEpsilon = 1e-005f;

		// 요약:
		//     X component of the vector.
		public float x;
		//
		// 요약:
		//     Y component of the vector.
		public float y;

		public Vector2(float init_x, float init_y)
		{
			x = init_x; y = init_y;
		}

		public static Vector2 operator -(Vector2 a)
		{
			return new Vector2(-a.x, -a.y);
		}
		public static Vector2 operator -(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x - b.x, a.y - b.y);
		}
		public static bool operator !=(Vector2 lhs, Vector2 rhs)
		{
			return ((lhs.x != rhs.x) || (lhs.y != rhs.y));
		}
		public static Vector2 operator *(float d, Vector2 a)
		{
			return new Vector2(d * a.x, d * a.y);
		}
		public static Vector2 operator *(Vector2 a, float d)
		{
			return new Vector2(a.x * d, a.y * d);
		}
		public static Vector2 operator /(Vector2 a, float d)
		{
			return new Vector2(a.x / d, a.y / d);
		}
		public static Vector2 operator +(Vector2 a, Vector2 b)
		{
			return new Vector2(a.x + b.x, a.y + b.y);
		}
		public static bool operator ==(Vector2 lhs, Vector2 rhs)
		{
			return ((lhs.x == rhs.x) && (lhs.y == rhs.y));
		}
		public static implicit operator Vector3(Vector2 v)
		{
			return new Vector3(v.x, v.y, 0.0f);
		}
		public static implicit operator Vector2(Vector3 v)
		{
			return new Vector2(v.x, v.y);
		}

		// 요약:
		//     Returns the length of this vector (Read Only).
		public float magnitude { get { Debugger.Break(); return 0.0f; } }
		//
		// 요약:
		//     Returns this vector with a magnitude of 1 (Read Only).
		public Vector2 normalized { get { Debugger.Break(); return Vector2.zero; } }
		//
		// 요약:
		//     Shorthand for writing Vector2(1, 1).
		private static readonly Vector2 _one = new Vector2(1.0f, 1.0f);
		public static Vector2 one { get { return _one; } }
		//
		// 요약:
		//     Shorthand for writing Vector2(1, 0).
		private static readonly Vector2 _right = new Vector2(1.0f, 0.0f);
		public static Vector2 right { get { return _right; } }
		//
		// 요약:
		//     Returns the squared length of this vector (Read Only).
		public float sqrMagnitude { get { Debugger.Break(); return 0.0f; } }
		//
		// 요약:
		//     Shorthand for writing Vector2(0, 1).
		private static readonly Vector2 _up = new Vector2(0.0f, 1.0f);
		public static Vector2 up { get { return _up; } }
		//
		// 요약:
		//     Shorthand for writing Vector2(0, 0).
		private static readonly Vector2 _zero = new Vector2(0.0f, 0.0f);
		public static Vector2 zero { get { return _zero; } }

		public float this[int index]
		{
			get
			{
				switch (index)
				{
				case 0: return x;
				case 1: return y;
				default: return 0.0f;
				}
			}
			set
			{
				switch (index)
				{
				case 0: x = value; break;
				case 1: y = value; break;
				}
			}
		}

		// 요약:
		//     Returns the angle in degrees between from and to.
		public static float Angle(Vector2 from, Vector2 to)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns a copy of vector with its magnitude clamped to maxLength.
		public static Vector2 ClampMagnitude(Vector2 vector, float maxLength)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		//
		// 요약:
		//     Returns the distance between a and b.
		public static float Distance(Vector2 a, Vector2 b)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Dot Product of two vectors.
		public static float Dot(Vector2 lhs, Vector2 rhs)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		public override bool Equals(object other)
		{
			Debugger.Break();
			return false;
		}
		public override int GetHashCode()
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Linearly interpolates between two vectors.
		public static Vector2 Lerp(Vector2 from, Vector2 to, float t)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		//
		// 요약:
		//     Returns a vector that is made from the largest components of two vectors.
		public static Vector2 Max(Vector2 lhs, Vector2 rhs)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		//
		// 요약:
		//     Returns a vector that is made from the smallest components of two vectors.
		public static Vector2 Min(Vector2 lhs, Vector2 rhs)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		//
		// 요약:
		//     Moves a point current towards target.
		public static Vector2 MoveTowards(Vector2 current, Vector2 target, float maxDistanceDelta)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		public void Normalize()
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Multiplies two vectors component-wise.
		public void Scale(Vector2 scale)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Multiplies two vectors component-wise.
		public static Vector2 Scale(Vector2 a, Vector2 b)
		{
			Debugger.Break();
			return Vector2.zero;
		}
		//
		// 요약:
		//     Set x and y components of an existing Vector2.
		public void Set(float new_x, float new_y)
		{
			x = new_x; y = new_y;
		}
		public float SqrMagnitude()
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		public static float SqrMagnitude(Vector2 a)
		{
			Debugger.Break();
			return 0.0f;
		}
		public override string ToString()
		{
			return String.Format("({0},{1})", x, y);
		}
		//
		// 요약:
		//     Returns a nicely formatted string for this vector.
		public string ToString(string format)
		{
			return String.Format("({0},{1})", x, y);
		}
	}
}
