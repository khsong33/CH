﻿using System.Collections;
using System.Diagnostics;
using System;

namespace UnityEngine
{
	// 요약:
	//     Representation of 3D vectors and points.
	public struct Vector3
	{
		public const float kEpsilon = 1e-005f;

		// 요약:
		//     X component of the vector.
		public float x;
		//
		// 요약:
		//     Y component of the vector.
		public float y;
		//
		// 요약:
		//     Z component of the vector.
		public float z;

		public Vector3(float init_x, float init_y)
		{
			x = init_x; y = init_y; z = 0.0f;
		}
		public Vector3(float init_x, float init_y, float init_z)
		{
			x = init_x; y = init_y; z = init_z;
		}
		public static Vector3 operator -(Vector3 a)
		{
			return new Vector3(-a.x, -a.y, -a.z);
		}
		public static Vector3 operator -(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x - b.x, a.y - b.y, a.z - b.z);
		}
		public static bool operator !=(Vector3 lhs, Vector3 rhs)
		{
			return ((lhs.x != rhs.x) || (lhs.y != rhs.y) || (lhs.z != rhs.z));
		}
		public static Vector3 operator *(float d, Vector3 a)
		{
			return new Vector3(d * a.x, d * a.y, d * a.z);
		}
		public static Vector3 operator *(Vector3 a, float d)
		{
			return new Vector3(a.x * d, a.y * d, a.z * d);
		}
		public static Vector3 operator /(Vector3 a, float d)
		{
			return new Vector3(a.x / d, a.y / d, a.z / d);
		}
		public static Vector3 operator +(Vector3 a, Vector3 b)
		{
			return new Vector3(a.x + b.x, a.y + b.y, a.z + b.z);
		}
		public static bool operator ==(Vector3 lhs, Vector3 rhs)
		{
			return ((lhs.x == rhs.x) && (lhs.y == rhs.y) && (lhs.z == rhs.z));
		}

		// 요약:
		//     Shorthand for writing Vector3(0, 0, -1).
		private static readonly Vector3 _back = new Vector3(0.0f, 0.0f, -1.0f);
		public static Vector3 back { get { return _back; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(0, -1, 0).
		private static readonly Vector3 _down = new Vector3(0.0f, -1.0f, 0.0f);
		public static Vector3 down { get { return _down; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(0, 0, 1).
		private static readonly Vector3 _forward = new Vector3(0.0f, 0.0f, 1.0f);
		public static Vector3 forward { get { return _forward; } }
		[Obsolete("Use Vector3.forward instead.")]
		public static Vector3 fwd { get { return _forward; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(-1, 0, 0).
		private static readonly Vector3 _left = new Vector3(-1.0f, 0.0f, 0.0f);
		public static Vector3 left { get { return _left; } }
		//
		// 요약:
		//     Returns the length of this vector (Read Only).
		public float magnitude { get { Debugger.Break(); return 0.0f; } }
		//
		// 요약:
		//     Returns this vector with a magnitude of 1 (Read Only).
		public Vector3 normalized { get { Debugger.Break(); return Vector3.zero; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(1, 1, 1).
		private static readonly Vector3 _one = new Vector3(1.0f, 1.0f, 1.0f);
		public static Vector3 one { get { return _one; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(1, 0, 0).
		private static readonly Vector3 _right = new Vector3(1.0f, 0.0f, 0.0f);
		public static Vector3 right { get { return _right; } }
		//
		// 요약:
		//     Returns the squared length of this vector (Read Only).
		public float sqrMagnitude { get { Debugger.Break(); return 0.0f; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(0, 1, 0).
		private static readonly Vector3 _up = new Vector3(0.0f, 1.0f, 0.0f);
		public static Vector3 up { get { return _up; } }
		//
		// 요약:
		//     Shorthand for writing Vector3(0, 0, 0).
		private static readonly Vector3 _zero = new Vector3(0.0f, 0.0f, 0.0f);
		public static Vector3 zero { get { return _zero; } }

		public float this[int index]
		{
			get
			{
				switch (index)
				{
				case 0: return x;
				case 1: return y;
				case 2: return z;
				default: return 0.0f;
				}
			}
			set
			{
				switch (index)
				{
				case 0: x = value; break;
				case 1: y = value; break;
				case 2: z = value; break;
				}
			}
		}

		// 요약:
		//     Returns the angle in degrees between from and to.
		public static float Angle(Vector3 from, Vector3 to)
		{
			Debugger.Break();
			return 0.0f;
		}
		[Obsolete("Use Vector3.Angle instead. AngleBetween uses radians instead of degrees and was deprecated for this reason")]
		public static float AngleBetween(Vector3 from, Vector3 to)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns a copy of vector with its magnitude clamped to maxLength.
		public static Vector3 ClampMagnitude(Vector3 vector, float maxLength)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Cross Product of two vectors.
		public static Vector3 Cross(Vector3 lhs, Vector3 rhs)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Returns the distance between a and b.
		public static float Distance(Vector3 a, Vector3 b)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Dot Product of two vectors.
		public static float Dot(Vector3 lhs, Vector3 rhs)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		public override bool Equals(object other)
		{
			Debugger.Break();
			return false;
		}
		public static Vector3 Exclude(Vector3 excludeThis, Vector3 fromThat)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public override int GetHashCode()
		{
			Debugger.Break();
			return 0;
		}
		//
		// 요약:
		//     Linearly interpolates between two vectors.
		public static Vector3 Lerp(Vector3 from, Vector3 to, float t)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public static float Magnitude(Vector3 a)
		{
			Debugger.Break();
			return 0.0f;
		}
		//
		// 요약:
		//     Returns a vector that is made from the largest components of two vectors.
		public static Vector3 Max(Vector3 lhs, Vector3 rhs)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Returns a vector that is made from the smallest components of two vectors.
		public static Vector3 Min(Vector3 lhs, Vector3 rhs)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Moves a point current in a straight line towards a target point.
		public static Vector3 MoveTowards(Vector3 current, Vector3 target, float maxDistanceDelta)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public void Normalize()
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Makes this vector have a magnitude of 1.
		public static Vector3 Normalize(Vector3 value)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent)
		{
			Debugger.Break();
		}
		public static void OrthoNormalize(ref Vector3 normal, ref Vector3 tangent, ref Vector3 binormal)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Projects a vector onto another vector.
		public static Vector3 Project(Vector3 vector, Vector3 onNormal)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Reflects a vector off the plane defined by a normal.
		public static Vector3 Reflect(Vector3 inDirection, Vector3 inNormal)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Rotates a vector current towards target.
		public static Vector3 RotateTowards(Vector3 current, Vector3 target, float maxRadiansDelta, float maxMagnitudeDelta)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Multiplies two vectors component-wise.
		public void Scale(Vector3 scale)
		{
			Debugger.Break();
		}
		//
		// 요약:
		//     Multiplies two vectors component-wise.
		public static Vector3 Scale(Vector3 a, Vector3 b)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//
		// 요약:
		//     Set x, y and z components of an existing Vector3.
		public void Set(float new_x, float new_y, float new_z)
		{
			x = new_x; y = new_y; z = new_z;
		}
		//
		// 요약:
		//     Spherically interpolates between two vectors.
		public static Vector3 Slerp(Vector3 from, Vector3 to, float t)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		//[ExcludeFromDocs]
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, float maxSpeed)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public static Vector3 SmoothDamp(Vector3 current, Vector3 target, ref Vector3 currentVelocity, float smoothTime, float maxSpeed, float deltaTime)
		{
			Debugger.Break();
			return Vector3.zero;
		}
		public static float SqrMagnitude(Vector3 a)
		{
			Debugger.Break();
			return 0.0f;
		}
		public override string ToString()
		{
			return String.Format("({0},{1},{2})", x, y, z);
		}
		//
		// 요약:
		//     Returns a nicely formatted string for this vector.
		public string ToString(string format)
		{
			return String.Format(format, x, y, z);
		}
	}
}
