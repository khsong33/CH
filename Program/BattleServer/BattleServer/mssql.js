﻿var mssql = require('mssql');
var logger = require('./logger');

var configQA = {
	user: 'saych',
	password: 'saych!!!',
	server: 'dbmsdev1.nwz.kr', 
	port: '14334',
	database: 'CH_KR_GAME',
	pool: { idelTimeout : 30 }
}
var configffdev = {
	user: 'chdb',
	password: 'ffdev123!',
	server: 'ffdev', 
	database: 'CH_gamedb',
	pool: { idleTimeout : 30 }
}
// DB 초기화 및 연결
var connection = null;
var kind_server = process.argv[4];
function InitSql() {
    if (kind_server == 'Local') {
        connection = new mssql.Connection(configffdev, function (err) {
            if (err) {
                console.log("mssql connection err - " + err);
            }
            else {
                console.log("connect success db");
            }
        });
    }
    else if (kind_server == 'QA') {
        connection = new mssql.Connection(configQA, function (err) {
            if (err) {
                console.log("mssql connection err - " + err);
            }
            else {
                console.log("connect success db");
            }
        });
    }
}
exports.InitSql = InitSql;
// 게임 진행 동안 저장된 유저 입력 내용 SQL에 저장(프로시저 : exec SetJSONData @inRoomId, @inJSONData)
function addActionJSon(roomId, jsonData, callback) {
	if (jsonData == null)
		return;
	
	var request = new mssql.Request(connection);
	request.input('inRoomId', roomId);
	request.input('inJSONData', JSON.stringify(jsonData));
	request.execute('SetJSONData', function (err, recordsets, returnValue) {
		if (err) {
			var log = new logger();
			log.error("Failed Excute SetJSONData - " + err);
		}
		else {
			callback(roomId);
		}
	})
}
exports.addActionJSon = addActionJSon;
// 게임 진행 동안 저장된 유저 입력 내용 SQL에 저장(프로시저 : exec SetBattleLog @inRoomId, @inData) - redis를 이용한 저장
function transferBattleLog(roomId, userUniqueIds, battleLogData, battleMatchinfo, callback) {
	var request = new mssql.Request(connection);
	request.input('inRoomId', roomId);
	request.input('inUniqueIds', userUniqueIds);
	request.input('inData', battleLogData);
	request.input('inMatchinfo', battleMatchinfo);
	request.execute('SetBattleLog', function (err, recordsets, returnValue) {
		if (err) {
			var log = new logger();
			log.error("Failed Excute SetBattleLog - " + err);
		}
        else {
            var battle_log_key = recordsets[0][0]["LogKey"];
            var battle_log_time = recordsets[0][0]["LogTime"];
			callback(roomId, battle_log_key, battle_log_time);
		}
	});
}
exports.transferBattleLog = transferBattleLog;

// 저장된 내용 불러오기(프로시저 : exec GetJSONData @inRoomId)
function getActionLog(roomId) {
	var request = new mssql.Request(connection);
	request.input('inRoomId', roomId);
	request.execute('GetJSONData', function (err, recordsets, returnValue) {
		if (err) {
			var log = new logger();
			log.error("Failed Excute GetJSONData");
		}
		var getJson = JSON.parse(recordsets[0][0]["json"]);
		return getJson;
	})
}
exports.getActionLog = getActionLog;

// String.format 처리
if (!String.prototype.format) {
	String.prototype.format = function () {
		var args = arguments;
		return this.replace(/{(\d+)}/g, function (match, number) {
			return typeof args[number] != 'undefined'
        ? args[number]
        : match;
		});
	};
}