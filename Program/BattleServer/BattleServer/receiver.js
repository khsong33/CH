﻿var ticker = require('delta-ticker');
var HashMap = require('hashmap').HashMap;
var logger = require('./logger');
var redisClient = require('./store_redis');
var store = require('./store_redis').store_redis;
var mssql = require('./mssql');
mssql.InitSql();

// 랜덤
var random = require('./random');
random.InitSyncRandom();
// 로그
var log = new logger();
// 프레임 틱 관리
var tickerMap = new HashMap();
exports.tickerMap = tickerMap;
// 패킷 구성
var gameData = new HashMap();
exports.gameData = gameData;
// 방별 핑 통과 처리
var roomPing = new HashMap();
// 핑 확인시 처리
var roomPingCheck = new HashMap();
// 방에서 일어난 입력 저장
//var roomActionLog = new HashMap();
//// 방 생성에 관련된 정보
//var roomNumber = 0;

//// 전체 서버의 방번호 진행사항 확인을 위한 방번호 입력
//function setRoomNumber(RoomNum) {
//    roomNumber = RoomNum;
//}
//exports.setRoomNuber = setRoomNumber;
// 각 방별 핑 확인
function setRoomPing(roomId, isReady) {
    roomPing.set(roomId, isReady);
}
exports.setRoomPing = setRoomPing;

// 접속 확인 요청 처리
function on_connection(io, socket, data) {
    var user_find_lobby_sockid = 'user:' + data.UserKey;
    redisClient.store_redis.get(user_find_lobby_sockid, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        var set_battle_socketid_uniqueid = 'bsu:' + socket.id;
        redisClient.store_redis.set(set_battle_socketid_uniqueid, Number(userInfoJson.UniqueId), function (err) {
            socket.emit('sc_connection');
        });
    });
}
exports.on_connection = on_connection;
// 대전 상대가 모두 들어왔을 경우 게임을 시작
function game_match(io, socket, data) {
    var find_roomKey = 'userroom:' + data.UniqueId;
    store.get(find_roomKey, function (err, roomInfo) {
        var roomInfoJSON = JSON.parse(roomInfo);
        var roomId = roomInfoJSON.roomId;
        var isAI = roomInfoJSON.AI;
        //console.log("roomNumber - " + roomId);
        socket.join(String(roomId));
        socket.room = String(roomId);
        socket.uniqueId = Number(data.UniqueId);
        
        setJoinRoom(io, String(roomId), socket);
        
        redisClient.sub.subscribe("packet:" + roomId);
        redisClient.sub.subscribe("tickready:" + roomId);
        
        var ai_user_count_key = 'aiusercount:' + roomId;
        var find_room_user_key = 'room:' + roomId; // 로비서버에서 접속한 방인원
        var battle_room_join_key = 'battleroom:' + roomId; // 실제 배틀 서버에서 접속 완료한 인원(실제 게임 인원)
        var real_check_count = 0;
        store.get(ai_user_count_key, function (err, ai_user_count) {
            store.scard(find_room_user_key, function (err, max_battle_count) {
                real_check_count = max_battle_count - ai_user_count;
                store.scard(battle_room_join_key, function (err, join_count) {
                    // 모두 접속 완료 했을 경우에만 정보를 서버에서 클라이언트로 전송합니다.
                    if (join_count >= real_check_count) {
                        store.smembers(find_room_user_key, function (err, room_users) {
                            var game_matchinfo = {
                                'UserInfo' : [],
                                'RandomIndex' : random.randomRange(0, 16),
                                'RandomState' : [],
                                'StageId' : Number(1) // 임시로 1만 보내줍니다.
                            }
                            for (var iState = 0; iState < 16; iState++) {
                                game_matchinfo.RandomState.push(random.randomRangeInc(0, 9999));
                            }
                            random.SyncRandomNotOverlap(0, max_battle_count, function (user_idx_random, random_size) {
                                for (var index = 0; index < max_battle_count; index++) {
                                    var user_room_find_key = 'userroom:' + room_users[index];
                                    redisClient.store_redis.get(user_room_find_key, function (err, roominfo) {
                                        var roominfoJSON = JSON.parse(roominfo);
                                        if (roominfoJSON.AI == false) {
                                            var user_info_from_uid_key = 'unique:' + roominfoJSON.UniqueId;
                                            redisClient.store_redis.get(user_info_from_uid_key, function (err, userdata) {
                                                var userdataJson = JSON.parse(userdata);
                                                userdataJson.Ai = 'FALSE';
                                                userdataJson.UserIdx = Number(user_idx_random[game_matchinfo.UserInfo.length]);
                                                console.log("send match info name - " + userdataJson.UserName + "/user idx - " + userdataJson.UserIdx);
                                                game_matchinfo.UserInfo.push(userdataJson);
                                                if (game_matchinfo.UserInfo.length >= max_battle_count) {
                                                    var set_battle_matchinfo = 'logmatchinfo:' + roomId;
                                                    redisClient.battle_log_redis.set(set_battle_matchinfo, JSON.stringify(game_matchinfo));
                                                    io.sockets.to(socket.room).emit('sc_match_info', game_matchinfo);
                                                }
                                            });
                                        }
                                        else {
                                            var aiUserFindKey = 'aiUseUser:' + roominfoJSON.UniqueId;
                                            redisClient.ai_user_redis.get(aiUserFindKey, function (err, userdata) {
                                                var userdataJson = JSON.parse(userdata);
                                                userdataJson.Ai = 'TRUE';
                                                userdataJson.UserIdx = Number(user_idx_random[game_matchinfo.UserInfo.length]);
                                                console.log("send match info name - " + userdataJson.UserName + "/user idx - " + userdataJson.UserIdx);
                                                game_matchinfo.UserInfo.push(userdataJson);
                                                if (game_matchinfo.UserInfo.length >= max_battle_count) {
                                                    var set_battle_matchinfo = 'logmatchinfo:' + roomId;
                                                    redisClient.battle_log_redis.set(set_battle_matchinfo, JSON.stringify(game_matchinfo));
                                                    io.sockets.to(socket.room).emit('sc_match_info', game_matchinfo);
                                                } // end if
                                                redisClient.store_redis.del('userroom:' + userdataJson.UniqueId);
                                            }); // end redis get userfind from uniqueid
                                        } // end else
                                    }); // end redis get room find key
                                } // end for
                            }); // end synd random not overlay
                        });
                    }
                });
            });

        });

    });
}
exports.game_match = game_match;

// 배틀 서버 접속 후 개인 방에 대한 정보 요청
// 모든 유저가 게임 시작 준비가 완료 되었을 경우 프로세스 시작
function ready_to_play(io, socket, data) {
    var find_room_user_key = 'room:' + socket.room; // 로비서버에서 접속한 방인원
    var real_ready_count = 0;
    redisClient.store_redis.get('aiusercount:' + socket.room, function (err, ai_user_count) {
        redisClient.store_redis.scard(find_room_user_key, function (err, max_battle_count) {
            real_ready_count = max_battle_count - ai_user_count;
            redisClient.store_redis.incr("ready:" + socket.room, function (err, count) {
                if (count >= real_ready_count) {
                    store.get("ticker:" + socket.room, function (err, replyTick) {
                        if (replyTick == null) {
                            store.set("ticker:" + socket.room, true);
                            game_frame_update(io, socket);
                            io.sockets.to(socket.room).emit('sc_start_play');
                            log.info("ticker On process pid - " + process.pid);
                            store.del("ready:" + socket.room);
                        }
                    });
                }
            });
        });
    });
}
exports.ready_to_play = ready_to_play;

// 클라이언트에서의 입력 처리
function on_game_ping(io, socket, data) {
    var find_battle_user_uniqueid = 'bsu:' + socket.id;
    redisClient.store_redis.get(find_battle_user_uniqueid, function (err, uniqueId) {
        for (var iInput = 0; iInput < data.InputCount; iInput++) {
            var lInputData = {
                'Header' : "Input",
                'Data': data.PingData[iInput].Data,
                'UniqueId': uniqueId
            };
            _createPacket(socket, lInputData);
        }
    });
    // 틱보다 먼저 먼저 들어오면 안되는 부분.
    store.incr("ping:" + socket.room, function (err, value) {
        var find_room_user_key = 'room:' + socket.room; // 로비서버에서 접속한 방인원
        var find_ai_user_count = 'aiusercount:' + socket.room;
        store.scard(find_room_user_key, function (err, room_max_count) {
            store.get(find_ai_user_count, function (err, ai_user_count) {
                if (value >= room_max_count - ai_user_count) {
                    store.set("ping:" + socket.room, 0);
                    redisClient.pub.publish("tickready:" + socket.room, true);
                }
            });
        })
    });
}
exports.on_game_ping = on_game_ping;

// 게임 종료
function destroy_game(io, socket) {
    //console.log("destroy socket id - " + socket.id);
    io.sockets.to(socket.room).emit('sc_battle_quit');
    socket.leave(socket.room);
    
    //removeJoinRoom(socket);
    var lTicker = tickerMap.get(socket.room);
    if (lTicker != null) {
        lTicker.stop();
        tickerMap.remove(socket.room);
    }
    redisClient.store_redis.del('socketid:' + socket.id);
    redisClient.store_redis.del('bsu:' + socket.id);
    redisClient.store_redis.del('ticker:' + socket.room);
    redisClient.store_redis.del('frame:' + socket.room);
    redisClient.store_redis.del('ping:' + socket.room);
    
    redisClient.sub.unsubscribe('packet:' + socket.room);
    redisClient.sub.unsubscribe('tickready:' + socket.room);
    
    // 방정보 파기
    redisClient.store_redis.del('battleroom:' + socket.room);
    
    // battle log 처리 확인
    redisClient.store_redis.del('dblogging:' + socket.room);
    
    //var actionLog = roomActionLog.get(socket.room);
    // mssql.addActionJSon(socket.room, actionLog, _endGame);
    //setDB_BattleLog(io, socket, socket.room);
    //roomActionLog.remove(socket.room);
}
exports.destroy_game = destroy_game;

// 전투 종료
function end_battle(io, socket, data) {
    // 임시 ECHO 서버로 처리
    var complete_battle = {
        'UniqueId' : Number(data.UniqueId),
        'BattleResult' : Number(data.BattleCode)
    }
    _updateEndBattle(data.UniqueId, data.BattleCode);
    // 임시 ECHO 서버 처리 끝
    
    // 실제로는 아래 처리를 진행 
    // setDB_BattleLog에서 게임 기록을 DB에 저장후 검증 요청을 검증프로그램에 요청 및 subscribe를 통해 게임 확인 후
    // 게임 승패 정보를 room에 있는 유저들에게 전송 패킷 이름 
    // 패킷 이름 sc_battle_end || 데이터 { 'Win' : [teamid], 'Lose' : [teamid] }
    
    var db_battle_log_transfer = 'dblogging:' + socket.room;
    redisClient.store_redis.exists(db_battle_log_transfer, function (err, key_count) {
        if (key_count <= 0) {
            redisClient.store_redis.set(db_battle_log_transfer, Boolean(true));
            redisClient.sub.subscribe("check_battle:" + socket.room);
            _setDBBattleLogAndVerifyBattle(io, socket, socket.room, function (endGameInfo) {
                var battle_end_data = {
                    'UniqueId' : Number(data.UniqueId),
                    'BattleResult' : Number(data.BattleCode),
                    'LogKey' : String(endGameInfo.logkey),
                    'LogTime' : String(endGameInfo.logtime)
                }
                // sc_battle_end 패킷은 검증이 끝나는 위치에서 보내준다.
                io.sockets.to(endGameInfo.room).emit('sc_battle_end', battle_end_data);
            });
        }
    });
    //roomActionLog.remove(socket.room);
}
exports.end_battle = end_battle;
// 전투 종료 후 전투 관련 유저정보 redis 갱신
function _updateEndBattle(uniqueId, battle_code) {
    var find_user_uniqueid = 'unique:' + uniqueId;
    redisClient.store_redis.get(find_user_uniqueid, function (err, userinfo) {
        var userinfo_json = JSON.parse(userinfo);
        if (battle_code == 0) {
            // win
            userinfo_json.Win = userinfo_json.Win + 1;
            userinfo_json.WinStreak = userinfo_json.WinStreak + 1;
        }
        else if (battle_code == 1) {
            // lose
            userinfo_json.Lose = userinfo_json.Lose + 1;
            userinfo_json.WinStreak = 0;
        }
        var find_user_uniqueid = 'unique:' + userinfo_json.UniqueId;
        redisClient.store_redis.set(find_user_uniqueid, JSON.stringify(userinfo_json));
        var find_user_socketid = 'user:' + userinfo_json.UserKey;
        redisClient.store_redis.set(find_user_socketid, JSON.stringify(userinfo_json));
    });
}

// 전투 종료 데이터 검증 요청
function _endGame(roomId) {
    var endGameInfo = {
        'room' : roomId
    }
    redisClient.pub.publish("endgame", JSON.stringify(endGameInfo)); // 상용 코드
}
// 전투 종료 확인 완료
function CheckEndBattle(io, roomId, data) {
    var complete_battle = {
        'BattleResult' : Number(data.BattleCode),
        'Result' : data.IsValidResult
    }
    
    // reward는 redis에 저장된 값을 lobby server를 통해서 전송합니다.
    io.sockets.to(roomId).emit('sc_battle_end', complete_battle);
    redisClient.sub.unsubscribe("check_battle:" + roomId);
    log.info("Checking End Battle Room - " + roomId);
}
exports.CheckEndBattle = CheckEndBattle;
// 패킷 제작
function _createPacket(socket, data) {
    redisClient.pub.publish("packet:" + socket.room, JSON.stringify(data));
}
// 패킷 준비 데이터 제작
function createPacket(roomId, data) {
    var packetData = JSON.parse(data.toString());
    if (gameData.has(roomId)) {
        var lData = gameData.get(roomId);
        lData.Datas.push(packetData);
    }
    else {
        var lData = { Datas: [] };
        lData.Datas.push(packetData);
        gameData.set(roomId, lData);
    }
}
exports.createPacket = createPacket;

// tick 전송(프레임 업데이트)
function game_frame_update(io, socket) {
    tickerMap.set(socket.room, (ticker({
            task : function () {
                var lRoomPing = roomPing.get(socket.room)
                if (!roomPing.has(socket.room) || lRoomPing)/**/ {
                    store.incr("frame:" + socket.room, function (err, frame) {
                        var roomFrameCount = Number(frame);
                        var lPacketData = gameData.get(socket.room);
                        if (lPacketData == null) {
                            lPacketData = { "Frame" : roomFrameCount };
                        }
                        else {
                            lPacketData["Frame"] = roomFrameCount;
                        }
                        if (lPacketData.Datas != null) {
                            //setRoomActionLog(socket.room, roomFrameCount, lPacketData.Datas);
                            for (var iData = 0; iData < lPacketData.Datas.length; iData++) {
                                setBattleLog(socket.room, roomFrameCount, lPacketData.Datas[iData].Data, lPacketData.Datas[iData].UniqueId);
                            }
                        }
                        io.to(socket.room).emit('sc_recv_tick', lPacketData);
                        gameData.remove(socket.room);
                        roomPing.set(socket.room, false);
                    })
                }
            },
            delay : 300
        }).start()));
}

// 방 접속
var setJoinRoom = function (io, roomId, socket) {
    var battleRoomKey = "battleroom:" + roomId;
    store.sadd(battleRoomKey, socket.id);
}
// 방 탈퇴
var removeJoinRoom = function (socket) {
    store.srem(socket.room, socket.id);
}
// 전투중 일어난 행동 로그 redis에 저장
function setBattleLog(roomId, roomFrame, data, uniqueId) {
    //redisClient.store_redis.get('bsu:' + socket
    var battleLogKey = 'battlelog:' + roomId;
    var saveData = roomFrame + ':' + data + '/' + uniqueId;
    redisClient.battle_log_redis.lpush(battleLogKey, saveData, function (err, count) {
        if (err)
            log.error(err);
    });
}
function _setDBBattleLogAndVerifyBattle(io, socket, roomId, cb_endgame) {
    var find_room_user_key = 'room:' + roomId;
    var battleLogKey = 'battlelog:' + roomId;
    var user_unique_ids = '';
    redisClient.store_redis.smembers(find_room_user_key, function (err, room_users) {
        for (var iUser = 0; iUser < room_users.length; iUser++) {
            user_unique_ids += room_users[iUser] + ',';
        }
        redisClient.store_redis.del(find_room_user_key);
        redisClient.battle_log_redis.lrange(battleLogKey, 0, -1, function (err, datas) {
            if (err) {
                log.error(err);
            }
            else {
                var insertData = '';
                for (var iData = 0; iData < datas.length; iData++) {
                    insertData += datas[iData] + ',';
                }
                var find_battle_matchinfo = 'logmatchinfo:' + roomId;
                redisClient.battle_log_redis.get(find_battle_matchinfo, function (err, insertMatchinfo) {
                    mssql.transferBattleLog(roomId, user_unique_ids, insertData, insertMatchinfo, function (endRoomId, battle_log_key, battle_log_time) {
                        var endGameInfo = {
                            'room' : String(endRoomId)
                        }
                        var endGameDBInfo = {
                            'room' : String(endRoomId),
                            'logkey' : String(battle_log_key),
                            'logtime' : String(battle_log_time.toISOString())
                        }
                        redisClient.pub.publish("endgame", JSON.stringify(endGameInfo)); // 상용 코드
                        cb_endgame(endGameDBInfo);
                    });
                });
            }
        }); // end of battle_log_redis.lrange 
    }); // end of store_redis.smembers
}
// 방에서 일어난 액션 입력 저장
//var setRoomActionLog = function (roomId, roomFrame, action) {
//    if (roomActionLog.has(roomId)) {
//        var actionLog = roomActionLog.get(roomId);
//        var frameActionLog = {
//            'frame' : roomFrame,
//            'input' : action
//        }
//        actionLog.logs.push(frameActionLog);
//        roomActionLog.set(roomId, actionLog);
//    }
//    else {
//        var frameActionLog = {
//            'frame' : roomFrame,
//            'input' : action
//        }

//        var actionLog = {
//            'logs' : []
//        }
//        actionLog.logs.push(frameActionLog);
//        roomActionLog.set(roomId, actionLog);
//    }
//}