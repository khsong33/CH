﻿var cluster = require('cluster');
var numCpus = require('os').cpus().length;
var sio = require('socket.io');
//var redis = require('redis');
var redisClient = require('./store_redis');
var receiver = require('./receiver');
var logger = require('./logger');
var store = require('./store_redis').store_redis;

var port = 6265;
var BattleClientId = 0;
var isUseCluster = (process.argv[2] == '-usecluster');
var custom_port = process.argv[3];

if (cluster.isMaster && isUseCluster) {
    
    process.title = "Combat Battle Server";
	var log = new logger();
    
    log.info("Server Start : cluster mode");
	for (var i = 0; i < numCpus; i++) {
		cluster.fork();
	}
	cluster.on('online', function (worker) {
		log.info("Worker " + worker.process.pid + " is online.");
	});
	cluster.on('exit', function (worker, code, signal) {
        log.info("worker " + worker.process.pid + " died.");
        cluster.fork();
    });

}
else {
	var log = new logger();
	
	if (!isUseCluster) {
		log.info("Battle Server Start : none cluster mode");
	}
	
	var io = sio.listen(custom_port);
    redisClient.initRedis(io);
    redisClient.initCallback(cbSubMessage);
    
    //_RegistBattleServer();
	
    io.on('connection', function (socket) {
		log.info("connect process pid - " + process.pid + "/ connection socket id - " + socket.id);
		// 연결 종료
		socket.on('disconnect', function () {
			log.info("disconnect socket id - " + socket.id);
			receiver.destroy_game(io, socket);
		})
		// 접속 승인
		socket.on('cs_connection', function (data) {
			receiver.on_connection(io, socket, data);
		});
		// 상대편 데이터 전송
		socket.on('cs_match_info', function (data) {
			receiver.game_match(io, socket, data);
		})
		// 게임 준비 완료(전투 시작)
		socket.on('cs_ready_to_play', function (data) {
			receiver.ready_to_play(io, socket, data);
		})
		// 클라이언트 게임 입력
		socket.on('cs_frame_ping', function (data) {
			receiver.on_game_ping(io, socket, data);
		})
		// 전투 종료
		socket.on('cs_battle_end', function (data) {
			receiver.end_battle(io, socket, data);
		})
	})
	// redis subscribe callback 처리
	function cbSubMessage(channel, message) {
		var arr = channel.split(':');
		if (arr[0] == "packet") {
			receiver.createPacket(arr[1], message);
		}
		else if (arr[0] == "tickready") {
			receiver.setRoomPing(arr[1], true);
		}
		else if (arr[0] == "check_battle") {
			receiver.CheckEndBattle(io, arr[1], message);
		}
	}
}// end of slave process
// 예외처리
process.on('uncaughtException', function (err) {
	var log = new logger();
	log.error('Caught exception: ' + err);
});
// 종료 처리
process.on('exit', function (code) {
	var log = new logger();
	log.error("closed server process id - " + process.pid);
});

function _RegistBattleServer() {
    var os = require('os');
    var ifaces = os.networkInterfaces();    
    
    Object.keys(ifaces).forEach(function (ifname) {
        ifaces[ifname].forEach(function (iface) {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }
            var lConnectionAddress = "ws://" + iface.address + ":" + custom_port + "/socket.io/?EIO=4&transport=websocket";
            redisClient.store_redis.incr("battleCount", function (err, battle_count) {
                redisClient.store_redis.hmset('battle', battle_count, lConnectionAddress);
            });
            //console.log("connection address - " + lConnetionAddress);

            // TODO :: Redis에 주소를 입력합니다.
        });
    });

}


