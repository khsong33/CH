﻿var socketio_redis = require('socket.io-redis');
var redis = require('redis');

var redis_ip = 'localhost';
var redis_port = 38300;

// data store redis setting!
var store_redis = redis.createClient(redis_port, redis_ip, {});
exports.store_redis = store_redis;
var ai_user_redis = redis.createClient(redis_port, redis_ip, {});
exports.ai_user_redis = ai_user_redis;
// only battle server
var battle_log_redis = redis.createClient(redis_port, redis_ip, {});
exports.battle_log_redis = battle_log_redis;

// subscriber에 return_buffers가 없을 경우 trailing byte error가 발생
var sub = redis.createClient(redis_port, redis_ip, { detect_buffers: true });
var pub = redis.createClient(redis_port, redis_ip, {});
exports.sub = sub;
exports.pub = pub;
var global_callback = null;
function initRedis(io) {
	io.adapter(socketio_redis({
			host: redis_ip, port: redis_port ,
			pubClient : pub,
			subClient : sub
		}));
	
	store_redis.select(0, function (err) { });
	ai_user_redis.select(1, function (err) { });
	battle_log_redis.select(2, function (err) { });
}
exports.initRedis = initRedis;

function initCallback(callback) {
	global_callback = callback;
}
exports.initCallback = initCallback;

sub.on('message', function (channel, message) {
	if (global_callback != null) {
		global_callback(channel, message);
	}
});
