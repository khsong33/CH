﻿var path = require('path');
var winston = require('winston');
module.exports = function () {
    transportsFile = [];
    transportsFile.push(new winston.transports.DailyRotateFile({
        name : 'file:log',
          level : 'debug',
          json: false,
          filename: path.join(__dirname, "logs", "Log-"),
          datePattern: 'yyyy-MM-dd.log',
    }));
    transportsFile.push(new winston.transports.Console({
        level : 'info',
        colorize: true
    }));
    var logger = new winston.Logger({
        transports: transportsFile
    });
    
    return logger;
};