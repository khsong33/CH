﻿var util = require('util');
var sio = require('socket.io');
var redisClient = require('./redis_store');
var logger = require('./logger');
var log = new logger();
var receiver = require('./receiver');
var serverconfig = require('./serverconfig').getServerConfig();

var custom_port = serverconfig.port;

var io = sio.listen(custom_port);
redisClient.initRedis();
redisClient.initCallback(CallbackSubscribe);

log.info("Start Battle Server");

redisClient.battle_sub.subscribe("game_result");

io.on('connection', function (socket) {
    log.info("Client Connection - " + socket.id);
    socket.emit('check');
    // 접속 종료
    socket.on('disconnect', function () {
        log.info("Client Disconnect - " + socket.id);
        receiver.leave_game(io, socket);
    });
    // 접속 승인
    socket.on('cs_battle_connection', function (data) {
        receiver.on_battle_connect(io, socket, data);
    });
    // 게임 준비 완료(전투 시작)
    socket.on('cs_ready_to_play', function (data) {
        receiver.ready_to_play(io, socket, data);
    })
    // 클라이언트 게임 입력
    socket.on('cs_frame_ping', function (data) {
        receiver.on_game_ping(io, socket, data);
    })
    // 전투 종료
    socket.on('cs_battle_end', function (data) {
        receiver.on_battle_end(io, socket, data);
    })
});

_RegistBattleServer();

// 예외처리
process.on('uncaughtException', function (err) {
    var log = new logger();
    log.error('Caught exception: ' + err);
});

function _RegistBattleServer() {
    var os = require('os');
    
    var interfaces = os.networkInterfaces();
    for (var k in interfaces) {
        for (var k2 in interfaces[k]) {
            var address = interfaces[k][k2];
            if (address.family === 'IPv4' && !address.internal) {
                var log = new logger();
                log.info("Registe battle server - " + address.address + ":" + custom_port);
                var lConnectionAddress = "ws://" + address.address + ":" + custom_port + "/socket.io/?EIO=4&transport=websocket";
                var lConnectionAddressKey = address.address + ',' + custom_port;
                redisClient.match_redis.hmset('battle_server', lConnectionAddressKey, lConnectionAddress);
                var checker_data = {
                    'ip' : address.address,
                    'port' : custom_port,
                    'key' : lConnectionAddressKey
                }
                redisClient.match_redis.publish('battle_serv_reg', JSON.stringify(checker_data));
            }
        }
    }
}

// redis 구독 처리
function CallbackSubscribe(channel, message) {
    console.log("[" + channel + "] - " + message);
    if (channel == "game_result") {
        var end_game_json = JSON.parse(message);
        var roomId = end_game_json.EndRoom;
        console.log('game_result - ' + roomId);
        var end_data = end_game_json.EndData;
        var reward_data = end_game_json.EndReward;
        var reward_set_count = 0;
        for (var ireward = 0; ireward < reward_data.length; ireward++) {
            redisClient.match_redis.set('battlereward:' + reward_data[ireward].UniqueId, JSON.stringify(reward_data[ireward]), function (err) {
                reward_set_count++;
                if (reward_set_count >= reward_data.length) {
                    receiver.send_end_game(io, roomId, end_data);
                }
            });
        }
    }
}