﻿var mssql = require('mssql');
var serverconfig = require('./serverconfig').getServerConfig();

var connection = null;
var dbconfig = serverconfig.store_mssql;
function InitSql(cb_connect) {
    connection = new mssql.Connection(dbconfig, function (err) {
        if (err) {
            console.log("mssql connection err - " + err);
        }
        else {
            console.log("connect success db - " + dbconfig.server);
            cb_connect();
        }
    });
}
exports.InitSql = InitSql;

// 게임 진행 동안 저장된 유저 입력 내용 SQL에 저장(프로시저 : exec SetBattleLog @inRoomId, @inData) - redis를 이용한 저장
function TransferBattleLog(roomId, userUniqueIds, battleLogData, battleMatchinfo, callback) {
    var request = new mssql.Request(connection);
    request.input('inRoomId', roomId);
    request.input('inUniqueIds', userUniqueIds);
    request.input('inData', battleLogData);
    request.input('inMatchinfo', battleMatchinfo);
    request.execute('SetBattleLog', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute SetBattleLog - " + err);
        }
        else {
            var battle_log_key = recordsets[0][0]["LogKey"];
            var battle_log_time = recordsets[0][0]["LogTime"];
            callback(roomId, battle_log_key, battle_log_time);
        }
    });
}
exports.TransferBattleLog = TransferBattleLog;