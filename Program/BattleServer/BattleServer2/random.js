﻿// exclusive max value
function randomRange(min, max) {
    return Math.floor(Math.random() * (max - min) + min);
}
exports.randomRange = randomRange;

// inclusive max value;
function randomRangeInc(min, max) {
    return Math.floor(Math.random() * (max - min + 1) + min);
}
exports.randomRangeInc = randomRangeInc;

// WELL512 Sync random algorithm
var mState = new Array();
var mIndex;

// initialized sync random seed and state value
function InitSyncRandom(index, state) {
    mIndex = index;
    for (var iState = 0; iState < state.length; iState++) {
        mState.push(state[iState]);
    }
}
function InitSyncRandom(index) {
    mIndex = index;
    for (var iState = 0; iState < 16; iState++) {
        mState.push(randomRangeInc(1, 9999));
    }
}
function InitSyncRandom() {
    mIndex = randomRangeInc(1, 9999);
    for (var iState = 0; iState < 16; iState++) {
        mState.push(randomRangeInc(1, 9999));
    }
}
exports.InitSyncRandom = InitSyncRandom;

// max exclusive sync random function
function SyncRandomRange(min, max) {
    return Math.floor(SyncRandom() % (max - min) + min);
}
exports.SyncRandomRange = SyncRandomRange;

// max inclusive sync random function
function SyncRandomRangeInc(min, max) {
    return Math.floor(SyncRandom() % (max - min) + min + 1);
}
exports.SyncRandomRangeInc = SyncRandomRangeInc;

// Sync random construct function
function SyncRandom() {
    var a, b, c, d;
    
    a = mState[mIndex];
    c = mState[(mIndex + 13) & 15];
    b = a ^ c ^ (a << 16) ^ (c << 15);
    c = mState[(mIndex + 9) & 15];
    c ^= (c >> 11);
    a = mState[mIndex] = b ^ c;
    d = a ^ ((a << 5) & 0xda442d24);
    mIndex = (mIndex + 15) & 15;
    a = mState[mIndex];
    mState[mIndex] = a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);
    
    return mState[mIndex] >>> 0; // convert var to unsinged int 32
}
// Not include max value random. please insert max:var more than max value
function SyncRandomNotOverlap(min, max, callback_result) {
    var random_size = Math.abs(max - min);
    var result_random_index = new Array();
    
    for (var i = 0; i < random_size; i++) {
        result_random_index.push(i + min);
    }
    
    for (var i = 0; i < random_size; i++) {
        var lRandomIndex = SyncRandomRange(min, max);
        var lTempValue = result_random_index[i];
        if (lRandomIndex == i)
            continue;
        
        result_random_index[i] = result_random_index[lRandomIndex];
        result_random_index[lRandomIndex] = lTempValue;
    }
    callback_result(result_random_index, random_size)
}
exports.SyncRandomNotOverlap = SyncRandomNotOverlap;