﻿var redisClient = require('./redis_store');
var mssql = require('./mssql');
mssql.InitSql(function () {

});
// 랜덤
var random = require('./random');
random.InitSyncRandom();

var match_room_user_count = {};
var match_room_ping_count = {};
var match_input_data = {};
var match_interval = {};
var match_frame = {};
var battle_socketid_uniqueid = {};
var battle_end_check = {};
var save_battle_log = {};

// 전투 서버 접속
function on_battle_connect(io, socket, data) {
    var room_key = String(data.RoomKey);
    // 전투 방 정보를 셋팅
    socket.join(room_key);
    socket.room = room_key;
    save_battle_log[room_key] = true; // 기본적으론 모든 로그는 저장됩니다.
    if (!match_room_user_count.hasOwnProperty(room_key)) {
        var find_room_info_key = 'room:' + room_key;
        redisClient.match_redis.get(find_room_info_key, function (err, match_info) {
            var match_info_json = JSON.parse(match_info);
            var match_play_count = 0;
            for (var iuser = 0; iuser < match_info_json.UserInfo.length; iuser++) {
                // UniqueId가 0인 경우는 캠페인 유저입니다.
                if (match_info_json.UserInfo[iuser].Ai == 'FALSE' && match_info_json.UserInfo[iuser].UniqueId > 0) {
                    match_play_count++;
                }
                // 캠페인은 로그가 저장되지 않습니다. (현재는 튜토리얼만 적용)
                if(match_info_json.UserInfo[iuser].UniqueId == 0) { // 0은 캠페인 유저
                    save_battle_log[room_key] = true;  // 튜토리얼도 저장합니다.
                }
                var user_key = String(data.UserKey);
                if (match_info_json.UserInfo[iuser].UserKey == user_key) {
                    battle_socketid_uniqueid[socket.id] = match_info_json.UserInfo[iuser].UniqueId;
                }
            }
            match_room_user_count[room_key] = Number(match_play_count);
            if (save_battle_log[room_key] == true) {
                redisClient.battle_log_redis.set('matchinfo:' + room_key, match_info);
            }
            socket.emit('sc_battle_connection');
        });
    }
    else {
        var find_room_info_key = 'room:' + room_key;
        redisClient.match_redis.get(find_room_info_key, function (err, match_info) {
            var match_info_json = JSON.parse(match_info);
            for (var iuser = 0; iuser < match_info_json.UserInfo.length; iuser++) {
                var user_key = String(data.UserKey);
                if (match_info_json.UserInfo[iuser].UserKey == user_key) {
                    battle_socketid_uniqueid[socket.id] = match_info_json.UserInfo[iuser].UniqueId;
                }
            }
            socket.emit('sc_battle_connection');
        })
    }
}
exports.on_battle_connect = on_battle_connect;
// 전투 서버 접속 종료
function destroy_game(io, socket) {
    delete battle_socketid_uniqueid[socket.id];
    delete match_room_user_count[socket.room];
    delete match_room_ping_count[socket.room];
    delete match_input_data[socket.room];
    delete match_frame[socket.room];
    delete battle_end_check[socket.room];

    clearInterval(match_interval[socket.room]);
    delete match_interval[socket.room];

    // match redis의 room 정보 삭제
    var del_match_room_info = 'room:' + socket.room;
    redisClient.match_redis.del(del_match_room_info);
}
exports.destroy_game = destroy_game;
// 전투 서버 떠남
function leave_game(io, socket) {
    match_room_user_count[socket.room] = match_room_user_count[socket.room] - 1;
    if (match_room_user_count[socket.room] <= 0) {
        destroy_game(io, socket);
    }
    else {
        io.sockets.to(socket.room).emit('sc_leave_user', {'UserKey' : String(socket.id)});
        // 유저가 떠났는데 방이 파괴 안되는 경우 처리
    }
}
exports.leave_game = leave_game;
// 게임 시작
function ready_to_play(io, socket, data) {
    if (!match_room_ping_count.hasOwnProperty(socket.room)) {
        match_room_ping_count[socket.room] = Number(1);
        battle_end_check[socket.room] = Number(0);
    }
    else {
        match_room_ping_count[socket.room] = match_room_ping_count[socket.room] + 1;
    }
    
    if (!match_input_data.hasOwnProperty(socket.room)) {
        match_input_data[socket.room] = {
            'Frame' : Number(0),
            'Datas' : []
        };
    }

    if (match_room_user_count[socket.room] <= match_room_ping_count[socket.room]) {
        _add_battle_interval(io, socket.room);
        io.sockets.to(socket.room).emit('sc_start_play');
    }
}
exports.ready_to_play = ready_to_play;
// 클라이언트에서 입력이 오면 처리합니다.
function on_game_ping(io, socket, data) {
    for (var iInput = 0; iInput < data.InputCount; iInput++) {
        var input_data = {
            'Header' : "Input",
            'Data': data.PingData[iInput].Data
        };
        match_input_data[socket.room].Datas.push(input_data);
        // 로그 입력
        if (save_battle_log[socket.room] == true) {
            _set_battle_log(socket.room, 
            match_frame[socket.room] + 1, 
            input_data.Data, 
            battle_socketid_uniqueid[socket.id]
            );
        }
    }
    match_room_ping_count[socket.room] = match_room_ping_count[socket.room] + 1;
}
exports.on_game_ping = on_game_ping;
// 전투 종료
function on_battle_end(io, socket, data) {
    battle_end_check[socket.room]++;
    if (battle_end_check[socket.room] >= match_room_user_count[socket.room]) {
        var battle_end_json = {
            'RoomId' : String(socket.room),
            'UserKey' : String(data.UserKey),
            'BattleCode' : Number(data.BattleCode)
        };
        redisClient.battle_pub.publish('endgame', JSON.stringify(battle_end_json));
    }
}
exports.on_battle_end = on_battle_end;
// 전투 종료 전송
function send_end_game(io, roomId, end_data) {
    // 이곳에서 로그 생성
    if (save_battle_log[roomId] == true) { // 로그 전송
        var find_battle_log_key = 'battlelog:' + roomId;
        redisClient.battle_log_redis.lrange(find_battle_log_key, 0, -1, function (err, battle_log) {
            var find_match_info_key = 'matchinfo:' + roomId;
            redisClient.battle_log_redis.get(find_match_info_key, function (err, match_info) {
                var db_log_datas = '';
                for (var ilog = 0; ilog < battle_log.length; ilog++) {
                    db_log_datas += battle_log[ilog] + ',';
                }
                var db_log_matchinfo = match_info;
                
                var matchinfo_json = JSON.parse(match_info);
                var db_unique_ids = '';
                for (var iuser = 0; iuser < matchinfo_json.UserInfo.length; iuser++) {
                    db_unique_ids += matchinfo_json.UserInfo[iuser].UniqueId + ',';
                }
                mssql.TransferBattleLog(roomId, db_unique_ids, db_log_datas, db_log_matchinfo, function (endRoomId, battle_log_key, battle_log_time) {
                    end_data['LogKey'] = String(battle_log_key);
                    end_data['LogTime'] = String(battle_log_time.toISOString());
                    io.sockets.to(roomId).emit('sc_battle_end2', end_data);
                });
            });
        });
    }
    else  {// 로그 비 전송(캠페인, 튜토리얼)
        io.sockets.to(roomId).emit('sc_battle_end2', end_data);
    }
}
exports.send_end_game = send_end_game;

// 전투 인터벌 등록
function _add_battle_interval(io, roomId) {
    match_frame[roomId] = Number(0);
    match_interval[roomId] = setInterval(function () { _interval_room(io, roomId)}, 300);
}
// 전투별 인터벌
function _interval_room(io, roomId) {
    if (match_room_ping_count[roomId] == match_room_user_count[roomId]) {
        match_frame[roomId] = match_frame[roomId] + 1;
        match_input_data[roomId]["Frame"] = match_frame[roomId]; // frame을 추가
        var frame_input_data = match_input_data[roomId];
        io.sockets.to(roomId).emit('sc_recv_tick', frame_input_data);
        match_room_ping_count[roomId] = 0;
        if (match_input_data[roomId]["Datas"].length > 0) {
            match_input_data[roomId]["Datas"] = [];
        }
    }
}

// 전투중 일어난 행동 로그 redis에 저장
function _set_battle_log(roomId, roomFrame, data, uniqueId) {
    var battleLogKey = 'battlelog:' + roomId;
    var saveData = roomFrame + ':' + data + '/' + uniqueId;
    redisClient.battle_log_redis.lpush(battleLogKey, saveData, function (err, count) {
        if (err)
            log.error(err);
    });
}
