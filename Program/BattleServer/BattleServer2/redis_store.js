﻿var redis = require('redis');
var serverconfig = require('./serverconfig').getServerConfig();

var battle_log_redis_ip = serverconfig.battle_log_redis.ip;
var battle_log_redis_port = serverconfig.battle_log_redis.port;

var match_redis_ip = serverconfig.match_redis.ip;
var match_redis_port = serverconfig.match_redis.port;

// redis setting!
var battle_log_redis = redis.createClient(battle_log_redis_port, battle_log_redis_ip, { detect_buffers : true });
exports.battle_log_redis = battle_log_redis;
var match_redis = redis.createClient(match_redis_port, match_redis_ip, { detect_buffers : true });
exports.match_redis = match_redis;

// subscriber에 return_buffers가 없을 경우 trailing byte error가 발생
var battle_sub = redis.createClient(battle_log_redis_port, battle_log_redis_ip, { detect_buffers : true });
var battle_pub = redis.createClient(battle_log_redis_port, battle_log_redis_ip, { detect_buffers : true });
exports.battle_sub = battle_sub;
exports.battle_pub = battle_pub;

function initRedis() {
    battle_log_redis.select(1, function (err) { });
    match_redis.select(2, function (err) { });
}
exports.initRedis = initRedis;

function initCallback(callback) {
    global_callback = callback;
}
exports.initCallback = initCallback;

battle_sub.on('message', function (channel, message) {
    if (global_callback != null) {
        global_callback(channel, message);
    }
});
