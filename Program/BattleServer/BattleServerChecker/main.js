﻿console.log("Start Battle Server Checker");

var io = require('socket.io-client');
var redisClient = require('./redis_store');

redisClient.initRedis();
redisClient.initCallback(CallbackSubscribe);
redisClient.match_sub.subscribe('battle_serv_reg');
var ipfunc = require('ipfunctions');
var isUserPrivateIp = process.argv[2];
var private_ips_ranges = [
    { 'min' : ipfunc.ip2long('192.168.0.0'), 'max' : ipfunc.ip2long('192.168.255.255') },
    { 'min' : ipfunc.ip2long('172.16.0.0'), 'max' : ipfunc.ip2long('172.31.255.255') },
    { 'min' : ipfunc.ip2long('10.0.0.0'), 'max' : ipfunc.ip2long('10.255.255.255') },
    { 'min' : ipfunc.ip2long('169.254.0.0'), 'max' : ipfunc.ip2long('169.254.255.255') },
    { 'min' : ipfunc.ip2long('127.0.0.0'), 'max' : ipfunc.ip2long('127.255.255.255') },
];

var find_battle_serv_key = 'battle_server';
redisClient.match_redis.hkeys(find_battle_serv_key, function (err, servlist) {
    for (var iserv = 0; iserv < servlist.length; iserv++) {
        var servipnport = servlist[iserv].split(',');
        var ip = servipnport[0];
        var port = servipnport[1];
        _CheckSocket(ip, port, servlist[iserv]);
    }
});

function _CheckSocket(ip, port, key) {
    if (isUserPrivateIp != 'USE_PRIVATE_IP') {
        var isPrivateIp = false;
        for (var iip = 0; iip < private_ips_ranges.length; iip++) {
            var current_ip_long = ipfunc.ip2long(ip);
            if (private_ips_ranges[iip].min <= current_ip_long && private_ips_ranges[iip].max >= current_ip_long) {
                var del_battle_serv_key = 'battle_server';
                redisClient.match_redis.hdel(del_battle_serv_key, key, function (err) {
                    console.log("delete private ip battle server - " + current_ip_long);
                });
            }
            else {
                _ManageSocket(ip, port, key);
            }
        }
    }
    else {
        _ManageSocket(ip, port, key);
    }
}
var sockets = {};
function _ManageSocket(ip, port, address_key) {
    var save_address_key = address_key;
    var connect_address = "ws://" + ip + ":" + port + "/socket.io/?EIO=4&transport=websocket";
    if (!sockets.hasOwnProperty(save_address_key)) {
        sockets[save_address_key] = io.connect(connect_address, { 'reconnection' : false });
        sockets[save_address_key].on('connect_error', function (err) {
            var del_battle_serv_key = 'battle_server';
            redisClient.match_redis.hdel(del_battle_serv_key, save_address_key, function (err) {
                console.log("delete battle server - " + save_address_key);
                sockets[save_address_key].close();
            });
        });
        sockets[save_address_key].on('disconnect', function () {
            var del_battle_serv_key = 'battle_server';
            redisClient.match_redis.hdel(del_battle_serv_key, save_address_key, function (err) {
                console.log("delete battle server - " + save_address_key);
                sockets[save_address_key].close();
            });
        });
    }
    else {
        sockets[save_address_key].connect();
    }
}

// redis 구독 처리
function CallbackSubscribe(channel, message) {
    // console.log("[" + channel + "] - " + message);
    if (channel == "battle_serv_reg") {
        var battle_serv_reg_info = JSON.parse(message);
        _CheckSocket(battle_serv_reg_info.ip, battle_serv_reg_info.port, battle_serv_reg_info.key);
        console.log("Add Check Battle Server - " + battle_serv_reg_info.key);
    }
}