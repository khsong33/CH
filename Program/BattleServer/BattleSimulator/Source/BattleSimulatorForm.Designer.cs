﻿namespace BattleSimulator
{
	partial class BattleSimulatorForm
	{
		/// <summary>
		/// 필수 디자이너 변수입니다.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// 사용 중인 모든 리소스를 정리합니다.
		/// </summary>
		/// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form 디자이너에서 생성한 코드

		/// <summary>
		/// 디자이너 지원에 필요한 메서드입니다.
		/// 이 메서드의 내용을 코드 편집기로 수정하지 마십시오.
		/// </summary>
		private void InitializeComponent()
		{
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(BattleSimulatorForm));
			this.mToolStrip = new System.Windows.Forms.ToolStrip();
			this.mStatusStrip = new System.Windows.Forms.StatusStrip();
			this.mInfoPanel = new System.Windows.Forms.Panel();
			this.mViewPanel = new System.Windows.Forms.Panel();
			this.mBattleGridPanel = new System.Windows.Forms.Panel();
			this.mFirstLeftCoordLabel = new System.Windows.Forms.Label();
			this.mFirstBottomCoordLabel = new System.Windows.Forms.Label();
			this.mFirstRightCoordLabel = new System.Windows.Forms.Label();
			this.mFirstTopCoordLabel = new System.Windows.Forms.Label();
			this.mViewPanel.SuspendLayout();
			this.mBattleGridPanel.SuspendLayout();
			this.SuspendLayout();
			// 
			// mToolStrip
			// 
			this.mToolStrip.Location = new System.Drawing.Point(0, 0);
			this.mToolStrip.Name = "mToolStrip";
			this.mToolStrip.Size = new System.Drawing.Size(1200, 25);
			this.mToolStrip.TabIndex = 1;
			this.mToolStrip.Text = "toolStrip1";
			// 
			// mStatusStrip
			// 
			this.mStatusStrip.Location = new System.Drawing.Point(0, 813);
			this.mStatusStrip.Name = "mStatusStrip";
			this.mStatusStrip.Size = new System.Drawing.Size(1200, 22);
			this.mStatusStrip.TabIndex = 2;
			this.mStatusStrip.Text = "statusStrip1";
			// 
			// mInfoPanel
			// 
			this.mInfoPanel.AutoScroll = true;
			this.mInfoPanel.Dock = System.Windows.Forms.DockStyle.Right;
			this.mInfoPanel.Location = new System.Drawing.Point(800, 25);
			this.mInfoPanel.Name = "mInfoPanel";
			this.mInfoPanel.Size = new System.Drawing.Size(400, 788);
			this.mInfoPanel.TabIndex = 3;
			// 
			// mViewPanel
			// 
			this.mViewPanel.AutoScroll = true;
			this.mViewPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
			this.mViewPanel.Controls.Add(this.mBattleGridPanel);
			this.mViewPanel.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mViewPanel.Location = new System.Drawing.Point(0, 25);
			this.mViewPanel.Name = "mViewPanel";
			this.mViewPanel.Size = new System.Drawing.Size(800, 788);
			this.mViewPanel.TabIndex = 4;
			// 
			// mBattleGridPanel
			// 
			this.mBattleGridPanel.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("mBattleGridPanel.BackgroundImage")));
			this.mBattleGridPanel.Controls.Add(this.mFirstLeftCoordLabel);
			this.mBattleGridPanel.Controls.Add(this.mFirstBottomCoordLabel);
			this.mBattleGridPanel.Controls.Add(this.mFirstRightCoordLabel);
			this.mBattleGridPanel.Controls.Add(this.mFirstTopCoordLabel);
			this.mBattleGridPanel.Location = new System.Drawing.Point(0, 0);
			this.mBattleGridPanel.Name = "mBattleGridPanel";
			this.mBattleGridPanel.Size = new System.Drawing.Size(800, 788);
			this.mBattleGridPanel.TabIndex = 0;
			// 
			// mFirstLeftCoordLabel
			// 
			this.mFirstLeftCoordLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.mFirstLeftCoordLabel.BackColor = System.Drawing.Color.Transparent;
			this.mFirstLeftCoordLabel.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.mFirstLeftCoordLabel.Location = new System.Drawing.Point(0, 18);
			this.mFirstLeftCoordLabel.Name = "mFirstLeftCoordLabel";
			this.mFirstLeftCoordLabel.Size = new System.Drawing.Size(24, 12);
			this.mFirstLeftCoordLabel.TabIndex = 3;
			this.mFirstLeftCoordLabel.Text = "000";
			this.mFirstLeftCoordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// mFirstBottomCoordLabel
			// 
			this.mFirstBottomCoordLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.mFirstBottomCoordLabel.BackColor = System.Drawing.Color.Transparent;
			this.mFirstBottomCoordLabel.Location = new System.Drawing.Point(28, 776);
			this.mFirstBottomCoordLabel.Name = "mFirstBottomCoordLabel";
			this.mFirstBottomCoordLabel.Size = new System.Drawing.Size(24, 12);
			this.mFirstBottomCoordLabel.TabIndex = 2;
			this.mFirstBottomCoordLabel.Text = "000";
			this.mFirstBottomCoordLabel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			// 
			// mFirstRightCoordLabel
			// 
			this.mFirstRightCoordLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.mFirstRightCoordLabel.BackColor = System.Drawing.Color.Transparent;
			this.mFirstRightCoordLabel.Location = new System.Drawing.Point(777, 40);
			this.mFirstRightCoordLabel.Name = "mFirstRightCoordLabel";
			this.mFirstRightCoordLabel.Size = new System.Drawing.Size(24, 12);
			this.mFirstRightCoordLabel.TabIndex = 1;
			this.mFirstRightCoordLabel.Text = "000";
			this.mFirstRightCoordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
			// 
			// mFirstTopCoordLabel
			// 
			this.mFirstTopCoordLabel.Anchor = System.Windows.Forms.AnchorStyles.Top;
			this.mFirstTopCoordLabel.BackColor = System.Drawing.Color.Transparent;
			this.mFirstTopCoordLabel.Location = new System.Drawing.Point(28, 2);
			this.mFirstTopCoordLabel.Name = "mFirstTopCoordLabel";
			this.mFirstTopCoordLabel.Size = new System.Drawing.Size(24, 12);
			this.mFirstTopCoordLabel.TabIndex = 0;
			this.mFirstTopCoordLabel.Text = "000";
			this.mFirstTopCoordLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter;
			// 
			// BattleSimulatorForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(1200, 835);
			this.Controls.Add(this.mViewPanel);
			this.Controls.Add(this.mInfoPanel);
			this.Controls.Add(this.mStatusStrip);
			this.Controls.Add(this.mToolStrip);
			this.Name = "BattleSimulatorForm";
			this.Text = "전투 시뮬레이터";
			this.mViewPanel.ResumeLayout(false);
			this.mBattleGridPanel.ResumeLayout(false);
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.ToolStrip mToolStrip;
		private System.Windows.Forms.StatusStrip mStatusStrip;
		private System.Windows.Forms.Panel mInfoPanel;
		private System.Windows.Forms.Panel mViewPanel;
		private System.Windows.Forms.Panel mBattleGridPanel;
		private System.Windows.Forms.Label mFirstTopCoordLabel;
		private System.Windows.Forms.Label mFirstBottomCoordLabel;
		private System.Windows.Forms.Label mFirstRightCoordLabel;
		private System.Windows.Forms.Label mFirstLeftCoordLabel;
	}
}

