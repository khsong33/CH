﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToolsCommon;

namespace BattleSimulatorAndTools
{
    public class Utils
    {
        public static Point WorldToCanvas(Coord2 world, int canvasHeight)
        {
            return new Point(world.x / 10, canvasHeight - world.z / 10);
        }

        public static Coord2 CanvasToWorld(Point canvas, int canvasHeight)
        {
            return new Coord2((int)(canvas.X * 10), (int)((canvasHeight - canvas.Y) * 10));
        }
    }

    public class UnitShape
    {
        static Dictionary<string, BitmapImage> bitmaps = new Dictionary<string, BitmapImage>();
        static BitmapImage GetBitmap(string filename, int userIdx)
        {
            if (bitmaps.ContainsKey(filename) == false)
            {
                var bi = new BitmapImage();
                bi.BeginInit();
                bi.UriSource = new Uri(@"..\..\" + filename + ".png", UriKind.Relative);
                bi.CacheOption = BitmapCacheOption.OnLoad;
                bi.EndInit();

                bitmaps.Add(filename, bi);
            }

            return bitmaps[filename];
        }

        public static void UpdateShape(FrameworkElement shape, Coord2 coord, int directionY, Canvas canvas)
        {
            var size = (double)shape.Tag / 2;
            var point = Utils.WorldToCanvas(coord, (int)canvas.Height);
            Canvas.SetTop(shape, point.Y - size);
            Canvas.SetLeft(shape, point.X - size);

            shape.RenderTransform = new RotateTransform(directionY, size, size);
        }

        public static FrameworkElement AddImage(bool fill, double size, int teamIdx, string filename, Canvas canvas)
        {
            var image = new Image();
            image.Source = GetBitmap(filename, teamIdx);
            image.Width = size;
            image.Height = size;

            var circle = new Ellipse();
            circle.Width = size * 0.7;
            circle.Height = size * 0.7;

            Color color;
            if (teamIdx == 0)
                color = Colors.Green;
            else if (teamIdx == 1)
                color = Colors.Blue;
            else
                color = Colors.Red;

            color.A = 128;
            circle.Fill = new SolidColorBrush(color);

            var grid = new Grid();
            grid.Children.Add(circle);
            grid.Children.Add(image);

            var vb = new Viewbox();
            vb.Child = grid;
            vb.Tag = size;
            vb.Width = size;
            vb.Height = size;

            canvas.Children.Add(vb);
            return vb;
        }

        public static FrameworkElement AddShape(bool fill, double size, int teamIdx, Canvas canvas)
        {
            var shape = new Ellipse();
            shape.Width = size;
            shape.Height = size;
            shape.Tag = size;

            Brush brush = null;

            if (teamIdx == 0)
                brush = Brushes.SeaGreen;
            else if (teamIdx == 1)
                brush = Brushes.DarkRed;
            else
                brush = Brushes.DodgerBlue;

            if (fill)
                shape.Fill = brush;
            else
            {
                shape.Stroke = brush;
                shape.StrokeThickness = 5;
            }
            canvas.Children.Add(shape);
            return shape;
        }
    }

    public class EffectUpdater
    {
        private class Effect
        {
            public float lifeTime;
            public Coord2 from;
            public Coord2 to;
            public FrameworkElement shape;
            public int teamIdx;
        }

        private static List<Effect> effects = new List<Effect>();
        private static Canvas world;
        static System.Random random = new System.Random();

        public static void Init(Canvas world_)
        {
            world = world_;
        }

        public static void AddEffect(Coord2 from, Coord2 to, float lifeTime, int teamIdx)
        {
            Effect effect = new Effect() { lifeTime = lifeTime, from = from, to = to, teamIdx = teamIdx };
            effects.Add(effect);
        }


        public static void UpdateEffect(float elapsedTime, Canvas world)
        {
            List<Effect> toDelete = new List<Effect>();
            foreach (var effect in effects)
            {
                if (effect.shape == null)
                {
                    var from = Utils.WorldToCanvas(effect.from, (int)world.Height);
                    var to = Utils.WorldToCanvas(effect.to, (int)world.Height);


                    var line = new Line();
                    line.X1 = from.X + random.NextDouble() * 10;
                    line.Y1 = from.Y + random.NextDouble() * 10;
                    line.X2 = to.X;
                    line.Y2 = to.Y;
                    switch (effect.teamIdx)
                    {
                        case 0: line.Stroke = Brushes.GreenYellow; break;
                        case 1: line.Stroke = Brushes.DeepSkyBlue; break;
                        case 2: line.Stroke = Brushes.Red; break;
                    }
                    line.StrokeThickness = 5;
                    world.Children.Add(line);
                    effect.shape = line;
                }
                else
                {
                    effect.lifeTime -= elapsedTime;
                    if (effect.lifeTime < 0)
                        toDelete.Add(effect);
                }
            }
            foreach (var effect in toDelete)
            {
                world.Children.Remove(effect.shape);
                effects.Remove(effect);
            }
        }
    }

    /// <summary>
    /// Interaction logic for BattleView.xaml
    /// </summary>
    public partial class BattleView : UserControl
    {
        public BattleView()
        {
            InitializeComponent();

        }

        public void Reset()
        {
            world.Children.Clear();
        }

        Coord2 lastClickPos = new Coord2();
        MouseButton lastClickButton = 0;
        private void world_MouseDown(object sender, MouseButtonEventArgs e)
        {
            lastClickPos = Utils.CanvasToWorld(e.GetPosition(world), (int)world.Height);
            lastClickButton = e.ChangedButton;
        }

        private void world_MouseUp(object sender, MouseButtonEventArgs e)
        {
            var coord = Utils.CanvasToWorld(e.GetPosition(world), (int)world.Height);

            int userIdx = 0;
            int commanderNo = 101;
            if (e.ChangedButton == MouseButton.Left)
            {
                userIdx = 0;
                commanderNo = 101;
            }
            else if (e.ChangedButton == MouseButton.Right)
            {
                userIdx = 1;
                commanderNo = 103;
            }
            else if (e.ChangedButton == MouseButton.Middle)
            {
                return;
            }

            Dictionary<string, object> cmd = new Dictionary<string, object>();
            cmd.Add("cmd", "spawnCommander");
            cmd.Add("userIdx", userIdx);
            cmd.Add("commanderNo", commanderNo);
            cmd.Add("commanderLevel", EnvironmentSetup.get.fakeTroopInfo.commanderLevel);
            cmd.Add("directionY", 0);
            cmd.Add("coord", coord);
            cmd.Add("weaponName", EnvironmentSetup.get.fakeTroopInfo.weaponName);
            cmd.Add("troopName", EnvironmentSetup.get.fakeTroopInfo.troopName);
            TestCommandManager.get.AddCommand(cmd);
        }
    }
}
