﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CustomBattleCommanderItemTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String vDefaultUserName = "default";

	public readonly String cCommanderItemCSVTextAssetPath = "CustomCommanderItemCSV";
	public readonly String vUserNameColumnName = "UserName";
	public readonly String vItemIdxColumnName = "ItemId";
	public readonly String vItemNoColumnName = "ItemNo";
	public readonly String vItemExpColumnName = "Exp";
	public readonly String vRankColumnName = "Rank";
	public readonly String vLevelColumnName = "Level";
	public readonly String vPrefixEnchantColumnName = "Enchant";
	public readonly String vCountColumnName = "Count";

	public readonly String cCommanderDeckCSVTextAssetPath = "CustomCommanderDeckCSV";
	public readonly String vDeckNationColumnName = "Nation";
	public readonly String vDeckNoColumnName = "DeckNo";
	public readonly String vItemListColumnName = "Items";
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CustomBattleCommanderItemTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CustomBattleCommanderItemTable();
		}
	}


	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CustomBattleCommanderItemTable()
	{
		DebugUtil.Assert(CommanderTroopSet.cMaxTroopCount == CustomBattleCommanderItemData.cMaxEnchatnLevel, "CommanderTroopSet.cMaxTroopCount == TestBattleCommanderItemData.cMaxEnchatnLevel");

		LoadCommanderItem();
		LoadCommanderDeck();
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템 테이블을 로딩합니다.
	public void LoadCommanderItem()
	{
		mItemCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCommanderItemCSVTextAssetPath);
		mItemCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mCustomBattleCommanderItemDatas = new Dictionary<String, Dictionary<int, CustomBattleCommanderItemData>>();
		int lCSVRowCount = mItemCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mItemCSVObject.GetRow(iRow);

			CustomBattleCommanderItemData lItem = new CustomBattleCommanderItemData();

			// UserName
			lItem.mUserName = lCSVRow.GetStringValue(vUserNameColumnName);

			// ItemIdx
			lItem.mItemIdx = lCSVRow.GetIntValue(vItemIdxColumnName, 0);

			// ItemNo
			lItem.mItemNo = lCSVRow.GetIntValue(vItemNoColumnName, 0);

			// exp
			lItem.mExp = lCSVRow.GetIntValue(vItemExpColumnName, 0);

			// Rank
			lItem.mRank = lCSVRow.GetIntValue(vRankColumnName, 0);

			// Level
			lItem.mLevel = lCSVRow.GetIntValue(vLevelColumnName, 0);

			for (int iEnchant = 0; iEnchant < CustomBattleCommanderItemData.cMaxEnchatnLevel; iEnchant++)
			{
				String lEnchantLevelColumnName = vPrefixEnchantColumnName + iEnchant.ToString();

				// EnchantLevel
				lItem.mEnchantLevel[iEnchant] = lCSVRow.GetIntValue(lEnchantLevelColumnName, 0);
			}

			// Count
			lItem.mCount = lCSVRow.GetIntValue(vCountColumnName, 0);

			// 해당 유저에게 아이템을 등록합니다.
			if (!mCustomBattleCommanderItemDatas.ContainsKey(lItem.mUserName))
			{
				mCustomBattleCommanderItemDatas.Add(lItem.mUserName, new Dictionary<int, CustomBattleCommanderItemData>());
			}
			if (!mCustomBattleCommanderItemDatas[lItem.mUserName].ContainsKey(lItem.mItemIdx))
			{
				mCustomBattleCommanderItemDatas[lItem.mUserName].Add(lItem.mItemIdx, lItem);
			}
			else
			{
				mCustomBattleCommanderItemDatas[lItem.mUserName][lItem.mItemIdx] = lItem;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 테이블을 로딩합니다.
	public void LoadCommanderDeck()
	{
		mDeckCSVObject = new CSVObject();
		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCommanderDeckCSVTextAssetPath);
		mDeckCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mCustomBattleCommanderDeckDatas = new Dictionary<String, List<CustomBattleCommanderDeckData>>();

		int lCSVRowCount = mDeckCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mDeckCSVObject.GetRow(iRow);

			CustomBattleCommanderDeckData lDeck = new CustomBattleCommanderDeckData();

			// UserName
			lDeck.mUserName = lCSVRow.GetStringValue(vUserNameColumnName);

			// DeckNation
			lDeck.mDeckNation = CommanderTable.get.FindDeckNation(lCSVRow.GetStringValue(vDeckNationColumnName));

			// DeckNo
			lDeck.mDeckNo = lCSVRow.GetIntValue(vDeckNoColumnName, 0);

			// 덱 구성을 등록합니다.
			String lItemNos = lCSVRow.GetStringValue(vItemListColumnName);
			String[] lItemArray = lItemNos.Split(',');
			for (int iItem = 0; iItem < lItemArray.Length; iItem++)
			{
				lDeck.mItemList.Add(Convert.ToInt32(lItemArray[iItem]));
			}
			if (!mCustomBattleCommanderDeckDatas.ContainsKey(lDeck.mUserName))
			{
				mCustomBattleCommanderDeckDatas.Add(lDeck.mUserName, new List<CustomBattleCommanderDeckData>());
			}
			mCustomBattleCommanderDeckDatas[lDeck.mUserName].Add(lDeck);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 전체 커맨더 데이터를 불러옵니다.
	public List<CommanderItemData> GetAllTestCommanderItemData(String pUserName)
	{
		String lDeckUserName;
		if (!String.IsNullOrEmpty(pUserName))
		{
			lDeckUserName = pUserName;
			if (!mCustomBattleCommanderDeckDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
			if (!mCustomBattleCommanderItemDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
		}
		else
		{
			lDeckUserName = vDefaultUserName;
		}
		List<CommanderItemData> lResultItemDatas = new List<CommanderItemData>();
		Dictionary<int, CustomBattleCommanderItemData>.Enumerator iter = mCustomBattleCommanderItemDatas[lDeckUserName].GetEnumerator();
		while (iter.MoveNext())
		{
			CustomBattleCommanderItemData lTestItemData = iter.Current.Value;
			CommanderItemData lItemData = new CommanderItemData();
			lItemData.mCommanderItemIdx = lTestItemData.mItemIdx;
			lItemData.mCommanderSpecNo = lTestItemData.mItemNo;
			lItemData.mCommanderExp = lTestItemData.mExp;
			lItemData.mCommanderExpRank = lTestItemData.mRank;
			lItemData.mCommanderLevel = lTestItemData.mLevel;
			lItemData.mSpawnCount = lTestItemData.mCount;

			lResultItemDatas.Add(lItemData);
		}
		return lResultItemDatas;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 이름으로 CommaderItemData를 구성해서 불러옵니다.
	public List<CommanderItemData> GetTestCommanderItemData(String pUserName, int pUseDeckNo)
	{
		// 유저 이름을 찾는데 없다면 기본 유저로 설정합니다.
		String lDeckUserName;
		if (!String.IsNullOrEmpty(pUserName))
		{
			lDeckUserName = pUserName;
			if (!mCustomBattleCommanderDeckDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
			if (!mCustomBattleCommanderItemDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
		}
		else
		{
			lDeckUserName = vDefaultUserName;
		}

		List<CommanderItemData> lResultItemDatas = new List<CommanderItemData>();

		List<CustomBattleCommanderDeckData> lUserDeck = mCustomBattleCommanderDeckDatas[lDeckUserName];
		CustomBattleCommanderDeckData lUserUseDeck = null;
		for (int iDeck = 0; iDeck < lUserDeck.Count; iDeck++)
		{
			if (lUserDeck[iDeck].mDeckNo == pUseDeckNo)
			{
				lUserUseDeck = lUserDeck[iDeck];
			}
		}
		for (int iItem = 0; iItem < lUserUseDeck.mItemList.Count; iItem++)
		{
			if (!mCustomBattleCommanderItemDatas[lDeckUserName].ContainsKey(lUserUseDeck.mItemList[iItem]))
				continue;
			CustomBattleCommanderItemData lTestItemData = mCustomBattleCommanderItemDatas[lDeckUserName][lUserUseDeck.mItemList[iItem]];
			CommanderItemData lItemData = new CommanderItemData();
			lItemData.mCommanderItemIdx = lTestItemData.mItemIdx;
			lItemData.mCommanderSpecNo = lTestItemData.mItemNo;
			lItemData.mCommanderExp = lTestItemData.mExp;
			lItemData.mCommanderExpRank = lTestItemData.mRank;
			lItemData.mCommanderLevel = lTestItemData.mLevel;
			lItemData.mSpawnCount = lTestItemData.mCount;

			lResultItemDatas.Add(lItemData);
		}
		return lResultItemDatas;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 이름으로 DeckDataList를 불러옵니다.
	public List<DeckData> GetDeckDataList(String pUserName)
	{
		String lDeckUserName;
		if (!String.IsNullOrEmpty(pUserName))
		{
			lDeckUserName = pUserName;
			if (!mCustomBattleCommanderDeckDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
			if (!mCustomBattleCommanderItemDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
		}
		else
		{
			lDeckUserName = vDefaultUserName;
		}
		List<CustomBattleCommanderDeckData> lTestDeckDataList = mCustomBattleCommanderDeckDatas[lDeckUserName];
		List<DeckData> lResultDeckDataList = new List<DeckData>();
		for (int iDeck = 0; iDeck < lTestDeckDataList.Count; iDeck++)
		{
			DeckData lDeckData = new DeckData();
			lDeckData.mDeckIdx = lTestDeckDataList[iDeck].mDeckNo;
			lDeckData.mDeckName = "Test + " + iDeck;
			lDeckData.mDeckNation = lTestDeckDataList[iDeck].mDeckNation;
			List<CommanderItemData> lCommanderItemDataList = GetTestCommanderItemData(lDeckUserName, lDeckData.mDeckIdx);
			for(int iCommanderItem = 0; iCommanderItem < lCommanderItemDataList.Count; iCommanderItem++)
			{
				lDeckData.mDeckItems[iCommanderItem] = new CommanderItemData();
				CommanderItemData.Copy(lCommanderItemDataList[iCommanderItem], lDeckData.mDeckItems[iCommanderItem]);
			}
			lResultDeckDataList.Add(lDeckData);
		}
		return lResultDeckDataList;
	}
	public CustomBattleCommanderDeckData GetCustomDeckData(String pUserName, int pDeckIdx)
	{
		// 유저 이름을 찾는데 없다면 기본 유저로 설정합니다.
		String lDeckUserName;
		if (!String.IsNullOrEmpty(pUserName))
		{
			lDeckUserName = pUserName;
			if (!mCustomBattleCommanderDeckDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
			if (!mCustomBattleCommanderItemDatas.ContainsKey(pUserName))
				lDeckUserName = vDefaultUserName;
		}
		else
		{
			lDeckUserName = vDefaultUserName;
		}

		List<CustomBattleCommanderDeckData> lUserDeck = mCustomBattleCommanderDeckDatas[lDeckUserName];
		for (int iDeck = 0; iDeck < lUserDeck.Count; iDeck++)
		{
			if (lUserDeck[iDeck].mDeckNo == pDeckIdx)
			{
				return lUserDeck[iDeck];
			}
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CustomBattleCommanderItemTable sInstance = null;
	
	private CSVObject mItemCSVObject;
	private CSVObject mDeckCSVObject;

	private Dictionary<String, Dictionary<int, CustomBattleCommanderItemData>> mCustomBattleCommanderItemDatas;
	private Dictionary<String, List<CustomBattleCommanderDeckData>> mCustomBattleCommanderDeckDatas;
}
