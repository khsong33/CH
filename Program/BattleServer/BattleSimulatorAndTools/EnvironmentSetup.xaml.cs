﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ToolsCommon;

namespace BattleSimulatorAndTools
{
    /// <summary>
    /// Interaction logic for EnvironmentSetup.xaml
    /// </summary>
    public partial class EnvironmentSetup : UserControl
    {
        public static EnvironmentSetup get;

        public FakeUserInfo[] fakeUserInfos = new FakeUserInfo[2];
        public FakeMatchInfo fakeMatchInfo;
        public FakeTroopInfo fakeTroopInfo;

        public EnvironmentSetup()
        {
            get = this;

            InitializeComponent();

            fakeMatchInfo = new FakeMatchInfo();
            fakeUserInfos[0] = new FakeUserInfo() { teamIdx = 1, hqGuid = 1 };
            fakeUserInfos[1] = new FakeUserInfo() { teamIdx = 2, hqGuid = 2 };
            fakeTroopInfo = new FakeTroopInfo();

            List<object> layout = new List<object>();

            Dictionary<string, List<object>> comboList = new Dictionary<string,List<object>>();
            List<object> troopList = new List<object>();
            List<object> weaponList = new List<object>();
            List<object> layoutList = new List<object>();

            for (int i = 0; i < TroopTable.get.GetSpecCount(); i++)
                troopList.Add(TroopTable.get.GetTroopSpecByIdx(i).mName);

            for (int i = 0; i < WeaponTable.get.GetSpecCount(); i++)
                weaponList.Add(WeaponTable.get.GetWeaponSpecByIdx(i).mName);

            var files = System.IO.Directory.GetFiles(Environment.CurrentDirectory + @"\BundleResources\GridLayouts0").Where(x => x.Contains(".meta") == false);
            foreach (var file in files)
            {
                layoutList.Add(file.Substring(file.LastIndexOf(@"\") + 1).Replace(".txt", ""));
            }

            comboList.Add("troop", troopList);
            comboList.Add("weapon", weaponList);
            comboList.Add("layout", layoutList);

            var matchInfoGrid = new PropertyGrid(null, fakeMatchInfo, comboList);
            var userInfo1Grid = new PropertyGrid(null, fakeUserInfos[0]);
            var userInfo2Grid = new PropertyGrid(null, fakeUserInfos[1]);
            var troopInfoGrid = new PropertyGrid(null, fakeTroopInfo, comboList);
            
            matchInfoGrid.SetTitle("Match Info");
            userInfo1Grid.SetTitle("User 1");
            userInfo2Grid.SetTitle("User 2");
            troopInfoGrid.SetTitle("Troop Info");

            matchInfo.Child = matchInfoGrid;
            userInfo1.Child = userInfo1Grid;
            userInfo2.Child = userInfo2Grid;
            uiTroopInfo.Child = troopInfoGrid;

            List<CommanderItemData> lCommanderItemDataList = CustomBattleCommanderItemTable.get.GetTestCommanderItemData("default", 0);
            for (int iSlot = 0; iSlot < lCommanderItemDataList.Count; iSlot++)
            {
                fakeUserInfos[0].commanderItemDataList.Add(lCommanderItemDataList[iSlot]);
                fakeUserInfos[1].commanderItemDataList.Add(lCommanderItemDataList[iSlot]);
            }

            uiCommanderList1.ItemsSource = fakeUserInfos[0].commanderItemDataList;
            uiCommanderList2.ItemsSource = fakeUserInfos[1].commanderItemDataList;

            var commander = CommanderTable.get.FindCommanderSpec(fakeUserInfos[0].commanderItemDataList[0].mCommanderSpecNo);
            var troop = commander.mRankTroopSets[0].mTroopInfos[0].mTroopSpec;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var btn = e.Source as Button;
            //int idx = int.Parse(btn.Tag.ToString());

            //var input = BattleFrameInput.Create(0);
            //input.mType = (int)BattleFrameInputType.SpawnCommander;
            //input.mX = lastClickPos.x;
            //input.mY = lastClickPos.z;
            //input.mA = lastClickButton;
            //input.mB = idx;
            //lock (battleFrameInputList)
            //    battleFrameInputList.Add(input);
        }

        private void ListBox_User1CommanderSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            uiCommanderItemData.Child = new PropertyGrid(null, e.AddedItems[0]);
        }

        private void ListBox_User2CommanderSelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            uiCommanderItemData.Child = new PropertyGrid(null, e.AddedItems[0]);
        }
    }
}
