﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Media.Media3D;
using ToolsCommon;
using HelixToolkit.Wpf;

namespace BattleSimulatorAndTools
{
    /// <summary>
    /// Interaction logic for GridLayoutTool.xaml
    /// </summary>
    public partial class GridLayoutTool : UserControl
    {
        List<AnyItem> assetList = new List<AnyItem>();
        List<AnyItem> layoutList = new List<AnyItem>();

        public GridLayoutTool()
        {
            InitializeComponent();

            viewPort3d.RotateGesture = new MouseGesture(MouseAction.LeftClick);
            viewPort3d.ShowFrameRate = true;

            {
                var files = System.IO.Directory.GetFiles(ToolConfig.cGridLayoutFolder, "*.txt");
                foreach (var file in files)
                {
                    var item = new AnyItem();
                    item.Add("Path", file);
                    item.Add("Name", file.Substring(file.LastIndexOf(@"\") + 1).Replace(".txt", ""));
                    layoutList.Add(item);
                }
                lbLayout.ItemsSource = layoutList;
            }

            //{
            //    var files = System.IO.Directory.GetFiles(ToolConfig.c3dMapAssetFolder, "*.obj");
            //    foreach (var file in files)
            //    {
            //        var item = new AnyItem();
            //        item.Add("Path", file);
            //        item.Add("Name", file.Substring(file.LastIndexOf(@"\") + 1).Replace(".obj", ""));
            //        assetList.Add(item);
            //    }
            //    lbAssets.ItemsSource = assetList;
            //}
        }

        void LoadAsset(string mapName)
        {
            viewPort3d.Children.Clear();
            viewPort3d.Children.Add(new DefaultLights());

            ModelVisual3D model = new ModelVisual3D();
            model.Content = new ModelImporter().Load(mapName);
            viewPort3d.Children.Add(model);
        }

        private void MapFilter_TextChanged(object sender, TextChangedEventArgs e)
        {

        }

        private void OnLoadGridLayout(object sender, RoutedEventArgs e)
        {

        }

        private void OnLoadAsset(object sender, RoutedEventArgs e)
        {
            var btn = sender as Button;
            var item = btn.Tag as AnyItem;
            LoadAsset(item.GetString("Path"));
        }
    }
}
