﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Shapes;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using ToolsCommon;
using HelixToolkit.Wpf;

namespace BattleSimulatorAndTools
{
   
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private BattleLogic battleLogic;
        private volatile bool stopPlease = false;
        private volatile int updateFrame = 0;
        private volatile int renderFrame = 0;
        private volatile int sleepTick = 200;

        public MainWindow()
        {
            InitializeComponent();

            environmentPanel.Child = new EnvironmentSetup();
            layoutTool.Child = new GridLayoutTool();

            scrollViewer.ScrollToBottom();
            EffectUpdater.Init(uiBattleView.world);
            SleepTick.Value = sleepTick;
        }

        private BackgroundWorker simulationWorker = null;

        private Dictionary<uint, FrameworkElement> effectIcons = new Dictionary<uint, FrameworkElement>();
        private Dictionary<uint, FrameworkElement> troopIcons = new Dictionary<uint, FrameworkElement>();
        private Dictionary<uint, FrameworkElement> checkPointIcons = new Dictionary<uint, FrameworkElement>();

        private List<BattleFrameInput> battleFrameInputList = new List<BattleFrameInput>();

        private void Button_RunTest(object sender, RoutedEventArgs e)
        {
            if (simulationWorker != null)
                return;

            troopIcons.Clear();
            checkPointIcons.Clear();
            uiBattleView.Reset();

            stopPlease = false;
            updateFrame = 0;
            renderFrame = 0;

            simulationWorker = new BackgroundWorker();
            simulationWorker.WorkerSupportsCancellation = true;
            simulationWorker.DoWork += simulator_DoWork;
            simulationWorker.RunWorkerAsync();
        }


        private void Button_PauseTest(object sender, RoutedEventArgs e)
        {
            if (sleepTick == int.MaxValue)
                sleepTick = (int)SleepTick.Value;
            else
                sleepTick = int.MaxValue;
        }

        int step = 0;
        private void Button_PrevStep(object sender, RoutedEventArgs e)
        {
            sleepTick = int.MaxValue;
            step = -1;
        }

        private void Button_NextStep(object sender, RoutedEventArgs e)
        {
            sleepTick = int.MaxValue;
            step = 1;
        }
        private void Button_Next10Step(object sender, RoutedEventArgs e)
        {
            sleepTick = int.MaxValue;
            step = 10;
        }
        
        private void Button_StopTest(object sender, RoutedEventArgs e)
        {
            stopPlease = true;
            simulationWorker = null;
        }
        
        private void Window_Closing(object sender, CancelEventArgs e)
        {
            stopPlease = true;
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            sleepTick = (int)e.NewValue;
        }

        private void simulator_DoWork(object sender, DoWorkEventArgs e)
        {
            Work();
        }

        private void Work()
        {
            TestCommandManager.get.Reset();

            battleLogic = new BattleLogic();

            Simulator simulator = new Simulator(battleLogic, new BattleSimulatorStage());
            FakeBattleMatchInfo matchInfo = new FakeBattleMatchInfo();

            var env = environmentPanel.Child as EnvironmentSetup;

            matchInfo.FakeMatchInfo(env.fakeMatchInfo, env.fakeUserInfos);
            simulator.StartSimulation(matchInfo);

            int lVerifyDataFrameInputIdx = 0;
            while (simulator.UpdateSimulation(battleFrameInputList, ref lVerifyDataFrameInputIdx) == true && stopPlease == false)
            {
                TestCommandManager.get.CommandHandler(simulator);

                while (updateFrame != renderFrame) ;
                updateFrame++;

                this.Dispatcher.Invoke(delegate() { RenderScreen(simulator); }, System.Windows.Threading.DispatcherPriority.Normal);

                if (sleepTick > 0)
                {
                    if (sleepTick == int.MaxValue)
                    {
                        while (sleepTick == int.MaxValue)
                        {
                            if (step > 0)
                            {
                                step--;
                                System.Threading.Thread.Sleep(50);
                                break;
                            }
                            System.Threading.Thread.Sleep(200);
                        }
                    }
                    else
                        System.Threading.Thread.Sleep(sleepTick);
                }
            }

            var progressInfo = simulator.aProgressInfo;
        }

        private void RenderScreen(Simulator lBattleVerifier)
        {
            RemoveDeadTroops(lBattleVerifier);
            RemoveDeadEffects(lBattleVerifier);

            RenderCheckPoints(lBattleVerifier);
            RenderTroops(lBattleVerifier);
            RenderEffects(lBattleVerifier);

            renderFrame++;
        }

        private void RenderEffects(Simulator lBattleVerifier)
        {
            EffectUpdater.UpdateEffect(0.1f, uiBattleView.world);
        }

        private void RenderTroops(Simulator simulator)
        {
            foreach (var commander in simulator.aBattleGrid.aCommanders)
            {
                if (commander == null) continue;

                foreach (BattleTroop troop in commander.aTroops)
                {
                    if (troop == null) continue;
                    if (troopIcons.ContainsKey(troop.aGuid) == false)
                    {
                        var shape = UnitShape.AddImage(true, 100, commander.aTeamIdx, troop.aTroopSpec.mFieldIconAssetKey.aAssetPath, uiBattleView.world);
                        troopIcons.Add(troop.aGuid, shape);
                    }

                    UnitShape.UpdateShape(troopIcons[troop.aGuid], troop.aCoord, troop.aDirectionY, uiBattleView.world);
                }
            }
        }


        private void RenderCheckPoints(Simulator lBattleVerifier)
        {
            foreach (var cp in lBattleVerifier.aBattleGrid.aCheckPointList)
            {
                if (checkPointIcons.ContainsKey(cp.aGuid) == false)
                {
                    var shape = UnitShape.AddShape(false, 120, cp.aTeamIdx, uiBattleView.world);
                    checkPointIcons.Add(cp.aGuid, shape);
                }

                UnitShape.UpdateShape(checkPointIcons[cp.aGuid], cp.aCoord, cp.aDirectionY, uiBattleView.world);
            }
        }

        private void RemoveDeadEffects(Simulator lBattleVerifier)
        {
            List<uint> toDelete = new List<uint>();
            foreach (var guid in troopIcons.Keys)
            {
                bool found = false;
                foreach (var commander in lBattleVerifier.aBattleGrid.aCommanders)
                {
                    if (commander == null) continue;
                    foreach (BattleTroop troop in commander.aTroops)
                    {
                        if (troop == null) continue;
                        if (troop.aGuid == guid)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }

                if (found == false)
                    toDelete.Add(guid);
            }

            foreach (var guid in toDelete)
            {
                uiBattleView.world.Children.Remove(troopIcons[guid] as FrameworkElement);
                troopIcons.Remove(guid);
            }
        }

        private void RemoveDeadTroops(Simulator lBattleVerifier)
        {
            List<uint> toDelete = new List<uint>();
            foreach (var guid in troopIcons.Keys)
            {
                bool found = false;
                foreach (var commander in lBattleVerifier.aBattleGrid.aCommanders)
                {
                    if (commander == null) continue;
                    foreach (BattleTroop troop in commander.aTroops)
                    {
                        if (troop == null) continue;
                        if (troop.aGuid == guid)
                        {
                            found = true;
                            break;
                        }
                    }
                    if (found)
                        break;
                }

                if (found == false)
                    toDelete.Add(guid);
            }

            foreach (var guid in toDelete)
            {
                uiBattleView.world.Children.Remove(troopIcons[guid] as FrameworkElement);
                troopIcons.Remove(guid);
            }
        }

        private void Button_KillAll(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> cmd = new Dictionary<string, object>();
            cmd.Add("cmd", "killAll");
            TestCommandManager.get.AddCommand(cmd);
        }
    }
}