﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Reflection;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows.Input;

namespace BattleSimulatorAndTools
{
    /// <summary>
    /// Interaction logic for EditPanel_HitObject.xaml
    /// </summary>
    public partial class PropertyGrid : UserControl
    {
        public delegate void OnWrite(object obj, FieldInfo fi, object subItem);
        public OnWrite OnDataChanged = null;

        public float keyWidth;
        public float valueWidth;
        object selectedObj;
        Dictionary<string, List<object>> dynamicListMap;

        public PropertyGrid(OnWrite dataChanged, object selectedObj, Dictionary<string, List<object>> dynamicListMap = null, float keyWidth = 150, float valueWidth = 65*3)
        {
            InitializeComponent();

            OnDataChanged = dataChanged;

            this.keyWidth = keyWidth;
            this.valueWidth = valueWidth;
            if (dynamicListMap != null)
                this.dynamicListMap = dynamicListMap;

            type.Content = selectedObj.GetType().ToString();
            this.selectedObj = selectedObj;
            Read(selectedObj);
        }

        public void SetTitle(string title)
        {
            type.Content = title;
        }

        object GetFieldValue(string name)
        {
            // 스택패널
            //  ㄴ 택스트블럭 | 데이터별 엘리먼트
            foreach (StackPanel panel in stackField.Children)
            {
                TextBlock txtblock = panel.Children[0] as TextBlock;
                if (txtblock == null) continue;

                if (((object[])txtblock.Tag)[0] as string == name)
                {
                    Type type = ((object[])txtblock.Tag)[1] as Type;

                    if (typeof(Coord2) == type)
                    {
                        TextBox x = panel.Children[1] as TextBox;
                        TextBox z = panel.Children[3] as TextBox;

                        return x.Text + "," + z.Text;
                    }
                    else if (typeof(Color) == type)
                    {
                        var picker = panel.Children[1] as Xceed.Wpf.Toolkit.ColorPicker;
                        picker.DisplayColorAndName = true;
                        picker.UsingAlphaChannel = true;
                        return picker.SelectedColor;
                    }
                    else if (typeof(System.Double) == type)
                    {
                        Slider val = panel.Children[1] as Slider;
                        return val.Value;
                    }
                    else
                    {
                        // int, single, double, string
                        TextBox txtbox = panel.Children[1] as TextBox;
                        if (txtbox != null) return txtbox.Text;

                        // 일반 enum 값인 경우
                        ComboBox combo = panel.Children[1] as ComboBox;
                        if (combo != null) return combo.Text;

                        // god daum bit fields!
                        WrapPanel stackBits = panel.Children[1] as WrapPanel;
                        if (stackBits != null)
                        {
                            int result = 0;
                            int count = 0;
                            // enum 값들을 쫙 뽑아서 콤보박스에 넣어주자!
                            foreach (FrameworkElement element in stackBits.Children)
                            {
                                CheckBox check = element as CheckBox;

                                if (check != null)
                                {
                                    if (check.IsChecked == true)
                                    {
                                        result |= 1 << count;
                                    }
                                }
                                count++;
                            }
                            return result.ToString();
                        }
                    }
                    return null;
                }
            }

            return null;
        }

        Type GetFieldType(string name)
        {
            foreach (StackPanel panel in stackField.Children)
            {
                TextBlock txtblock = panel.Children[0] as TextBlock;
                if (txtblock == null) continue;

                if (((object[])txtblock.Tag)[0] as string == name)
                {
                    return ((object[])txtblock.Tag)[1] as Type;
                }
            }

            return null;
        }

        void Read(object obj)
        {
            stackField.Children.Clear();

            Type type = obj.GetType();

            foreach (FieldInfo fi in obj.GetType().GetFields())
            {
                string customType = "";
                bool skip = false;
                string toolTip = "";
                object o = fi.GetValue(obj);
                bool readOnly = false;
                float numericEditDelta = 1;
                string dynamicListKey = null;

                object[] attrs = fi.GetCustomAttributes(false);
                foreach (object attr in attrs)
                {
                    if (attr.GetType() == typeof(ToolsCommon.ToolTipAttribute))
                    {
                        toolTip = ((ToolsCommon.ToolTipAttribute)attr).toolTip;
                    }
                    else if (attr.GetType() == typeof(ToolsCommon.HiddenAttribute))
                    {
                        skip = true;
                    }
                    else if (attr.GetType() == typeof(ToolsCommon.CustomAttribute))
                    {
                        customType = ((ToolsCommon.CustomAttribute)attr).type;
                    }
                    else if (attr.GetType() == typeof(ReadOnlyAttribute))
                    {
                        readOnly = true;
                    }
                    else if (attr.GetType() == typeof(ToolsCommon.NumericEditDeltaAttribute))
                    {
                        numericEditDelta = ((ToolsCommon.NumericEditDeltaAttribute)attr).delta;
                    }
                    else if (attr.GetType() == typeof(ToolsCommon.DynamicListKeyAttribute))
                    {
                        dynamicListKey = ((ToolsCommon.DynamicListKeyAttribute)attr).key;
                    }
                }

                if (skip) continue;

                var field = AddField(fi, o, toolTip, customType, numericEditDelta, dynamicListKey);
                field.IsEnabled = !readOnly;
            }
        }

        void WriteItem(FieldInfo fi, string subItem = "")
        {
            if (fi.FieldType.Name == "CategoryDummy")
                return;

            object value = GetFieldValue(fi.Name);
            string valueStr = value as string;
            Type fieldType = GetFieldType(fi.Name);
            if (fieldType == null || value == null)
                return;

            if (fi.FieldType.BaseType.Name == "Enum")
            {
                fi.SetValue(selectedObj, Enum.Parse(fieldType, valueStr));
            }
            else if (fi.FieldType.BaseType.Name.Contains("Collection"))
            {
            }
            else
            {
                try
                {
                    if (fi.FieldType == typeof(int))
                        fi.SetValue(selectedObj, int.Parse(valueStr));
                    else if (fi.FieldType == typeof(float))
                        fi.SetValue(selectedObj, float.Parse(valueStr));
                    else if (fi.FieldType == typeof(System.Double))
                    {
                        double val = System.Convert.ToDouble(value);
                        fi.SetValue(selectedObj, val);
                    }
                    else if (fi.FieldType == typeof(double))
                    {
                        fi.SetValue(selectedObj, double.Parse(valueStr));
                    }
                    else if (fi.FieldType == typeof(bool))
                        fi.SetValue(selectedObj, bool.Parse(valueStr));
                    else if (fi.FieldType == typeof(string))
                        fi.SetValue(selectedObj, value);
                    else if (fi.FieldType == typeof(Coord2))
                    {
                        string[] vs = valueStr.Split(new char[] { ',' });
                        Coord2 vec = new Coord2();
                        vec.x = int.Parse(vs[0]);
                        vec.z = int.Parse(vs[1]);

                        Coord2 old = (Coord2)fi.GetValue(selectedObj);
                        if (old.x != vec.x)
                            subItem = "x";
                        else if (old.z != vec.z)
                            subItem = "z";
                        else
                            subItem = "nc";

                        fi.SetValue(selectedObj, vec);
                    }
                    // experimental 
                    else if (fi.FieldType == typeof(Nullable<float>))
                        fi.SetValue(selectedObj, float.Parse(valueStr));
                    else if (fi.FieldType == typeof(Nullable<int>))
                        fi.SetValue(selectedObj, int.Parse(valueStr));
                    else if (fi.FieldType == typeof(Color))
                        fi.SetValue(selectedObj, value);
                }
                catch
                {
                }
                //else
                //throw new Exception("필드에 지원 안하는 타입이 존재함");
            }
        
            if (OnDataChanged != null)
                OnDataChanged(selectedObj, fi, subItem);
        }

        // 필드 타입별로 적절한 컨트롤을 만들어서 등록
        UIElement AddField(FieldInfo tfi, object value, string toolTip, string customType, float numericEditDelta, string dynamicListKey)
        {
            // 레이아웃 때문에, 각 필드별 구조가 초큼 복잡함. 
            // 보더
            //  ㄴ 스택패널
            //  ㄴ 택스트블럭 | 데이터별 엘리먼트

            StackPanel field = new StackPanel();
            field.Orientation = Orientation.Horizontal;
            if (toolTip.Length == 0)
                toolTip = tfi.Name;
            field.ToolTip = toolTip;
            field.Margin = new Thickness(1);
            field.Width = keyWidth + valueWidth + 10;

            TextBlock fieldName = new TextBlock();
            fieldName.Text = tfi.Name;
            if (tfi.Name == "Comment")
            {
                fieldName.Width = 0;
                field.Background = Brushes.OrangeRed;
            }
            else if (tfi.Name == "FixedComment")
            {
                fieldName.Width = 0;
                field.Background = Brushes.LawnGreen;
            }
            else
            {
                fieldName.Width = keyWidth;
                fieldName.Height = 20;
            }
            if (value == null)
            //if (tfi.FieldType == typeof(string) && value == null)
                value = "";

            fieldName.Tag = new object[] { tfi.Name, value.GetType() };
            field.Children.Add(fieldName);


            if (value.GetType().BaseType == typeof(Enum))
            {
                object[] attrs = value.GetType().GetCustomAttributes(Type.GetType("System.FlagsAttribute"), false);

                // normal enums
                if (attrs.Length == 0)
                {
                    ComboBox combo = new ComboBox();

                    // enum 값들을 쫙 뽑아서 콤보박스에 넣어주자!
                    foreach (FieldInfo fi in value.GetType().GetFields())
                    {
                        if (fi.FieldType.BaseType.Name == "Enum")
                            combo.Items.Add(fi.GetValue(value));
                    }
                    combo.Width = valueWidth;
                    combo.Height = 20;
                    combo.Tag = value;
                    combo.SelectedItem = value;//.ToString();
                    combo.SelectionChanged += new SelectionChangedEventHandler(delegate(object sender, SelectionChangedEventArgs e)
                    {
                        if (e.AddedItems.Count != 0)
                        {
                            (sender as ComboBox).Text = e.AddedItems[0].ToString();
                            //WriteItem(tfi);
                            WriteItem(tfi);
                        }
                    });
                    field.Children.Add(combo);
                }
                else // Bit Field Enums
                {
                    WrapPanel stackBits = new WrapPanel();
                    stackBits.Width = valueWidth;

                    // enum 값들을 쫙 뽑아서 콤보박스에 넣어주자!
                    foreach (FieldInfo fi in value.GetType().GetFields())
                    {
                        if (fi.FieldType.BaseType.Name == "Enum")
                        {
                            CheckBox check = new CheckBox();
                            check.Margin = new Thickness(2, 0, 2, 0);

                            // 아놔 복잡한 비트연산
                            int val = (int)value;
                            int fieldVal = (int)fi.GetValue(value);

                            if ((val & fieldVal) != 0)
                                check.IsChecked = true;
                            else
                                check.IsChecked = false;
                            check.Content = fi.GetValue(value);
                            stackBits.Children.Add(check);

                            check.Unchecked += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { WriteItem(tfi); });
                            check.Checked += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e) { WriteItem(tfi); });
                        }
                    }

                    field.Children.Add(stackBits);
                }
            }
            else if (value.GetType() == typeof(bool))
            {
                ComboBox combo = new ComboBox();

                // enum 값들을 쫙 뽑아서 콤보박스에 넣어주자!
                foreach (FieldInfo fi in value.GetType().GetFields())
                {
                    combo.Items.Add(fi.GetValue(value));
                }
                combo.Width = valueWidth;
                combo.Height = 20;
                combo.Tag = value;
                combo.Text = value.ToString();
                combo.SelectionChanged += new SelectionChangedEventHandler(delegate(object sender, SelectionChangedEventArgs e)
                {
                    (sender as ComboBox).Text = e.AddedItems[0].ToString();
                    WriteItem(tfi);
                });
                field.Children.Add(combo);
            }
            else if (value.GetType() == typeof(bool?))
            {
                ComboBox combo = new ComboBox();

                // enum 값들을 쫙 뽑아서 콤보박스에 넣어주자!
                foreach (FieldInfo fi in value.GetType().GetFields())
                {
                    combo.Items.Add(fi.GetValue(value));
                }
                combo.Width = valueWidth;
                combo.Height = 20;
                combo.Tag = value;
                combo.Text = value.ToString();
                combo.SelectionChanged += new SelectionChangedEventHandler(delegate(object sender, SelectionChangedEventArgs e)
                {
                    (sender as ComboBox).Text = e.AddedItems[0].ToString();
                    WriteItem(tfi);
                });
                field.Children.Add(combo);
            }
            else if (value.GetType() == typeof(Color))
            {
                var picker = new Xceed.Wpf.Toolkit.ColorPicker();
                picker.DisplayColorAndName = true;
                picker.UsingAlphaChannel = true;
                picker.SelectedColor = (Color)value;
                picker.Width = valueWidth;
                picker.SelectedColorChanged += new RoutedPropertyChangedEventHandler<Color>(delegate { WriteItem(tfi); });
                picker.UpdateLayout();
                field.Children.Add(picker);
            }
            else if (value.GetType() == typeof(System.Double))
            {
                Slider slider = new Slider();
                //string[] list = new string[2];
                //list = customType.Substring(7).Split(new char[] { ',' });
                slider.Width = 200;
                slider.Minimum = 0.0;// System.Convert.ToDouble(list[0]);
                slider.Maximum = 255.0;// System.Convert.ToDouble(list[1]);
                slider.Tag = value;
                slider.Value = (Double)value;
                slider.ValueChanged += new RoutedPropertyChangedEventHandler<Double>(delegate { WriteItem(tfi); });
                field.Children.Add(slider);
            }
            else if (value.GetType() == typeof(Coord2))
            {
                {
                    TextBox txtbox = new TextBox();
                    txtbox.Text = ((Coord2)value).x.ToString();
                    txtbox.Width = valueWidth/3 - 4;
                    txtbox.Margin = new Thickness(2,1,2,1);
                    txtbox.Tag = numericEditDelta;// (value as Coord2).x;
                    txtbox.TextChanged += new TextChangedEventHandler(delegate { WriteItem(tfi, "x"); });
                    txtbox.MouseDoubleClick += (s, e) => { txtbox.SelectAll(); };
                    txtbox.GotFocus += (s, e) => { txtbox.SelectAll(); };
                    txtbox.PreviewMouseLeftButtonDown += (s, e) => { if (txtbox.IsKeyboardFocusWithin == false) { e.Handled = true; txtbox.Focus(); } };
                    txtbox.PreviewKeyDown += Txtbox_KeyDown;
                    field.Children.Add(txtbox);
                }
                {
                    TextBox txtbox = new TextBox();
                    txtbox.Text = ((Coord2)value).z.ToString();
                    txtbox.Width = valueWidth / 3 - 4;
                    txtbox.Margin = new Thickness(2,1,2,1);
                    txtbox.Tag = numericEditDelta;// (value as Coord2).z;
                    txtbox.TextChanged += new TextChangedEventHandler(delegate { WriteItem(tfi, "z"); });
                    txtbox.MouseDoubleClick += (s, e) => { txtbox.SelectAll(); };
                    txtbox.GotFocus += (s, e) => { txtbox.SelectAll(); };
                    txtbox.PreviewMouseLeftButtonDown += (s, e) => { if (txtbox.IsKeyboardFocusWithin == false) { e.Handled = true; txtbox.Focus(); } };
                    txtbox.PreviewKeyDown += Txtbox_KeyDown;
                    field.Children.Add(txtbox);
                }
            }
            else
            {
                if (dynamicListKey != null)
                {
                    ComboBox combo = new ComboBox();
                    combo.IsEditable = true;
                    combo.ItemsSource = dynamicListMap[dynamicListKey];

                    combo.Width = valueWidth;
                    combo.Height = 20;
                    combo.Tag = value;
                    combo.SelectedItem = value;//.ToString();
                    combo.SelectionChanged += new SelectionChangedEventHandler(delegate (object sender, SelectionChangedEventArgs e)
                    {
                        if (e.AddedItems.Count != 0)
                        {
                            (sender as ComboBox).Text = e.AddedItems[0].ToString();
                            WriteItem(tfi);
                        }
                    });
                    field.Children.Add(combo);
                }
                else if (customType.StartsWith("Combo:"))
                {
                    ComboBox combo = new ComboBox();
                    combo.ItemsSource = customType.Substring(6).Split(new char[] { ',' }).ToList();

                    combo.Width = valueWidth;
                    combo.Height = 20;
                    combo.Tag = value;
                    combo.SelectedItem = value;//.ToString();
                    combo.SelectionChanged += new SelectionChangedEventHandler(delegate(object sender, SelectionChangedEventArgs e)
                    {
                        if (e.AddedItems.Count != 0)
                        {
                            (sender as ComboBox).Text = e.AddedItems[0].ToString();
                            WriteItem(tfi);
                        }
                    });
                    field.Children.Add(combo);
                }
                else if (customType.StartsWith("Browser:"))
                {
                    string fileExt = "";
                    string dlgFilter = "";
                    if (customType.Contains("tga"))
                    {
                        fileExt = ".tga";
                        dlgFilter = "Texture files|*.tga|All Files|*.*";
                    }
                    else if (customType.Contains("dds"))
                    {
                        fileExt = ".dds";
                        dlgFilter = "Texture files|*.dds;*.tga|All Files|*.*";
                    }
                    else if (customType.Contains("model"))
                    {
                        fileExt = ".model";
                        dlgFilter = "model or cloth files|*.model;*.hkt|All Files|*.*";
                    }
                    else if (customType.Contains("xml"))
                    {
                        fileExt = ".xml";
                        dlgFilter = "xml files|*.xml|All Files|*.*";
                    }
                    else if (customType.Contains("cbodyparams"))
                    {
                        fileExt = ".cbodyparams";
                        dlgFilter = "cbodyparams files|*.cbodyparams|All Files|*.*";
                    }
                    else if (customType.Contains("hkt"))
                    {
                        fileExt = ".hkt";
                        dlgFilter = "hkt files|*.hkt|All Files|*.*";
                    }
                        
                    TextBox txtbox = new TextBox();
                    txtbox.Text = value.ToString();
                    txtbox.Width = 100;
                    txtbox.TextWrapping = TextWrapping.NoWrap;
                    txtbox.Margin = new Thickness(2, 1, 2, 1);
                    txtbox.Tag = value;
                    txtbox.AllowDrop = true;
                    txtbox.TextChanged += new TextChangedEventHandler(delegate
                    {
                        WriteItem(tfi);
                    });
                    txtbox.MouseDoubleClick += (s, e) => { txtbox.SelectAll(); };
                    txtbox.GotFocus += (s, e) => { txtbox.SelectAll(); };
                    txtbox.PreviewMouseLeftButtonDown += (s, e) => { if (txtbox.IsKeyboardFocusWithin == false) { e.Handled = true; txtbox.Focus(); } };
                    txtbox.PreviewKeyDown += Txtbox_KeyDown;
                    field.Children.Add(txtbox);

                    //resource browser button
                    {
                        var btn = new Button();
                        btn.Content = "...";
                        btn.Width = 30;
                        btn.Tag = txtbox;
                        btn.Click += new RoutedEventHandler(delegate(object sender, RoutedEventArgs e)
                        {
                            Microsoft.Win32.OpenFileDialog dlg = new Microsoft.Win32.OpenFileDialog();
                            dlg.DefaultExt = fileExt;
                            dlg.Filter = dlgFilter;

                            Nullable<bool> r = dlg.ShowDialog();
                            if (r == true)
                            {
                                try
                                {
                                    //var item = (e.OriginalSource as FrameworkElement).Tag as Xceed.Wpf.Toolkit.PropertyGrid.PropertyItem;
                                    //Button item = e.OriginalSource as Button;
                                    //if (item != null)
                                    {
                                        (btn.Tag as TextBox).Text = dlg.FileName as string;
                                    }
                                }
                                catch { }
                            }
                        });
                        field.Children.Add(btn);
                    }
                }                 
                else if (customType.StartsWith("Color:"))
                {
                    var picker = new Xceed.Wpf.Toolkit.ColorPicker();
                    picker.SelectedColor = (Color)value;
                    picker.DisplayColorAndName = true;
                    picker.UsingAlphaChannel = true;
                    picker.Width = valueWidth;
                    picker.SelectedColorChanged += new RoutedPropertyChangedEventHandler<Color>(delegate { WriteItem(tfi); });
                    field.Children.Add(picker);
                }
                else
                {
                    TextBox txtbox = new TextBox();
                    txtbox.Text = value.ToString();
                    txtbox.Width = valueWidth;
                    txtbox.TextWrapping = TextWrapping.NoWrap;
                    txtbox.Margin = new Thickness(2, 1, 2, 1);
                    txtbox.Tag = numericEditDelta;// value;
                    txtbox.AllowDrop = true;
                    txtbox.TextChanged += new TextChangedEventHandler(delegate
                    {
                        WriteItem(tfi);
                    });
                    txtbox.MouseDoubleClick += (s, e) => { txtbox.SelectAll(); };
                    txtbox.GotFocus += (s, e) => { txtbox.SelectAll(); };
                    txtbox.PreviewMouseLeftButtonDown += (s, e) => { if (txtbox.IsKeyboardFocusWithin == false) { e.Handled = true; txtbox.Focus(); } };
                    txtbox.PreviewKeyDown += Txtbox_KeyDown;
                    field.Children.Add(txtbox);
                }
            }
            stackField.Children.Add(field);
            return field;
        }

        private void Txtbox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                TextBox txtbox = sender as TextBox;
                var numericEditDelta = txtbox.Tag as float?;
                if (numericEditDelta == null || numericEditDelta == 0)
                    return;

                float value;
                if (float.TryParse(txtbox.Text, out value))
                {
                    if ((Keyboard.GetKeyStates(Key.LeftShift) & KeyStates.Down) > 0)
                        numericEditDelta *= 10;
                    if ((Keyboard.GetKeyStates(Key.LeftCtrl) & KeyStates.Down) > 0)
                        numericEditDelta *= 0.1f;

                    if (e.Key == Key.Up)
                        value += numericEditDelta.Value;
                    if (e.Key == Key.Down)
                        value -= numericEditDelta.Value;

                    txtbox.Text = value.ToString();
                }
            }
        }
    }
}
