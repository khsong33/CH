﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleSimulatorStage : BattleVerifyStage
{
    public override IStageTroop CreateTroop(BattleTroop pBattleTroop)
	{
        BattleSimulatorStageTroop lStageTroop;
        if (mStageTroopPoolingQueue.Count <= 0)
            lStageTroop = new BattleSimulatorStageTroop();
		else
            lStageTroop = mStageTroopPoolingQueue.Dequeue() as BattleSimulatorStageTroop;

		lStageTroop.OnCreateTroop(this, pBattleTroop);

		return lStageTroop;
	}
}
