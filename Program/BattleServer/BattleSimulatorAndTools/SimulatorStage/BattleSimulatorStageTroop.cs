﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleSimulatorStageTroop : BattleVerifyStageTroop 
{
    public override void FireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay)
    {
        if (pFireEffect.aAttackTroop != null && pFireEffect.aTargetTroop != null)
            BattleSimulatorAndTools.EffectUpdater.AddEffect(pFireEffect.aAttackTroop.aCoord, pFireEffect.aTargetTroop.aCoord, pCooldownDelay / 1000, pFireEffect.aAttackTroop.aTeamIdx);

    }
}
