﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BattleSimulatorAndTools
{
    class ToolConfig
    {
        public const string c3dMapAssetFolder = @"..\..\..\..\Tools\TempMaps\";
        public const string cGridLayoutFolder = @"..\..\..\..\UnityClient\Assets\AssetData\Bundles\Resources\GridLayouts0";
    }
}
