"Rank","Level","NeedCard","Cost","Exp"
"1","1","2","5","4"
"1","2","4","20","5"
"1","3","10","50","6"
"1","4","20","150","10"
"1","5","50","400","25"
"1","6","100","1000","50"
"1","7","200","2000","100"
"1","8","400","4000","200"
"1","9","800","8000","400"
"1","10","2000","20000","800"
"1","11","5000","50000","1600"
"1","12","0","0","0"
"2","1","2","50","6"
"2","2","4","150","10"
"2","3","10","400","25"
"2","4","20","1000","50"
"2","5","50","2000","100"
"2","6","100","4000","200"
"2","7","200","8000","400"
"2","8","500","20000","800"
"2","9","1000","50000","1600"
"2","10","0","0","0"
"3","1","2","400","25"
"3","2","4","1000","50"
"3","3","10","2000","100"
"3","4","20","4000","200"
"3","5","50","8000","400"
"3","6","100","20000","800"
"3","7","300","50000","1600"
"3","8","0","0","0"
