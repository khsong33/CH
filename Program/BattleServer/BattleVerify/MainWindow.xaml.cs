﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using StackExchange.Redis;
using System.Threading;
// using System.Net.Json;
using CombatJson;

namespace BattleVerify
{
    /// <summary>
    /// MainWindow.xaml에 대한 상호 작용 논리
    /// </summary>
    public partial class MainWindow : Window
    {
		//-------------------------------------------------------------------------------------------------------
		// 메서드
		//-------------------------------------------------------------------------------------------------------
		// 생성자
		public MainWindow()
        {
            InitializeComponent ();
            Loaded += MainWindow_Loaded;

            mRedisManager = new RedisManager();
#if !BATTLE_VERIFY_TEST
            mRedisManager.aOnSubScribe += _OnSubScribe;
#endif
            mDBManager = new DBManager();

            mPlayerDataManager = new PlayerDataManager();
        }

		//-------------------------------------------------------------------------------------------------------
		// 구현
		//-------------------------------------------------------------------------------------------------------
		// 윈도우 로딩 콜백
        private void MainWindow_Loaded(object sender, RoutedEventArgs e)
        {
            VerifyLogger.mMainWindow = this;

			mBattleLogic = new BattleLogic();
			CompositionTarget.Rendering += VerifyLogger.sInstance.UpdateLogMessage;

			string[] lCommanderline = Environment.GetCommandLineArgs();
			if (lCommanderline.Length > 1)
			{
				String lRedisConnect = String.Empty;
				lRedisConnect = lCommanderline[1] + ":" + lCommanderline[2];
				if (mRedisManager.ConnectRedis(lRedisConnect))
				{
					VerifyLogger.sInstance.SetLog("Redis Connected!! - " + lRedisConnect);
				}
			}
			else
			{
				if (mRedisManager.ConnectRedis("localhost:38300"))
				{
					VerifyLogger.sInstance.SetLog("Default Redis Connected!!");
				}
			}
        }
		//-------------------------------------------------------------------------------------------------------
		// redis subscribe
        private void _OnSubScribe(string channel, string message)
        {
			//VerifyLogger.sInstance.StackLog("channel[" + (string)channel + "]/message[" + (string)message + "]");
			// 방 이름을 얻습니다.
			if (channel == "endgame")
			{
				//String lEndRoomName = (string)message;

				JSONObject lEndRoomData = new JSONObject(message);
				try
				{
					String lEndRoomName = lEndRoomData.GetField("RoomId").str;
					String lUserKey = lEndRoomData.GetField("UserKey").str;
					int lBattleCode = (int)lEndRoomData.GetField("BattleCode").n;
					JSONObject lVerifyResult = new JSONObject();

					// start... if do not verify processed
					//JSONObject lVerifyResultJson = new JSONObject();
					//lVerifyResultJson.AddField("IsValidResult", true);
					//JSONObject lVerifyRoomData = lVerifyResultJson;
					// end...

					JSONObject lVerifyData = _ThreadSafeVerifyBattle(lEndRoomName);

					JSONObject lVerifyRoomData = lVerifyData.GetField("BattleResult");
					JSONObject lVerifyRewards = lVerifyData.GetField("BattleReward");

					lVerifyRoomData.AddField("UserKey", lUserKey);
					lVerifyRoomData.AddField("BattleCode", lBattleCode);

					lVerifyResult.AddField("EndData", lVerifyRoomData);
					lVerifyResult.AddField("EndReward", lVerifyRewards);
					lVerifyResult.AddField("EndRoom", lEndRoomName);
				
					String lChannel = "game_result";
					String lMessage = lVerifyResult.ToString();
					mRedisManager.SetPublish(lChannel, lMessage);

					VerifyLogger.sInstance.StackLog("Pub : " + lChannel + " /Msg : " + lMessage);
				}
				catch (System.Exception ex)
				{
					VerifyLogger.sInstance.StackLog("publish exception - " + ex);
				}
			}
		}
		//-------------------------------------------------------------------------------------------------------
        // Redis conn 버튼 클릭
        private void ClickRedisConnect(object sender, RoutedEventArgs e)
        {
            if (mRedisManager.ConnectRedis())
            {
                VerifyLogger.sInstance.SetLog("Redis Connected!!");
            }
        }
		//-------------------------------------------------------------------------------------------------------
		// MSSQL conn 버튼 클릭
        private void ClickMSSQLConnect(object sender, RoutedEventArgs e)
        {
            if (mDBManager != null)
            {
                mDBManager.Connect();
                VerifyLogger.sInstance.SetLog("DB Connected!!");
            }
        }
		//-------------------------------------------------------------------------------------------------------
		// test button 클릭
        private void ClickTestButton(object sender, RoutedEventArgs e)
        {
			//JsonObject ActionJsonObject = mDBManager.GetGameActionData();
			//if (ActionJsonObject == null)
			//{
			//	VerifyLogger.sInstance.SetLog("Error : Not Json Data Parse!!");
			//	return;
			//}
			//List<TestPlayerFrameData> lPlayerData = mPlayerDataManager.Parse(ActionJsonObject);
			//mPlayerDataManager.PrintLog(lPlayerData);

			//string lParsingString = "{\"UserInfo\":{\"UniqueId\":1,\"UserName\":\"khsong33\",\"UserLevel\":1,\"GuildId\":0,\"Win\":0,\"Lose\":1,\"WinStreak\":0,\"Rating\":237,\"UseDeckId\":0,\"Decks\":{\"Count\":28,\"Items\":[{\"ItemNo\":1001,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":0},{\"ItemNo\":1002,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":1},{\"ItemNo\":1003,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":2},{\"ItemNo\":1004,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":3},{\"ItemNo\":1005,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":4},{\"ItemNo\":1006,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":5},{\"ItemNo\":1007,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":6},{\"ItemNo\":1008,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":7},{\"ItemNo\":1009,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":8},{\"ItemNo\":1010,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":9},{\"ItemNo\":1011,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":10},{\"ItemNo\":1012,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":11},{\"ItemNo\":1013,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":12},{\"ItemNo\":1014,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":13},{\"ItemNo\":1015,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":14},{\"ItemNo\":1016,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":15},{\"ItemNo\":1017,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":16},{\"ItemNo\":1018,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":17},{\"ItemNo\":1019,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":18},{\"ItemNo\":1020,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":19},{\"ItemNo\":1021,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":20},{\"ItemNo\":1022,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":21},{\"ItemNo\":1023,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":22},{\"ItemNo\":1024,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":23},{\"ItemNo\":1025,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":24},{\"ItemNo\":1026,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":25},{\"ItemNo\":1027,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":26},{\"ItemNo\":1028,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":27}]},\"UserKey\":\"jpNRHuOjEbSfjPkxAAAJ\",\"Ai\":\"FALSE\",\"UserIdx\":1},{\"UniqueId\":34,\"UserName\":\"mokbii3\",\"UserLevel\":1,\"GuildId\":0,\"Win\":0,\"Lose\":0,\"WinStreak\":0,\"Rating\":0,\"UseDeckId\":0,\"Decks\":{\"Count\":28,\"Items\":[{\"ItemNo\":1001,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":0},{\"ItemNo\":1002,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":1},{\"ItemNo\":1003,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":2},{\"ItemNo\":1004,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":3},{\"ItemNo\":1005,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":4},{\"ItemNo\":1006,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":5},{\"ItemNo\":1007,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":6},{\"ItemNo\":1008,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":7},{\"ItemNo\":1009,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":8},{\"ItemNo\":1010,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":9},{\"ItemNo\":1011,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":10},{\"ItemNo\":1012,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":11},{\"ItemNo\":1013,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":12},{\"ItemNo\":1014,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":13},{\"ItemNo\":1015,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":14},{\"ItemNo\":1016,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":15},{\"ItemNo\":1017,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":16},{\"ItemNo\":1018,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":17},{\"ItemNo\":1019,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":18},{\"ItemNo\":1020,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":19},{\"ItemNo\":1021,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":20},{\"ItemNo\":1022,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":21},{\"ItemNo\":1023,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":22},{\"ItemNo\":1024,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":23},{\"ItemNo\":1025,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":24},{\"ItemNo\":1026,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":25},{\"ItemNo\":1027,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":26},{\"ItemNo\":1028,\"ItemRank\":1,\"ItemLevel\":1,\"SlotId\":27}]},\"Ai\":\"TRUE\",\"UserIdx\":0}]}";

			//String lBattleRoomName = "12";
			//String lVerifyResultJsonString = _ThreadSafeVerifyBattle(lBattleRoomName);
			//VerifyLogger.sInstance.StackLog("lVerifyResultJsonString=" + lVerifyResultJsonString);
        }
		//-------------------------------------------------------------------------------------------------------
		// 쓰레드 안정성을 가지고 전투 결과를 검증합니다.
		private JSONObject _ThreadSafeVerifyBattle(String pBattleRoomName)
		{
			Monitor.Enter(mBattleLogic);
			try
			{
				return _VerifyBattle(pBattleRoomName);
			}
			finally
			{
				Monitor.Exit(mBattleLogic);
			}
		}
		//-------------------------------------------------------------------------------------------------------
		// 전투 결과를 검증합니다.
		private JSONObject _VerifyBattle(String pBattleRoomName, bool createLogger = true)
		{
			// 검증 데이터와 결과 담을 곳을 준비합니다.
			BattleVerifier lBattleVerifier = mBattleLogic.CreateVerifier();
			BattleVerifyData lBattleVerifyData = mBattleLogic.CreateVerifyData();

			// 검증 데이터에 대전 정보를 넣습니다.
			String lMatchinfoKey = String.Format("matchinfo:" + pBattleRoomName);
			String lMatchInfoJsonString = (String)mRedisManager.GetValue(1, lMatchinfoKey);
			//VerifyLogger.sInstance.StackLog(lMatchInfoJsonString);
			JSONObject lMatchInfoJson = new JSONObject(lMatchInfoJsonString);
			lBattleVerifyData.aMatchInfo.InitFromJson(lMatchInfoJson);

			// 검증 데이터에 유저 입력 정보를 넣습니다.
			String lBattleLogKey = String.Format("battlelog:{0}", pBattleRoomName);
			RedisValue[] lBattleLogs = mRedisManager.GetListRange(1, lBattleLogKey);
			for (int iLog = 0; iLog < lBattleLogs.Length; iLog++)
			{
				String lFrameInputJsonString = (String)lBattleLogs[iLog];
				//VerifyLogger.sInstance.StackLog(lFrameInputJsonString);
				lBattleVerifyData.AddFrameInput(lFrameInputJsonString);
			}
			lBattleVerifyData.aFrameInputList.Sort();

			// 검증합니다.
			bool lIsValidResult = lBattleVerifier.VerifyData(lBattleVerifyData, createLogger);

			// 검증 결과를 만듭니다.
			JSONObject lVerifyDataJson = new JSONObject();
			JSONObject lVerifyRewardListJson = new JSONObject(JSONObject.Type.ARRAY);

			for (int iUser = 0; iUser < lBattleVerifyData.aMatchInfo.mUserCount; ++iUser)
			{
				BattleUser lUser = lBattleVerifier.aUsers[iUser];
				if (lUser.aUserInfo.mIsAi) // 상대가 AI일경우
					continue;
				if (lUser.aUserInfo.mClientGuid == 0) // tutorial enemy일 경우
					continue;

				BattleProtocol.BattleEndCode lBattleEndCode = lBattleVerifier.aProgressInfo.aTeamBattleEndCodes[lUser.aTeamIdx];

				JSONObject lRewardInfoJson;
				if (TutorialTable.get.IsTutorial(lBattleVerifyData.aMatchInfo.aStageSpec.mNo))
					lRewardInfoJson = lUser.CalcBattleEndTutorialReward(lBattleEndCode);
				else
					lRewardInfoJson = lUser.CalcBattleEndPvPReward2(lBattleEndCode);

				lRewardInfoJson.AddField("UniqueId", lUser.aUserInfo.mClientGuid);

				lVerifyRewardListJson.Add(lRewardInfoJson);
				
				VerifyLogger.sInstance.StackLog("lRewardInfoJson[" + iUser + "] = " + lRewardInfoJson);
			}

			JSONObject lVerifyBattleResultJson = new JSONObject();
			lVerifyBattleResultJson.AddField("IsValidResult", lIsValidResult);
			
			lVerifyDataJson.AddField("BattleResult", lVerifyBattleResultJson);
			lVerifyDataJson.AddField("BattleReward", lVerifyRewardListJson);

			// 검증에 사용된 자원을 파괴합니다.
			lBattleVerifyData.Destroy();
			lBattleVerifier.Destroy();

			return lVerifyDataJson;
		}

		//-------------------------------------------------------------------------------------------------------
		// 데이터
		//-------------------------------------------------------------------------------------------------------
		private RedisManager mRedisManager = null;
		private DBManager mDBManager = null;
		private PlayerDataManager mPlayerDataManager = null;

		private BattleLogic mBattleLogic;

		private void Test_Button_Click(object sender, RoutedEventArgs e)
		{
			VerifyLogger.sInstance.SetLog("Test Button Click");

			String lRoomName = this.BattleRoomName.Text;
			_VerifyBattle(lRoomName);
		}

        private void TestRoomResultAll_Click(object sender, RoutedEventArgs e)
        {
            TestAll.IsEnabled = false;
#if BATTLE_VERIFY_TEST
            BackgroundWorker lWorker = new BackgroundWorker();
            lWorker.WorkerSupportsCancellation = true;
            lWorker.WorkerReportsProgress = true;
            lWorker.DoWork += TestRoomResultAll;
            lWorker.ProgressChanged += TestAllWorker_ProgressChanged;
            lWorker.RunWorkerCompleted += TestAllWorker_RunWorkerCompleted;
            lWorker.RunWorkerAsync(TestCount.Text);
#endif
        }
#if BATTLE_VERIFY_TEST

        private void TestAllWorker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {
            if ((e.Cancelled == true))
            {
                TestResult.Items.Add("Error ");
            }
            else if (!(e.Error == null))
            {
                TestResult.Items.Add("Error: " + e.Error.Message);
            }

            TestAll.IsEnabled = true;
        }

        private void TestAllWorker_ProgressChanged(object sender, ProgressChangedEventArgs e)
        {
            TestResult.Items.Add(e.UserState);
        }

        private void TestRoomResultAll(object sender, DoWorkEventArgs e)
        {
            var lWorker = sender as BackgroundWorker;

            Dictionary<string, List<string>> mMapKeyResultMD5List = new Dictionary<string, List<string>>();

            int lTestCount = 2;
            int.TryParse(e.Argument as string, out lTestCount);

            var lKeys = mRedisManager.GetKeys(1, "battlelog:*");

            for (int i = 0; i < lTestCount; i++)
            {
                for (int k = 0; k < lKeys.Count; k++)
                {
                    var ms = new System.IO.MemoryStream();
                    var logger = new StreamLogger(ms);
                    Logger.SetInstance(logger);

                    var roomKey = lKeys[k].Substring(10);
                    _VerifyBattle(roomKey, false);

                    using (var md5 = System.Security.Cryptography.MD5.Create())
                    {
                        if (mMapKeyResultMD5List.ContainsKey(roomKey) == false)
                            mMapKeyResultMD5List.Add(roomKey, new List<string>());

                        ms.Position = 0;
                        //byte[] buffer = new byte[ms.Length];
                        //ms.Read(buffer, 0, (int)ms.Length);
                        //var hash = System.Text.Encoding.UTF8.GetString(buffer);
                        var hash = BitConverter.ToString(md5.ComputeHash(ms));
                        mMapKeyResultMD5List[roomKey].Add(hash);

                        int percent = (i * lKeys.Count + k + 1) * 100 / (lKeys.Count * lTestCount);

                        bool logged = false;
                        for (int test = mMapKeyResultMD5List[roomKey].Count - 1; test >= 1; test--)
                        {
                            if (mMapKeyResultMD5List[roomKey][test] != mMapKeyResultMD5List[roomKey][test - 1])
                            {
                                logged = true;
                                lWorker.ReportProgress(percent, percent.ToString() + "%, " + roomKey + ", #" + i + ", has mismatch");
                                break;
                            }
                        }

                        if (logged == false)
                            lWorker.ReportProgress(percent, percent.ToString() + "%, " + roomKey + ", #" + i + ", " + hash);
                    }

                    logger.Dispose();
                    Logger.SetInstance(null);
                }
            }

            foreach (var key in lKeys)
            {
                for (int i = 0; i < lTestCount - 1; i++)
                {
                    var roomKey = key.Substring(10);

                    if (mMapKeyResultMD5List[roomKey][i] != mMapKeyResultMD5List[roomKey][i + 1])
                    {
                        lWorker.ReportProgress(100, "Error in key:" + key);
                        break;
                    }
                }
            }

            lWorker.ReportProgress(100, "All Ok");
        }
#endif
    }
}
