﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Json;

public class PlayerDataManager
{
    //public List<TestPlayerFrameData> mFrameDataSet;
    public List<TestPlayerFrameData> Parse(JsonObject pPlayerFrameData)
    {
        JsonObjectCollection ActionJsonCollection = (JsonObjectCollection)pPlayerFrameData;
        TestPlayerFrameData lData = new TestPlayerFrameData();
        List<JsonObject> logList = (List<JsonObject>)ActionJsonCollection["logs"].GetValue();
        List<TestPlayerFrameData> lFrameDataSet = new List<TestPlayerFrameData>();
        for (int iLog = 0; iLog < logList.Count; iLog++)
        {
            TestPlayerFrameData lFrameData = new TestPlayerFrameData();
            JsonObjectCollection lLogData = (JsonObjectCollection)logList[iLog];
            Double lFrame = (Double)lLogData["frame"].GetValue();
            lFrameData.mFrame = (int)lFrame;
            List<JsonObject> inputDataList = (List<JsonObject>)lLogData["input"].GetValue();
            for (int iInput = 0; iInput < inputDataList.Count; iInput++)
            {
                JsonObjectCollection inputData = (JsonObjectCollection)inputDataList[iInput];
                String lHeader = (String)inputData["Header"].GetValue();
                if (lHeader.ToUpper().Equals("INPUT"))
                {
                    TestPlayerInputData lPlayerInputData = new TestPlayerInputData();
                    lPlayerInputData.mType = (int)((Double)inputData["Type"].GetValue());
                    lPlayerInputData.mClientId = (int)((Double)inputData["ClientId"].GetValue());
                    lPlayerInputData.mX = (int)((Double)inputData["X"].GetValue());
                    lPlayerInputData.mY = (int)((Double)inputData["Y"].GetValue());
                    lPlayerInputData.mA = (int)((Double)inputData["A"].GetValue());
                    lPlayerInputData.mB = (int)((Double)inputData["B"].GetValue());
                    lFrameData.mPlayerDataList.Add(lPlayerInputData);
                }
                else if (lHeader.ToUpper().Equals("BLITZ"))
                {
                    TestPlayerBlitzData lBlitzData = new TestPlayerBlitzData();
                    lBlitzData.mClientId = (int)((Double)inputData["ClientId"].GetValue());
                    lBlitzData.mBlitzTroopId = (int)((Double)inputData["BlitzTroopId"].GetValue());
                    lBlitzData.mPointCoordX = (int)((Double)inputData["PointCoordX"].GetValue());
                    lBlitzData.mPointCoordZ = (int)((Double)inputData["PointCoordZ"].GetValue());
                    lBlitzData.mGaugeCount = (int)((Double)inputData["GaugeCount"].GetValue());
                    lBlitzData.mTargetTroopCount = (int)((Double)inputData["TargetTroopCount"].GetValue());
                    List<JsonObject> lTargetTroopIdsObj = (List<JsonObject>)inputData["TargetTroopIds"].GetValue();
                    for (int iTroopTarget = 0; iTroopTarget < lTargetTroopIdsObj.Count; iTroopTarget++)
                    {
                        Double lTroopTargetId = (Double)lTargetTroopIdsObj[iTroopTarget].GetValue();
                        lBlitzData.mTargetTroopIds.Add((int)lTroopTargetId);
                    }
                    lFrameData.mPlayerBlitzList.Add(lBlitzData);
                }
            }
            lFrameDataSet.Add(lFrameData);
        }

        return lFrameDataSet;
    }
    public void PrintLog(List<TestPlayerFrameData> pFrameData)
    {
        for (int iFrame = 0; iFrame < pFrameData.Count; iFrame++)
        {
			VerifyLogger.sInstance.StackLog("Process Input Data - Frame : " + pFrameData[iFrame].mFrame);
        }
    }
}

public class TestPlayerFrameData
{
    public int mFrame;
    public List<TestPlayerInputData> mPlayerDataList;
    public List<TestPlayerBlitzData> mPlayerBlitzList;

    public TestPlayerFrameData()
    {
        mPlayerDataList = new List<TestPlayerInputData>();
        mPlayerBlitzList = new List<TestPlayerBlitzData>();
    }
}
public class TestPlayerBlitzData
{
    public string mHeader;
    public int mClientId;
    public int mBlitzTroopId;
    public int mPointCoordX;
    public int mPointCoordZ;
    public int mGaugeCount;
    public int mTargetTroopCount;
    public List<int> mTargetTroopIds;

    public TestPlayerBlitzData()
    {
        mTargetTroopIds = new List<int>();
    }
}
public class TestPlayerInputData
{
    public string mHeader;
    public int mType;
    public int mClientId;
    public int mX;
    public int mY;
    public int mA;
    public int mB;

    public TestPlayerInputData()
    {

    }
}