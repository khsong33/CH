﻿//////////////////////////////////////////////////////////////////////////
// lagacy code - this project do not using mssql
//////////////////////////////////////////////////////////////////////////
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;
using System.Net.Json;

class DBManager
{
    public void Connect()
    {
        mConn = new SqlConnection(m_strSqlAddr);
        try
        {
            mConn.Open();
        }
        catch (System.Exception ex)
        {
            VerifyLogger.sInstance.SetLog("Error : DB Access Failed - " + ex);
        }
    }
    public JsonObject GetGameActionData(String pEndRoomName = "room0")
    {
        if (mConn == null)
            return null;
        try
        {
            string query = "GetJsonData";
            SqlCommand command = new SqlCommand(query, mConn);
            command.CommandType = CommandType.StoredProcedure;
            command.Parameters.AddWithValue("@inRoomId", pEndRoomName);
            //if (reader != null && !reader.IsClosed)
            //    reader.Close();
            reader = command.ExecuteReader();
            if (reader.Read())
            {
                //VerifyLogger.sInstance.SetLog(reader["json"].ToString());
                JsonTextParser jsonParse = new JsonTextParser();
                JsonObject jsonObject = jsonParse.Parse(reader["json"].ToString());
                reader.Close();
                return jsonObject;
            }
            return null;
            //SqlDataAdapter adapter = new SqlDataAdapter(command);
            //DataSet dataSet = new DataSet();
            //adapter.Fill(dataSet);
            //if (dataSet.Tables.Count > 0)
            //{
            //    foreach (DataRow row in dataSet.Tables[0].Rows)
            //    {
            //        //Console.WriteLine(row["json"].ToString());
            //        JsonTextParser parser = new JsonTextParser();
            //        JsonObject obj = parser.Parse(row["json"].ToString());
            //        JsonObjectCollection col = (JsonObjectCollection)obj;
            //        VerifyLogger.sInstance.SetLog(row["json"].ToString());
            //    }
            //}
        }
        catch (System.Exception ex)
        {
			VerifyLogger.sInstance.StackLog("Exception - " + ex);
            return null;
        }

    }
    private string m_strSqlAddr = "server=ffdev;uid=chdb;pwd=ffdev123!;database=CH_gamedb;connection timeout = 30";
    //private string m_strSqlAddr = "server=dbmsdev1.nwz.kr,14334;uid=saych;pwd=saych!!!;database=CH_KR_GAME;connection timeout = 30";
    private SqlConnection mConn = null;
    private SqlDataReader reader;
}
