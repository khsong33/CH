﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StackExchange.Redis;

class RedisManager
{
    public delegate void OnSubscribe(string channel, string message);
    public OnSubscribe aOnSubScribe { get; set; }

    public string m_strConnectInfo = "localhost:6264";
    
    public bool ConnectRedis()
    {
        if (mRedis == null || !mRedis.IsConnected)
        {
            mRedis = ConnectionMultiplexer.Connect(m_strConnectInfo);
            SetSubscribe();
            return true;
        }
        return false;
    }
	public bool ConnectRedis(string pConnectInfo)
	{
		if (mRedis == null || !mRedis.IsConnected)
		{
			mRedis = ConnectionMultiplexer.Connect(pConnectInfo);
			mRedisServer = mRedis.GetServer(pConnectInfo);
			SetSubscribe();
			return true;
		}
		return false;
	}

    public void SetSubscribe()
    {
        ISubscriber sub = mRedis.GetSubscriber();
        sub.Subscribe("testMessage", (channel, message) =>
        {
            //Logger.sInstance.SetLog("message - " + (string)message + " /channel - " + (string)channel);
            if(aOnSubScribe != null)
                aOnSubScribe((string)channel, (string)message);
        });
        sub.Subscribe("endgame", (channel, message) =>
        {
            if(aOnSubScribe != null)
                aOnSubScribe((string)channel, (string)message);
        });
    }
	public void SetPublish(String channel, String message)
	{
		ISubscriber pub = mRedis.GetSubscriber();
		pub.Publish(channel, message);
	}
	public RedisValue[] GetListRange(int pSelectDB, String key, long start = 0, long stop = -1)
	{
		IDatabase lBattleLogRedis = mRedis.GetDatabase(pSelectDB);
		RedisKey lBattleLogKey = key;
		RedisValue[] lBattleLogValue = lBattleLogRedis.ListRange(lBattleLogKey, start, stop);
		return lBattleLogValue;
	}
	public RedisValue GetValue(int pSelectDB, String key)
	{
		IDatabase lBattleLogRedis = mRedis.GetDatabase(pSelectDB);
		RedisKey lBattleLogKey = key;
		RedisValue lBattleLog = lBattleLogRedis.StringGet(lBattleLogKey);
		return lBattleLog;
	}
	public List<String> GetKeys(int pSelectDB, String pattern)
	{
		var keys = mRedisServer.Keys(pSelectDB, pattern);
		List<String> keylist = new List<String>();
		foreach(var key in keys)
		{
			keylist.Add(key.ToString());
		}
		return keylist;
	}
    private ConnectionMultiplexer mRedis = null;
	private IServer mRedisServer = null;
}
