﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BattleVerify;
using System.Windows;
using System.Windows.Controls;
using StackExchange.Redis;

public class VerifyLogger
{
    // 구성요소
    public static MainWindow mMainWindow = null;
	public static VerifyLogger mVerifyLogger = null;
	public static VerifyLogger sInstance
    {
        get 
        {
            if (mVerifyLogger == null)
				mVerifyLogger = new VerifyLogger();
            return mVerifyLogger;
        }
    }
    // 동기 로그 메시지
    public void SetLog(string pMessage)
    {
        Log lLogMessage = new Log();
        lLogMessage.m_strLogMessage = pMessage;
        lLogMessage.m_strLogTime = DateTime.Now.ToString();
        mMainWindow.LogWindow.Items.Add(lLogMessage);
    }
    // 로그 메시지 남김
	public void StackLog(string pMessage)
    {
        Log lLogMessage = new Log();
        lLogMessage.m_strLogMessage = pMessage;
        lLogMessage.m_strLogTime = DateTime.Now.ToString();
        m_AsyncLogMessage.Enqueue(lLogMessage);
    }
    // 로그화면 갱신(비동기 메시지 처리)
    public void UpdateLogMessage(object sender, EventArgs e)
    {
        if (m_AsyncLogMessage.Count <= 0)
            return;

        Log lLog = m_AsyncLogMessage.Dequeue();
        mMainWindow.LogWindow.Items.Add(lLog);
    }
    // 데이터
    public Queue<Log> m_AsyncLogMessage = new Queue<Log>();
}

public class Log
{
    public string m_strLogMessage { get; set; }
    public string m_strLogTime { get; set; }
}