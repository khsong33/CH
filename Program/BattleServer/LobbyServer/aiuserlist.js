﻿var redisClient = require('./store_redis');
var mssql = require('./mssql');
var random = require('./random');

var AIListRatingRange = [
	{ 'min' : 0, 'max' : 500 },
	{ 'min' : 501, 'max' : 1000 }
];
var AiUserListCallCount = 10;
var AiUserListCallTimeSec = 100;
// AI User List를 초기화합니다.
function AddAiListFromSQL() {
	for (var iList = 0; iList < AIListRatingRange.length; iList++) {
		mssql.getAIList(AiUserListCallCount, AIListRatingRange[iList].min, AIListRatingRange[iList].max, AiUserListCallTimeSec, SetAiList, true);
	}
}
exports.AddAiListFromSQL = AddAiListFromSQL;
// AI User List 중 로그인한 유저를 리스트에서 제외합니다.
function RemoveLoginUser(uniqueId, rating) {
	var findRatingIndex = FindRatingRangeIndex(rating);
	var aiUserFindKey = 'AiUser:' + String(AIListRatingRange[findRatingIndex].min) + "," + String(AIListRatingRange[findRatingIndex].max) + ':' + String(uniqueId);
	redisClient.ai_user_redis.del(aiUserFindKey);
}
exports.RemoveLoginUser = RemoveLoginUser;
// 로그 아웃한 유저를 AI user list에 추가합니다.
function AddLogoutUser(userInfo) {
	mssql.getUserCommanderItem(userInfo, userInfo.UseDeckId, function (ai_userinfo_cb, commanderItem) {
		ai_userinfo_cb.Decks = commanderItem;
		var findRatingIndex = FindRatingRangeIndex(userInfo.Rating);
		var aiUserKey = 'AiUser:' + String(AIListRatingRange[findRatingIndex].min) + "," + String(AIListRatingRange[findRatingIndex].max) + ':' + String(userInfo.UniqueId);
		redisClient.ai_user_redis.set(aiUserKey, JSON.stringify(ai_userinfo_cb));
	});
}
exports.AddLogoutUser = AddLogoutUser;
// AI 유저를 선택합니다.
function SelectAIUser(rating, count, cbAIUserInfo) {
	var RatingIndex = FindRatingRangeIndex(rating);
	var minRat = AIListRatingRange[RatingIndex].min;
	var maxRat = AIListRatingRange[RatingIndex].max;
	var aiUserFindKey = 'AiUser:' + String(minRat) + "," + String(maxRat) + ':*';
	
	var currentCount = 0;
	
	var aiUserInfos = [];
	
	redisClient.ai_user_redis.keys(aiUserFindKey, function (err, aiuserListKeys) {
		for (var iCount = 0; iCount < count; iCount++) {
			var selectIndex = random.randomRange(0, Number(aiuserListKeys.length));
			redisClient.ai_user_redis.get(aiuserListKeys[selectIndex], function (err, userInfo) {
				var userInfoJSON = JSON.parse(userInfo);
				aiUserInfos.push(userInfo);
				// 기존 키는 삭제
				aiuserListKeys.splice(selectIndex, 1);
				currentCount++;
				
				if (currentCount >= count) {
					// 인원이 5명 이하로 줄어들면 새로운 인원을 추가합니다.
					if (aiuserListKeys.length < 5) {
						var ratingIndex = FindRatingRangeIndex(userInfoJSON.Rating);
						mssql.getAIList(AiUserListCallCount, AIListRatingRange[ratingIndex].min, AIListRatingRange[ratingIndex].max, AiUserListCallTimeSec, SetAiList, false);
					}
					cbAIUserInfo(aiUserInfos);
				} // end of if(currentCount >= count)
			}); // end of redis get
			var arr = aiuserListKeys[selectIndex].split(':');
			redisClient.ai_user_redis.rename(aiuserListKeys[selectIndex], 'aiUseUser:' + arr[2]); // 비동기 처리이기 때문에 get안에서 변경할 수 없다.(selectIndex 값이 변함)
		} // end of for
	}); // end of keys
}
exports.SelectAIUser = SelectAIUser;

// 해당 rating에 맞는 rating index를 찾아줍니다.
function FindRatingRangeIndex(rating) {
	for (var iList = 0; iList < AIListRatingRange.length; iList++) {
		if (rating >= AIListRatingRange[iList].min && rating <= AIListRatingRange[iList].max) {
			return iList;
		}
	}
}
// redis에 ai user list를 셋팅합니다.
function SetAiList(aiUserList, minRat, maxRat, isOverwrite) {
	if (isOverwrite == true) {
		var aiUserFindKey = 'AiUser:' + String(minRat) + "," + String(maxRat) + ':*';
		redisClient.ai_user_redis.keys(aiUserFindKey, function (err, values) {
			for (var iIndex = 0; iIndex < values.length; iIndex++) {
				redisClient.ai_user_redis.del(values[iIndex]);
			}
			for (var aiUser = 0; aiUser < aiUserList.Users.length; aiUser++) {
				var aiUserAddKey = 'AiUser:' + String(minRat) + "," + String(maxRat) + ':' + aiUserList.Users[aiUser].UniqueId;
				redisClient.ai_user_redis.set(aiUserAddKey, JSON.stringify(aiUserList.Users[aiUser]));
			}
		});
	}
	else {
		for (var aiUser = 0; aiUser < aiUserList.Users.length; aiUser++) {
			var aiUserAddKey = 'AiUser:' + String(minRat) + "," + String(maxRat) + ':' + aiUserList.Users[aiUser].UniqueId;
			redisClient.ai_user_redis.set(aiUserAddKey, JSON.stringify(aiUserList.Users[aiUser]));
		}
	}
}