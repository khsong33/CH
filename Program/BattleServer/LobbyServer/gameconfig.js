﻿var max_commander_deck = 10;
exports.max_commander_deck = max_commander_deck;

var max_deck_commander_count = 10;
exports.max_deck_commander_count = max_deck_commander_count;

var maxRoomMember = 2;
exports.maxRoomMember = maxRoomMember;

var max_log_out_update_user = 0;
exports.max_log_out_update_user = max_log_out_update_user;

var max_nation_count = 4; // 중립국 : 0, US : 1, Gemany : 2, Soviet : 3, Count : 4
exports.max_nation_count = max_nation_count;