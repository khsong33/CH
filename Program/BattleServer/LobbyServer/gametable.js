﻿var fs = require('fs');

var commanderItemTable;
function getCommanderItemTable() {
    if (commanderItemTable == undefined) {
        _loadCommanderItem();
    }
    return commanderItemTable;
}
exports.getCommanderItemTable = getCommanderItemTable;

function LoadTable() {
    if (commanderItemTable == undefined) {
        _loadCommanderItem();
    }
}
exports.LoadTable = LoadTable;

function _loadCommanderItem() {
    var data = fs.readFileSync('./data/CommanderTableCSV.txt', 'ucs2');
    var result = _loadCsv(data.toString(), 0);

    commanderItemTable = new Object();
    for (var row in result) {
        var info = new Object;
        info.name = String(result[row]['Name']);
        info.specNo = Number(result[row]['No']);
        info.rankupExp = new Array();

        commanderItemTable[info.specNo] = info;
    }
 }

//============================================================================
// loadCsv
// csv 파서
//============================================================================
function _loadCsv(csv, headerRow) {
    var i, j, line, trimL, trimR;
    var result;
    var data;
    var header;
    
    trimL = /^[\s\xA0\"]+/;
    trimR = /[\s\xA0\"]+$/;
    
    line = csv.split('\n');
    
    var count = line.length;
    
    if (line[count - 1] == '')
        count = count - 1;
    
    data = new Array(count);
    
    for (i = 0; i < count; ++i) {
        if (headerRow >= 0) {
            if (i == 0) //header
            {
                header = new Array();
                header = line[0].split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
                //console.log(header);
                j = header.length;
                while (j--) {
                    header[j] = header[j].replace(trimL, '').replace(trimR, '');
                }
            }
            else {
                var data_idx = i - 1;
                data[data_idx] = line[i].split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
                for (j in data[data_idx]) {
                    data[data_idx][j] = data[data_idx][j].replace(trimL, '').replace(trimR, '');
                }
            }
        }
        else {
            data[i] = line[i].split(/,(?=(?:[^\"]*\"[^\"]*\")*(?![^\"]*\"))/);
            j = data[i].length;
            while (j--)
                data[i][j] = data[i][j].replace(trimL, '').replace(trimR, '');
        }

    }
    
    result = new Array();
    
    for (var data_i in data) {
        var headers = new Array();
        
        for (var head_i in header) {
            
            var header_name = header[head_i];
            
            headers[header_name] = data[data_i][head_i];
        }
        result[data_i] = headers;
    }
    
    return result;
}

