﻿var logger = require('./logger');
var redisClient = require('./store_redis');
var mssql = require('./mssql');
var gameconfig = require('./gameconfig');
var gametable = require('./gametable');

// 새로운 지휘관 아이템이 들어왔을 경우 처리합니다.
function AddCommanderItem(uniqueId, itemNoList, cb_addcommander) {
    var find_user_commander_item = 'uidcommander:' + uniqueId;
    mssql.AddCommanderItem(uniqueId, itemNoList, function (addItemInfoList) {
        var itemInfoCount = addItemInfoList.Items.length;
        for (var iItem = 0; iItem < itemInfoCount; iItem++) {
            var itemInfoJson = {
                'ItemId' : Number(addItemInfoList.Items[iItem].ItemId),
                'ItemNo' : Number(addItemInfoList.Items[iItem].ItemNo),
                'ItemRank' : 1,
                'ItemExp' : 0,
            };
            redisClient.store_redis.hmset(find_user_commander_item, String(addItemInfoList.Items[iItem].ItemId), JSON.stringify(itemInfoJson));
        }
        cb_addcommander(itemInfoJson);
    });
}
exports.AddCommanderItem = AddCommanderItem;

// 지휘관 아이템의 경험치를 업데이트합니다.
function UpdateBattleRewardCommanderItem(uniqueId, battle_reward_data, cb_reward_db_query) {
    var find_user_commander_item = 'uidcommander:' + uniqueId;
    var update_exp_commander_count = battle_reward_data.CommanderExp.length;
    var result_exp_data = '';

    var add_commander_count = 0;
    var result_add_data = '';
    if (battle_reward_data.RewardCommander != null) {
        add_commander_count = battle_reward_data.RewardCommander.length;
        // 추가 지휘관을 저장합니다.
        for (var iAddItem = 0; iAddItem < add_commander_count; iAddItem++) {
            result_add_data += (battle_reward_data.RewardCommander[iAddItem] + ',');
        }
    }
    
    // 지휘관 경험치 데이터를 처리합니다.
    var current_update_idx = 0;
    for (var iItem = 0; iItem < update_exp_commander_count; iItem++) {
        var update_item_data = battle_reward_data.CommanderExp[iItem];
        redisClient.store_redis.hmget(find_user_commander_item, update_item_data.ItemId, function (err, itemData) {
            var itemDataJson = JSON.parse(itemData);
            
            itemDataJson.ItemExp = battle_reward_data.CommanderExp[current_update_idx].Exp;
            itemDataJson.ItemRank = battle_reward_data.CommanderExp[current_update_idx].Rank;
            
            redisClient.store_redis.hmset(find_user_commander_item, itemDataJson.ItemId, JSON.stringify(itemDataJson));

            result_exp_data += (itemDataJson.ItemId + '|' + itemDataJson.ItemRank + '|' + itemDataJson.ItemExp + ',');
            current_update_idx++;
            if (current_update_idx >= update_exp_commander_count) {
                cb_reward_db_query(result_exp_data, result_add_data);
            }
        });
    }
}
exports.UpdateBattleRewardCommanderItem = UpdateBattleRewardCommanderItem;