﻿var mssql = require('mssql');
var logger = require('./logger');
var gameconfig = require('./gameconfig');

var kind_server = process.argv[4];

var configQA = {
    user: 'saych',
    password: 'saych!!!',
    server: 'dbmsdev1.nwz.kr', 
    port: '14334',
    database: 'CH_KR_GAME',
    pool: { idelTimeout : 30 }
}
var configffdev = {
    user: 'chdb',
    password: 'ffdev123!',
    server: 'ffdev', 
    database: 'CH_gamedb',
    pool: { idleTimeout : 30 }
}
// DB 초기화 및 연결
var connection = null;

function InitSql(cb_connect) {
    if (kind_server == 'QA') {
        connection = new mssql.Connection(configQA, function (err) {
            if (err) {
                console.log("mssql connection err - " + err);
            }
            else {
                console.log("connect success QA db");
                cb_connect();
            }
        });
    }
    else if (kind_server == 'Local') {
        connection = new mssql.Connection(configffdev, function (err) {
            if (err) {
                console.log("mssql connection err - " + err);
            }
            else {
                console.log("connect success Dev db");
                cb_connect();
            }
        });
    }
}
exports.InitSql = InitSql;

// 유저 정보 불러오기
function getUserLogin(username, cb_userinfo) {
    var request = new mssql.Request(connection);
    request.input('UserName', username);
    request.execute('LoginCH2User', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute LoginCH2User");
        }
        
        var userinfo_json = _ConvertUserInfoDBtoJson(recordsets[0], 0);
        cb_userinfo(userinfo_json);
    })
}
exports.getUserLogin = getUserLogin;
// 유저 카드 정보 가져오기 (전체덱을 가져오려면 find_deck_id에 0보다 작은값을 넣습니다)
function getUserCommanderItem(userinfo_json, find_deck_id, cb_commanderitem) {
    var request = new mssql.Request(connection);
    request.input('UniqueId', userinfo_json.UniqueId);
    request.input('UseDeckId', find_deck_id);
    request.execute('GetCH2CommanderItem', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2CommanderItem");
        }
        var user_commander = _ConvertCommanderItemDBtoJson(recordsets[0], userinfo_json.UniqueId);
        cb_commanderitem(userinfo_json, user_commander)
    })
}
exports.getUserCommanderItem = getUserCommanderItem;
// User Deck 정보 얻어오기
function getUserDeck(userinfoJson, cb_deckinfo) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', userinfoJson.UniqueId);
    request.execute('GetCH2Deck', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2Deck");
        }
        var deckinfo = _ConvertDeckDBtoJson(recordsets[0], userinfoJson.UniqueId);
        cb_deckinfo(userinfoJson, deckinfo);
    });
}
exports.getUserDeck = getUserDeck;
// AI 리스트 얻어오기
function getAIList(listCount, minRating, maxRating, addSec, cb_aiList, isOverwrite) {
    var ai_user_count = 0;
    var request = new mssql.Request(connection);
    request.input('ListCount', Number(listCount));
    request.input('MinRating', Number(minRating));
    request.input('MaxRating', Number(maxRating));
    request.input('AddSeconds', Number(addSec));
    request.execute('GetAiList', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetAIList - " + err);
        }
        var aiUserList = {
            'Users' : []
        }
        var aiListCount = recordsets[0].length;
        for (var iUsers = 0; iUsers < aiListCount; iUsers++) {
            var UseDeckId = Number(recordsets[0][iUsers]["UseDeckId"]);
            var ai_userinfo =_ConvertUserInfoDBtoJson(recordsets[0], iUsers);
            
            getUserCommanderItem(ai_userinfo, ai_userinfo.UseDeckId, function (ai_userinfo_cb, commanderItem) {
                ai_userinfo_cb.Decks = commanderItem;
                aiUserList.Users.push(ai_userinfo_cb);
                ai_user_count++;
                if (ai_user_count >= aiListCount) {
                    cb_aiList(aiUserList, minRating, maxRating, isOverwrite);
                }
            });
        }// end for
    });// end excute
}
exports.getAIList = getAIList;
// 로그 아웃시에 유저 정보 DB 업데이트
function updateLogoutUserinfo(logoutUserList) {
    var request = new mssql.Request(connection);
    request.input('inLogoutUserList', logoutUserList);
    request.execute('UpdateCH2Userinfo', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2Userinfo - " + err);
        }
    });
}
exports.updateLogoutUserinfo = updateLogoutUserinfo;
// 로그 아웃시에 유저 지휘관 아이템 DB 업데이트
function updateLogoutCommandItem(commandItem) {
    var request = new mssql.Request(connection);
    request.input('inCommanderItem', commandItem);
    request.execute('UpdateCH2CommandItemDeck', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2CommandItemDeck - " + err);
        }
    });
}
exports.updateLogoutCommandItem = updateLogoutCommandItem;
// 리플레이를 위한 로그키 획득
function getLogkey(uniqueId, getCount, cb_GetLogkey) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inCount', Number(getCount));
    request.execute('GetCH2LogKey', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2LogKey - " + err);
        }
        var logKeyList = {
            'Keys' : []
        }
        var logKeyCount = recordsets[0].length;
        for (var iLogKey = 0; iLogKey < logKeyCount; iLogKey++) {
            var logtimeString = String(recordsets[0][iLogKey]["LogTime"].toISOString());
            var logKey = {
                'LogKey' : String(recordsets[0][iLogKey]["LogKey"]),
                'LogTime' : logtimeString
            }
            logKeyList.Keys.push(logKey);
        }// end for
        cb_GetLogkey(logKeyList);
    });// end excute
}
exports.getLogkey = getLogkey;
// 리플레이 데이터 획득
function getReplayData(logKey, cb_replayData) {
    var request = new mssql.Request(connection);
    request.input('inLogKey', String(logKey));
    request.execute('GetCH2ReplayData', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2ReplayData - " + err);
        }
        var replay_data = {
            'Datas' : []
        };
        var match_info_json = JSON.parse(recordsets[0][0]["Matchinfo"]);
        replay_data.MatchInfo = match_info_json;
        var replaydataCount = recordsets[1].length;
        for (var iData = 0; iData < replaydataCount; iData++) {
            var log_data = {
                'Data' : String(recordsets[1][iData]["Frame"]) + ":" + String(recordsets[1][iData]["Data"])
            };
            replay_data.Datas.push(log_data);
        }
        cb_replayData(replay_data);
    });
}
exports.getReplayData = getReplayData;
// 지휘관 추가
function AddCommanderItem(uniqueId, itemNoList, cb_addItemList) {
    var itemNoListString = '';
    for (var iItem = 0; iItem < itemNoList.length; iItem++) {
        itemNoListString += (itemNoList[iItem] + ',');
    }
    
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inItemNoList', String(itemNoListString));
    request.execute('AddCH2CommanderItem', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute AddCH2CommanderItem - " + err);
        }
        var addItemList = {
            'Items' : []
        };
        for (var iItem = 0; iItem < recordsets[0].length; iItem++) {
            var addItem = {
                'ItemId' : Number(recordsets[0][iItem]["ItemId"]),
                'ItemNo' : Number(recordsets[0][iItem]["ItemNo"])
            };
            addItemList.Items.push(addItem);
        }
        cb_addItemList(addItemList);
    });
}
exports.AddCommanderItem = AddCommanderItem;

// 지휘관 경험치 업데이트
function UpdateBattleReward(uniqueId, nation, nationExp, nationLevel, update_exp_data, add_commander_data, cb_addItemList) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inNation', Number(nation));
    request.input('inNationExp', Number(nationExp));
    request.input('inNationLevel', Number(nationLevel));
    request.input('inUpdateCommanderData', String(update_exp_data));
    request.input('inAddCommanderData', String(add_commander_data));
    request.execute('UpdateCH2BattleReward', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2BattleReward - " + err);
        }
        var addItemList = {
            'Items' : []
        };
        for (var iItem = 0; iItem < recordsets[0].length; iItem++) {
            var addItem = {
                'ItemId' : Number(recordsets[0][iItem]["ItemId"]),
                'ItemNo' : Number(recordsets[0][iItem]["ItemNo"])
            };
            addItemList.Items.push(addItem);
        }
        cb_addItemList(addItemList);
    });
}
exports.UpdateBattleReward = UpdateBattleReward;

// 로그인시 필요한 정보 전체 획득
function GetLoginInfo(username, cb_infos) {
    var request = new mssql.Request(connection);
    request.input('inUserName', String(username));
    request.execute('GetCH2LoginInfo', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2LoginInfo - " + err);
        }
        // user info
        var userinfo_json = _ConvertUserInfoDBtoJson(recordsets[0], 0);
        // commander item
        var user_commander_json = _ConvertCommanderItemDBtoJson(recordsets[1], userinfo_json.UniqueId);
        // deck
        var deckinfo_json = _ConvertDeckDBtoJson(recordsets[2], userinfo_json.UniqueId);
        
        cb_infos(userinfo_json, user_commander_json, deckinfo_json);
    });
}
exports.GetLoginInfo = GetLoginInfo;

// 튜토리얼 보상 정보를 DB에서 Json저장합니다.
function GetStageInfo(cb_infos) {
    var request = new mssql.Request(connection);
    var query = 'SELECT StageId, Kind, ParamString, Param1, Param2, Param3, Param4 FROM ch2_stageinfo';
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("failed Excute Stage Info query - " + err);
        }
        var stage_data_json = {
            'datas' : []
        };
        for (var idata = 0; idata < recordsets.length; idata++) {
            var stage_info_json = {
                'StageId' : Number(recordsets[idata]['StageId']),
                'Kind' : String(recordsets[idata]['Kind']),
                'ParamString' : String(recordsets[idata]['ParamString']),
                'Param1' : Number(recordsets[idata]['Param1']),
                'Param2' : Number(recordsets[idata]['Param2']),
                'Param3' : Number(recordsets[idata]['Param3']),
                'Param4' : Number(recordsets[idata]['Param4'])
            };
            stage_data_json.datas.push(stage_info_json);
        }
        cb_infos(stage_data_json);
    });
}
exports.GetStageInfo = GetStageInfo;
// 튜토리얼 진행 상황을 DB에 저장합니다.
function UpdateTutorialInfo(uniqueId, cb_tutorial_info) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.execute('UpdateCH2Tutorial', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2Tutorial - " + err);
        }
        // user info
        var tutorial_clear_info = 
        {
            'UniqueId' : Number(recordsets[0][0]['UniqueId']),
            'ClearIdx' : Number(recordsets[0][0]['ClearTutorial'])
        };
        cb_tutorial_info(tutorial_clear_info);
    });
}
exports.UpdateTutorialInfo = UpdateTutorialInfo;
// String.format 처리
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match;
        });
    };
}
// 유저 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertUserInfoDBtoJson(dbdata, dbidx) {
    var userinfo_json = {
        'UniqueId' : Number(dbdata[dbidx]["UniqueId"]),
        'UserName' : dbdata[dbidx]["UserName"],
        'UserExp' : Number(dbdata[dbidx]["UserExp"]),
        'UserLevel' : Number(dbdata[dbidx]["UserLevel"]),
        'GuildId' : Number(dbdata[dbidx]["GuildId"]),
        'Win' : Number(dbdata[dbidx]["Win"]),
        'Lose' : Number(dbdata[dbidx]["Lose"]),
        'WinStreak' : Number(dbdata[dbidx]["WinStreak"]),
        'Rating' : Number(dbdata[dbidx]["Rating"]),
        'UseDeckId' : Number(dbdata[dbidx]["UseDeckId"]),
        'UseDeckNation' : Number(dbdata[dbidx]["UseDeckNation"]),
        'NationExps' : [Number(dbdata[dbidx]["NationExp1"]), Number(dbdata[dbidx]["NationExp2"]), Number(dbdata[dbidx]["NationExp3"])],
        'NationLevels' : [Number(dbdata[dbidx]["NationLevel1"]), Number(dbdata[dbidx]["NationLevel2"]), Number(dbdata[dbidx]["NationLevel3"])],
        'ClearTutorial' : Number(dbdata[dbidx]["ClearTutorial"])
    }
    return userinfo_json;
}
// 지휘관 아이템 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertCommanderItemDBtoJson(dbdata, uniqueId) {
    var commander_item_count = dbdata.length;
    var user_commander_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(commander_item_count),
        'Items' : []
    };
    for (var iItem = 0; iItem < commander_item_count; iItem++) {
        var user_commander_item = {
            'ItemId' : Number(dbdata[iItem]["ItemId"]),
            'ItemNo' : Number(dbdata[iItem]["ItemNo"]),
            'ItemRank' : Number(dbdata[iItem]["ItemRank"]),
            'ItemExp' : Number(dbdata[iItem]["ItemExp"]),
            'SlotId' : Number(dbdata[iItem]["SlotId"])
        };
        user_commander_json.Items.push(user_commander_item);
    }
    return user_commander_json;
}
// 지휘관 덱 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertDeckDBtoJson(dbdata, uniqueId) {
    var deck_count = dbdata.length;
    var deckinfo_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(deck_count),
        'Decks' : []
    }
    for (var iDeck = 0; iDeck < deck_count; iDeck++) {
        var deck = {
            'DeckId' : Number(dbdata[iDeck]["DeckId"]),
            'DeckNation' : Number(dbdata[iDeck]["DeckNation"]),
            'DeckName' : String(dbdata[iDeck]["DeckName"]),
            'ItemIds' : [],
            'Active' : Number(1)
        }
        var itemIdString = dbdata[iDeck]["ItemIds"];
        var itemIdArray = itemIdString.split(',', gameconfig.max_deck_commander_count);
        for (var iItem = 0; iItem < gameconfig.max_deck_commander_count; iItem++) {
            deck.ItemIds.push(Number(itemIdArray[iItem]));
        }
        deckinfo_json.Decks.push(deck);
    }

    return deckinfo_json;
}

