﻿var ticker = require('delta-ticker');
var HashMap = require('hashmap').HashMap;
var logger = require('./logger');
var redisClient = require('./store_redis');
var mssql = require('./mssql');
var random = require('./random');
var aiuserlist = require('./aiuserlist');
var gameconfig = require('./gameconfig');
var item = require('./item');
var stage = require('./stage');
var battlereward = require('./battlereward');

// 로그
var log = new logger();

// 게임 큐 대기 관련 변수
var wait_queue_number = 0;
var wait_queue_ticker_active = false;

var kind_server = process.argv[4];

// 접속 처리(로그인)
function OnLogin(io, socket, data) {
    var get_logout_user_key = 'logoutuser:' + data.Name;
    redisClient.store_redis.get(get_logout_user_key, function (err, userinfo) {
        if (userinfo == null) {
            mssql.GetLoginInfo(data.Name, function (userinfoJson, usercommanderItemJson, userdeckJson) {
                _postLoginSet(io, socket, userinfoJson);
                _postLoginSetCommander(io, socket, userinfoJson.UniqueId, usercommanderItemJson);
                _postLoginSetDeck(io, socket, userinfoJson.UniqueId, userdeckJson);
            });
        } 
        else {
            var userinfoJson = JSON.parse(userinfo);
            _postLoginSet(io, socket, userinfoJson);
            var del_logout_user_key = 'logoutuser:' + userinfoJson.UserName;
            redisClient.store_redis.del(del_logout_user_key);
            redisClient.store_redis.decr('logoutusercount');
            
            var logout_commanderitemKey = 'logoutcommander:' + userinfoJson.UniqueId;
            redisClient.store_redis.get(logout_commanderitemKey, function (err, commanderitem) {
                var usercommanderItemJson = JSON.parse(commanderitem);
                _postLoginSetCommander(io, socket, userinfoJson.UniqueId, usercommanderItemJson);
                redisClient.store_redis.del(logout_commanderitemKey);
                
                var logout_deckKey = 'logoutdeck:' + userinfoJson.UniqueId;
                redisClient.store_redis.get(logout_deckKey, function (err, deck) {
                    var userdeckJson = JSON.parse(deck);
                    _postLoginSetDeck(io, socket, userinfoJson.UniqueId, userdeckJson);
                    redisClient.store_redis.del(logout_deckKey);
                });
            }); // end redis get commander item
        }
    }); // end redisClient.store_redis.get
}
exports.OnLogin = OnLogin;

// 연결 해제 처리
function OnDisconnected(io, socket) {
    redisClient.store_redis.lrem('wait', -1, socket.id, function (err, value) {
        if (err)
            log.error("un-registe wait queue failed - " + socket.id);
    });
    var userKey = 'user:' + socket.id;
    redisClient.store_redis.incr('logoutusercount', function (err, logout_user_count) {
        redisClient.store_redis.get(userKey, function (err, userInfo) {
            var userInfoJSON = JSON.parse(userInfo);
            var set_logout_user_key = 'logoutuser:' + userInfoJSON.UserName;
            redisClient.store_redis.set(set_logout_user_key, userInfo, function (err) {
                _updateLogoutUserInfo(logout_user_count, setDBLogoutUser);
                
                var find_user_commander_key = 'uidcommander:' + userInfoJSON.UniqueId;
                redisClient.store_redis.hgetall(find_user_commander_key, function (err, commanderitemlist) {
                    var user_commander = {
                        'UniqueId' : Number(userInfoJSON.UniqueId),
                        'Items' : []
                    };
                    var item_count = 0;
                    while (commanderitemlist[item_count] != undefined) {
                        var commanderitemJson = JSON.parse(commanderitemlist[item_count])
                        if (commanderitemJson.ItemId >= 0) {
                            user_commander.Items.push(commanderitemJson);
                            item_count++;
                        }
                    }
                    user_commander.Count = Number(item_count) - 1;
                    var set_logout_commander_key = 'logoutcommander:' + userInfoJSON.UniqueId;
                    redisClient.store_redis.set(set_logout_commander_key, JSON.stringify(user_commander), function (err) {
                        _updateLogoutCommanderItem(logout_user_count);
                    });
                    
                    var del_commander_item_key = 'uidcommander:' + userInfoJSON.UniqueId;
                    redisClient.store_redis.del(del_commander_item_key);

                });
                var find_user_deck_key = 'uiddeck:' + userInfoJSON.UniqueId;
                redisClient.store_redis.hgetall(find_user_deck_key, function (err, decklist) {
                    redisClient.store_redis.hkeys(find_user_deck_key, function (err, keys) {
                        var user_deck = {
                            'UniqueId' : Number(userInfoJSON.UniqueId),
                            'Decks' : []
                        };
                        var deck_count = 0;
                        for (ideck = 0; ideck < keys.length; ideck++) {
                            user_deck.Decks.push(JSON.parse(decklist[keys[ideck]]));
                            deck_count++;
                        }
                        user_deck.Count = Number(deck_count);
                        var set_logout_deck_key = 'logoutdeck:' + userInfoJSON.UniqueId;
                        redisClient.store_redis.set(set_logout_deck_key, JSON.stringify(user_deck), function (err) {
                            _updateLogoutCommanderItemDeck(logout_user_count, setDBLogoutCommanderItem);
                        });
                        var del_deck_key = 'uiddeck:' + userInfoJSON.UniqueId;
                        redisClient.store_redis.del(del_deck_key);
                    });
                });
                
                redisClient.store_redis.del(userKey);

                var useruidKey = 'unique:' + userInfoJSON.UniqueId;
                redisClient.store_redis.del(useruidKey);
                var userroomKey = 'userroom:' + userInfoJSON.UniqueId;
                redisClient.store_redis.del(userroomKey);

            }); // end set logout user
            aiuserlist.AddLogoutUser(userInfoJSON);
        }); // end
    }); // end incr
    
    _OnDestroyUserRoom(socket);
}
exports.OnDisconnected = OnDisconnected;
// 게임 대기 등록 ver.2
function OnWaitRegiste(io, socket, data) {
    var random_score = random.randomRangeInc(10, 1000);
    var find_user = 'user:' + socket.id;
    redisClient.store_redis.get(find_user, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        var deckInfo = {
            'Items' : []
        };
        var current_item_idx = 0;
        
        var user_use_deckId = Number(data.DeckId);
        userInfoJson.UseDeckId = user_use_deckId;
        
        var find_commanderitem_key = 'uidcommander:' + userInfoJson.UniqueId;
        var current_item_idx = 0;
        var empty_item_count = 0;
        
        var find_user_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
        redisClient.store_redis.hmget(find_user_deck_key, user_use_deckId, function (err, deckData) {
            var deckDataJson = JSON.parse(deckData);
            for (var iItem = 0; iItem < deckDataJson.ItemIds.length; iItem++) {
                var itemId = Number(deckDataJson.ItemIds[iItem]);
                redisClient.store_redis.hmget(find_commanderitem_key, itemId, function (err, itemData) {
                    var itemDataJson = JSON.parse(itemData);
                    var user_commander_item = {
                        'ItemId' : Number(itemDataJson.ItemId),
                        'ItemNo' : Number(itemDataJson.ItemNo),
                        'ItemRank' : Number(itemDataJson.ItemRank),
                        'ItemExp' : Number(itemDataJson.ItemExp),
                        'SlotId' : Number(current_item_idx)
                    };
                    if (itemDataJson.ItemId >= 0) {
                        deckInfo.Items.push(user_commander_item);
                    }
                    else {
                        empty_item_count++;
                    }
                    current_item_idx++;
                    if (current_item_idx >= deckDataJson.ItemIds.length) {
                        deckInfo.Count = Number(current_item_idx) - Number(empty_item_count);
                        userInfoJson.Decks = deckInfo;
                        userInfoJson.UseDeckNation = deckDataJson.DeckNation;
                        var find_set_userinfo = 'user:' + userInfoJson.UserKey;
                        redisClient.store_redis.set(find_set_userinfo, JSON.stringify(userInfoJson), function (err) {
                            redisClient.store_redis.zadd('wait' + wait_queue_number, random_score, socket.id, function (err, value) {
                                if (err) {
                                    log.error(err);
                                }
                                else {
                                    socket.emit('sc_wait_registe');
                                }
                            }); // end zadd in wating queue
                        }); // end redis set userinfo what userkey
                        var find_set_userinfo_uniqueid = 'unique:' + userInfoJson.UniqueId;
                        redisClient.store_redis.set(find_set_userinfo_uniqueid, JSON.stringify(userInfoJson)); // 실제 방을 구성할 경우 사용되지 않는 정보
                    }
                }); // end hmget
            }
        });
    }); // end redis get user info
}
exports.OnWaitRegiste = OnWaitRegiste;
// 게임 대기 해제 ver.2
function OnWaitUnregiste(io, socket, data) {
    redisClient.store_redis.zrem('wait' + wait_queue_number, socket.id, function (err, value) {
        if (err)
            log.error(err);
        else {
            socket.emit('sc_wait_unregiste');
        }
    });
}
exports.OnWaitUnregiste = OnWaitUnregiste;


// 방 매칭 확인 요청
function OnMatchRoomJoinAcceptRequest(io, socket) {
    redisClient.store_redis.incr('roomNo', function (err, roomId) {
        for (iMember = 0; iMember < gameconfig.maxRoomMember; iMember++) {
            redisClient.store_redis.rpop('wait', function (err, socketid) {
                var userKey = 'user:' + socketid;
                redisClient.store_redis.get(userKey, function (err, userInfo) {
                    var user_socket = io.sockets.connected[socketid];
                    
                    user_socket.join(String(roomId));
                    user_socket.room = String(roomId);
                    
                    var userInfoJSON = JSON.parse(userInfo);
                    redisClient.store_redis.set('userroom:' + Number(userInfoJSON.UniqueId), String(roomId));
                    redisClient.store_redis.sadd('room:' + String(roomId), Number(userInfoJSON.UniqueId));
                    
                    user_socket.emit('sc_room_join_accept_request');
                });
            });
        }
    });
}
// 방 매칭 접속 확인(승인)
function OnRoomJoinAccept(io, socket, data) {
    redisClient.store_redis.scard('room:' + socket.room, function (err, roomMemberCount) {
        redisClient.store_redis.incr('roomjoin:' + socket.room, function (err, joinCount) {
            if (joinCount >= roomMemberCount) {
                OnMatchRoom2(io, socket);
                redisClient.store_redis.del('roomjoin:' + socket.room);
            }
            else {
                var join_count_info = {
                    total : Number(roomMemberCount),
                    join : Number(joinCount)
                }
                socket.emit('sc_room_join_accept', join_count_info);
            }
        });
    });
}
exports.OnRoomJoinAccept = OnRoomJoinAccept;
// 방 매칭 접속 취소
function OnRoomJoinCancel(io, socket, data) {
    var roomId = socket.room;
    var room = io.nsps['/'].adapter.rooms[roomId];
    if (room) {
        for (var user_socket_id in room) {
            if (user_socket_id != socket.id) {
                var user_socket = io.sockets.connected[user_socket_id];
                // 유저는 현재 방을 떠나도록 처리
                user_socket.leave(roomId);
                user_socket.emit('sc_room_join_cancel', { type : Number(1) });
                // 해당 유저 제외 나머지 인원 rpop으로 큐에 재배치
                redisClient.store_redis.rpush('wait', user_socket_id, function (err, count) {
                    if (count >= gameconfig.maxRoomMember) {
                        OnMatchRoomJoinAcceptRequest(io, socket);
                    }
                });// rpush end
            }
            else {
                socket.leave(roomId);
                socket.emit('sc_room_join_cancel', { type : Number(0) });
            }
        }
        var roomKey = 'room:' + roomId;
        redisClient.store_redis.del(roomKey);
        var roomJoinKey = 'roomjoin:' + roomId;
        redisClient.store_redis.del(roomJoinKey);
        var roomAiCountKey = 'aiusercount:' + roomId;
        redisClient.store_redis.del(roomAiCountKey);
    }
}
exports.OnRoomJoinCancel = OnRoomJoinCancel;
// 전투 종료
function OnBattleEnd(io, socket) {
    _OnDestroyUserRoom(socket);
}
exports.OnBattleEnd = OnBattleEnd;
// 전투 방 정보 제거
function _OnDestroyUserRoom(socket) {
    //redisClient.store_redis.del('room:' + socket.room);
    redisClient.store_redis.del('aiusercount:' + socket.room);
    
    socket.leave(socket.room);
}
// 방 매칭2(확인 과정을 거친 방 매칭)
function OnMatchRoom2(io, socket) {
    var roomInfo = {
        userinfos : []
    };
    var find_room_key = 'room:' + socket.room;
    redisClient.store_redis.smembers(find_room_key, function (err, room_users) {
        for (var iUser = 0; iUser < room_users.length; iUser++) {
            redisClient.store_redis.get('unique:' + room_users[iUser], function (err, userinfo) {
                roomInfo.userinfos.push(JSON.parse(userinfo));
                if (roomInfo.userinfos.length >= room_users.length) {
                    io.sockets.to(socket.room).emit('sc_match_roominfo', roomInfo);
                } // end if
            }); // end redis get
        } // end for;
    });//end redis smemebers
}
// 방 매칭(큐가 잡히면 바로 방으로 이동하는 형식)
function OnMatchRoom(io, socket) {
    var join_count = 0;
    redisClient.store_redis.incr('roomNo', function (err, value) {
        // 방번호 증가
        var roomId = value;
        var roomInfo = {
            userinfos : []
        };
        for (var iMember = 0; iMember < gameconfig.maxRoomMember; iMember++) {
            redisClient.store_redis.rpop('wait', function (err, socketid) {
                // 대기자 호출
                var user_socket = io.sockets.connected[socketid];
                user_socket.join(String(roomId));
                user_socket.room = String(roomId);
                var userKey = 'user:' + user_socket.id;
                redisClient.store_redis.get(userKey, function (err, userInfo) {
                    var userInfoJSON = JSON.parse(userInfo);
                    roomInfo.userinfos.push(userInfoJSON);
                    redisClient.store_redis.set('userroom:' + Number(userInfoJSON.UniqueId), String(roomId));
                    redisClient.store_redis.sadd('room:' + String(roomId), Number(userInfoJSON.UniqueId));
                    join_count++;
                    if (join_count >= gameconfig.maxRoomMember) {
                        io.sockets.to(String(roomId)).emit('sc_match_roominfo', roomInfo);
                    }
                })
            })
        }
    });
}
// 방 유저 준비
function OnRoomReady(io, socket, data) {
    var userKey = 'user:' + socket.id;
    var battleServer_Local = "ws://127.0.0.1:38301/socket.io/?EIO=4&transport=websocket";
    var battleServer_QA = "ws://182.162.250.72:38301/socket.io/?EIO=4&transport=websocket";
    
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        var roomReadyKey = 'roomready:' + socket.room;
        redisClient.store_redis.incr(roomReadyKey, function (err, ready_count) {
            if (ready_count >= gameconfig.maxRoomMember) {
                if (kind_server == 'QA') {
                    io.sockets.to(socket.room).emit('sc_room_start', { battleserv : String(battleServer_QA) });
                }
                else if (kind_server == 'Local') {
                    io.sockets.to(socket.room).emit('sc_room_start', { battleserv : String(battleServer_Local) });
                }
                redisClient.store_redis.del(roomReadyKey);
            }
            else {
                io.sockets.to(socket.room).emit('sc_room_ready', { ready_uid : Number(userInfoJson.UniqueId) });
            }
        });
    });
}
exports.OnRoomReady = OnRoomReady;
// 방 유저 준비 취소
function OnRoomReadyCancel(io, socket, data) {
    var userKey = 'user:' + socket.id;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        var roomReadyKey = 'roomready:' + socket.room;
        redisClient.store_redis.decr(roomReadyKey, function (err, ready_count) {
            io.sockets.to(socket.room).emit('sc_room_ready_cancel', { ready_uid : Number(userInfoJson.UniqueId) });
        })
    });
}
exports.OnRoomReadyCancel = OnRoomReadyCancel;
// 지휘관 아이템 업데이트
function OnUpdateCommandItemDeck(io, socket, data) {
    var userKey = 'user:' + socket.id;
    var update_item_count = data.length;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        
        var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
        var current_deck = 0;
        var total_update_deck = data.length;
        for (var iDeck = 0; iDeck < total_update_deck; iDeck++) {
            var deck_id = data[iDeck].DeckId;
            redisClient.store_redis.hmget(find_commander_deck_key, deck_id, function (err, deckData) {
                var deckDataJson = JSON.parse(deckData);
                deckDataJson.DeckNation = data[current_deck].DeckNation;
                deckDataJson.DeckName = data[current_deck].DeckName;
                deckDataJson.ItemIds = data[current_deck].ItemIds;
                
                redisClient.store_redis.hmset(find_commander_deck_key, deck_id, JSON.stringify(deckDataJson));
                current_deck++;
                if (current_deck >= total_update_deck) {
                    socket.emit('sc_update_commanditemdeck');
                }
            });
        }
    });
}
exports.OnUpdateCommandItemDeck = OnUpdateCommandItemDeck;
function OnAddCommanderItemDeck(io, socket, data) {
    var userKey = 'user:' + socket.id;
    var add_item_count = data.length;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);

        var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
        var total_add_deck = data.length;
        var current_add_deck = 0;
        redisClient.store_redis.hkeys(find_commander_deck_key, function (err, keys) {
            var maxId = 0;
            for (var ikey = 0; ikey < keys.length; ikey++) {
                if (Number(keys[ikey]) > maxId)
                    maxId = Number(keys[ikey]);
            }
            var deckId = maxId + 1;
            var deckDataJson = {
                'DeckId' : Number(deckId),
                'DeckNation' : data[current_add_deck].DeckNation,
                'DeckName' : data[current_add_deck].DeckName,
                'ItemIds' : data[current_add_deck].ItemIds,
                'Active' : Number(2)
            }
            redisClient.store_redis.hmset(find_commander_deck_key, deckId, JSON.stringify(deckDataJson), function (err) {
                socket.emit('sc_add_commanditemdeck', { 'NewDeckId' : Number(deckId) });
            })
        });
    });
}
exports.OnAddCommanderItemDeck = OnAddCommanderItemDeck;

function OnRemoveCommanderItemDeck(io, socket, data) {
    var userKey = 'user:' + socket.id;
    var remove_deck_id = data.DeckId;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);

        var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
        redisClient.store_redis.hmget(find_commander_deck_key, remove_deck_id, function (err, deckdata) {
            if (err)
                log.error("failed remove deck - " + err);
            var deckdataJson = JSON.parse(deckdata);
            deckdataJson.Active = Number(3);
            var set_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
            redisClient.store_redis.hmset(set_commander_deck_key, remove_deck_id, JSON.stringify(deckdataJson), function (err) {
                socket.emit('sc_remove_commanditemdeck', { 'DeckId' : Number(remove_deck_id) });
            });
        });
    });
}
exports.OnRemoveCommanderItemDeck = OnRemoveCommanderItemDeck;

// 리플레이 리스트 받기
function OnReplayList(io, socket, data) {
    var userKey = 'user:' + socket.id;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        mssql.getLogkey(userInfoJson.UniqueId, 10, function (logKeyList) {
            socket.emit('sc_replaylist', logKeyList);
        }); // end mssql get log key
    });// end redis get 
    //mssql.getLogkey(
}
exports.OnReplayList = OnReplayList;
// 리플레이 데이터 받기(DB에서 바로 받기)
function OnReplayData(io, socket, data) {
    var log_key = data.LogKey;
    mssql.getReplayData(log_key, function (replay_data) {
        socket.emit('sc_replaydata', replay_data);
    });
}
exports.OnReplayData = OnReplayData;
// 전투 결과 보상을 받습니다.
function OnBattleReward(io, socket, data) {
    stage.GetStageInfo(data.StageId, function (stage_info_json) {
        if (stage_info_json.Kind == 'PvP') {
            battlereward.UpdatePvPBattleReward(io, socket, data, function (reward_data_json) {
                socket.emit('sc_battle_reward', data);
            });
        }
        else if (stage_info_json.Kind == 'Tutorial') {
            socket.emit('sc_battle_reward', data);
        }
    });
}
exports.OnBattleReward = OnBattleReward;
// 튜토리얼을 시작합니다.
function OnTutorialStart(io, socket, data) {
    socket.emit('sc_tutorial_start', data);
}
exports.OnTutorialStart = OnTutorialStart;
// 튜토리얼을 종료합니다.
function OnTutorialEnd(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userInfoJson = JSON.parse(userinfo);
        userInfoJson.ClearTutorial += 1;
        
        var set_user_key = 'user:' + socket.id;
        var set_user_uniqueId = 'uniqueid:' + userInfoJson.UniqueId;
        redisClient.store_redis.set(set_user_key, JSON.stringify(userInfoJson));
        redisClient.store_redis.set(set_user_uniqueId, JSON.stringify(userInfoJson));

        mssql.UpdateTutorialInfo(userInfoJson.UniqueId, function (tutorialInfo) {
            socket.emit('sc_tutorial_end', tutorialInfo);
        });
    });
}
exports.OnTutorialEnd = OnTutorialEnd;
// 채팅 처리
function OnChat(io, socket, data) {
    var userKey = 'user:' + socket.id;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        var chatInfo = {
            Name : userInfoJson.UserName,
            Msg : data.Msg
        }
        io.sockets.to(socket.guildChat).emit('sc_chat', chatInfo); // 길드 채팅
        //io.sockets.emit('sc_chat', chatInfo); // 전체 채팅
    });
}
exports.OnChat = OnChat;


// 큐 찾기 진행
function game_queue_ticker_start(io) {
    if (wait_queue_ticker_active) {
        return;
    }
    else {
        wait_queue_ticker_active = true;
        ticker({
            task : function () {
                var current_wait_queue = wait_queue_number;
                // console.log("wait queue - enqueue " + wait_queue_number);
                var socketids = [];
                redisClient.store_redis.zcard('wait' + String(current_wait_queue), function (err, userCount) {
                    if (userCount > 0) {
                        wait_queue_number ^= 0x01;
                        redisClient.store_redis.zrange('wait' + String(current_wait_queue), 0, -1, function (err, userList) {
                            for (var iUser = 0; iUser < userList.length; iUser++) {
                                // 유저에게 상대 전송. 
                                socketids.push(userList[iUser]);
                                if (socketids.length >= gameconfig.maxRoomMember) {
                                    createMatchRoom(io, gameconfig.maxRoomMember, socketids, 0);
                                    socketids = [];
                                }
                                else if (iUser + 1 >= userList.length) {
                                    createMatchRoom(io, userList.length, socketids, gameconfig.maxRoomMember - socketids.length);
                                    socketids = [];
                                }
                            }// end of for
                            redisClient.store_redis.del('wait' + String(current_wait_queue));
                        });// end of zrange 
                    }
                })
            },
            delay : 10000
        }).start();
    }
}// end game_queue_ticker_start
exports.game_queue_ticker_start = game_queue_ticker_start;
// 방 매칭 처리
function createMatchRoom(io, maxRoomUser, socketids, AIUserCount) {
    var roomInfo = {
        userinfos : []
    };
    var user_count = 0;
    var user_rating = 0;
    
    redisClient.store_redis.incr('roomNo', function (err, roomId) {
        for (iMember = 0; iMember < maxRoomUser; iMember++) {
            var userKey = 'user:' + socketids[iMember];
            redisClient.store_redis.get('user:' + socketids[iMember], function (err, userInfo) {
                var userInfoJSON = JSON.parse(userInfo);
                
                var user_socketid = userInfoJSON.UserKey;
                var user_socket = io.sockets.connected[user_socketid];
                
                user_socket.join(String(roomId));
                user_socket.room = String(roomId);
                
                roomInfo.userinfos.push(userInfoJSON);
                
                redisClient.store_redis.set('userroom:' + Number(userInfoJSON.UniqueId), JSON.stringify({ 'UniqueId' : Number(userInfoJSON.UniqueId), 'roomId' : String(roomId), 'AI': false }));
                redisClient.store_redis.sadd('room:' + String(roomId), Number(userInfoJSON.UniqueId));
                
                user_rating += userInfoJSON.Rating;
                
                user_count++;
                if (user_count >= maxRoomUser) {
                    user_rating = user_rating / maxRoomUser;
                    if (AIUserCount <= 0) {
                        // AI 유저가 없는 경우
                        io.sockets.to(String(roomId)).emit('sc_match_roominfo', roomInfo);
                    }
                    else {
                        createAIMatchUser(io, roomId, roomInfo, AIUserCount, user_rating);
                    }
                    redisClient.store_redis.set('aiusercount:' + String(roomId), Number(AIUserCount));
                }
            }); // end redis get
        } // end for
    }); // end incr
}
// AI 유저를 배치합니다. (DB에서 정보 얻어오는 방식)
function createAIMatchUser(io, roomId, roomInfo, aiUserCount, rating) {
    aiuserlist.SelectAIUser(rating, aiUserCount, function (aiUserList) {
        for (var iBot = 0; iBot < aiUserCount; iBot++) {
            var userinfo = JSON.parse(aiUserList[iBot]);
            roomInfo.userinfos.push(userinfo);
            
            redisClient.store_redis.set('userroom:' + Number(userinfo.UniqueId), JSON.stringify({ 'UniqueId' : Number(userinfo.UniqueId), 'roomId' : String(roomId), 'AI': true }));
            redisClient.store_redis.sadd('room:' + String(roomId), Number(userinfo.UniqueId));
            
            if (roomInfo.userinfos.length >= aiUserCount) {
                io.sockets.to(String(roomId)).emit('sc_match_roominfo', roomInfo);
            }
            // 이미 ready가 된 상태로 셋팅합니다.
            redisClient.store_redis.set('roomready:' + roomId, Number(aiUserCount));
        }
    });
}
// 로그인 이후 redis를 셋팅합니다.
function _postLoginSet(io, socket, userinfoJson) {
    
    var userKey = 'user:' + String(socket.id);
    userinfoJson.UserKey = String(socket.id);
    
    var userinfo = JSON.stringify(userinfoJson);
    redisClient.store_redis.set(userKey, userinfo);
    
    var userUniqueKey = 'unique:' + userinfoJson.UniqueId;
    redisClient.store_redis.set(userUniqueKey, userinfo);
    
    aiuserlist.RemoveLoginUser(userinfoJson.UniqueId, userinfoJson.Rating);
    
    // 차후 길드 처리
    socket.join('chat');
    socket.guildChat = 'chat';
    
    log.info("Login user - " + userinfoJson.UserName);
    socket.emit('sc_login', userinfoJson);
}
// 아이템 정보를 발송합니다.
function _postLoginSetCommander(io, socket, uniqueId, usercommanderJson) {
    // 커맨더 아이템 저장 처리
    var userCommanderUidKey = 'uidcommander:' + uniqueId;
    
    // 빈 아이템 표시를 위해 빈 아이템 하나를 만듭니다.
    var empty_commander_item = {
        'ItemId' : -1,
        'ItemNo' : -1,
        'ItemRank' : 0,
        'ItemExp' : 0
    };
    redisClient.store_redis.hmset(userCommanderUidKey, -1, JSON.stringify(empty_commander_item));

    for (var iItem = 0; iItem < Number(usercommanderJson.Count); iItem++) {
        redisClient.store_redis.hmset(userCommanderUidKey, String(usercommanderJson.Items[iItem].ItemId), JSON.stringify(usercommanderJson.Items[iItem]));
    }
    //redisClient.store_redis.set(userCommanderUidKey, JSON.stringify(usercommanderJson));
    
    log.info("Login commander item info - uniqueid : " + uniqueId);
    socket.emit('sc_login_commanderitem', usercommanderJson);
}
// Deck 정보를 발송합니다.
function _postLoginSetDeck(io, socket, uniqueId, userdeckJson) {
    var user_deck_set_key = 'uiddeck:' + uniqueId;
    
    for (var iDeck = 0; iDeck < userdeckJson.Decks.length; iDeck++) {
        redisClient.store_redis.hmset(user_deck_set_key, userdeckJson.Decks[iDeck].DeckId, JSON.stringify(userdeckJson.Decks[iDeck]));
    }
    log.info("Login commander deck info - uniqueid : " + uniqueId);
    socket.emit('sc_login_deck', userdeckJson);
}
// 로그 아웃한 유저의 정보를 DB로 전송합니다.
function _updateLogoutUserInfo(logout_user_count, callback) {
    var userinfo_count = 0;
    var logoutUserList = '';
    
    if (logout_user_count > gameconfig.max_log_out_update_user) {
        // 테스트 버전에선 1명 이상의 정보를 모두 저장. 정식버전의 경우 숫자 수정
        redisClient.store_redis.keys('logoutuser:*', function (err, logout_users) {
            logout_user_count = logout_users.length;
            for (var iUser = 0; iUser < logout_users.length; iUser++) {
                redisClient.store_redis.get(logout_users[iUser], function (err, userinfo) {
                    var userinfoJson = JSON.parse(userinfo);
                    
                    logoutUserList += userinfoJson.UniqueId + '|' 
                        + userinfoJson.Win + '|' 
                        + userinfoJson.Lose + '|' 
                        + userinfoJson.WinStreak + '|' 
                        + userinfoJson.UseDeckId + '|' 
                        + userinfoJson.UseDeckNation + '|'
                        + userinfoJson.Rating + ',';
                    
                    userinfo_count++;
                    var del_logout_user_key = 'logoutuser:' + userinfoJson.UserName;
                    redisClient.store_redis.del(del_logout_user_key);
                    redisClient.store_redis.del('logoutusercount');
                    if (userinfo_count == logout_user_count) {
                        callback(logoutUserList);
                    }
                }); //end redisClient.store_redis.get
            } // end for
        }); // end redisClient.store_redis.keys
            // db전송
    }// end if (logout_user_count > 0)
}
// 로그 아웃한 유저의 지휘관 덱 정보를 DB로 전송합니다.
function _updateLogoutCommanderItemDeck(logout_user_count, cb_deckupdate) {
    var commander_count = 0;
    var updateList = '';
    var current_logout_count = 0;
    if (logout_user_count > gameconfig.max_log_out_update_user) {
        // 테스트 버전에선 1명 이상의 정보를 모두 저장. 정식버전의 경우 숫자 수정
        redisClient.store_redis.keys('logoutdeck:*', function (err, logout_decks) {
            var deck_count = logout_decks.length;
            for (var iDeck = 0; iDeck < deck_count; iDeck++) {
                var uniqueId = Number(logout_decks[iDeck].split(':')[1]);
                redisClient.store_redis.get(logout_decks[iDeck], function (err, deck) {
                    var deckJson = JSON.parse(deck);
                    for (var iDeck = 0; iDeck < deckJson.Count; iDeck++) {
                        updateList += (deckJson.Decks[iDeck].Active + '|' + deckJson.UniqueId + '|' + deckJson.Decks[iDeck].DeckId + '|' + deckJson.Decks[iDeck].DeckNation + '|' + deckJson.Decks[iDeck].DeckName + '|');
                        var itemCount = deckJson.Decks[iDeck].ItemIds.length;
                        for (var iItem = 0; iItem < itemCount; iItem++) {
                            updateList += deckJson.Decks[iDeck].ItemIds[iItem] + ',';
                        }
                        updateList += '/';
                    }
                    current_logout_count++;
                    if (current_logout_count >= logout_user_count) {
                        // console.log("check log out - " + updateList);
                        cb_deckupdate(updateList);
                    }
                }); // end get
                redisClient.store_redis.del(logout_decks[iDeck]); // del logout deck
            }
        });
    }// end if (logout_user_count > 0)
}
function _updateLogoutCommanderItem(logout_user_count) {
    if (logout_user_count > gameconfig.max_log_out_update_user) {
        redisClient.store_redis.keys('logoutcommander:*', function (err, logout_commanders) {
            var commander_count = logout_commanders.length;
            for (var iItem = 0; iItem < commander_count; iItem++) {
                redisClient.store_redis.del(logout_commanders[iItem]);
            }
        });
    }
}
function setDBLogoutUser(logoutUserList) {
    log.info('Update DB logout user list - ' + logoutUserList);
    mssql.updateLogoutUserinfo(logoutUserList);
}
function setDBLogoutCommanderItem(logoutCommanderItem) {
    log.info('Update DB logout commander Item');
    mssql.updateLogoutCommandItem(logoutCommanderItem);
}