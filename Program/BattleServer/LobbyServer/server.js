﻿/*
 * Lobby Server
 * 2014. 11. 26 
 * 
 * */

var cluster = require('cluster');
var numCpus = require('os').cpus().length;
var sio = require('socket.io');
var ticker = require('delta-ticker');
//var redis = require('redis');
var redisClient = require('./store_redis');
var receiver = require('./receiver');
var logger = require('./logger');
var store = require('./store_redis').store_redis;
var mssql = require('./mssql');
var aiuserlist = require('./aiuserlist.js');

var random = require('./random');

var item = require('./item');
var stage = require('./stage');

var gametable = require('./gametable');
// gametable.LoadTable();

var port = 6262;
var BattleClientId = 0;
var isUseCluster = (process.argv[2] == '-usecluster');
var custom_port = process.argv[3];
var kind_server = process.argv[4];

var isInitAiList = false;

if (cluster.isMaster && isUseCluster) {
	var log = new logger();
	log.info("Lobby Server Start : cluster mode");
	for (var i = 0; i < numCpus; i++) {
		cluster.fork();
	}
	cluster.on('online', function (worker) {
		log.info("Worker " + worker.process.pid + " is online.");
	});
	cluster.on('exit', function (worker, code, signal) {
        log.info("worker " + worker.process.pid + " died.");
        cluster.fork();
	});
}
else {
	var log = new logger();
	
	if (!isUseCluster) {
		log.info("Lobby Server Start : none cluster mode");
	}
    
    mssql.InitSql(function () {
        // aiList를 받아옵니다.
        aiuserlist.AddAiListFromSQL();
        // 스테이지 정보를 셋팅합니다.
        stage.InitStageInfo();
    });

	var io = sio.listen(custom_port);
	redisClient.initRedis(io);
	redisClient.initCallback(cbSubMessage);
	redisClient.sub.subscribe("addwait");
    
    // 게임대기 큐의 ticker를 실행합니다.
    receiver.game_queue_ticker_start(io);

	io.on('connection', function (socket) {
		log.info("connect process pid - " + process.pid + "/ connection socket id - " + socket.id);
		socket.on('disconnect', function () {
			log.info("disconnect socket id - " + socket.id);
			receiver.OnDisconnected(io, socket);
		})
		// 유저 로그인 및 신규유저 등록
		socket.on('cs_login', function (data) {
            receiver.OnLogin(io, socket, data);
        })
        socket.on('cs_logout', function (data) {
            receiver.OnDisconnected(io, socket);
        })
		// 대기열 등록
		socket.on('cs_wait_registe', function (data) {
			receiver.OnWaitRegiste(io, socket, data);
		})
		// 대기열 해제
		socket.on('cs_wait_unregiste', function (data) {
			receiver.OnWaitUnregiste(io, socket, data);
		});
		// 방 접속 확인
		socket.on('cs_room_join_accept', function (data) {
			receiver.OnRoomJoinAccept(io, socket, data);
		});
		// 방 접속 취소
		socket.on('cs_room_join_cancel', function (data) {
			receiver.OnRoomJoinCancel(io, socket, data);
		});
		// 방 준비
		socket.on('cs_room_ready', function (data) {
			receiver.OnRoomReady(io, socket, data);
		})
		// 방 준비 취소
		socket.on('cs_room_ready_cancel', function (data) {
			receiver.OnRoomReadyCancel(io, socket, data);
		})
		// 방 나가기
		socket.on('cs_room_exit', function (data) {
			receiver.OnRoomJoinCancel(io, socket, data);
		});
		// 전투 종료
		socket.on('cs_battle_end', function (data) {
            receiver.OnBattleEnd(io, socket);
        });
        // 지휘관 카드 리스트 전송
        socket.on('cs_update_commanditemdeck', function (data) {
            receiver.OnUpdateCommandItemDeck(io, socket, data);
        });
        // 지휘관 덱 생성
        socket.on('cs_add_commanditemdeck', function (data) {
            receiver.OnAddCommanderItemDeck(io, socket, data);
        });
        // 지휘관 덱 삭제
        socket.on('cs_remove_commanditemdeck', function (data) {
            receiver.OnRemoveCommanderItemDeck(io, socket, data);
        });
        // 리플레이 리스트 전송
        socket.on('cs_replaylist', function (data) {
            receiver.OnReplayList(io, socket, data);
        });
        // 리플레이 데이터 전송
        socket.on('cs_replaydata', function (data) {
            receiver.OnReplayData(io, socket, data);
        });
        // 전투 보상 처리
        socket.on('cs_battle_reward', function (data) {
            receiver.OnBattleReward(io, socket, data);
        });
        socket.on('cs_tutorial_start', function (data) {
            receiver.OnTutorialStart(io, socket, data);
        });
        socket.on('cs_tutorial_end', function (data) {
            receiver.OnTutorialEnd(io, socket, data);
        });
        // 채팅 처리
        socket.on('cs_chat', function (data) {
            receiver.OnChat(io, socket, data);
        });
    })
    
    // redis callback 처리
    function cbSubMessage(channel, message) {
        console.log("[" + channel + "] - " + message);
    }

}// end of slave process
// 예외처리
process.on('uncaughtException', function (err) {
    var log = new logger();
    log.error('Caught exception: ' + err);
});
// 종료 처리
process.on('exit', function (code) {
    var log = new logger();
    log.info("closed server process id - " + process.pid);
});
