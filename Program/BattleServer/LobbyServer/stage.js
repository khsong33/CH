﻿var logger = require('./logger');
var redisClient = require('./store_redis');
var mssql = require('./mssql');
var gameconfig = require('./gameconfig');

// 튜토리얼 보상 정보를 redis에 저장합니다.
function InitStageInfo() {
    mssql.GetStageInfo(function (cb_stage_datas) {
        var set_stageinfo_key = 'stageinfo';
        var set_tutorialinfo_key = 'tutorialinfo';
        for (var idata = 0; idata < cb_stage_datas.datas.length; idata++) {
            var stage_info_json = cb_stage_datas.datas[idata];
            redisClient.gamedata_redis.hmset(set_stageinfo_key, stage_info_json.StageId, JSON.stringify(stage_info_json));
            if (stage_info_json.Kind == 'Tutorial') {
                redisClient.gamedata_redis.zadd(set_tutorialinfo_key, stage_info_json.Param4, JSON.stringify(stage_info_json));
            }
        }
    });
}
exports.InitStageInfo = InitStageInfo;
// Stage Id로 Stage의 정보를 얻어옵니다.
function GetStageInfo(stageid, cb_kind_stage) {
    var get_stageinfo_key = 'stageinfo';
    redisClient.gamedata_redis.hmget(get_stageinfo_key, stageid, function (err, stage_info) {
        var stage_info_json = JSON.parse(stage_info);
        cb_kind_stage(stage_info_json);
    });
}
exports.GetStageInfo = GetStageInfo;