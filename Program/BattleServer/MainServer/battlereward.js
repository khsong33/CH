﻿var logger = require('./logger');
var redisClient = require('./redis_store');
var mssql = require('./mssql');
var gameconfig = require('./gameconfig');
var item = require('./item');

function UpdatePvPBattleReward(io, socket, data, cb_reward_data) {
    // 테스트 에코 서버로 구현
    var userKey = 'user:' + socket.id;
    redisClient.store_redis.get(userKey, function (err, userInfo) {
        var userInfoJson = JSON.parse(userInfo);
        //userInfoJson.NationExps[data.Nation - 1] = data.NationExp;
        //userInfoJson.NationLevels[data.Nation - 1] = data.NationLevel;
        userInfoJson.Win = Number(data.Win);
        userInfoJson.WinStreak = Number(data.WinStreak);
        userInfoJson.Lose = Number(data.Lose);
        userInfoJson.LoseStreak = Number(data.LoseStreak);
        
        // 경험치를 저장합니다.
        var set_userinfo_userkey = 'user:' + userInfoJson.UserKey;
        redisClient.store_redis.set(set_userinfo_userkey, JSON.stringify(userInfoJson));
        
        item.UpdateBattleRewardCommanderItem(userInfoJson.UniqueId, data, function (db_exp_update_data, db_add_commander_data) {
            mssql.UpdateBattleReward(userInfoJson.UniqueId, data.Nation, data.NationExp, data.NationLevel, db_exp_update_data, db_add_commander_data, data.Win, data.WinStreak, data.Lose, data.LoseStreak, function (addItemList) {
                var itemInfoCount = addItemList.Items.length;
                for (var iItem = 0; iItem < itemInfoCount; iItem++) {
                    var itemInfoJson = {
                        'ItemId' : Number(addItemList.Items[iItem].ItemId),
                        'ItemNo' : Number(addItemList.Items[iItem].ItemNo),
                        'ItemRank' : 1,
                        'ItemExp' : 0,
                    };
                    var find_user_commander_item = 'uidcommander:' + userInfoJson.UniqueId;
                    redisClient.store_redis.hmset(find_user_commander_item, String(addItemList.Items[iItem].ItemId), JSON.stringify(itemInfoJson));
                }
                data.AddItemList = addItemList.Items;
                cb_reward_data(data);
                //socket.emit('sc_battle_reward', data);
            });
        });
    });
}
exports.UpdatePvPBattleReward = UpdatePvPBattleReward;

//function UpdateTutorialReward(io, socket, data, cb_reward_data) {
//    var userKey = 'user:' + socket.id;
//    redisClient.store_redis(userKey, function (err, userInfo) {
//        var userInfoJson = JSON.parse(userInfo);
//        var current_clear_tutorial_index = userInfoJson.ClearTutorial + 1;
//        var get_tutorial_access_by_index = 'tutorialinfo';
//        redisClient.store_redis.zrange(get_tutorial_access_by_index, current_clear_tutorial_index, current_clear_tutorial_index, function (err, stageInfo) {
//            var stageInfoJson = JSON.parse(stageInfo);
//            // 현재 완료 하여야하는 스테이지를 깨는 경우에만 경험치를 증가시킵니다.
//            if (stageInfoJson.StageId == data.StageId) {

//            }
//        });
//    });
//}
//exports.UpdateTutorialReward = UpdateTutorialReward;