﻿var mssql = require('./mssql');
var redisClient = require('./redis_store');

// db 전송에 필요한 데이터
var update_db_deckinfo = {};
var update_db_userinfo = {};

var update_db_deck_count = 0;
var update_db_userinfo_count = 0;
// Cache data 초기화
function Init() {
    var get_cache_user_info = 'usercache';
    redisClient.store_redis.hgetall(get_cache_user_info, function (err, cache_userlist) {
        if (cache_userlist != null) {
            update_db_userinfo = cache_userlist;
        }
    });
    var get_cache_deck_info = 'deckcache';
    redisClient.store_redis.hgetall(get_cache_deck_info, function (err, cache_decklist) {
        if (cache_decklist != null) {
            update_db_deckinfo = cache_decklist;
        }
    });
}
exports.Init = Init;
// DB로 보낼 덱 정보를 저장합니다.
function UpdateDBDeck(uniqueid, deckid, deckdataJson) {
    var set_db_deck_key = String(uniqueid + '/' + deckid);
    
    var set_data = deckdataJson.Active + '|' + uniqueid + '|' + deckdataJson.DeckId + '|' + deckdataJson.DeckNation + '|' + deckdataJson.DeckName + '|';
    var item_data_string = '';
    for (var iitem = 0; iitem < deckdataJson.ItemIds.length; iitem++) {
        item_data_string += deckdataJson.ItemIds[iitem] + ',';
    }
    set_data += item_data_string;
    set_data += '/';
    update_db_deckinfo[set_db_deck_key] = set_data;
    
    var set_db_deck_info = 'deckcache';
    redisClient.store_redis.hmset(set_db_deck_info, set_db_deck_key, set_data);

    // 현재는 즉시 저장
    var save_deck_count = Object.keys(update_db_deckinfo).length
    if (save_deck_count > update_db_deck_count) {
        var db_query_deck_data = '';
        for (var db_deck_data_key in update_db_deckinfo) {
            db_query_deck_data += update_db_deckinfo[db_deck_data_key];
        }
        mssql.UpdateDBDeckinfo(db_query_deck_data);
        var del_db_deck_info = 'deckcache';
        redisClient.store_redis.del(del_db_deck_info);
    }
}
exports.UpdateDBDeck = UpdateDBDeck;

// DB로 보낼 유저 정보를 저장합니다.
function UpdateDBUserinfo(uniqueid, use_deckid, use_decknation, tutorial_clearidx) {
    update_db_userinfo[uniqueid] = uniqueid + '|' + use_deckid + '|' + use_decknation + '|' + tutorial_clearidx + ',';
    
    var set_db_user_info = 'usercache';
    redisClient.store_redis.hmset(set_db_user_info, uniqueid, update_db_userinfo[uniqueid]);

    var save_userinfo_count = Object.keys(update_db_userinfo).length;
    if (save_userinfo_count > update_db_userinfo_count) {
        var db_query_userinfo_data = '';
        for (var db_userinfo_data_key in update_db_userinfo) {
            db_query_userinfo_data += update_db_userinfo[db_userinfo_data_key];
        }
        mssql.UpdateDBUserinfo(db_query_userinfo_data);
        var del_db_userinfo = 'usercache';
        redisClient.store_redis.del(del_db_userinfo);
    }
}
exports.UpdateDBUserinfo = UpdateDBUserinfo;