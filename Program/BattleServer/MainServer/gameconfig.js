﻿var max_commander_deck = 10;
exports.max_commander_deck = max_commander_deck;

var max_deck_commander_count = 10;
exports.max_deck_commander_count = max_deck_commander_count;

var maxRoomMember = 2;
exports.maxRoomMember = maxRoomMember;

var max_log_out_update_user = 0;
exports.max_log_out_update_user = max_log_out_update_user;

var max_nation_count = 4; // 중립국 : 0, US : 1, Gemany : 2, Soviet : 3, Count : 4
exports.max_nation_count = max_nation_count;

var nationinfo = {
    'US' : 1,
    'GE' : 2,
    'USSR' : 3
}
exports.nationinfo = nationinfo;

var chestinfo = {
    'Empty' : 0,
    'Free' : 1,
    'Crown' : 2,
    'Wooden' : 3,
    'Silver' : 4,
    'Golden' : 5,
    'Magic' : 6,
}
exports.chestinfo = chestinfo

var rankinfo = {
    'Common' : 1,
    'Rear' : 2,
    'Epic' : 3,
    'Max' : 3
}
exports.rankinfo = rankinfo;

var shopinfo = {
    'Commander' : 0,
	'Chest' : 1,
	'Gem' : 2,
	'Gold' : 3
}
exports.shopinfo = shopinfo;

var goodsinfo = {
    'Gem' : 1,
    'Gold' : 2
}
exports.goodsinfo = goodsinfo

var random_chest_min_item_count = 5; // 랭크 1,2의 가지고 있어야 될 최소수
exports.random_chest_min_item_count = random_chest_min_item_count;

var max_area = 6;
exports.max_area = max_area;

var max_shop_commander_count = 6
exports.max_shop_commander_count = max_shop_commander_count;

var today_max_shop_commander_count = 3;
exports.today_max_shop_commander_count = today_max_shop_commander_count;

var max_shop_buy_commander = {
    '1' : 50,
    '2' : 20,
    '3' : 10
}
exports.max_shop_buy_commander = max_shop_buy_commander;

// 1골드당 0.045캐쉬
var change_ratio_cash = 0.045; 
exports.change_ratio_cash = change_ratio_cash;

var max_chest_count = 4;
exports.max_chest_count = max_chest_count;

var max_battle_medal_count = 50;
exports.max_battle_medal_count = max_battle_medal_count;

// 보상 상자 오픈 시간
function GetNormalChestSecond(chesttype) {
    if (chesttype == chestinfo.Wooden)
        return 15;
    else if (chesttype == chestinfo.Silver)
        return 10800;
    else if (chesttype == chestinfo.Golden)
        return 28800;
    else if (chesttype == chestinfo.Magic)
        return 43200;
}
exports.GetNormalChestSecond = GetNormalChestSecond;