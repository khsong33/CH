﻿var logger = require('./logger');
var redisClient = require('./redis_store');
var mssql = require('./mssql');
var gameconfig = require('./gameconfig');
var random = require('./random');
var areatable = require('./table/commanderareatable');
var chesttable = require('./table/randomchesttable');
var commanderleveltable = require('./table/commanderleveltable');

// 새로운 지휘관 아이템이 들어왔을 경우 처리합니다.
function AddCommanderItem(uniqueId, itemNoList, cb_addcommander) {
    var find_user_commander_item = 'uidcommander:' + uniqueId;
    mssql.AddCommanderItem(uniqueId, itemNoList, function (addItemInfoList) {
        var itemInfoCount = addItemInfoList.Items.length;
        for (var iItem = 0; iItem < itemInfoCount; iItem++) {
            var itemInfoJson = {
                'ItemId' : Number(addItemInfoList.Items[iItem].ItemId),
                'ItemNo' : Number(addItemInfoList.Items[iItem].ItemNo),
                'ItemLevel' : 1,
                'ItemCount' : 1,
                'ItemRank' : 1,
                'ItemExp' : 0
            };
            redisClient.store_redis.hmset(find_user_commander_item, String(addItemInfoList.Items[iItem].ItemId), JSON.stringify(itemInfoJson));
        }
        cb_addcommander(itemInfoJson);
    });
}
exports.AddCommanderItem = AddCommanderItem;

// 지휘관 아이템의 경험치를 업데이트합니다.
function UpdateBattleRewardCommanderItem(uniqueId, battle_reward_data, cb_reward_db_query) {
    var find_user_commander_item = 'uidcommander:' + uniqueId;
    var update_exp_commander_count = battle_reward_data.CommanderExp.length;
    var result_exp_data = '';

    var add_commander_count = 0;
    var result_add_data = '';
    if (battle_reward_data.RewardCommander != null) {
        add_commander_count = battle_reward_data.RewardCommander.length;
        // 추가 지휘관을 저장합니다.
        for (var iAddItem = 0; iAddItem < add_commander_count; iAddItem++) {
            result_add_data += (battle_reward_data.RewardCommander[iAddItem] + ',');
        }
    }
    
    // 지휘관 경험치 데이터를 처리합니다.
    if (update_exp_commander_count == 0) { // 경험치가 없다면 넘어갑니다.
        cb_reward_db_query(result_exp_data, result_add_data);
    }
    var current_update_idx = 0;
    for (var iItem = 0; iItem < update_exp_commander_count; iItem++) {
        var update_item_data = battle_reward_data.CommanderExp[iItem];
        redisClient.store_redis.hmget(find_user_commander_item, update_item_data.ItemId, function (err, itemData) {
            var itemDataJson = JSON.parse(itemData);
            
            itemDataJson.ItemExp = battle_reward_data.CommanderExp[current_update_idx].Exp;
            itemDataJson.ItemRank = battle_reward_data.CommanderExp[current_update_idx].Rank;
            
            redisClient.store_redis.hmset(find_user_commander_item, itemDataJson.ItemId, JSON.stringify(itemDataJson));

            result_exp_data += (itemDataJson.ItemId + '|' + itemDataJson.ItemRank + '|' + itemDataJson.ItemExp + ',');
            current_update_idx++;
            if (current_update_idx >= update_exp_commander_count) {
                cb_reward_db_query(result_exp_data, result_add_data);
            }
        });
    }
}
exports.UpdateBattleRewardCommanderItem = UpdateBattleRewardCommanderItem;

// 지휘관 아이템 상자 개봉
function OpenChest(uniqueid, chesttype, nation, area, price_cash, istest, cb_result) {
    
    var current_area_all_item = new Array();
    for (var iarea = 0; iarea <= area; iarea++) {
        current_area_all_item = current_area_all_item.concat(areatable.GetAreaNationCommander(iarea, nation));
    }
    
    var db_result_gold = 0;
    var db_result_cash = price_cash * -1;
    var db_result_card = '';
    
    var result_chest = {};
    // 랭크별 구분
    var new_item_list = {};
    var already_item_list = {};
    var all_item_list = {};
    
    var chest_data = chesttable.GetCheatData(chesttype, area);
    
    result_chest["ChestType"] = Number(chesttype);
    result_chest["ChestArea"] = Number(area);
    
    var find_item_key = 'uiditem:' + uniqueid;
    redisClient.store_redis.hgetall(find_item_key, function (err, commanderitemlist) {
        // 뽑기 가능한 아이템 중 새로운 아이템과 기존 있는 아이템을 분리합니다.
        for (var iitem = 0; iitem < current_area_all_item.length; iitem++) {
            //String(current_area_all_item[iitem].itemno))
            if (!already_item_list.hasOwnProperty(current_area_all_item[iitem].rank))
                already_item_list[current_area_all_item[iitem].rank] = new Array();
            if (!new_item_list.hasOwnProperty(current_area_all_item[iitem].rank))
                new_item_list[current_area_all_item[iitem].rank] = new Array();
            
            if (commanderitemlist.hasOwnProperty(current_area_all_item[iitem].itemno)) {
                current_area_all_item[iitem].newcard = false;
                already_item_list[current_area_all_item[iitem].rank].push(current_area_all_item[iitem]);
            }
            else {
                current_area_all_item[iitem].newcard = true;
                new_item_list[current_area_all_item[iitem].rank].push(current_area_all_item[iitem]);
            }
        }
        // 겹치지 않는 아이템 뽑기를 위한 각랭크별 뽑는 정보를 먼저 확보합니다.
        var already_overlap_index = { '1' : [], '2' : [], '3' : [] };
        var already_overlap_current = { '1' : 0, '2' : 0, '3' : 0 };
        var new_overlap_index = { '1' : [], '2' : [], '3' : [] };
        var new_overlap_current = { '1' : 0, '2' : 0, '3' : 0 };
        var always_new_item = { '1' : false, '2' : false, '3' : false };
        var new_item_ratio = { '1' : 0, '2' : 0, '3': 0 }; // 새로운 아이템이 나오는 확률
        for (var irank = gameconfig.rankinfo.Common; irank <= gameconfig.rankinfo.Max; irank++) {
            // 예외 처리
            // Common과 Rear의 경우 5개 미만의 아이템을 가진 경우 무조건 new 아이템이 나오도록 설정
            if (irank < gameconfig.rankinfo.Epic && already_item_list[irank].length < gameconfig.random_chest_min_item_count) {
                always_new_item[irank] = true;
            }
            already_overlap_index[irank] = random.SyncRandomNotOverlap(0, already_item_list[irank].length);
            new_overlap_index[irank] = random.SyncRandomNotOverlap(0, new_item_list[irank].length);
            if (new_item_list[irank].length > 0) {
                new_item_ratio[irank] = Math.round(new_item_list[irank].length / (already_item_list[irank].length + new_item_list[irank].length) * 100)
            }
        }

        // 골드 분배
        result_chest['Gold'] = Number(random.SyncRandomRange(chest_data.goldmin, chest_data.goldmax+1));
        db_result_gold = Number(random.SyncRandomRange(chest_data.goldmin, chest_data.goldmax + 1));

        // Cash(Gem) 분배
        var active_cash = false;
        if (chest_data.gemratio <= 0)
            active_cash = false;
        else if (random.SyncRandomRange(0, 100) < chest_data.gemratio)
            active_cash = true;
        if (active_cash) {
            result_chest['Cash'] = Number(random.SyncRandomRange(chest_data.gemmin, chest_data.gemmax + 1));
            db_result_cash += Number(random.SyncRandomRange(chest_data.gemmin, chest_data.gemmax + 1));
        }
        
        // Card에 대한 처리
        result_chest['Card'] = new Array();
        var total_card_count = chest_data.totalcard;
        var current_rank_card_count = 0;
        var current_check_rank = gameconfig.rankinfo.Epic;
        var is_change_rank = true;
        var is_get_new_card = false;
        var exception_new_card = false;
        var current_slot_count = chest_data.slotnum;
        for (; current_slot_count > 0;) {
            if (current_check_rank == gameconfig.rankinfo.Common) {
                if (is_change_rank) {
                    is_change_rank = false;
                    if (current_slot_count == 1)
                        is_get_new_card = false;
                    else if (new_item_list[current_check_rank].length <= 0)
                        is_get_new_card = false;
                    else if (always_new_item[current_check_rank])
                        is_get_new_card = true;
                    else
                        is_get_new_card = ((random.SyncRandomRange(1, 101) < new_item_ratio[current_check_rank] ) ? true : false);
                }

                var select_card = null;
                if (!is_get_new_card) {
                    var select_card_index = already_overlap_index[current_check_rank][already_overlap_current[current_check_rank]];
                    select_card = already_item_list[current_check_rank][select_card_index];
                    already_overlap_current[current_check_rank]++;
                }
                else {
                    var select_card_index = new_overlap_index[current_check_rank][new_overlap_current[current_check_rank]];
                    select_card = new_item_list[current_check_rank][select_card_index];
                    new_overlap_current[current_check_rank]++;
                    is_get_new_card = false; // 새로운 카드를 한개 뽑았을 경우 같은 랭크에서는 다시 새로운 카드를 뽑지 않습니다.
                }

                var select_card_count = 0;
                
                if (select_card.newcard)
                    select_card_count = 1;
                else {
                    var max_card_count = Math.round(total_card_count / current_slot_count);

                    if (current_slot_count == current_check_rank) {
                        select_card_count = total_card_count;
                    }
                    else {
                        select_card_count = random.SyncRandomRange(1, max_card_count + 1);
                    }
                }
                
                // 얻을 수 있는 최대 카드수 설정
                if (commanderitemlist.hasOwnProperty(select_card.itemno)) {
                    var select_card_info = JSON.parse(commanderitemlist[select_card.itemno]);
                    var max_commander_item_count = commanderleveltable.GetMaxCommanderItemCount(select_card_info.ItemLevel, current_check_rank)
                    if (max_commander_item_count <= select_card_info.ItemCount + select_card_count) {
                        select_card_count = max_commander_item_count - select_card_info.ItemCount;
                    }
                }

                total_card_count -= select_card_count;
                
                var result_card = {
                    'ItemNo' : Number(select_card.itemno),
                    'Rank' : Number(select_card.rank),
                    'Count' : Number(select_card_count),
                    'New' : Boolean(select_card.newcard)
                }
                result_chest['Card'].push(result_card);
                db_result_card += (result_card.New ? 1 : 0) + '|' + result_card.ItemNo + '|' + result_card.Count + ',';
                current_slot_count--;
            }
            else {
                var rank_count_info = 'rank' + String(current_check_rank) + 'card';
                if (chest_data[rank_count_info] > 0) {
                    if (is_change_rank) {
                        current_rank_card_count = chest_data[rank_count_info];
                        is_change_rank = false;
                        if (current_slot_count == current_check_rank) 
                            is_get_new_card = false;
                        else if (new_item_list[current_check_rank].length <= 0)
                            is_get_new_card = false;
                        else if (always_new_item[current_check_rank])
                            is_get_new_card = true;
                        else
                            is_get_new_card = ((random.SyncRandomRange(1, 101) < new_item_ratio[current_check_rank]) ? true : false);
                    }
                    // 카드 선택
                    var select_card = null;
                    if (!is_get_new_card) {
                        var select_card_index = already_overlap_index[current_check_rank][already_overlap_current[current_check_rank]];
                        select_card = already_item_list[current_check_rank][select_card_index];
                        already_overlap_current[current_check_rank]++;
                    }
                    else {
                        var select_card_index = new_overlap_index[current_check_rank][new_overlap_current[current_check_rank]];
                        select_card = new_item_list[current_check_rank][select_card_index];
                        new_overlap_current[current_check_rank]++;
                        is_get_new_card = false;
                    }
                    
                    // 카드 갯수 확정
                    var select_card_count = 0;
                    if (select_card.newcard || current_rank_card_count == 1) {
                        select_card_count = 1;
                    }
                    else if (current_slot_count == current_check_rank) {
                        select_card_count = current_rank_card_count;
                    }
                    else {
                        select_card_count = random.SyncRandomRange(1, current_rank_card_count + 1);
                    }
                    
                    // 얻을 수 있는 최대 카드수 설정
                    if (commanderitemlist.hasOwnProperty(select_card.itemno)) {
                        var select_card_info = JSON.parse(commanderitemlist[select_card.itemno]);
                        var max_commander_item_count = commanderleveltable.GetMaxCommanderItemCount(select_card_info.ItemLevel, current_check_rank)
                        if (max_commander_item_count <= select_card_info.ItemCount + select_card_count) {
                            select_card_count = max_commander_item_count - select_card_info.ItemCount;
                        }
                    }                 

                    current_rank_card_count -= select_card_count;
                    total_card_count -= select_card_count;
                    
                    var result_card = {
                        'ItemNo' : Number(select_card.itemno),
                        'Rank' : Number(select_card.rank),
                        'Count' : Number(select_card_count),
                        'New' : Boolean(select_card.newcard)
                    }
                    result_chest['Card'].push(result_card);
                    db_result_card += (result_card.New ? 1 : 0) + '|' + result_card.ItemNo + '|' + result_card.Count + ',';
                    current_slot_count--;
                    
                    // 랭크 카드를 모두 찾았을 경우 다음 랭크로 넘어갑니다.
                    if (current_rank_card_count <= 0) {
                        current_check_rank--;
                        is_change_rank = true;
                        current_rank_card_count = true;
                    }
                }
                else {
                    current_check_rank--;
                    is_change_rank = true;
                    current_rank_card_count = true;
                }
            }
        }
        if (!istest) {
            mssql.UpdateChest(uniqueid, db_result_gold, db_result_cash, db_result_card, function (db_result_chest) {
                db_result_chest["ChestType"] = Number(chesttype);
                db_result_chest["Area"] = Number(area);
                cb_result(db_result_chest);
            });
        }
        else {
            for (var icard = 0; icard < result_chest.Card.length; icard++) {
                for (var icard2 = icard + 1; icard2 < result_chest.Card.length; icard2++) {
                    if (result_chest.Card[icard].ItemNo == result_chest.Card[icard2].ItemNo) {
                        console.error("Same Item Drop!!!! - " + result_chest.Card[icard2].ItemNo);
                    }
                }
            }
            cb_result(result_chest);
        }
    });
}
exports.OpenChest = OpenChest;

function UpdateResultChest(userinfoJson, result_chest) {
    // redis 정보 셋팅
    userinfoJson.Gold = result_chest.Gold;
    userinfoJson.Cash = result_chest.Cash;
    var set_userinfo_key = 'user:' + userinfoJson.UserKey;
    redisClient.store_redis.set(set_userinfo_key, JSON.stringify(userinfoJson));
    
    var find_commanderitem_key = 'uidcommander:' + userinfoJson.UniqueId;
    redisClient.store_redis.hgetall(find_commanderitem_key, function (err, commanderitemlist) {
        for (var iitem = 0; iitem < result_chest.Items.length; iitem++) {
            
            var commanderiteminfojson = undefined;  
            // 신규 아이템이 
            if (!commanderitemlist.hasOwnProperty(result_chest.Items[iitem].ItemId)) {
                commanderiteminfojson = _NewCommanderItem(result_chest.Items[iitem].ItemId, result_chest.Items[iitem].ItemNo);
            }
            else {
                commanderiteminfojson = JSON.parse(commanderitemlist[result_chest.Items[iitem].ItemId]);
                commanderiteminfojson.ItemCount = Number(result_chest.Items[iitem].ItemCount);
            }
            var set_commanderitem_key = 'uidcommander:' + userinfoJson.UniqueId;
            redisClient.store_redis.hmset(set_commanderitem_key, result_chest.Items[iitem].ItemId, JSON.stringify(commanderiteminfojson));
        }
    });
    var find_item_key = 'uiditem:' + userinfoJson.UniqueId;
    redisClient.store_redis.hgetall(find_item_key, function (err, itemlist) {
        for (var iitem = 0; iitem < result_chest.Items.length; iitem++) {
            var commanderiteminfojson = undefined;
            if (!itemlist.hasOwnProperty(result_chest.Items[iitem].ItemNo)) {
                commanderiteminfojson = _NewCommanderItem(result_chest.Items[iitem].ItemId, result_chest.Items[iitem].ItemNo);
            }
            else {
                commanderiteminfojson = JSON.parse(itemlist[result_chest.Items[iitem].ItemNo]);
                commanderiteminfojson.ItemCount = Number(result_chest.Items[iitem].ItemCount);
            }
            
            var set_item_key = 'uiditem:' + userinfoJson.UniqueId;
            redisClient.store_redis.hmset(set_item_key, result_chest.Items[iitem].ItemNo, JSON.stringify(commanderiteminfojson));
        }
    });
}
exports.UpdateResultChest = UpdateResultChest;
// 상점에서 구매한 아이템을 업데이트합니다.
function UpdateShopCommander(uniqueid, itemid, itemno, itemcount) {
    var find_commander_itemid = 'uidcommander:' + uniqueid;
    redisClient.store_redis.hmget(find_commander_itemid, itemid, function (err, commanderitemid) {
        var commander_id_json = null;
        if (commanderitemid[0] == null) {
            commander_id_json = _NewCommanderItem(itemid, itemno);
        }
        else {
            commander_id_json = JSON.parse(commanderitemid);
            commander_id_json.ItemCount = itemcount;
        }
        var set_commander_itemid = 'uidcommander:' + uniqueid;
        redisClient.store_redis.hmset(set_commander_itemid, itemid, JSON.stringify(commander_id_json));
    });
    
    var find_commander_itemno = 'uiditem:' + uniqueid;
    redisClient.store_redis.hmget(find_commander_itemno, itemno, function (err, commanderitemno) {
        var commander_no_json = null;
        if (commanderitemno[0] == null) {
            commander_no_json = _NewCommanderItem(itemid, itemno);
        }
        else {
            commander_no_json = JSON.parse(commanderitemno);
            commander_no_json.ItemCount = itemcount;
        }
        var set_commander_itemno = 'uiditem:' + uniqueid;
        redisClient.store_redis.hmset(set_commander_itemno, itemno, JSON.stringify(commander_no_json));
    });
}
exports.UpdateShopCommander = UpdateShopCommander;
function _NewCommanderItem(itemid, itemno) {
    var itemInfoJson = {
        'ItemId' : Number(itemid),
        'ItemNo' : Number(itemno),
        'ItemLevel' : 1,
        'ItemCount' : 1,
        'ItemRank' : 1,
        'ItemExp' : 0
    };
    return itemInfoJson;
}