﻿var util = require('util');
var sio = require('socket.io');
var redisClient = require('./redis_store');
var receiver = require('./receiver');
var recvchest = require('./recvchest');
var logger = require('./logger');
var mssql = require('./mssql');
var gameshop = require('./shop');

var random = require('./random');

var stage = require('./stage');
var gameconfig = require('./gameconfig');

var serverconfig = require('./serverconfig').getServerConfig();

var custom_port = serverconfig.port;

var io = sio.listen(custom_port);
redisClient.initRedis();
redisClient.initCallback(CallbackSubscribe);

var log = new logger();
log.info("Start Main Server");
mssql.InitSql(function () {
    gameshop.InitShopChestDB();
    gameshop.InitShopGoodsInfo();
    gameshop.InitShopCommanderPriceInfo();
});

io.on('connection', function (socket) {
    // 접속 종료
    socket.on('disconnect', function () {
        receiver.OnDisconnected(io, socket);
    })
    // 유저 로그인 및 신규유저 등록
    socket.on('cs_login', function (data) {
        receiver.OnLogin(io, socket, data);
    })
    socket.on('cs_retry_login', function (data) {
        receiver.OnRetryLogin(io, socket, data);
    })
    socket.on('cs_reconnect', function (data) {
        receiver.OnReconnect(io, socket, data);
    })
    socket.on('cs_logout', function (data) {
        receiver.OnDisconnected(io, socket);
    })
    // 대기열 등록
    socket.on('cs_wait_registe', function (data) {
        receiver.OnWaitRegiste(io, socket, data);
    })
    // 대기열 해제
    socket.on('cs_wait_unregiste', function (data) {
        receiver.OnWaitUnregiste(io, socket, data);
    });
    // 지휘관 카드 리스트 전송
    socket.on('cs_update_commanditemdeck', function (data) {
        receiver.OnUpdateCommandItemDeck(io, socket, data);
    });
    // 지휘관 덱 생성
    socket.on('cs_add_commanditemdeck', function (data) {
        receiver.OnAddCommanderItemDeck(io, socket, data);
    });
    // 지휘관 덱 삭제
    socket.on('cs_remove_commanditemdeck', function (data) {
        receiver.OnRemoveCommanderItemDeck(io, socket, data);
    });
    // 리플레이 리스트 전송
    socket.on('cs_replaylist', function (data) {
        receiver.OnReplayList(io, socket, data);
    });
    // 리플레이 데이터 전송
    socket.on('cs_replaydata', function (data) {
        receiver.OnReplayData(io, socket, data);
    });
    // 전투 보상 처리
    socket.on('cs_battle_reward', function (data) {
        //receiver.OnBattleReward(io, socket, data);
        recvchest.OnBattleReward(io, socket, data);
    });
    // [개발용 패킷]전투 보상 입력
    socket.on('cs_dev_battle_reward', function (data) {
        receiver.OnDevBattleReward(io, socket, data);
    });
    // 튜토리얼 시작
    socket.on('cs_tutorial_start', function (data) {
        receiver.OnTutorialStart(io, socket, data);
    });
    // 튜토리얼 종료
    socket.on('cs_tutorial_end', function (data) {
        receiver.OnTutorialEnd(io, socket, data);
    });
    // 카드 레벨업
    socket.on('cs_commander_levelup', function (data) {
        receiver.OnCommanderItemLevelUp(io, socket, data);
    });
    // 상자 열기
    socket.on('cs_open_chest', function (data) {
        receiver.OnOpenChest(io, socket, data); 
    });
    // 샵 물품 목록
    socket.on('cs_shop_list', function (data) {
        receiver.OnShopList(io, socket, data);
    });
    // 샵 상자 구매
    socket.on('cs_shop_chest_buy', function (data) {
        receiver.OnShopChestBuy(io, socket, data);
    });
    // 샵 골드 구매
    socket.on('cs_shop_gold_buy', function (data) {
        receiver.OnShopGoldBuy(io, socket, data);
    });
    socket.on('cs_shop_buy_gem', function (data) {
        receiver.OnShopCashBuy(io, socket, data);
    });
    // 샵 지휘관 구매
    socket.on('cs_shop_commander_buy', function (data) {
        receiver.OnShopCommanderBuy(io, socket, data);
    });
    // 상자 리스트 요청
    socket.on('cs_chest_list', function (data) {
        recvchest.OnChestList(io, socket, data);
    });
    // 보상 상자 활성화
    socket.on('cs_active_normal_chest', function (data) {
        recvchest.OnActiveNormalChest(io, socket, data);
    });
    // 보상 상자 오픈
    socket.on('cs_open_normal_chest', function (data) {
        recvchest.OnOpenNormalChest(io, socket, data);
    });
    // 무료 상자 오픈
    socket.on('cs_open_free_chest', function (data) {
        recvchest.OnOpenFreeChest(io, socket, data);
    });
    // 메달 상자 오픈
    socket.on('cs_open_medal_chest', function (data) {
        recvchest.OnOpenMedalChest(io, socket, data);
    });
    // 지휘관 덱 업데이트
    socket.on('cs_update_deck', function (data) {
        receiver.OnUpdateDeck(io, socket, data);
    });
});

// 예외처리
process.on('uncaughtException', function (err) {
    var log = new logger();
    log.error('Caught exception: ' + err);
});

function _RegistBattleServer() {
    var os = require('os');
    var ifaces = os.networkInterfaces();
    Object.keys(ifaces).forEach(function (ifname) {
        ifaces[ifname].forEach(function (iface) {
            if ('IPv4' !== iface.family || iface.internal !== false) {
                // skip over internal (i.e. 127.0.0.1) and non-ipv4 addresses
                return;
            }
            var lConnectionAddress = "ws://" + iface.address + ":" + custom_port + "/socket.io/?EIO=4&transport=websocket";
            var lConnectionAddressKey = iface.address + ',' + custom_port;
            redisClient.store_redis.hmset('main_server', lConnectionAddressKey, lConnectionAddress);
        });
    });
}
// redis 구독 처리
function CallbackSubscribe(channel, message) {
    // console.log("[" + channel + "] - " + message);
    var arr = channel.split(':');
    if (arr[0] == "roomstart") {
        //console.log('roomstart - ' + arr[1]);
        receiver.OnRoomStart(io, arr[1], message);
    }
}