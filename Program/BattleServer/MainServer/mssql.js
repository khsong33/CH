﻿var mssql = require('mssql');
var logger = require('./logger');
var gameconfig = require('./gameconfig');
var serverconfig = require('./serverconfig').getServerConfig();
var commandertable = require('./table/commandertable');
var shop = require('./shop');

var configQA = {
    user: 'saych',
    password: 'saych!!!',
    server: 'dbmsdev1.nwz.kr', 
    port: '14334',
    database: 'CH_KR_GAME',
    pool: { idelTimeout : 30 }
}
var configffdev = {
    user: 'chdb',
    password: 'ffdev123!',
    server: 'ffdev', 
    database: 'CH_gamedb',
    pool: { idleTimeout : 30 }
}
// DB 초기화 및 연결
var connection = null;
var dbconfig = serverconfig.store_mssql;
function InitSql(cb_connect) {
    connection = new mssql.Connection(dbconfig, function (err) {
        if (err) {
            console.log("mssql connection err - " + err);
        }
        else {
            console.log("connect success db - " + dbconfig.server);
            cb_connect();
        }
    });
}
exports.InitSql = InitSql;

// 유저 정보 불러오기 // lagacy
function getUserLogin(username, cb_userinfo) {
    var request = new mssql.Request(connection);
    request.input('UserName', username);
    request.execute('LoginCH2User', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute LoginCH2User");
        }
        
        var userinfo_json = _ConvertUserInfoDBtoJson(recordsets[0], 0, undefined);
        cb_userinfo(userinfo_json);
    })
}
exports.getUserLogin = getUserLogin;
// 유저 카드 정보 가져오기 (전체덱을 가져오려면 find_deck_id에 0보다 작은값을 넣습니다)
function getUserCommanderItem(userinfo_json, find_deck_id, cb_commanderitem) {
    var request = new mssql.Request(connection);
    request.input('UniqueId', userinfo_json.UniqueId);
    request.input('UseDeckId', find_deck_id);
    request.execute('GetCH2CommanderItem', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2CommanderItem");
        }
        var user_commander = _ConvertCommanderItemDBtoJson(recordsets[0], userinfo_json.UniqueId);
        cb_commanderitem(userinfo_json, user_commander)
    })
}
exports.getUserCommanderItem = getUserCommanderItem;
// User Deck 정보 얻어오기
function getUserDeck(userinfoJson, cb_deckinfo) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', userinfoJson.UniqueId);
    request.execute('GetCH2Deck', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2Deck");
        }
        var deckinfo = _ConvertDeckDBtoJson(recordsets[0], userinfoJson.UniqueId);
        cb_deckinfo(userinfoJson, deckinfo);
    });
}
exports.getUserDeck = getUserDeck;
// AI 리스트 얻어오기
function getAIList(listCount, minRating, maxRating, addSec, cb_aiList, isOverwrite) {
    var ai_user_count = 0;
    var request = new mssql.Request(connection);
    request.input('ListCount', Number(listCount));
    request.input('MinRating', Number(minRating));
    request.input('MaxRating', Number(maxRating));
    request.input('AddSeconds', Number(addSec));
    request.execute('GetAiList', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetAIList - " + err);
        }
        var aiUserList = {
            'Users' : []
        }
        var aiListCount = recordsets[0].length;
        for (var iUsers = 0; iUsers < aiListCount; iUsers++) {
            var UseDeckId = Number(recordsets[0][iUsers]["UseDeckId"]);
            var ai_userinfo =_ConvertUserInfoDBtoJson(recordsets[0], iUsers, undefined);
            
            getUserCommanderItem(ai_userinfo, ai_userinfo.UseDeckId, function (ai_userinfo_cb, commanderItem) {
                ai_userinfo_cb.Decks = commanderItem;
                aiUserList.Users.push(ai_userinfo_cb);
                ai_user_count++;
                if (ai_user_count >= aiListCount) {
                    cb_aiList(aiUserList, minRating, maxRating, isOverwrite);
                }
            });
        }// end for
    });// end excute
}
exports.getAIList = getAIList;
// 리플레이를 위한 로그키 획득
function getLogkey(uniqueId, getCount, cb_GetLogkey) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inCount', Number(getCount));
    request.execute('GetCH2LogKey', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2LogKey - " + err);
        }
        var logKeyList = {
            'Keys' : []
        }
        var logKeyCount = recordsets[0].length;
        for (var iLogKey = 0; iLogKey < logKeyCount; iLogKey++) {
            var logtimeString = String(recordsets[0][iLogKey]["LogTime"].toISOString());
            var logKey = {
                'LogKey' : String(recordsets[0][iLogKey]["LogKey"]),
                'LogTime' : logtimeString
            }
            logKeyList.Keys.push(logKey);
        }// end for
        cb_GetLogkey(logKeyList);
    });// end excute
}
exports.getLogkey = getLogkey;
// 리플레이 데이터 획득
function getReplayData(logKey, cb_replayData) {
    var request = new mssql.Request(connection);
    request.input('inLogKey', String(logKey));
    request.execute('GetCH2ReplayData', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2ReplayData - " + err);
        }
        var replay_data = {
            'Datas' : []
        };
        var match_info_json = JSON.parse(recordsets[0][0]["Matchinfo"]);
        replay_data.MatchInfo = match_info_json;
        var replaydataCount = recordsets[1].length;
        for (var iData = 0; iData < replaydataCount; iData++) {
            var log_data = {
                'Data' : String(recordsets[1][iData]["Frame"]) + ":" + String(recordsets[1][iData]["Data"])
            };
            replay_data.Datas.push(log_data);
        }
        cb_replayData(replay_data);
    });
}
exports.getReplayData = getReplayData;
// 지휘관 추가
function AddCommanderItem(uniqueId, itemNoList, cb_addItemList) {
    var itemNoListString = '';
    for (var iItem = 0; iItem < itemNoList.length; iItem++) {
        itemNoListString += (itemNoList[iItem] + ',');
    }
    
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inItemNoList', String(itemNoListString));
    request.execute('AddCH2CommanderItem', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute AddCH2CommanderItem - " + err);
        }
        var addItemList = {
            'Items' : []
        };
        for (var iItem = 0; iItem < recordsets[0].length; iItem++) {
            var addItem = {
                'ItemId' : Number(recordsets[0][iItem]["ItemId"]),
                'ItemNo' : Number(recordsets[0][iItem]["ItemNo"])
            };
            addItemList.Items.push(addItem);
        }
        cb_addItemList(addItemList);
    });
}
exports.AddCommanderItem = AddCommanderItem;

// 전투 경험치 업데이트
function UpdateBattleReward(uniqueId, nation, nationExp, nationLevel, update_exp_data, add_commander_data, win, winstreak, lose, losestreak, cb_addItemList) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.input('inNation', Number(nation));
    request.input('inNationExp', Number(nationExp));
    request.input('inNationLevel', Number(nationLevel));
    request.input('inUpdateCommanderData', String(update_exp_data));
    request.input('inAddCommanderData', String(add_commander_data));
    request.input('inWin', Number(win));
    request.input('inWinStreak', Number(winstreak));
    request.input('inLose', Number(lose));
    request.input('inLoseStreak', Number(losestreak));
    request.execute('UpdateCH2BattleReward2', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2BattleReward2 - " + err);
        }
        var addItemList = {
            'Items' : []
        };
        for (var iItem = 0; iItem < recordsets[0].length; iItem++) {
            var addItem = {
                'ItemId' : Number(recordsets[0][iItem]["ItemId"]),
                'ItemNo' : Number(recordsets[0][iItem]["ItemNo"])
            };
            addItemList.Items.push(addItem);
        }
        cb_addItemList(addItemList);
    });
}
exports.UpdateBattleReward = UpdateBattleReward;

// 로그인시 필요한 정보 전체 획득
function GetLoginInfo(username, cb_infos) {
    var request = new mssql.Request(connection);
    request.input('inUserName', String(username));
    request.execute('GetCH2LoginInfo', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2LoginInfo - " + err);
        }
        // user info
        var userinfo_json = _ConvertUserInfoDBtoJson(recordsets[0], 0, recordsets[4]);
        // commander item
        var user_commander_json = _ConvertCommanderItemDBtoJson(recordsets[1], userinfo_json.UniqueId);
        // deck
        var deckinfo_json = _ConvertDeckDBtoJson(recordsets[2], userinfo_json.UniqueId);
        // shop commander item list
        var shop_commander_json = _ConvertShopCommanderItemList(recordsets[3]);
        
        cb_infos(userinfo_json, user_commander_json, deckinfo_json, shop_commander_json);
    });
}
exports.GetLoginInfo = GetLoginInfo;

// 튜토리얼 보상 정보를 DB에서 Json저장합니다.
function GetStageInfo(cb_infos) {
    var request = new mssql.Request(connection);
    var query = 'SELECT StageId, Kind, ParamString, Param1, Param2, Param3, Param4 FROM ch2_stageinfo';
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("failed Excute Stage Info query - " + err);
        }
        var stage_data_json = {
            'datas' : []
        };
        for (var idata = 0; idata < recordsets.length; idata++) {
            var stage_info_json = {
                'StageId' : Number(recordsets[idata]['StageId']),
                'Kind' : String(recordsets[idata]['Kind']),
                'ParamString' : String(recordsets[idata]['ParamString']),
                'Param1' : Number(recordsets[idata]['Param1']),
                'Param2' : Number(recordsets[idata]['Param2']),
                'Param3' : Number(recordsets[idata]['Param3']),
                'Param4' : Number(recordsets[idata]['Param4'])
            };
            stage_data_json.datas.push(stage_info_json);
        }
        cb_infos(stage_data_json);
    });
}
exports.GetStageInfo = GetStageInfo;

// 튜토리얼 진행 상황을 DB에 저장합니다.
function UpdateTutorialInfo(uniqueId, cb_tutorial_info) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', Number(uniqueId));
    request.execute('UpdateCH2Tutorial', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2Tutorial - " + err);
        }
        // user info
        var tutorial_clear_info = 
        {
            'UniqueId' : Number(recordsets[0][0]['UniqueId']),
            'ClearIdx' : Number(recordsets[0][0]['ClearTutorial'])
        };
        cb_tutorial_info(tutorial_clear_info);
    });
}
exports.UpdateTutorialInfo = UpdateTutorialInfo;

// 로그아웃 유저 리스트 DB 전송
function UpdateDBUserinfo(logoutUserList) {
    var request = new mssql.Request(connection);
    request.input('inLogoutUserList', logoutUserList);
    request.execute('UpdateCH2Userinfo2', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2Userinfo2 - " + err);
        }
    });
}
exports.UpdateDBUserinfo = UpdateDBUserinfo;
// 유저 정보 DB 전송
function UpdateDBDeckinfo(commandItem) {
    var request = new mssql.Request(connection);
    request.input('inCommanderItem', commandItem);
    request.execute('UpdateCH2CommandItemDeck', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute UpdateCH2CommandItemDeck - " + err);
        }
    });
}
exports.UpdateDBDeckinfo = UpdateDBDeckinfo;
// 지휘관 아이템 레벨업
function UpdateCommanderItemLevel(uniqueid, commanderitem, needcard, costgold, costcash, nationinfo, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inCommanderItemNo', commanderitem.ItemNo);
    request.input('inNeedCard', needcard);
    request.input('inCostGold', costgold);
    request.input('inCostCash', costcash);
    request.input('inNation', nationinfo.nation);
    request.input('inNationExp', nationinfo.nationexp);
    request.input('inNationLevel', nationinfo.nationlevel);
    request.execute('UpdateCH2CommanderLevel', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2CommanderLevel - " + err);
        }
        var item_levelup_result = {
            'UniqueId' : Number(recordsets[0][0]["UniqueId"]),
            'ItemId' : Number(recordsets[0][0]["ItemId"]),
            'ItemNo' : Number(recordsets[0][0]["ItemNo"]),
            'ItemLevel' : Number(recordsets[0][0]["ItemLevel"]),
            'ItemCount' : Number(recordsets[0][0]["ItemCount"]),
            'Gold' : Number(recordsets[0][0]["Gold"]),
            'Cash' : Number(recordsets[0][0]["Cash"])
        };
        cb_result(item_levelup_result);
    });
}
exports.UpdateCommanderItemLevel = UpdateCommanderItemLevel;

// 보상 상자 획득
function UpdateChest(uniqueid, gold, cash, cards, cb_chest) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inGold', gold);
    request.input('inCash', cash);
    request.input('inItem', cards);
    request.execute('UpdateCH2Chest', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2Chest - " + err);
        }
        var chest_result = {
            'UniqueId' : Number(recordsets[0][0]["UniqueId"]),
            'Gold' : Number(recordsets[0][0]["Gold"]),
            'Cash' : Number(recordsets[0][0]["Cash"]),
            'Items' : []
        };
        for (var iitem = 0; iitem < recordsets[1].length; iitem++) {
            var item_data = {
                'ItemId' : Number(recordsets[1][iitem]["ItemId"]),
                'ItemNo' : Number(recordsets[1][iitem]["ItemNo"]),
                'ItemCount' : Number(recordsets[1][iitem]["ItemCount"])
            };
            chest_result.Items.push(item_data);
        }
        cb_chest(chest_result);
    });
}
exports.UpdateChest = UpdateChest;
// 재화 골드 업데이트
function ShopBuyGold(uniqueid, price, gold, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inPrice', price);
    request.input('inGold', gold);
    request.execute('AddCH2ShopGold', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute AddCH2ShopGold - " + err);
        }
        var shop_gold_result = {
            'Gold' : Number(recordsets[0][0]["Gold"]),
            'Cash' : Number(recordsets[0][0]["Cash"])
        };
        cb_result(shop_gold_result);
    });
}
exports.ShopBuyGold = ShopBuyGold;
// 재화 캐쉬 업데이트
function ShopBuyCash(uniqueid, cash, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inCash', cash);
    request.execute('AddCH2ShopCash', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute AddCH2ShopCash - " + err);
        }
        var shop_cash_result = { 'Cash' : Number(recordsets[0][0]["Cash"]) };
        cb_result(shop_cash_result);
    });
}
exports.ShopBuyCash = ShopBuyCash;
// 상점 상자 정보 획득
function GetShopChestInfo(cb_info) {
    var request = new mssql.Request(connection);
    var query = 'SELECT Type, Area, Count, Price FROM ch2_shopchesttable';
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("failed Excute Shop Chest query - " + err);
        }
        var shop_chest_info = {
            'datas' : []
        };
        for (var idata = 0; idata < recordsets.length; idata++) {
            var shop_chest_info_json = {
                'Type' : Number(gameconfig.chestinfo[recordsets[idata]['Type']]),
                'Area' : Number(recordsets[idata]['Area']),
                'Count' : Number(recordsets[idata]['Count']),
                'Price' : Number(recordsets[idata]['Price'])
            };
            shop_chest_info.datas.push(shop_chest_info_json);
        }
        cb_info(shop_chest_info);
    });
}
exports.GetShopChestInfo = GetShopChestInfo;
// 상점 재화 정보 획득
function GetShopGoodsInfo(cb_info) {
    var request = new mssql.Request(connection);
    var query = 'SELECT GoodsId, Type, Count, PriceGem, PriceCash FROM ch2_shopgoodstable';
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("failed Excute Shop Goods query - " + err);
        }
        var shop_goods_info = {
            'datas' : []
        };
        for (var idata = 0; idata < recordsets.length; idata++) {
            var shop_goods_info_json = {
                'GoodsId' : Number(recordsets[idata]['GoodsId']),
                'Type' : Number(gameconfig.goodsinfo[recordsets[idata]['Type']]),
                'Count' : Number(recordsets[idata]['Count']),
                'PriceGem' : Number(recordsets[idata]['PriceGem']),
                'PriceCash' : Number(recordsets[idata]['PriceCash'])
            };
            shop_goods_info.datas.push(shop_goods_info_json);
        }
        cb_info(shop_goods_info);
    });
}
exports.GetShopGoodsInfo = GetShopGoodsInfo;
// 샵 지휘관 정보를 얻어옵니다.
function GetShopCommanderInfo(uniqueid, commanderlist, cb_shopcommander) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    for (var icommander = 0; icommander < gameconfig.max_shop_commander_count; icommander++) {
        var input_column = 'inCommander' + icommander;
        request.input(input_column, commanderlist[icommander]);
    }
    request.execute('GetCH2ShopCommander', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute GetCH2ShopCommander - " + err);
        }
        var result_shop_commander = _ConvertShopCommanderItemList(recordsets[0]);
        cb_shopcommander(result_shop_commander);
    });
}
exports.GetShopCommanderInfo = GetShopCommanderInfo;
// 샵지휘관 구매 가격 관련 정보를 얻어옵니다.
function GetShopCommanderPriceInfo(cb_info) {
    var request = new mssql.Request(connection);
    var query = 'SELECT Rank, Count, Cost FROM ch2_shopcardtable';
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("failed Excute Shop Goods query - " + err);
        }
        var shop_price_info = [];
        for (var idata = 0; idata < recordsets.length; idata++) {
            var info = {
                'Rank' : Number(gameconfig.rankinfo[recordsets[idata]['Rank']]),
                'Count' : Number(recordsets[idata]['Count']),
                'Cost' : Number(recordsets[idata]['Cost'])
            };
            shop_price_info.push(info);
        }
        cb_info(shop_price_info);
    });
}
exports.GetShopCommanderPriceInfo = GetShopCommanderPriceInfo;
// 지휘관 상점 구매 내역 업데이트
function UpdateShopCommander(uniqueid, shopidx, costgold, costcash, cb_shopcommander) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inShopIndex', shopidx);
    request.input('inCostGold', costgold);
    request.input('inCostCash', costcash);
    request.execute('UpdateCH2ShopCommander', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2ShopCommander - " + err);
        }
        var result_shop_commander = {
            'ShopIndex' : Number(recordsets[0][0]["ShopIndex"]),
            'ShopCount' : Number(recordsets[0][0]["ShopCount"]),
            'Gold' : Number(recordsets[0][0]["Gold"]),
            'Cash' : Number(recordsets[0][0]["Cash"]),
            'ItemId' : Number(recordsets[1][0]["ItemId"]),
            'ItemNo' : Number(recordsets[1][0]["ItemNo"]),
            'ItemLevel' : Number(recordsets[1][0]["ItemLevel"]),
            'ItemCount' : Number(recordsets[1][0]["ItemCount"])
        };
        cb_shopcommander(result_shop_commander);
    });

}
exports.UpdateShopCommander = UpdateShopCommander;
// 현재 선택 국가 상자 정보 획득
function GetChestData(uniqueid, nation, cb_chest) {
    var request = new mssql.Request(connection);
    var query = 'SELECT FCTime0, FCTime1, MCCount, MCTime0, NCType0, NCState0, NCTime0, NCArea0, NCType1, NCState1, NCTime1, NCArea1, ';
    query += 'NCType2, NCState2, NCTime2, NCArea2, NCType3, NCState3, NCTime3, NCArea3 ';
    query += 'FROM ch2_nation WHERE UniqueId = ' + uniqueid + ' AND Nation = ' + nation;
    request.query(query, function (err, recordsets) {
        if (err) {
            var log = new logger();
            log.error("Failed Query GetChestData - " + err);
        }
        var result_chest = {
            'FCTime0' : recordsets[0]["FCTime0"].toISOString(),
            'FCTime1' : recordsets[0]["FCTime1"].toISOString(),
            'MCCount' : Number(recordsets[0]["MCCount"]),
            'MCMaxCount' : Number(gameconfig.max_battle_medal_count),
            'MCTime' : recordsets[0]["MCTime0"].toISOString(),
            'NCInfo' : []
        };
        //
        for (var ichest = 0; ichest < gameconfig.max_chest_count; ichest++) {
            var db_type_col = 'NCType' + ichest;
            var db_state_col = 'NCState' + ichest;
            var db_time_col = 'NCTime' + ichest;
            var db_area_col = 'NCArea' + ichest;
            var normal_chest_info = {
                'SlotIdx' : Number(ichest),
                'Type' : Number(recordsets[0][db_type_col]),
                'State' : Number(recordsets[0][db_state_col]),
                'Area' : Number(recordsets[0][db_area_col]),
                'Time' : recordsets[0][db_time_col].toISOString()
            };
            result_chest.NCInfo.push(normal_chest_info);
        }
        cb_chest(result_chest);
    });
}
exports.GetChestData = GetChestData
// 보상상자 획득
function UpdatePVPRewardChest(reward_info, cb_result_reward) {
    var chest_slot_idx = -1;
    var chest_type = -1;
    if (reward_info.hasOwnProperty('RewardChest')) {
        chest_slot_idx = reward_info.RewardChest.Slot;
        chest_type = reward_info.RewardChest.Type;
    }
    
    var request = new mssql.Request(connection);
    request.input('inUniqueId', reward_info.UniqueId);
    request.input('inWin', reward_info.UserInfo.Win);
    request.input('inLose', reward_info.UserInfo.Lose);
    request.input('inWinStreak', reward_info.UserInfo.WinStreak);
    request.input('inLoseStreak', reward_info.UserInfo.LoseStreak);
    request.input('inArea', reward_info.UserInfo.Area);
    request.input('inRating', reward_info.UserInfo.Rating);
    request.input('inChestSlotIdx', chest_slot_idx);
    request.input('inChestType', chest_type);
    request.input('inAddMedal', reward_info.GainMedal);
    request.input('inTotalMedal', reward_info.TotalMedal);
    request.execute('UpdateCH2BattleReward3', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2BattleReward3 - " + err);
        }
        var result_reward = {
            'Win' : Number(recordsets[0][0]["Win"]),
            'WinStreak' : Number(recordsets[0][0]["WinStreak"]),
            'Lose' : Number(recordsets[0][0]["Lose"]),
            'LoseStreak' : Number(recordsets[0][0]["LoseStreak"]),
            'Area' : Number(recordsets[0][0]["Area"]),
            'Rating' : Number(recordsets[0][0]["Rating"]),
            'ChestSlot' : Number(recordsets[0][0]["ChestSlot"]),
            'ChestType' : Number(recordsets[0][0]["ChestType"]),
            'TotalMedal' : Number(recordsets[0][0]["TotalMedal"]),
            'GainMedal' : Number(recordsets[0][0]["GainMedal"])
        }
        cb_result_reward(result_reward);
    });
}
exports.UpdatePVPRewardChest = UpdatePVPRewardChest;
// 보상 상자 업데이트
function UpdateNormalChest(uniqueid, nation, slotid, addsecond, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inNation', nation);
    request.input('inSlotIdx', slotid);
    request.input('inAddSecond', addsecond);
    request.execute('UpdateCH2NormalChest', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2NormalChest - " + err);
        }
        var result_normalchest = {
            'Nation' : Number(recordsets[0][0]["Nation"]),
            'SlotIdx' : Number(recordsets[0][0]["SlotIdx"]),
            'ChestTime' : recordsets[0][0]["ChestTime"].toISOString()
        }
        cb_result(result_normalchest);
    });
}
exports.UpdateNormalChest = UpdateNormalChest;
// 일반 보상 상자 열기
function UpdateOpenNormalChest(uniqueid, nation, slotid, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inNation', nation);
    request.input('inSlotIdx', slotid);
    request.execute('UpdateCH2OpenNormalChest', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2OpenNormalChest - " + err);
        }
        var result_open = {
            'SlotIdx' : Number(recordsets[0][0]["SlotIdx"]),
            'Nation' : Number(recordsets[0][0]["Nation"]),
            'RemainSec' : Number(recordsets[0][0]["RemainSec"]),
            'ChestType' : Number(recordsets[0][0]["ChestType"])
        };
        cb_result(result_open);
    });
}
exports.UpdateOpenNormalChest = UpdateOpenNormalChest;
// 무료 상자 열기
function UpdateOpenFreeChest(uniqueid, nation, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inNation', nation);
    request.execute('UpdateCH2FreeChest', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Execute UpdateCH2FreeChest - " + err);
        }
        var result_freechest = {
            'Success' : false
        };
        // 아직 오픈 시간이 아닌 경우
        if (returnvalue < 0) {
            result_freechest.Success = false;
        }
        else {
            result_freechest.Success = true;
            result_freechest["Time0"] = recordsets[0][0]["FCTime0"].toISOString();
            result_freechest["Time1"] = recordsets[0][0]["FCTime1"].toISOString();
        }
        cb_result(result_freechest);
    });
}
exports.UpdateOpenFreeChest = UpdateOpenFreeChest;
// 메달 상자 오픈
function UpdateOpenMedalChest(uniqueid, nation, cb_result) {
    var request = new mssql.Request(connection);
    request.input('inUniqueId', uniqueid);
    request.input('inNation', nation);
    request.execute('UpdateCH2MedalChest', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.exitOnError("Failed Execute UpdateCH2MedalChest - " + err);
        }
        var result_medalchest = {
            'Time' : recordsets[0][0]["MCTime0"].toISOString()
        };
        cb_result(result_medalchest);
    });
}
exports.UpdateOpenMedalChest = UpdateOpenMedalChest;
// 전투원 덱 업데이트
function UpdateDeck(uniqueid, decknation, deckinfolist, usedeckid, cb_result)
{
	var request = new mssql.Request(connection);
	request.input('inUniqueId', uniqueid);
	request.input('inDeckNation', decknation);
	request.input('inUpdateItem', deckinfolist);
	request.input('inUseDeckId', usedeckid);
	request.execute('UpdateCH2Deck', function (err, recordsets, returnvalue) {
		if (err) {
			var log = new logger();
			log.exitOnError("Failed Execute UpdateCH2Deck - " + err);
		}
		cb_result();
	})
}
exports.UpdateDeck = UpdateDeck;

// String.format 처리
if (!String.prototype.format) {
    String.prototype.format = function () {
        var args = arguments;
        return this.replace(/{(\d+)}/g, function (match, number) {
            return typeof args[number] != 'undefined'
        ? args[number]
        : match;
        });
    };
}

// 유저 정보 DB 데이터를 Json으로 변환합니다. dbnationidx가 undefined이면 nation 정보를 셋팅하지 않습니다.
function _ConvertUserInfoDBtoJson(dbdata, dbuseridx, dbnationdata) {
    var userinfo_json = {
        'UniqueId' : Number(dbdata[dbuseridx]["UniqueId"]),
        'UserName' : dbdata[dbuseridx]["UserName"],
        'Cash' : dbdata[dbuseridx]["Cash"],
        'Gold' : dbdata[dbuseridx]["Gold"],
        'GuildId' : Number(dbdata[dbuseridx]["GuildId"]),
        'Win' : Number(dbdata[dbuseridx]["Win"]),
        'Lose' : Number(dbdata[dbuseridx]["Lose"]),
        'WinStreak' : Number(dbdata[dbuseridx]["WinStreak"]),
        'LoseStreak' : Number(dbdata[dbuseridx]["LoseStreak"]),
        'Rating' : Number(dbdata[dbuseridx]["Rating"]),
        'Area' : Number(dbdata[dbuseridx]["Area"]),
        'UseDeckId' : Number(dbdata[dbuseridx]["UseDeckId"]),
        'UseDeckNation' : Number(dbdata[dbuseridx]["UseDeckNation"]),
        'ClearTutorial' : Number(dbdata[dbuseridx]["ClearTutorial"]),
        'LoginTime' : dbdata[dbuseridx]["LoginTime"].toISOString(), // UTC Time
        'IsResetShop' : Number(dbdata[dbuseridx]["ResetShop"])
    }
    if (dbnationdata != undefined) {
        userinfo_json.NationInfo = {};
        for (var ination = 0; ination < dbnationdata.length; ination++) {
            var nationinfo ={
                'Nation' : Number(dbnationdata[ination]["Nation"]),
                'NationLevel' : Number(dbnationdata[ination]["NationLevel"]),
                'NationExp' : Number(dbnationdata[ination]["NationExp"]),
                'Area' : Number(dbnationdata[ination]["Area"]),
                'Rating' : Number(dbnationdata[ination]["Rating"])
            }
            userinfo_json.NationInfo[nationinfo.Nation] = nationinfo;
        }
    }
    //'NationExps' : [],
    //    'NationLevels' : [],

    return userinfo_json;
}
// 지휘관 아이템 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertCommanderItemDBtoJson(dbdata, uniqueId) {
    var commander_item_count = dbdata.length;
    var user_commander_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(commander_item_count),
        'Items' : []
    };
    for (var iItem = 0; iItem < commander_item_count; iItem++) {
        var user_commander_item = {
            'ItemId' : Number(dbdata[iItem]["ItemId"]),
            'ItemNo' : Number(dbdata[iItem]["ItemNo"]),
            'ItemRank' : Number(dbdata[iItem]["ItemRank"]),
            'ItemExp' : Number(dbdata[iItem]["ItemExp"]),
            'ItemLevel' : Number(dbdata[iItem]["ItemLevel"]),
            'ItemCount' : Number(dbdata[iItem]["ItemCount"]),
            'SlotId' : Number(dbdata[iItem]["SlotId"])
        };
        user_commander_json.Items.push(user_commander_item);
    }
    return user_commander_json;
}
// 지휘관 덱 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertDeckDBtoJson(dbdata, uniqueId) {
    var deck_count = dbdata.length;
    var deckinfo_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(deck_count),
        'Decks' : []
    }
    for (var iDeck = 0; iDeck < deck_count; iDeck++) {
        var deck = {
            'DeckId' : Number(dbdata[iDeck]["DeckId"]),
            'DeckNation' : Number(dbdata[iDeck]["DeckNation"]),
            'DeckName' : String(dbdata[iDeck]["DeckName"]),
            'ItemIds' : [],
            'Active' : Number(1)
        }
        var itemIdString = dbdata[iDeck]["ItemIds"];
        var itemIdArray = itemIdString.split(',', gameconfig.max_deck_commander_count);
        for (var iItem = 0; iItem < gameconfig.max_deck_commander_count; iItem++) {
            deck.ItemIds.push(Number(itemIdArray[iItem]));
        }
        deckinfo_json.Decks.push(deck);
    }
    return deckinfo_json;
}
// Shop Commander Item에 대한 정보를 Json으로 변환합니다.
function _ConvertShopCommanderItemList(dbdata) {
    if (dbdata[0] == null)
        return null;
    var result_shop_commander = {
        'ResetTime' : dbdata[0]["ResetTime"].toISOString(),
        'CommanderList' : []
    };
    for (var icommander = 0; icommander < gameconfig.max_shop_commander_count; icommander++) {
        var commanderidcol = 'Commander' + icommander;
        var countcol = 'Commander' + icommander + 'Count';
        var result_commander_info = {
            'Index' : Number(icommander),
            'CommanderNo' : Number(dbdata[0][commanderidcol]),
            'Count' : Number(dbdata[0][countcol]),
            'Price' : Number(0),
            'RemainCount' : Number(0)
        }
        if (result_commander_info.CommanderNo >= 0) {
            var commanderitem = commandertable.GetCommanderTable(result_commander_info.CommanderNo)
            var pricedata = shop.GetShopCommanderPrice(commanderitem.rank, result_commander_info.Count + 1);
            if (pricedata == null)
                result_commander_info.Price = 0;
            else
                result_commander_info.Price = Number(pricedata.Cost);
            result_commander_info.RemainCount = gameconfig.max_shop_buy_commander[commanderitem.rank] - result_commander_info.Count;
        }

        result_shop_commander.CommanderList.push(result_commander_info);
    }
    return result_shop_commander;
}
