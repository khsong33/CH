﻿var redisClient = require('./redis_store');
var mssql = require('./mssql');
var item = require('./item');
var crypto = require('crypto');
var shop = require('./shop');
var gameconfig = require('./gameconfig');
// 랜덤
var random = require('./random');
var logger = require('./logger');
var log = new logger();

var battlereward = require('./battlereward');
var cachedata = require('./cachedata');
var tutorialtable = require('./table/tutorialtable');
var commandertable = require('./table/commandertable');
var commanderleveltable = require('./table/commanderleveltable');
var nationleveltable = require('./table/nationleveltable');
var chesttable = require('./table/randomchesttable');
var areatable = require('./table/commanderareatable');
var rewardchesttable = require('./table/rewardchesttable');
var areapointtable = require('./table/areapointtable');
var errorcode = require('./servererror');


// 접속 해지 된 유저 임시 저장
var discon_user_socketid = {};

// 초기화
cachedata.Init();
random.InitSyncRandom();
tutorialtable.Load();
commandertable.Load();
commanderleveltable.Load();
areatable.Load(); // 항상 commandertable 이후에 로딩되어야함
nationleveltable.Load();
chesttable.Load();
rewardchesttable.Load();
areapointtable.Load();

// 유저 접속 해제
function OnDisconnected(io, socket) {
	// 대기방 해제    
	OnWaitUnregiste(io, socket, null);
	
	var find_user_info_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_info_key, function (err, userinfo) {
		if (userinfo != null) {
			var userinfoJson = JSON.parse(userinfo);
			cachedata.UpdateDBUserinfo(userinfoJson.UniqueId, userinfoJson.UseDeckId, userinfoJson.UseDeckNation, userinfoJson.ClearTutorial);
            // log.info('Disconnected user Cache user data - ' + userinfoJson.UniqueId);
		}
	});
	// 유저 정보는 시간이 지난 후 해제
	discon_user_socketid[socket.id] = setTimeout(function () {
		_deleteUserInfo(socket.id);
	}, 1000);
}
exports.OnDisconnected = OnDisconnected;
// 유저 접속 해제 이후 DB 데이터 삭제
function _deleteUserInfo(user_key) {
	var find_user_userkey = 'user:' + user_key;
	redisClient.store_redis.get(find_user_userkey, function (err, userInfo) {
		if (userInfo != null) {
			var userInfoJson = JSON.parse(userInfo);
			
			var del_list = [];
			var del_userinfo_key = 'user:' + userInfoJson.UserKey;
			var del_commander_key = 'uidcommander:' + userInfoJson.UniqueId;
			var del_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
			var del_item_key = 'uiditem:' + userInfoJson.UniqueId;
			var del_shop_commander = 'shopcommander:' + userInfoJson.UniqueId;
			var del_chest_key = 'chest:' + userInfoJson.UniqueId;
			
			del_list.push(del_userinfo_key);
			del_list.push(del_commander_key);
			del_list.push(del_deck_key);
			del_list.push(del_item_key);
			del_list.push(del_shop_commander);
			del_list.push(del_chest_key);
			
			redisClient.store_redis.del(del_list);
		}
	});
	delete discon_user_socketid[user_key];
}
// 유저 로그인 및 생성
function OnLogin(io, socket, data) {
	if (data.UserKey != null && discon_user_socketid.hasOwnProperty(data.UserKey)) {
		log.info("Exist User info Login - " + data.Name);
		clearTimeout(discon_user_socketid[data.UserKey]);
		
		delete discon_user_socketid[data.UserKey];
		
		var find_user_key = 'user:' + data.UserKey;
		redisClient.store_redis.get(find_user_key, function (err, userinfo) {
			var userinfoJson = JSON.parse(userinfo);
			var del_old_userinfo_key = 'user:' + userinfoJson.UserKey;
			redisClient.store_redis.del(del_old_userinfo_key);
			var set_new_userinfo_key = 'user:' + socket.id;
			userinfoJson.UserKey = socket.id;
			redisClient.store_redis.set(set_new_userinfo_key, JSON.stringify(userinfoJson), function (err) {
				socket.emit('sc_login', userinfoJson);
			});
			_ConvertCommanderAllJson(userinfoJson.UniqueId, function (commanderinfoJson) {
				socket.emit('sc_login_commanderitem', commanderinfoJson);
			});
			_ConvertDeckAllJson(userinfoJson.UniqueId, function (deckinfoJson) {
				socket.emit('sc_login_deck', deckinfoJson);
			});
		});
	}
	else {
		log.info("Not Exist User info Login - " + data.Name);
		mssql.GetLoginInfo(data.Name, function (userinfoJson, usercommanderItemJson, userdeckJson, shopcommanderJson) {
			_postLoginSet(io, socket, userinfoJson);
			socket.emit('sc_login', userinfoJson);
			_postLoginSetCommander(io, socket, userinfoJson.UniqueId, usercommanderItemJson);
			socket.emit('sc_login_commanderitem', usercommanderItemJson);
			_postLoginSetDeck(io, socket, userinfoJson.UniqueId, userdeckJson);
			socket.emit('sc_login_deck', userdeckJson);
			if (shopcommanderJson != null) {
				var set_shop_commander = 'shopcommander:' + userinfoJson.UniqueId;
				redisClient.store_redis.set(set_shop_commander, JSON.stringify(shopcommanderJson));
			}
		});
	}

}
exports.OnLogin = OnLogin;
// 로그인 재시도
function OnRetryLogin(io, socket, data) {
	log.info("Retry Login - " + data.Name);
	mssql.GetLoginInfo(data.Name, function (userinfoJson, usercommanderItemJson, userdeckJson) {
		_postLoginSet(io, socket, userinfoJson);
		socket.emit('sc_login', userinfoJson);
		_postLoginSetCommander(io, socket, userinfoJson.UniqueId, usercommanderItemJson);
		socket.emit('sc_login_commanderitem', usercommanderItemJson);
		_postLoginSetDeck(io, socket, userinfoJson.UniqueId, userdeckJson);
		socket.emit('sc_login_deck', userdeckJson);
	});
}
exports.OnRetryLogin = OnRetryLogin;
// 재접속
function OnReconnect(io, socket, data) {
	if (discon_user_socketid.hasOwnProperty(data.UserKey)) {
		clearTimeout(discon_user_socketid);
	}
	var find_old_user = 'user:' + data.UserKey;
	var rename_new_user = 'user:' + socket.id;
	redisClient.store_redis.rename(find_old_user, rename_new_user, function (err) {
		var reconnect_info = {
			'Success' : '',
			'UserKey' : String(socket.id)
		}
		if (err) {
			reconnect_info.Success = 'FALSE';
			socket.emit('sc_reconnect', reconnect_info);
		}
		else {
			reconnect_info.Success = 'TRUE';
			socket.emit('sc_reconnect', reconnect_info);
		}
	});
	delete discon_user_socketid[data.UserKey];
}
exports.OnReconnect = OnReconnect;
// 대기 등록
function OnWaitRegiste(io, socket, data) {
	var find_user = 'user:' + socket.id;
	redisClient.store_redis.get(find_user, function (err, userInfo) {
		var userInfoJson = JSON.parse(userInfo);
		var deckInfo = {
			'Items' : []
		};
		var user_use_deckId = Number(data.DeckId);
		
		var find_commanderitem_key = 'uidcommander:' + userInfoJson.UniqueId;
		var current_item_idx = 0;
		var empty_item_count = 0;
		
		var find_user_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
		redisClient.store_redis.hmget(find_user_deck_key, user_use_deckId, function (err, deckData) {
			var deckDataJson = JSON.parse(deckData);
			for (var iItem = 0; iItem < deckDataJson.ItemIds.length; iItem++) {
				var itemId = Number(deckDataJson.ItemIds[iItem]);
				redisClient.store_redis.hmget(find_commanderitem_key, itemId, function (err, itemData) {
					var itemDataJson = JSON.parse(itemData);
					var user_commander_item = {
						'ItemId' : Number(itemDataJson.ItemId),
						'ItemNo' : Number(itemDataJson.ItemNo),
						'ItemRank' : Number(itemDataJson.ItemRank),
						'ItemExp' : Number(itemDataJson.ItemExp),
						'SlotId' : Number(current_item_idx),
						'ItemLevel' : Number(itemDataJson.ItemLevel),
						'ItemCount' : Number(itemDataJson.ItemCount)
                       
					};
					if (itemDataJson.ItemId >= 0) {
						deckInfo.Items.push(user_commander_item);
					}
					else {
						empty_item_count++;
					}
					current_item_idx++;
					if (current_item_idx >= deckDataJson.ItemIds.length) {
						deckInfo.Count = Number(current_item_idx) - Number(empty_item_count);
						userInfoJson.Decks = deckInfo;
						
						if (userInfoJson.UseDeckId != deckDataJson.DeckId) {
							userInfoJson.UseDeckId = deckDataJson.DeckId;
						}
						if (userInfoJson.UseDeckNation != deckDataJson.DeckNation) {
							userInfoJson.UseDeckNation = deckDataJson.DeckNation;
						}
						
						var find_set_userinfo = 'user:' + userInfoJson.UserKey;
						redisClient.store_redis.set(find_set_userinfo, JSON.stringify(userInfoJson), function (err) {
							redisClient.match_sub.subscribe('roomstart:' + socket.id);
							redisClient.match_redis.publish('registe_pvp', JSON.stringify(userInfoJson));
							socket.emit('sc_wait_registe');
						}); // end redis set userinfo what userkey
					}
				}); // end hmget
			}
		});
	}); // end redis get user info
}
exports.OnWaitRegiste = OnWaitRegiste;
// 대기방 시작
function OnRoomStart(io, socketid, roomkey) {
	log.info('On Room Start - ' + roomkey);
	_GetBattleServ(function (battle_serv) {
		redisClient.match_redis.get('room:' + roomkey, function (err, roomdata) {
			var battle_room_json = {
				'BattleServ' : String(battle_serv),
				'MatchInfo' : JSON.parse(roomdata)
			};
			//roomdataJson.RoomKey = String(roomkey); // Room Key는 보내준 정보로 유닛이 생성
			// 배틀 서버 주소도 이곳에 저장
			io.sockets.connected[socketid].emit('sc_start_room', battle_room_json);
		});
	});
}
exports.OnRoomStart = OnRoomStart;
// 대기방 해제
function OnWaitUnregiste(io, socket, data) {
	redisClient.match_sub.unsubscribe('roomstart:' + socket.id);
	redisClient.match_redis.publish('unregiste_pvp', socket.id);
	socket.emit('sc_wait_unregiste');
}
exports.OnWaitUnregiste = OnWaitUnregiste;
// 지휘관 덱 업데이트
function OnUpdateCommandItemDeck(io, socket, data) {
	var userKey = 'user:' + socket.id;
	var update_item_count = data.length;
	redisClient.store_redis.get(userKey, function (err, userInfo) {
		var userInfoJson = JSON.parse(userInfo);
		
		var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
		var current_deck = 0;
		var total_update_deck = data.length;
		for (var iDeck = 0; iDeck < total_update_deck; iDeck++) {
			var deck_id = data[iDeck].DeckId;
			redisClient.store_redis.hmget(find_commander_deck_key, deck_id, function (err, deckData) {
				var deckDataJson = JSON.parse(deckData);
				deckDataJson.DeckNation = data[current_deck].DeckNation;
				deckDataJson.DeckName = data[current_deck].DeckName;
				deckDataJson.ItemIds = data[current_deck].ItemIds;
				
				redisClient.store_redis.hmset(find_commander_deck_key, deck_id, JSON.stringify(deckDataJson));
				cachedata.UpdateDBDeck(userInfoJson.UniqueId, deckDataJson.DeckId, deckDataJson);
				current_deck++;
				if (current_deck >= total_update_deck) {
					socket.emit('sc_update_commanditemdeck');
				}
			});
		}
	});
}
exports.OnUpdateCommandItemDeck = OnUpdateCommandItemDeck;
// 지휘관 덱 추가
function OnAddCommanderItemDeck(io, socket, data) {
	var userKey = 'user:' + socket.id;
	var add_item_count = data.length;
	redisClient.store_redis.get(userKey, function (err, userInfo) {
		var userInfoJson = JSON.parse(userInfo);
		
		var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
		var total_add_deck = data.length;
		var current_add_deck = 0;
		redisClient.store_redis.hkeys(find_commander_deck_key, function (err, keys) {
			var maxId = 0;
			for (var ikey = 0; ikey < keys.length; ikey++) {
				if (Number(keys[ikey]) > maxId)
					maxId = Number(keys[ikey]);
			}
			var deckId = maxId + 1;
			var deckDataJson = {
				'DeckId' : Number(deckId),
				'DeckNation' : data[current_add_deck].DeckNation,
				'DeckName' : data[current_add_deck].DeckName,
				'ItemIds' : data[current_add_deck].ItemIds,
				'Active' : Number(2)
			}
			cachedata.UpdateDBDeck(userInfoJson.UniqueId, deckDataJson.DeckId, deckDataJson);
			redisClient.store_redis.hmset(find_commander_deck_key, deckId, JSON.stringify(deckDataJson), function (err) {
				socket.emit('sc_add_commanditemdeck', { 'NewDeckId' : Number(deckId) });
			})
		});
	});
}
exports.OnAddCommanderItemDeck = OnAddCommanderItemDeck;
// 지휘관 덱 삭제
function OnRemoveCommanderItemDeck(io, socket, data) {
	var userKey = 'user:' + socket.id;
	var remove_deck_id = data.DeckId;
	redisClient.store_redis.get(userKey, function (err, userInfo) {
		var userInfoJson = JSON.parse(userInfo);
		
		var find_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
		redisClient.store_redis.hmget(find_commander_deck_key, remove_deck_id, function (err, deckdata) {
			if (err)
				log.error("failed remove deck - " + err);
			var deckdataJson = JSON.parse(deckdata);
			deckdataJson.Active = Number(3);
			
			cachedata.UpdateDBDeck(userInfoJson.UniqueId, deckdataJson.DeckId, deckdataJson);
			
			var set_commander_deck_key = 'uiddeck:' + userInfoJson.UniqueId;
			redisClient.store_redis.hmset(set_commander_deck_key, remove_deck_id, JSON.stringify(deckdataJson), function (err) {
				socket.emit('sc_remove_commanditemdeck', { 'DeckId' : Number(remove_deck_id) });
			});
		});
	});
}
exports.OnRemoveCommanderItemDeck = OnRemoveCommanderItemDeck;
// 전투 보상 처리
function OnBattleReward(io, socket, data) {
	var find_user_info_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_info_key, function (err, userinfo) {
		var userinfoJson = JSON.parse(userinfo);
		var find_battle_reward_key = 'battlereward:' + userinfoJson.UniqueId;
		redisClient.match_redis.get(find_battle_reward_key, function (err, battle_reward) {
			var battle_reward_json = JSON.parse(battle_reward);
			battlereward.UpdatePvPBattleReward(io, socket, battle_reward_json, function (result_battle_reward) {
				socket.emit('sc_battle_reward', result_battle_reward);
			});
		});
		redisClient.match_redis.del(find_battle_reward_key); // 얻은 보상은 삭제합니다.
	});
}
exports.OnBattleReward = OnBattleReward;
// 개발용 보상 저장. 실제로는 전투서버에서 match redis로 저장
function OnDevBattleReward(io, socket, data) {
	var find_user_info_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_info_key, function (err, userinfo) {
		var userinfoJson = JSON.parse(userinfo);
		redisClient.match_redis.set('battlereward:' + userinfoJson.UniqueId, JSON.stringify(data), function (err) {
			socket.emit('sc_dev_battle_reward');
		});
	});
}
exports.OnDevBattleReward = OnDevBattleReward;
// 튜토리얼 시작
function OnTutorialStart(io, socket, data) {
	var find_user_info_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_info_key, function (err, userinfo) {
		var userinfoJson = JSON.parse(userinfo);
		var tutorial_matchinfo = tutorialtable.CreateTutorialMatchInfo(userinfoJson, function (matchinfoJson) {
			_GetBattleServ(function (battle_serv) {
				var room_key = crypto.createHash('md5').update(JSON.stringify(matchinfoJson)).digest('hex');
				matchinfoJson.Room = String(room_key);
				var set_tutorial_room_key = 'room:' + room_key;
				var tutorial_room_json = {
					'BattleServ' : String(battle_serv),
					'MatchInfo' : matchinfoJson
				};
				redisClient.match_redis.set(set_tutorial_room_key, JSON.stringify(matchinfoJson), function (err) {
					socket.emit('sc_tutorial_start2', tutorial_room_json);
				});
			});
		});
	});
}
exports.OnTutorialStart = OnTutorialStart;
// 튜토리얼 종료
function OnTutorialEnd(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userInfoJson = JSON.parse(userinfo);
		userInfoJson.ClearTutorial += 1;
		var set_user_key = 'user:' + socket.id;
		redisClient.store_redis.set(set_user_key, JSON.stringify(userInfoJson));
		var tutorial_end_json = {
			'ClearIdx' : Number(userInfoJson.ClearTutorial)
		}
		socket.emit('sc_tutorial_end', tutorial_end_json);
	});
}
exports.OnTutorialEnd = OnTutorialEnd;
// 리플레이 리스트 받기
function OnReplayList(io, socket, data) {
	var userKey = 'user:' + socket.id;
	redisClient.store_redis.get(userKey, function (err, userInfo) {
		var userInfoJson = JSON.parse(userInfo);
		mssql.getLogkey(userInfoJson.UniqueId, 10, function (logKeyList) {
			socket.emit('sc_replaylist', logKeyList);
		}); // end mssql get log key
	});// end redis get 
    //mssql.getLogkey(
}
exports.OnReplayList = OnReplayList;
// 리플레이 데이터 받기(DB에서 바로 받기)
function OnReplayData(io, socket, data) {
	var log_key = data.LogKey;
	mssql.getReplayData(log_key, function (replay_data) {
		socket.emit('sc_replaydata', replay_data);
	});
}
exports.OnReplayData = OnReplayData;
// 지휘관 아이템 레벨업
function OnCommanderItemLevelUp(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		userinfojson = JSON.parse(userinfo);
		var find_item_key = 'uiditem:' + userinfojson.UniqueId;
		redisClient.store_redis.hmget(find_item_key, data.ItemNo, function (err, commanderitem) {
			var commanderitem_json = JSON.parse(commanderitem);
			var item_rank = commandertable.GetCommanderTable(commanderitem_json.ItemNo).rank;
			var item_nation = commandertable.GetCommanderTable(commanderitem_json.ItemNo).nation;
			var item_level = commanderitem_json.ItemLevel;
			var levelup_gold = commanderleveltable.GetCommanderLevelTable(item_rank, item_level).cost;
			var levelup_cash = 0;
			var levelup_needcard = commanderleveltable.GetCommanderLevelTable(item_rank, item_level).needcard;
			var max_level = commanderleveltable.GetCommanderMaxLevel(item_rank);
			userinfojson.NationInfo[item_nation].NationExp = userinfojson.NationInfo[item_nation].NationExp + commanderleveltable.GetCommanderLevelTable(item_rank, item_level).exp;
			// Nation level 업데이트            
			for (var nationlevel = userinfojson.NationInfo[item_nation].NationLevel; nationlevel < nationleveltable.GetMaxLevel(); nationlevel++) {
				var nation_level_data = nationleveltable.GetNationLevel(nationlevel);
				if (userinfojson.NationInfo[item_nation].NationExp >= nation_level_data.needexp && userinfojson.NationInfo[item_nation].NationExp < nation_level_data.maxexp) {
					userinfojson.NationInfo[item_nation].NationLevel = nationlevel;
					break;
				}
			}
			
			if (userinfojson.Gold < levelup_gold) {
				var lackcost = levelup_gold - userinfojson.Gold;
				levelup_gold = userinfojson.Gold;
				levelup_cash = Number(Math.floor(lackcost * gameconfig.change_ratio_cash));
				if (levelup_cash <= 1)
					levelup_cash = 1;
			}
			if (userinfojson.Cash < levelup_cash) {
				errorcode.send_error(socket, errorcode.error_code.NOT_ENOUGH_CASH);
				return;
			}
			// 레벨업이 가능한 경우
			if (commanderitem_json.ItemCount >= levelup_needcard && commanderitem_json.ItemLevel < max_level) {
				var nation_up_info = {
					"nation" : Number(item_nation),
					"nationlevel" : Number(userinfojson.NationInfo[item_nation].NationLevel),
					"nationexp" : Number(userinfojson.NationInfo[item_nation].NationExp)
				}
				mssql.UpdateCommanderItemLevel(userinfojson.UniqueId, commanderitem_json, levelup_needcard, levelup_gold, levelup_cash, nation_up_info, function (levelup_result) {
					userinfojson.Gold = levelup_result.Gold;
					userinfojson.Cash = levelup_result.Cash;
					commanderitem_json.ItemLevel = levelup_result.ItemLevel;
					commanderitem_json.ItemCount = levelup_result.ItemCount;
					
					var set_userinfo_key = 'user:' + userinfojson.UserKey;
					var set_commanderitem_key = 'uiditem:' + userinfojson.UniqueId;
					var set_commanderitem_key2 = 'uidcommander:' + userinfojson.UniqueId;
					
					redisClient.store_redis.set(set_userinfo_key, JSON.stringify(userinfojson));
					redisClient.store_redis.hmset(set_commanderitem_key, levelup_result.ItemNo, JSON.stringify(commanderitem_json));
					redisClient.store_redis.hmset(set_commanderitem_key2, levelup_result.ItemId, JSON.stringify(commanderitem_json));
					
					socket.emit('sc_commander_levelup', levelup_result);
				});
			}
		});
	});
}
exports.OnCommanderItemLevelUp = OnCommanderItemLevelUp;
// 상자 열기
function OnOpenChest(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfoJson = JSON.parse(userinfo);
		// data.ChestType과 data.Area는 서버에서 판단하도록 변경
		// 유저가 갖고 있는 ChestSlotId만 받아서 열어야 하는 구조. 유저가 갖고 있는 Chest의 경우 redis와 DB에 별도 저장
		item.OpenChest(userinfoJson.UniqueId, data.ChestType, userinfoJson.UseDeckNation, data.Area, 0, false, function (result_chest) {
			socket.emit('sc_open_chest', result_chest);
			item.UpdateResultChest(userinfoJSon, result_chest);
		});
	});
}
exports.OnOpenChest = OnOpenChest;
// Shop Item 전체를 획득
function OnShopList(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		shop.GetShopCommanderList(socket, data.Area, function (shopcommanderlist) {
			var shop_items = {
				'Chest' : shop.GetShopChestList(data.Area),
				'Goods' : shop.GetShopGoodsList(),
				'Commander' : shopcommanderlist
			};
			socket.emit('sc_shop_list', shop_items);
			var set_shop_info = 'shopcommander:' + userinfojson.UniqueId;
			redisClient.store_redis.set(set_shop_info, JSON.stringify(shopcommanderlist));
		});
	});
}
exports.OnShopList = OnShopList;
// Shop Chest을 구매합니다.
function OnShopChestBuy(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		var shop_info = shop.GetShopChest(userinfojson.Area, data.ChestType);
		if (shop_info.Price <= userinfojson.Cash) {
			item.OpenChest(userinfojson.UniqueId, data.ChestType, userinfojson.UseDeckNation, userinfojson.Area, shop_info.Price, false, function (result_chest) {
				socket.emit('sc_shop_chest_buy', result_chest);
				item.UpdateResultChest(userinfojson, result_chest);
			});
		}
		else {
			errorcode.send_error(socket, errorcode.error_code.NOT_ENOUGH_CASH);
			return;
		}
	});
}
exports.OnShopChestBuy = OnShopChestBuy;
// Shop 골드를 구매합니다.
function OnShopGoldBuy(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		var shop_goods_info = shop.GetShopGoods(data.GoodsNo);
		if (shop_goods_info.PriceGem > userinfojson.Cash) {
			log.error("Not engough gem - " + userinfojson.UniqueId);
			errorcode.send_error(socket, errorcode.error_code.NOT_ENOUGH_CASH);
			return;
		}
		else {
			mssql.ShopBuyGold(userinfojson.UniqueId, shop_goods_info.PriceGem, shop_goods_info.Count, function (result_shop_gold) {
				socket.emit('sc_shop_gold_buy', result_shop_gold);
				// redis updated                
				userinfojson.Cash = result_shop_gold.Cash;
				userinfojson.Gold = result_shop_gold.Gold;
				var update_user_key = 'user:' + userinfojson.UserKey;
				redisClient.store_redis.set(update_user_key, JSON.stringify(userinfojson));
			});
		}
	});
}
exports.OnShopGoldBuy = OnShopGoldBuy;
// Shop Cash(Gem)을 구매합니다.
function OnShopCashBuy(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		var shop_goods_info = shop.GetShopGoods(data.GoodsNo);
		if (shop_goods_info.Type != gameconfig.goodsinfo.Gem) {
			log.error("Just buy gem - " + userinfojson.UniqueId);
			return;
		}
		mssql.ShopBuyCash(userinfojson.UniqueId, shop_goods_info.Count, function (result_shop_cash) {
			socket.emit('sc_shop_buy_cash', result_shop_cash);
			// redis updated
			userinfojson.Cash = result_shop_cash.Cash;
			var update_user_key = 'user:' + userinfojson.UserKey;
			redisClient.store_redis.set(update_user_key, JSON.stringify(userinfojson));
		});
	});
}
exports.OnShopCashBuy = OnShopCashBuy;
// Shop 지휘관을 구매합니다.
function OnShopCommanderBuy(io, socket, data) {
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		var find_shop_commander = 'shopcommander:' + userinfojson.UniqueId;
		redisClient.store_redis.get(find_shop_commander, function (err, shopinfo) {
			var shopinfojson = JSON.parse(shopinfo);
			var commanderitemno = shopinfojson.CommanderList[data.ShopIndex].CommanderNo;
			var current_buy_count = shopinfojson.CommanderList[data.ShopIndex].Count;
			var commanderiteminfo = commandertable.GetCommanderTable(commanderitemno);
			var max_count = gameconfig.max_shop_buy_commander[commanderiteminfo.rank]
			var costinfo = shop.GetShopCommanderPrice(commanderiteminfo.rank, current_buy_count + 1);
			var costgold = costinfo.Cost;
			var costcash = 0;
			if (max_count <= current_buy_count) {
				return;
			}
			if (costinfo == null) {
				return;
			}
			if (userinfojson.Gold < costgold) {
				var lackcost = costinfo.Cost - userinfojson.Gold;
				costgold = userinfojson.Gold;
				costcash = Number(Math.floor(lackcost * gameconfig.change_ratio_cash));
				if (costcash <= 1)
					costcash = 1;
			}
			if (userinfojson.Cash < costcash) {
				errorcode.send_error(socket, errorcode.error_code.NOT_ENOUGH_CASH);
				return;
			}
			mssql.UpdateShopCommander(userinfojson.UniqueId, data.ShopIndex, costgold, costcash, function (result_shop_commander) {
				shopinfojson.CommanderList[result_shop_commander.ShopIndex].Count = result_shop_commander.ShopCount;
				// 보유 최대 갯수 도달 시 처리
				var max_have_item_count = commanderleveltable.GetMaxCommanderItemCount(result_shop_commander.ItemLevel, commanderiteminfo.rank)
				var isMaxHave = false;
				if (result_shop_commander.ItemCount >= max_have_item_count) {
					shopinfojson.CommanderList[result_shop_commander.ShopIndex].RemainCount = 0;
					isMaxHave = true;
				}
				else {
					shopinfojson.CommanderList[result_shop_commander.ShopIndex].RemainCount--;
				}
				userinfojson.Gold = result_shop_commander.Gold;
				userinfojson.Cash = result_shop_commander.Cash;
				var result_costinfo = shop.GetShopCommanderPrice(commanderiteminfo.rank, result_shop_commander.ShopCount + 1);
				if (result_costinfo == null) {
					result_shop_commander.Price = 0;
				}
				else if (isMaxHave) {
					result_shop_commander.Price = 0;
				}
				else {
					result_shop_commander.Price = shop.GetShopCommanderPrice(commanderiteminfo.rank, result_shop_commander.ShopCount + 1).Cost;
				}
				
				var set_user_info = 'user:' + userinfojson.UserKey;
				redisClient.store_redis.set(set_user_info, JSON.stringify(userinfojson));
				var set_shop_info = 'shopcommander:' + userinfojson.UniqueId;
				redisClient.store_redis.set(set_shop_info, JSON.stringify(shopinfojson));
				
				item.UpdateShopCommander(userinfojson.UniqueId, result_shop_commander.ItemId, result_shop_commander.ItemNo, result_shop_commander.ItemCount);
				
				result_shop_commander.RemainCount = shopinfojson.CommanderList[result_shop_commander.ShopIndex].RemainCount;
				socket.emit('sc_shop_commander_buy', result_shop_commander);
			});
		});
	});
}
exports.OnShopCommanderBuy = OnShopCommanderBuy;

function OnUpdateDeck(io, socket, data) {
	
	var find_user_key = 'user:' + socket.id;
	redisClient.store_redis.get(find_user_key, function (err, userinfo) {
		var userinfojson = JSON.parse(userinfo);
		var find_deck_user = 'uiddeck:' + userinfojson.UniqueId;
		var update_deck_id_list = [];
		for (var ideck = 0; ideck < data.DeckInfo.length; ideck++) {
			update_deck_id_list.push(data.DeckInfo[ideck].DeckId);
		}
		
		// db 전송전 변경 데이터 검사 (이전에 클라이언트에서 선 검사 후 전송)
		redisClient.store_redis.hmget(find_deck_user, update_deck_id_list, function (err, deckinfolist) {
			var db_update_query_data = '';
			if (deckinfolist != undefined) {
				for (var ideck = 0; ideck < deckinfolist.length; ideck++) {
					var deckinfojson = JSON.parse(deckinfolist[ideck]);
					var is_update_deck = false;
					var db_update_query_itemids = '';
					for (var iitem = 0; iitem < deckinfojson.ItemIds.length; iitem++) {
						if (data.DeckInfo[ideck].ItemIds[iitem] != deckinfojson.ItemIds[iitem]) {
							is_update_deck = true;
						}
						db_update_query_itemids += data.DeckInfo[ideck].ItemIds[iitem] + ",";
					}
					if (is_update_deck) {
						db_update_query_data += data.DeckInfo[ideck].DeckId + '^' + db_update_query_itemids + '|';
						deckinfojson.ItemIds = data.DeckInfo[ideck].ItemIds;
						var set_deck_key = 'uiddeck:' + userinfojson.UniqueId;
						redisClient.store_redis.hmset(set_deck_key, deckinfojson.DeckId, JSON.stringify(deckinfojson));
					}
				}
			}
			if (db_update_query_data.length || userinfojson.UseDeckId != data.UseDeckId) {
				userinfojson.UseDeckId = data.UseDeckId;
				var set_userinfo_key = 'user:' + socket.id;
				redisClient.store_redis.set(set_userinfo_key, JSON.stringify(userinfojson));
				mssql.UpdateDeck(userinfojson.UniqueId, userinfojson.UseDeckNation, db_update_query_data, data.UseDeckId, function () {
					socket.emit('sc_update_deck');
				});
			}
		});
	});
}
exports.OnUpdateDeck = OnUpdateDeck;

// 로그인 이후 redis를 셋팅합니다.
function _postLoginSet(io, socket, userinfoJson, cb_userinfo) {
	var userKey = 'user:' + String(socket.id);
	userinfoJson.UserKey = String(socket.id);
	
	var userinfo = JSON.stringify(userinfoJson);
	redisClient.store_redis.set(userKey, userinfo);
}
// 아이템 정보를 발송합니다.
function _postLoginSetCommander(io, socket, uniqueId, usercommanderJson, cb_commanderitem) {
	// 커맨더 아이템 저장 처리
	var userCommanderUidKey = 'uidcommander:' + uniqueId;
	var user_commanderitem_key = 'uiditem:' + uniqueId;
	// 빈 아이템 표시를 위해 빈 아이템 하나를 만듭니다.
	var empty_commander_item = {
		'ItemId' : -1,
		'ItemNo' : -1,
		'ItemRank' : 0,
		'ItemExp' : 0,
		'ItemLevel' : 0,
		'ItemCount' : 0
	};
	redisClient.store_redis.hmset(userCommanderUidKey, -1, JSON.stringify(empty_commander_item));
	redisClient.store_redis.hmset(user_commanderitem_key, -1, JSON.stringify(empty_commander_item));
	
	for (var iItem = 0; iItem < Number(usercommanderJson.Count); iItem++) {
		redisClient.store_redis.hmset(userCommanderUidKey, String(usercommanderJson.Items[iItem].ItemId), JSON.stringify(usercommanderJson.Items[iItem]));
		redisClient.store_redis.hmset(user_commanderitem_key, String(usercommanderJson.Items[iItem].ItemNo), JSON.stringify(usercommanderJson.Items[iItem]));
	}
}
// Deck 정보를 발송합니다.
function _postLoginSetDeck(io, socket, uniqueId, userdeckJson) {
	var user_deck_set_key = 'uiddeck:' + uniqueId;
	
	for (var iDeck = 0; iDeck < userdeckJson.Decks.length; iDeck++) {
		redisClient.store_redis.hmset(user_deck_set_key, userdeckJson.Decks[iDeck].DeckId, JSON.stringify(userdeckJson.Decks[iDeck]));
	}
}
// hash map으로 되어 있는 json commander item 정보를 클라이언트에 보내는 형태로 컨버팅
function _ConvertCommanderAllJson(uniqueId, cb_commanderallJson) {
	var find_commander_items_key = 'uidcommander:' + uniqueId;
	redisClient.store_redis.hkeys(find_commander_items_key, function (err, commanderitemlistkey) {
		var commander_item_count = commanderitemlistkey.length;
		var user_commander_json = {
			'UniqueId' : Number(uniqueId),
			'Count' : Number(commander_item_count) - 1,
			'Items' : []
		};
		for (var ikey = 0; ikey < commander_item_count; ikey++) {
			var find_commander_key = 'uidcommander:' + uniqueId;
			redisClient.store_redis.hmget(find_commander_key, commanderitemlistkey[ikey], function (err, commanderitem) {
				var commanderitemJson = JSON.parse(commanderitem);
				if (commanderitemJson.ItemId >= 0) {
					var user_commander_item = {
						'ItemId' : Number(commanderitemJson.ItemId),
						'ItemNo' : Number(commanderitemJson.ItemNo),
						'ItemRank' : Number(commanderitemJson.ItemRank),
						'ItemExp' : Number(commanderitemJson.ItemExp),
						'ItemCount' : Number(commanderitemJson.ItemCount),
						'SlotId' : Number(0)
					};
					user_commander_json.Items.push(user_commander_item);
					// 빈아이템을 제외한 나머지 아이템을 전송합니다.
					if (user_commander_json.Items.length == commander_item_count - 1) {
						cb_commanderallJson(user_commander_json);
					}
				}
			});
		}
	});
}
// hash map으로 되어 있는 json deck 정보를 클라이언트에 보내는 형태로 컨버팅
function _ConvertDeckAllJson(uniqueId, cb_deckallJson) {
	var find_deck_list_key = 'uiddeck:' + uniqueId;
	redisClient.store_redis.hkeys(find_deck_list_key, function (err, decklistkey) {
		var deck_count = decklistkey.length;
		var deckinfo_json = {
			'UniqueId' : Number(uniqueId),
			'Count' : Number(deck_count),
			'Decks' : []
		}
		for (var ideck = 0; ideck < deck_count; ideck++) {
			var current_active_deck = 0;
			var find_deck_key = 'uiddeck:' + uniqueId;
			redisClient.store_redis.hmget(find_deck_key, decklistkey[ideck], function (err, deckinfo) {
				var deckinfoJson = JSON.parse(deckinfo);
				current_active_deck++;
				if (deckinfoJson.Active != 3) { // 덱의 상태가 삭제가 아닌 경우
					var deck = {
						'DeckId' : Number(deckinfoJson.DeckId),
						'DeckNation' : Number(deckinfoJson.DeckNation),
						'DeckName' : String(deckinfoJson.DeckName),
						'ItemIds' : deckinfoJson.ItemIds,
						'Active' : Number(deckinfoJson.Active)
					}
					deckinfo_json.Decks.push(deck);
					if (current_active_deck == deck_count) {
						deckinfo_json.Count = deckinfo_json.Decks.length;
						cb_deckallJson(deckinfo_json);
					}
				}
			});
		}
	});
}
// Battle Server를 선택합니다.
function _GetBattleServ(cb_battle_serv) {
	var incr_battle_serv_idx_key = 'battleservidx'
	redisClient.match_redis.incr(incr_battle_serv_idx_key, function (err, battle_idx) {
		var find_battle_serv_key = 'battle_server';
		redisClient.match_redis.hkeys(find_battle_serv_key, function (err, battle_serv_list) {
			var select_idx = battle_idx % battle_serv_list.length;
			var select_battle_serv_key = 'battle_server';
			redisClient.match_redis.hmget(select_battle_serv_key, battle_serv_list[select_idx], function (err, battle_serv) {
				cb_battle_serv(battle_serv);
			});
		});
		if (battle_idx > 99999) {
			var set_battle_serv_idx_key = 'battleservidx'
			redisClient.match_redis.set(set_battle_serv_idx_key, Number(0));
		}
	});
}