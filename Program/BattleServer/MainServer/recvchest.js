﻿var redisClient = require('./redis_store');
var mssql = require('./mssql');
var item = require('./item');
var crypto = require('crypto');
var shop = require('./shop');
var gameconfig = require('./gameconfig');
// 랜덤
var random = require('./random');
var logger = require('./logger');
var log = new logger();

var battlereward = require('./battlereward');
var cachedata = require('./cachedata');
var tutorialtable = require('./table/tutorialtable');
var commandertable = require('./table/commandertable');
var commanderleveltable = require('./table/commanderleveltable');
var nationleveltable = require('./table/nationleveltable');
var chesttable = require('./table/randomchesttable');
var areatable = require('./table/commanderareatable');
var rewardchesttable = require('./table/rewardchesttable');
var areapointtable = require('./table/areapointtable');
var errorcode = require('./servererror');

// 상자 업데이트
function OnChestList(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        mssql.GetChestData(userinfojson.UniqueId, userinfojson.UseDeckNation, function (chestlist) {
            socket.emit('sc_chest_list', chestlist);
            var set_userchest_key = 'chest:' + userinfojson.UniqueId;
            redisClient.store_redis.set(set_userchest_key, JSON.stringify(chestlist));
        });
    });
    //mssql.GetChestData
}
exports.OnChestList = OnChestList;

function OnBattleReward(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_battle_reward_key = 'battlereward:' + userinfojson.UniqueId;
        redisClient.match_redis.get(find_battle_reward_key, function (err, rewardinfo) {
            var rewardinfojson = JSON.parse(rewardinfo);

            if (userinfojson.Rating < rewardinfojson.UserInfo.Rating) { // 승점 증가시
                var up_point_info = areapointtable.GetUpPointArea(rewardinfojson.UserInfo.Rating);
                rewardinfojson.UserInfo["Area"] = up_point_info.area;
            }
            else if (userinfojson.Rating > rewardinfojson.UserInfo.Rating) { // 감소시
                var down_point_info = areapointtable.GetDownPointArea(rewardinfojson.UserInfo.Rating);
                rewardinfojson.UserInfo["Area"] = down_point_info.area;
            }
            else { // 변경 없을 시
                rewardinfojson.UserInfo["Area"] = userinfojson.Area;
            }

            var find_chest_slot_key = 'chest:' + rewardinfojson.UniqueId;
            redisClient.store_redis.get(find_chest_slot_key, function (err, chestinfo) {
                var chestinfojson = JSON.parse(chestinfo);
                
                // 메달 처리            
                rewardinfojson.GainMedal = rewardinfojson.UserInfo.Medal;
                rewardinfojson.TotalMedal = chestinfojson.MCCount + rewardinfojson.UserInfo.Medal;
                if (rewardinfojson.TotalMedal > gameconfig.max_battle_medal_count) {
                    rewardinfojson.TotalMedal = gameconfig.max_battle_medal_count;
                }
                var battleendcode = rewardinfojson.UserInfo["BattleEndCode"]
                for (var islot = 0; islot < chestinfojson.NCInfo.length; islot++) {
                    if (chestinfojson.NCInfo[islot].Type == 0 && battleendcode == 0) {// 슬롯이 비어 있으면
                        var select_reward_chest = rewardchesttable.GetRewardChest(userinfojson.Area);
                        rewardinfojson.RewardChest = {
                            'Slot' : Number(islot), 
                            'Type' : Number(select_reward_chest.Type)
                        };
                        break;
                    }
                }
                // DB 업데이트
                mssql.UpdatePVPRewardChest(rewardinfojson, function (result_reward) {
                    result_reward.BattleEndCode = rewardinfojson.UserInfo.BattleEndCode;
                    socket.emit('sc_battle_reward', result_reward);

                    // redis 저장
                    userinfojson.Win = result_reward.Win;
                    userinfojson.WinStreak = result_reward.WinStreak;
                    userinfojson.Lose = result_reward.Lose;
                    userinfojson.LoseStreak = result_reward.LoseStreak;
                    userinfojson.Area = result_reward.Area;
                    userinfojson.Rating = result_reward.Rating;
                    var set_userinfo_key = 'user:' + userinfojson.UserKey;
                    redisClient.store_redis.set(set_userinfo_key, JSON.stringify(userinfojson));
                    // Chest 획득시만 처리
                    if (result_reward.ChestSlot >= 0) {
                        chestinfojson.NCInfo[Number(result_reward.ChestSlot)].Type = result_reward.ChestType;
                        chestinfojson.NCInfo[Number(result_reward.ChestSlot)].State = 1;
                    }
                    // 메달 처리
                    chestinfojson.MCCount = result_reward.TotalMedal;
                    var set_chset_key = 'chest:' + userinfojson.UniqueId;
                    redisClient.store_redis.set(set_chset_key, JSON.stringify(chestinfojson));
                });
            });
        });
    });
}
exports.OnBattleReward = OnBattleReward;
// 일반 보상 상자 활성화
function OnActiveNormalChest(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_chest_key = 'chest:' + userinfojson.UniqueId;
        redisClient.store_redis.get(find_chest_key, function (err, chestinfo) {
            var chestinfojson = JSON.parse(chestinfo);
            if (chestinfojson.NCInfo[data.SlotIdx].State != 1) {
                errorcode.send_error(socket, errorcode.error_code.FAILED_ACCESS_CHEST);
                return;
            }
            var chesttype = chestinfojson.NCInfo[data.SlotIdx].Type;
            var addsecond = gameconfig.GetNormalChestSecond(chesttype);
            mssql.UpdateNormalChest(userinfojson.UniqueId, userinfojson.UseDeckNation, data.SlotIdx, addsecond, function (result_chest) {
                socket.emit('sc_active_normal_chest', result_chest);
                chestinfojson.NCInfo[result_chest.SlotIdx].State = 2;
                chestinfojson.NCInfo[result_chest.SlotIdx].Time = result_chest.ChestTime;
                var set_chest_key = 'chest:' + userinfojson.UniqueId;
                redisClient.store_redis.set(set_chest_key, JSON.stringify(chestinfojson));
            });
        });
    });
}
exports.OnActiveNormalChest = OnActiveNormalChest;
// 일반 보상 상자 오픈
function OnOpenNormalChest(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_chest_key = 'chest:' + userinfojson.UniqueId;
        redisClient.store_redis.get(find_chest_key, function (err, chestinfo) {
            var chestinfojson = JSON.parse(chestinfo);
            var isopen_immediately = false;
            if (chestinfojson.NCInfo[data.SlotIdx].State == 1) { // 예외 UnActive 상태의 박스 열기
                isopen_immediately = true;
            }
            else if (chestinfojson.NCInfo[data.SlotIdx].State == 0) { // 없는 박스 열기시 에러
                errorcode.send_error(socket, errorcode.error_code.FAILED_ACCESS_CHEST);
                return;
            }
            mssql.UpdateOpenNormalChest(userinfojson.UniqueId, userinfojson.UseDeckNation, data.SlotIdx, function (result_open) {
                var open_price = 0;
                if (isopen_immediately) {
                    open_price = Math.floor(gameconfig.GetNormalChestSecond(result_open.ChestType) / 600);
                }
                else if (result_open.RemainSec > 0) {
                    open_price = Math.floor(result_open.RemainSec / 600);
                }
                if (userinfojson.Cash < open_price) {
                    errorcode.send_error(socket, errorcode.error_code.NOT_ENOUGH_CASH);
                    return;
                }
                item.OpenChest(userinfojson.UniqueId, result_open.ChestType, result_open.Nation, userinfojson.Area, open_price, false, function (result_chest) {
                    var result_normal_chest = { 'Chest' : result_chest, 'SlotIdx' : Number(result_open.SlotIdx) };
                    socket.emit('sc_open_normal_chest', result_normal_chest);
                    item.UpdateResultChest(userinfojson, result_chest);
                    chestinfojson.NCInfo[data.SlotIdx].State = 0;
                    chestinfojson.NCInfo[data.SlotIdx].Type = 0;
                    
                    var set_chest_key = 'chest:' + userinfojson.UniqueId;
                    redisClient.store_redis.set(set_chest_key, JSON.stringify(chestinfojson));
                });
            });
        });
    });
}
exports.OnOpenNormalChest = OnOpenNormalChest;
// 무료 상자 획득
function OnOpenFreeChest(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_chest_key = 'chest:' + userinfojson.UniqueId;
        redisClient.store_redis.get(find_chest_key, function (err, chestinfo) {
            var chestinfojson = JSON.parse(chestinfo);
            mssql.UpdateOpenFreeChest(userinfojson.UniqueId, userinfojson.UseDeckNation, function (result_freechest) {
                if (result_freechest.Success == false) {
                    errorcode.send_error(socket, errorcode.error_code.FAILED_FREE_CHEST);
                    return;
                }
                else {
                    item.OpenChest(userinfojson.UniqueId, gameconfig.chestinfo.Free, userinfojson.UseDeckNation, userinfojson.Area, 0, false, function (result_chest) {
                        var result_open_chest = {
                            'Time0' : result_freechest.Time0, 
                            'Time1': result_freechest.Time1,
                            'OpenChest' : result_chest
                        };
                        socket.emit('sc_open_free_chest', result_open_chest);
                        
                        item.UpdateResultChest(userinfojson, result_chest);
                        
                        chestinfojson.FCTime0 = result_freechest.Time0;
                        chestinfojson.FCTime1 = result_freechest.Time1;
                        var set_chest_key = 'chest:' + userinfojson.UniqueId;
                        redisClient.store_redis.set(set_chest_key, JSON.stringify(chestinfojson));
                    });
                }
            });
        });
    });
}
exports.OnOpenFreeChest = OnOpenFreeChest;
// 메달 상자 획득
function OnOpenMedalChest(io, socket, data) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_chest_key = 'chest:' + userinfojson.UniqueId;
        redisClient.store_redis.get(find_chest_key, function (err, chestinfo) {
            var chestinfojson = JSON.parse(chestinfo);
            if (chestinfojson.MCCount < gameconfig.max_battle_medal_count) {
                errorcode.send_error(socket, errorcode.error_code.FAILED_MEDAL_CHEST);
                return;
            }
            mssql.UpdateOpenMedalChest(userinfojson.UniqueId, userinfojson.UseDeckNation, function (result_medalchest) {
                item.OpenChest(userinfojson.UniqueId, gameconfig.chestinfo.Crown, userinfojson.UseDeckNation, userinfojson.Area, 0, false, function (result_chest) {
                    var result_open_chest = {
                        'MedalTime' : result_medalchest.Time,
                        'OpenChest' : result_chest
                    };
                    socket.emit('sc_open_medal_chest', result_open_chest);
                    
                    item.UpdateResultChest(userinfojson, result_chest);
                    
                    chestinfojson.MCCount = 0;
                    chestinfojson.MCTime = result_medalchest.Time;
                    var set_chest_key = 'chest:' + userinfojson.UniqueId;
                    redisClient.store_redis.set(set_chest_key, JSON.stringify(chestinfojson));
                });
            });
        });
    });
}
exports.OnOpenMedalChest = OnOpenMedalChest;