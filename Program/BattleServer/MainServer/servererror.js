﻿var error_code = {
    NOT_ENOUGH_GOLD : 1,
    NOT_ENOUGH_CASH : 2,
    FAILED_ACCESS_CHEST : 3, // 잘못된 보상상자 접근
    FAILED_FREE_CHEST : 4, // 무료 상자 개봉 시간이 아닌 경우 접근
    FAILED_MEDAL_CHEST : 5 // 메달 상자 개봉이 안되는 경우 접근
}
exports.error_code = error_code;

function send_error(socket, errorcode) {
    socket.emit('sc_server_error', { 'ErrorCode' : Number(errorcode) });
}
exports.send_error = send_error;