﻿var logger = require('./logger');
var redisClient = require('./redis_store');
var mssql = require('./mssql');
var gameconfig = require('./gameconfig');
var random = require('./random');
var areatable = require('./table/commanderareatable');

var shop_chest = {};
var shop_chest_list = {};
var shop_goods = {};
var shop_goods_list = [];
var shop_commander_price = {};
// DB를 통해서 Chest와 관련된 내용을 초기화합니다.
function InitShopChestDB() {
    mssql.GetShopChestInfo(function (result_shop_chest) {
        for (var ichest = 0; ichest < result_shop_chest.datas.length; ichest++) {
            var chest_data = result_shop_chest.datas[ichest]
            if (!shop_chest.hasOwnProperty(chest_data.Area)) {
                shop_chest[chest_data.Area] = {};
            }
            if (!shop_chest_list.hasOwnProperty(chest_data.Area)) {
                shop_chest_list[chest_data.Area] = new Array();
            }
            shop_chest[chest_data.Area][chest_data.Type] = chest_data;
            shop_chest_list[chest_data.Area].push(chest_data);
        }
    });
}
exports.InitShopChestDB = InitShopChestDB;
// 상점 박스 정보를 획득합니다.
function GetShopChest(area, type) {
    return shop_chest[area][type];
}
exports.GetShopChest = GetShopChest;
function GetShopChestList(area) {
    return shop_chest[area]
}
exports.GetShopChestList = GetShopChestList;
// 샵 재화 초기화
function InitShopGoodsInfo() {
    mssql.GetShopGoodsInfo(function (result_shop_goods) {
        for (var igoods = 0; igoods < result_shop_goods.datas.length; igoods++) {
            var goods_data = result_shop_goods.datas[igoods];
            shop_goods[goods_data.GoodsId] = goods_data;
            shop_goods_list.push(goods_data);
        }
    });
}
exports.InitShopGoodsInfo = InitShopGoodsInfo;
// 재화 전체 정보
function GetShopGoodsList() {
    return shop_goods_list;
}
exports.GetShopGoodsList = GetShopGoodsList;
// 특정 재화 정보
function GetShopGoods(goodsid) {
    return shop_goods[goodsid];
}
exports.GetShopGoods = GetShopGoods;
// 상점 지휘관 정보
function GetShopCommanderList(socket, area, cb_commanderlist) {
    var find_user_key = 'user:' + socket.id;
    redisClient.store_redis.get(find_user_key, function (err, userinfo) {
        var userinfojson = JSON.parse(userinfo);
        var find_shop_commander = 'shopcommander:' + userinfojson.UniqueId;
        redisClient.store_redis.get(find_shop_commander, function (err, shopcommander) {
            var shopcommanderjson = JSON.parse(shopcommander);
            
            if (userinfojson.IsResetShop == 0) { // 상점에 변화가 없는 경우
                cb_commanderlist(shopcommanderjson);
            }
            else { // 상점에 변화가 있는 경우
                var check_commander_list = [];
                if (shopcommanderjson != null) {
                    for (var ishopcommander = 0; ishopcommander < shopcommanderjson.CommanderList.length; ishopcommander++) {
                        check_commander_list.push(shopcommanderjson.CommanderList[ishopcommander].itemno);
                    }
                }
                
                var area_rank_item = { '1' : [], '2' : [], '3' : [] };
                for (var iarea = 0; iarea <= area; iarea++) {
                    var common_commander = areatable.GetAreaRankNationCommander(iarea, gameconfig.rankinfo.Common, userinfojson.UseDeckNation);
                    var rear_commander = areatable.GetAreaRankNationCommander(iarea, gameconfig.rankinfo.Rear, userinfojson.UseDeckNation);
                    var epic_commander = areatable.GetAreaRankNationCommander(iarea, gameconfig.rankinfo.Epic, userinfojson.UseDeckNation);
                    area_rank_item[gameconfig.rankinfo.Common] = area_rank_item[gameconfig.rankinfo.Common].concat(common_commander);
                    area_rank_item[gameconfig.rankinfo.Rear] = area_rank_item[gameconfig.rankinfo.Rear].concat(rear_commander);
                    area_rank_item[gameconfig.rankinfo.Epic] = area_rank_item[gameconfig.rankinfo.Epic].concat(epic_commander);
                }
                var randomindex = {
                    '1' : random.SyncRandomNotOverlap(0, area_rank_item[gameconfig.rankinfo.Common].length),
                    '2' : random.SyncRandomNotOverlap(0, area_rank_item[gameconfig.rankinfo.Rear].length),
                    '3' : random.SyncRandomNotOverlap(0, area_rank_item[gameconfig.rankinfo.Epic].length)
                };
                
                var result_select_commander_list = new Array();
                var max_shop_rank_count = gameconfig.today_max_shop_commander_count / gameconfig.rankinfo.Max;
                for (var irank = gameconfig.rankinfo.Common; irank <= gameconfig.rankinfo.Max; irank++) {
                    var item_count = 0;
                    for (var iitem = 0; iitem < max_shop_rank_count;) {
                        var select_item_no = area_rank_item[irank][randomindex[irank][item_count]].itemno;
                        // 이미 어제 받은 상점 지휘관 아이템이라면 업데이트 목록에서 제외합니다.
                        if (check_commander_list.indexOf(select_item_no) < 0) {
                            result_select_commander_list.push(select_item_no);
                            iitem++;
                        }
                        item_count++;
                    }
                }
                for (var iempty = result_select_commander_list.length; iempty < gameconfig.max_shop_commander_count; iempty++) {
                    result_select_commander_list.push(-1); // 빈 공간은 -1로 채웁니다. DB 에러 방지
                }
                mssql.GetShopCommanderInfo(userinfojson.UniqueId, result_select_commander_list, function (result_shop_commander_list) {
                    cb_commanderlist(result_shop_commander_list);
                });
            }
        });
    });
}
exports.GetShopCommanderList = GetShopCommanderList;
// 지휘관 상점 데이터 초기화
function InitShopCommanderPriceInfo() {
    mssql.GetShopCommanderPriceInfo(function (result_shop_commander_price) {
        for (var iprice = 0; iprice < result_shop_commander_price.length; iprice++) {
            var price_data = result_shop_commander_price[iprice]
            if (!shop_commander_price.hasOwnProperty(price_data.Rank)) {
                shop_commander_price[price_data.Rank] = {};
            }
            shop_commander_price[price_data.Rank][price_data.Count] = result_shop_commander_price[iprice];
        }
    });
}
exports.InitShopCommanderPriceInfo = InitShopCommanderPriceInfo;
// 상점 구매 가격 확인을 합니다.
function GetShopCommanderPrice(rank, count) {
    return shop_commander_price[rank][count];
}
exports.GetShopCommanderPrice = GetShopCommanderPrice;