﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var serverconfig = require('../serverconfig').getServerConfig();

var area_point;
var area_point_up;
var area_point_down;
var area_range_list;
// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerAreaTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    area_point = {};
    area_range_list = [];
    for (var row in result) {
        var info = new Object;
        info.area = Number(result[row]['Area']);
        info.uppoint = Number(result[row]['UpPoint']);
        info.downpoint = Number(result[row]['DownPoint']);
        
        area_point[info.area] = info;
        area_range_list.push(info);
    }
}
exports.Load = Load;
// 지역 점수 정보
function GetAreaPoint(area) {
    return area_point[area];
}
exports.GetAreaPoint = GetAreaPoint;
function GetUpPointArea(rating) {
    var result_rating = area_range_list[0];
    for (var irating = 0; irating < area_range_list.length; irating++) {
        if (rating >= area_range_list[irating].uppoint) {
            result_rating = area_range_list[irating];
        }
    }
    return result_rating;
}
exports.GetUpPointArea = GetUpPointArea;
function GetDownPointArea(rating) {
    var result_rating = area_range_list[area_range_list.length - 1];
    for (var irating = area_range_list.length - 1; irating >= 0; irating--) {
        if (rating <= area_range_list[irating].downpoint) {
            result_rating = area_range_list[irating];
        }
    }
    return result_rating;
}
exports.GetDownPointArea = GetDownPointArea;

