﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var gameconfig = require('../gameconfig');
var commandertable = require('./commandertable');

var area_nation_list_table;
var rank_nation_list_table;
var area_rank_nation_list_table;

// 테이블 로드
// CommanderTable보다 항상 나중에 로드 되어야 함
function Load() {
    var data = fs.readFileSync('./data/ServerCommanderAreaTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    area_nation_list_table = {};
    rank_nation_list_table = {};
    area_rank_nation_list_table = {};
    // 초기화
    for (var iarea = 0; iarea <= gameconfig.max_area; iarea++) {
        area_nation_list_table[iarea] = {};
        for (var ination = gameconfig.nationinfo.US; ination <= gameconfig.nationinfo.USSR; ination++) {
            area_nation_list_table[iarea][ination] = new Array();
        }
    }
    for (var irank = 1; irank <= gameconfig.rankinfo.Max; irank++) {
        rank_nation_list_table[irank] = {};
        for (var ination = gameconfig.nationinfo.US; ination <= gameconfig.nationinfo.USSR; ination++) {
            rank_nation_list_table[irank][ination] = new Array();
        }
    }
    for (var iarea = 0; iarea <= gameconfig.max_area; iarea++) {
        area_rank_nation_list_table[iarea] = {};
        for (var irank = 1; irank <= gameconfig.rankinfo.Max; irank++) {
            area_rank_nation_list_table[iarea][irank] = {};
            for (var ination = gameconfig.nationinfo.US; ination <= gameconfig.nationinfo.USSR; ination++) {
                area_rank_nation_list_table[iarea][irank][ination] = new Array();
            }
        }
    }

    for (var row in result) {
        var info = new Object;
        info.no = Number(result[row]['No']);
        info.itemno = Number(result[row]['CommanderIndex']);
        info.area = Number(result[row]['Area']);
        info.nation = commandertable.GetCommanderTable(info.itemno).nation
        info.rank = commandertable.GetCommanderTable(info.itemno).rank;
        
        area_nation_list_table[info.area][info.nation].push(info);

        rank_nation_list_table[info.rank][info.nation].push(info);

        area_rank_nation_list_table[info.area][info.rank][info.nation].push(info);
    }
}
exports.Load = Load;
// Area와 Nation으로 지휘관 정보를 얻어옵니다.
function GetAreaNationCommander(area, nation) {
    return area_nation_list_table[area][nation];
}
exports.GetAreaNationCommander = GetAreaNationCommander;
// Rank와 Nation으로 지휘관 정보를 얻어옵니다.
function GetRankNationCommander(rank, nation) {
    return rank_nation_list_table[rank][nation];
}
exports.GetRankNationCommander = GetRankNationCommander;
function GetAreaRankNationCommander(area, rank, nation) {
    return area_rank_nation_list_table[area][rank][nation];
}
exports.GetAreaRankNationCommander = GetAreaRankNationCommander;