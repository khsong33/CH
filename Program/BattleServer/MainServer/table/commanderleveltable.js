﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');

var commander_level_table;
var commander_max_level;

// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerCommanderLevelTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    commander_level_table = {};
    commander_max_level = {};
    for (var row in result) {
        var info = new Object;
        info.rank = Number(result[row]['Rank']);
        info.level = Number(result[row]['Level']);
        info.needcard = Number(result[row]['NeedCard']);
        info.cost = Number(result[row]['Cost']);
        info.exp = Number(result[row]['Exp']);
        if (!commander_level_table.hasOwnProperty(info.rank)) {
            commander_level_table[info.rank] = {};
            commander_max_level[info.rank] = Number(0);
        }
        commander_level_table[info.rank][info.level] = info;
        commander_max_level[info.rank]++;
    }
}
exports.Load = Load;
// 지휘관 레벨 테이블을 얻어옵니다.
function GetCommanderLevelTable(rank, level) {
    return commander_level_table[rank][level];
}
exports.GetCommanderLevelTable = GetCommanderLevelTable;
// 랭크별 지휘관 최대 레벨을 받아옵니다.
function GetCommanderMaxLevel(rank) {
    return commander_max_level[rank];
}
exports.GetCommanderMaxLevel = GetCommanderMaxLevel;
// 현재 레벨에서 최대 레벨까지 필요한 카드수
function GetMaxCommanderItemCount(current_level, rank) {
    var start_level = Number(current_level);
    var max_level = commander_max_level[rank];
    var result_max_card = 0;
    for (var ilevel = start_level; ilevel <= max_level; ilevel++) {
        result_max_card += commander_level_table[rank][ilevel].needcard;
    }
    return result_max_card + 10; // 여유 카드는 10장으로 제한합니다.
}
exports.GetMaxCommanderItemCount = GetMaxCommanderItemCount;
