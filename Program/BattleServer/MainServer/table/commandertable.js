﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var gameconfig = require('../gameconfig');

var commander_table;

// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerCommanderTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    commander_table = {};
    for (var row in result) {
        var info = new Object;
        info.no = Number(result[row]['No']);
        info.nation = Number(gameconfig.nationinfo[result[row]['Nation']]);
        info.rank = Number(result[row]['Rank']);
        
        commander_table[info.no] = info;
    }
}
exports.Load = Load;
// 지휘관 아이템 테이블 얻어오기
function GetCommanderTable(itemNo) {
    return commander_table[itemNo];
}
exports.GetCommanderTable = GetCommanderTable;