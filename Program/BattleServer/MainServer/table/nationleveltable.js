﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var serverconfig = require('../serverconfig').getServerConfig();

var nation_level_table;
var max_level;

// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerNationLevelTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    nation_level_table = {};
    max_level = 0;
    for (var row in result) {
        var info = new Object;
        info.level = Number(result[row]['Level']);
        info.needexp = Number(result[row]['NeedExp']);
        info.maxexp = Number(result[row]['MaxExp']);
        info.gainlimitexp = Number(result[row]['GainLimitExp']);
        
        nation_level_table[info.level] = info;
        max_level++;
    }
}
exports.Load = Load;
// 지휘관 아이템 테이블 얻어오기
function GetNationLevel(level) {
    return nation_level_table[level];
}
exports.GetNationLevel = GetNationLevel;
// 최대 레벨을 얻어옵니다.
function GetMaxLevel() {
    return max_level;
}
exports.GetMaxLevel = GetMaxLevel;