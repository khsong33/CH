﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var gameconfig = require('../gameconfig');

var chest_table;

// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerChestTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    chest_table = {};
    for (var row in result) {
        var info = new Object;
        info.no = Number(result[row]['No']);
        info.area = Number(result[row]['Area']);
        info.type = Number(gameconfig.chestinfo[result[row]['Type']]);
        info.slotnum = Number(result[row]['SlotNum']);
        info.goldmin = Number(result[row]['GoldMin']);
        info.goldmax = Number(result[row]['GoldMax']);
        info.gemratio = Number(result[row]['GemActiveRatio']);
        info.gemmin = Number(result[row]['GemMin']);
        info.gemmax = Number(result[row]['GemMax']);
        info.totalcard = Number(result[row]['TotalCard']);
        info.rank2card = Number(result[row]['Rank2Card']);
        info.rank3card = Number(result[row]['Rank3Card']);
        
        if (!chest_table.hasOwnProperty(info.type)) {
            chest_table[info.type] = {};
        }
        chest_table[info.type][info.area] = info;
    }
}
exports.Load = Load;
// 상자 정보 얻어오기
function GetChestData(type, area) {
    return chest_table[type][area];
}
exports.GetCheatData = GetChestData;
