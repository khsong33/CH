﻿var fs = require('fs');
var csvparser = require('../csvparser');
var redisClient = require('../redis_store');
var gameconfig = require('../gameconfig');
var random = require('../random');

var reward_chest_table;

// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerRewardChestRatioCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    reward_chest_table = {};
    for (var row in result) {
        var info = new Object;
        info.area = Number(result[row]['Area']);
        info.chestratio = new Array();
        info.chestratio.push({ 'Type' : gameconfig.chestinfo.Wooden, 'Ratio' : Number(result[row]['Wooden']) });
        info.chestratio.push({ 'Type' : gameconfig.chestinfo.Silver, 'Ratio' : Number(result[row]['Silver']) });
        info.chestratio.push({ 'Type' : gameconfig.chestinfo.Golden, 'Ratio' : Number(result[row]['Golden']) });
        info.chestratio.push({ 'Type' : gameconfig.chestinfo.Magic, 'Ratio' : Number(result[row]['Magic']) });
        
        reward_chest_table[info.area] = info;
    }
}
exports.Load = Load;
// 지휘관 아이템 테이블 얻어오기
function GetRewardChest(area) {
    var reward_chest = reward_chest_table[area];
    var ratio_total = 0;
    for (var ichest = 0; ichest < reward_chest.chestratio.length; ichest++) {
        ratio_total += reward_chest.chestratio[ichest].Ratio;
    }
    var select_value = random.randomRange(0, ratio_total + 1);
    var update_value = 0;
    for (var ichest = 0; ichest < reward_chest.chestratio.length; ichest++) {
        update_value += reward_chest.chestratio[ichest].Ratio;
        if (reward_chest.chestratio[ichest].Ratio <= 0)
            continue;
        if (select_value <= update_value)
            return reward_chest.chestratio[ichest];
    }
}
exports.GetRewardChest = GetRewardChest;
