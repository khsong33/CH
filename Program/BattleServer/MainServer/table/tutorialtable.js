﻿var fs = require('fs');
var csvparser = require('../csvparser');
var random = require('../random');
var redisClient = require('../redis_store');

var tutorial_table;
var tutorial_table_by_order;
// 테이블 로드
function Load() {
    var data = fs.readFileSync('./data/ServerTutorialTableCSV.txt', 'ucs2');
    var result = csvparser.LoadCsv(data.toString(), 0);
    
    tutorial_table = {};
    tutorial_table_by_order = [];
    for (var row in result) {
        var info = new Object;
        info.no = Number(result[row]['NO']);
        info.stageid = Number(result[row]['StageId']);
        info.order = Number(result[row]['Order']);
        var my_item_ids_string = String(result[row]['MyItemsId']).split(',');
        info.myitemsid = [];
        for (var index in my_item_ids_string) {
            info.myitemsid.push(Number(my_item_ids_string[index]));
        }
        var enemy_item_numbers_string = String(result[row]['EnemyItemsNo']).split(',');
        info.enemyitemsno = [];
        for (var index in enemy_item_numbers_string) {
            info.enemyitemsno.push(Number(enemy_item_numbers_string[index]));
        }        
        info.enemyname = String(result[row]['EnemyName']);
        
        tutorial_table[info.specNo] = info;
        tutorial_table_by_order.push(info);
    }
    tutorial_table_by_order.sort(function (ldata, rdata) {
        return ldata.order - rdata.order;
    });
}
exports.Load = Load;
// 해당 index에 맞는 튜토리얼 정보를 얻어옵니다.
function GetTutorialByOrder(tutorial_index) {
    if (tutorial_index >= tutorial_table_by_order.length) {
        return null;
    }
    return tutorial_table_by_order[tutorial_index];
}
exports.GetTutorialByOrder = GetTutorialByOrder;

function CreateTutorialMatchInfo(userinfoJson, cb_mathinfo) {
    var tutorial_index = userinfoJson.ClearTutorial + 1;
    var tutorial_info = GetTutorialByOrder(tutorial_index);
    var match_info = {
        'StageId' : Number(tutorial_info.stageid),
        'RandomIndex' : Number(random.SyncRandomRange(0, 16)),
        'RandomState' : [],
        'UserInfo' : [],
    }
    for (var istate = 0; istate < 16; istate++) {
        match_info.RandomState.push(Number(random.SyncRandomRangeInc(0, 9999)));
    }
    var enemy_info = {
        'UniqueId' : Number(0),
        'UserName' : String(tutorial_info.enemyname),
        'GuildId' : Number(0),
        'Win' : Number(0),
        'Lose' : Number(0),
        'WinStreak' : Number(0),
        'LoseStreak' : Number(0),
        'Rating' : Number(0),
        'UseDeckId' : Number(0),
        'UseDeckNation' : Number(2),
        //'NationExps' : [0, 0, 0],
        //'NationLevels' : [1, 1, 1],
        'ClearTutorial' : Number(-1),
        'UserKey' : '',
        'Decks' : {
            'UniqueId' : Number(-1),
            'Count' : Number(tutorial_info.enemyitemsno.length),
            'Items' : []
        },
        'Ai' : 'FALSE',
        'UserIdx' : Number(1)
    }
    for (var itemidx in tutorial_info.enemyitemsno) {
        var item_info = {
            'ItemId' : Number(itemidx),
            'ItemNo' : Number(tutorial_info.enemyitemsno[itemidx]),
            'ItemRank' : Number(1),
            'ItemExp' : Number(0),
            'SlotId' : Number(itemidx),
            'ItemLevel' : Number(1),
            'ItemCount' : Number(0)
        }
        enemy_info.Decks.Items.push(item_info);
    }
    match_info.UserInfo.push(enemy_info);

    var find_commander_item_key = 'uidcommander:' + userinfoJson.UniqueId;
    var find_item_id_list = [];
    for (var itemidx in tutorial_info.myitemsid) {
        find_item_id_list.push(tutorial_info.myitemsid[itemidx]);
    }
    redisClient.store_redis.hmget(find_commander_item_key, find_item_id_list, function (err, commanderitem) {
        var my_user_info = {
            'UniqueId' : Number(userinfoJson.UniqueId),
            'UserName' : String(userinfoJson.UserName),
            'GuildId' : Number(userinfoJson.GuildId),
            'Win' : Number(userinfoJson.Win),
            'Lose' : Number(userinfoJson.Lose),
            'WinStreak' : Number(userinfoJson.WinStreak),
            'LoseStreak' : Number(userinfoJson.LoseStreak),
            'Rating' : Number(userinfoJson.Rating),
            'UseDeckId' : Number(userinfoJson.UseDeckId),
            'UseDeckNation' : Number(userinfoJson.UseDeckNation),
            //'NationExps' : userinfoJson.NationExps,
            //'NationLevels' : userinfoJson.NationLevels,
            'ClearTutorial' : Number(userinfoJson.ClearTutorial),
            'UserKey' : String(userinfoJson.UserKey),
            'Decks' : {
                'UniqueId' : Number(userinfoJson.UniqueId),
                'Count' : Number(tutorial_info.myitemsid.length),
                'Items' : []
            },
            'Ai' : 'FALSE',
            'UserIdx' : Number(0)
        }
        for (var itemidx in commanderitem) {
            var commanderitem_json = JSON.parse(commanderitem[itemidx]);
            commanderitem_json.SlotId = Number(itemidx);
            my_user_info.Decks.Items.push(commanderitem_json);
        }
        match_info.UserInfo.push(my_user_info);

        cb_mathinfo(match_info);
    });
}
exports.CreateTutorialMatchInfo = CreateTutorialMatchInfo;