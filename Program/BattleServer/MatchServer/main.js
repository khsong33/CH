﻿var aiuserlist = require('./aiuserlist')
var random = require('./random');
var redisClient = require('./redis_store');
var mssql = require('./mssql');
var match = require('./match');
var logger = require('./logger');
var log = new logger();

redisClient.initRedis();
redisClient.initCallback(CallbackSubscribe);

var wait_pvp_number = 0;

log.info("Start Match Server");

mssql.InitSql(function () {
    aiuserlist.AddAiListFromSQL();
});

var pvp_queue = setInterval(function () {
    redisClient.match_redis.zcard('wait_pvp_list_' + wait_pvp_number, function (err, wait_count) {
        if (wait_count > 0) {
            match.GoMatchPvP(wait_pvp_number, Number(2), function (remain_user_list) {
                redisClient.match_redis.del('wait_pvp_' + wait_pvp_number);
                redisClient.match_redis.del('wait_pvp_list_' + wait_pvp_number);
                wait_pvp_number = wait_pvp_number + 1;
                for (var iuser = 0; iuser < remain_user_list.length; iuser++) {
                    redisClient.match_redis.hmset('wait_pvp_' + wait_pvp_number, remain_user_list[iuser].UserKey, remain_user_list[iuser]);
                    redisClient.match_redis.zadd('wait_pvp_list_' + wait_pvp_number, remain_user_list[iuser].Rating, remain_user_list[iuser].UserKey);
                }
            });
        }
    });
}, 10000);

// redis 구독 처리
redisClient.match_sub.subscribe('registe_pvp');
redisClient.match_sub.subscribe('unregiste_pvp');
function CallbackSubscribe(channel, message) {
    if (channel == "registe_pvp") {
        var userInfo = message;
        var userInfoJson = JSON.parse(userInfo);
        redisClient.match_redis.hmset('wait_pvp_' + wait_pvp_number, userInfoJson.UserKey, userInfo);
        redisClient.match_redis.zadd('wait_pvp_list_' + wait_pvp_number, userInfoJson.Rating, userInfoJson.UserKey);
        aiuserlist.RemoveAiUser(userInfoJson.UniqueId, userInfoJson.Rating);
    }
    else if (channel == "unregiste_pvp") {
        var userKey = message;
        redisClient.match_redis.hdel('wait_pvp_' + wait_pvp_number, userKey);
        redisClient.match_redis.zrem('wait_pvp_list_' + wait_pvp_number, userKey);
    }
}