﻿var redisClient = require('./redis_store');
var crypto = require('crypto');
var aiuserlist = require('./aiuserlist');
var random = require('./random');
random.InitSyncRandom();

//var room_hash = crypto.createHash('md5').update('tadgawey' + 'gasdayey').digest('hex');
//console.log(room_hash);

// 1대 1 매칭 처리
function GoMatchPvP(wait_number, play_count, cb_remain_user) {
    var find_user_list = 'wait_pvp_list_' + wait_number;
    redisClient.match_redis.zrange(find_user_list, 0, -1, 'WITHSCORES', function (err, user_list) {
        var user_data_list = [];
        var remain_user_list = [];
        for (var idata = 0; idata < user_list.length; idata += 2) {
            var user_rating_info = {
                'UserKey' : String(user_list[idata]),
                'Rating' : Number(user_list[idata + 1])
            }
            user_data_list.push(user_rating_info);
        }
        var play_user_list = [];
        for (var iuser = 0; iuser < user_data_list.length; iuser++) {
            play_user_list.push(user_data_list[iuser].UserKey);
            if (play_user_list.length == play_count) {
                _MakePvPRoom(wait_number, play_user_list, play_count, function (make_remain_user_list) {
                    for (var iuser = 0; iuser < make_remain_user_list.length; iuser++) {
                        remain_user_list.push(make_remain_user_list[iuser]);
                    }
                });
                play_user_list.length = 0;
            }
        }
        if (play_user_list.length != 0) {
            _MakePvPRoom(wait_number, play_user_list, play_count, function (make_remain_user_list) {
                for (var iuser = 0; iuser < make_remain_user_list.length; iuser++) {
                    remain_user_list.push(make_remain_user_list[iuser]);
                }
                cb_remain_user(remain_user_list)
            });
        }
        else {
            cb_remain_user(remain_user_list);
        }
    });
}
exports.GoMatchPvP = GoMatchPvP;

// 1:1 PVP방 만들기
function _MakePvPRoom(wait_number, userkeylist, play_count, cb_remain_user) {
    var find_user_key = 'wait_pvp_' + wait_number;
    redisClient.match_redis.hmget(find_user_key, userkeylist, function (err, userinfolist) {
        var roominfo = _GetRoomInfo(userinfolist);
        var ai_user_count = play_count - roominfo.PlayUser.length;
        _GetAiUser(roominfo.AiRating, ai_user_count, function (ai_user_list) {
            random.SyncRandomNotOverlap(0, play_count, function (result_random, size) {
                var userdatas = [];
                for (var iuser = 0; iuser < roominfo.PlayUser.length; iuser++) {
                    roominfo.PlayUser[iuser].Ai = String('FALSE');
                    roominfo.PlayUser[iuser].UserIdx = Number(result_random[iuser]);
                    userdatas.push(roominfo.PlayUser[iuser]);
                }
                for (var iaiuser = 0; iaiuser < ai_user_count; iaiuser++) {
                    ai_user_list[iaiuser].Ai = String('TRUE');
                    ai_user_list[iaiuser].UserIdx = Number(result_random[userinfolist.length + iaiuser]);
                    userdatas.push(ai_user_list[iaiuser]);
                }
                _MakeRoom('PVP', userdatas, function (roomname, user_key_list) {
                    for (var iuser = 0; iuser < user_key_list.length; iuser++) {
                        redisClient.match_redis.publish('roomstart:' + user_key_list[iuser], roomname);
                    }
                    cb_remain_user(roominfo.RemainUser);
                });
            });
        });
    });
}
// 방에서 제외되어야 될 유저와 플레이할 유저를 나눠주며, AI를 위한 rating 정보를 셋팅합니다.
function _GetRoomInfo(userinfolist) {
    var rating_user = JSON.parse(userinfolist[0]);
    var result_remain = {
        'AiRating' : rating_user.Rating,
        'PlayUser' : [],
        'RemainUser' : []
    };
    for (var iuser = 0; iuser < userinfolist.length; iuser++) {
        // 플레이할 유저와 다음 큐로 넘길 유저를 처리합니다.
        result_remain.PlayUser.push(JSON.parse(userinfolist[iuser]));
    }
    return result_remain;
}
// 유저 점수로 Ai 유저 얻어오기
function _GetAiUser(rating, aicount, cb_ai_list) {
    if (aicount == 0) {
        cb_ai_list([]);
        return;
    }
    aiuserlist.SelectAIUser(rating, aicount, function (select_aiUserList) {
        var ai_user_list = [];
        for (var iBot= 0; iBot < aicount; iBot++) {
            var userinfo = JSON.parse(select_aiUserList[iBot]);
            ai_user_list.push(userinfo);
        }
        cb_ai_list(ai_user_list);
    });
}
// 방을 만듭니다.
function _MakeRoom(type, userdatas, cb_room) {
    var match_info;        
    if (type == 'PVP') {
        match_info = {
            'StageId' : Number(1),
            'RandomIndex' : Number(random.randomRange(0, 16)),
            'RandomState' : [],
            'UserInfo' : []
        }
        var user_key_list = [];
        
        for (var istate = 0; istate < 16; istate++) {
            match_info.RandomState.push(random.randomRange(0, 10000));
        }
        
        for (var iuser = 0; iuser < userdatas.length; iuser++) {
            match_info.UserInfo.push(userdatas[iuser]);
            user_key_list.push(userdatas[iuser].UserKey);
        }
        var room_name = crypto.createHash('md5').update(JSON.stringify(match_info)).digest('hex');
        match_info.Room = String(room_name);
        redisClient.match_redis.set('room:' + room_name, JSON.stringify(match_info), function (err) {
            cb_room(room_name, user_key_list);
        });
    }
}