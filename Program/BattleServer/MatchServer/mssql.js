﻿var mssql = require('mssql');
var logger = require('./logger');
var serverconfig = require('./serverconfig').getServerConfig();

// DB 초기화 및 연결
var connection = null;
var dbconfig = serverconfig.store_mssql;
function InitSql(cb_connect) {
    connection = new mssql.Connection(dbconfig, function (err) {
        if (err) {
            console.log("mssql connection err - " + err);
        }
        else {
            console.log("connect success db - " + dbconfig.server);
            cb_connect();
        }
    });
}
exports.InitSql = InitSql;

// AI 리스트 얻어오기
function getAIList(listCount, minRating, maxRating, addSec, cb_aiList, isOverwrite) {
    var ai_user_count = 0;
    var request = new mssql.Request(connection);
    request.input('ListCount', Number(listCount));
    request.input('MinRating', Number(minRating));
    request.input('MaxRating', Number(maxRating));
    request.input('AddSeconds', Number(addSec));
    request.execute('GetAiList', function (err, recordsets, returnvalue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetAIList - " + err);
        }
        var aiUserList = {
            'Users' : []
        }
        var aiListCount = recordsets[0].length;
        for (var iUsers = 0; iUsers < aiListCount; iUsers++) {
            var UseDeckId = Number(recordsets[0][iUsers]["UseDeckId"]);
            var ai_userinfo =_ConvertUserInfoDBtoJson(recordsets[0], iUsers);
            
            getUserCommanderItem(ai_userinfo, ai_userinfo.UseDeckId, function (ai_userinfo_cb, commanderItem) {
                ai_userinfo_cb.Decks = commanderItem;
                aiUserList.Users.push(ai_userinfo_cb);
                ai_user_count++;
                if (ai_user_count >= aiListCount) {
                    cb_aiList(aiUserList, minRating, maxRating, isOverwrite);
                }
            });
        }// end for
    });// end excute
}
exports.getAIList = getAIList;

// 유저 카드 정보 가져오기 (전체덱을 가져오려면 find_deck_id에 0보다 작은값을 넣습니다)
function getUserCommanderItem(userinfo_json, find_deck_id, cb_commanderitem) {
    var request = new mssql.Request(connection);
    request.input('UniqueId', userinfo_json.UniqueId);
    request.input('UseDeckId', find_deck_id);
    request.execute('GetCH2CommanderItem', function (err, recordsets, returnValue) {
        if (err) {
            var log = new logger();
            log.error("Failed Excute GetCH2CommanderItem");
        }
        var user_commander = _ConvertCommanderItemDBtoJson(recordsets[0], userinfo_json.UniqueId);
        cb_commanderitem(userinfo_json, user_commander)
    })
}
exports.getUserCommanderItem = getUserCommanderItem;

// 유저 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertUserInfoDBtoJson(dbdata, dbidx) {
    var userinfo_json = {
        'UniqueId' : Number(dbdata[dbidx]["UniqueId"]),
        'UserName' : dbdata[dbidx]["UserName"],
        'GuildId' : Number(dbdata[dbidx]["GuildId"]),
        'Win' : Number(dbdata[dbidx]["Win"]),
        'Lose' : Number(dbdata[dbidx]["Lose"]),
        'WinStreak' : Number(dbdata[dbidx]["WinStreak"]),
        'LoseStreak' : Number(dbdata[dbidx]["LoseStreak"]),
        'Rating' : Number(dbdata[dbidx]["Rating"]),
        'Area' : Number(dbdata[dbidx]["Area"]),
        'UseDeckId' : Number(dbdata[dbidx]["UseDeckId"]),
        'UseDeckNation' : Number(dbdata[dbidx]["UseDeckNation"]),
        //'NationExps' : [Number(dbdata[dbidx]["NationExp1"]), Number(dbdata[dbidx]["NationExp2"]), Number(dbdata[dbidx]["NationExp3"])],
        //'NationLevels' : [Number(dbdata[dbidx]["NationLevel1"]), Number(dbdata[dbidx]["NationLevel2"]), Number(dbdata[dbidx]["NationLevel3"])],
        'ClearTutorial' : Number(dbdata[dbidx]["ClearTutorial"])
    }
    return userinfo_json;
}
// 지휘관 아이템 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertCommanderItemDBtoJson(dbdata, uniqueId) {
    var commander_item_count = dbdata.length;
    var user_commander_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(commander_item_count),
        'Items' : []
    };
    for (var iItem = 0; iItem < commander_item_count; iItem++) {
        var user_commander_item = {
            'ItemId' : Number(dbdata[iItem]["ItemId"]),
            'ItemNo' : Number(dbdata[iItem]["ItemNo"]),
            'ItemRank' : Number(dbdata[iItem]["ItemRank"]),
            'ItemExp' : Number(dbdata[iItem]["ItemExp"]),
            'ItemLevel' : Number(dbdata[iItem]["ItemLevel"]),
            'ItemCount' : Number(dbdata[iItem]["ItemCount"]),
            'SlotId' : Number(dbdata[iItem]["SlotId"])
        };
        user_commander_json.Items.push(user_commander_item);
    }
    return user_commander_json;
}
// 지휘관 덱 정보 DB 데이터를 Json으로 변환합니다.
function _ConvertDeckDBtoJson(dbdata, uniqueId) {
    var deck_count = dbdata.length;
    var deckinfo_json = {
        'UniqueId' : Number(uniqueId),
        'Count' : Number(deck_count),
        'Decks' : []
    }
    for (var iDeck = 0; iDeck < deck_count; iDeck++) {
        var deck = {
            'DeckId' : Number(dbdata[iDeck]["DeckId"]),
            'DeckNation' : Number(dbdata[iDeck]["DeckNation"]),
            'DeckName' : String(dbdata[iDeck]["DeckName"]),
            'ItemIds' : [],
            'Active' : Number(1)
        }
        var itemIdString = dbdata[iDeck]["ItemIds"];
        var itemIdArray = itemIdString.split(',', gameconfig.max_deck_commander_count);
        for (var iItem = 0; iItem < gameconfig.max_deck_commander_count; iItem++) {
            deck.ItemIds.push(Number(itemIdArray[iItem]));
        }
        deckinfo_json.Decks.push(deck);
    }
    
    return deckinfo_json;
}

