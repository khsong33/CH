﻿var redis = require('redis');
var serverconfig = require('./serverconfig').getServerConfig();

var match_redis_ip = serverconfig.match_redis.ip;
var match_redis_port = serverconfig.match_redis.port;

var match_redis = redis.createClient(match_redis_port, match_redis_ip, { detect_buffers : true });
exports.match_redis = match_redis;


// subscriber에 return_buffers가 없을 경우 trailing byte error가 발생
var match_sub = redis.createClient(match_redis_port, match_redis_ip, { detect_buffers : true });
exports.match_sub = match_sub;

function initRedis() {
    match_redis.select(2, function (err) { });
}
exports.initRedis = initRedis;

function initCallback(callback) {
    global_callback = callback;
}
exports.initCallback = initCallback;

match_sub.on('message', function (channel, message) {
    if (global_callback != null) {
        global_callback(channel, message);
    }
});
