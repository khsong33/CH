﻿var fs = require('fs');
var serverconfig = null;

function getServerConfig() {
    if (serverconfig == null) {
        var config_file_name = process.argv[2];
        serverconfig = JSON.parse(fs.readFileSync(config_file_name, 'utf8'));
    }
    return serverconfig;
}
exports.getServerConfig = getServerConfig;