﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Windows;
using System.ComponentModel;
using System.IO;
using Microsoft.VisualBasic.FileIO;

namespace ToolsCommon
{
    public class AnyItem
    {
        public void Add(string key, object value)
        {
            prop[key] = value;
        }
        public string GetString(string key)
        {
            return prop[key] as string;
        }
        public int? GetInt(string key)
        {
            return prop[key] as int?;
        }

        public string Name {
            get {
                return prop["Name"].ToString();
            }
            set {
                if (prop.ContainsKey("Name") == false)
                    prop.Add("Name", value);
                else
                    prop["Name"] = value; }
           }
        public Dictionary<string, object> prop = new Dictionary<string, object>();
    }

    public class AnyCSVTable
    {
        public class HeaderLine
        {
            public string type;
            public Dictionary<string, int> headerColumnNameToIdx = new Dictionary<string, int>();
            public Dictionary<int, string> headerColumnIdxToName = new Dictionary<int, string>();
        }

        public class ItemLine
        {
            public List<string> columns;
        }

        Dictionary<string, HeaderLine> headers = new Dictionary<string, HeaderLine>();
        List<ItemLine> items = new List<ItemLine>();

        public int Count {
            get { return items.Count; }
        }

        public string Get(int idx, string headerName, string headerType = null)
        {
            if (headerType == null)
                return items[idx].columns[headers.First().Value.headerColumnNameToIdx[headerName]];
            else
                return items[idx].columns[headers[headerType].headerColumnNameToIdx[headerName]];
        }

        public void Set(int idx, string headerName, string value, string headerType = null)
        {
            HeaderLine selectedHeader = null;
            if (headerType == null)
                selectedHeader = headers.First().Value;
            else
                selectedHeader = headers[headerType];

            if (selectedHeader.headerColumnNameToIdx.ContainsKey(headerName) == false)
            {
                // 새로등장한 컬럼이다.
                selectedHeader.headerColumnNameToIdx.Add(headerName, selectedHeader.headerColumnNameToIdx.Count);
                selectedHeader.headerColumnIdxToName.Add(selectedHeader.headerColumnIdxToName.Count, headerName);

                foreach (var item in items)
                    item.columns.Add("");
            }

            items[idx].columns[selectedHeader.headerColumnNameToIdx[headerName]] = value;
        }

        public int GetInt(int idx, string name, string headerType = null)
        {
            return int.Parse(Get(idx, name, headerType));
        }

        public void Read(string filename, List<string> headerTypes, bool append = false)
        {
            if (append == false)
            {
                headers.Clear();
                items.Clear();
            }

            TextFieldParser ps = new TextFieldParser(filename, Encoding.GetEncoding("EUC-KR"));
            using (ps)
            {
                ps.TextFieldType = FieldType.Delimited;
                ps.SetDelimiters(",");

                if (append == false)
                {
                    foreach (string headerType in headerTypes)
                    {
                        var headerRow = new HeaderLine();
                        headerRow.type = headerType;
                        var columns = ps.ReadFields().ToList();
                        for (int i = 0; i < columns.Count ; i++)
                        {
                            headerRow.headerColumnNameToIdx.Add(columns[i], i);
                            headerRow.headerColumnIdxToName.Add(i, columns[i]);
                        }
                        headers.Add(headerType, headerRow);
                    }
                }

                while (!ps.EndOfData)
                {
                    var item = new ItemLine(); // 젤 처음 열린 파일 구조 기반.
                    item.columns = ps.ReadFields().ToList();
                    items.Add(item);
                }
                ps.Close();
            }
        }
        
        public void Write(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Create);
            using (TextWriter tw = new StreamWriter(fs, System.Text.Encoding.UTF8))
            {
                foreach (var header in headers.Values)
                {
                    for (int i = 0; i < header.headerColumnIdxToName.Count; i++)
                    {
                        if (i < header.headerColumnIdxToName.Count - 1)
                            tw.Write(header.headerColumnIdxToName[i] + ",");
                        else
                            tw.Write(header.headerColumnIdxToName[i] + "\n");
                    }
                }

                foreach (var item in items)
                {
                    for (int i = 0; i < item.columns.Count; i++)
                    {
                        if (i < item.columns.Count - 1)
                            tw.Write(item.columns[i] + ",");
                        else
                            tw.Write(item.columns[i] + "\n");
                    }
                }
            }
        }
    }
}
