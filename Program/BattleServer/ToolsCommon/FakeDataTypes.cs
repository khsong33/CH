﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolsCommon
{
    [Serializable]
    public class FakeTroopInfo
    {
        [DynamicListKey("troop")]
        public string troopName = "US001 Rifleman";
        [DynamicListKey("weapon")]
        public string weaponName = "M1GarandRFS";
        public int commanderLevel = 1;
    }

    [Serializable]
    public class FakeUserInfo
    {
        public bool ai = false;
        public int hqGuid;
        public int teamIdx;
        public int maxMp = 25;
        public int startMp = 0;
        public string userName = "user";
        public int deckIdx = 0;
        public DeckNation nation = DeckNation.US;
        [Hidden]
        public List<CommanderItemData> commanderItemDataList = new List<CommanderItemData>();
    }

    [Serializable]
    public class FakeMatchInfo
    {
        public int stageNo = 1;
        public int playTime = 10 * 60 * 1000;
        public string bundleName = "GridLayouts0";
        [DynamicListKey("layout")]
        public string assetPath = "GridLayout - empty";
    }

    public class FakeBattleMatchInfo : BattleMatchInfo
    {
        public string bundleName;
        public string assetPath;

        public void FakeMatchInfo(FakeMatchInfo toolMatchInfo, FakeUserInfo[] userInfos)
        {
            bundleName = toolMatchInfo.bundleName;
            assetPath = toolMatchInfo.assetPath;

            mRuleType = BattleMatchInfo.RuleType.Conquest;
            mStageNo = toolMatchInfo.stageNo;

            mRandomStates = new UInt32[16];
            
            mRandomStates[0] = 3732;
            mRandomStates[1] = 5403;
            mRandomStates[2] = 1749;
            mRandomStates[3] = 9599;
            mRandomStates[4] = 4558;
            mRandomStates[5] = 6711;
            mRandomStates[6] = 1366;
            mRandomStates[7] = 2879;
            mRandomStates[8] = 6821;
            mRandomStates[9] = 520;
            mRandomStates[10] = 8160;
            mRandomStates[11] = 671;
            mRandomStates[12] = 766;
            mRandomStates[13] = 5053;
            mRandomStates[14] = 4006;
            mRandomStates[15] = 6443;

            mRandomSeed = 4;

            mPlayTime = toolMatchInfo.playTime;
            mCheckPointStarCount = 3;
            mUserCount = 2;

            for (int iUser = 0; iUser < mUserCount; iUser++)
            {
                BattleUserInfo lUserInfo = new BattleUserInfo();

                lUserInfo.mUserIdx = iUser;
                lUserInfo.mUserName = userInfos[iUser].userName;
                lUserInfo.mClientGuid = 0;
                lUserInfo.mIsAi = userInfos[iUser].ai;

                lUserInfo.mHeadquarterGuid = (uint)userInfos[iUser].hqGuid;
                lUserInfo.mTeamIdx = userInfos[iUser].teamIdx;
                lUserInfo.mMaxMp = userInfos[iUser].maxMp;
                lUserInfo.mStartMp = userInfos[iUser].startMp;

				//lUserInfo.mNationExps = new int[] { 1, 1 };
				//lUserInfo.mNationLevels = new int[] { 1, 1 };
                lUserInfo.mWin = 0;
                lUserInfo.mLose = 0;
                lUserInfo.mWinStreak = 0;
                lUserInfo.mLoseStreak = 0;
                lUserInfo.mBattleNation = userInfos[iUser].nation;

                for (int iSlot = 0; iSlot < userInfos[iUser].commanderItemDataList.Count; iSlot++)
                    lUserInfo.AddCommanderItem(iSlot, userInfos[iUser].commanderItemDataList[iSlot]);

                mUserInfos[iUser] = lUserInfo;
            }

            _InitPlay();
        }
    }
}
