﻿using UnityEngine;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Windows;

namespace ToolsCommon
{
    public class Simulator
    {
        public Simulator(BattleLogic pBattleLogic, IStage pStage)
        {
            mBattleLogic = pBattleLogic;
            mStage = pStage;

            mProgressInfo = new BattleProgressInfo();
            mUsers = new BattleUser[BattleMatchInfo.cMaxUserCount];

            mUpdateScheduler = new BattleUpdateScheduler();

            mFrameTickContext = new BattleFrameTickContext();
            mFrameTickContext.aUsers = mUsers;
        }

        // 데이터를 검증합니다.
        public void StartSimulation(FakeBattleMatchInfo matchInfo)
        {
            Reset();

            mMatchInfo = matchInfo;

            mMatchInfo.aStageSpec.mGridLayoutAssetKey = AssetManager.get.CreateAssetKey(matchInfo.bundleName, matchInfo.assetPath);

            AssetKey lGridLayoutAssetKey = mMatchInfo.aStageSpec.mGridLayoutAssetKey;
            String lGridLayoutText = AssetManager.get.LoadText(lGridLayoutAssetKey);

            // 진행 정보를 초기화합니다.
            mProgressInfo.Init(mMatchInfo);

            // 전투 격자를 생성합니다.
            mBattleGrid = mBattleLogic.CreateGrid(
                lGridLayoutAssetKey.ToString(),
                lGridLayoutText,
                mMatchInfo,
                mProgressInfo,
				mStage);

            // 유저를 생성합니다.
            for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
            {
                BattleUserInfo lUserInfo = mMatchInfo.mUserInfos[iUser];

                BattleUser.ControlType lControlType = lUserInfo.mIsAi ? BattleUser.ControlType.Auto : BattleUser.ControlType.Manual;

                mUsers[iUser] = mBattleLogic.CreateUser(
                    mBattleGrid,
                    lUserInfo,
                    lControlType);
            }

            // 갱신 스케쥴을 잡습니다.
            mUpdateScheduler.Init(mMatchInfo, mBattleGrid.aFrameUpdater);

            // 전투 시작 전 처리를 합니다.
            mBattleGrid.PreBattleSetUp();

            // 프레임 입력을 처리하며 갱신 루프를 돕니다.
            mFrameTickContext.aMatchInfo = mMatchInfo;
            mFrameTickContext.aBattleGrid = mBattleGrid;
            mFrameTickContext.aProgressInfo = mProgressInfo;

            mBattleGrid.aFrameUpdater.aOnFrameSliceChangeDelegate = _OnFrameSliceChange;
        }

        public bool UpdateSimulation(List<BattleFrameInput> battleFrameInputList, ref int lVerifyDataFrameInputIdx)
        {
            bool lIsValidResult = true;

            // 틱을 처리합니다.
            if (mBattleGrid.aFrameUpdater.aFrameIdx == mUpdateScheduler.aFrameIdxUpdateLimit)
            {
                // 틱을 생성합니다.
                BattleFrameTick lFrameTick = BattleFrameTick.Create(mUpdateScheduler.aBattleFrameTickNo);

                if (battleFrameInputList != null)
                {
                    lock (battleFrameInputList)
                    {
                        if (battleFrameInputList.Count > 0)
                        {
                            lFrameTick.AddFrameInput(battleFrameInputList[0]);
                            battleFrameInputList.RemoveAt(0);
                        }
                    }
                }

                // 틱을 처리합니다.
                try
                {
                    BattleProtocol.OnFrameTick(lFrameTick, mFrameTickContext);
                }
                catch (System.Exception ex)
                {
                    lIsValidResult = false;
                    UnityEngine.Debug.LogError("[Exception] BattleProtocol.OnFrameTick() - " + ex.Message);
                    Debugger.Break();
                    return false;
                }
                lFrameTick.Destroy();

                // 틱을 갱신합니다.
                mUpdateScheduler.UpdateTick();
            }

            // 전투 로직을 갱신합니다.
            try
            {
                // 프레임을 갱신합니다.
                mBattleGrid.aFrameUpdater.UpdateFrame();
            }
            catch (System.Exception ex)
            {
                lIsValidResult = false;
                UnityEngine.Debug.LogError("Exception] mBattleGrid.aFrameUpdater.UpdateFrame() - " + ex.Message);
                Debugger.Break();
                return false;
            }

            if (mBattleGrid.aFrameUpdater.aIsStopped)
                return false;

            return lIsValidResult;
        }

        public class BattleFrameTickContext : IBattleFrameTickContext
        {
            public BattleMatchInfo aMatchInfo { get; set; }
            public BattleGrid aBattleGrid { get; set; }
            public BattleProgressInfo aProgressInfo { get; set; }
            public BattleUser[] aUsers { get; set; }

            public void OnFrameInputSpawnCommander(BattleCommander pSpawnCommander, int pLastTroopCount)
            {
            }
            public void OnFrameInputGoodGame(BattleUser pInputUser)
            {
            }
        }

        public BattleProgressInfo aProgressInfo
        {
            get { return mProgressInfo; }
        }
        //-------------------------------------------------------------------------------------------------------
        // 전투 격자
        public BattleGrid aBattleGrid
        {
            get { return mBattleGrid; }
        }
        //-------------------------------------------------------------------------------------------------------
        // 유저 테이블
        public BattleUser[] aUsers
        {
            get { return mUsers; }
        }

        //-------------------------------------------------------------------------------------------------------
        // 검증 정보를 초기화합니다.
        public void Reset()
        {
            if (mBattleGrid == null) // BattleGrid의 유무에 따라서 초기화 판단
                return;

            for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; ++iUser)
                if (mUsers[iUser] != null)
                {
                    mUsers[iUser].Destroy();
                    mUsers[iUser] = null;
                }

            mBattleGrid.Destroy();
            mBattleGrid = null;
        }

        //-------------------------------------------------------------------------------------------------------
        // 구현
        //-------------------------------------------------------------------------------------------------------
        // FrameUpdater.OnFrameSliceChange
        protected void _OnFrameSliceChange(int pFrameNo, int pFrameSliceIdx, int pFrameDelta)
        {
            //Debug.Log("_OnFrameSliceChange() pFrameNo=" + pFrameNo + " pFrameSliceIdx=" + pFrameSliceIdx);

            // 전투 진행을 갱신합니다.
            if (pFrameSliceIdx == mUpdateScheduler.aBattleProgressFrameSliceIdx)
            {
                if (!mProgressInfo.UpdateProgress(mBattleGrid, pFrameDelta))
                    mBattleGrid.aFrameUpdater.StopUpdate(true);
            }

            // 각 조각에서는 유저별 갱신을 합니다.
            for (int iProcess = 0; iProcess < mUpdateScheduler.aFrameSliceUpdateUserProcessCount[pFrameSliceIdx]; ++iProcess)
            {
                int iProcessUserIdx = mUpdateScheduler.aFrameSliceUpdateProcessUserIdxs[pFrameSliceIdx, iProcess];
                mUsers[iProcessUserIdx].UpdateFrame();
            }

            // AI 콜렉터 갱신을 합니다.
            if (pFrameSliceIdx == mUpdateScheduler.aAiCollectorFrameSliceIdx)
            {
                if (mBattleGrid.aAiCollector != null)
                    mBattleGrid.aAiCollector.UpdateFrame();
            }
        }

        //-------------------------------------------------------------------------------------------------------
        // 데이터
        //-------------------------------------------------------------------------------------------------------
        private BattleLogic mBattleLogic;
        private BattleMatchInfo mMatchInfo;
        private BattleProgressInfo mProgressInfo;
        private IStage mStage;
        private BattleGrid mBattleGrid;
        private BattleUser[] mUsers;
        private BattleFrameTickContext mFrameTickContext;
        private BattleUpdateScheduler mUpdateScheduler;
    }
}
