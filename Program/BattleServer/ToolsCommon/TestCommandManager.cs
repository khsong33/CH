﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolsCommon
{
    public class TestCommandManager
    {
        static TestCommandManager sInstance;

        public static TestCommandManager get
        {
            get {
                if (sInstance == null)
                    sInstance = new TestCommandManager();
                return sInstance;
            }
        }

        public List<Dictionary<string, object>> commandList = new List<Dictionary<string, object>>();

        public void Reset()
        {
            commandList.Clear();
        }

        public void AddCommand(Dictionary<string, object> cmd)
        {
            lock (commandList)
            {
                commandList.Add(cmd);
            }
        }

        public void CommandHandler(Simulator host)
        {
            Dictionary<string, object> command = null;
            lock (commandList)
            {
                if (commandList.Count > 0)
                {
                    command = commandList[0];
                    commandList.RemoveAt(0);
                }
            }

            if (command != null)
            {
                string cmd = (string)command["cmd"];
                if (cmd == "spawnCommander")
                {
                    int userIdx = (int)command["userIdx"];
                    int commanderNo = (int)command["commanderNo"];
                    int commanderLevel = (int)command["commanderLevel"];
                    int directionY = (int)command["directionY"];
                    Coord2 coord = (Coord2)command["coord"];
                    string troopName = (string)command["troopName"];
                    string weaponName = (string)command["weaponName"];

                    SpawnCommander(host, userIdx, commanderNo, commanderLevel, directionY, coord, troopName, weaponName);
                }
                else if (cmd == "killAll")
                {
                    KillCommanders(host, 0);
                    KillCommanders(host, 1);
                }
            }
        }

        static int GetEmptyCommanderIdx(Simulator host, int userIdx)
        {
            foreach (var user in host.aUsers)
            {
                if (user == null)
                    continue;

                if (user.aUserIdx == userIdx)
                {
                    for (int i=0; i<BattleUserInfo.cMaxCommanderCount; i++)
                    {
                        if (user.aCommanders[i] == null)
                            return i;
                    }
                }
            }

            return -1;
        }

        public static void KillCommanders(Simulator host, int userIdx)//, int commanderIdx)
        {
            foreach (var user in host.aUsers)
            {
                if (user == null)
                    continue;

                if (user.aUserIdx == userIdx)
                {
                    for (int i = 0; i < BattleUserInfo.cMaxCommanderCount; i++)
                    {
                        if (user.aCommanders[i] != null)
                        {
                            for (int troop = 0; troop < CommanderTroopSet.cMaxTroopCount; troop ++)
                            {
                                if (user.aCommanders[i].aTroops[troop] != null)
                                    user.aCommanders[i].aTroops[troop].Destroy();
                            }
                            user.aAiManager.RemoveCommander(user.aCommanders[i]);
                            user.aCommanders[i] = null;
                        }
                    }
                }
            }
        }

        public static BattleCommander SpawnCommander(Simulator host, int userIdx, int commanderNo, int commanderLevel, int directionY, Coord2 coord, string troopName = null, string weaponName = null)
        {
            int commanderInstIdx = GetEmptyCommanderIdx(host, userIdx);
            if (commanderInstIdx == BattleUserInfo.cMaxCommanderCount || commanderInstIdx == -1)
                return null;

            var commanderItem = new CommanderItem(commanderInstIdx);

            var commanderSpec = CommanderTable.get.FindCommanderSpec(commanderNo);
            for (int i = 0; i < CommanderSpec.cRankCount; ++i)
            {
                for (int troop = 0; troop < CommanderTroopSet.cMaxTroopCount; troop++)
                {
                    var troopInfo = commanderSpec.mRankTroopSets[i].mTroopInfos[troop];

                    if (troopInfo != null)
                    {
                        if (troopName != null)
                            troopInfo.mTroopSpec = TroopTable.get.FindTroopSpec(troopName);

                        if (weaponName != null)
                        {
                            for (int weapon = 0; weapon < TroopSpec.cWeaponCount; weapon++)
                            {
                                if (troopInfo.mTroopSpec.mWeaponSpecs[weapon] != null)
                                    troopInfo.mTroopSpec.mWeaponSpecs[weapon] = WeaponTable.get.FindWeaponSpec(weaponName);
                            }
                        }
                    }
                }
            }

            commanderItem.mCommanderLevel = commanderLevel;
            commanderItem.Init(CommanderItemType.TestCommander, commanderSpec, commanderInstIdx, 1, 0, 1);
            commanderItem.mCommanderLevel = commanderLevel;
            return host.aBattleGrid.SpawnCommander(host.aBattleGrid.aUsers[userIdx], commanderItem, (Coord2)coord, directionY, host.aBattleGrid.aUsers[userIdx]);
        }
    }
}
