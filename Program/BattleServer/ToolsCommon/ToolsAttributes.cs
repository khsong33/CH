﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToolsCommon
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class NumericEditDeltaAttribute : Attribute
    {
        public float delta;

        public NumericEditDeltaAttribute(float delta)
        {
            this.delta = delta;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class CustomAttribute : Attribute
    {
        public string type;

        public CustomAttribute(string type)
        {
            this.type = type;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class ToolTipAttribute : Attribute
    {
        public string toolTip;

        public ToolTipAttribute(string toolTip)
        {
            this.toolTip = toolTip;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DummyAttribute : Attribute
    {
        public string dummyName;

        public DummyAttribute(string dummyName)
        {
            this.dummyName = dummyName;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class DynamicListKeyAttribute : Attribute
    {
        public string key;

        public DynamicListKeyAttribute(string key)
        {
            this.key = key;
        }
    }

    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class HiddenAttribute : Attribute
    {
        public HiddenAttribute()
        {
        }
    }
}
