﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Reflection;

namespace ToolsCommond
{
    public class Util
    {
        //public static void ShowException(Exception e)
        //{
        //    if (e.InnerException != null)
        //        System.Windows.MessageBox.Show(e.Message + "\n\n" + e.InnerException.ToString());
        //    else
        //        System.Windows.MessageBox.Show(e.Message);
        //}

        public static void Serialize(string filename, XmlSerializer serializer, object obj)
        {
            try
            {
                StreamWriter streamWriter = new StreamWriter(filename);
                serializer.Serialize(streamWriter, obj);
                streamWriter.Close();
            }
            catch
            {
                //ShowException(e);
            }
        }

        public static object Deserialize(string filename, XmlSerializer serializer, bool silent = false)
        {
            if (File.Exists(filename) == false)
                return null;

            StreamReader stream = null;
            try
            {
                stream = new StreamReader(filename);
                System.Xml.XmlReader reader = System.Xml.XmlReader.Create(stream);
                XmlDeserializationEvents dsevt = new XmlDeserializationEvents();

                object result = serializer.Deserialize(reader, dsevt);
                stream.Close();
                return result;
            }
            catch
            {
                //if (!silent)
                //    ShowException(e);

                if (stream != null)
                    stream.Close();
            }
            return null;
        }

        public static void Write(BinaryWriter bw, object obj)
        {
            if (obj.GetType() == typeof(String))
            {
                string str = obj as string;
                var encoding = new System.Text.UTF8Encoding();
                int len = encoding.GetByteCount(str);

                bw.Write((ushort)len);
                if (str.Length != 0)
                    bw.Write(encoding.GetBytes(str), 0, len);
            }
            else if (obj.GetType() == typeof(Boolean))
                bw.Write((Boolean)obj);
            else if (obj.GetType() == typeof(Char))
                bw.Write((Char)obj);
            else if (obj.GetType() == typeof(Byte))
                bw.Write((Byte)obj);
            else if (obj.GetType() == typeof(Boolean))
                bw.Write((Boolean)obj);
            else if (obj.GetType() == typeof(Int16))
                bw.Write((Int16)obj);
            else if (obj.GetType() == typeof(UInt16))
                bw.Write((UInt16)obj);
            else if (obj.GetType() == typeof(Int32))
                bw.Write((Int32)obj);
            else if (obj.GetType() == typeof(UInt32))
                bw.Write((UInt32)obj);
            else if (obj.GetType() == typeof(Single))
                bw.Write((Single)obj);
            else if (obj.GetType() == typeof(Double))
                bw.Write((Double)obj);
            else if (obj.GetType().IsEnum)
                bw.Write((Int32)obj);
            else
                throw new Exception("게임데이터 저장시 지원되지 않는 타입이 있음");
        }
    }
}
