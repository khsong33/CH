﻿using System;
using System.Linq;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;
using ToolsCommon;

namespace UnitTest_BattleLogic
{
    [TestClass]
    public class BattleTest
    {
        static BattleLogic battleLogic = null;
        static FakeUserInfo[] fakeUserInfos;
        static FakeMatchInfo fakeMatchInfo;
        static Simulator simulator;
        static FakeBattleMatchInfo matchInfo;

        private static TestContext context;


        [ClassInitialize]
        public static void ClassInitialize(TestContext testContext)
        {
            context = testContext;

            TestCommandManager.get.Reset();
            //IStage lStage = new BattleVerifyStage();
            battleLogic = new BattleLogic();

            fakeMatchInfo = new FakeMatchInfo();
            fakeUserInfos = new FakeUserInfo[2];
            fakeUserInfos[0] = new FakeUserInfo() { teamIdx = 1, hqGuid = 1 };
            fakeUserInfos[1] = new FakeUserInfo() { teamIdx = 2, hqGuid = 2 };

            simulator = new Simulator(battleLogic);
            matchInfo = new FakeBattleMatchInfo();

            matchInfo.FakeMatchInfo(fakeMatchInfo, fakeUserInfos);
            simulator.StartSimulation(matchInfo);
        }

        [ClassCleanup]
        public static void ClassCleanup()
        {
        }


        [TestCleanup]
        public void TestCleanup()
        {
        }

        [TestInitialize]
        public void TestInitialize()
        {
        }

        [TestMethod]
        public void Test_AllCommandersHasCorrectTroopSetAndWeapon()
        {
            for (int i = 0; i < CommanderTable.get.GetSpecCount(); i++)
            {
                var comander = CommanderTable.get.GetCommanderSpecByIdx(i);

                for (int rank = 0; rank < CommanderSpec.cRankCount; rank++)
                {
                    if (comander.mRankTroopSets[rank] == null) continue;

                    for (int troopIdx = 0; troopIdx < comander.mRankTroopSets[rank].mTroopCount; troopIdx++)
                    {
                        var troopInfo = comander.mRankTroopSets[rank].mTroopInfos[troopIdx];
                        Assert.AreNotEqual(null, troopInfo, "TroopInfo is null - " + comander.mRankTroopSets[rank].mName + " #" + i);
                        Assert.AreNotEqual(null, troopInfo.mTroopSpec, "TroopSpec is null - " + comander.mRankTroopSets[rank].mName + " #" + i);

                        for (int weaponIdx = 0; weaponIdx < TroopSpec.cWeaponCount; weaponIdx++)
                            Assert.AreNotEqual(null, troopInfo.mTroopSpec.mWeaponSpecs[TroopSpec.cPrimaryWeaponIdx], " #" + i);
                    }

                }
            }
        }

        [TestMethod]
        public void Test_SpawnAllCommanders_Ok()
        {
            for (int i = 0; i < CommanderTable.get.GetSpecCount(); i++)
            {
                var comander = CommanderTable.get.GetCommanderSpecByIdx(i);

                for (int rank = 0; rank < CommanderSpec.cRankCount; rank++)
                {
                    if (comander.mRankTroopSets[rank] == null) continue;

                    for (int troopIdx = 0; troopIdx < comander.mRankTroopSets[rank].mTroopCount; troopIdx++)
                    {
                        var commander = TestCommandManager.SpawnCommander(simulator, 0, 101, 0, new Coord2(10000, 10000));

                        Assert.AreNotEqual(null, commander, "No Commander #" + i);
                        TestCommandManager.KillCommanders(simulator, 0);
                    }
                }
            }
        }
        const int baseX = 10000;
        const int baseZ = 1000;
        [TestMethod]
        public void Test_GetAttackResult()
        {
            ToolsCommon.AnyCSVTable battleTestList;

            battleTestList = new ToolsCommon.AnyCSVTable();
            battleTestList.Read(@"..\..\hittest.csv", new List<string> { "name" });

            for (int i = 0; i < battleTestList.Count; i++)
            {
#if BATTLE_VERIFY_TEST
                BattleLogicTestStatistics.Reset();
                const int cAttack = 0;
                const int cTarget = 1;

                var id = battleTestList.GetInt(i, "No");
                var weapon = battleTestList.Get(i, "Weapon");
                var distance = battleTestList.GetInt(i, "TargetDistance");
                var targetTroopName = battleTestList.Get(i, "TargetTroop");

                var troopCoords = new List<Coord2>() { new Coord2(baseX, baseZ), new Coord2(baseX, baseZ + distance) };
                var userIndexes = new List<int>() { 0, 1 };
                var commanderNos = new List<int>() { 101, 103 }; // 테스트에 적합하다고 알려진 커맨더. 번호를 조절할 필요는 있을 듯
                var angles = new List<int>() { 0, 180 };

                Assert.AreNotEqual(null, WeaponTable.get.FindWeaponSpec(weapon), " #" + i);

                // 커맨더를 생성하고. (트룹까지 생성이 되겠지.)
                var commanders = new List<BattleCommander>() {
                    TestCommandManager.SpawnCommander(simulator, userIndexes[cAttack], commanderNos[cAttack], angles[cAttack], troopCoords[cAttack], null, weapon),
                    TestCommandManager.SpawnCommander(simulator, userIndexes[cTarget], commanderNos[cTarget], angles[cTarget], troopCoords[cTarget], targetTroopName, weapon)};
                Assert.AreNotEqual(null, commanders[cAttack], "No Attack Commander #" + i);
                Assert.AreNotEqual(null, commanders[cTarget], "No Target Commander #" + i);

                Assert.AreEqual(baseZ + distance, commanders[cTarget].aCoord.z, "타겟이 정상 위치에 생성 안됨 (맵이 작아서?) #" + i);

                List<BattleTroop> troops = new List<BattleTroop>();
                for (int c = 0; c < 2; c++)
                {
                    int actualTroops = 0;
                    for (int troopIdx = 0; troopIdx < commanders[c].aTroops.Length; troopIdx++)
                    {
                        if (commanders[c].aTroops[troopIdx] != null)
                            actualTroops++;
                    }
                    Assert.AreEqual(1, actualTroops, " #" + i);

                    troops.Add(commanders[c].aTroops[0]);
                }


                // 색적 실패를 막기위해 넓은 시야를 부여하고
                troops[cAttack].Test_ModifyStat(TroopStat.SightRange, 30000);
                troops[cAttack].UpdateDetection();

                // 색적, 타겟을 얻어온다.
                var actuallTarget = troops[cAttack].SearchTarget(null, troops[cAttack].Test_aController.aBodyAttackControl.aTargetingInfo, troops[cAttack].aIsPrimaryAttackBody);
                if (actuallTarget == null)
                {
                    battleTestList.Set(i, "ResultHit", "색적실패");
                    continue;
                }
                //Assert.AreNotEqual(null, actuallTarget, "Search Target Failed with" + weapon + " in " + distance + " #" + i);

                // 어택컨트롤에 타겟 정보를 설정해주고
                troops[cAttack].Test_aController.aBodyAttackControl.ResetTargetTroop(actuallTarget.aTargetTroop, 0);

                // 붐!
                var fireEffect = battleLogic.CreateFireEffect(troops[cAttack].Test_aController.aBodyAttackControl.aTargetingInfo);
                
                battleTestList.Set(i, "ResultHit", fireEffect.aHitResult.ToString());
                if (fireEffect.aHitResult == TroopHitResult.GroundHit)
                {
                    fireEffect.OnActivateEffect();

                    var groundHitResult = BattleLogicTestStatistics.aItems.Where(x => x.troopGuid == troops[cTarget].aGuid && x.type == "GroundHitResult").ToList();
                    if (groundHitResult.Count == 0)
                        battleTestList.Set(i, "GroundHitResult", TroopHitResult.Miss.ToString());
                    else
                        battleTestList.Set(i, "GroundHitResult", groundHitResult[0].value.ToString());

                    battleTestList.Set(i, "MaxAoeRadius", fireEffect.aFireWeaponData.aMaxAoeRadius.ToString());
                    battleTestList.Set(i, "UnitRadius", troops[cTarget].aTroopSpec.mUnitRadius.ToString());
                }


                // 결과를 뽑아서 기록하자.
                var resultAccuracyList = BattleLogicTestStatistics.aItems.Where(x => x.troopGuid == troops[cAttack].aGuid && x.type == "ResultAccuracy").ToList();
                if (resultAccuracyList.Count != 0)
                    battleTestList.Set(i, "ResultAccuracy", resultAccuracyList[0].value.ToString());

                var resultScatterAngleList = BattleLogicTestStatistics.aItems.Where(x => x.troopGuid == troops[cAttack].aGuid && x.type == "ResultScatterAngle").ToList();
                if (resultScatterAngleList.Count != 0)
                    battleTestList.Set(i, "ResultScatterAngle", resultScatterAngleList[0].value.ToString());

                var resultScatterDistanceList = BattleLogicTestStatistics.aItems.Where(x => x.troopGuid == troops[cAttack].aGuid && x.type == "ResultScatterDistance").ToList();
                if (resultScatterDistanceList.Count != 0)
                    battleTestList.Set(i, "ResultScatterDistance", resultScatterDistanceList[0].value.ToString());

                int targetBulletDistance = (fireEffect.aHitCoord - troopCoords[cTarget]).SqrMagnitude();
                battleTestList.Set(i, "TargetBulletDistance", targetBulletDistance.ToString());

                for (int u = 0; u < 2; u++)
                    TestCommandManager.KillCommanders(simulator, userIndexes[u]);
#endif
            }

            battleTestList.Write(@"../../../TestResults/" + DateTime.Now.ToShortDateString() + " " + DateTime.Now.Hour + "." + DateTime.Now.Minute + "." + DateTime.Now.Second + " TestResult.csv");
        }
    }
}
