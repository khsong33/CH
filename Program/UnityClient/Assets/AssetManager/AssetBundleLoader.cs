using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
#if UNITY_EDITOR
	using UnityEditor;
#endif

public class AssetBundleLoader : MonoBehaviour
{
	[Serializable]
	public class AssetBundleInfo
	{
		public String vName;
		public String vProjectFolder;
		public String vAssetFileExtension;
		public String vUrl;
		public int vVersion;

		public override string ToString()
		{
			return string.Format("AssetBundle<Name={0} Url={1} vVersion={2}>", vName, vUrl, vVersion);
		}

		public String GetAssetEditorPath(String pAssetPath)
		{
			String lAssetEditorPath = vProjectFolder + pAssetPath + vAssetFileExtension;
			return lAssetEditorPath;
		}
	}

	public delegate void OnLoadingDelegate(AssetBundleLoader pAssetBundleLoader, int pCurrentBundleId, int pMaxBundleId, WWW pDownloadWWW);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public AssetBundleInfo[] vAssetBundleInfos;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 에셋 번들 번호 사전
	public Dictionary<String, int> aAssetBundleIdDictionary
	{
		get { return mAssetBundleIdDictionary; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 번들에서 읽지 않고 리소스에서 읽도록 하는 플래그
	public bool aIsResourceMode
	{
		get { return mIsResourceMode; }
		set { mIsResourceMode = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestroy()
	{
		for (int iBundle = 0; iBundle < mAssetBundles.Length; ++iBundle)
			if (mAssetBundles[iBundle] != null)
			{
				mAssetBundles[iBundle].Unload(false);
				mAssetBundles[iBundle] = null;
			}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화를 합니다.
	public void SetUp(bool pIsResourceMode)
	{
		if (mAssetBundles != null) // 중복 초기화 검사
			return;
	
		mAssetBundles = new AssetBundle[vAssetBundleInfos.Length];
		mAssetBundleIdDictionary = new Dictionary<string, int>();

		for (int iBundle = 0; iBundle < vAssetBundleInfos.Length; ++iBundle)
			mAssetBundleIdDictionary.Add(vAssetBundleInfos[iBundle].vName, iBundle);

		mIsResourceMode = pIsResourceMode;
		mIsDownloading = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로딩합니다.
	public IEnumerator Load(String pFileServerUrl, OnLoadingDelegate pOnLoadingDelegate, bool pIsForceBundleLoading, bool pIsCleanCache)
	{
		if (pOnLoadingDelegate != null)
			pOnLoadingDelegate(this, 1, vAssetBundleInfos.Length, null);
	
		for (int iBundle = 0; iBundle < vAssetBundleInfos.Length; ++iBundle)
		{
			AssetBundleInfo lAssetBundleInfo = vAssetBundleInfos[iBundle];

			// 캐싱 시스템을 준비합니다.
			while (!Caching.ready)
				yield return null;

			// 번들을 다운로드할 주소를 얻습니다.
			String lServerUrl;
			mIsForceBundleLoading = pIsForceBundleLoading;
		#if UNITY_EDITOR
			if (!pIsForceBundleLoading)
			{
				pOnLoadingDelegate(this, vAssetBundleInfos.Length, vAssetBundleInfos.Length, null); // 바로 완료
				yield break;
			}
			else
				lServerUrl = pFileServerUrl;
		#else
			lServerUrl = pFileServerUrl;
		#endif
			String lDownloadUrl = lServerUrl + lAssetBundleInfo.vUrl;
			//Debug.Log("lDownloadUrl=" + lDownloadUrl + " lServerUrl=" + lServerUrl);

			// 필요할 경우 기존에 받았던 정보를 삭제합니다.
			if (pIsCleanCache)
				Caching.CleanCache();
			
			// 비동기로 번들을 받습니다.
			StartCoroutine(_LoadFromCacheOrDownload(
				iBundle,
				lDownloadUrl,
				lAssetBundleInfo.vVersion));
			
			// 번들을 받는 동안 콜백 함수를 호출합니다.
			do
			{
				if (pOnLoadingDelegate != null)
					pOnLoadingDelegate(
						this,
						iBundle + 1,
						vAssetBundleInfos.Length,
						mCurrentWWW);
				yield return null;
			} while (mIsDownloading); // 최소 한 번은 콜백이 호출되도록 do-while 문을 사용했습니다.
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋을 로딩합니다.
	public T_Object Load<T_Object>(int pBundleId, String pAssetPath) where T_Object : UnityEngine.Object
	{
		T_Object lObject;
		if (!mIsResourceMode &&
			(pBundleId >= 0))
		{
#if UNITY_EDITOR
		    if (!mIsForceBundleLoading)
		    {
		        // 번들에서 읽지 않고 에디터 안의 개별 에셋을 직접 읽습니다.
		        String lAssetEditorPath = vAssetBundleInfos[pBundleId].GetAssetEditorPath(pAssetPath);
		        //Debug.Log("lAssetEditorPath=" + lAssetEditorPath);
		        lObject = AssetDatabase.LoadAssetAtPath(lAssetEditorPath, typeof(T_Object)) as T_Object;
		    }
		    else
		    {
		        // 에디터 모드이지만 강제로 번들에서 읽습니다.
				lObject = mAssetBundles[pBundleId].LoadAsset(pAssetPath, typeof(T_Object)) as T_Object;
		    }
#else
			lObject = mAssetBundles[pBundleId].LoadAsset(pAssetPath, typeof(T_Object)) as T_Object;
#endif
		}
		else
		{
			if (pBundleId >= 0)
				lObject = Resources.Load(vAssetBundleInfos[pBundleId].vName + "/" + pAssetPath, typeof(T_Object)) as T_Object;
			else
				lObject = Resources.Load(pAssetPath, typeof(T_Object)) as T_Object;
		}		
		return lObject;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 캐쉬에서 읽거나 다운로드 받습니다.
	private IEnumerator _LoadFromCacheOrDownload(int pBundleId, String pDownloadUrl, int pVersion)
	{
		mIsDownloading = true;

		using (WWW www = WWW.LoadFromCacheOrDownload(pDownloadUrl, pVersion))
		{
			mCurrentWWW = www;
		
			yield return www;
			if (www.error != null)
				throw new Exception("WWW download had an error:" + www.error);

			mAssetBundles[pBundleId] = www.assetBundle;

			mCurrentWWW = null;

			www.Dispose();
		}
		
		mIsDownloading = false;
	}	

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private AssetBundle[] mAssetBundles;
	private Dictionary<String, int> mAssetBundleIdDictionary;
	
	private bool mIsResourceMode;
	private bool mIsDownloading;
	private bool mIsForceBundleLoading;
	private WWW mCurrentWWW;

}
