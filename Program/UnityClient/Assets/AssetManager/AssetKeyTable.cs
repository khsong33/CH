using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AssetKeyTable
{
	public readonly String vAssetKeyNameColumnName = "AssetKey";
	public readonly String vAssetBundleColumnName = "AssetBundle";
	public readonly String vAssetPathColumnName = "AssetPath";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public AssetKeyTable()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(String pCSVTextAssetBundle, String pCSVTextAssetPath, bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(pCSVTextAssetBundle, pCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lAssetKeyName;
			if (!lCSVRow.TryStringValue(vAssetKeyNameColumnName, out lAssetKeyName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 애셋 키 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lAssetKeyName))
			{
				Debug.LogError(String.Format("AssetKeyTable[{0}] duplicate Name", lAssetKeyName));
				continue;
			}
			mNameToRowDictionary.Add(lAssetKeyName, iRow);
		}

		// 검색 테이블을 초기화합니다.

		// 애셋 키 테이블을 초기화합니다.
		mAssetKeys = new AssetKey[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllAssetKey();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllAssetKey()
	{
		Debug.LogWarning("LoadAllAssetKey() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mAssetKeys[iRow] == null) // _SetUpSpawnLinkTroopInfo() 속에서 초기화되었을 수도 있습니다.
				mAssetKeys[iRow] = _LoadAssetKey(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 애셋 키를 찾습니다.
	public AssetKey FindAssetKey(String pAssetKeyName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pAssetKeyName, out lRowIdx))
		{
			if (mAssetKeys[lRowIdx] == null)
				mAssetKeys[lRowIdx] = _LoadAssetKey(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mAssetKeys[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find asset key name(" + pAssetKeyName + ") - " + this);
			return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("AssetKeyTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 애셋 키를 로딩합니다.
	private AssetKey _LoadAssetKey(CSVRow pCSVRow, int pRowIdx)
	{
		// AssetBundle, AssetPath
		AssetKey lAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vAssetBundleColumnName),
			pCSVRow.GetStringValue(vAssetPathColumnName));

		// 파생 정보를 초기화합니다.
// 		_InitExtraInfo(lAssetKey);

		return lAssetKey;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
// 	private void _InitExtraInfo(AssetKey pAssetKey)
// 	{
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CSVObject mCSVObject;
	private Dictionary<String, int> mNameToRowDictionary;
	private AssetKey[] mAssetKeys;
}
