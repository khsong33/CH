using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[Serializable]
public class AssetInfo
{
	public String vAssetPath;
	public String vBundleName;

	public AssetInfo()
	{
	}
	public AssetInfo(String pAssetPath, String pBundleName)
	{
		vAssetPath = pAssetPath;
		vBundleName = pBundleName;
	}
	public static bool IsValid(AssetInfo pAssetInfo)
	{
		if ((pAssetInfo != null) &&
			!String.IsNullOrEmpty(pAssetInfo.vAssetPath))
			return true;
		else
			return false;
	}
}

public class AssetKey : System.Object
{
	internal AssetKey(int pBundleId, String pAssetPath)
	{
		mBundleId = pBundleId;
		mAssetPath = pAssetPath;
	}

	internal int mBundleId;
	internal String mAssetPath;

	public String aAssetPath
	{
		get { return mAssetPath; }
	}

	public override string ToString()
	{
		return String.Format("AssetKey<BundleId={0} AssetPath={1}>", mBundleId, mAssetPath);
	}

	public static bool IsValid(AssetKey pAssetKey)
	{
		if ((pAssetKey != null) &&
			!String.IsNullOrEmpty(pAssetKey.mAssetPath))
			return true;
		else
			return false;
	}

	//public static bool operator ==(AssetKey lhs, AssetKey rhs)
	//{
	//	bool status = false;
	//	if (lhs.mBundleId == rhs.mBundleId && lhs.mAssetPath == rhs.mAssetPath)
	//	{
	//		status = true;
	//	}
	//	return status;
	//}
	//public static bool operator !=(AssetKey lhs, AssetKey rhs)
	//{
	//	bool status = false;
	//	if (lhs.mBundleId != rhs.mBundleId || lhs.mAssetPath != rhs.mAssetPath)
	//	{
	//		status = true;
	//	}
	//	return status;
	//}

}

public class AssetManager : MonoBehaviour
{
	public enum FileServerType
	{
		Ftp,
		Ftp_test,
		Local,
		Local_test,
	}

	public const String sCacheVersionKey = "vCacheVersion";

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public AssetBundleLoader vAssetBundleLoader;

	public String vFtpFileServerUrl = "http://mch.devmdl.pmang.com/mch/AssetBundles0/";
	public String vFtpFileServerUrl_test = "http://mch.devmdl.pmang.com/mch/AssetBundles0_test/";
	public String vLocalFileServerUrl = "/../AssetBundles/";
	public String vLocalFileServerUrl_test = "/../AssetBundles_test/";
	public FileServerType vFileServerType = FileServerType.Ftp;
	public bool vIsForceBundleLoading = false;
	public bool vIsCleanCache = false;
	public bool vIsResourceMode = false;
	
	public int vCacheVersion = 1;
	
	// 개발:	http://mch.devmdl.pmang.com/mch/AssetBundles0
	// QA:		http://mch.dqmdl.pmang.com/mch/AssetBundles0
	// 라이브:	http://mch.mdl.pmang.com/mch/AssetBundles0

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static AssetManager get
	{
		get
		{
			if (sInstance == null)
				CreateSingletonObject();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vAssetBundleLoader == null)
			Debug.LogError("vAssetBundleLoader is empty. - " + this);

		// 싱글톤 처리를 합니다.
		if (sInstance == null)
		{
			OnCreate();
		}
		else if (sInstance != this)
		{
			DestroyObject(gameObject); // 기존 객체가 있으므로 이 객체는 삭제합니다.
			return;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성 콜백
	void OnCreate()
	{
		//Debug.Log("OnCreate() - " + this);

		// 싱글톤을 등록합니다.
		sInstance = this;
		if (Application.isPlaying)
			DontDestroyOnLoad(gameObject); // 이후로 파괴하지 않습니다.

		// 에셋 번들 로더를 구축합니다.
		vAssetBundleLoader.SetUp(vIsResourceMode);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingletonObject()
	{
		if (sInstance == null)
		{
			GameObject lSingletonRoot = GameObject.Find("_Singletons");
			if (lSingletonRoot == null)
			{
				lSingletonRoot = new GameObject("_Singletons");
				DontDestroyOnLoad(lSingletonRoot);
			}
			else
				sInstance = ObjectUtil.FindChild<AssetManager>(lSingletonRoot.transform, false);

			if (sInstance == null) // ObjectUtil.FindChild<AssetManager>()로도 못찾았다면 생성합니다.
			{
				sInstance = ResourceUtil.InstantiateComponent<AssetManager>(Config.get.GetStringValue("AssetManagerPath"));
				sInstance.transform.parent = lSingletonRoot.transform;
			}
			else
				sInstance.OnCreate();

// 			if (!Application.isPlaying) // Editor 실행시에는 Awake()가 아직 호출이 안되므로 명시적으로 호출합니다.
// 				sInstance.OnCreate();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 로딩합니다.
	public IEnumerator LoadAssetBundles(AssetBundleLoader.OnLoadingDelegate pOnLoadingDelegate)
	{
		//Debug.Log("LoadAssetBundles() - " + this);
	
		String lFileServerUrl;
		switch (vFileServerType)
		{
		case FileServerType.Ftp:
			lFileServerUrl = vFtpFileServerUrl;
			break;
		case FileServerType.Ftp_test:
			lFileServerUrl = vFtpFileServerUrl_test;
			break;
		case FileServerType.Local:
			lFileServerUrl = "file://" + Application.dataPath + vLocalFileServerUrl;
			break;
		case FileServerType.Local_test:
			lFileServerUrl = "file://" + Application.dataPath + vLocalFileServerUrl_test;
			break;
		default:
			yield break;
// 			break;
		}
		//Debug.Log("lFileServerUrl=" + lFileServerUrl);
		
		// 캐쉬 버전을 비교해서 삭제합니다.
		int lCacheVersion = PlayerPrefs.GetInt("sCacheVersionKey", 0);
		if (lCacheVersion != vCacheVersion)
		{
			Caching.CleanCache();
			PlayerPrefs.SetInt("sCacheVersionKey", vCacheVersion);
		}
		
		// 에셋 번들을 로딩합니다.
		if (!vIsResourceMode)
			yield return StartCoroutine(vAssetBundleLoader.Load(
				lFileServerUrl,
				pOnLoadingDelegate,
				vIsForceBundleLoading,
				vIsCleanCache));
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋 키를 생성합니다.
	public AssetKey CreateAssetKey(String pAssetBundleName, String pAssetPath)
	{
		int lAssetBundleId;
		//Debug.Log("vAssetBundleLoader=" + vAssetBundleLoader);
		//Debug.Log("vAssetBundleLoader.aAssetBundleIdDictionary=" + vAssetBundleLoader.aAssetBundleIdDictionary);
		if (!vAssetBundleLoader.aAssetBundleIdDictionary.TryGetValue(pAssetBundleName, out lAssetBundleId))
		{
			Debug.LogError("invalid asset bundle name:" + pAssetBundleName + " for " + pAssetPath);
			return null;
		}
		
		AssetKey lAssetKey = new AssetKey(lAssetBundleId, pAssetPath);
		return lAssetKey;
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋 키를 생성합니다.
	public AssetKey CreateAssetKey(AssetInfo pAssetInfo)
	{
		return CreateAssetKey(pAssetInfo.vBundleName, pAssetInfo.vAssetPath);
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋을 로딩한 다음에 반환합니다.
	public T_Object Load<T_Object>(AssetKey pAssetKey) where T_Object : UnityEngine.Object
	{
		T_Object lObject = vAssetBundleLoader.Load<T_Object>(pAssetKey.mBundleId, pAssetKey.mAssetPath);
		if (lObject == null)
		{
			Debug.LogError("can't load asset " + pAssetKey + " at " + _GetBundleName(pAssetKey));
			return null;
		}

		return lObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋을 로딩한 다음에 반환합니다.
	public String LoadText(AssetKey pAssetKey)
	{
		return Asset.LoadText(vAssetBundleLoader.vAssetBundleInfos[pAssetKey.mBundleId].vName, pAssetKey.mAssetPath);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 읽은 프리팹으로 게임 오브젝트를 생성한 다음에 반환합니다.
	public T_Object InstantiateObject<T_Object>(AssetKey pAssetKey) where T_Object : UnityEngine.Object
	{
		UnityEngine.Object lPrefabObject = vAssetBundleLoader.Load<UnityEngine.Object>(pAssetKey.mBundleId, pAssetKey.mAssetPath);
		if (lPrefabObject == null)
		{
			Debug.LogError("can't load asset " + pAssetKey + " at " + _GetBundleName(pAssetKey));
			return null;
		}

		T_Object lObject = UnityEngine.Object.Instantiate(lPrefabObject) as T_Object;
		if (lObject == null)
		{
			Debug.LogError(String.Format("can't instantiate asset {0} as {1} at {2}", pAssetKey, typeof(T_Object).Name, _GetBundleName(pAssetKey)));
			return null;
		}

		return lObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 읽은 프리팹으로 게임 오브젝트를 생성한 다음에 자식으로 지정하고 반환합니다.
	public Transform InstantiateChildObject(AssetKey pAssetKey, Transform pParent)
	{
		GameObject lPrefabObject = vAssetBundleLoader.Load<GameObject>(pAssetKey.mBundleId, pAssetKey.mAssetPath);
		if (lPrefabObject == null)
		{
			Debug.LogError("can't load asset " + pAssetKey + " at " + _GetBundleName(pAssetKey));
			return null;
		}

		GameObject lObject = UnityEngine.Object.Instantiate(lPrefabObject) as GameObject;
		if (lObject == null)
		{
			Debug.LogError(String.Format("can't instantiate asset {0} as {1} at {2}", pAssetKey, typeof(GameObject).Name, _GetBundleName(pAssetKey)));
			return null;
		}
		
		Transform lObjectTransform = lObject.transform;
		lObjectTransform.parent = pParent;
		lObjectTransform.localPosition = lPrefabObject.transform.localPosition;

		return lObjectTransform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 읽은 프리팹으로 게임 오브젝트를 생성한 다음에 해당 콤포넌트를 찾아 반환합니다.
	public T_Component InstantiateComponent<T_Component>(AssetKey pAssetKey) where T_Component : Component
	{
		UnityEngine.Object lPrefabObject = vAssetBundleLoader.Load<UnityEngine.Object>(pAssetKey.mBundleId, pAssetKey.mAssetPath);
		if (lPrefabObject == null)
		{
			Debug.LogError("can't load asset " + pAssetKey + " at " + _GetBundleName(pAssetKey));
			return null;
		}

		GameObject lGameObject = UnityEngine.Object.Instantiate(lPrefabObject) as GameObject;
		if (lGameObject == null)
		{
			Debug.LogError(String.Format("can't instantiate asset {0} as {1} at {2}", pAssetKey, typeof(GameObject).Name, _GetBundleName(pAssetKey)));
			return null;
		}

		T_Component lComponent = lGameObject.GetComponent<T_Component>();
		if (lComponent == null)
		{
			Debug.LogError(String.Format("can't find component {0} in asset object {1} at {2}", typeof(T_Component).Name, pAssetKey, _GetBundleName(pAssetKey)));
			return null;
		}

		return lComponent;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 로그 출력용 번들 이름을 얻습니다.
	private String _GetBundleName(AssetKey pAssetKey)
	{
		if (pAssetKey.mBundleId >= 0)
			return vAssetBundleLoader.vAssetBundleInfos[pAssetKey.mBundleId].ToString();
		else
			return "'Resources'";
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static AssetManager sInstance = null;
}
