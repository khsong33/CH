﻿/*
	File:		AtlasMagick.cs

	Contains	EditorWindow for Main Window
	Version	    1.1


	Dependencies  	Unity3d v 4.3.4 Free
                    AtlasMagickHelper.cs
		            MagickCreator.cs
		            AtlasMagickMerge.cs
					

	File Ownership  Levon Marcus Ravel
	Writer          Levon Marcus Ravel
	Contact         LevonRavel@outlook.com
	Copyright	2014 by 5TillFalling Inc., all rights reserved



	Change History (most recent first):

	Initial Version
		
*/

using UnityEditor;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;

public class AtlasMagick : EditorWindow{
	[MenuItem ("Window/Atlas Magick")]
	static void Init () {
		EditorWindow.GetWindow (typeof (AtlasMagick));
	}

	//Singles
	public Material _selectedMaterial;
	public GameObject _go;
	public string _goNameO = "initialize";
	
	//Lists
	public List<string> _gosName = new List<string>();
	public List<string> _textureNames = new List<string> ();
	
	//Lists extended information
	public List<MeshFilter> _gos = new List<MeshFilter>();
	public List<Mesh> _goMesh = new List<Mesh>();
	public List<string> _textureCoords = new List<string> ();
	
	//Indexers
	public int _textureIndex;
	public int _textureIndexO;
	public int _meshIndex;
	public int _meshIndexO;
	
	//Bools
	public bool _inital = false;
	public bool _makeStatic = false;
	
	//Get Between Method
	public static string getBetween(string strSource, string strStart, string strEnd){
		int Start, End;
		if (strSource.Contains(strStart) && strSource.Contains(strEnd)){
			Start = strSource.IndexOf(strStart, 0) + strStart.Length;
			End = strSource.IndexOf(strEnd, Start);
			return strSource.Substring(Start, End - Start);
		}else{
			return "";
		}
	}
	
	void OnGUI(){
		
		if (_textureIndex != _textureIndexO) {
			_textureIndexO = _textureIndex;
			_gos[_meshIndex].GetComponent<MeshRenderer>().sharedMaterial = _selectedMaterial;
			PreviewTexture ();
		}	
		//initialize
		
		//might need seperate mesh list
		_selectedMaterial =(Material) EditorGUILayout.ObjectField (_selectedMaterial, typeof(Material), false);
		UnityEngine.Object source = EditorGUILayout.ObjectField(_go, typeof(GameObject), true);
		_go = source as GameObject;
		_meshIndex = EditorGUILayout.Popup (_meshIndex, _gosName.ToArray ());
		_textureIndex = EditorGUILayout.Popup (_textureIndex,_textureNames.ToArray ());
		_makeStatic = EditorGUILayout.Toggle ("Make Static",_makeStatic);
		
		if(GUILayout.Button("Apply UvHelper") && _go != null){
			Undo.AddComponent<AtlasMagickHelper>(_gos[_meshIndex].gameObject).enabled = false;
			_gos[_meshIndex].gameObject.GetComponent<AtlasMagickHelper>().origMesh = _goMesh[_meshIndex];
			_gos[_meshIndex].gameObject.GetComponent<AtlasMagickHelper>().uvs = (Vector2[])_gos[_meshIndex].sharedMesh.uv.Clone ();
			_gos[_meshIndex].gameObject.GetComponent<AtlasMagickHelper>().uvMaterial = _selectedMaterial;
			if(_makeStatic == true){
			_gos[_meshIndex].GetComponent<AtlasMagickHelper>().makestatic = true;
		}
	}
		if(GUILayout.Button ("Make Prefab") && _go != null){
			if(_selectedMaterial == null){
				EditorUtility.DisplayDialog("Missing Atlas Material", "Please apply a Atlas Material", "Cancel");
				return;
			}
			foreach(MeshFilter i in _gos){
				if(i.gameObject.GetComponent<AtlasMagickHelper>() == null){
					EditorUtility.DisplayDialog("Missing AtlasMagickHelper",i.name+"-"+_meshIndex+" Is missing AtlasMagickHelper, You must press apply AtlasMagickHelper on every game object listed", "Cancel");
					return;
				}else{
					i.GetComponent<AtlasMagickHelper>().enabled = true;
				}
			}
			PrefabUtility.CreatePrefab ("Assets/AtlasMagick/AtlasPrefab/"+_go.name+_textureNames[_textureIndex]+".prefab",_go);
			Undo.DestroyObjectImmediate (_go);
		}
		if(GUILayout.Button ("Make Combine Location")){
			if(_selectedMaterial == null){
				EditorUtility.DisplayDialog("Missing Atlas Material", "Please apply a Atlas Material", "Cancel");
				return;
			}
			GameObject combineL = new GameObject();
			GameObject combineO = new GameObject();
			Undo.RegisterCreatedObjectUndo (combineL, "Combine Location");
			Undo.RegisterCreatedObjectUndo (combineO, "Combine Here");
			combineL.name = "AlasMagickInitialCombined";
			combineL.AddComponent<AtlasMagickMerge>().mat = _selectedMaterial;
			combineO.name = "PlaceHere";
			combineO.transform.parent = combineL.transform.root;
		}
		if(GUILayout.Button ("Open Atlas Creator")){
			EditorWindow.GetWindow(typeof(MagickCreator));
			
			if(_meshIndex != _meshIndexO){
				_gos[_meshIndex].GetComponent<MeshFilter>().sharedMesh = _goMesh[_meshIndex];
				_meshIndexO = _meshIndex;
				_gos[_meshIndex].GetComponent<MeshRenderer>().sharedMaterial = _selectedMaterial;
				}
			}
		if ( _go == null || _go.name != _goNameO) {

			if(_selectedMaterial == null)
				return;
			
			if(_go == null){
				_goNameO = "initialize";
			}else{_goNameO = _go.name;}
			
			_gosName.Clear ();
			_gos.Clear ();
			_goMesh.Clear ();
			_textureCoords.Clear ();
			_textureNames.Clear ();			
						
			if(_go != null && _selectedMaterial != null)
				InitialSetup();
		}
	}

	void InitialSetup(){
		
		Component[] mesh = _go.GetComponentsInChildren<MeshFilter>();
		foreach (MeshFilter i in mesh) {
			_gos.Add (i);
			_goMesh.Add (i.sharedMesh);
			_gosName.Add (i.gameObject.name+"-"+_gosName.Count);
		}
		
		string _texturePath = AssetDatabase.GetAssetPath(_selectedMaterial).Replace(".mat",".txt");
		StreamReader readAtlas = new StreamReader (_texturePath);
		string line;
		
		while ((line = readAtlas.ReadLine()) != null){
			string data = getBetween (line,"*","*");
			_textureNames.Add (data);
			string dataX = getBetween (line,"x:",",");
			string dataY = getBetween (line,"y:",",");
			string dataW = getBetween (line,"width:",",");
			string dataH = getBetween (line,"height:",")");
			_textureCoords.Add("x"+dataX+"y"+dataY+"w"+dataW+"h"+dataH+"end");
		}
		readAtlas.Close ();
	}
	
	void PreviewTexture(){
		
		float x = float.Parse (getBetween (_textureCoords[_textureIndex],"x","y"));
		float y = float.Parse (getBetween (_textureCoords[_textureIndex],"y","w"));
		float w = float.Parse (getBetween (_textureCoords[_textureIndex],"w","h"));
		float h = float.Parse (getBetween (_textureCoords[_textureIndex],"h","end"));


		Mesh _meshCopy = new Mesh ();

		foreach(var property in typeof(Mesh).GetProperties()){
			if(property.GetSetMethod() != null && property.GetGetMethod() != null){	
				property.SetValue(_meshCopy, property.GetValue(_goMesh[_meshIndex], null), null);
			}
		}	
		Vector2[] _uvOffset = _meshCopy.uv;

		for(int uv = 0; uv < _uvOffset.Length;uv++){
			_uvOffset[uv] = new Vector2 (_uvOffset[uv].x * (w) + (x),_uvOffset[uv].y *(h)+(y));
		}

		_meshCopy.uv = _uvOffset;
		_gos[_meshIndex].sharedMesh =_meshCopy;
	}
}