﻿/*
	File:		MagickCreator.cs

	Contains	EditorWindow for Main Window
	Version	    1.1


	Dependencies  	Unity3d v 4.3.4 Free

					

	File Ownership  Levon Marcus Ravel
	Writer          Levon Marcus Ravel
	Contact         LevonRavel@outlook.com
	Copyright	2014 by 5TillFalling Inc., all rights reserved



	Change History (most recent first):

	Initial Version
		
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;
using System.IO;

public class MagickCreator : EditorWindow {
	static void Init (){
		EditorWindow.GetWindow (typeof (MagickCreator));
	}
	public Texture2D normatlas;
	public Rect normrect;
	public string atlaspath;
	public List<Texture2D> specTextures = new List<Texture2D>();

	public List<string> atlasType = new List<string> ();
	public int typeIndex = 0;
	public int index = 0;
	public int atlasWidth =0;
	public int atlasHeight =0;
	public Texture2D atlas;
	public string[] coords;
	public Rect[] rect;
	public List<string> textureList = new List<string> ();
	public bool added = false;
	public int oldTypeIndex = 3;
	public bool test = false;
	public bool adjusting = false;

	void OnGUI(){
		DragAndDrop.visualMode = DragAndDropVisualMode.Generic;		
		if (Event.current.type == EventType.DragExited) {

			for (int i = 0; i < DragAndDrop.objectReferences.Length; i++) {
				if(DragAndDrop.objectReferences[i].GetType() != typeof(Texture2D)){
					EditorUtility.DisplayDialog("Invalid image type", "Please select an image.", "Cancel");
					return;
				}
				specTextures.Add ((Texture2D)DragAndDrop.objectReferences[i]);
				string path = AssetDatabase.GetAssetPath(specTextures[i]);
				TextureImporter textureImporter = AssetImporter.GetAtPath(path) as TextureImporter;
				textureImporter.isReadable = true;
				AssetDatabase.ImportAsset(path);
				AssetDatabase.Refresh ();
			}
			foreach(Texture2D txt in specTextures){
				if(!textureList.Contains (txt.name) || textureList.Count == 0){
					textureList.Add (txt.name);
				}
			}
		}
		index = EditorGUILayout.Popup(index,textureList.ToArray ());
		if(GUILayout.Button("Make Atlas"))
			SetupAtlas ();

		if(GUILayout.Button("Clear AtlasTextures")){
			textureList.Clear ();
			specTextures.Clear ();
		}
	}

	void SetupAtlas() {
		if(specTextures.Count > 64){
			EditorUtility.DisplayDialog("Exceeded safe texture limit", "If you experience quality loss remake atlas with fewer textures.", "Cancel");
		}
		atlas = new Texture2D (4096, 4096);
		rect = atlas.PackTextures(specTextures.ToArray(),0,8192);
		atlas.name = "New Atlas";
		atlas.Apply();
		AtlasCoordinatesDocument ();
	}
	void SaveAtlas(){
		// Opens a file selection dialog for a PNG file and saves a selected texture to the file.
			atlaspath = EditorUtility.SaveFilePanelInProject("Save texture as PNG",
			                                                atlas.name + ".png",
			                                                "png",
			                                                "Please enter a file name to save the texture to");
			if(atlaspath.Length != 0) {
				// Convert the texture to a format compatible with EncodeToPNG
				if(atlas.format != TextureFormat.ARGB32 && atlas.format != TextureFormat.RGB24) {
					Texture2D newTexture = new Texture2D(atlas.width, atlas.height);
					newTexture.SetPixels32(atlas.GetPixels32(0),0);
					atlas = newTexture;
				}
//first write specs
			byte[] bytes= atlas.EncodeToPNG();
			FileStream file = File.Open(atlaspath,FileMode.Create);
			BinaryWriter binary= new BinaryWriter(file);
			binary.Write(bytes);
			file.Close();
//second write norms

			//byte[] nbytes= atlas.EncodeToPNG();
			File.Copy (@atlaspath,@atlaspath.Replace (".png","Normal.png"));
			//BinaryWriter nbinary= new BinaryWriter(nfile);
			//nbinary.Write(nbytes);
			//nfile.Close();
			AssetDatabase.Refresh ();
//setup the new normal map
			string npath = atlaspath.Replace (".png","Normal.png");
			TextureImporter textureImporter = AssetImporter.GetAtPath(npath) as TextureImporter;
			textureImporter.grayscaleToAlpha = true;
			textureImporter.convertToNormalmap = true;
			textureImporter.heightmapScale = .02f;
			textureImporter.isReadable = true;
			AssetDatabase.ImportAsset(npath);

//Create Material
			Material atlasMaterial = new Material (Shader.Find("Bumped Specular"));
			AssetDatabase.CreateAsset(atlasMaterial,atlaspath.Replace (".png",".mat"));
			atlasMaterial.SetTexture ("_MainTex",(Texture)AssetDatabase.LoadAssetAtPath(atlaspath,typeof(Texture)));
			atlasMaterial.SetTexture("_BumpMap",(Texture)AssetDatabase.LoadAssetAtPath(npath,typeof(Texture)));
			File.WriteAllLines(atlaspath.Replace (".png","")+".txt",coords);
			AssetDatabase.Refresh ();
		}
	}

	void AtlasCoordinatesDocument(){
		Array.Resize (ref coords, specTextures.Count);
		for (int i = 0; i < specTextures.Count; i++) {
			coords[i]=("*"+specTextures[i].name+"*"+rect[i]);
		}
		SaveAtlas ();
	}
}