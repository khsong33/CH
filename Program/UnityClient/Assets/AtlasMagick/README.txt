Point of contact:  LevonRavel@outlook.com

Getting Started.

1.
	Click on the window pull down tab.(Find AtlasMagick click it)

2.
	Click Open MagickCreator.(Drag all textures to the new window / Release anywhere on MagickCreator window)

3.
	Once you have all textures added click Make Atlas.
	(The more textures added the longer it will take to complete, sit back and relax.)

	
	****************************************************
	** The more textures placed inside the atlas will **
	**          Decrease the texture quality          **
	****************************************************

4.
	Close MagickCreator window, Drag your new Atlas material to AtlasMagick window (None Material).

5.
	Drag a prefab from the Project View to the Hierarchy (or) create a new game object.

6.
	Drag the created game object from the hierarchy to the AtlasMagick window(None GameObject).
	The editor will go through the game object, and find all MESH FILTERS.
	You will then see two additional drop boxes, the top one is for all the mesh objects found and
	the other is for Texture selections.

7. 
	Select the game object to modify from the drop down list then select a texture below it.
		(Once you are done editing press Apply UvHelper)

	This will add a script to the game object selected, Thus allowing the uv offset.


	************************************************************
	**You must press Apply UvHelper on all meshes being edited**
	************************************************************

8.
	Once you are completely finished editing all the meshes found with in the game object.
	(Press Create Prefab your prefabs can be found at Assets/AtlasManger/AtlasPrefabs).

	********************************************
	****DO NOT DELETE OR MOVE FILE STRUCTURE****
	****You may only delete the Demo Folder ****
	**** and move prefabs that are created  ****
	********************************************
9.
	If you wish to combined meshes,Make sure the AtlasMagick window has the atlas material assigned.
	(Press Make Combine Location)
	
	**********************************************************
	** To Combine the meshes Click AtlasMagckInitialCombined**
	**  You will see PlaceHere child object drag all items  **
	** to this location then click AtlasMagckInitialCombined**
	**        In the inspector click run in editor          **
	**********************************************************

10.
	Drag all objects you wish to combine to the Combine Location Game Object.
	
	**********************************************************
	**Requirements, All objects must share the same material**
	**   Objects total combined verts cannot surpass 65k    **
	**********************************************************

	If you wish to combine more then 65k verts create another Combine Location and seperate the objects accordingly.
