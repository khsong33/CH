﻿/*
	File:		AtlasMagickHelper.cs

	Contains	EditorWindow for Main Window
	Version	    1.1


	Dependencies  	Unity3d v 4.3.4 Free
					AtlasMagic
					

	File Ownership  Levon Marcus Ravel
	Writer          Levon Marcus Ravel
	Contact         LevonRavel@outlook.com
	Copyright	2014 by 5TillFalling Inc., all rights reserved



	Change History (most recent first):

	Initial Version
		
*/

using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class AtlasMagickHelper : MonoBehaviour {
	
	public Material uvMaterial;
	public Vector2[] uvs;
	public GameObject[] objectName;
	public bool makestatic = false;
	public bool complete = false;
	public Mesh origMesh;
	public string meshLocation = "";


	// Use this for initialization
	void Start () {
		Mesh newMesh = new Mesh();
		foreach(var property in typeof(Mesh).GetProperties()){			
			if(property.GetSetMethod() != null && property.GetGetMethod() != null){				
				property.SetValue(newMesh, property.GetValue(origMesh, null), null);				
			}			
		}

		newMesh.uv = uvs;
		gameObject.GetComponent<MeshFilter> ().sharedMesh = newMesh;
		newMesh.name = "Clone: " + origMesh.name;
		gameObject.GetComponent<MeshRenderer> ().sharedMaterial = uvMaterial;

		if (makestatic != false) {
			gameObject.isStatic = true;
		}
		complete = true;
	}
	void LateUpdate(){
		if(gameObject.GetComponent<MeshFilter>() == null){
			gameObject.GetComponent<MeshFilter>().sharedMesh = origMesh;
		}
	}
}