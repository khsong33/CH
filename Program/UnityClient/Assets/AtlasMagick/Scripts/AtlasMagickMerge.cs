﻿/*
	File:		AtlasMagickMerge.cs

	Contains	EditorWindow for Main Window
	Version	    1.1


	Dependencies  	Unity3d v 4.3.4 Free
                    AtlasMagickHelper.cs



	File Ownership  Levon Marcus Ravel
	Writer          Levon Marcus Ravel
	Contact         LevonRavel@outlook.com
	Copyright		2014 by 5TillFalling Inc., all rights reserved



	Change History (most recent first):

	Initial Version
		
*/

using UnityEngine;
using System.Collections;
//using UnityEditor;
[ExecuteInEditMode]
[RequireComponent(typeof(MeshRenderer))]
[RequireComponent(typeof(MeshCollider))]

public class AtlasMagickMerge : MonoBehaviour {
	public Material mat;
	public bool runInEditor = false;
	private bool created = false;
	private GameObject _overFill;
	private GameObject _temp;
	private MeshFilter[] meshFilters;
	private int count = 0;
	private int i = 0;

	void Update() {
		if(runInEditor == true){
			runInEditor = false;
			EditorCombined();
		}
	}

	void EditorCombined(){
			MeshFilter[] meshFilters = GetComponentsInChildren<MeshFilter>();
			CombineInstance[] combine = new CombineInstance[meshFilters.Length];
			while (i < meshFilters.Length) {
				if (count >= 64000 && created == false) {
					_overFill = new GameObject ();
					_overFill.name = "AtlasMagickOverFill";
					_overFill.AddComponent<AtlasMagickMerge>();
					_overFill.GetComponent<AtlasMagickMerge> ().mat = mat;
					_temp = new GameObject();
					_temp.name = "PlaceHere";
					_temp.transform.parent = _overFill.transform.root;
					created = true;
				}

				if (created == true) {
					meshFilters [i].gameObject.transform.parent = _temp.transform;
				}

				if (created == false) {
					combine [i].mesh = meshFilters [i].sharedMesh;
					combine [i].transform = meshFilters [i].transform.localToWorldMatrix;
					count += meshFilters[i].sharedMesh.vertexCount;
				}
				i++;
			}
			gameObject.AddComponent<MeshFilter>().sharedMesh = new Mesh();
			gameObject.GetComponent<MeshFilter> ().sharedMesh.CombineMeshes (combine);
			gameObject.GetComponent<MeshCollider> ().sharedMesh = null;
			gameObject.GetComponent<MeshCollider> ().sharedMesh = transform.GetComponent<MeshFilter> ().sharedMesh;
			gameObject.GetComponent<MeshRenderer> ().sharedMaterial = mat;
		DestroyImmediate (gameObject.transform.GetChild (0).gameObject);
		if (created) {
				_overFill.GetComponent<AtlasMagickMerge> ().runInEditor = true;
		}
	}
}