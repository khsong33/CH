﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class AlphaActor : MonoBehaviour
{
	public enum eActorState
	{
		Stop,
		Arrived,
		Move,
		Rotation, // 제자리 회전
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public AlphaActorManager vActorManager;

	public float vRotationSpeed;
	public bool vIsLeader = false;
	public EffectUpdater vShotEffector;

	public Renderer[] vMeshRenderers;
	public String vTransparentShaderPath = "Legacy Shaders/Transparent/Diffuse";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aBodyTransform
	{
		get { return mBodyTransform; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public virtual void Init(StageTroop pStageTroop)
	{
		if (vMeshRenderers.Length <= 0)
			_ConstructMeshRenderers();

		mStageTroop = pStageTroop;
		mStage = pStageTroop.aStage;

		if (mFireEffectIdxs == null)
			mFireEffectIdxs = new int[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			BattleTroopStat.WeaponData lWeaponData = mStageTroop.aBattleTroop.aStat.aWeaponDatas[iWeapon];
			if (lWeaponData != null)
				mFireEffectIdxs[iWeapon] = _GetFireEffectIdx(lWeaponData.aWeaponSpec.mAttackType);
		}

		mIsActive = gameObject.activeInHierarchy;

		SetTransparency(1.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리더를 설정합니다.
	public virtual void SetLeader(bool pIsLeader)
	{
		vIsLeader = pIsLeader;
		if (vIsLeader)
		{
			Transform lTransform = transform;
			lTransform.localPosition = Vector3.zero;
			lTransform.localRotation = Quaternion.identity;

			mBodyTransform = vActorManager.aTransform;
		}
		else
		{
			mBodyTransform = transform;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 오브젝트 View 처리
	public virtual void SetActiveActor(bool pIsActive)
	{
		if (mIsActive == pIsActive)
			return;
		mIsActive = pIsActive;

		if (pIsActive)
		{
			mBodyTransform.position = mStage.GetTerrainPosition(mStageTroop.aTransform.position + new Vector3(0.0f, vActorManager.aHeightOffset, 0.0f));
			aBodyTransform.rotation = mStageTroop.aTransform.rotation;
		}

		if (vIsLeader)
			mStageTroop.SetMarkActorTransform(pIsActive ? mBodyTransform : mStageTroop.aTransform);

		gameObject.SetActive(pIsActive);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 활성 처리
	public abstract void EnableActor();
	//-------------------------------------------------------------------------------------------------------
	// Actor 비활성 처리
	public abstract void DisableActor();
	//-------------------------------------------------------------------------------------------------------
	// 위치 정의
	public abstract void SetPosition(Coord2 pCoord);
	//-------------------------------------------------------------------------------------------------------
	// 방향 정의
	public virtual void SetDirection(int pDirectionY)
	{
		aBodyTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 모션 처리
	public abstract void MoveTo(Coord2 pMoveCoord, bool pIsArrived);
	//-------------------------------------------------------------------------------------------------------
	// 공격 이전 모션 처리
	public abstract void GoPreAttack(int pDelayTime, Coord2 pTargetCoord);
	//-------------------------------------------------------------------------------------------------------
	// 공격 모션 처리
	public abstract void GoAttack(BattleFireEffect pFireEffect);
	//-------------------------------------------------------------------------------------------------------
	// 공격 정지 처리
	public abstract void StopAttack();
	//-------------------------------------------------------------------------------------------------------
	// 상호작용을 시작합니다.
	public virtual void GoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호작용을 끝냅니다.
	public virtual void ComeBackInteraction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 시작합니다.
	public virtual void StartSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 중단합니다.
	public virtual void StopSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션을 시작합니다.
	public virtual void GoAction(TroopActionType pTroopActionType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체 제자리 회전
	public abstract void GoBodyRotation(int pDirectionY);
	//-------------------------------------------------------------------------------------------------------
	// 터렛 제자리 회전
	public abstract void GoTurretRotation(int pDirectionY);
	//-------------------------------------------------------------------------------------------------------
	// Battle Troop 갱신 처리
	public virtual void OnFrameUpdate(int pDeltaTime)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 투명도를 지정합니다.
	public void SetTransparency(float pTransparency)
	{
		if (pTransparency >= 1.0f)
		{
			for (int iRenderer = 0; iRenderer < vMeshRenderers.Length; ++iRenderer)
				if (vMeshRenderers[iRenderer] != null)
					vMeshRenderers[iRenderer].material = mDefaultMaterials[iRenderer];
		}
		else
		{
			for (int iRenderer = 0; iRenderer < vMeshRenderers.Length; ++iRenderer)
				if (vMeshRenderers[iRenderer] != null)
				{
					Color lMaterialColor = mTransparentMaterials[iRenderer].color;
					lMaterialColor.a = pTransparency;
					mTransparentMaterials[iRenderer].color = lMaterialColor;

					vMeshRenderers[iRenderer].material = mTransparentMaterials[iRenderer];
				}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 메쉬 렌더러 리스트를 구성합니다.
	private void _ConstructMeshRenderers()
	{
		Shader lTransparentShader = Shader.Find(vTransparentShaderPath);
		if (lTransparentShader == null)
			Debug.LogError("can't find shader:" + vTransparentShaderPath + " - " + this);

		vMeshRenderers = GetComponentsInChildren<Renderer>();
		mDefaultMaterials = new Material[vMeshRenderers.Length];
		mTransparentMaterials = new Material[vMeshRenderers.Length];

		for (int iRenderer = 0; iRenderer < vMeshRenderers.Length; ++iRenderer)
		{
			mDefaultMaterials[iRenderer] = vMeshRenderers[iRenderer].material;
			mTransparentMaterials[iRenderer] = new Material(mDefaultMaterials[iRenderer]);
			mTransparentMaterials[iRenderer].shader = lTransparentShader;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전 처리
	protected void _UpdateRotation(Transform pRotationTransform, float pNextRotationValue, float pXRotValue, bool pIsChild = false)
	{
		// 회전 처리
		float lCurrentRotationValue = pRotationTransform.rotation.eulerAngles.y;
		float lNextRotationValue = pNextRotationValue;
		if (lNextRotationValue < 0)
			lNextRotationValue = lNextRotationValue + 360;
		// 4분면을 얻어옵니다.
		int lCurrentQuadrant = _GetQuadrant(lCurrentRotationValue);
		int lNextQuadrant = _GetQuadrant(lNextRotationValue);

		// 우측 회전 값
		float lRightRotationValue = lNextRotationValue - lCurrentRotationValue;
		if (lRightRotationValue < 0)
			lRightRotationValue = lRightRotationValue + 360;
		// 좌측 회전 값
		float lLeftRotationValue = lCurrentRotationValue - lNextRotationValue;
		if (lLeftRotationValue < 0)
			lLeftRotationValue = 360 + lLeftRotationValue;

		float lRotationValue = 0.0f;
		bool lLeftRot = false;
		if (lRightRotationValue > lLeftRotationValue) //좌측 회전
		{
			lRotationValue = -1 * vRotationSpeed * Time.deltaTime;
			lLeftRot = true;
		}
		else //우측회전
		{
			lRotationValue = vRotationSpeed * Time.deltaTime;
			lLeftRot = false;
		}

		// 실제 회전 값 적용
		float lResultRotationValue = lCurrentRotationValue + lRotationValue;
		if (lResultRotationValue < 0)
		{
			lResultRotationValue = 360 + lResultRotationValue;
		}

		float lResultRotationValueTransform = _TransformThetaValue(lResultRotationValue);
		float lNextRotationValueTransform = _TransformThetaValue(lNextRotationValue);

		if (lCurrentQuadrant != lNextQuadrant)
		{
			pRotationTransform.rotation = Quaternion.Euler(pXRotValue, lResultRotationValue, pRotationTransform.localEulerAngles.z);
		}
		else
		{
			if (lLeftRot)
			{
				if (lResultRotationValueTransform > lNextRotationValueTransform)
					pRotationTransform.rotation = Quaternion.Euler(pXRotValue, lResultRotationValue, pRotationTransform.eulerAngles.z);
				else
					pRotationTransform.rotation = Quaternion.Euler(pXRotValue, lNextRotationValue, pRotationTransform.eulerAngles.z);
			}
			else
			{
				if (lResultRotationValueTransform < lNextRotationValueTransform)
					pRotationTransform.rotation = Quaternion.Euler(pXRotValue, lResultRotationValue, pRotationTransform.eulerAngles.z);
				else
					pRotationTransform.rotation = Quaternion.Euler(pXRotValue, lNextRotationValue, pRotationTransform.eulerAngles.z);
			}
		}
		if (pIsChild)
			pRotationTransform.localEulerAngles = new Vector3(0.0f, pRotationTransform.localEulerAngles.y, 0.0f);
		//Debug.Log("Left Rot - " + lLeftRot + "/Rotation Value - " + pRotationTransform.rotation.eulerAngles.y);
	}
	//-------------------------------------------------------------------------------------------------------
	// 180이상의 값이 올경우 음수 각도 값으로 변환
	protected float _TransformThetaValue(float pValue)
	{
		if (pValue > 180)
			return pValue - 360;
		return pValue;
	}
	protected int _GetQuadrant(float pValue)
	{
		if ((pValue >= 0 && pValue <= 90) || (pValue <= -270 && pValue >= -360))
			return 1;
		else if ((pValue > 90 && pValue <= 180) || (pValue <= -180 && pValue > -270))
			return 2;
		else if ((pValue > 180 && pValue <= 270) || (pValue <= -90 && pValue > -180))
			return 3;
		else if ((pValue > 270 && pValue <= 360) || (pValue <= 0 && pValue > -90))
			return 4;

		return 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 위치의 높이값을 얻어옵니다.(메쉬, ray 이용)
	protected float _GetMeshMapHeight(Vector3 pPosition)
	{
		Vector3 lStartPos = new Vector3(pPosition.x, pPosition.y + 10.0f, pPosition.z);
		Vector3 lEndPos = new Vector3(pPosition.x, pPosition.y - 10.0f, pPosition.z);

		//Debug.DrawLine(lStartPos, lEndPos, Color.red);
		//Ray ray = new Ray(pPosition.transform.position, lEndPos);
		RaycastHit hitInfo;
		float lResultHeight = 0.0f;
		if (Physics.Raycast(lStartPos, lEndPos, out hitInfo))
		{
			lResultHeight = hitInfo.point.y;
			//lResultHeight = hitInfo.transform.position.y;
			if (lResultHeight > 0.0f)
				Debug.Log("Get Height - " + lResultHeight + "/Hit Transform name - " + hitInfo.transform.name);
		}
		return lResultHeight;
	}
	//-------------------------------------------------------------------------------------------------------
	// 발사 무기를 선택합니다.
	protected EffectUpdater _GetAttackEffect(int pWeaponIdx)
	{
		EffectUpdater lEffect = mStage.vEffectPool.CreateEffector(mFireEffectIdxs[pWeaponIdx], aBodyTransform.position);
		return lEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 공격 타입별 발사 이펙트 번호를 얻습니다.
	private int _GetFireEffectIdx(WeaponAttackType pWeaponAttackType)
	{
		switch (pWeaponAttackType)
		{
			case WeaponAttackType.Rifle: return 0;
			case WeaponAttackType.Grenade: return 1;
			case WeaponAttackType.MachineGun: return 3;
			case WeaponAttackType.TankGun: return 2;
			case WeaponAttackType.HighAngleGun: return 1;
			default: return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체가 지면에 맞게 기울이는 갱신을 합니다.
	protected void _UpdateBodySlope(Vector3[] pThreeHeightPoints, int pBodyDirectionY)
	{
		aBodyTransform.position = mStage.aMapHeight.SampleHeightPosition(aBodyTransform.position);

		Quaternion lBodyRotation = MathLib.GetRotation(pBodyDirectionY);
		Vector3 lHeightPoint0Position = mStage.aMapHeight.SampleHeightPosition(aBodyTransform.position + lBodyRotation * pThreeHeightPoints[0]);
		Vector3 lHeightPoint1Position = mStage.aMapHeight.SampleHeightPosition(aBodyTransform.position + lBodyRotation * pThreeHeightPoints[1]);
		Vector3 lHeightPoint2Position = mStage.aMapHeight.SampleHeightPosition(aBodyTransform.position + lBodyRotation * pThreeHeightPoints[2]);

		mBodyNormalVector = Vector3.Cross(lHeightPoint2Position - lHeightPoint0Position, lHeightPoint1Position - lHeightPoint0Position);
		Vector3 lForwardVector = lHeightPoint0Position - aBodyTransform.position;
		mBodyTransform.rotation = Quaternion.LookRotation(lForwardVector, mBodyNormalVector);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mBodyTransform;
	protected Vector3 mBodyNormalVector;
	protected StageTroop mStageTroop;
	protected Stage mStage;

	protected eActorState mActorBodyState = eActorState.Stop;
	protected eActorState mActorTurretState = eActorState.Stop;
	protected BattleFireEffect mFireEffect;

	protected int mFrameDeltaTime;
	protected float mCurrentUpdateTime;

	private int[] mFireEffectIdxs;
	protected bool mIsActive;

	private Material[] mDefaultMaterials;
	private Material[] mTransparentMaterials;
}
