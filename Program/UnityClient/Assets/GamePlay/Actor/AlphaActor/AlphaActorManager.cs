﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorManager : MonoBehaviour, IActorManager
{
	private const float cAircraftHeight = 30.0f;
	private const float cHidingTransparency = 0.333f;

    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public AlphaActorPool vActorPool;

    public int vActorManagerIndex;
    public StageTroop vStageTroop;
	public uint vTroopGuid;

	public Transform vPositioningObjectTransform;
	public bool vIsInteractionAttack = false; // 공격 동작이 상호작용인지 여부

    //-------------------------------------------------------------------------------------------------------
    // 속성
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public Collider aCollider
	{
		get { return mSphrerCollider; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public float aVisualRadius
	{
		get { return mVisualRadius; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public bool aIsVisible
	{
		get { return mIsActive; }
		set
		{
			SetActiveActor(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public bool aIsHiding
	{
		get { return mIsHiding; }
		set
		{
			SetHiding(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	//public IActorManager aIActorManager
	//{
	//	get { return this; }
	//}
	public float aHeightOffset
	{
		get { return mHeightOffset; }
		set
		{
			mHeightOffset = value;

			for (int iActor = 0; iActor < mActorList.Count; iActor++)
				mActorList[iActor].SetPosition(vStageTroop.aBattleTroop.aCoord);
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mTransform = transform;

		mSphrerCollider = GetComponent<SphereCollider>();
		mVisualRadius = mSphrerCollider.radius;
		float lColliderRadius = mSphrerCollider.radius * mTransform.localScale.x;
		if (lColliderRadius < BattleConfig.get.mMinTouchRadius)
			lColliderRadius = BattleConfig.get.mMinTouchRadius;
		mSphrerCollider.radius = lColliderRadius / mTransform.localScale.x;

		mStageTroopTag = gameObject.AddComponent<StageTroopTag>();
		mActorList = new List<AlphaActor>();

		int lActorCount = gameObject.transform.childCount;
		for (int iActor = 0; iActor < lActorCount; iActor++)
		{
			Transform lChildObjectTransform = gameObject.transform.GetChild(iActor);
			AlphaActor lChildActor = lChildObjectTransform.GetComponent<AlphaActor>();
			if (lChildActor != null)
			{
				mActorList.Add(lChildActor);
				lChildActor.vActorManager = this;
				lChildActor.SetLeader(iActor == 0);
			}
		}

		mHeightOffset = 0.0f;
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
	//-------------------------------------------------------------------------------------------------------
	//
	public void SetStageTroop(StageTroop pStageTroop)
	{
		vStageTroop = pStageTroop;
		vTroopGuid = pStageTroop.aBattleTroop.aGuid;

		mTransform.position = vStageTroop.aTransform.position;

		mStageTroopTag.vStageTroop = pStageTroop;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].Init(pStageTroop);
		}
	}
	//-------------------------------------------------------------------------------------------------------
    //
    public void EnableActor()
    {
        for (int iActor = 0; iActor < mActorList.Count; iActor++)
        {
            mActorList[iActor].EnableActor();
        }
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;

		mIsActive = true; // SetActiveActor()가 작동하도록
		mIsVisible = true;
		SetActiveActor(false);
	}
    //-------------------------------------------------------------------------------------------------------
    //
    public void DisableActor()
    {
        for (int iActor = 0; iActor < mActorList.Count; iActor++)
        {
            mActorList[iActor].DisableActor();
        }
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;
    }
	//-------------------------------------------------------------------------------------------------------
	// Actor의 랜더링
	public void SetActiveActor(bool pIsActive)
	{
		if (mIsActive == pIsActive)
			return;
		mIsActive = pIsActive;

		float lViewDist = BattlePlay.main.vControlPanel.vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance;
		//Debug.Log("lView Dist - " + lViewDist + "/Actor - " + vActorManagerIndex);
		mIsVisible = (lViewDist <= vStageTroop.aStage.vLodDistance);

		int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
		bool lIsActive;
		if (!vStageTroop.vIsDebugShowActor)
			lIsActive = vStageTroop.aBattleTroop.aIsDetecteds[lObserverTeamIdx]; // 색적이 안 된 부대의 경우에는 계속 안 보이도록
		else
			lIsActive = true;
		lIsActive &= mIsVisible;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].SetActiveActor(lIsActive);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void Destroy()
	{
		vActorPool.DestoryActorManager(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSpawn()
	{
		if (vStageTroop.aBattleTroop.aCommander.aCommanderSpec.mSpawnType == CommanderSpawnType.Aircraft)
			StartCoroutine(_UpdateAircraftSpawn());
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].SetPosition(pCoord);
			mActorList[iActor].SetDirection(pDirectionY);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].OnFrameUpdate(pDeltaTime);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnHoldMove(bool pIsHoldMove)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopMove(Coord2 pStopCoord)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnMoveTo(Coord2 pMoveCoord, bool pIsArrived)
    {
        for (int iActor = 0; iActor < mActorList.Count; iActor++)
        {
            mActorList[iActor].MoveTo(pMoveCoord, pIsArrived);
        }
    }
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateBodyToDirectionY(int pDirectionY)
	{
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].GoBodyRotation(pDirectionY);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateBodyWeaponToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateTurretToDirectionY(int pDirectionY)
	{
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].GoTurretRotation(pDirectionY);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnInstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnTrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay)
	{
		GoPreAttack(pTrackingDelay, pTargetCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnWindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay)
	{
		GoPreAttack(pWindupDelay, pTargetCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnAimWeapon(int pAimDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnFireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim)
	{
		GoAttack(pFireEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnEndCooldown()
	{
		StopAttack();
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnGoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
		bool lIsShowAlways;
		if ((pTargetTroop != null) &&
			(pTargetTroop.aTeamIdx == BattlePlay.main.vStage.vObserverTeamIdx))
			lIsShowAlways = true;
		else
			lIsShowAlways = false;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			if (lIsShowAlways ||
				mActorList[iActor].gameObject.activeInHierarchy)
				mActorList[iActor].GoToInteraction(pTroopInteractionType, pTargetTroop);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnComeBackInteraction()
	{
		if (!mIsVisible)
			return;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
			mActorList[iActor].ComeBackInteraction();
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStartSkill(BattleCommanderSkill pCommanderSkill)
	{
		if (!mIsVisible)
			return;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
			mActorList[iActor].StartSkill(pCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStopSkill(BattleCommanderSkill pCommanderSkill)
	{
		if (!mIsVisible)
			return;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
			mActorList[iActor].StopSkill(pCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStartAction(TroopActionType pTroopActionType)
	{
		if (!mIsVisible)
			return;

		for (int iActor = 0; iActor < mActorList.Count; iActor++)
			mActorList[iActor].GoAction(pTroopActionType);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStopAction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
	}
	//-------------------------------------------------------------------------------------------------------
    // 공격전 모션을 처리합니다.
    public void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
    {
		if (!mIsVisible)
			return;
		
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
            mActorList[iActor].GoPreAttack(pDelayTime, pTargetCoord);
    }
    //-------------------------------------------------------------------------------------------------------
	// 공격 모션을 처리합니다.
	public void GoAttack(BattleFireEffect pFireEffect)
    {
		if (!mIsVisible)
			return;

		bool lIsShowAlways;
		if ((pFireEffect.aTargetTroop != null) &&
			(pFireEffect.aTargetTroop.aTeamIdx == BattlePlay.main.vStage.vObserverTeamIdx))
			lIsShowAlways = true;
		else
			lIsShowAlways = false;

        for (int iActor = 0; iActor < mActorList.Count; iActor++)
        {
			if (lIsShowAlways ||
				mIsActive)
				mActorList[iActor].GoAttack(pFireEffect);
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // 공격 중지를 처리합니다.
    public void StopAttack()
    {
		if (!mIsVisible)
			return;
		
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
            mActorList[iActor].StopAttack();
    }
	//-------------------------------------------------------------------------------------------------------
	// 리더 액터를 가져옵니다.
	public AlphaActor GetLeader()
	{
		return mActorList[0];
	}
	//-------------------------------------------------------------------------------------------------------
	// 투명도를 지정합니다.
	public void SetHiding(bool pIsHiding)
	{
		mIsHiding = pIsHiding;
		float lTransparency = pIsHiding ? cHidingTransparency : 1.0f;
		for (int iActor = 0; iActor < mActorList.Count; iActor++)
		{
			mActorList[iActor].SetTransparency(lTransparency);
		}
	}

	//-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
	// 줌이 변경될때 호출됩니다.
	private void _OnZoomChanged()
	{
		mIsActive = !mIsActive; // SetActiveActor()가 작동하도록
		SetActiveActor(!mIsActive);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 낙하산 스폰을 갱신합니다.
	private IEnumerator _UpdateAircraftSpawn()
	{
		float lSpawnDelay = BattleConfig.get.mAircraftSpawnDelay * 0.001f;
		float lSpawnTimer = lSpawnDelay;
		do
		{
			aHeightOffset = lSpawnTimer / lSpawnDelay * cAircraftHeight;

			lSpawnTimer -= Time.deltaTime;
			yield return null;
		} while (lSpawnTimer > 0.0f);

		yield return null;
	}

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private SphereCollider mSphrerCollider;
	private float mVisualRadius;
	private StageTroopTag mStageTroopTag;
	private List<AlphaActor> mActorList;
	private bool mIsActive;
	private bool mIsVisible;
	private bool mIsHiding;
	private float mHeightOffset;
}
