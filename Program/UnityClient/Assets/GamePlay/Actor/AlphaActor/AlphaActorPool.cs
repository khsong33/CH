﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorPool : MonoBehaviour, IActorPool
{
    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public String vAssetKeyTableBundle;
	public String vAssetKeyTablePath;

	public List<AlphaActorManager> vActorManagerList;

    //-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------
	//
    public IActorPool aIActorPool
    {
        get { return this; }
    }

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
		mTransform = transform;

		mAssetKeyTable = new AssetKeyTable();
		mAssetKeyTable.Load(vAssetKeyTableBundle, vAssetKeyTablePath, false);

		mActorManagerIdxDictionary = new Dictionary<String, Queue<int>>();

		vActorManagerList = new List<AlphaActorManager>();
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // IActorPool 메서드
	public IActorManager CreateActorManager(StageTroop pStageTroop)
    {
        // 기존의 정보가 없으면 새로 생성합니다.
		AlphaActorManager lActorManager;
		String lStageTroopAssetKeyName = pStageTroop.aBattleTroop.aTroopSpec.mStageTroopAssetKeyName;
		int lActorPoolIndex = _FindActorManager(lStageTroopAssetKeyName);
        if (lActorPoolIndex < 0)
        {
			AssetKey lActorManagerAssetKey = mAssetKeyTable.FindAssetKey(lStageTroopAssetKeyName);
			if (!AssetKey.IsValid(lActorManagerAssetKey))
				Debug.LogError("invalid asset key '" + lActorManagerAssetKey + "' - " + this);

			lActorManager = AssetManager.get.InstantiateComponent<AlphaActorManager>(lActorManagerAssetKey);
			Transform lActorTransform = lActorManager.transform;
			lActorTransform.position = pStageTroop.aTransform.position;
			lActorTransform.rotation = pStageTroop.aTransform.rotation;
			lActorTransform.parent = mTransform;

			//lActorManager.aIActorManager.Init();
			lActorManager.vActorPool = this;
            vActorManagerList.Add(lActorManager);
            lActorManager.vActorManagerIndex = vActorManagerList.Count - 1;
        }
        else
        {
			lActorManager = vActorManagerList[lActorPoolIndex];
			lActorManager.gameObject.SetActive(true);
        }

		lActorManager.SetStageTroop(pStageTroop);
		lActorManager.EnableActor();

		lActorManager.OnSetCoordAndDirectionY(pStageTroop.aBattleTroop.aCoord, pStageTroop.aBattleTroop.aDirectionY);

		return lActorManager;
    }
    //-------------------------------------------------------------------------------------------------------
    // IActorPool 메서드
	public void DestoryActorManager(AlphaActorManager pActorManager)
    {
		pActorManager.DisableActor();

        if (pActorManager == null)
            return;

		String lStageTroopAssetKeyName = pActorManager.vStageTroop.aBattleTroop.aTroopSpec.mStageTroopAssetKeyName;
		if (!mActorManagerIdxDictionary.ContainsKey(lStageTroopAssetKeyName))
        {
			mActorManagerIdxDictionary.Add(lStageTroopAssetKeyName, new Queue<int>());
        }
		mActorManagerIdxDictionary[lStageTroopAssetKeyName].Enqueue(pActorManager.vActorManagerIndex);
        pActorManager.gameObject.SetActive(false);
    }

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 풀 안에 생성된 ActorManager가 있는지 확인합니다.
    private int _FindActorManager(String pAssetPath)
    {
		if (mActorManagerIdxDictionary.ContainsKey(pAssetPath))
        {
			int lResultActorListIndex = mActorManagerIdxDictionary[pAssetPath].Dequeue();
            // 더 이상 풀에 id가 없으면 삭제
			if (mActorManagerIdxDictionary[pAssetPath].Count <= 0)
				mActorManagerIdxDictionary.Remove(pAssetPath);

            return lResultActorListIndex;
        }
        return -1;
    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private AssetKeyTable mAssetKeyTable;
    private Dictionary<String, Queue<int>> mActorManagerIdxDictionary;
}
