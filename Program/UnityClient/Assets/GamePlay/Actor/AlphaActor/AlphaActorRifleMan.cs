﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorRifleMan : AlphaActor
{
	private const int cStateMax = 3;

	private const float cSqrTroopMoveEndCheckDistance = 0.2f * 0.2f;
	private const float cSqrInteractionMoveEndCheckDistance = 3.0f * 3.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Animation vAnim;
	public String vIdleState = "Idle01";
	public String vFireState = "Attack01";
	public String vRunState = "Run";
	public String vPanicState = "Panic";
	public String vSkillState = "Idle02";
	public String vActionState = "Idle02";

	public Vector3 vMaxOffset;
	public Vector3 vMinOffset;

	public int vAccelSpeed = 10; // 초당 가속
	public int vMoveSpeed = 0;

	public Transform vPositionObjectTransform; // 메딕의 구급 상자처럼 위치를 상징하는 오브젝트(null이면 없음)

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		vAnim = gameObject.GetComponent<Animation>();
		mTargetMovePosition = new Vector3(0, 0, 0);

		if (vAnim == null)
			Debug.LogError("vAnim is empty - " + this);

		mBaseOffset = transform.localPosition;
		_RefreshArragement();
	}
	//-------------------------------------------------------------------------------------------------------
	// AlphaActor 메서드
	public override void SetActiveActor(bool pIsActive)
	{
		base.SetActiveActor(pIsActive);

		if (mIsActive)
		{
			mSqrMoveEndCheckDistance = cSqrTroopMoveEndCheckDistance;

			mIsFiring = false;
			mIsInInteraction = false;
			mIsUpdatePositioningObject = false;
			mIsResetPositioningObject = false;

			mAniState = null;
			mIsSetAniState = true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트
	void Update()
	{
		if (mStageTroop == null)
			return;
		
		// 패닉상태 선처리
		if (!mStageTroop.aBattleTroop.aCommander.aIsFleeing &&
			(mStageTroop.aBattleTroop.aStat.aMoraleState == TroopMoraleState.PinDown) &&
			(mStageTroop.aBattleTroop.aMoveUpdater.aMoveType != BattleTroopMoveUpdater.MoveType.PositioningMove))
		{
			_SetAniState(vPanicState, 0.2f);
			return;
		}
		else if (mAniState == vPanicState)
		{
			if (!(!mStageTroop.aBattleTroop.aCommander.aIsFleeing &&
				(mStageTroop.aBattleTroop.aStat.aMoraleState == TroopMoraleState.PinDown) &&
				(mStageTroop.aBattleTroop.aMoveUpdater.aMoveType != BattleTroopMoveUpdater.MoveType.PositioningMove)))
				_SetAniState(vIdleState, 0.2f);
		}

		if (mActorBodyState == AlphaActor.eActorState.Stop)
		{
			return;
		}

		if (mCurrentMoveDelay < mMoveDelay)
		{
			mCurrentMoveDelay += Time.deltaTime;
			return;
		}

		Vector2 lMoveVector = new Vector2(mTargetMovePosition.x - aBodyTransform.position.x, mTargetMovePosition.z - aBodyTransform.position.z);
		if (lMoveVector.sqrMagnitude < mSqrMoveEndCheckDistance)
			mActorBodyState = AlphaActor.eActorState.Arrived;

		// 이동시에만 방향 벡터 갱신
		if (mActorBodyState == AlphaActor.eActorState.Move)
		{
			mMoveDirectVector = new Vector3(
				mTargetMovePosition.x - aBodyTransform.position.x,
				0.0f,
				mTargetMovePosition.z - aBodyTransform.position.z);
			mMoveDirectVector = mMoveDirectVector.normalized;
		}

		// 이동 처리
		if (mActorBodyState != AlphaActor.eActorState.Stop)
		{
			// 블리츠 모드인지 확인
			int lMaxMoveSpeed = mStageTroop.aBattleTroop.aMoveUpdater.aMoveSpeed + mMoveSpeedOffset;
			_SetAniState(vRunState, 0.2f);

			if (mActorBodyState == AlphaActor.eActorState.Arrived)
			{
				vMoveSpeed -= vAccelSpeed;
				_SetAniState(vRunState, 0.2f);
			}
			else
			{
				vMoveSpeed += vAccelSpeed;
			}

			if (vMoveSpeed >= lMaxMoveSpeed)
				vMoveSpeed = lMaxMoveSpeed;
			else if (vMoveSpeed <= 0)
			{
				mActorBodyState = AlphaActor.eActorState.Stop;
				//Debug.Log("Speed Zero Stop");
				_SetAniState(vIdleState, 0.2f);
				vMoveSpeed = 0;
				_RefreshArragement();
			}

			//float lMoveValue = ((lMoveSpeed + mMoveSpeedOffset) * 0.01f) * Time.fixedDeltaTime;
			float lMoveValue = (vMoveSpeed * 0.01f) * Time.deltaTime;
			aBodyTransform.position = mStage.GetTerrainPosition(new Vector3(
				aBodyTransform.position.x + (mMoveDirectVector.x * lMoveValue),
				vActorManager.aHeightOffset,
				aBodyTransform.position.z + (mMoveDirectVector.z * lMoveValue)));

			// 회전 처리
			float lRotationY = MathLib.GetDirectionY(mMoveDirectVector);
			_UpdateRotation(aBodyTransform, lRotationY, 0.0f);
//			_UpdateTerrain();
		}

		// 상호작용중이라면 그동안 부대의 위치를 나타내는 포지션 오브젝트를 고정시킵니다.
		if (mIsUpdatePositioningObject)
		{
			if (!mIsResetPositioningObject)
			{
				vActorManager.vPositioningObjectTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord));
				vActorManager.vPositioningObjectTransform.rotation = MathLib.GetRotation(mStageTroop.aBattleTroop.aDirectionY);
			}
			else
			{
				mIsUpdatePositioningObject = false;
				mIsResetPositioningObject = false;

				vActorManager.vPositioningObjectTransform.localPosition = Vector3.zero;
				vActorManager.vPositioningObjectTransform.localRotation = Quaternion.identity;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void EnableActor()
	{
		mActorBodyState = eActorState.Stop;
		mActorTurretState = eActorState.Stop;

		_RefreshArragement();
		_SetAniState(vIdleState, 0.2f);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void DisableActor()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void Init(StageTroop pStageTroop)
	{
		base.Init(pStageTroop);

		mMoveSpeedOffset = UnityEngine.Random.Range(-25, 25);
		mMoveDirectVector = new Vector3(0, 0, 0);
		//Debug.Log("mFrameAdvanceSpeed - " + mFrameAdvanceSpeed);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void SetPosition(Coord2 pPosition)
	{
		aBodyTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(pPosition, vActorManager.aHeightOffset));
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void MoveTo(Coord2 pMoveCoord, bool pIsArrived)
	{
		if (mIsActive)
		{
			if (mActorBodyState == AlphaActor.eActorState.Stop)
			{
				if (vIsLeader)
				{
					mMoveDelay = 0.0f;
					mMoveSpeedOffset = 0;
				}
				else
				{
					mMoveDelay = UnityEngine.Random.Range(0.0f, 1.0f);
					mMoveSpeedOffset = UnityEngine.Random.Range(-25, 25);
				}
				mCurrentMoveDelay = 0.0f;
			}
			mTargetMovePosition = Coord2.ToWorld(pMoveCoord) + mArrangedOffset;
			mActorBodyState = AlphaActor.eActorState.Move;
		}
		else
		{
			mActorBodyState = AlphaActor.eActorState.Stop;
			mTargetMovePosition = Coord2.ToWorld(pMoveCoord, vActorManager.aHeightOffset) + mArrangedOffset;
			aBodyTransform.position = mStage.GetTerrainPosition(mTargetMovePosition);
		}

		mIsResetPositioningObject = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
	{
		if (mIsInInteraction)
			return;

		aBodyTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset));

		mActorBodyState = AlphaActor.eActorState.Stop;
		_SetAniState(vIdleState, 0.2f);
		//Debug.Log("GoPreAttack Stop");
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoAttack(BattleFireEffect pFireEffect)
	{
		if (mIsInInteraction)
			return;

		if (mActorBodyState != AlphaActor.eActorState.Stop)
			return;

		mFireEffect = pFireEffect;

		if (!vActorManager.vIsInteractionAttack)
		{
			if (!mIsFiring)
			{
				mIsFiring = true;

				float lAttackDelay = vIsLeader ? 0.0f : (float)UnityEngine.Random.Range(0.0f, 1.0f);
				mStageTroop.StartCoroutine(_GoFireAttack(lAttackDelay, pFireEffect.aHitCoord));
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 실 유닛 공격 처리
	private IEnumerator _GoFireAttack(float pAttackDelay, Coord2 pTargetCoord)
	{
		yield return new WaitForSeconds(pAttackDelay);
		// 방향 회전 처리
		Vector3 lDirectVector = (aBodyTransform.position - Coord2.ToWorld(pTargetCoord));
		aBodyTransform.rotation = MathLib.GetRotation(lDirectVector);

		if (vIsLeader) // 리더일 경우에만 발포
		{
			// EffectUpdater lEffect = mStage.vEffectPool.CreateEffector(vFireEffectId1st, aBodyTransform.position);
			EffectUpdater lEffect = _GetAttackEffect(mFireEffect.aFireWeaponData.mWeaponIdx);
			if (lEffect != null)
				lEffect.gameObject.SetActive(true);

			yield return null;

			if (lEffect != null)
				lEffect.GoFire(aBodyTransform, Coord2.ToWorld(mFireEffect.aHitCoord), (float)(mFireEffect.aHitDelay / 1000.0f));
		}

		_SetAniState(vFireState, 0.2f);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void StopAttack()
	{
		if (mIsInInteraction)
			return;

		//_SetAniState(vIdleState, 0.2f);
		if (mIsFiring)
		{
			mIsFiring = false;

			_SetAniState(vIdleState, 0.2f);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
		if (!mIsInInteraction)
		{
			if (vIsLeader) // 리더일 경우에만 상호작용
			{
				mIsInInteraction = true;
				mInteractionCoord = pTargetTroop.aCoord;

				mSqrMoveEndCheckDistance = cSqrInteractionMoveEndCheckDistance;
				//Debug.Log("START mInteractionCoord=" + mInteractionCoord);
				MoveTo(mInteractionCoord, true);

				if (vActorManager.vPositioningObjectTransform != null)
				{
					mIsUpdatePositioningObject = true;
					mIsResetPositioningObject = false;
				}
			}
		}
		else
		{
			if (mInteractionCoord != pTargetTroop.aCoord)
			{
				mInteractionCoord = pTargetTroop.aCoord;

				//Debug.Log("CHANGE mInteractionCoord=" + mInteractionCoord);
				MoveTo(mInteractionCoord, true);

				if (vActorManager.vPositioningObjectTransform != null)
				{
					mIsResetPositioningObject = false;
				}
			}
			else
			{
				//Debug.Log("mInteractionCoord is not changed");
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void ComeBackInteraction()
	{
		if (mIsInInteraction)
		{
			mIsInInteraction = false;

			//Debug.Log("COME BACK");
			mSqrMoveEndCheckDistance = cSqrTroopMoveEndCheckDistance;
			MoveTo(mStageTroop.aBattleTroop.aCoord, true);

			if (vActorManager.vPositioningObjectTransform != null)
			{
				mIsUpdatePositioningObject = true;
				mIsResetPositioningObject = false;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void StartSkill(BattleCommanderSkill pCommanderSkill)
	{
		aBodyTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset));

		mActorBodyState = AlphaActor.eActorState.Stop;
		_SetAniState(vSkillState, 0.2f);

		mStageTroop.StartCoroutine(_StopSkillAction(pCommanderSkill.aReadyDelay * MathUtil.cMilliToSec));
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void StopSkill(BattleCommanderSkill pCommanderSkill)
	{
		aBodyTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset));

		mActorBodyState = AlphaActor.eActorState.Stop;
		_SetAniState(vSkillState, 0.2f);

		mStageTroop.StartCoroutine(_StopSkillAction(pCommanderSkill.aReadyDelay * MathUtil.cMilliToSec));
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoAction(TroopActionType pTroopActionType)
	{
		aBodyTransform.position = mStage.GetTerrainPosition(Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset));

		mActorBodyState = AlphaActor.eActorState.Stop;
		_SetAniState(vActionState, 0.2f);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoBodyRotation(int pDirectionY)
	{
		aBodyTransform.rotation = MathLib.GetRotation(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoTurretRotation(int pDirectionY)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정렬 offset 재설정합니다.
	private void _RefreshArragement()
	{
		if (!vIsLeader)
		{
			mArrangedOffset = mBaseOffset;
// 			mArrangedOffset += new Vector3(UnityEngine.Random.Range(vMinOffset.x, vMaxOffset.x),
// 										   0,
// 										   UnityEngine.Random.Range(vMinOffset.z, vMaxOffset.z));

			if (mStageTroop != null)
				mArrangedOffset = mStageTroop.aTransform.rotation * mArrangedOffset;
		}
		else
		{
			mArrangedOffset = Vector3.zero;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 애니를 지정합니다.
	private void _SetAniState(String pAniState, float pFadeDelay)
	{
		if (mAniState == pAniState)
			return;

		mAniState = pAniState;
		vAnim.CrossFade(pAniState, pFadeDelay);

		mIsSetAniState = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 높이 처리
// 	private void _UpdateTerrain()
// 	{
// 		if (Terrain.activeTerrain == null)
// 			return;
// 
// 		float lCurrentHeight = Terrain.activeTerrain.SampleHeight(aBodyTransform.position);
// 		aBodyTransform.position = new Vector3(aBodyTransform.position.x, lCurrentHeight + vActorManager.aHeightOffset, aBodyTransform.position.z);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 일정 시간 후에 아이들 동작으로 전환
	private IEnumerator _StopSkillAction(float pStopDelay)
	{
		mIsSetAniState = false;
		yield return new WaitForSeconds(pStopDelay);

		if (!mIsSetAniState)
			_SetAniState(vIdleState, 0.2f);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private String mAniState;
	private Vector3 mBaseOffset;
	private Vector3 mArrangedOffset;
	private Vector3 mTargetMovePosition;
	public int mMoveSpeedOffset = 0;
	private float mMoveDelay;
	private float mCurrentMoveDelay;
	private Vector3 mMoveDirectVector;
	private float mSqrMoveEndCheckDistance;

	private bool mIsFiring;
	private bool mIsInInteraction;
	private bool mIsUpdatePositioningObject;
	private bool mIsResetPositioningObject;
	private Coord2 mInteractionCoord;

	private bool mIsSetAniState;
}
