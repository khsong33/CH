﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorSoldier : AlphaActor
{
    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
    public Animation vAnim;
    public Vector3 vOffset;

    public Vector3 vMaxOffset;
    public Vector3 vMinOffset;

    public int vAccelSpeed = 10; // 초당 가속
    public int vMoveSpeed = 0;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
        vAnim = gameObject.GetComponent<Animation>();
        if (vAnim != null)
        {
            AnimationState lWalkAniState = vAnim["walk"];
            lWalkAniState.speed = 0.3f;
            AnimationState lFireAniState = vAnim["attack"];
            lFireAniState.speed = 0.3f;
            AnimationState lStandAniState = vAnim["stand"];
            lStandAniState.speed = 0.3f;

			float lRandomStartAnimTime = UnityEngine.Random.Range(0.0f, 1.0f);
            lWalkAniState.normalizedTime = lRandomStartAnimTime;
        }

		mTargetMovePosition = new Vector3(0, 0, 0);

        if (vAnim == null)
            Debug.LogError("Not Exist Animation - " + gameObject.name);
        else
            vAnim.Play("stand");

        _RefreshArragement();
    }
    //-------------------------------------------------------------------------------------------------------
    // 업데이트
    void Update()
    {
        if (mStageTroop == null)
            return;
        if (mActorBodyState == AlphaActor.eActorState.Stop)
            return;

        if (mCurrentMoveDelay < mMoveDelay)
        {
            mCurrentMoveDelay += Time.fixedDeltaTime;
            if(vAnim != null)
                vAnim.Play("stand");
            return;
        }

		float lTargetDistance = Vector3.Distance(mTargetMovePosition, aBodyTransform.position);
        if (lTargetDistance < 0.5f)
        {
            mActorBodyState = AlphaActor.eActorState.Arrived;
        }

        // 이동시에만 방향 벡터 갱신
        if (mActorBodyState == AlphaActor.eActorState.Move)
        {
			mMoveDirectVector = mTargetMovePosition - aBodyTransform.position;
            mMoveDirectVector = mMoveDirectVector.normalized;
        }
  
        // 이동 처리
        if (mActorBodyState != AlphaActor.eActorState.Stop)
        {
            if (vAnim != null)
                vAnim.Play("walk");
 
            // 블리츠 모드인지 확인
			int lMaxMoveSpeed = mStageTroop.aBattleTroop.aMoveUpdater.aMoveSpeed;

            if (mActorBodyState == AlphaActor.eActorState.Arrived)
            {
                vMoveSpeed -= vAccelSpeed;
            }
            else
            {
                vMoveSpeed += vAccelSpeed;
            }

            if (vMoveSpeed >= lMaxMoveSpeed)
                vMoveSpeed = lMaxMoveSpeed;
            else if (vMoveSpeed <= 0)
            {
                mActorBodyState = AlphaActor.eActorState.Stop;
                //Debug.Log("Speed Zero Stop");
                if(vAnim != null)
                    vAnim.Play("stand");
                vMoveSpeed = 0;
                _RefreshArragement();
            }

            //float lMoveValue = ((lMoveSpeed + mMoveSpeedOffset) * 0.01f) * Time.fixedDeltaTime;
            float lMoveValue = (vMoveSpeed * 0.01f) * Time.deltaTime;
			aBodyTransform.position = new Vector3(aBodyTransform.position.x + (mMoveDirectVector.x * lMoveValue),
											vActorManager.aHeightOffset,
											aBodyTransform.position.z + (mMoveDirectVector.z * lMoveValue));

            // 회전 처리
			float lRotationY = MathLib.GetDirectionY(mMoveDirectVector);
			_UpdateRotation(aBodyTransform, lRotationY, 0.0f);
        }
	}
     
    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void EnableActor()
    {
		mActorBodyState = eActorState.Stop;
		mActorTurretState = eActorState.Stop;
		
		if (vAnim != null)
            vAnim.Play("stand");
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void DisableActor()
    {
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void Init(StageTroop pStageTroop)
    {
        base.Init(pStageTroop);

        mMoveSpeedOffset = UnityEngine.Random.Range(-25, 25);
        mMoveDirectVector = new Vector3(0, 0, 0);
        //Debug.Log("mFrameAdvanceSpeed - " + mFrameAdvanceSpeed);
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void SetPosition(Coord2 pPosition)
    {
		aBodyTransform.position = Coord2.ToWorld(pPosition, vActorManager.aHeightOffset);
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
	public override void MoveTo(Coord2 pMoveCoord, bool pIsArrived)
    {
		if (mIsActive)
		{
			if (mActorBodyState == AlphaActor.eActorState.Stop)
			{
				if (vIsLeader)
				{
					mMoveDelay = 0.0f;
					mMoveSpeedOffset = 0;
				}
				else
				{
					mMoveDelay = UnityEngine.Random.Range(0.0f, 1.0f);
					mMoveSpeedOffset = UnityEngine.Random.Range(-25, 25);
				}
				mCurrentMoveDelay = 0.0f;
			}
			mTargetMovePosition = Coord2.ToWorld(pMoveCoord) + vOffset;

			mActorBodyState = AlphaActor.eActorState.Move;
		}
		else
		{
			mActorBodyState = AlphaActor.eActorState.Stop;
			mTargetMovePosition = Coord2.ToWorld(pMoveCoord, vActorManager.aHeightOffset) + vOffset;
			aBodyTransform.position = mTargetMovePosition;
		}
	}
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
    {
        mActorBodyState = AlphaActor.eActorState.Stop;
        //Debug.Log("GoPreAttack Stop");
        vAnim.Play("stand");
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
	public override void GoAttack(BattleFireEffect pFireEffect)
    {
        if (mActorBodyState != AlphaActor.eActorState.Stop)
            return;

        // 방향 회전 처리
		Vector3 lDirectVector = (aBodyTransform.position - Coord2.ToWorld(pFireEffect.aHitCoord));
		aBodyTransform.rotation = MathLib.GetRotation(lDirectVector);

        if (vAnim != null)
            vAnim.Play("attack");        
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void StopAttack()
    {
    }
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoBodyRotation(int pDirectionY)
	{
		aBodyTransform.rotation = MathLib.GetRotation(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoTurretRotation(int pDirectionY)
	{
	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 정렬 offset 재설정합니다.
    private void _RefreshArragement()
    {
        vOffset = new Vector3(UnityEngine.Random.Range(vMinOffset.x, vMaxOffset.x),
                              0,
                              UnityEngine.Random.Range(vMinOffset.z, vMaxOffset.z));
    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private Vector3 mTargetMovePosition;
    public int mMoveSpeedOffset = 0;
    private float mMoveDelay;
    private float mCurrentMoveDelay;
    private Vector3 mMoveDirectVector;
}
