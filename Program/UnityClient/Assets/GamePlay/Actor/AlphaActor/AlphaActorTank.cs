﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorTank : AlphaActor
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vBodyTransform;
	public Transform vHeadTransform;

	public float vArraivedDistance = 1.0f; // Tank의 도착 거리, Gizmo가 해당 거리안에 있으면 arrived로 상태를 변경합니다.
	public int vAccelSpeed = 4; // 초당 가속
	public float vDecreasAccelSpeed = 20; // 초당 감속
	public int vMoveSpeed = 0;

	public Transform vFrontPoint;
	public Transform vBackLeftPoint;
	public Transform vBackRightPoint;
	public Transform vTerrainPosSet;
	public float vHeightOffset = 0.0f;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		mTargetMovePosition = new Vector3(0, 0, 0);
		if (vFrontPoint != null && vBackLeftPoint != null && vBackRightPoint != null)
			mIsMeshSlope = true;
		else
			mIsMeshSlope = false;

		mActorBodyState = AlphaActor.eActorState.Stop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트
	void Update()
	{
		if (mStageTroop == null)
			return;

// Debug Test
//vBodyTransform.rotation = mStageTroop.mTransform.rotation;
//vActorManager.aTransform.rotation = vBodyTransform.rotation;
//mTransform.position = mStageTroop.mTransform.position;
//vActorManager.aTransform.position = mTransform.position;
//return;
// Debug Test End
		// 이동시 방향 처리
		float lTargetDistance = Vector3.Distance(mTargetMovePosition, aBodyTransform.position);
		if (mActorBodyState == AlphaActor.eActorState.Move)
		{
			int lMaxMoveSpeed = mStageTroop.aBattleTroop.aMoveUpdater.aMoveSpeed;
			mMoveDirectVector = mTargetMovePosition - aBodyTransform.position;
			mMoveDirectVector = mMoveDirectVector.normalized;
			if (lTargetDistance > 0.1f)
			{
				vMoveSpeed += vAccelSpeed;
				if (vMoveSpeed >= lMaxMoveSpeed)
				{
					vMoveSpeed = lMaxMoveSpeed;
				}
				float lMoveValue = (vMoveSpeed * 0.01f) * Time.deltaTime;
				aBodyTransform.position = new Vector3(aBodyTransform.position.x + (mMoveDirectVector.x * lMoveValue),
												aBodyTransform.position.y + vActorManager.aHeightOffset,
												aBodyTransform.position.z + (mMoveDirectVector.z * lMoveValue));
			}
			else
			{
				mActorBodyState = AlphaActor.eActorState.Arrived;
			}
			float lNextRotationY = MathLib.GetDirectionY(mMoveDirectVector);
			_UpdateRotation(vBodyTransform, lNextRotationY, 0.0f);
			_UpdateRotation(vTerrainPosSet, lNextRotationY, 0.0f);
//			_UpdateTerrain();
		}
		else if (mActorBodyState == AlphaActor.eActorState.Arrived)
		{
			mMoveDirectVector = mTargetMovePosition - aBodyTransform.position;
			mMoveDirectVector = mMoveDirectVector.normalized;
			if (lTargetDistance > 0.1f)
			{
				float lMoveSpeed = Mathf.Min(lTargetDistance, (vMoveSpeed * 0.01f));

				float lMoveValue = lMoveSpeed * Time.deltaTime;
				aBodyTransform.position = new Vector3(aBodyTransform.position.x + (mMoveDirectVector.x * lMoveValue),
												  aBodyTransform.position.y + vActorManager.aHeightOffset,
												  aBodyTransform.position.z + (mMoveDirectVector.z * lMoveValue));
			}
			else
			{
				mActorBodyState = AlphaActor.eActorState.Stop;
				vMoveSpeed = 0;
				aBodyTransform.position = new Vector3(mTargetMovePosition.x, mTargetMovePosition.y + vActorManager.aHeightOffset, mTargetMovePosition.z);
			}
			float lNextRotationY = MathLib.GetDirectionY(mMoveDirectVector);
			_UpdateRotation(vBodyTransform, lNextRotationY, 0.0f);
			_UpdateRotation(vTerrainPosSet, lNextRotationY, 0.0f);
//			_UpdateTerrain();
		}
		else if (mActorBodyState == AlphaActor.eActorState.Rotation) // 제자리 회전
		{
			_UpdateRotation(vBodyTransform, mRotationValue, 0.0f);
			_UpdateRotation(vTerrainPosSet, mRotationValue, 0.0f);
//			_UpdateTerrain();
		}
		// 포탑 회전 처리
		if (mIsHeadRotation)
		{
			Vector3 lDirectVector = mAttackTargetPosition - aBodyTransform.position;
			float lRoatationDirectY = MathLib.GetDirectionY(lDirectVector.normalized);
			_UpdateRotation(vHeadTransform, lRoatationDirectY, 0.0f, true);
		}
	}
// 	//-------------------------------------------------------------------------------------------------------
// 	// 지형의 높이 및 경사를 계산 적용합니다.
// 	private void _UpdateTerrain()
// 	{
// 		//if (Terrain.activeTerrain == null)
// 		//	return;
// 		bool lIsActiveTerrain = (Terrain.activeTerrain != null) ? true : false;
// 
// 		// 차량의 중심점을 구합니다.
// 		Vector3 lCenterPosition = new Vector3((vFrontPoint.position.x + vBackLeftPoint.position.x + vBackRightPoint.position.x) / 3.0f,
// 											  (vFrontPoint.position.y + vBackLeftPoint.position.y + vBackRightPoint.position.y) / 3.0f,
// 											  (vFrontPoint.position.z + vBackLeftPoint.position.z + vBackRightPoint.position.z) / 3.0f);
// 		// 높이를 갱신합니다.
// 		float lTerrainHeight = 0.0f;
// 		if (lIsActiveTerrain)
// 		{
// 			lTerrainHeight = Terrain.activeTerrain.SampleHeight(lCenterPosition) + vHeightOffset;
// 		}
// 		else
// 		{
// 			lTerrainHeight = _GetMeshMapHeight(lCenterPosition) + vHeightOffset;
// 		}
// 		vBodyTransform.position = new Vector3(vBodyTransform.position.x,
// 											  lTerrainHeight + vActorManager.aHeightOffset,
// 											  vBodyTransform.position.z);
// 
// 
// 		if (mIsMeshSlope)
// 		{
// 			float lFrontHeight = 0.0f;
// 			float lBackLeftHeight = 0.0f;
// 			float lBackRightHeight = 0.0f;
// 			// 법선 벡터를 구해 차량을 회전합니다.
// 			if (lIsActiveTerrain)
// 			{
// 				lFrontHeight = Terrain.activeTerrain.SampleHeight(vFrontPoint.position);
// 				lBackLeftHeight = Terrain.activeTerrain.SampleHeight(vBackLeftPoint.position);
// 				lBackRightHeight = Terrain.activeTerrain.SampleHeight(vBackRightPoint.position);
// 			}
// 			else
// 			{
// 				lFrontHeight = _GetMeshMapHeight(vFrontPoint.position);
// 				lBackLeftHeight = _GetMeshMapHeight(vBackLeftPoint.position);
// 				lBackRightHeight = _GetMeshMapHeight(vBackRightPoint.position);
// 			}
// 
// 			vFrontPoint.position = new Vector3(vFrontPoint.position.x, lFrontHeight, vFrontPoint.position.z);
// 			vBackLeftPoint.position = new Vector3(vBackLeftPoint.position.x, lBackLeftHeight, vBackLeftPoint.position.z);
// 			vBackRightPoint.position = new Vector3(vBackRightPoint.position.x, lBackRightHeight, vBackRightPoint.position.z);
// 
// 			Vector3 lCenterNormalVector = Vector3.Cross((vFrontPoint.position - vBackLeftPoint.position), (vFrontPoint.position - vBackRightPoint.position));
// 
// 			Vector3 lForwardVector = vFrontPoint.position - lCenterPosition;
// 			if (mMoveDirectVector != Vector3.zero)
// 			{
// 				vBodyTransform.rotation = Quaternion.LookRotation(lForwardVector.normalized, -(lCenterNormalVector.normalized));
//				vActorManager.aTransform.rotation = vBodyTransform.rotation;
// 			}
// 		}
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void EnableActor()
	{
		mActorBodyState = eActorState.Stop;
		mActorTurretState = eActorState.Stop;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void DisableActor()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void Init(StageTroop pStageTroop)
	{
		base.Init(pStageTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void SetPosition(Coord2 pPosition)
	{
		aBodyTransform.position = Coord2.ToWorld(pPosition, vActorManager.aHeightOffset);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void MoveTo(Coord2 pMoveCoord, bool pIsArrived)
	{
		mTargetMovePosition = Coord2.ToWorld(pMoveCoord);

		if (!pIsArrived)
		{
			mActorBodyState = AlphaActor.eActorState.Move;
		}
		else
		{
			//if (vMoveSpeed <= 0)
			//{
			//	mActorBodyState = AlphaActor.eActorState.Stop;
			//}
			//else
			//{
			//	mActorBodyState = AlphaActor.eActorState.Arrived;
			//}
			mActorBodyState = AlphaActor.eActorState.Arrived;
		}
		mIsAttack = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
	{
		//if(mActorBodyState != AlphaActor.eActorState.Stop)
		//    mActorBodyState = AlphaActor.eActorState.Arrived;

		mAttackTargetPosition = Coord2.ToWorld(pTargetCoord);

		mIsHeadRotation = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoAttack(BattleFireEffect pFireEffect)
	{
		if (mIsAttack)
			return;
		
		mFireEffect = pFireEffect;
		
		// 방향 회전 처리
		//Vector3 lDirectVector = (vHeadTransform.position - Coord2.ToWorld(pFireEffect.aHitCoord));
		//vHeadTransform.rotation = MathLib.GetRotation(lDirectVector);


		if (mFireEffect.aFireWeaponData.mWeaponSpec.mAttackType == WeaponAttackType.TankGun)
			mIsHeadRotation = false;

		mStageTroop.StartCoroutine(_GoAttack());
	}
	public IEnumerator _GoAttack()
	{
		EffectUpdater lEffect = _GetAttackEffect(mFireEffect.aFireWeaponData.mWeaponIdx);
		if(lEffect != null)
			lEffect.gameObject.SetActive(true);

		yield return null;

		if (lEffect != null)
			lEffect.GoFire(vBodyTransform, Coord2.ToWorld(mFireEffect.aHitCoord), (float)(mFireEffect.aHitDelay / 1000.0f));
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void StopAttack()
	{
		mIsHeadRotation = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoBodyRotation(int pDirectionY)
	{
		if (mActorBodyState == AlphaActor.eActorState.Stop || mActorBodyState == AlphaActor.eActorState.Rotation)
		{
			mActorBodyState = AlphaActor.eActorState.Rotation;
			mRotationValue = pDirectionY;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoTurretRotation(int pDirectionY)
	{
		vHeadTransform.rotation = MathLib.GetRotation(pDirectionY);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsAttack = false;
	public bool mIsHeadRotation = false;
	private Vector3 mTargetMovePosition;
	private Vector3 mAttackTargetPosition;
	private Vector3 mMoveDirectVector;

	public float vDebugRotationValue;

	public bool mIsMeshSlope = false;

	private float mRotationValue = 0.0f; // 제자리 회전 값
}
