﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorTank2 : AlphaActor
{
	private const float cSqrPositionUpdateEndRange = 0.1f * 0.1f;
	private const float cArrivedDelay = 0.333f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vBodyTransform;
	public Transform vHeadTransform;

	public Vector3[] vThreeHeightPoints = { new Vector3(0.0f, 0.0f, 2.0f), new Vector3(-1.0f, 0.0f, -1.0f), new Vector3(1.0f, 0.0f, -1.0f) };
	public float vHeightOffset = 0.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
//		mTargetMovePosition = new Vector3(0, 0, 0);

		vThreeHeightPoints[0].Scale(aBodyTransform.localScale);
		vThreeHeightPoints[1].Scale(aBodyTransform.localScale);
		vThreeHeightPoints[2].Scale(aBodyTransform.localScale);

		mActorBodyState = AlphaActor.eActorState.Stop;
	}
	//-------------------------------------------------------------------------------------------------------
	// AlphaActor 메서드
	public override void SetActiveActor(bool pIsActive)
	{
		base.SetActiveActor(pIsActive);

		if (mIsActive)
		{
			mArrivedTimer = 0.0f;
			mLastUpdatePosition = aBodyTransform.position;
			mLastUpdateBodyDirectionY = mStageTroop.aBattleTroop.aDirectionY;
			mLastUpdateTurretDirectionY = mStageTroop.aBattleTroop.aTurretDirectionY;

			if (mStage.aMapHeight != null)
				_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);
			else
				aBodyTransform.rotation = MathLib.GetRotation(mLastUpdateBodyDirectionY);

			if (mStage.aMapHeight != null)
			{
				Vector3 lTurrectDirection = aBodyTransform.rotation * MathLib.GetRotation(mLastUpdateTurretDirectionY - mLastUpdateBodyDirectionY) * new Vector3(0.0f, 0.0f, 1.0f);
				vHeadTransform.rotation = Quaternion.LookRotation(lTurrectDirection, mBodyNormalVector);
			}
			else
			{
				vHeadTransform.rotation = MathLib.GetRotation(mLastUpdateTurretDirectionY);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트
	void Update()
	{
		mCurrentUpdateTime += Time.deltaTime;
	
		if (mStageTroop == null)
			return;

// 		if (mFrameDeltaTime <= 0)
// 			mFrameDeltaTime = 1;
// 		float lBodyFactor = (float)(mCurrentUpdateTime * 1000) / mFrameDeltaTime;
// 		float lTurretFactor = lBodyFactor;
// 		if ((mActorBodyState != AlphaActor.eActorState.Stop) &&
// 			(mActorBodyState != AlphaActor.eActorState.Rotation))
// 		{
// 			if (lBodyFactor > 1.0f)
// 			{
// 				mTransform.position = mTargetMovePosition;
// 				mActorBodyState = AlphaActor.eActorState.Stop;
// 				lBodyFactor = 1.0f;
// 			}
// 			else
// 			{
// 				mTransform.position = Vector3.Lerp(mOriginPosition, mTargetMovePosition, lBodyFactor);
// 			}

// 			float lLerpRotation = MathUtil.LerpDegree(vBodyTransform.rotation.eulerAngles.y, mBodyRotationValue, lBodyFactor);
// 			Quaternion lTurretRotation = vHeadTransform.rotation;
// 			vBodyTransform.rotation = Quaternion.Euler(vBodyTransform.rotation.x, lLerpRotation, vBodyTransform.rotation.z);
//			vActorManager.aTransform.rotation = vBodyTransform.rotation;
//			vTerrainPosSet.rotation = Quaternion.Euler(vTerrainPosSet.rotation.x, lLerpRotation, vTerrainPosSet.rotation.z);
//			_UpdateTerrain();

// 			vHeadTransform.rotation = lTurretRotation;
// 		}

// 		if (mActorTurretState != AlphaActor.eActorState.Stop)
// 		{
// 			if (lTurretFactor > 1.0f || mActorTurretState == AlphaActor.eActorState.Rotation)
// 			{
// 				mActorTurretState = AlphaActor.eActorState.Stop;
// 				lTurretFactor = 1.0f;
// 			}

// 			float lLerpRotation = MathUtil.LerpDegree(vHeadTransform.rotation.eulerAngles.y, mTurretRotationValue, lTurretFactor);
// 			vHeadTransform.rotation = Quaternion.Euler(vHeadTransform.rotation.x, lLerpRotation, vHeadTransform.rotation.z);

// 		}

		if (mLastUpdateBodyDirectionY != mStageTroop.aBattleTroop.aDirectionY)
		{
			mLastUpdateBodyDirectionY = mStageTroop.aBattleTroop.aDirectionY;
			mArrivedTimer = cArrivedDelay;
		}

		if (mArrivedTimer > 0.0f)
		{
			if (vActorManager.aHeightOffset > 0.0f)
				aBodyTransform.position = Vector3.Lerp(aBodyTransform.position + new Vector3(0.0f, vActorManager.aHeightOffset, 0.0f), Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset), Time.deltaTime * 2.0f);
			else
				aBodyTransform.position = Vector3.Lerp(aBodyTransform.position, Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord), Time.deltaTime * 2.0f);

			if (mStage.aMapHeight != null)
				_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);
			else
				aBodyTransform.rotation = MathLib.GetRotation(mLastUpdateBodyDirectionY);

			Vector2 lMoveVector = new Vector2(aBodyTransform.position.x - mLastUpdatePosition.x, aBodyTransform.position.z - mLastUpdatePosition.z);
			if (lMoveVector.sqrMagnitude <= cSqrPositionUpdateEndRange)
				mArrivedTimer -= Time.deltaTime;
			else
				mArrivedTimer = cArrivedDelay;
			mLastUpdatePosition = aBodyTransform.position;
		}

		if (mLastUpdateTurretDirectionY != mStageTroop.aBattleTroop.aTurretDirectionY)
		{
			mLastUpdateTurretDirectionY = mStageTroop.aBattleTroop.aTurretDirectionY;

			if (mStage.aMapHeight != null)
			{
				Vector3 lTurrectDirection = aBodyTransform.rotation * MathLib.GetRotation(mLastUpdateTurretDirectionY - mLastUpdateBodyDirectionY) * new Vector3(0.0f, 0.0f, 1.0f);
				vHeadTransform.rotation = Quaternion.LookRotation(lTurrectDirection, mBodyNormalVector);
			}
			else
			{
				vHeadTransform.rotation = MathLib.GetRotation(mLastUpdateTurretDirectionY);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 플레이별 업데이트
	public override void OnFrameUpdate(int pDeltaTime)
	{
		if (mActorBodyState != AlphaActor.eActorState.Arrived)
		{
// 			mFrameDeltaTime = pDeltaTime;
			mCurrentUpdateTime = 0.0f;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형의 높이 및 경사를 계산 적용합니다.
// 	private void _UpdateTerrain()
// 	{
// 		bool lIsActiveTerrain = (Terrain.activeTerrain != null) ? true : false;
// 
// 		// 차량의 중심점을 구합니다.
// 		Vector3 lCenterPosition = new Vector3((vFrontPoint.position.x + vBackLeftPoint.position.x + vBackRightPoint.position.x) / 3.0f,
// 											  (vFrontPoint.position.y + vBackLeftPoint.position.y + vBackRightPoint.position.y) / 3.0f,
// 											  (vFrontPoint.position.z + vBackLeftPoint.position.z + vBackRightPoint.position.z) / 3.0f);
// 		// 높이를 갱신합니다.
// 		float lTerrainHeight = 0.0f;
// 		if (lIsActiveTerrain)
// 		{
// 			lTerrainHeight = Terrain.activeTerrain.SampleHeight(lCenterPosition) + vHeightOffset;
// 		}
// 		else
// 		{
// 			lTerrainHeight = _GetMeshMapHeight(lCenterPosition) + vHeightOffset;
// 		}
// 		vBodyTransform.position = new Vector3(vBodyTransform.position.x,
// 											  lTerrainHeight + vActorManager.aHeightOffset,
// 											  vBodyTransform.position.z);
// 
// 
// 		if (mIsMeshSlope)
// 		{
// 			float lFrontHeight = 0.0f;
// 			float lBackLeftHeight = 0.0f;
// 			float lBackRightHeight = 0.0f;
// 			// 법선 벡터를 구해 차량을 회전합니다.
// 			if (lIsActiveTerrain)
// 			{
// 				lFrontHeight = Terrain.activeTerrain.SampleHeight(vFrontPoint.position);
// 				lBackLeftHeight = Terrain.activeTerrain.SampleHeight(vBackLeftPoint.position);
// 				lBackRightHeight = Terrain.activeTerrain.SampleHeight(vBackRightPoint.position);
// 			}
// 
// 			vFrontPoint.position = new Vector3(vFrontPoint.position.x, lFrontHeight, vFrontPoint.position.z);
// 			vBackLeftPoint.position = new Vector3(vBackLeftPoint.position.x, lBackLeftHeight, vBackLeftPoint.position.z);
// 			vBackRightPoint.position = new Vector3(vBackRightPoint.position.x, lBackRightHeight, vBackRightPoint.position.z);
// 
// 			Vector3 lCenterNormalVector = Vector3.Cross((vFrontPoint.position - vBackLeftPoint.position), (vFrontPoint.position - vBackRightPoint.position));
// 
// 			Vector3 lForwardVector = vFrontPoint.position - lCenterPosition;
// 			vBodyTransform.rotation = Quaternion.LookRotation(lForwardVector.normalized, -(lCenterNormalVector.normalized));
// 		}
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void EnableActor()
	{
		mActorBodyState = eActorState.Stop;
		mActorTurretState = eActorState.Stop;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void DisableActor()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void Init(StageTroop pStageTroop)
	{
		base.Init(pStageTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void SetPosition(Coord2 pPosition)
	{
		aBodyTransform.position = Coord2.ToWorld(pPosition, vActorManager.aHeightOffset);

		if (mStage.aMapHeight != null)
			_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);

		if (mStage.aMapHeight != null)
		{
			Vector3 lTurrectDirection = aBodyTransform.rotation * MathLib.GetRotation(mLastUpdateTurretDirectionY - mLastUpdateBodyDirectionY) * new Vector3(0.0f, 0.0f, 1.0f);
			vHeadTransform.rotation = Quaternion.LookRotation(lTurrectDirection, mBodyNormalVector);
		}

		mLastUpdatePosition = aBodyTransform.position;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void MoveTo(Coord2 pMoveCoord, bool pIsArrived)
	{
// 		if (mIsActive)
// 		{
// 			if (pIsArrived)
// 				mActorBodyState = AlphaActor.eActorState.Arrived;
// 			else
// 				mActorBodyState = AlphaActor.eActorState.Move;
// 			mOriginPosition = mTransform.position;
// 			mTargetMovePosition = Coord2.ToWorld(pMoveCoord);
// 		}
// 		else
// 		{
// 			mActorBodyState = AlphaActor.eActorState.Arrived;
// 			mTargetMovePosition = Coord2.ToWorld(pMoveCoord);
// 			mTransform.position = mTargetMovePosition;
// 		}
// if (vActorManager.aHeightOffset > 0.0f)
// 	aBodyTransform.position = Vector3.Lerp(aBodyTransform.position + new Vector3(0.0f, vActorManager.aHeightOffset, 0.0f), Coord2.ToWorld(pMoveCoord, vActorManager.aHeightOffset), Time.deltaTime * 2.0f);
// else
// 	aBodyTransform.position = Vector3.Lerp(aBodyTransform.position, Coord2.ToWorld(pMoveCoord), Time.deltaTime * 2.0f);
// aBodyTransform.rotation = MathLib.GetRotation(mStageTroop.aBattleTroop.aDirectionY);
// //vHeadTransform.rotation = MathLib.GetRotation(mStageTroop.aBattleTroop.aTurretDirectionY);

mArrivedTimer = cArrivedDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoAttack(BattleFireEffect pFireEffect)
	{
		if (mIsAttack)
			return;

		mFireEffect = pFireEffect;

		mStageTroop.StartCoroutine(_GoAttack());
	}
	public IEnumerator _GoAttack()
	{
		EffectUpdater lEffect = _GetAttackEffect(mFireEffect.aFireWeaponData.mWeaponIdx);
		if (lEffect != null)
			lEffect.gameObject.SetActive(true);

		yield return null;

		if (lEffect != null)
		{
			TroopSpec lTroopSpec = mStageTroop.aBattleTroop.aTroopSpec;
			Transform lFireTransform;
			if (lTroopSpec.mIsTurretWeapons[mFireEffect.aFireWeaponData.mWeaponIdx])
				lFireTransform = vHeadTransform;
			else
				lFireTransform = vBodyTransform;
			lEffect.GoFire(lFireTransform, Coord2.ToWorld(mFireEffect.aHitCoord), (float)(mFireEffect.aHitDelay / 1000.0f));
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void StopAttack()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoBodyRotation(int pDirectionY)
	{
		mActorBodyState = AlphaActor.eActorState.Rotation;
//		mBodyRotationValue = pDirectionY;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoTurretRotation(int pDirectionY)
	{
		mActorTurretState = AlphaActor.eActorState.Rotation;
//		mTurretRotationValue = pDirectionY;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsAttack = false;
// 	private Vector3 mOriginPosition;
//	private Vector3 mTargetMovePosition;

	public float vDebugRotationValue;

	public bool mIsMeshSlope = false;

// 	private float mBodyRotationValue = 0.0f; // 회전 값
// 	private float mTurretRotationValue = 0.0f; // 회전 값

	private float mArrivedTimer;
	private Vector3 mLastUpdatePosition;
	private int mLastUpdateBodyDirectionY;
	private int mLastUpdateTurretDirectionY;
}
