﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AlphaActorVehicle : AlphaActor
{
	private const float cSqrPositionUpdateEndRange = 0.1f * 0.1f;
	private const float cArrivedDelay = 0.333f;
	
	//-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
    public int vAccelSpeed = 4; // 초당 가속
    public int vDecreasAccelSpeed = 20; // 초당 감속
    public int vMoveSpeed = 0;

    public Transform vBodyTransform;

	public Vector3[] vThreeHeightPoints = { new Vector3(0.0f, 0.0f, 2.0f), new Vector3(-1.0f, 0.0f, -1.0f), new Vector3(1.0f, 0.0f, -1.0f) };
	public float vHeightOffset = 0.0f;

	//-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
        if (vBodyTransform == null)
            Debug.LogError("vBodyTranform is null - " + gameObject.name);
//		mTargetMovePosition = Vector3.zero;

// 		if (vFrontPoint != null && vBackLeftPoint != null && vBackRightPoint != null)
// 			mIsMeshSlope = true;
// 		else
// 			mIsMeshSlope = false;

		vThreeHeightPoints[0].Scale(aBodyTransform.localScale);
		vThreeHeightPoints[1].Scale(aBodyTransform.localScale);
		vThreeHeightPoints[2].Scale(aBodyTransform.localScale);
	}
	//-------------------------------------------------------------------------------------------------------
	// AlphaActor 메서드
	public override void SetActiveActor(bool pIsActive)
	{
		base.SetActiveActor(pIsActive);

		if (mIsActive)
		{
			mArrivedTimer = 0.0f;
			mLastUpdatePosition = aBodyTransform.position;
			mLastUpdateBodyDirectionY = mStageTroop.aBattleTroop.aDirectionY;

			if (mStage.aMapHeight != null)
				_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);
			else
				aBodyTransform.rotation = MathLib.GetRotation(mLastUpdateBodyDirectionY);
		}
	}
	//-------------------------------------------------------------------------------------------------------
    // 업데이트
    void Update()
    {
		mCurrentUpdateTime += Time.deltaTime;
	
		if (mStageTroop == null)
			return;

// 		if (mFrameDeltaTime <= 0)
// 			mFrameDeltaTime = 1;
// 		float lFactor = (float)(mCurrentUpdateTime * 1000) / mFrameDeltaTime;
// 		if ((mActorBodyState != AlphaActor.eActorState.Stop) &&
// 			(mActorBodyState != AlphaActor.eActorState.Rotation))
// 		{
// 			if (lFactor > 1.0f)
// 			{
// 				mTransform.position = mTargetMovePosition;
// 				mActorBodyState = AlphaActor.eActorState.Stop;
// 				lFactor = 1.0f;
// 			}
// 			else
// 			{
// 				mTransform.position = Vector3.Lerp(mOriginPosition, mTargetMovePosition, lFactor);
// 			}

// 			float lLerpRotation = MathUtil.LerpDegree(vBodyTransform.rotation.eulerAngles.y, mBodyRotationValue, lFactor);
// 			vBodyTransform.rotation = Quaternion.Euler(vBodyTransform.rotation.x, lLerpRotation, vBodyTransform.rotation.z);
//			vTerrainPosSet.rotation = Quaternion.Euler(vTerrainPosSet.rotation.x, lLerpRotation, vTerrainPosSet.rotation.z);
// 			_UpdateTerrain();

// 		}

		if (mLastUpdateBodyDirectionY != mStageTroop.aBattleTroop.aDirectionY)
		{
			mLastUpdateBodyDirectionY = mStageTroop.aBattleTroop.aDirectionY;
			mArrivedTimer = cArrivedDelay;
		}

		if (mArrivedTimer > 0.0f)
		{
			if (vActorManager.aHeightOffset > 0.0f)
				aBodyTransform.position = Vector3.Lerp(aBodyTransform.position + new Vector3(0.0f, vActorManager.aHeightOffset, 0.0f), Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord, vActorManager.aHeightOffset), Time.deltaTime * 2.0f);
			else
				aBodyTransform.position = Vector3.Lerp(aBodyTransform.position, Coord2.ToWorld(mStageTroop.aBattleTroop.aCoord), Time.deltaTime * 2.0f);

			if (mStage.aMapHeight != null)
				_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);
			else
				aBodyTransform.rotation = MathLib.GetRotation(mLastUpdateBodyDirectionY);

			Vector2 lMoveVector = new Vector2(aBodyTransform.position.x - mLastUpdatePosition.x, aBodyTransform.position.z - mLastUpdatePosition.z);
			if (lMoveVector.sqrMagnitude <= cSqrPositionUpdateEndRange)
				mArrivedTimer -= Time.deltaTime;
			else
				mArrivedTimer = cArrivedDelay;
			mLastUpdatePosition = aBodyTransform.position;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 플레이별 업데이트
	public override void OnFrameUpdate(int pDeltaTime)
	{
		if (mActorBodyState != AlphaActor.eActorState.Arrived)
		{
// 			mFrameDeltaTime = pDeltaTime;
			mCurrentUpdateTime = 0.0f;
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void EnableActor()
    {
		mActorBodyState = eActorState.Stop;
		mActorTurretState = eActorState.Stop;
	}
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void DisableActor()
    {

    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void Init(StageTroop pStageTroop)
    {
        base.Init(pStageTroop);
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void SetPosition(Coord2 pPosition)
    {
		aBodyTransform.position = Coord2.ToWorld(pPosition, vActorManager.aHeightOffset);

		if (mStage.aMapHeight != null)
			_UpdateBodySlope(vThreeHeightPoints, mLastUpdateBodyDirectionY);

		mLastUpdatePosition = aBodyTransform.position;
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
	public override void MoveTo(Coord2 pMoveCoord, bool pIsArrived)
    {
// 		if (mIsActive)
// 		{
// 			if (pIsArrived)
// 				mActorBodyState = AlphaActor.eActorState.Arrived;
// 			else
// 				mActorBodyState = AlphaActor.eActorState.Move;
// 			mOriginPosition = mTransform.position;
// 			mTargetMovePosition = Coord2.ToWorld(pMoveCoord);
// 		}
// 		else
// 		{
// 			mActorBodyState = AlphaActor.eActorState.Arrived;
// 			mTargetMovePosition = Coord2.ToWorld(pMoveCoord);
// 			mTransform.position = mTargetMovePosition;
// 		}
// 
// if (vActorManager.aHeightOffset > 0.0f)
// 	aBodyTransform.position = aBodyTransform.position = Vector3.Lerp(aBodyTransform.position + new Vector3(0.0f, vActorManager.aHeightOffset, 0.0f), Coord2.ToWorld(pMoveCoord, vActorManager.aHeightOffset), Time.deltaTime * 2.0f);
// else
// 	aBodyTransform.position = Vector3.Lerp(aBodyTransform.position, Coord2.ToWorld(pMoveCoord), Time.deltaTime * 2.0f);
// aBodyTransform.rotation = MathLib.GetRotation(mStageTroop.aBattleTroop.aDirectionY);

mArrivedTimer = cArrivedDelay;
	}
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void GoPreAttack(int pDelayTime, Coord2 pTargetCoord)
    {
    }
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
	public override void GoAttack(BattleFireEffect pFireEffect)
    {
        if (mIsAttack)
            return;
        // 방향 회전 처리
		//Vector3 lDirectVector = (mTransform.position - Coord2.ToWorld(pFireEffect.aHitCoord));
		//mTransform.rotation = MathLib.GetRotation(lDirectVector);
		//vActorManager.aTransform.rotation = mTransform.rotation;

		mFireEffect = pFireEffect;

		mStageTroop.StartCoroutine(_GoAttack());
    }
	public IEnumerator _GoAttack()
	{
		//EffectUpdater lEffect = mStageTroop.aStage.vEffectPool.CreateEffector(vFireEffectId1st, transform.position);
		EffectUpdater lEffect = _GetAttackEffect(mFireEffect.aFireWeaponData.mWeaponIdx);
		if (lEffect != null)
			lEffect.gameObject.SetActive(true);

		yield return null;

		if (lEffect != null)
			lEffect.GoFire(transform, Coord2.ToWorld(mFireEffect.aHitCoord), (float)(mFireEffect.aHitDelay / 1000.0f));
	}
    //-------------------------------------------------------------------------------------------------------
    // Actor 메서드
    public override void StopAttack()
    {
    }
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoBodyRotation(int pDirectionY)
	{
		mActorBodyState = AlphaActor.eActorState.Rotation;
	}
	//-------------------------------------------------------------------------------------------------------
	// Actor 메서드
	public override void GoTurretRotation(int pDirectionY)
	{
	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
	// 지형의 높이 및 경사를 계산 적용합니다.
// 	private void _UpdateTerrain()
// 	{
// 		if (Terrain.activeTerrain == null)
// 			return;
// 
// 		// 차량의 중심점을 구합니다.
// 		Vector3 lCenterPosition = new Vector3((vFrontPoint.position.x + vBackLeftPoint.position.x + vBackRightPoint.position.x) / 3.0f,
// 											  (vFrontPoint.position.y + vBackLeftPoint.position.y + vBackRightPoint.position.y) / 3.0f,
// 											  (vFrontPoint.position.z + vBackLeftPoint.position.z + vBackRightPoint.position.z) / 3.0f);
// 		// 높이를 갱신합니다.
// 		float lTerrainHeight = Terrain.activeTerrain.SampleHeight(lCenterPosition) + vHeightOffset;
// 		vBodyTransform.position = new Vector3(vBodyTransform.position.x,
// 											  lTerrainHeight,
// 											  vBodyTransform.position.z);
// 
// 
// 		if (mIsMeshSlope)
// 		{
// 			// 법선 벡터를 구해 차량을 회전합니다.
// 			float lFrontHeight = Terrain.activeTerrain.SampleHeight(vFrontPoint.position);
// 			float lBackLeftHeight = Terrain.activeTerrain.SampleHeight(vBackLeftPoint.position);
// 			float lBackRightHeight = Terrain.activeTerrain.SampleHeight(vBackRightPoint.position);
// 
// 			vFrontPoint.position = new Vector3(vFrontPoint.position.x, lFrontHeight, vFrontPoint.position.z);
// 			vBackLeftPoint.position = new Vector3(vBackLeftPoint.position.x, lBackLeftHeight, vBackLeftPoint.position.z);
// 			vBackRightPoint.position = new Vector3(vBackRightPoint.position.x, lBackRightHeight, vBackRightPoint.position.z);
// 
// 			Vector3 lCenterNormalVector = Vector3.Cross((vFrontPoint.position - vBackLeftPoint.position), (vFrontPoint.position - vBackRightPoint.position));
// 
// 			Vector3 lForwardVector = vFrontPoint.position - lCenterPosition;
// 			vBodyTransform.rotation = Quaternion.LookRotation(lForwardVector.normalized, -(lCenterNormalVector.normalized));
//			vActorManager.aTransform.rotation = vBodyTransform.rotation;
// 		}
// 	}

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private bool mIsAttack = false;
//	private bool mIsMeshSlope = false;
//	private Vector3 mTargetMovePosition;
// 	private Vector3 mMoveDirectVector;
// 	private Vector3 mOriginPosition;
	public float vDebugRotationValue;

	private float mArrivedTimer;
	private Vector3 mLastUpdatePosition;
	private int mLastUpdateBodyDirectionY;
}
