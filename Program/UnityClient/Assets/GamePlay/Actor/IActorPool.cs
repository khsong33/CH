﻿using UnityEngine;
using System.Collections;

public interface IActorPool
{
    // ActorManager를 생성합니다.
	IActorManager CreateActorManager(StageTroop pStageTroop);
}
