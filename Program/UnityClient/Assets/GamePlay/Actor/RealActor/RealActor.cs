﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class RealActor : MonoBehaviour
{
	public delegate OnUpdateDelegate OnStartDelegate();
	public delegate OnUpdateDelegate OnUpdateDelegate(float pUpdateDelta);

	//-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public RealActorManager vActorManager;

	public float vMoveAccel = 0.0f;

    //-------------------------------------------------------------------------------------------------------
    // 속성
	//-------------------------------------------------------------------------------------------------------
	// (액터 매니저에서의) 인덱스
	public int aActorIdx
	{
		get { return mActorIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 메인 액터
	public RealActor aNextMainActor
	{
		get { return mNextMainActor; }
		set { mNextMainActor = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 정보
	public RealActorManager.ActorInfo aManagerActorInfo
	{
		get { return mManagerActorInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroop 접근
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// Stage 접근
	public Stage aStage
	{
		get { return mStage; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 좌표
	public Vector3 aActorPosition
	{
		get { return mPositionUpdater.aPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 방향각
	public float aActorDirectionY
	{
		get { return mBodyRotationUpdater.aDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 갱신자
	public RealActorPositionUpdater aPositionUpdater
	{
		get { return mPositionUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체 회전 갱신자
	public RealActorRotationUpdater aBodyRotationUpdater
	{
		get { return mBodyRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체 무기 회전 갱신자
	public RealActorRotationUpdater aBodyWeaponRotationUpdater
	{
		get { return mBodyWeaponRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 터렛 회전 갱신자
	public RealActorRotationUpdater aTurretRotationUpdater
	{
		get { return mTurretRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태를 표현하는 리드 액터인지 여부
	public bool aIsMainActor
	{
		get { return (vActorManager.aMainActor == this); }
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화될 때 호출됩니다.
	public virtual void OnNewActor(RealActorManager pActorManager, int pActorIdx, RealActorManager.ActorInfo pManagerActorInfo)
	{
		vActorManager = pActorManager;
		mActorIdx = pActorIdx;
		mManagerActorInfo = pManagerActorInfo;
		mTransform = transform;
		mTransform.parent = pActorManager.vActorPool.vActorRoot;

		mPositionUpdater = new RealActorPositionUpdater(this, mManagerActorInfo.vPositioningType);

		mBodyRotationUpdater = new RealActorRotationUpdater(this);
		mBodyWeaponRotationUpdater = new RealActorRotationUpdater(this);
		mTurretRotationUpdater = new RealActorRotationUpdater(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성될 때 호출됩니다(풀링 고려).
	public virtual void OnCreateActor()
	{
		gameObject.SetActive(false);

		mBattleTroop = vActorManager.vStageTroop.aBattleTroop;
		mStage = vActorManager.aStage;

		mPositionUpdater.Init(mBattleTroop.aMoveUpdater);

		mBodyRotationUpdater.Init(mBattleTroop.aBodyRotationUpdater);
		if (mBattleTroop.aBodyWeaponRotationUpdater.aIsValid)
			mBodyWeaponRotationUpdater.Init(mBattleTroop.aBodyWeaponRotationUpdater);
		if (mBattleTroop.aTurretRotationUpdater.aIsValid)
			mTurretRotationUpdater.Init(mBattleTroop.aTurretRotationUpdater);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public virtual void OnDestroyActor()
	{
		mBattleTroop = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 보여질 때 호출됩니다.
	public virtual void OnVisibleActor()
	{
		gameObject.SetActive(true);

		mBodyRotationUpdater.Set(mBattleTroop.aBodyRotationUpdater.aRotater.aRotationDirectionY);
		if (mBattleTroop.aBodyWeaponRotationUpdater.aIsValid)
			mBodyWeaponRotationUpdater.Set(mBattleTroop.aBodyWeaponRotationUpdater.aRotater.aRotationDirectionY);
		if (mBattleTroop.aTurretRotationUpdater.aIsValid)
			mTurretRotationUpdater.Set(mBattleTroop.aTurretRotationUpdater.aRotater.aRotationDirectionY);

		mPositionUpdater.Set(Coord2.ToWorld(mBattleTroop.aCoord)); // 방향에 따라 위치가 바뀔 수도 있으므로 나중에 초기화
	}
	//-------------------------------------------------------------------------------------------------------
	// 숨겨질 때 호출됩니다.
	public virtual void OnInvisibleActor()
	{
		gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신될 때 호출됩니다.
	public virtual void OnUpdateActor(float pUpdateDelta)
	{
		if (mPositionUpdater.aIsToUpdate)
		{
			mPositionUpdater.UpdatePosition(pUpdateDelta);
			vActorManager.aPositioning.NotifyActorMove(); // 액터 매니저가 인지하도록
		}

		if (mBodyRotationUpdater.aIsToUpdate)
			mBodyRotationUpdater.UpdateRotation(pUpdateDelta);
		if (mBodyWeaponRotationUpdater.aIsToUpdate)
			mBodyWeaponRotationUpdater.UpdateRotation(pUpdateDelta);
		if (mTurretRotationUpdater.aIsToUpdate)
			mTurretRotationUpdater.UpdateRotation(pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 사기 상태가 바뀔 때 호출됩니다.
	public virtual void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표와 방향각을 지정합니다.
	public virtual void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		mPositionUpdater.Set(Coord2.ToWorld(pCoord));
		mBodyRotationUpdater.Set(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 목표 이동이 지정될 때 호출됩니다.
	public virtual void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
		mPositionUpdater.SetDstPosition(Coord2.ToWorld(pDstCoord), pArrivalDelay * MathUtil.cMilliToSec, pStopType);
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체 회전이 지정될 때 호출됩니다.
	public virtual void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
		if (!mPositionUpdater.aIsToUpdate) // 이동중에는 몸통 회전을 이동 갱신자가 제어함
			mBodyRotationUpdater.SetDstDirectionY(pDstBodyDirectionY, pArrivalDelay * MathUtil.cMilliToSec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 몸체 무기 회전이 지정될 때 호출됩니다.
	public virtual void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
		mBodyWeaponRotationUpdater.SetDstDirectionY(pDstBodyWeaponDirectionY, pArrivalDelay * MathUtil.cMilliToSec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 터렛 회전이 지정될 때 호출됩니다.
	public virtual void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
		mTurretRotationUpdater.SetDstDirectionY(pDstTurretDirectionY, pArrivalDelay * MathUtil.cMilliToSec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 멈춤과 계속이 바뀔 때 호출됩니다.
	public virtual void OnHoldMove(bool pIsHoldMove)
	{
		if (mPositionUpdater.aIsToUpdate)
			mPositionUpdater.SetHoldMove(pIsHoldMove);
	}
	//-------------------------------------------------------------------------------------------------------
	// 멈출 때 호출됩니다.
	public virtual void OnStopMove(Coord2 pStopCoord)
	{
		if (mPositionUpdater.aIsToUpdate)
			mPositionUpdater.SetStopMove(Coord2.ToWorld(pStopCoord));
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 설치합니다.
	public virtual void OnInstallWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 설치를 해제합니다.
	public virtual void OnUnInstallWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 조준한 상태에서 회전합니다.
	public virtual void OnTrackWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 야포의 와인드업 액션을 합니다.
	public virtual void OnWindupWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 조준합니다.
	public virtual void OnAimWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 발사합니다.
	public virtual void OnFireWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 발사 후에 쿨다운이 끝났을 때의 처리를 합니다.
	public virtual void OnEndCooldown()
	{
	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
	//-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
	private int mActorIdx;
	private RealActor mNextMainActor;
	private RealActorManager.ActorInfo mManagerActorInfo;
	private Transform mTransform;

	private BattleTroop mBattleTroop;
	private Stage mStage;

	private RealActorPositionUpdater mPositionUpdater;
	private RealActorRotationUpdater mBodyRotationUpdater;
	private RealActorRotationUpdater mBodyWeaponRotationUpdater;
	private RealActorRotationUpdater mTurretRotationUpdater;
}
