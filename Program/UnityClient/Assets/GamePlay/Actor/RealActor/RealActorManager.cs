﻿//#define TEST_SINGLE_ACTOR // 테스트로 하나의 액터만 보이게 합니다.

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorManager : MonoBehaviour, IActorManager
{
	[Serializable]
	public class ActorInfo
	{
		public bool vCanBeMainActor = true;
		public RealActor vActorPrefab;

		public RealActorPositionUpdater.PositioningType vPositioningType = RealActorPositionUpdater.PositioningType.CenterOffset;
		public Vector3 vPositioningOffset = Vector3.zero;
		public float vPositioningRandomInRange = 0.0f;

		public float vMaxActionDelay2 = 0.5f;
	}

	private const float cAircraftHeight = 30.0f;
	private const float cHidingTransparency = 0.333f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public RealActorPool vActorPool;
	public int vActorManagerIndex;

	public StageTroop vStageTroop;
	public uint vTroopGuid;

	public ActorInfo[] vActorInfos;
	public RealActor[] vActors;
	public SphereCollider vSphereCollider;

	public RealActorManagerPositioning.PositioningType vPositioningType = RealActorManagerPositioning.PositioningType.ActorsCenter;
	
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public Collider aCollider
	{
		get { return vSphereCollider; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public float aVisualRadius
	{
		get { return mVisualRadius; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public bool aIsVisible
	{
		get { return mIsVisible; }
		set
		{
			if (mIsVisible != value)
				_SetVisible(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 속성
	public bool aIsHiding
	{
		get { return mIsHiding; }
		set
		{
			if (mIsHiding != value)
				_SetHiding(value);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroop 접근
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// Stage 접근
	public Stage aStage
	{
		get { return mStage; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표 지정
	public RealActorManagerPositioning aPositioning
	{
		get { return mPositioning; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터를 갱신한 순간의 시간
	public float aActorUpdateTime
	{
		get { return mActorUpdateTime; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태를 표현하는 메인 액터
	public RealActor aMainActor
	{
		get { return mMainActor; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("RealActorManager@{0}", vStageTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화될 때 호출됩니다.
	public void OnNewActorManager(RealActorPool pActorPool, int pActorManagerIndex)
	{
		mTransform = transform;

		if (vSphereCollider == null)
			Debug.LogError("vSphereCollider is empty - " + this);
		mVisualRadius = vSphereCollider.radius;
		float lColliderRadius = vSphereCollider.radius * mTransform.localScale.x;
		if (lColliderRadius < BattleConfig.get.mMinTouchRadius)
			lColliderRadius = BattleConfig.get.mMinTouchRadius;
		vSphereCollider.radius = lColliderRadius / mTransform.localScale.x;

		vActorPool = pActorPool;
		vActorManagerIndex = pActorManagerIndex;
		mTransform.parent = vActorPool.vActorManagerRoot;

		// 충돌체에 StageTroop에 대한 태그를 붙입니다.
		mStageTroopTag = vSphereCollider.gameObject.AddComponent<StageTroopTag>();

		// 액터를 할당합니다.
		_AllocateActors();

		// 이동 갱신자를 생성합니다.
		mPositioning = new RealActorManagerPositioning(this, vPositioningType);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성될 때 호출됩니다.
	public void OnCreateActorManager(StageTroop pStageTroop)
	{
		vStageTroop = pStageTroop;

		mBattleTroop = pStageTroop.aBattleTroop;
		mStage = pStageTroop.aStage;

		vTroopGuid = pStageTroop.aBattleTroop.aGuid;
		mTransform.position = vStageTroop.aTransform.position;
		mStageTroopTag.vStageTroop = pStageTroop;

		vStageTroop.SetMarkActorTransform(mTransform);

		_SetVisible(false);
		_SetHiding(false);
		_SetShowMesh(true);

		for (int iActor = 0; iActor < vActors.Length; ++iActor)
			if (vActors[iActor] != null)
				vActors[iActor].OnCreateActor();

		OnSetCoordAndDirectionY(mBattleTroop.aCoord, mBattleTroop.aDirectionY);

		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다.
	public void OnDestroyActorManager()
	{
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;

		if (mIsVisible)
			_SetVisible(false);

		for (int iActor = 0; iActor < vActors.Length; ++iActor)
			if (vActors[iActor] != null)
				vActors[iActor].OnDestroyActor();
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void Destroy()
	{
		vActorPool.DestoryActorManager(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSpawn()
	{
		if (mBattleTroop.aCommander.aCommanderSpec.mSpawnType == CommanderSpawnType.Aircraft)
			StartCoroutine(_UpdateAircraftSpawn());
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		for (int iActor = 0; iActor < vActors.Length; ++iActor)
			if (vActors[iActor] != null)
				vActors[iActor].SetCoordAndDirectionY(pCoord, pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
// 		for (int iActor = 0; iActor < mActorList.Count; iActor++)
// 		{
// 			mActorList[iActor].OnFrameUpdate(pDeltaTime);
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
		for (int iActor = 0; iActor < vActors.Length; ++iActor)
			if (vActors[iActor] != null)
				vActors[iActor].OnUpdateMoraleState(pTroopMoraleState);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
		if (!mIsMoving)
			ChangeMainActor(); // 이동을 시작할 때 메인 액터를 교체
		mIsMoving = true; // 이제부터 이동중으로 간주

		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnSetDstCoord(pDstCoord, pArrivalDelay, pStopType);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnSetDstBodyDirectionY(pDstBodyDirectionY, pArrivalDelay);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnSetDstBodyWeaponDirectionY(pDstBodyWeaponDirectionY, pArrivalDelay);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnSetDstTurretDirectionY(pDstTurretDirectionY, pArrivalDelay);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnHoldMove(bool pIsHoldMove)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnHoldMove(pIsHoldMove);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopMove(Coord2 pStopCoord)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnStopMove(pStopCoord);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnMoveTo(Coord2 pMoveCoord, bool pIsArrived)
	{
		// 아무 처리도 하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateBodyToDirectionY(int pDirectionY)
	{
		// 아무 처리도 하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateBodyWeaponToDirectionY(int pDirectionY)
	{
		// 아무 처리도 하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnRotateTurretToDirectionY(int pDirectionY)
	{
		// 아무 처리도 하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnInstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnInstallWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnUnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnUnInstallWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnTrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnTrackWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnWindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnWindupWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnAimWeapon(int pAimDelay)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnAimWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnFireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim)
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnFireWeapon();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnEndCooldown()
	{
		if (mIsVisible)
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnEndCooldown();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnGoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
// 		bool lIsShowAlways;
// 		if ((pTargetTroop != null) &&
// 			(pTargetTroop.aTeamIdx == BattlePlay.main.vStage.vObserverTeamIdx))
// 			lIsShowAlways = true;
// 		else
// 			lIsShowAlways = false;
// 
// 		for (int iActor = 0; iActor < mActorList.Count; iActor++)
// 		{
// 			if (lIsShowAlways ||
// 				mActorList[iActor].gameObject.activeInHierarchy)
// 				mActorList[iActor].GoToInteraction(pTroopInteractionType, pTargetTroop);
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnComeBackInteraction()
	{
// 		for (int iActor = 0; iActor < mActorList.Count; iActor++)
// 		{
// 			mActorList[iActor].ComeBackInteraction();
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStartSkill(BattleCommanderSkill pCommanderSkill)
	{
// 		for (int iActor = 0; iActor < mActorList.Count; iActor++)
// 		{
// 			mActorList[iActor].StartSkill(pSkillSpec);
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStopSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStartAction(TroopActionType pTroopActionType)
	{
// 		for (int iActor = 0; iActor < mActorList.Count; iActor++)
// 		{
// 			mActorList[iActor].GoAction(pTroopActionType);
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnStopAction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 위치를 지정합니다.
	public void SetPosition(Vector3 pPosition)
	{
		mTransform.position = pPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각을 지정합니다.
	public void SetDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메인 액터를 교체합니다.
	public void ChangeMainActor()
	{
		if (mMainActor == null)
		{
			// 메인 액터가 될 수 있는 액터들 중에서 랜덤하게 하나를 고릅니다.
			int lActorIdxOffset = UnityEngine.Random.Range(0, vActorInfos.Length);
			for (int iActor = 0; iActor < vActorInfos.Length; ++iActor)
			{
				int lActorIdx = lActorIdxOffset + iActor;
				if (lActorIdx >= vActorInfos.Length)
					lActorIdx -= vActorInfos.Length;

				if ((vActors[lActorIdx] != null) &&
					vActorInfos[lActorIdx].vCanBeMainActor)
				{
					mMainActor = vActors[lActorIdx];
					return;
				}
			}

			// 그래도 메인 액터를 찾지 못했다면 첫 번째 액터를 메인 액터로 지정합니다.
			for (int iActor = 0; iActor < vActorInfos.Length; ++iActor)
				if (vActors[iActor] != null)
				{
					mMainActor = vActors[iActor];
					return;
				}
		}
		else
		{
			mMainActor = mMainActor.aNextMainActor;
		}
		//Debug.Log("mMainActor=" + mMainActor);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 액터를 할당합니다.
	private void _AllocateActors()
	{
	#if TEST_SINGLE_ACTOR
		for (int iActor = 0; iActor < vActorInfos.Length; ++iActor)
			if (vActorInfos[iActor].vActorPrefab != null)
				for (int iOtherActor = (iActor + 1); iOtherActor < vActorInfos.Length; ++iOtherActor)
					vActorInfos[iOtherActor].vActorPrefab = null;
	#endif

		vActors = new RealActor[vActorInfos.Length];
		for (int iActor = 0; iActor < vActorInfos.Length; ++iActor)
		{
			ActorInfo lActorInfo = vActorInfos[iActor];
			if (lActorInfo.vActorPrefab != null)
			{
				GameObject lActorObject = Instantiate(lActorInfo.vActorPrefab.gameObject);
				RealActor lActor = lActorObject.GetComponent<RealActor>();
				vActors[iActor] = lActor;

				lActor.OnNewActor(this, iActor, lActorInfo);
			}
		}
		for (int iActor = 0; iActor < vActorInfos.Length; ++iActor)
		{
			if (vActors[iActor] != null)
			{
				vActors[iActor].aNextMainActor = vActors[iActor];
				for (int iNextActor = 1; iNextActor < vActorInfos.Length; ++iNextActor)
				{
					int lNextActorIdx = iActor + iNextActor;
					if (lNextActorIdx >= vActorInfos.Length)
						lNextActorIdx -= vActorInfos.Length;

					if ((vActors[lNextActorIdx] != null) &&
						vActorInfos[lNextActorIdx].vCanBeMainActor)
					{
						vActors[iActor].aNextMainActor = vActors[lNextActorIdx];
						//Debug.Log(String.Format("vActors[{0}].aNextMainActor = vActors[{1}]", iActor, lNextActorIdx));
						break;
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 보이기 여부를 지정합니다.
	private void _SetVisible(bool pIsVisible)
	{
		mIsVisible = pIsVisible;
		if (pIsVisible)
		{
			mActorUpdateTime = 0.0f;
			mMainActor = null;
			mIsMoving = false;
			ChangeMainActor();

			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnVisibleActor();

			mPositioning.Init();

			StartCoroutine(_UpdateActors());
		}
		else
		{
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
					vActors[iActor].OnInvisibleActor();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 은닉 여부를 지정합니다.
	private void _SetHiding(bool pIsHiding)
	{
		mIsHiding = pIsHiding;
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌이 변경될때 호출됩니다.
	private void _OnZoomChanged()
	{
		bool lIsShowMesh = (BattlePlay.main.vControlPanel.vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance <= vStageTroop.aStage.vLodDistance);
		if (mIsShowMesh != lIsShowMesh)
			_SetShowMesh(lIsShowMesh);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메쉬 보이기 여부를 지정합니다.
	private void _SetShowMesh(bool pIsShowMesh)
	{
		mIsShowMesh = pIsShowMesh;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 낙하산 스폰을 갱신합니다.
	private IEnumerator _UpdateAircraftSpawn()
	{
		float lSpawnDelay = BattleConfig.get.mAircraftSpawnDelay * 0.001f;
		float lSpawnTimer = lSpawnDelay;
		do
		{
//			aHeightOffset = lSpawnTimer / lSpawnDelay * cAircraftHeight;

			lSpawnTimer -= Time.deltaTime;
			yield return null;
		} while (lSpawnTimer > 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 액터 상태를 갱신합니다.
	private IEnumerator _UpdateActors()
	{
		do
		{
			float lUpdateDelta = Time.deltaTime;
			mActorUpdateTime += lUpdateDelta;
			//Debug.Log("mActorUpdateTime=" + mActorUpdateTime);

			bool lActorIsMoving = false;
			for (int iActor = 0; iActor < vActors.Length; ++iActor)
				if (vActors[iActor] != null)
				{
					vActors[iActor].OnUpdateActor(lUpdateDelta);
					
					if (vActors[iActor].aPositionUpdater.aIsToUpdate)
						lActorIsMoving = true;
				}

			if (mPositioning.aIsUpdating)
				mPositioning.aOnUpdateDelegate();

			mIsMoving = lActorIsMoving;

			yield return null;
		} while (mIsVisible);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private float mVisualRadius;
	private StageTroopTag mStageTroopTag;
	private RealActorManagerPositioning mPositioning;

	private BattleTroop mBattleTroop;
	private Stage mStage;

	private bool mIsVisible;
	private bool mIsHiding;
	private bool mIsShowMesh;

	private float mActorUpdateTime;
	private RealActor mMainActor;
	private bool mIsMoving;
}
