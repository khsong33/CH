﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorManagerPositioning
{
	public enum PositioningType
	{
		ActorsCenter,
		FirstActor,
	}

	private delegate void OnInitDelegate();
	public delegate void OnUpdateDelegate();

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 갱신중 여부
	public bool aIsUpdating { get; private set; }
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	public OnUpdateDelegate aOnUpdateDelegate { get; private set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public RealActorManagerPositioning(RealActorManager pActorManager, PositioningType pPositioningType)
	{
		mActorManager = pActorManager;
		mActors = pActorManager.vActors;

		switch (pPositioningType)
		{
		case PositioningType.ActorsCenter:
			mOnInitDelegate = _OnInit_ActorsCenter;
			aOnUpdateDelegate = _OnUpdate_ActorsCenter;
			break;
		case PositioningType.FirstActor:
			mOnInitDelegate = _OnInit_FirstActor;
			aOnUpdateDelegate = _OnUpdate_FirstActor;
			break;
		default:
			mOnInitDelegate = _OnInit_Static;
			aOnUpdateDelegate = _OnUpdate_Static;
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init()
	{
		mOnInitDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터가 이동했는지를 알립니다.
	public void NotifyActorMove()
	{
		aIsUpdating = true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_ActorsCenter()
	{
		_OnUpdate_ActorsCenter();
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateDelegate
	private void _OnUpdate_ActorsCenter()
	{
		int lActorCount = 0;
		Vector3 lPositionSum = Vector3.zero;
		for (int iActor = 0; iActor < mActors.Length; ++iActor)
			if (mActors[iActor] != null)
			{
				lPositionSum += mActors[iActor].aActorPosition;
				++lActorCount;
			}
		if (lActorCount > 0)
			mActorManager.SetPosition(lPositionSum / lActorCount);

		aIsUpdating = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_FirstActor()
	{
		_OnUpdate_FirstActor();
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateDelegate
	private void _OnUpdate_FirstActor()
	{
		for (int iActor = 0; iActor < mActors.Length; ++iActor)
			if (mActors[iActor] != null)
			{
				mActorManager.SetPosition(mActors[iActor].aActorPosition);
				return;
			}

		aIsUpdating = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_Static()
	{
		aIsUpdating = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateDelegate
	private void _OnUpdate_Static()
	{
		aIsUpdating = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private RealActorManager mActorManager;
	private RealActor[] mActors;
	private OnInitDelegate mOnInitDelegate;
}
