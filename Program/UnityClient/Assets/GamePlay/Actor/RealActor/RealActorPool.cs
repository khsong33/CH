﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorPool : MonoBehaviour, IActorPool
{
    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public String vAssetKeyTableBundle;
	public String vAssetKeyTablePath;

	public List<RealActorManager> vActorManagerList;

	public Transform vActorManagerRoot;
	public Transform vActorRoot;

    //-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
		mAssetKeyTable = new AssetKeyTable();
		mAssetKeyTable.Load(vAssetKeyTableBundle, vAssetKeyTablePath, false);

		mActorManagerIdxDictionary = new Dictionary<String, Queue<int>>();

		vActorManagerList = new List<RealActorManager>();

		if (vActorManagerRoot == null)
		{
			GameObject lActorManagerRootObject = new GameObject("ActorManagerRoot");
			vActorManagerRoot = lActorManagerRootObject.transform;
		}
		if (vActorRoot == null)
		{
			GameObject lActorRootObject = new GameObject("ActorRoot");
			vActorRoot = lActorRootObject.transform;
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // IActorPool 메서드
	public IActorManager CreateActorManager(StageTroop pStageTroop)
    {
		RealActorManager lActorManager;
		String lStageTroopAssetKeyName = pStageTroop.aBattleTroop.aTroopSpec.mStageTroopAssetKeyName;
		int lActorPoolIndex = _FindActorManager(lStageTroopAssetKeyName);
		if (lActorPoolIndex < 0)
		{
			AssetKey lActorManagerAssetKey = mAssetKeyTable.FindAssetKey(lStageTroopAssetKeyName);
			if (!AssetKey.IsValid(lActorManagerAssetKey))
				Debug.LogError("invalid asset key '" + lActorManagerAssetKey + "' - " + this);

			lActorManager = AssetManager.get.InstantiateComponent<RealActorManager>(lActorManagerAssetKey);
			lActorManager.OnNewActorManager(this, vActorManagerList.Count);
			vActorManagerList.Add(lActorManager);
		}
		else
		{
			lActorManager = vActorManagerList[lActorPoolIndex];
			lActorManager.gameObject.SetActive(true);
		}

		lActorManager.OnCreateActorManager(pStageTroop);

		return lActorManager;
    }
    //-------------------------------------------------------------------------------------------------------
    // IActorPool 메서드
	public void DestoryActorManager(RealActorManager pActorManager)
    {
		pActorManager.OnDestroyActorManager();

		String lStageTroopAssetKeyName = pActorManager.aBattleTroop.aTroopSpec.mStageTroopAssetKeyName;
		if (!mActorManagerIdxDictionary.ContainsKey(lStageTroopAssetKeyName))
		{
			mActorManagerIdxDictionary.Add(lStageTroopAssetKeyName, new Queue<int>());
		}
		mActorManagerIdxDictionary[lStageTroopAssetKeyName].Enqueue(pActorManager.vActorManagerIndex);
		pActorManager.gameObject.SetActive(false);
    }

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 풀 안에 생성된 ActorManager가 있는지 확인합니다.
    private int _FindActorManager(String pAssetPath)
    {
		if (mActorManagerIdxDictionary.ContainsKey(pAssetPath))
        {
			int lResultActorListIndex = mActorManagerIdxDictionary[pAssetPath].Dequeue();
            // 더 이상 풀에 id가 없으면 삭제
			if (mActorManagerIdxDictionary[pAssetPath].Count <= 0)
				mActorManagerIdxDictionary.Remove(pAssetPath);

            return lResultActorListIndex;
        }
        return -1;
    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
	private AssetKeyTable mAssetKeyTable;
	private Dictionary<String, Queue<int>> mActorManagerIdxDictionary;
}
