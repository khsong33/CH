﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorPositionUpdater
{
	public enum PositioningType
	{
		CenterOffset,
		RandomInRange,
	}
	public enum StopType
	{
		Arrived,
		Hold,

		None,
	}

	public delegate void OnStartMoveDelegate();
	public delegate void OnUpdatePositionDelegate(Vector3 pPosition, StopType pStopType);
	public delegate void OnHoldPositionDelegate(bool pHoldPosition);

	private delegate void OnInitDelegate(Vector3 pInitPosition);
	private delegate Vector3 OnSetDstActorPositionDelegate(Vector3 pDstTroopPosition);

	private const float cEndDelayMargin = 0.0f;
	private const float cMaxApproachFactorScale = 5.0f;
	private const float cSqrPositionUpdateEndRange = 0.1f * 0.1f;
	private const float cRotationUpdateEndDegree = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 연결된 부대 이동 갱신자
	public BattleTroopMoveUpdater aTroopMoveUpdater
	{
		get { return mTroopMoveUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 위치
	public Vector3 aPosition
	{
		get { return mPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 필요 여부
	public bool aIsToUpdate
	{
		get { return mIsToUpdate; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnStartMoveDelegate aOnStartMoveDelegate { get; set; }
	public OnUpdatePositionDelegate aOnUpdatePositionDelegate { get; set; }
	public OnHoldPositionDelegate aOnHoldPositionDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public RealActorPositionUpdater(RealActor pActor, PositioningType pPositioningType)
	{
		mActor = pActor;

		switch (pPositioningType)
		{
		case PositioningType.CenterOffset:
			mOnInitDelegate = _OnInit_CenterOffset;
			mOnSetDstActorPositionDelegate = _OnSetDstActorPosition_CenterOffset;
			break;
		case PositioningType.RandomInRange:
			mOnInitDelegate = _OnInit_RandomInRange;
			mOnSetDstActorPositionDelegate = _OnSetDstActorPosition_RandomInRange;
			break;
		default:
			mOnInitDelegate = _OnInit_Static;
			mOnSetDstActorPositionDelegate = _OnSetDstActorPosition_Static;
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopMoveUpdater pTroopMoveUpdater)
	{
		mTroopMoveUpdater = pTroopMoveUpdater;

		mIsMoving = false;
		mIsToUpdate = false;
		mIsHoldMove = false;
		mLateMoveTimer = 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표를 지정합니다.
	public void Set(Vector3 pPosition)
	{
		mOnInitDelegate(pPosition); // 내부에서 mPosition 초기화

		mIsMoving = false;
		mIsToUpdate = false;
		mIsHoldMove = false;
		mLateMoveTimer = 0.0f;

		if (aOnUpdatePositionDelegate != null)
			aOnUpdatePositionDelegate(mPosition, StopType.Arrived);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표로 이동합니다.
	public void SetDstPosition(Vector3 pDstTroopPosition, float pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
		//Debug.Log("SetDstPosition() - " + this);

		mIsNewDstPosition = true;
		mDstActorPosition = mOnSetDstActorPositionDelegate(pDstTroopPosition);
		mDstPositionArrivalDelay = pArrivalDelay;
		if (mLateMoveTimer <= 0.0f) // 첫 이동 혹은 지연 이동 대기가 끝나서 이동중이라면
			mDstPositionSetTime = mActor.vActorManager.aActorUpdateTime; // 늦은 이동 대기중의 처리를 위해 기억해 둠
		else
			mLateMoveTimeOffset = mActor.vActorManager.aActorUpdateTime - mDstPositionSetTime; // 늦은 이동을 시작하는 시간차를 증가시킴
		mDstStopType = pStopType;
		//Debug.Log(String.Format("***************************** pArrivalDelay={0,4:0.##} mDstPositionArrivalDelay={1,4:0.##}(+{2,4:0.##})", pArrivalDelay, mDstPositionArrivalDelay, (mDstPositionArrivalDelay - pArrivalDelay)));

		if (!mIsToUpdate) // 아직 갱신 전이라면
		{
			// 늦은 이동 여부를 초기화합니다.
			if (mActor.aIsMainActor)
				mLateMoveTimer = 0.0f;
			else
				mLateMoveTimer = UnityEngine.Random.Range(0.0f, mActor.aManagerActorInfo.vMaxActionDelay2);
			//Debug.Log("mActor[" + mActor.aActorIdx + "] mLateMoveTimer=" + mLateMoveTimer);

			mLateMoveTimeOffset = Mathf.Min(mLateMoveTimer, pArrivalDelay); // 시간차 초기화
			mIsToUpdate = true; // 갱신 시작
		}

		if (mLateMoveTimer <= 0.0f)
			_SetNextMove();

		mIsMoveToPositionArrived = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 대기와 지속을 지정합니다.
	public void SetHoldMove(bool pIsHoldMove)
	{
		//Debug.Log("SetHoldMove() pIsHoldMove=" + pIsHoldMove);

		mIsHoldMove = pIsHoldMove;

		if (aOnHoldPositionDelegate != null)
			aOnHoldPositionDelegate(pIsHoldMove);
	}
	//-------------------------------------------------------------------------------------------------------
	// 멈춥니다.
	public void SetStopMove(Vector3 pStopTroopPosition)
	{
		//Debug.Log("SetStopMove()");

		SetDstPosition(pStopTroopPosition, 0.0f, BattleTroopMoveUpdater.StopType.Arrived);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 갱신합니다.
	public void UpdatePosition(float _pUpdateDelta) // lUpdatePositionDelta를 사용합니다.
	{
		if (!mIsToUpdate)
			return;
		if (mIsHoldMove)
			return;

		float lUpdatePositionDelta = _pUpdateDelta;

		// 늦은 이동을 처리합니다.
		if (mLateMoveTimer > 0.0f)
		{
			mLateMoveTimer -= lUpdatePositionDelta;
			if (mLateMoveTimer > 0.0f)
				return;

			lUpdatePositionDelta = -mLateMoveTimer; // 늦은 이동 타이머를 감소한 만큼을 뺌
			_SetNextMove();
		}

		// 목표 지점까지의 보간 정보를 얻습니다.
		if (!mIsMoveToPositionArrived)
		{
			// 목표 좌표를 갱신합니다.
			float lLerpFactor;
			if (mArrivalDelay > 0.0f)
			{
				mArrivalTimer += lUpdatePositionDelta;
				lLerpFactor = mArrivalTimer / mArrivalDelay;
				if (lLerpFactor >= 1.0f)
				{
					lLerpFactor = 1.0f;
					mArrivalTimer = mArrivalDelay;
					mIsMoveToPositionArrived = true;
				}
			}
			else
			{
				lLerpFactor = 1.0f;
				mIsMoveToPositionArrived = true;
			}
			mMoveToPosition = Vector3.Lerp(mStartPosition, mEndPosition, lLerpFactor);
		}

		// 액터를 이동시킵니다.
		if (mActor.vMoveAccel <= 0.0f) // 가속도가 없다면
		{
			// 이동 방향으로 회전시킵니다.
			Vector3 lMoveVector = mMoveToPosition - mPosition;
			float lMoveDirectionY = MathUtil.GetDirectionY(lMoveVector);
			mActor.aBodyRotationUpdater.RotateToDirectionY(lMoveDirectionY, false);

			// 목표 좌표를 액터 좌표로 바로 지정합니다.
			mPosition = mMoveToPosition;
			mIsToUpdate = !mIsMoveToPositionArrived;
			if (aOnUpdatePositionDelegate != null)
				aOnUpdatePositionDelegate(
					mPosition,
					mIsMoveToPositionArrived ? mPositionStopType : StopType.None);
		}
		else // 가속도가 있다면
		{
			// 자기 속도를 가지고 목표 좌표로 이동합니다.

			// 보간 정보에 대한 점근 처리로 실제 좌표를 얻습니다.
			if (mApproachFactorScale < cMaxApproachFactorScale)
			{
				mApproachFactorScale += lUpdatePositionDelta * mActor.vMoveAccel;
				if (mApproachFactorScale > cMaxApproachFactorScale)
					mApproachFactorScale = cMaxApproachFactorScale;
			}
			float lApproachFactor = lUpdatePositionDelta * mApproachFactorScale;
			if (lApproachFactor > 1.0f)
				lApproachFactor = 1.0f;

			Vector3 lLerpPositionVector = mMoveToPosition - mPosition;
			mPosition += lLerpPositionVector * lApproachFactor;
			bool lIsArrived = (lLerpPositionVector.sqrMagnitude <= cSqrPositionUpdateEndRange) && mIsMoveToPositionArrived;

			// 이동 방향으로 회전시킵니다.
			float lMoveDirectionY = MathUtil.GetDirectionY(lLerpPositionVector);
			mActor.aBodyRotationUpdater.RotateToDirectionY(lMoveDirectionY, false);

			// 액터에 반영합니다.
			if (aOnUpdatePositionDelegate != null)
				aOnUpdatePositionDelegate(
					mPosition,
					lIsArrived ? mPositionStopType : StopType.None); // 도착했더라도 멈춤 이동이 아니라면 이동중으로 처리

			mIsToUpdate = !lIsArrived;
		}

		// 예약된 다음 이동을 처리합니다.
		if (!mIsToUpdate && mIsNewDstPosition)
		{
			_SetNextMove();
			
			mIsToUpdate = true;
			mIsMoveToPositionArrived = false;
		}

		// 이동 종료와 도착 회전을 처리합니다.
		if (!mIsToUpdate &&
			(mPositionStopType != StopType.None))
		{
			mIsMoving = false; // 이동 종료
			mLateMoveTimeOffset = 0.0f;

			// 부대 방향으로 회전시킵니다.
			mActor.aBodyRotationUpdater.RotateToDirectionY(mActor.aBattleTroop.aBodyRotationUpdater.aRotationEndDirectionY, true);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_CenterOffset(Vector3 pInitPosition)
	{
		mPosition = _OnSetDstActorPosition_CenterOffset(pInitPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnGetPosition
	private Vector3 _OnSetDstActorPosition_CenterOffset(Vector3 pDstTroopPosition)
	{
		Vector3 lPositionOffset = MathLib.GetRotation(mActor.aBattleTroop.aDirectionY) * mActor.aManagerActorInfo.vPositioningOffset;
		Vector3 lDstActorPosition = pDstTroopPosition + lPositionOffset;

		return lDstActorPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_RandomInRange(Vector3 pInitPosition)
	{
		mPosition = _OnSetDstActorPosition_RandomInRange(pInitPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnSetDstPosition
	private Vector3 _OnSetDstActorPosition_RandomInRange(Vector3 pDstTroopPosition)
	{
		float lRandomInRange =  mActor.aManagerActorInfo.vPositioningRandomInRange;
		Vector3 lDstActorPosition = new Vector3(
			pDstTroopPosition.x + mActor.aManagerActorInfo.vPositioningOffset.x + UnityEngine.Random.Range(-lRandomInRange, lRandomInRange),
			pDstTroopPosition.y,
			pDstTroopPosition.z + mActor.aManagerActorInfo.vPositioningOffset.z + UnityEngine.Random.Range(-lRandomInRange, lRandomInRange));

		return lDstActorPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnInitDelegate
	private void _OnInit_Static(Vector3 pInitPosition)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// OnSetDstPosition
	private Vector3 _OnSetDstActorPosition_Static(Vector3 pDstTroopPosition)
	{
		return pDstTroopPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 이동을 지정합니다.
	private void _SetNextMove()
	{
		if (!mIsMoving)
		{
			mStartPosition = mPosition; // 현재 좌표
			mApproachFactorScale = 0.0f; // 점근 팩터 초기화

			if (aOnStartMoveDelegate != null)
				aOnStartMoveDelegate();

			mIsMoving = true; // 이동 시작
		}
		else
		{
			mStartPosition = mMoveToPosition; // 목표 좌표
		}
		mEndPosition = mDstActorPosition;
		mArrivalDelay = mLateMoveTimeOffset + mDstPositionArrivalDelay + cEndDelayMargin; // 끊기지 않는 이동을 위해 여유 시간을 더함
		mArrivalTimer = 0.0f;
		mPositionStopType = (RealActorPositionUpdater.StopType)mDstStopType; // 값이 같음
		//Debug.Log(String.Format("mArrivalDelay {0}={1}+{2} speed {3:0.##}", mArrivalDelay, mDstPositionArrivalDelay, mLateMoveTimeOffset, (mEndPosition - mStartPosition).magnitude / mArrivalDelay));

		mIsNewDstPosition = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private RealActor mActor;
	private BattleTroopMoveUpdater mTroopMoveUpdater;
	private OnInitDelegate mOnInitDelegate;
	private OnSetDstActorPositionDelegate mOnSetDstActorPositionDelegate;

	private Vector3 mPosition;

	private bool mIsNewDstPosition;
	private Vector3 mDstActorPosition;
	private float mDstPositionArrivalDelay;
	private float mDstPositionSetTime;
	private BattleTroopMoveUpdater.StopType mDstStopType;

	private bool mIsMoving;
	private bool mIsToUpdate;
	private bool mIsHoldMove;
	private bool mIsMoveToPositionArrived;
	private float mLateMoveTimer;
	private float mLateMoveTimeOffset;

	private Vector3 mStartPosition;
	private Vector3 mEndPosition;
	private float mArrivalTimer;
	private float mArrivalDelay;
	private RealActorPositionUpdater.StopType mPositionStopType;

	private Vector3 mMoveToPosition;
	private float mApproachFactorScale;
}
