﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorRotationUpdater
{
	private enum UpdateType
	{
		None,
		RotationSpeed,
		ArrivalDelay,
	}

	public delegate void OnUpdateRotationDelegate(float pDirectionY, bool pIsArrived);

	private const float cRotateToDStartThreshold = 10.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 연결된 부대 회전 갱신자
	public BattleTroopRotationUpdater aTroopRotationUpdater
	{
		get { return mTroopRotationUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각
	public float aDirectionY
	{
		get { return mDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 필요 여부
	public bool aIsToUpdate
	{
		get { return (mUpdateType != UpdateType.None); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnUpdateRotationDelegate aOnUpdateRotationDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public RealActorRotationUpdater(RealActor pActor)
	{
		mActor = pActor;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopRotationUpdater pTroopRotationUpdater)
	{
		mTroopRotationUpdater = pTroopRotationUpdater;
	}
	//-------------------------------------------------------------------------------------------------------
	// 각도를 지정합니다.
	public void Set(int pDirectionY)
	{
		mDirectionY = pDirectionY;

		mUpdateType = UpdateType.None;

		if (aOnUpdateRotationDelegate != null)
			aOnUpdateRotationDelegate(mDirectionY, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 방향각으로 몸체를 회전합니다.
	public void RotateToDirectionY(float pDstDirectionY, bool pIsStoppingRotation)
	{
		float lAngleVector = MathUtil.DegreeVector(mDirectionY, pDstDirectionY);
		if (Mathf.Abs(lAngleVector) < cRotateToDStartThreshold)
			return;

		mDstDirectionY = MathUtil.AbsDegree(pDstDirectionY);
		mIsStoppingRotation = pIsStoppingRotation;
		mRotationSpeed = 180.0f / mTroopRotationUpdater.aMaxRotationDelay * MathUtil.cSecToMilli;

		mUpdateType = UpdateType.RotationSpeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 방향각으로 몸체를 회전합니다.
	public void SetDstDirectionY(float pDstDirectionY, float pArrivalDelay)
	{
		mStartDegree = mDirectionY;
		mEndDegree = pDstDirectionY;
		mStartTime = mActor.vActorManager.aActorUpdateTime;
		mEndDelay = pArrivalDelay;

		mUpdateType = UpdateType.ArrivalDelay;
	}
	//-------------------------------------------------------------------------------------------------------
	// 회전을 갱신합니다.
	public void UpdateRotation(float pUpdateDelta)
	{
		if (mUpdateType == UpdateType.None)
			return;

		bool lIsArrived;
		switch (mUpdateType)
		{
		case UpdateType.RotationSpeed:
			{
				//Debug.Log("mUpdateType=" + mUpdateType + " mDstDirectionY=" + mDstDirectionY);

				float lMaxRotationDegree = mRotationSpeed * pUpdateDelta;
				mDirectionY = MathUtil.ApproachDegree(mDirectionY, mDstDirectionY, lMaxRotationDegree);

				lIsArrived = (mDirectionY == mDstDirectionY) && mIsStoppingRotation;

				if (mDirectionY == mDstDirectionY)
					mUpdateType = UpdateType.None; // 갱신 종료
			}
			break;
		case UpdateType.ArrivalDelay:
			{
				//Debug.Log(">>> mUpdateType=" + mUpdateType + " mEndDegree=" + mEndDegree);

				float lLerpFactor = (mActor.vActorManager.aActorUpdateTime - mStartTime) / mEndDelay;
				if (lLerpFactor >= 1.0f)
				{
					lLerpFactor = 1.0f;
					lIsArrived = true;

					mUpdateType = UpdateType.None; // 갱신 종료
				}
				else
				{
					lIsArrived = false;
				}
				mDirectionY = MathUtil.LerpDegree(mStartDegree, mEndDegree, lLerpFactor);
			}
			break;
		default:
			lIsArrived = true;
			break;
		}

		// 액터에 반영합니다.
		if (aOnUpdateRotationDelegate != null)
			aOnUpdateRotationDelegate(mDirectionY, lIsArrived);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private RealActor mActor;
	private BattleTroopRotationUpdater mTroopRotationUpdater;

	private float mDirectionY;

	private float mDstDirectionY;
	private bool mIsStoppingRotation;
	private float mRotationSpeed;

	private float mStartDegree;
	private float mEndDegree;
	private float mStartTime;
	private float mEndDelay;

	private UpdateType mUpdateType;
}
