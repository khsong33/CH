﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorSoldier : RealActor
{
	public enum AniTag
	{
		SearchIdleLoop,
		MoveLoop,
		AttackIdleLoop,

		None,
	}

	public const float cCrossFadeDelay = 0.2f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Animation vAnimation;

	public String vSearchIdleLoopAniName;
	public String vSearchIdleSuppressedLoopAniName;

	public String vMoveLoopAniName;
	public String vMoveSuppressedLoopAniName;

	public String vDieWhenStandingAniName;
	public String vDieWhenAttackingAniName;
	public String vDieByExplosionAniName;

	public String vInstallWeaponAniName;
	public String vUnInstallWeaponAniName;

	public String vDrawSubWeaponAniName;
	public String vPutDownSubWeaponAniName;
	public String vDrawVeteranWeaponAniName;
	public String vPutDownVeteranWeaponAniName;

	public String vAttackIdleLoopAniName;
	public String vAttackIdleSuppressedLoopAniName;

	public String vWindupMainWeaponAniName;
	public String vWindupSubWeaponAniName;
	public String vWindupVeteranWeaponAniName;

	public String vFireMainWeaponAniName;
	public String vFireSubWeaponAniName;
	public String vFireVeteranWeaponAniName;

	public String vPinDownAniName;
	public String vHealAniName;
	public String vParachuteAniName;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("RealActorSoldier[{0}]@{1}", aActorIdx, vActorManager);
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnNewActor(RealActorManager pActorManager, int pActorIdx, RealActorManager.ActorInfo pManagerActorInfo)
	{
		if (vAnimation == null)
			Debug.LogError("vAnimation is empty - " + this);

		base.OnNewActor(pActorManager, pActorIdx, pManagerActorInfo);

	#if UNITY_EDITOR
		_ValidateAllAnis();
	#endif

		aPositionUpdater.aOnStartMoveDelegate = _OnStartMove;
		aPositionUpdater.aOnUpdatePositionDelegate = _OnUpdatePosition;
		aPositionUpdater.aOnHoldPositionDelegate = _OnHoldPosition;
		aBodyRotationUpdater.aOnUpdateRotationDelegate = _OnUpdateBodyRotation;
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnCreateActor()
	{
		//Debug.Log("OnCreateActor() - " + this);

		base.OnCreateActor();

		mLastAniName = null;
		mLastAniTag = AniTag.None;
		mLastStopType = RealActorPositionUpdater.StopType.Arrived;

		if (aBattleTroop.aCommander.aCommanderSpec.mSpawnType == CommanderSpawnType.Aircraft)
			_SetAni(vParachuteAniName, AniTag.None);
		else
			_SetAni(vSearchIdleLoopAniName, AniTag.SearchIdleLoop);	
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnDestroyActor()
// 	{
// 		//Debug.Log("OnDestroyActor() - " + this);
// 
// 		base.OnDestroyActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnVisibleActor()
// 	{
// 		base.OnVisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnInvisibleActor()
// 	{
// 		base.OnInvisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnUpdateActor(float pUpdateDelta)
// 	{
// 		base.OnUpdateActor(pUpdateDelta);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
		//Debug.Log("OnUpdateMoraleState() pTroopMoraleState=" + pTroopMoraleState + " - " + this);

		switch (pTroopMoraleState)
		{
		case TroopMoraleState.Good:
			{
				mMoraleSearchIdleLoopAniName = vSearchIdleLoopAniName;
				if (mLastAniTag == AniTag.SearchIdleLoop)
					_SetAni(mMoraleSearchIdleLoopAniName, AniTag.SearchIdleLoop);

				mMoraleMoveLoopAniName = vMoveLoopAniName;
				if (mLastAniTag == AniTag.MoveLoop)
					_SetAni(mMoraleMoveLoopAniName, AniTag.MoveLoop);

				mMoraleAttackIdleLoopAniName = vAttackIdleLoopAniName;
				if (mLastAniTag == AniTag.AttackIdleLoop)
					_SetAni(mMoraleAttackIdleLoopAniName, AniTag.AttackIdleLoop);
			}
			break;
		case TroopMoraleState.Suppressed:
			{
				mMoraleSearchIdleLoopAniName = vSearchIdleSuppressedLoopAniName;
				if (mLastAniTag == AniTag.SearchIdleLoop)
					_SetAni(mMoraleSearchIdleLoopAniName, AniTag.SearchIdleLoop);

				mMoraleMoveLoopAniName = vMoveSuppressedLoopAniName;
				if (mLastAniTag == AniTag.MoveLoop)
					_SetAni(mMoraleMoveLoopAniName, AniTag.MoveLoop);

				mMoraleAttackIdleLoopAniName = vAttackIdleSuppressedLoopAniName;
				if (mLastAniTag == AniTag.AttackIdleLoop)
					_SetAni(mMoraleAttackIdleLoopAniName, AniTag.AttackIdleLoop);
			}
			break;
		case TroopMoraleState.PinDown:
			{
				_SetAni(vPinDownAniName, mLastAniTag); // 기존 태그 유지
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 설치합니다.
	public override void OnInstallWeapon()
	{
		if (!String.IsNullOrEmpty(vInstallWeaponAniName))
			_SetAni(vInstallWeaponAniName, AniTag.None);
		else
			_SetAni(vWindupMainWeaponAniName, AniTag.None);
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 설치를 해제합니다.
	public override void OnUnInstallWeapon()
	{
		if (!String.IsNullOrEmpty(vUnInstallWeaponAniName))
			_SetAni(vUnInstallWeaponAniName, AniTag.None);
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 조준한 상태에서 회전합니다.
	public override void OnTrackWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 야포의 와인드업 액션을 합니다.
	public override void OnWindupWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 조준합니다.
	public override void OnAimWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기를 발사합니다.
	public override void OnFireWeapon()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 발사 후에 쿨다운이 끝났을 때의 처리를 합니다.
	public override void OnEndCooldown()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnStartMoveDelegate
	private void _OnStartMove()
	{
		_SetAni(mMoraleMoveLoopAniName, AniTag.MoveLoop);
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnUpdatePositionDelegate
	private void _OnUpdatePosition(Vector3 pPosition, RealActorPositionUpdater.StopType pStopType)
	{
		aTransform.position = aStage.GetTerrainPosition(pPosition);

		if (mLastStopType != pStopType)
		{
			mLastStopType = pStopType;
			switch (pStopType)
			{
			case RealActorPositionUpdater.StopType.None: // 이동 중이라면
				_SetAni(mMoraleMoveLoopAniName, AniTag.MoveLoop);
				break;
			case RealActorPositionUpdater.StopType.Arrived: // 도착 상태라면
				_SetAni(mMoraleSearchIdleLoopAniName, AniTag.SearchIdleLoop);
				break;
			case RealActorPositionUpdater.StopType.Hold: // 대기 상태라면
				_SetAni(mMoraleSearchIdleLoopAniName, AniTag.SearchIdleLoop);
				break;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnHoldPositionDelegate
	private void _OnHoldPosition(bool pHoldPosition)
	{
		if (pHoldPosition)
			_SetAni(mMoraleSearchIdleLoopAniName, AniTag.SearchIdleLoop);
		else
			_SetAni(mMoraleMoveLoopAniName, AniTag.MoveLoop);
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorRotationUpdater.OnUpdateRotationDelegate
	private void _OnUpdateBodyRotation(float pBodyDirectionY, bool pIsArrived)
	{
		aTransform.rotation = MathLib.GetRotation(pBodyDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 없는 애니 이름이 등록되어 있는지 검사합니다.
	private void _ValidateAllAnis()
	{
		_ValidateAni_mustBe(vSearchIdleLoopAniName);
		_ValidateAni_mustBe(vSearchIdleSuppressedLoopAniName);

		_ValidateAni_mustBe(vMoveLoopAniName);
		_ValidateAni_mustBe(vMoveSuppressedLoopAniName);

		_ValidateAni_mustBe(vDieWhenStandingAniName);
		_ValidateAni_mustBe(vDieWhenAttackingAniName);
		_ValidateAni_mustBe(vDieByExplosionAniName);

		_ValidateAni_mayEmpty(vInstallWeaponAniName);
		_ValidateAni_mayEmpty(vUnInstallWeaponAniName);

		_ValidateAni_mustBe(vDrawSubWeaponAniName);
		_ValidateAni_mustBe(vPutDownSubWeaponAniName);
		_ValidateAni_mustBe(vDrawVeteranWeaponAniName);
		_ValidateAni_mustBe(vPutDownVeteranWeaponAniName);

		_ValidateAni_mustBe(vAttackIdleLoopAniName);
		_ValidateAni_mustBe(vAttackIdleSuppressedLoopAniName);

		_ValidateAni_mustBe(vWindupMainWeaponAniName);
		_ValidateAni_mustBe(vWindupSubWeaponAniName);
		_ValidateAni_mustBe(vWindupVeteranWeaponAniName);

		_ValidateAni_mustBe(vFireMainWeaponAniName);
		_ValidateAni_mustBe(vFireSubWeaponAniName);
		_ValidateAni_mustBe(vFireVeteranWeaponAniName);

		_ValidateAni_mustBe(vPinDownAniName);
		_ValidateAni_mustBe(vHealAniName);
		_ValidateAni_mustBe(vParachuteAniName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 애니가 있는지 검사합니다.
	private void _ValidateAni_mustBe(String pAniName)
	{
		if (String.IsNullOrEmpty(pAniName))
		{
			Debug.LogError(String.Format("must-be ani is empty - {0}", this));
			return;
		}
		if (vAnimation.GetClip(pAniName) == null)
			Debug.LogError(String.Format("invalid ani name {0} - {1}", pAniName, this));
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 애니가 있는지 검사합니다.
	private void _ValidateAni_mayEmpty(String pAniName)
	{
		if (!String.IsNullOrEmpty(pAniName) &&
			(vAnimation.GetClip(pAniName) == null))
			Debug.LogError(String.Format("invalid ani name {0} - {1}", pAniName, this));
	}
	//-------------------------------------------------------------------------------------------------------
	// 애니를 지정합니다.
	private void _SetAni(String pAniName, AniTag pAniTag)
	{
		if (mLastAniName != pAniName)
		{
			mLastAniName = pAniName;
			vAnimation.CrossFade(pAniName, cCrossFadeDelay);
		}

		mLastAniTag = pAniTag;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private String mLastAniName;
	private AniTag mLastAniTag;
	private RealActorPositionUpdater.StopType mLastStopType;

	private String mMoraleSearchIdleLoopAniName;
	private String mMoraleMoveLoopAniName;
	private String mMoraleAttackIdleLoopAniName;
}
