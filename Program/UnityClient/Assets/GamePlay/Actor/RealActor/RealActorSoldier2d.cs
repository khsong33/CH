﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorSoldier2d : RealActor
{
	public enum ActionState
	{
		Idle,
		HoldMove,
		Move,
		Attack,
		PinDown,
		Die,
		StandUpMove,
		SitDownStop,

		WindupWeapon,
		ReloadWeapon,
		InstallWeapon,
		UnInstallWeapon,

		None,
		Count,
	}

	public delegate void OnActionCompleteDelegate(ActionState pActionState);
	public delegate void OnActionEventDelegate(ActionState pActionState, float pEventValue);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SpriteObject vSpriteObject;
	public RealActorSoldier2dAniLib vRealActorSoldier2dAniLib;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("RealActorSoldier2d[{0}]@{1}", aActorIdx, vActorManager);
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnNewActor(RealActorManager pActorManager, int pActorIdx, RealActorManager.ActorInfo pManagerActorInfo)
	{
		base.OnNewActor(pActorManager, pActorIdx, pManagerActorInfo);

		aPositionUpdater.aOnUpdatePositionDelegate = OnUpdatePosition;
		aPositionUpdater.aOnHoldPositionDelegate = OnHoldPosition;
		aBodyRotationUpdater.aOnUpdateRotationDelegate = OnUpdateBodyRotation;

		// 스프라이트를 준비합니다.
		if (vSpriteObject == null)
			Debug.LogError("vSpriteObject is empty - " + this);
		if (vRealActorSoldier2dAniLib == null)
			Debug.LogError("vRealActorSoldier2dAniLib is empty - " + this);
		if (vRealActorSoldier2dAniLib.vSpriteLib != vSpriteObject.vSpriteLib)
			Debug.LogError("vRealActorSoldier2dAniLib.vSpriteLib is different from vSpriteObject.vSpriteLib - " + this);

		vRealActorSoldier2dAniLib.Load(); // 애니를 로딩합니다.

		vSpriteObject.vSpriteAnimator.AnimationCompleted = _OnSpriteAnimationComplete;
		vSpriteObject.vSpriteAnimator.AnimationEventTriggered = _OnSpriteAnimationEventTriggered;

		// 액션 상태를 초기화합니다.
		mActionState = ActionState.None;

		mActionAniStates = new RealActorSoldier2dAniState[(int)ActionState.Count];
		mActionAniStates[(int)ActionState.Idle] = RealActorSoldier2dAniState.Idle;
		mActionAniStates[(int)ActionState.HoldMove] = RealActorSoldier2dAniState.HoldMove;
		mActionAniStates[(int)ActionState.Move] = RealActorSoldier2dAniState.Move;
		mActionAniStates[(int)ActionState.Attack] = RealActorSoldier2dAniState.Attack;
		mActionAniStates[(int)ActionState.PinDown] = RealActorSoldier2dAniState.PinDown;
		mActionAniStates[(int)ActionState.Die] = RealActorSoldier2dAniState.Die;
		mActionAniStates[(int)ActionState.StandUpMove] = RealActorSoldier2dAniState.StandUpMove;
		mActionAniStates[(int)ActionState.SitDownStop] = RealActorSoldier2dAniState.SitDownStop;
		mActionAniStates[(int)ActionState.WindupWeapon] = RealActorSoldier2dAniState.WindupWeapon;
		mActionAniStates[(int)ActionState.ReloadWeapon] = RealActorSoldier2dAniState.ReloadWeapon;
		mActionAniStates[(int)ActionState.InstallWeapon] = RealActorSoldier2dAniState.InstallWeapon;
		mActionAniStates[(int)ActionState.UnInstallWeapon] = RealActorSoldier2dAniState.UnInstallWeapon;
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnCreateActor()
	{
		//Debug.Log("OnCreateActor() - " + this);

		base.OnCreateActor();

		mSpriteObjectUpdateMask = SpriteObject.UpdateMask.None;
		_SetActionState(ActionState.Idle);
		mNextActionState = ActionState.None;
		mStopType = RealActorPositionUpdater.StopType.Arrived;
		mOnActionCompleteDelegate = null;
		mOnActionEventDelegate = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnDestroyActor()
// 	{
// 		//Debug.Log("OnDestroyActor() - " + this);
// 
// 		base.OnDestroyActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnVisibleActor()
// 	{
// 		base.OnVisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnInvisibleActor()
// 	{
// 		base.OnInvisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnUpdateActor(float pUpdateDelta)
	{
		base.OnUpdateActor(pUpdateDelta);

		// 스프라이트를 갱신합니다.
		if (mSpriteObjectUpdateMask != SpriteObject.UpdateMask.None)
		{
			vSpriteObject.UpdateChange(mSpriteObjectUpdateMask);
			mSpriteObjectUpdateMask = SpriteObject.UpdateMask.None;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
		switch (pTroopMoraleState)
		{
		case TroopMoraleState.Good:
			{
				mActionAniStates[(int)ActionState.Idle] = RealActorSoldier2dAniState.Idle;
				mActionAniStates[(int)ActionState.Move] = RealActorSoldier2dAniState.Move;
				mActionAniStates[(int)ActionState.Attack] = RealActorSoldier2dAniState.Attack;
			}
			break;
		case TroopMoraleState.Suppressed:
			{
				mActionAniStates[(int)ActionState.Idle] = RealActorSoldier2dAniState.IdleSuppressed;
				mActionAniStates[(int)ActionState.Move] = RealActorSoldier2dAniState.MoveSuppressed;
				mActionAniStates[(int)ActionState.Attack] = RealActorSoldier2dAniState.AttackSuppressed;
			}
			break;
		case TroopMoraleState.PinDown:
			{
				mActionAniStates[(int)ActionState.Idle] = RealActorSoldier2dAniState.PinDown;
			}
			break;
		}

		switch (mActionState)
		{
		case ActionState.Idle:
			_SetActionState(ActionState.Idle);
			break;
		case ActionState.Move:
			_SetActionState(ActionState.Move);
			break;
		case ActionState.Attack:
			_SetActionState(ActionState.Attack);
			break;
		}
	}
// 	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnStartMoveDelegate
// 	public void OnStartMove()
// 	{
// 		_ChangeActionState(ActionState.StandUpMove, ActionState.Move, null);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnUpdatePositionDelegate
	public void OnUpdatePosition(Vector3 pPosition, RealActorPositionUpdater.StopType pStopType)
	{
		aTransform.position = aStage.GetTerrainPosition(pPosition);

		if (mStopType != pStopType)
		{
			switch (mStopType)
			{
			case RealActorPositionUpdater.StopType.None: // 이동중이었다면
				{
					switch (pStopType)
					{
					case RealActorPositionUpdater.StopType.Arrived: // 도착했을 경우
						_ChangeActionState(ActionState.SitDownStop, ActionState.Idle, null); // 앉음
						break;
					case RealActorPositionUpdater.StopType.Hold: // 대기일 경우
						_ChangeActionState(ActionState.HoldMove, ActionState.Idle, null); // 대기
						break;
					}
				}
				break;
			case RealActorPositionUpdater.StopType.Arrived: // 도착 상태였다면
				{
					if (pStopType == RealActorPositionUpdater.StopType.None) // 이동할 경우
						_ChangeActionState(ActionState.StandUpMove, ActionState.Move, null); // 일어나며 이동
				}
				break;
			case RealActorPositionUpdater.StopType.Hold: // 대기 상태였다면
				{
					if (pStopType == RealActorPositionUpdater.StopType.None) // 이동할 경우
						_ChangeActionState(ActionState.Move, ActionState.None, null); // 바로 이동
				}
				break;
			}
			mStopType = pStopType;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater.OnHoldPositionDelegate
	public void OnHoldPosition(bool pHoldPosition)
	{
		switch (mStopType)
		{
		case RealActorPositionUpdater.StopType.None: // 이동중이었다면
			{
				if (pHoldPosition)
					_ChangeActionState(ActionState.HoldMove, ActionState.None, null); // 대기
			}
			break;
		case RealActorPositionUpdater.StopType.Arrived: // 도착했다면
			{
				if (!pHoldPosition)
					_ChangeActionState(ActionState.StandUpMove, ActionState.Move, null); // 일어나며 이동
			}
			break;
		case RealActorPositionUpdater.StopType.Hold: // 대기 상태라면
			{
				if (!pHoldPosition)
					_ChangeActionState(ActionState.Move, ActionState.None, null); // 이동
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorRotationUpdater.OnUpdateRotationDelegate
	public void OnUpdateBodyRotation(float pBodyDirectionY, bool pIsArrived)
	{
		// 게임 오브젝트를 회전하는 것이 아니라 스프라이트의 방향을 갱신합니다.
		vSpriteObject.vDirectionY = pBodyDirectionY + 180.0f; // 스프라이트는 0도가 정면을 향함
		mSpriteObjectUpdateMask |= SpriteObject.UpdateMask.Direction;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 액션 애니를 지정합니다.
	private void _SetActionState(ActionState pActionState)
	{
		//Debug.Log("_SetActionState() pActionState=" + (ActionState)pActionState + " - " + this);

		mActionState = pActionState;
		SpriteLib.Action lSpriteAction = vRealActorSoldier2dAniLib.aAniActions[(int)mActionAniStates[(int)pActionState]];
		if (vSpriteObject.vActionIdx != lSpriteAction.vActionIdx)
		{
			vSpriteObject.vActionIdx = lSpriteAction.vActionIdx;
			mSpriteObjectUpdateMask |= SpriteObject.UpdateMask.Action;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 애니 상태를 지정합니다.
	private void _ChangeActionState(ActionState pActionState, ActionState pNextActionState, OnActionCompleteDelegate pOnActionCompleteDelegate)
	{
		//Debug.Log("_ChangeActionState() pActionState=" + (ActionState)pActionState + " - " + aUnit);

		if (mActionState != pActionState)
			_SetActionState(pActionState);
		mNextActionState = pNextActionState;
		mOnActionCompleteDelegate = pOnActionCompleteDelegate;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트의 애니메이션이 끝났을 때 호출되는 콜백 함수
	private void _OnSpriteAnimationComplete(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip)
	{
		//Debug.Log("_OnSpriteAnimationComplete() - " + sprite);

		if (mNextActionState != ActionState.None)
			_SetActionState(mNextActionState);
	
		if (mOnActionCompleteDelegate != null)
			mOnActionCompleteDelegate(mActionState);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 애니메이션의 이벤트를 받았을 때 호출되는 콜백 함수
	private void _OnSpriteAnimationEventTriggered(tk2dSpriteAnimator sprite, tk2dSpriteAnimationClip clip, int frameId)
	{
		if (mOnActionEventDelegate != null)
		{
			tk2dSpriteAnimationFrame lEventFrame = clip.frames[frameId];
			mOnActionEventDelegate(mActionState, lEventFrame.eventFloat);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [OnActionCompleteDelegate] 앉은 이후에 아이들 동작으로 전환합니다.
	private void _OnActionComplete_SitDownStop(ActionState pActionState)
	{
		_ChangeActionState(ActionState.Idle, ActionState.None, _OnActionComplete_SitDownStop);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private SpriteObject.UpdateMask mSpriteObjectUpdateMask;
	private ActionState mActionState;
	private ActionState mNextActionState;
	private RealActorPositionUpdater.StopType mStopType;
	private OnActionCompleteDelegate mOnActionCompleteDelegate;
	private OnActionEventDelegate mOnActionEventDelegate;

	private RealActorSoldier2dAniState[] mActionAniStates;
}
