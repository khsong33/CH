﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum RealActorSoldier2dAniState
{
	Idle,
	IdleSuppressed,
	HoldMove,
	Move,
	MoveSuppressed,
	Attack,
	AttackSuppressed,
	PinDown,
	Die,
	StandUpMove,
	SitDownStop,

	WindupWeapon,
	ReloadWeapon,
	InstallWeapon,
	UnInstallWeapon,
}

public class RealActorSoldier2dAniLib : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SpriteLib vSpriteLib;

	public String vIdleActionName;
	public String vIdleSuppressedActionName;
	public String vHoldMoveActionName;
	public String vMoveActionName;
	public String vMoveSuppressedActionName;
	public String vAttackActionName;
	public String vAttackSuppressedActionName;
	public String vPinDownActionName;
	public String vDieActionName;
	public String vStandUpMoveActionName;
	public String vSitDownStopActionName;

	public String vWindupWeaponActionName;
	public String vReloadWeaponActionName;
	public String vInstallWeaponActionName;
	public String vUnInstallWeaponActionName;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public SpriteLib.Action[] aAniActions
	{
		get { return mAniActions; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 종료 콜백
#if UNITY_EDITOR
	void OnApplicationQuit()
	{
		Free();
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 로딩합니다.
	public void Load()
	{
		//Debug.Log("Load() - " + this);
		//Debug.Log("mIsLoaded=" + mIsLoaded);

		if (_CheckLoaded(this))
			return;
		_SetLoaded(this);
		_Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 해제합니다.
	public void Free()
	{
		//Debug.Log("Free() - " + this);

		if (!_CheckLoaded(this))
			return;
		_ResetLoaded(this);
		_Free();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 로딩합니다.
	private void _Load()
	{
		//Debug.Log("_Load() - " + this);

		if (vSpriteLib == null)
		{
			vSpriteLib = GetComponent<SpriteLib>();
			if (vSpriteLib == null)
				Debug.LogError("can't find SpriteLib - " + this);
		}
		vSpriteLib.Load();

		_SetUpAniActions();
	}
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 해제합니다.
	private void _Free()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 애니 액션 테이블을 구축합니다.
	private void _SetUpAniActions()
	{
		int lAniCount = Enum.GetValues(typeof(RealActorSoldier2dAniState)).Length;
		mAniActions = new SpriteLib.Action[lAniCount];
		
		vSpriteLib.aActionDictionary.TryGetValue(vIdleActionName, out mAniActions[(int)RealActorSoldier2dAniState.Idle]);
		vSpriteLib.aActionDictionary.TryGetValue(vIdleSuppressedActionName, out mAniActions[(int)RealActorSoldier2dAniState.IdleSuppressed]);
		vSpriteLib.aActionDictionary.TryGetValue(vHoldMoveActionName, out mAniActions[(int)RealActorSoldier2dAniState.HoldMove]);
		vSpriteLib.aActionDictionary.TryGetValue(vMoveActionName, out mAniActions[(int)RealActorSoldier2dAniState.Move]);
		vSpriteLib.aActionDictionary.TryGetValue(vMoveSuppressedActionName, out mAniActions[(int)RealActorSoldier2dAniState.MoveSuppressed]);
		vSpriteLib.aActionDictionary.TryGetValue(vAttackActionName, out mAniActions[(int)RealActorSoldier2dAniState.Attack]);
		vSpriteLib.aActionDictionary.TryGetValue(vAttackSuppressedActionName, out mAniActions[(int)RealActorSoldier2dAniState.AttackSuppressed]);
		vSpriteLib.aActionDictionary.TryGetValue(vPinDownActionName, out mAniActions[(int)RealActorSoldier2dAniState.PinDown]);
		vSpriteLib.aActionDictionary.TryGetValue(vDieActionName, out mAniActions[(int)RealActorSoldier2dAniState.Die]);
		vSpriteLib.aActionDictionary.TryGetValue(vStandUpMoveActionName, out mAniActions[(int)RealActorSoldier2dAniState.StandUpMove]);
		vSpriteLib.aActionDictionary.TryGetValue(vSitDownStopActionName, out mAniActions[(int)RealActorSoldier2dAniState.SitDownStop]);
		vSpriteLib.aActionDictionary.TryGetValue(vWindupWeaponActionName, out mAniActions[(int)RealActorSoldier2dAniState.WindupWeapon]);
		vSpriteLib.aActionDictionary.TryGetValue(vReloadWeaponActionName, out mAniActions[(int)RealActorSoldier2dAniState.ReloadWeapon]);
		vSpriteLib.aActionDictionary.TryGetValue(vInstallWeaponActionName, out mAniActions[(int)RealActorSoldier2dAniState.InstallWeapon]);
		vSpriteLib.aActionDictionary.TryGetValue(vUnInstallWeaponActionName, out mAniActions[(int)RealActorSoldier2dAniState.UnInstallWeapon]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 로딩된 스프라이트 라이브러리 색인 사전을 얻습니다.
	private static bool _CheckLoaded(RealActorSoldier2dAniLib pRealActorSoldier2dAniLib)
	{
		if (sRealActorSoldier2dAniLibDictionary == null)
			sRealActorSoldier2dAniLibDictionary = new Dictionary<RealActorSoldier2dAniLib, RealActorSoldier2dAniLib>();

		RealActorSoldier2dAniLib lRealActorSoldier2dAniLib;
		return sRealActorSoldier2dAniLibDictionary.TryGetValue(pRealActorSoldier2dAniLib, out lRealActorSoldier2dAniLib);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 라이브러리를 로딩합니다.
	public static void _SetLoaded(RealActorSoldier2dAniLib pRealActorSoldier2dAniLib)
	{
		sRealActorSoldier2dAniLibDictionary.Add(pRealActorSoldier2dAniLib, pRealActorSoldier2dAniLib);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 라이브러리를 삭제합니다.
	public static void _ResetLoaded(RealActorSoldier2dAniLib pRealActorSoldier2dAniLib)
	{
		sRealActorSoldier2dAniLibDictionary.Remove(pRealActorSoldier2dAniLib);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Dictionary<RealActorSoldier2dAniLib, RealActorSoldier2dAniLib> sRealActorSoldier2dAniLibDictionary = null;

	private SpriteLib.Action[] mAniActions;
}
