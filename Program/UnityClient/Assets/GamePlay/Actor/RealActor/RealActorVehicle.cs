﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RealActorVehicle : RealActor
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("RealActorVehicle[{0}]@{1}", aActorIdx, vActorManager);
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
	public override void OnNewActor(RealActorManager pActorManager, int pActorIdx, RealActorManager.ActorInfo pManagerActorInfo)
	{
		base.OnNewActor(pActorManager, pActorIdx, pManagerActorInfo);

		aPositionUpdater.aOnUpdatePositionDelegate = OnUpdatePosition;
		aPositionUpdater.aOnHoldPositionDelegate = OnHoldPosition;
		aBodyRotationUpdater.aOnUpdateRotationDelegate = OnUpdateBodyRotation;
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnCreateActor()
// 	{
// 		//Debug.Log("OnCreateActor() - " + this);
// 
// 		base.OnCreateActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnDestroyActor()
// 	{
// 		//Debug.Log("OnDestroyActor() - " + this);
// 
// 		base.OnDestroyActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnVisibleActor()
// 	{
// 		base.OnVisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnInvisibleActor()
// 	{
// 		base.OnInvisibleActor();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnUpdateActor(float pUpdateDelta)
// 	{
// 		base.OnUpdateActor(pUpdateDelta);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActor 메서드
// 	public override void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
// 	{
// 	}
// 	//-------------------------------------------------------------------------------------------------------
	// 이동을 준비합니다.
// 	public override void StartMove()
// 	{
// 		_ChangeActionState(ActionState.StandUpMove, ActionState.Move, null);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionUpdater
	public void OnUpdatePosition(Vector3 pPosition, RealActorPositionUpdater.StopType pStopType)
	{
		aTransform.position = pPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorPositionHoldr
	public void OnHoldPosition(bool pHoldPosition)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// RealActorRotationUpdater
	public void OnUpdateBodyRotation(float pBodyDirectionY, bool pIsArrived)
	{
		aTransform.rotation = MathLib.GetRotation(pBodyDirectionY);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
