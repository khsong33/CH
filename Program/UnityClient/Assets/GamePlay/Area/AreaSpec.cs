﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AreaSpec
{
	public int mArea;
	public int mUpPoint;
	public String mNameKey;
	public AssetKey mIconAssetKey;

	public AreaSpec()
	{
	}
}
