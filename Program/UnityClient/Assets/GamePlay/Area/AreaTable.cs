﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AreaTable
{
	public static readonly String cCSVTextAssetPath = "AreaTableCSV";

	public static readonly String vAreaColumnName = "Area";
	public static readonly String vUpPointColumnName = "UpPoint";
	public static readonly String vNameKeyColumnName = "NameKey";
	public static readonly String vIconBundleColumnName = "IconBundle";
	public static readonly String vIconPathColumnName = "IconPath";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static AreaTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new AreaTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public AreaTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mDicAreaSpecs = new Dictionary<int, AreaSpec>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			AreaSpec info = new AreaSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mArea = lCSVRow.GetIntValue(vAreaColumnName);
			info.mUpPoint = lCSVRow.GetIntValue(vUpPointColumnName);
			info.mNameKey = lCSVRow.GetStringValue(vNameKeyColumnName);
			info.mIconAssetKey = AssetManager.get.CreateAssetKey(
				lCSVRow.GetStringValue(vIconBundleColumnName),
				lCSVRow.GetStringValue(vIconPathColumnName));

			mDicAreaSpecs.Add(info.mArea, info);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	public AreaSpec GetAreaSpec(int pArea)
	{
		if (!mDicAreaSpecs.ContainsKey(pArea))
		{
			Debug.LogError("Not Exist Area - " + pArea);
			return null;
		}
		return mDicAreaSpecs[pArea];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("AreaTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static AreaTable sInstance = null;

	private CSVObject mCSVObject;
	private Dictionary<int, AreaSpec> mDicAreaSpecs;
}
