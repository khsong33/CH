﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderAreaSpec
{
	public int mArea;
	public CommanderItemData mCommanderItemData;
	public CommanderItem mCommanderItem;

	public CommanderAreaSpec()
	{
		mCommanderItem = null;
	}
}
