﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderAreaTable
{
	public static readonly String cCSVTextAssetPath = "CommanderAreaTableCSV";

	public static readonly String vAreaColumnName = "Area";
	public static readonly String vCommanderNoColumnName = "CommanderIndex";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CommanderAreaTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CommanderAreaTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderAreaTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mDicCommanderAreaSpecs = new Dictionary<int, Dictionary<DeckNation, List<CommanderAreaSpec>>>();
		mDicNationCommanderCount = new Dictionary<DeckNation, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CommanderAreaSpec info = new CommanderAreaSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			info.mArea = lCSVRow.GetIntValue(vAreaColumnName);
			int lCommanderSpecNo = lCSVRow.GetIntValue(vCommanderNoColumnName);
			CommanderItemData lCommanderItemData = new CommanderItemData();
			lCommanderItemData.Init(lCommanderSpecNo);
			info.mCommanderItemData = lCommanderItemData;
			info.mCommanderItem = new CommanderItem(0);
			info.mCommanderItem.Init(CommanderItemType.UserCommander, lCommanderItemData);

			DeckNation lNation = info.mCommanderItem.mCommanderSpec.mDeckNation;
			if(!mDicCommanderAreaSpecs.ContainsKey(info.mArea))
			{
				mDicCommanderAreaSpecs.Add(info.mArea, new Dictionary<DeckNation, List<CommanderAreaSpec>>());
			}
			if (!mDicCommanderAreaSpecs[info.mArea].ContainsKey(lNation))
			{
				mDicCommanderAreaSpecs[info.mArea].Add(lNation, new List<CommanderAreaSpec>());
			}
			mDicCommanderAreaSpecs[info.mArea][lNation].Add(info);

			if (!mDicNationCommanderCount.ContainsKey(lNation))
			{
				mDicNationCommanderCount.Add(lNation, 0);
			}
			mDicNationCommanderCount[lNation]++;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Area별 AreaSpec 목록 얻어오기
	public List<CommanderAreaSpec> GetCommanderAreaSpecList(int pArea, DeckNation pNation)
	{
		if (!mDicCommanderAreaSpecs.ContainsKey(pArea))
			return null;
		if (!mDicCommanderAreaSpecs[pArea].ContainsKey(pNation))
			return null;
		return mDicCommanderAreaSpecs[pArea][pNation];
	}
	//-------------------------------------------------------------------------------------------------------
	// Nation 유닛 총수 얻어오기
	public int GetNationMaxCommanderCount(DeckNation pNation)
	{
		if (!mDicNationCommanderCount.ContainsKey(pNation))
		{
			Debug.LogError("Not Exist Nation - " + pNation.ToString());
			return -1;
		}
		return mDicNationCommanderCount[pNation];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderAreaTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CommanderAreaTable sInstance = null;

	private CSVObject mCSVObject;
	private Dictionary<int, Dictionary<DeckNation, List<CommanderAreaSpec>>> mDicCommanderAreaSpecs;
	private Dictionary<DeckNation, int> mDicNationCommanderCount;
}
