﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class BattleAiCollector
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 승리 규칙
	public BattleMatchInfo.RuleType aRuleType
	{
		get { return mRuleType; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이 오브젝트를 파괴합니다.
	public void Destroy()
	{
		OnDestroy();

		mBattleLogic.DestroyAiCollector(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleAiCollector(BattleLogic pBattleLogic, BattleMatchInfo.RuleType pRuleType)
	{
		mBattleLogic = pBattleLogic;
		mRuleType = pRuleType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreate()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public virtual void OnDestroy()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트를 진행합니다.
	public abstract void UpdateFrame();
	//-------------------------------------------------------------------------------------------------------
	// 선 업데이트를 1회 진행합니다.
	public abstract void PreUpdate();
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 프리퀀시를 정합니다.
	public abstract void SetUpdateFrequency(int pFrequency);
	//-------------------------------------------------------------------------------------------------------
	// 맵 기본 정보를 얻어옵니다.
	public abstract void SetBattleInfo(BattleMatchInfo pBattleMatchInfo, BattleGrid pBattleGrid, BattleProgressInfo pBattleProgressInfo);
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 추가합니다.
	public abstract void AddCommander(IBattleAiCommanderData pCommander);
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 삭제합니다.
	public abstract void RemoveCommander(IBattleAiCommanderData pCommander);

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;
	private BattleMatchInfo.RuleType mRuleType;
}
