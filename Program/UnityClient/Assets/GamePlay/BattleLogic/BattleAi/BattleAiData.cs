﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
//-------------------------------------------------------------------------------------------------------
// AI 판별용 전투 지휘관 정보 인터페이스
//-------------------------------------------------------------------------------------------------------
public interface IBattleAiCommanderData
{
	int aId { get; } // 고정 Id

	int aAiUserIdx { get; } // AI를 처리하는 유저 번호
	bool aIsActiveAi { get; } // AI 조정 활성화 여부
	uint aClientGuid { get; } // 고유 유저 식별 Id
	int aTeamIdx { get; } // 팀 Id
	bool aIsAlive { get; } // 현재 살아있는지 확인(Spawn이 되어있다면 True)
	bool aIsCoolTime { get; } // 현재 쿨타임이어서 소환을 못하는지 확인(쿨타임 중에는 소환 불가)
	bool aIsBattle { set; get; } // 명령을 받을 수 있는 상태, 전투 상태
	int aSpawnMp { get; } // 현재 Commander 소환 소모 Mp
	int aHpPercent { get; } // 현재 Commander Hp
	Coord2 aCoord { get; } // 현재 Commander 위치
	BattleTroop[] aTroops { get; } // 현재 Commander의 Troop 정보
	BattleTroop aLeaderTroop { get; } // 현재 Commander의 Leader 부대
	int aSummonOrder { get; } // 지휘관 소환 우선순위
	CommanderType aCommanderType { get; } // 커맨더 타입
	CommanderClass aCommanderClass { get; } // 커맨더 클래스
	bool aIsCaptureCheckPoint { get; } // 점령지 점령 가능

	int aUpdateIndex { set; get; } // AI 판단 업데이트시에 해당 index로 몇 frame update할지 결정
	int aOriginUpdateIndex { set; get; } // AI 판단 업데이트시 해당 index로 몇 frame update할지 결정 원본

	// 해당 지휘관의 부대를 Check point로 이동합니다.
	void OnMoveCheckPoint(int pCheckPointIdx);
	// 해당 지휘관의 부대를 본부로 귀환시킵니다.
	void OnMoveHQ(int pUserIdx);
	// 해당 지휘관의 부대를 공격합니다.
	void OnAttack(BattleTroop pTargetTroop);
	// 해당 지휘관의 부대 위치로 이동 명령을 내립니다.
	void OnAttackMove(Coord2 pTargetCoord);
	// 해당 지휘관의 부대 위치로 이동 명령을 내립니다.(방향정의)
	void OnAttackMoveDirection(Coord2 pTargetCoord, int lDirectionY);
}