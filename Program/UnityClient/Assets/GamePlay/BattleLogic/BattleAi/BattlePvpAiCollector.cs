﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattlePvpAiCollector : BattleAiCollector
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattlePvpAiCollector(BattleLogic pBattleLogic)
		: base(pBattleLogic, BattleMatchInfo.RuleType.Conquest)
	{
		mCommanderList = new Dictionary<int, IBattleAiCommanderData>();

		mTeamSpawnPoint = new Dictionary<int, int>();
		mTeamIdxToUserIdx = new Dictionary<int, int>();

		mTeamMaxInfluence = new Dictionary<int, float>();
		mCommanderInfluenceValue = new Dictionary<int, Dictionary<int, float>>();
		mTeamInfluenceValue = new Dictionary<int, Dictionary<int, float>>();
		mCheckPointTeamWeightValues = new Dictionary<int, Dictionary<int, float>>();
		mTeamCheckPointVpConquerValue = new Dictionary<int, float>();
		mTeamCheckPointMpConquerValue = new Dictionary<int, float>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public override void OnCreate()
	{
		base.OnCreate();

		mTeamSpawnPoint.Add(1, 0);
		mTeamSpawnPoint.Add(2, 0);

		mCurrentFrame = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		mCommanderList.Clear();

		mTeamSpawnPoint.Clear();
		mTeamIdxToUserIdx.Clear();

		mTeamMaxInfluence.Clear();
		mCommanderInfluenceValue.Clear();
		mTeamInfluenceValue.Clear();
		mCheckPointTeamWeightValues.Clear();
		mTeamCheckPointVpConquerValue.Clear();
		mTeamCheckPointMpConquerValue.Clear();

		base.OnDestroy();
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트를 진행합니다.
	public override void UpdateFrame()
	{
		if (mCurrentFrame == mUpdateFrequency)
		{
			_UpdateAiData();
			mCurrentFrame = 0;
		}
		else
		{
			mCurrentFrame++;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 선 업데이트를 1회 진행합니다.
	public override void PreUpdate()
	{
		_UpdateAiData();
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 프리퀀시를 정합니다.
	public override void SetUpdateFrequency(int pFrequency)
	{
		mUpdateFrequency = pFrequency;
	}
	//-------------------------------------------------------------------------------------------------------
	// 맵 기본 정보를 얻어옵니다.
	public override void SetBattleInfo(BattleMatchInfo pBattleMatchInfo, BattleGrid pBattleGrid, BattleProgressInfo pBattleProgressInfo)
	{
		mBattleMatchInfo = pBattleMatchInfo;
		mBattleGrid = pBattleGrid;
		mBattleProgressInfo = pBattleProgressInfo;

		mTeamIdxToUserIdx.Clear();
		for (int iUser = 0; iUser < mBattleMatchInfo.mUserCount; iUser++)
		{
			mTeamIdxToUserIdx.Add(mBattleMatchInfo.mUserInfos[iUser].mTeamIdx, mBattleMatchInfo.mUserInfos[iUser].mUserIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 추가합니다.
	public override void AddCommander(IBattleAiCommanderData pCommander)
	{
		mCommanderList.Add(pCommander.aId, pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 삭제합니다.
	public override void RemoveCommander(IBattleAiCommanderData pCommander)
	{
		if (!mCommanderList.ContainsKey(pCommander.aId))
		{
			Debug.LogError("Not Exist Commander id - " + pCommander.aId);
			return;
		}
		mCommanderList.Remove(pCommander.aId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 승리 거점 점령 여부를 가져옵니다. (True : 승리 거점을 점령합니다 / False : 보급 거점을 점령합니다)
	public bool IsConquerVitoryPoint(int pTeamIdx)
	{
		if (mTeamCheckPointVpConquerValue[pTeamIdx] > mTeamCheckPointMpConquerValue[pTeamIdx])
			return true;

		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 최대 영향력을 가져옵니다.
	public float GetTeamMaxInfluence(int pTeamIdx)
	{
		if (mTeamMaxInfluence.ContainsKey(pTeamIdx))
		{
			return mTeamMaxInfluence[pTeamIdx];
		}
		return 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 개별 영향력을 가져옵니다.
	public float GetCommanderInfluence(int pCommanderId, int pCheckPointIdx)
	{
		if (mCommanderInfluenceValue.ContainsKey(pCheckPointIdx))
		{
			if (mCommanderInfluenceValue[pCheckPointIdx].ContainsKey(pCommanderId))
			{
				return mCommanderInfluenceValue[pCheckPointIdx][pCommanderId];
			}
		}
		return 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 영향력을 가져옵니다.
	public float GetInfluence(int pTeamIdx, int pCheckPointIdx)
	{
		if (mTeamInfluenceValue.ContainsKey(pTeamIdx))
		{
			if (mTeamInfluenceValue[pTeamIdx].ContainsKey(pCheckPointIdx))
			{
				return mTeamInfluenceValue[pTeamIdx][pCheckPointIdx];
			}
		}
		return 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 세력값을 가져옵니다.
	public float GetCheckPointWeightValue(int pCheckPoint, int pTeamIdx)
	{
		if (mCheckPointTeamWeightValues.ContainsKey(pCheckPoint))
		{
			if (mCheckPointTeamWeightValues[pCheckPoint].ContainsKey(pTeamIdx))
			{
				return mCheckPointTeamWeightValues[pCheckPoint][pTeamIdx];
			}
		}
		return 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// AI 관련 데이터 계산을 진행합니다.
	private void _UpdateAiData()
	{
		// 거점 가치를 계산합니다.
		_UpdateCheckPointConquerValue();
		// 지휘관별 거점에 대한 영향력을 계산합니다.
		_UpdateInfluence();
	}
	//-------------------------------------------------------------------------------------------------------
	// 각 지휘관의 영향력을 계산합니다.
	private void _UpdateInfluence()
	{
		List<BattleCheckPoint> lCheckPointList = mBattleGrid.aCheckPointList;

		// 1팀, 2팀 spawn총합 초기화
		mTeamSpawnPoint[1] = 0;
		mTeamSpawnPoint[2] = 0;

		for (int iCheckPoint = 0; iCheckPoint < lCheckPointList.Count; iCheckPoint++)
		{
			if (lCheckPointList[iCheckPoint].aTeamIdx == 0)
				continue;
			if (lCheckPointList[iCheckPoint].aLinkHeadquarter != null)
				continue;
			// 기존 데이터 초기화
			_ResetInfluenceData(1, lCheckPointList[iCheckPoint].aCheckPointIdx);
			_ResetInfluenceData(2, lCheckPointList[iCheckPoint].aCheckPointIdx);

			//for (int iCommander = 0; iCommander < mCommanderList.Count; iCommander++)
			Dictionary<int, IBattleAiCommanderData>.Enumerator iter = mCommanderList.GetEnumerator();
			while(iter.MoveNext())
			{
				IBattleAiCommanderData lCommander = iter.Current.Value;
				if (lCommander.aTeamIdx == 0) // 중립팀일 경우 계산에서 제외
					continue;

				BattleTroop lCommanderTroop = lCommander.aLeaderTroop;

				int lMaxAttackRange = 0;
				int lMinAttackRange = 0;
				int lMoveSpeed = 0;
				if (lCommanderTroop != null)
				{
					lMaxAttackRange = lCommanderTroop.aStat.aMaxAttackRange / 100;
					lMinAttackRange = lCommanderTroop.aStat.aMinAttackRange / 100;
					lMoveSpeed = lCommanderTroop.aStat.aStatMoveSpeed / 100;
					// 거점 가치 계산을 위한 현재 나와있는 지휘관의 Spawn MP 계산
					mTeamSpawnPoint[lCommander.aTeamIdx] += lCommander.aSpawnMp;
				}
				else
				{
					continue;
				}
				RowCol lCommanderRowCol = mBattleGrid.GetTileIndex(lCommander.aCoord);
				RowCol lCheckPointRowCol = mBattleGrid.GetTileIndex(lCheckPointList[iCheckPoint].aCoord);
				int lDistanceFromCommander = BattleCoordDistTable.get.GetDistance(lCommanderRowCol, lCheckPointRowCol) / 100;
				int lCheckInfluence = lDistanceFromCommander - lMaxAttackRange;
				float lInfluence = 0.0f;
				if (lCheckInfluence >= 0)
				{
					lInfluence = 1.0f / (((float)(lDistanceFromCommander - lMaxAttackRange) / lMoveSpeed) * BattleConfig.get.mInfluenceSlopeCons + 1.0f);
				}
				else
				{
					if (lDistanceFromCommander - lMinAttackRange >= 0)
						lInfluence = 1.0f;
					else
						lInfluence = 1.0f / ((float)(-1.0f * (lDistanceFromCommander - lMinAttackRange) / lMoveSpeed) * BattleConfig.get.mInfluenceSlopeCons + 1.0f);
				}
				// 팀별 최대 영향력을 저장합니다.
				if (!mTeamMaxInfluence.ContainsKey(lCommander.aTeamIdx))
				{
					mTeamMaxInfluence.Add(lCommander.aTeamIdx, lInfluence);
				}
				else if (mTeamMaxInfluence[lCommander.aTeamIdx] <= lInfluence)
				{
					mTeamMaxInfluence[lCommander.aTeamIdx] = lInfluence;
				}

				// 커맨더별 영향력값을 저장합니다.
				if (!mCommanderInfluenceValue.ContainsKey(lCheckPointList[iCheckPoint].aCheckPointIdx))
				{
					mCommanderInfluenceValue.Add(lCheckPointList[iCheckPoint].aCheckPointIdx, new Dictionary<int, float>());
				}
				if (!mCommanderInfluenceValue[lCheckPointList[iCheckPoint].aCheckPointIdx].ContainsKey(lCommander.aId))
				{
					mCommanderInfluenceValue[lCheckPointList[iCheckPoint].aCheckPointIdx].Add(lCommander.aId, 0.0f);
				}
				mCommanderInfluenceValue[lCheckPointList[iCheckPoint].aCheckPointIdx][lCommander.aId] = lInfluence;

				// 현재 CheckPoint의 영향력값 합산합니다. (팀별 합산)
				if (!mTeamInfluenceValue.ContainsKey(lCommander.aTeamIdx))
				{
					mTeamInfluenceValue.Add(lCommander.aTeamIdx, new Dictionary<int, float>());
				}
				if (!mTeamInfluenceValue[lCommander.aTeamIdx].ContainsKey(lCheckPointList[iCheckPoint].aCheckPointIdx))
				{
					mTeamInfluenceValue[lCommander.aTeamIdx].Add(lCheckPointList[iCheckPoint].aCheckPointIdx, 0);
				}
				mTeamInfluenceValue[lCommander.aTeamIdx][lCheckPointList[iCheckPoint].aCheckPointIdx] += lInfluence;

				// 세력값을 업데이트합니다.
				_UpdateCheckPointTeamWeight(lCommander, lCheckPointList[iCheckPoint], lInfluence);
			}
			//for (int iUser = 0; iUser < mBattleMatchInfo.mUserCount; iUser++)
			//{
			//	int lUserIdx = mBattleGrid.aUsers[iUser].aUserIdx;
			//	if (lUserIdx >= 0 && mBattleGrid.aUsers.Length > lUserIdx)
			//	{
			//		if (mBattleGrid.aUsers[lUserIdx].aIsLogAi)
			//		{
			//			Debug.Log(String.Format("TeamWeightValues Check [TeamIdx : {0}] [CheckPoint Idx: {1}] = [Influence : {2}]",
			//				mBattleGrid.aUsers[lUserIdx].aTeamIdx, lCheckPointList[iCheckPoint].aCheckPointIdx,
			//				mCheckPointTeamWeightValues[lCheckPointList[iCheckPoint].aCheckPointIdx][mBattleGrid.aUsers[lUserIdx].aTeamIdx]));
			//		}
			//	}
			//}
		} 
	}
	//-------------------------------------------------------------------------------------------------------
	// 영향력 및 세력 데이터를 초기화합니다.
	private void _ResetInfluenceData(int pTeamIdx, int pCheckPointIdx)
	{
		// 기존 데이터 초기화
		if (mTeamInfluenceValue.ContainsKey(pTeamIdx))
		{
			if (mTeamInfluenceValue[pTeamIdx].ContainsKey(pCheckPointIdx))
			{
				mTeamInfluenceValue[pTeamIdx][pCheckPointIdx] = 0;
			}
		}
		if (mCheckPointTeamWeightValues.ContainsKey(pCheckPointIdx))
		{
			if (mCheckPointTeamWeightValues[pCheckPointIdx].ContainsKey(pTeamIdx))
			{
				mCheckPointTeamWeightValues[pCheckPointIdx][pTeamIdx] = 0;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 세력값을 업데이트합니다.
	private void _UpdateCheckPointTeamWeight(IBattleAiCommanderData pCommander, BattleCheckPoint pCheckPoint, float pInfluence)
	{
		int lUserMp = 0;
		for (int iUser = 0; iUser < mBattleMatchInfo.mUserCount; iUser++)
		{
			BattleUserInfo lUser = mBattleMatchInfo.mUserInfos[iUser];
			if (lUser.mClientGuid == pCommander.aClientGuid)
			{
				lUserMp = mBattleProgressInfo.aUserMps[lUser.mUserIdx];
			}
		}

		int lCommanderTroopCount = 0;
		float lTempWeight = 0;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			if (pCommander.aTroops[iTroop] != null)
				lCommanderTroopCount++;
		}
		lTempWeight = (int)((float)pInfluence * (float)lUserMp * ((float)lCommanderTroopCount / (float)CommanderTroopSet.cMaxTroopCount) * BattleConfig.get.mMPMultiplierCons);
		if (!mCheckPointTeamWeightValues.ContainsKey(pCheckPoint.aCheckPointIdx))
		{
			mCheckPointTeamWeightValues.Add(pCheckPoint.aCheckPointIdx, new Dictionary<int, float>());
		}
		if (!mCheckPointTeamWeightValues[pCheckPoint.aCheckPointIdx].ContainsKey(pCommander.aTeamIdx))
		{
			mCheckPointTeamWeightValues[pCheckPoint.aCheckPointIdx].Add(pCommander.aTeamIdx, 0);
		}
		mCheckPointTeamWeightValues[pCheckPoint.aCheckPointIdx][pCommander.aTeamIdx] += lTempWeight;
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 가치를 계산합니다.
	private void _UpdateCheckPointConquerValue()
	{
		Dictionary<int, int> lConquerVPCount = new Dictionary<int, int>();
		Dictionary<int, int> lConquerMPCount = new Dictionary<int, int>();
		List<BattleCheckPoint> lCheckPointList = mBattleGrid.aCheckPointList;
		// 1팀과 2팀 초기값을 넣어줍니다.
		lConquerVPCount.Add(1, 0);
		lConquerVPCount.Add(2, 0);
		lConquerMPCount.Add(1, 0);
		lConquerMPCount.Add(2, 0);
		// 각 팀별 거점 갯수를 얻어옵니다.
		for (int iCheckPoint = 0; iCheckPoint < lCheckPointList.Count; iCheckPoint++)
		{
			// 중립은 무시합니다.
			if (lCheckPointList[iCheckPoint].aTeamIdx == 0)
				continue;
			BattleCheckPoint lCheckPoint = lCheckPointList[iCheckPoint];
			// 승리거점 갯수 추가
			if (lCheckPoint.aStarId != 0)
			{
				lConquerVPCount[lCheckPoint.aTeamIdx] += 1;
			}
			else
			{
				lConquerMPCount[lCheckPoint.aTeamIdx] += 1;
			}
		}
		// 1팀과 2팀의 정보를 통해 계산을 진행합니다.
		_CalcCheckPointConquerValue(1, lConquerVPCount[1], lConquerMPCount[1], 2, lConquerVPCount[2], lConquerMPCount[2]);
		_CalcCheckPointConquerValue(2, lConquerVPCount[2], lConquerMPCount[2], 1, lConquerVPCount[1], lConquerMPCount[1]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 거점 가치를 계산합니다.
	private void _CalcCheckPointConquerValue(int lAllyTeamIdx, int lAllyVPCount, int lAllyMPCount, int lEnemyTeamIdx, int lEnemyVPCount, int lEnemyMPCount)
	{
		float lVPWeight = 0f;
		float lVPLocalWeight = 0f;

		int lAllyTeamPercentVP = (int)((float)mBattleProgressInfo.aTeamMilliVps[lAllyTeamIdx] / (float)BattleConfig.get.mTestMaxVp) * 100;
		int lEnemyTeamPercentVP = (int)((float)mBattleProgressInfo.aTeamMilliVps[lEnemyTeamIdx] / (float)BattleConfig.get.mTestMaxVp) * 100;

		int lTempVP = lAllyTeamPercentVP - lEnemyTeamPercentVP;
		// 승리 포인트 가치
		if (lTempVP == 0)
		{
			lVPWeight = 1;
		}
		else
		{
			lVPWeight = Mathf.Pow(BattleConfig.get.mVPValueCons1, (-1 * lTempVP) / BattleConfig.get.mVPCompareCons);
		}
		// 승리 거점 가치
		int lTempLocalVP = lAllyVPCount - lEnemyVPCount;
		if (lTempLocalVP == 0)
		{
			lVPLocalWeight = 1.0f;
		}
		else
		{
			lVPLocalWeight = Mathf.Pow(BattleConfig.get.mVPValueCons2, (-1 * (lTempLocalVP)));
		}
		// 승리 거점 최종 계산
		if (!mTeamCheckPointVpConquerValue.ContainsKey(lAllyTeamIdx))
		{
			mTeamCheckPointVpConquerValue.Add(lAllyTeamIdx, lVPWeight * lVPLocalWeight);
		}
		else
		{
			mTeamCheckPointVpConquerValue[lAllyTeamIdx] = lVPWeight * lVPLocalWeight;
		}

		float lMPWeight = 0f;
		float lMPLocalWeight = 0f;
		int lAllyTeamUserIdx = mTeamIdxToUserIdx[lAllyTeamIdx];
		int lEnemyTeamUserIdx = mTeamIdxToUserIdx[lEnemyTeamIdx];
		// 보급 포인트 가치계산
		int lTempMP = (mTeamSpawnPoint[lAllyTeamIdx] + mBattleProgressInfo.aUserMps[lAllyTeamUserIdx]) - (mTeamSpawnPoint[lEnemyTeamIdx] + mBattleProgressInfo.aUserMps[lEnemyTeamUserIdx]);
		if (lTempMP == 0)
		{
			lMPWeight = 1;
		}
		else
		{
			lMPWeight = Mathf.Pow(BattleConfig.get.mMPValueCons1, (-1 * lTempMP) / BattleConfig.get.mMPCompareCons);
		}
		// 보급 거점 가치 계산
		int lTempLocalMP = lAllyMPCount - lEnemyMPCount;
		if (lTempLocalMP == 0)
		{
			lMPLocalWeight = 1.0f;
		}
		else
		{
			lMPLocalWeight = Mathf.Pow(BattleConfig.get.mMPValueCons2, (-1 * lTempLocalMP));
		}
		// 보급 거점 최종 계산
		if (!mTeamCheckPointMpConquerValue.ContainsKey(lAllyTeamIdx))
		{
			mTeamCheckPointMpConquerValue.Add(lAllyTeamIdx, lMPWeight * lMPLocalWeight);
		}
		else
		{
			mTeamCheckPointMpConquerValue[lAllyTeamIdx] = lMPWeight * lMPLocalWeight;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleMatchInfo mBattleMatchInfo;
	private BattleGrid mBattleGrid;
	private BattleProgressInfo mBattleProgressInfo;

	private Dictionary<int, IBattleAiCommanderData> mCommanderList;

	private Dictionary<int, int> mTeamSpawnPoint; // 각 팀별 현재 소환 포인트를 계산 저장해두는 자리입니다.
	private Dictionary<int, int> mTeamIdxToUserIdx; // 팀별 유저아이디 매칭 정보

	// 얻어와야 되는 데이터들을 담는 컨테이너
	private Dictionary<int, float> mTeamMaxInfluence; // 팀별 최대 영향력 // int : team id, float : 영향력
	private Dictionary<int, Dictionary<int, float>> mCommanderInfluenceValue; // 지휘관별 영향력을 저장 // int : checkpoint id, int : commander id, float : influence(영향력)
	private Dictionary<int, Dictionary<int, float>> mTeamInfluenceValue; // 팀별 영향력을 저장 // int : team id, int : check point id, float : influence(영향력)
	private Dictionary<int, Dictionary<int, float>> mCheckPointTeamWeightValues; // int : CheckPoint Id, int : TeamIdx, float : weight value(세력값)
	private Dictionary<int, float> mTeamCheckPointVpConquerValue; // 팀별 승리 거점 가치 // int : team id, float : 승리 거점 가치
	private Dictionary<int, float> mTeamCheckPointMpConquerValue; // 팀별 보급 거점 가치 // int : team id, float : 보급 거점 가치

	private int mUpdateFrequency;
	private int mCurrentFrame;
}
