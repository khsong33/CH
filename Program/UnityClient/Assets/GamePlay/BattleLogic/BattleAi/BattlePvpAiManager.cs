﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattlePvpAiManager : BattleAiManager
{
	//-------------------------------------------------------------------------------------------------------
	// 변환 데이터
	//-------------------------------------------------------------------------------------------------------
	public int vCheckPointDist = 3500;
	public int vAiUpdateFramefrequency = 10;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattlePvpAiManager()
		: base()
	{
		mEnemyUserIdxs = new List<int>();
		mDistanceCheckPointList = new List<DistanceCheckPoint>();

		mAiUpdateCommanderList = new List<IBattleAiCommanderData>();

		mDicTargetCommander = new Dictionary<int, int>();

		Reset();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public override void Init(
		BattleUser pUser,
		BattleGrid pBattleGrid,
		BattleAiCollector pBattleAiCollector)
	{
		base.Init(pUser, pBattleGrid, pBattleAiCollector);

		for (int iUser = 0; iUser < pBattleGrid.aMatchInfo.mUserCount; iUser++)
		{
			if (mMatchInfo.mUserInfos[iUser].mTeamIdx != mUser.aTeamIdx)
			{
				mEnemyUserIdxs.Add(pBattleGrid.aMatchInfo.mUserInfos[iUser].mUserIdx);
			}
		}
		mSpawnCommanderItems = new List<CommanderItem>();
		// 소환과 관련된 데이터
		for (int iCommanderItem = 0; iCommanderItem < BattleUserInfo.cMaxCommanderItemCount; iCommanderItem++)
		{
			CommanderItem lCommanderItem = mUser.aUserInfo.mCommanderItems[iCommanderItem];
			if (lCommanderItem.mCommanderSpec == null)
			{
				continue;
			}
			mSpawnCommanderItems.Add(lCommanderItem);
			// 소환되는 최소값을 찾아줍니다.
			if (lCommanderItem.aSpawnMp < mSpawnMinMp)
			{
				mSpawnMinMp = lCommanderItem.aSpawnMp;
			}

		}

		mPvpAiCollector = (BattlePvpAiCollector)pBattleAiCollector;
		List<BattleCheckPoint> lCheckPointList = mBattleGrid.aCheckPointList;
		for (int iPoint = 0; iPoint < lCheckPointList.Count; ++iPoint)
		{
			lCheckPointList[iPoint].aOnTeamChangeDelegate += _OnCheckPointTeamChange;
			DistanceCheckPoint lDistanceData;
			Coord2 lDistanceCoord = lCheckPointList[iPoint].aCoord - mUser.aHeadquarter.aCoord;
			lDistanceData.mCheckPoint = lCheckPointList[iPoint];
			lDistanceData.mDistance = lDistanceCoord.SqrMagnitude();
			mDistanceCheckPointList.Add(lDistanceData);

			if (lCheckPointList[iPoint].aLinkHeadquarter != null && lCheckPointList[iPoint].aTeamIdx == pUser.aTeamIdx)
			{
				mCheckPointFarFromHQ = lCheckPointList[iPoint];
			}
			if (lCheckPointList[iPoint].aLinkHeadquarter != null && lCheckPointList[iPoint].aTeamIdx != pUser.aTeamIdx)
			{
				mEnemyHQCheckPointIdx = lCheckPointList[iPoint].aCheckPointIdx;
			}
		}
// 		mDistanceCheckPointList.Sort(_DistanceComparer);
		SortUtil.SortList<DistanceCheckPoint>(mDistanceCheckPointList, _DistanceComparer);
	}
	//-------------------------------------------------------------------------------------------------------
	// 검색 우선 순위 정렬
	private int _DistanceComparer(DistanceCheckPoint lValue, DistanceCheckPoint rValue)
	{
		if (lValue.mDistance < rValue.mDistance)
			return -1;
		else if (lValue.mDistance > rValue.mDistance)
			return 1;
		else if (lValue.mCheckPoint.aCheckPointGuid < rValue.mCheckPoint.aCheckPointGuid)
			return -1;
		else if (lValue.mCheckPoint.aCheckPointGuid > rValue.mCheckPoint.aCheckPointGuid)
			return 1;
		return 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 재사용을 위한 리셋
	public override void Reset()
	{
		base.Reset();

		mAiMode = eAiMode.PVP;

		mEnemyUserIdxs.Clear();
		mSpawnMinMp = Int32.MaxValue;

		if (mBattleGrid != null)
		{
			List<BattleCheckPoint> lCheckPointList = mBattleGrid.aCheckPointList;
			for (int iPoint = 0; iPoint < lCheckPointList.Count; ++iPoint)
			{
				lCheckPointList[iPoint].aOnTeamChangeDelegate -= _OnCheckPointTeamChange;
			}
			mDistanceCheckPointList.Clear();
		}

		mCurrentFrame = 0;
		mCurrentFrameUpdateCommanderIndex = 0;

		mAllyTeamIdx = -1;
		mEnemyTeamIdx = -1;
		mSelectSpawnIndex = -1;

		mAiUpdateCommanderList.Clear();
		mAiUpdateCommanderList.Capacity = 10;

		mDicTargetCommander.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 입력
	public override void AddCommander(IBattleAiCommanderData pCommander)
	{
		base.AddCommander(pCommander);
		if (IsMyCommander(pCommander))
		{
			if (mCurrentFrame > pCommander.aUpdateIndex)
			{
				int lUpdateCount = (mCurrentFrame / vAiUpdateFramefrequency) + 1;
				pCommander.aUpdateIndex = pCommander.aOriginUpdateIndex + (vAiUpdateFramefrequency * lUpdateCount);
			}
			mAiUpdateCommanderList.Add(pCommander);
// 			mAiUpdateCommanderList.Sort((lData, rData) =>
			SortUtil.SortList<IBattleAiCommanderData>(mAiUpdateCommanderList, (lData, rData) =>
			{
				if (lData.aUpdateIndex > rData.aUpdateIndex)
					return 1;
				else if (lData.aUpdateIndex < rData.aUpdateIndex)
					return -1;
				else if (lData.aId > rData.aId)
					return 1;
				else if (lData.aId < rData.aId)
					return -1;
				return 0;
			});
			_RefreshAiUpdateFrame();
			if (mAllyTeamIdx < 0)
				mAllyTeamIdx = pCommander.aTeamIdx;
		}
		else if (IsEnemyCommander(pCommander))
		{
			if (mEnemyTeamIdx < 0)
				mEnemyTeamIdx = pCommander.aTeamIdx;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 삭제
	public override void RemoveCommander(IBattleAiCommanderData pCommander)
	{
		mAiUpdateCommanderList.Remove(pCommander);
		// 다음 업데이트 커맨더의 순서를 다시 정해줍니다.
		_RefreshAiUpdateFrame();
		base.RemoveCommander(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관이 적 발견시 AI 매니저에게 다음 행동을 요청합니다.
	public override void ReqFindEnemy(int pCommanderId, Coord2 pMoveCoord, int pEnemyCommanderId)
	{
		IBattleAiCommanderData lAllyCommander;
		if (!mCommanderDictionary.TryGetValue(pCommanderId, out lAllyCommander))
		{
			Debug.LogError("Not Exist Ally Ai Commander Id - " + pCommanderId);
			return;
		}

		if (!lAllyCommander.aIsActiveAi)
			return;
		if (lAllyCommander.aIsBattle)
			return;

		IBattleAiCommanderData lEnemyCommander;
		if (!mEnemyCommanderDictionary.TryGetValue(pEnemyCommanderId, out lEnemyCommander)) // 일반 부대가 아니거나
		{
			if (!mBattleGrid.CheckCheckPointCommanderGridIdx(pEnemyCommanderId) 
				&& !mBattleGrid.CheckEventPointCommanderGridIdx(pEnemyCommanderId)) // 거점 방어군이 아니거나, 이벤트 부대가 아닐경우.
				Debug.LogError("Not Exist Enemy Ai Commander Id - " + pEnemyCommanderId);
			return;
		}

#if UNITY_EDITOR
		//if (mUser.aIsLogAi)
		//	Debug.Log(String.Format("Attack Commander Id From {0} to {1}", pCommanderId, pEnemyCommanderId));
#endif

		if (lAllyCommander.aLeaderTroop != null)
		{
			Coord2 lDirectCoord = lEnemyCommander.aCoord - lAllyCommander.aCoord;
			lAllyCommander.OnAttackMoveDirection(pMoveCoord, lDirectCoord.RoughDirectionY());
			if (mUser.aIsLogAi)
				Debug.Log("Search Enemy Target - " + pEnemyCommanderId + "/ Direction Y - " + lDirectCoord.RoughDirectionY());
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관이 전투를 시작합니다.
	public override void ReqBattleStart(int pCommanderId)
	{
		if (mCommanderDictionary.ContainsKey(pCommanderId))
		{
#if UNITY_EDITOR
			if (mUser.aIsLogAi)
				Debug.Log("Commander Battle Start - " + pCommanderId);
#endif
			mCommanderDictionary[pCommanderId].aIsBattle = true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟이 되는 지휘관의 부대가 사라졌을 때 다음 행동을 요청합니다.
	public override void ReqBattleEnd(int pCommanderId)
	{
		if (mCommanderDictionary.ContainsKey(pCommanderId))
		{
#if UNITY_EDITOR
			if (mUser.aIsLogAi)
				Debug.Log("Commander Battle End - " + pCommanderId);
#endif
			mCommanderDictionary[pCommanderId].aIsBattle = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 지휘관을 추가합니다.
	public override void AddTargetCommander(int pTargetCommanderId)
	{
		if (!mDicTargetCommander.ContainsKey(pTargetCommanderId))
		{
			mDicTargetCommander.Add(pTargetCommanderId, 0);
		}
		mDicTargetCommander[pTargetCommanderId]++;

		//if(!mDicBattleCommander.ContainsKey())

		if (mUser.aIsLogAi)
			Debug.Log("Add Target Commander Id [" + pTargetCommanderId + "]");
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 지휘관을 제거합니다.
	public override void RemoveTargetCommander(int pTargetCommanderId)
	{
		if (!mDicTargetCommander.ContainsKey(pTargetCommanderId))
		{
			Debug.LogError("Not Exist Target Commander Grid Idx - " + pTargetCommanderId);
			return;
		}
		mDicTargetCommander[pTargetCommanderId]--;
		if (mDicTargetCommander[pTargetCommanderId] <= 0)
		{
			mDicTargetCommander.Remove(pTargetCommanderId);
		}
		if (mUser.aIsLogAi)
			Debug.Log("Remove Target Commander Id [" + pTargetCommanderId + "]");
	}
	////-------------------------------------------------------------------------------------------------------
	//// 지휘관 부대가 전멸했을 경우 호출
	//public override void ReqDestoryCommander(int pCommanderId)
	//{	
	//	mCommanderDictionary[pCommanderId].aIsActiveOrder = true;
	//	_SelectNextSpawnCommander();
	//}

	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public override void UpdateFrame()
	{
		base.UpdateFrame();

		if (mSelectSpawnIndex < 0)
			_SelectNextSpawnCommander();

		if (mUser.aControlType == BattleUser.ControlType.Auto)
			_GoForcesSpawn();

		if (mAiUpdateCommanderList.Count > 0)
		{
			if (mCurrentFrameUpdateCommanderIndex < 0 && mCurrentFrameUpdateCommanderIndex >= mAiUpdateCommanderList.Count)
			{
				_RefreshAiUpdateFrame();
			}
			IBattleAiCommanderData lCommander = mAiUpdateCommanderList[mCurrentFrameUpdateCommanderIndex];
			if (mCurrentFrame == lCommander.aUpdateIndex)
			{
				//Debug.Log("Update Ai frame - " + mAiUpdateCommanderList[mCurrentFrameUpdateCommanderIndex].aId);
				if (mAiUpdateCommanderList[mCurrentFrameUpdateCommanderIndex].aIsActiveAi)
				{
					_UpdateAiCommanderFrame(mAiUpdateCommanderList[mCurrentFrameUpdateCommanderIndex]);
					if (mUser.aIsLogAi)
						Debug.Log("Current Update Commander Grid Idx - " + mCurrentFrameUpdateCommanderIndex);
				}
				mCurrentFrameUpdateCommanderIndex++;
				if (mCurrentFrameUpdateCommanderIndex >= mAiUpdateCommanderList.Count)
					mCurrentFrameUpdateCommanderIndex = 0;

			}
			if (mCurrentFrame > lCommander.aUpdateIndex)
			{
				int lUpdateCount = (mCurrentFrame / vAiUpdateFramefrequency) + 1;
				lCommander.aUpdateIndex = lCommander.aOriginUpdateIndex + (vAiUpdateFramefrequency * lUpdateCount);
			}
			mCurrentFrame++;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 각 커맨더의 행동을 선택합니다.
	private void _UpdateAiCommanderFrame(IBattleAiCommanderData pCommander)
	{
		int lDebugSelectNumber = -1;
		//String lDebugMessage = String.Empty;
		// 갱신 프레임은 수정합니다.
		pCommander.aUpdateIndex += vAiUpdateFramefrequency;

		if (!pCommander.aIsActiveAi)
		{
#if UNITY_EDITOR
			if (mUser.aIsLogAi)
				Debug.Log("Stop Ai Update what aIsActiveAi is false - " + pCommander.aId);
#endif
			return;
		}
		if (pCommander.aIsBattle)
		{
#if UNITY_EDITOR
			if (mUser.aIsLogAi)
				Debug.Log("Stop Ai Update what aIsBattle is false - " + pCommander.aId);
#endif
			return;
		}
		if (!pCommander.aIsAlive)
		{
#if UNITY_EDITOR
			if (mUser.aIsLogAi)
				Debug.Log("Stop Ai Update what aIsAlive is false - " + pCommander.aId);
#endif
			return;
		}

		int lCheckPointCount = mBattleGrid.aCheckPointList.Count;
		float lMaxEnemyInfluence = 0.0f;
		int lSelectType = 0; // 0 : checkpoint id 선택, 1 : enemy commander id 선택, 2 : my commander id 선택
		int lSelectTargetCheckPointIdx = -1;
		int lSelectTargetCommanderId = 0;
		bool lIsCommanderOrder = true; // 지휘관 개인 선택으로 변경됩니다.
		bool lIsConquerVP = mPvpAiCollector.IsConquerVitoryPoint(pCommander.aTeamIdx); // VP를 점령할지 MP를 점령할지 결정하는 요소 // 목적지가 없을 경우 처리

		// 선택이 되지 않았을 경우 사용하는 변수. 순수 영향력 비교만을 진행
		float lMaxEnemyInfluencePure = 0;
		int lMaxEnemyInflucePurePointIndex = -1;
		int lNextConquerCheckPointIdx = -1;
		int lMinDistance = int.MaxValue;
		RowCol lFarFromHQConquerRowCol = mBattleGrid.GetTileIndex(mCheckPointFarFromHQ.aCoord);
		for (int iCheckPoint = 0; iCheckPoint < lCheckPointCount; iCheckPoint++)
		{
			BattleCheckPoint lCheckPoint = mBattleGrid.aCheckPointList[iCheckPoint];
			// 적군의 영향력을 얻어와 임계치와 비교합니다.
			float lCheckPointEnemyInfluence = mPvpAiCollector.GetInfluence(mEnemyTeamIdx, lCheckPoint.aCheckPointIdx);
			// 차후 세력값으로 선택된 값이 없을 때 사용하는 임시 저장 변수
			if (lMaxEnemyInfluencePure < lCheckPointEnemyInfluence)
			{
				lMaxEnemyInfluencePure = lCheckPointEnemyInfluence;
				lMaxEnemyInflucePurePointIndex = iCheckPoint;
			}
			// 임계치 이상의 값을 적이 갖고 있는 포인트가 있을 경우
			if (lCheckPoint.aTeamIdx == pCommander.aTeamIdx && lCheckPointEnemyInfluence >= BattleConfig.get.mInfluenceActiveValue)
			{
				lIsCommanderOrder = false;
				// 세력값을 비교합니다.
				float lCheckPointAllyTeamWeightValue = mPvpAiCollector.GetCheckPointWeightValue(lCheckPoint.aCheckPointIdx, mAllyTeamIdx);
				float lCheckPointEnemyTeamWeightValue = mPvpAiCollector.GetCheckPointWeightValue(lCheckPoint.aCheckPointIdx, mEnemyTeamIdx);
				if (lCheckPointAllyTeamWeightValue > lCheckPointEnemyTeamWeightValue)
				{
					// 적군 영향력이 타겟보다 클 경우 또는 선택된 타겟이 없을 때 갱신합니다.
					if (lMaxEnemyInfluence < lCheckPointEnemyInfluence || lSelectTargetCheckPointIdx < 0)
					{
						lMaxEnemyInfluence = lCheckPointEnemyInfluence;
						lSelectTargetCheckPointIdx = lCheckPoint.aCheckPointIdx;
						lDebugSelectNumber = 1;
					}
				}
			}
			// 가장 먼 점령지에서 가장 가까운 점령지를 선택합니다. (차후 선택을 위한 선계산)
			if (pCommander.aTeamIdx == mAllyTeamIdx)
			{
				if (pCommander.aIsCaptureCheckPoint)
				{
					int lDistCheckPointFromFarFromCommander = BattleCoordDistTable.get.GetDistance(mBattleGrid.GetTileIndex(pCommander.aCoord), mBattleGrid.GetTileIndex(lCheckPoint.aCoord));
					// 승리 포인트 거점 점령
					if (lIsConquerVP
						&& lCheckPoint.aStarId > 0
						&& lDistCheckPointFromFarFromCommander <= lMinDistance
						&& lCheckPoint.aStarId != -1
						&& lCheckPoint.aTeamIdx != mAllyTeamIdx)
					{
						lMinDistance = lDistCheckPointFromFarFromCommander;
						lNextConquerCheckPointIdx = lCheckPoint.aCheckPointIdx;
					}
					else if (!lIsConquerVP
						&& lCheckPoint.aStarId <= 0
						&& lDistCheckPointFromFarFromCommander <= lMinDistance
						&& lCheckPoint.aStarId != -1
						&& lCheckPoint.aTeamIdx != mAllyTeamIdx)
					{
						lMinDistance = lDistCheckPointFromFarFromCommander;
						lNextConquerCheckPointIdx = lCheckPoint.aCheckPointIdx;
					}
				}
				else
				{
					int lDistCheckPointFromFarFromHQ = BattleCoordDistTable.get.GetDistance(lFarFromHQConquerRowCol, mBattleGrid.GetTileIndex(lCheckPoint.aCoord));
					// 승리 포인트 거점 점령
					if (lIsConquerVP
						&& lCheckPoint.aStarId > 0
						&& lDistCheckPointFromFarFromHQ <= lMinDistance
						&& lCheckPoint.aStarId != -1
						&& lCheckPoint.aTeamIdx != mAllyTeamIdx)
					{
						lMinDistance = lDistCheckPointFromFarFromHQ;
						lNextConquerCheckPointIdx = lCheckPoint.aCheckPointIdx;
					}
					else if (!lIsConquerVP
						&& lCheckPoint.aStarId <= 0
						&& lDistCheckPointFromFarFromHQ <= lMinDistance
						&& lCheckPoint.aStarId != -1
						&& lCheckPoint.aTeamIdx != mAllyTeamIdx)
					{
						lMinDistance = lDistCheckPointFromFarFromHQ;
						lNextConquerCheckPointIdx = lCheckPoint.aCheckPointIdx;
					}
				}
			}
		}
		if (lSelectTargetCheckPointIdx >= 0) // 목표 지점이 있는경우
		{
			if (pCommander.aIsCaptureCheckPoint) // 소총 부대인가
			{
				float lAllyTeamInfluence = mPvpAiCollector.GetInfluence(mAllyTeamIdx, lSelectTargetCheckPointIdx);
				float lEnemyTeamInfluence = mPvpAiCollector.GetInfluence(mEnemyTeamIdx, lSelectTargetCheckPointIdx);
				float lCommanderInfluence = mPvpAiCollector.GetCommanderInfluence(pCommander.aId, lSelectTargetCheckPointIdx);
				float lTeamMaxInfluence = mPvpAiCollector.GetTeamMaxInfluence(mAllyTeamIdx);
				if (lAllyTeamInfluence - lCommanderInfluence > lEnemyTeamInfluence && lTeamMaxInfluence < lCommanderInfluence)
				{
					// 내가 제외되어도 아군 세력이 높은경우 거점 가치 계산을 통해서 점령지를 선택.
					lSelectTargetCheckPointIdx = -1;
					lIsCommanderOrder = true; // 지휘관 개인 명령으로 전환
				}
			}
		}
		else
		{
			lIsCommanderOrder = true;
		}
		if (lSelectTargetCheckPointIdx < 0 && !lIsCommanderOrder)// 위 선택 과정 이후 선택된 지점이 없고, 적군 영향력이 임계치 이상이 한곳이라도 있는 경우
		{
			int lMaxDist = 0;
			BattleCheckPoint lMaxInfluenceCheckPoint = mBattleGrid.aCheckPointList[lMaxEnemyInflucePurePointIndex];
			RowCol lMaxInfluenceCheckPointRowCol = mBattleGrid.GetTileIndex(lMaxInfluenceCheckPoint.aCoord);
			for (int iCheckPoint = 0; iCheckPoint < lCheckPointCount; iCheckPoint++)
			{
				BattleCheckPoint lCheckPoint = mBattleGrid.aCheckPointList[iCheckPoint];
				// 아군 점령지만 확인
				if (lCheckPoint.aTeamIdx != pCommander.aTeamIdx)
					continue;
				if (lMaxInfluenceCheckPoint.aCheckPointIdx == lCheckPoint.aCheckPointIdx)
					continue;

				RowCol lCheckPointRowCol = mBattleGrid.GetTileIndex(lCheckPoint.aCoord);
				int lDistance = BattleCoordDistTable.get.GetDistance(lMaxInfluenceCheckPointRowCol, lCheckPointRowCol);
				if (lDistance > lMaxDist)
				{
					lDistance = lMaxDist;
					lSelectType = 0;
					lSelectTargetCheckPointIdx = lCheckPoint.aCheckPointIdx;
					lDebugSelectNumber = 5;
				}
			}
		}
		// 적군 영향력이 임계치 이상인 곳이 한 곳도 없는 경우 지휘관 개인 명령을 수행합니다.
		if (lIsCommanderOrder)
		{
			bool lIsAttackEnemyCommander = false;

			int lNearEnemyCommanderIndex = -1; // 전투중이 아닌 부대 포함 가장 가까운 부대 인덱스
			int lMinEnemyDist = int.MaxValue;
			int lNearTargetEnemyCommanderIndex = -1;
			int lMinTargetEnemyDist = int.MaxValue;

			// 전투중인 가장 가까운 적과, 전투중이지 않은 적을 포함한 가장 가까운 적을 우선 찾아냅니다.
			RowCol lAllyCommanderRowCol = mBattleGrid.GetTileIndex(pCommander.aCoord);
			Dictionary<int, IBattleAiCommanderData>.Enumerator iter = mEnemyCommanderDictionary.GetEnumerator();
			while (iter.MoveNext())
			{
				IBattleAiCommanderData lEnemyCommander = iter.Current.Value;
				RowCol lEnemyCommanderRowCol = mBattleGrid.GetTileIndex(lEnemyCommander.aCoord);
				int lDist = BattleCoordDistTable.get.GetDistance(lAllyCommanderRowCol, lEnemyCommanderRowCol);
				if (mDicTargetCommander.ContainsKey(lEnemyCommander.aId))
				{
					if (lDist < lMinTargetEnemyDist)
					{
						lMinTargetEnemyDist = lDist;
						lNearTargetEnemyCommanderIndex = lEnemyCommander.aId;
						lIsAttackEnemyCommander = true;
					}
				}
				if (lDist < lMinEnemyDist)
				{
					lMinEnemyDist = lDist;
					lNearEnemyCommanderIndex = lEnemyCommander.aId;
				}
			}

			int lLowHpCommanderIndex = -1; // 전투중이 아닌 부대 포함 가장 가까운 부대 인덱스
			int lLowHp = int.MaxValue;
			int lLowHpBattleCommanderIndex = -1;
			int lLowHpBattle = int.MaxValue;
			bool lIsBattleCommander = false; // 전투중인 보병 아군이 있는가.
			// 아군 지휘관에 대한 데이터를 수집합니다. (Class가 Medic, Repair일 경우만 수집)
			if (pCommander.aCommanderClass == CommanderClass.Medic || pCommander.aCommanderClass == CommanderClass.Repair)
			{
				Dictionary<int, IBattleAiCommanderData>.Enumerator iterCommander = mCommanderDictionary.GetEnumerator();
				while (iterCommander.MoveNext())
				{
					IBattleAiCommanderData lCommander = iterCommander.Current.Value;
					if ((pCommander.aCommanderClass == CommanderClass.Medic && lCommander.aCommanderType == CommanderType.Infantry)
						|| (pCommander.aCommanderClass == CommanderClass.Repair && lCommander.aCommanderType != CommanderType.Infantry))
					{
						int lHp = lCommander.aHpPercent;
						if (lCommander.aIsBattle)
						{
							if (lHp < lLowHpBattle)
							{
								lLowHpBattle = lHp;
								lLowHpBattleCommanderIndex = lCommander.aId;
								lIsBattleCommander = true;
							}
						}
						if (lHp < lLowHp)
						{
							lLowHp = lHp;
							lLowHpCommanderIndex = lCommander.aId;
						}
					}
				}
			}

			// 이동할 지역을 선택합니다.
			if (pCommander.aIsCaptureCheckPoint) // 점령 가능한 부대인가
			{
				lSelectTargetCheckPointIdx = lNextConquerCheckPointIdx;
				lDebugSelectNumber = 2;
			}
			else if (pCommander.aCommanderClass == CommanderClass.Medic) // 치료 부대인가.
			{
				lSelectType = 2; // 아군 위치로 이동
				lDebugSelectNumber = 6;
				if (lIsBattleCommander)
				{
					lSelectTargetCommanderId = lLowHpBattleCommanderIndex;
				}
				else
				{
					lSelectTargetCommanderId = lLowHpCommanderIndex;
				}
			}
			else if (pCommander.aCommanderClass == CommanderClass.Repair) // 수리 부대인가
			{
				lSelectType = 2;
				lDebugSelectNumber = 7;
				if (lIsBattleCommander)
				{
					lSelectTargetCommanderId = lLowHpBattleCommanderIndex;
				}
				else
				{
					lSelectTargetCommanderId = lLowHpCommanderIndex;
				}
			}
			else if (lIsAttackEnemyCommander) // 전투중인 적이 있는가
			{
				lSelectType = 1;
				lSelectTargetCommanderId = lNearTargetEnemyCommanderIndex;
				lDebugSelectNumber = 3;
			}
			else if (pCommander.aCommanderClass == CommanderClass.TankGun) // 전차 부대인가
			{
				lSelectTargetCheckPointIdx = lNextConquerCheckPointIdx;
				lDebugSelectNumber = 4;
			}
			else
			{
				lSelectType = 1;
				lSelectTargetCommanderId = lNearEnemyCommanderIndex;
				lDebugSelectNumber = 5;
			}
		}

		// 완료 후 병력을 전진시킵니다.
		if (lSelectType == 0)
		{
			if (lSelectTargetCheckPointIdx < 0)
				lSelectTargetCheckPointIdx = mEnemyHQCheckPointIdx;

			if (mUser.aIsLogAi)
				Debug.Log(String.Format("Commander Id [{0}] - Select Check Point Id [{1}]/Debug Num[{2}]", pCommander.aId, lSelectTargetCheckPointIdx, lDebugSelectNumber));
			pCommander.OnMoveCheckPoint(lSelectTargetCheckPointIdx);
		}
		else if (lSelectType == 1) // 적군의 위치로 지정합니다.
		{
			if (lSelectTargetCommanderId >= 0)
			{
				if (mUser.aIsLogAi)
					Debug.Log(String.Format("Commander Id [{0}] - Select Target Commander Id [{1}]/Debug Num[{2}]", pCommander.aId, lSelectTargetCommanderId, lDebugSelectNumber));
				IBattleAiCommanderData lTargetCommander = mEnemyCommanderDictionary[lSelectTargetCommanderId];
				pCommander.OnAttackMove(lTargetCommander.aCoord);
			}
		}
		else if (lSelectType == 2) // medis, repair class의 경우 아군의 위치로 지정합니다.
		{
			if (lSelectTargetCommanderId >= 0)
			{
				if (mUser.aIsLogAi)
					Debug.Log(String.Format("Commander Id [{0}] - Select Target Commander Id [{1}]/Debug Num[{2}]", pCommander.aId, lSelectTargetCommanderId, lDebugSelectNumber));

				IBattleAiCommanderData lTargetCommander = mCommanderDictionary[lSelectTargetCommanderId];
				Coord2 lTargetCoord = new Coord2();
				int lTargetDirectionY = 0;
				int lDistance = 0;
				
				if (pCommander.aCommanderClass == CommanderClass.Medic)
					lDistance = BattleConfig.get.mMedicAIDistance;
				else if (pCommander.aCommanderClass == CommanderClass.Repair)
					lDistance = BattleConfig.get.mRepairAIDistance;
				
				_GetTargetCoordAndDirectionY(mUser.aHeadquarter.aLinkCheckPoint.aCoord, lTargetCommander.aCoord, lDistance, out lTargetCoord, out lTargetDirectionY);
				// pCommander.OnAttackMove(lTargetCommander.aCoord);
				pCommander.OnAttackMoveDirection(lTargetCoord, lTargetDirectionY);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 다음 소환 지휘관 확인
	private void _SelectNextSpawnCommander()
	{
		int lMinOrder = int.MaxValue;
		for (int iCommanderItem = 0; iCommanderItem < mSpawnCommanderItems.Count; iCommanderItem++)
		{
			CommanderItem lCommanderItem = mSpawnCommanderItems[iCommanderItem];
			if (lCommanderItem == null)
				continue;
			if (!lCommanderItem.IsSpawnAvailable())
				continue;

			int lTempMinOrder = Mathf.Min(mSpawnCommanderItems[iCommanderItem].mCommanderSpec.mSummonOrder, lMinOrder);
			if (lMinOrder != lTempMinOrder)
			{
				lMinOrder = lTempMinOrder;
				mSelectSpawnIndex = iCommanderItem;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// Mp가 되는대로 소환
	private void _GoForcesSpawn()
	{
		if (mSpawnMinMp > mProgressInfo.aUserMps[mUser.aUserIdx])
			return;
		if (!mSpawnCommanderItems[mSelectSpawnIndex].IsSpawnAvailable())
		{
			_SelectNextSpawnCommander();
		}
		if (mSelectSpawnIndex >= 0 && mUser.CheckSpawnable(mSpawnCommanderItems[mSelectSpawnIndex]))
		{
			// Spawn;
			BattleCommander lBattleCommander = mBattleGrid.SpawnCommander(mUser,
																		  mSpawnCommanderItems[mSelectSpawnIndex],
																		  mUser.aHeadquarter.aCoord,
																		  mUser.aHeadquarter.aDirectionY,
																		  mUser);

			if (mUser.aIsLogAi)
			{
				Debug.Log("Spawn Commander - " + lBattleCommander.aCommanderGridIdx);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	//[콜백]체크 포인트의 점령자가 바뀌었을 때 호출됩니다.
	private void _OnCheckPointTeamChange(BattleCheckPoint pCheckPoint)
	{
		// ver 2.0에 사용 되는 처리
		// 아군이 점령한 최대 사거리에 있는
		for (int iDistCheckPoint = mDistanceCheckPointList.Count - 1; iDistCheckPoint >= 0; iDistCheckPoint--)
		{
			BattleCheckPoint lCheckPoint = mDistanceCheckPointList[iDistCheckPoint].mCheckPoint;
			if (lCheckPoint.aTeamIdx != mAllyTeamIdx)
				continue;

			mCheckPointFarFromHQ = mDistanceCheckPointList[iDistCheckPoint].mCheckPoint;
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 맵의 상태를 확인하고 어디로 보낼지 결정합니다.(실제 AI 프로세싱)
	private int _UpdateCheckPointAi()
	{
		if (mUser.aControlType == BattleUser.ControlType.Manual)
			return -1;

		int lResultCheckPointIdx = -1;
		int lCheckPointCount = mBattleGrid.aCheckPointList.Count;
		int lAllyVictoryPoint = 0;
		int lAllySupplyPoint = 0;
		int lEnemyVictoryPoint = 0;
		int lEmenySupplyPoint = 0;
		for (int iCheckPoint = 0; iCheckPoint < lCheckPointCount; iCheckPoint++)
		{
			BattleCheckPoint lCheckPoint = mBattleGrid.aCheckPointList[iCheckPoint];

			if (lCheckPoint.aTeamIdx <= 0) // 중립 지역은 체크하지 않음
				continue;
			// 아군과 적군의 승리 거점 확인
			if (lCheckPoint.aStarId > 0)
			{
				if (lCheckPoint.aTeamIdx == mUser.aTeamIdx)
					lAllyVictoryPoint++;
				else
					lEnemyVictoryPoint++;
			}
			else // 아군과 적군의 보급 거점 확인
			{
				if (lCheckPoint.aTeamIdx == mUser.aTeamIdx)
					lAllySupplyPoint++;
				else
					lEmenySupplyPoint++;
			}
		}

		int lVictoryPointComparer = lAllyVictoryPoint - lEnemyVictoryPoint; // 승리 거점 차이
		int lVictoryValue = lAllyVictoryPoint + lVictoryPointComparer;
		//승리 거점 + 점령 수 차이  >  1
		if (lVictoryValue <= 1)
		{
			for (int iPoint = 0; iPoint < mDistanceCheckPointList.Count; iPoint++)
			{
				// 승리거점이며 유저가 점령당하지 않은 거점
				if (mDistanceCheckPointList[iPoint].mCheckPoint.aStarId > 0
					&& mDistanceCheckPointList[iPoint].mCheckPoint.aTeamIdx != mUser.aTeamIdx)
				{
					lResultCheckPointIdx = mDistanceCheckPointList[iPoint].mCheckPoint.aCheckPointIdx;
#if UNITY_EDITOR
					if (mUser.aIsLogAi)
						Debug.Log("Find Check Point No.1 - " + lResultCheckPointIdx);
#endif
					break;
				}
			}
		}
		else
		{
			// 승리 거점 체크해서 위험도 확인
			int lDangerPoint = 0;
			// 최종 결정까지 갈 경우 사용해야할 변수
			int lMinMyCommanderCount = int.MaxValue;
			int lMinCommanderCheckPointIdx = 0;
			for (int iCheckPoint = 0; iCheckPoint < mDistanceCheckPointList.Count; iCheckPoint++)
			{
				BattleCheckPoint lCheckPoint = mDistanceCheckPointList[iCheckPoint].mCheckPoint;
				if (lCheckPoint.aStarId <= 0) // 승리 거점만 체크
					continue;

				int lMyCommanderCount = _GetNearbyCheckPointCommanderCount(mCommanderDictionary, lCheckPoint);
				if (lMyCommanderCount < lMinMyCommanderCount) // 지휘관이 가장 적은 승리 거점을 저장해둡니다.
				{
					lMinMyCommanderCount = lMyCommanderCount;
					lMinCommanderCheckPointIdx = lCheckPoint.aCheckPointIdx;
				}
				int lEnemyCommanderCount = _GetNearbyCheckPointCommanderCount(mEnemyCommanderDictionary, lCheckPoint);

				if (lMyCommanderCount - lEnemyCommanderCount < 0 && lCheckPoint.aTeamIdx != mUser.aTeamIdx)
				{
					lDangerPoint++;
					lResultCheckPointIdx = lCheckPoint.aCheckPointIdx;
#if UNITY_EDITOR
					if (mUser.aIsLogAi)
						Debug.Log("Find Check Point No.2 - " + lResultCheckPointIdx);
#endif
					break;
				}
			}
			// 위험도가 0일 경우
			if (lDangerPoint <= 0)
			{
				// 점령한 승리거점 * 3 - 점령한 보급 거점 > 0
				if ((lAllyVictoryPoint * 3) - lAllySupplyPoint > 0)
				{
					for (int iCheckPoint = 0; iCheckPoint < mDistanceCheckPointList.Count; iCheckPoint++)
					{
						BattleCheckPoint lCheckPoint = mDistanceCheckPointList[iCheckPoint].mCheckPoint;
						if (lCheckPoint.aStarId > 0) // 보급 거점만 확인
							continue;

						if (lCheckPoint.aTeamIdx != mUser.aTeamIdx) // 적군 지역만 확인
						{
							lResultCheckPointIdx = lCheckPoint.aCheckPointIdx;
#if UNITY_EDITOR
							if (mUser.aIsLogAi)
								Debug.Log("Find Check Point No.3 - " + lResultCheckPointIdx);
#endif
							break;
						}
					}
				}
				else
				{
					if (lAllyVictoryPoint < 2)
					{
						for (int iCheckPoint = 0; iCheckPoint < mDistanceCheckPointList.Count; iCheckPoint++)
						{
							BattleCheckPoint lCheckPoint = mDistanceCheckPointList[iCheckPoint].mCheckPoint;
							if (lCheckPoint.aStarId <= 0) // 승리거점만 확인
								continue;
							if (lCheckPoint.aTeamIdx > 0) // 중립 지역만 확인
								continue;

							lResultCheckPointIdx = lCheckPoint.aCheckPointIdx;
#if UNITY_EDITOR
							if (mUser.aIsLogAi)
								Debug.Log("Find Check Point No.4 - " + lResultCheckPointIdx);
#endif
							break;
						}
					}
					else
					{
						lResultCheckPointIdx = lMinCommanderCheckPointIdx;
#if UNITY_EDITOR
						if (mUser.aIsLogAi)
							Debug.Log("Find Check Point No.5 - " + lResultCheckPointIdx);
#endif
					}
				}
			}
		}

		return lResultCheckPointIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 위치에서 특정 거리안에 있는 지휘관의 수를 얻어옵니다.
	private int _GetNearbyCheckPointCommanderCount(Dictionary<int, IBattleAiCommanderData> pCommander, BattleCheckPoint pCheckPoint)
	{
		Dictionary<int, IBattleAiCommanderData>.Enumerator iterCommander = pCommander.GetEnumerator();
		int lCommanderCount = 0;
		while (iterCommander.MoveNext())
		{
			IBattleAiCommanderData lCommander = iterCommander.Current.Value;
			int lCommanderDistance = (pCheckPoint.aCoord - lCommander.aCoord).SqrMagnitude();
			if (lCommanderDistance <= vCheckPointDist * vCheckPointDist)
			{
				lCommanderCount++;
			}
		}
		return lCommanderCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 위치에서 특정 거리안에 있는 지휘관의 수를 얻어옵니다.
	private void _RefreshAiUpdateFrame()
	{
		// 다음 업데이트 커맨더의 순서를 다시 정해줍니다.
		for (int iCommander = 0; iCommander < mAiUpdateCommanderList.Count; iCommander++)
		{
			mCurrentFrameUpdateCommanderIndex = iCommander;
			if (mAiUpdateCommanderList[iCommander].aUpdateIndex > mCurrentFrame)
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Medic 및 Repair Commander class에 대해 위치값을 찾아줍니다.
	private void _GetTargetCoordAndDirectionY(Coord2 pStartCoord, Coord2 pTargetCoord, int pDistance, out Coord2 outResultTargetCoord, out int outResultDirectionY)
	{
		Coord2 lDirectCoord = pTargetCoord - pStartCoord;
		outResultDirectionY = lDirectCoord.RoughDirectionY();

		Coord2 lDirectionVectorFromTarget = Coord2.RoughDirectionVector(outResultDirectionY + 180, pDistance);
		outResultTargetCoord = pTargetCoord + lDirectionVectorFromTarget;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	// 팀 판단 판단 변수
	private List<int> mEnemyUserIdxs;
	// Spawn과 관련된 판단 변수
	private int mSpawnMinMp;
	private int mSelectSpawnIndex;
	// 본진(HQ)에서 각 CheckPoint의 거리를 측정 저장
	private List<DistanceCheckPoint> mDistanceCheckPointList;
	// 적군 아군 팀정보
	private int mAllyTeamIdx;
	private int mEnemyTeamIdx;
	private int mEnemyHQCheckPointIdx;

	// 업데이트 프레임
	private int mCurrentFrame;
	private int mCurrentFrameUpdateCommanderIndex;
	private List<IBattleAiCommanderData> mAiUpdateCommanderList;

	// PVP 모드에서만 사용되는 AI Collector
	private BattlePvpAiCollector mPvpAiCollector;

	private BattleCheckPoint mCheckPointFarFromHQ;

	private List<CommanderItem> mSpawnCommanderItems;

	private Dictionary<int, int> mDicTargetCommander; // 타겟팅 된 적군 커맨더
}
public struct DistanceCheckPoint
{
	public BattleCheckPoint mCheckPoint;
	public int mDistance;
}
