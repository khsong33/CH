using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleEndInfo
{
	public BattleProtocol.BattleEndCode mBattleEndCode;
	public bool mIsVictory;
	public bool mIsDefeat;
	public bool mIsDraw;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleEndInfo(BattleProtocol.BattleEndCode pBattleEndCode)
	{
		mBattleEndCode = pBattleEndCode;

		switch (pBattleEndCode)
		{
		case BattleProtocol.BattleEndCode.PlayWin:
		case BattleProtocol.BattleEndCode.TestWin:
			{
				mIsVictory = true;
				mIsDefeat = false;
				mIsDraw = false;
			}
			break;
		case BattleProtocol.BattleEndCode.TestLose:
		case BattleProtocol.BattleEndCode.PlayLose:
			{
				mIsVictory = false;
				mIsDefeat = true;
				mIsDraw = false;
			}
			break;
		case BattleProtocol.BattleEndCode.PlayDraw:
			{
				mIsVictory = false;
				mIsDefeat = false;
				mIsDraw = true;
			}
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
