using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;
using CombatJson;

public class BattleMatchInfo
{
	public enum RuleType
	{
		Conquest,
	}

	public RuleType mRuleType;
	public int mStageNo;
	public UInt32[] mRandomStates;
	public int mRandomSeed;
	public int mPlayTime;
	public int mCheckPointStarCount;

	public int mUserCount;
	public BattleUserInfo[] mUserInfos;

	public const int cMaxCheckPointStarCount = 3;
	public const int cMaxUserCount = 8;
	public const int cMaxTeamCount = 1 + cMaxUserCount; // 중립을 포함한 유저 개개의 팀일 경우가 최대 팀 수임

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 팀 수
	public int aTeamCount
	{
		get { return mTeamCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 스펙
	public StageSpec aStageSpec
	{
		get { return mStageSpec; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleMatchInfo()
	{
		mUserInfos = new BattleUserInfo[cMaxUserCount];
		for (int iInfo = 0; iInfo < cMaxUserCount; ++iInfo)
			mUserInfos[iInfo] = new BattleUserInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 복사본으로 초기화합니다.
	public void InitFromClone(BattleMatchInfo pCloneMatchInfo)
	{
	#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
		mRuleType = pCloneMatchInfo.mRuleType;
		mStageNo = pCloneMatchInfo.mStageNo;

		if (pCloneMatchInfo.mRandomStates != null)
		{
			mRandomStates = new UInt32[16];
			for (int iState = 0; iState < pCloneMatchInfo.mRandomStates.Length; ++iState)
				mRandomStates[iState] = pCloneMatchInfo.mRandomStates[iState];
		}
		mRandomSeed = pCloneMatchInfo.mRandomSeed;

		mPlayTime = pCloneMatchInfo.mPlayTime;
		mCheckPointStarCount = pCloneMatchInfo.mCheckPointStarCount;

		mUserCount = pCloneMatchInfo.mUserCount;
		for (int iUser = 0; iUser < mUserCount; iUser++)
		{
			BattleUserInfo lUserInfo = new BattleUserInfo();
			lUserInfo.mUserIdx = iUser;

			BattleUserInfo lCloneUserInfo = pCloneMatchInfo.mUserInfos[iUser];

			lUserInfo.mUserName = lCloneUserInfo.mUserName;
			lUserInfo.mClientGuid = lCloneUserInfo.mClientGuid;
			lUserInfo.mIsAi = lCloneUserInfo.mIsAi;

			lUserInfo.mHeadquarterGuid = lCloneUserInfo.mHeadquarterGuid;
			lUserInfo.mTeamIdx = lCloneUserInfo.mTeamIdx;

			lUserInfo.mMaxMp = lCloneUserInfo.mMaxMp;
			lUserInfo.mStartMp = lCloneUserInfo.mStartMp;

			//lUserInfo.mNationExps = lCloneUserInfo.mNationExps;
			//lUserInfo.mNationLevels = lCloneUserInfo.mNationLevels;
			lUserInfo.mWin = lCloneUserInfo.mWin;
			lUserInfo.mLose = lCloneUserInfo.mLose;
			lUserInfo.mWinStreak = lCloneUserInfo.mWinStreak;
			lUserInfo.mLoseStreak = lCloneUserInfo.mLoseStreak;

			lUserInfo.mBattleNation = lCloneUserInfo.mBattleNation;

			for (int iSlot = 0; iSlot < lCloneUserInfo.mCommanderItems.Length; iSlot++)
				if (lCloneUserInfo.mCommanderItems[iSlot].aIsValid)
					lUserInfo.AddCommanderItem(lCloneUserInfo.mCommanderItems[iSlot]);

			mUserInfos[iUser] = lUserInfo;
		}
	#endif

		// 플레이 관련 초기화를 합니다.
		_InitPlay();
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 정보로 초기화합니다.
	public void InitFromStage(int pStageNo)
	{
#if UNITY_EDITOR || UNITY_ANDROID || UNITY_IOS
		mRuleType = BattleMatchInfo.RuleType.Conquest;
		mStageNo = pStageNo;

// 		mRandomStates = new UInt32[16];
// 		mRandomStates[0] = 3732;
// 		mRandomStates[1] = 5403;
// 		mRandomStates[2] = 1749;
// 		mRandomStates[3] = 9599;
// 		mRandomStates[4] = 4558;
// 		mRandomStates[5] = 6711;
// 		mRandomStates[6] = 1366;
// 		mRandomStates[7] = 2879;
// 		mRandomStates[8] = 6821;
// 		mRandomStates[9] = 520;
// 		mRandomStates[10] = 8160;
// 		mRandomStates[11] = 671;
// 		mRandomStates[12] = 766;
// 		mRandomStates[13] = 5053;
// 		mRandomStates[14] = 4006;
// 		mRandomStates[15] = 6443;
// 		mRandomSeed = 4;

		mRandomStates = null;
		mRandomSeed = BattlePlay.main.vTestRandomSeed; // 로컬 플레이의 대전은 랜덤 시드를 인스펙터에서 받아옴

		mPlayTime = BattleConfig.get.mModeTime;
		mCheckPointStarCount = 3;

		StageSpec lStageSpec = StageTable.get.FindStageSpec(mStageNo);
		bool lIsTutorial = (TutorialTable.get.GetTutorialSpecByStageNo(mStageNo) != null);

		mUserCount = 2;
		for (int iUser = 0; iUser < mUserCount; iUser++)
		{
			BattleUserInfo lUserInfo = new BattleUserInfo();
			lUserInfo.mUserIdx = iUser;

			String lUserName = lStageSpec.GetCustomUserName(lUserInfo.mUserIdx);
			if (String.IsNullOrEmpty(lUserName))
				lUserName = CustomBattleCommanderItemTable.get.vDefaultUserName;
			int lUseDeckIdx = lStageSpec.GetCustomDeckId(lUserInfo.mUserIdx);
			//lUserInfo.mUserName = (iUser == BattlePlay.main.vTestPlayerUserIdx) ? BattlePlay.main.vTestPlayerId : BattlePlay.main.vTestEnemyId;
			lUserInfo.mUserName = lUserName;
			//lUserInfo.mClientGuid = (GameData.get.aPlayerInfo != null) ? GameData.get.aPlayerInfo.mUniqueId : 0;
			lUserInfo.mClientGuid = 0;
			if (!lIsTutorial)
				lUserInfo.mIsAi = (iUser == 0) ? false : true;
			else
				lUserInfo.mIsAi = false;

			lUserInfo.mHeadquarterGuid = (uint)(iUser + 1);
			lUserInfo.mTeamIdx = iUser + 1; // 아직 팀 인덱를 지정하는 개념이 없었기에 임시로 여기서 지정했습니다. [3/18/2015 jhpark]

			lUserInfo.mMaxMp = BattleConfig.get.mTestMaxMp;
			lUserInfo.mStartMp = BattleConfig.get.mTestStartMp;

			if (GameData.get.aPlayerInfo != null && lIsTutorial)
			{
				//lUserInfo.mNationExps = GameData.get.aPlayerInfo.mNationExps;
				//lUserInfo.mNationLevels = GameData.get.aPlayerInfo.mNationLevels;
				lUserInfo.mNationData = GameData.get.aPlayerInfo.mNationData;
				lUserInfo.mWin = 0;
				lUserInfo.mLose = 0;
				lUserInfo.mWinStreak = 0;
				lUserInfo.mLoseStreak = 0;
			}

			CustomBattleCommanderDeckData lCustomDeckData = CustomBattleCommanderItemTable.get.GetCustomDeckData(lUserName, lUseDeckIdx);
			lUserInfo.mBattleNation = lCustomDeckData.mDeckNation; /* (iUser % 2 == 0) ? DeckNation.US : DeckNation.GE;/**/

			List<CommanderItemData> lCommanderItemDataList = CustomBattleCommanderItemTable.get.GetTestCommanderItemData(lUserInfo.mUserName, lUseDeckIdx);
			for (int iSlot = 0; iSlot < lCommanderItemDataList.Count; iSlot++)
				lUserInfo.AddCommanderItem(iSlot, lCommanderItemDataList[iSlot]);

			mUserInfos[iUser] = lUserInfo;
		}

		// 플레이 관련 초기화를 합니다.
		_InitPlay();
#endif
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON으로 초기화합니다.
	public void InitFromJson(JSONObject pMatchInfoJson)
	{
		mStageNo = (int)pMatchInfoJson.GetField("StageId").n;
		mRuleType = BattleMatchInfo.RuleType.Conquest;
		mPlayTime = BattleConfig.get.mModeTime;

		mRandomSeed = (int)pMatchInfoJson.GetField("RandomIndex").n;

		JSONObject lRandomStateArrayJson = pMatchInfoJson.GetField("RandomState");
		if (lRandomStateArrayJson != null)
		{
			mRandomStates = new UInt32[lRandomStateArrayJson.list.Count];
			for (int iState = 0; iState < lRandomStateArrayJson.list.Count; iState++)
				mRandomStates[iState] = (UInt32)lRandomStateArrayJson.list[iState].n;
		}

		JSONObject lUserInfoArrayJson = pMatchInfoJson.GetField("UserInfo");

		mUserCount = lUserInfoArrayJson.list.Count;
		for (int iUser = 0; iUser < mUserCount; iUser++)
		{
			JSONObject lUserInfoJson = lUserInfoArrayJson.list[iUser];
			int lUserIdx = (int)lUserInfoJson.GetField("UserIdx").n;
			BattleUserInfo lUserInfo = mUserInfos[lUserIdx];
			lUserInfo.InitFromJson(lUserInfoJson);
		}

		// 플레이 관련 초기화를 합니다.
		_InitPlay();
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 객체를 만들어서 반환합니다.
	public JSONObject ToJson()
	{
		JSONObject lMatchInfoJson = new JSONObject();

		lMatchInfoJson.AddField("StageId", mStageNo);

		lMatchInfoJson.AddField("RandomIndex", mRandomSeed);

		if (mRandomStates != null)
		{
			JSONObject lRandomStateArrayJson = new JSONObject(JSONObject.Type.ARRAY);
			for (int iState = 0; iState < mRandomStates.Length; ++iState)
				lRandomStateArrayJson.Add(mRandomStates[iState]);
			lMatchInfoJson.AddField("RandomState", lRandomStateArrayJson);
		}

		JSONObject lUserInfoArrayJson = new JSONObject(JSONObject.Type.ARRAY);
		for (int iUser = 0; iUser < mUserCount; iUser++)
		{
			BattleUserInfo lUserInfo = mUserInfos[iUser];
			JSONObject lUserInfoJson = lUserInfo.ToJson();
			lUserInfoArrayJson.Add(lUserInfoJson);
		}
		lMatchInfoJson.AddField("UserInfo", lUserInfoArrayJson);

		return lMatchInfoJson;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID의 클라이언트와 연결된 유저 정보를 찾습니다.
	public BattleUserInfo FindClientUserInfo(uint pClientGuid)
	{
		for (int iUser = 0; iUser < mUserCount; ++iUser)
			if (mUserInfos[iUser].mClientGuid == pClientGuid)
				return mUserInfos[iUser];
		return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 플레이 관련 초기화를 합니다.
	protected void _InitPlay()
	{
		// 팀 개수를 얻습니다.
		int lMaxTeamIdx = 0;
		for (int iUser = 0; iUser < mUserCount; ++iUser)
			if (lMaxTeamIdx < mUserInfos[iUser].mTeamIdx)
				lMaxTeamIdx = mUserInfos[iUser].mTeamIdx;
		mTeamCount = lMaxTeamIdx + 1; // 0번 중립 팀 포함

		// 스테이지 스펙을 얻습니다.
		mStageSpec = StageTable.get.FindStageSpec(mStageNo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
    protected int mTeamCount;
    protected StageSpec mStageSpec;
}
