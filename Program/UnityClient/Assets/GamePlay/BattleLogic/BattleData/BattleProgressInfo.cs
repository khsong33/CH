using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleProgressInfo
{
	public enum UserState
	{
		Playing,
		GiveUp,
	}

	public delegate void OnMpChangeDelegate(int pUserIdx, int pCurrentMp, int pMaxMp);
	public delegate void OnVpChangeDelegate(int pTeamIdx, int pCurrentVp, int pMaxVp);

	public delegate void OnSetTeamBattleEndDelegate(int pTeamIdx, BattleProtocol.BattleEndCode pBattleEndCode);
	public delegate void OnMpBuffStartDelegate(int pMpBuffMilliRatio);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 대전 정보
	public BattleMatchInfo aMatchInfo
	{
		get { return mMatchInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저별 상태
	public UserState[] aUserStates
	{
		get { return mUserStates; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저별 MP
	public int[] aUserMps
	{
		get { return mUserMps; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저별 MP에 반영이 안 된 밀리 단위의 여분 MP
	public int[] aUserExtraMilliMps
	{
		get { return mUserExtraMilliMps; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 밀리 단위의 VP 현재 값
	public int[] aTeamMilliVps
	{
		get { return mTeamMilliVps; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 밀리 단위의 VP 이전 값
	public int[] aLastTeamMilliVps
	{
		get { return mLastTeamMilliVps; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 최대 지휘관 수
	public int[] aTeamMaxCommanderCounts
	{
		get { return mTeamMaxCommanderCounts; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 살아있는 지휘관 수
	public int[] aTeamLiveCommanderCounts
	{
		get { return mTeamLiveCommanderCounts; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 획득 메달 수
	public int[] aTeamMedalCounts
	{
		get { return mTeamMedalCounts; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 플레이 중인 팀 개수
	public int aLiveTeamCount
	{
		get { return mLiveTeamCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 플레이 타이머
	public int aPlayTimer
	{
		get { return mPlayTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 스케쥴러
	public BattleProgressEventScheduler aEventScheduler
	{
		get { return mEventScheduler; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 전투 종료 코드
	public BattleProtocol.BattleEndCode[] aTeamBattleEndCodes
	{
		get { return mTeamBattleEndCodes; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnMpChangeDelegate aOnMpChangeDelegate { get; set; }
	public OnVpChangeDelegate aOnVpChangeDelegate { get; set; }

	public OnSetTeamBattleEndDelegate aOnSetTeamBattleEndDelegate { get; set; }
	public OnMpBuffStartDelegate aOnMpBuffStartDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleProgressInfo()
	{
		mUserStates = new UserState[BattleMatchInfo.cMaxUserCount];
		mUserMps = new int[BattleMatchInfo.cMaxUserCount];
		mUserExtraMilliMps = new int[BattleMatchInfo.cMaxUserCount];

		mTeamMaxVp = BattleConfig.get.mTestMaxVp * 1000;
		mTeamMilliVps = new int[BattleMatchInfo.cMaxTeamCount];
		mLastTeamMilliVps = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamMilliMpGenerationSums = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamEnemyMilliVpDecreases = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamMilliSelfVpDecreases = new int[BattleMatchInfo.cMaxTeamCount];
		mIsTeamCommanderExisting = new bool[BattleMatchInfo.cMaxTeamCount];
		mTeamMaxCommanderCounts = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamLiveCommanderCounts = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamMedalCounts = new int[BattleMatchInfo.cMaxTeamCount];
		mTeamBattleEndCodes = new BattleProtocol.BattleEndCode[BattleMatchInfo.cMaxTeamCount];
		mTeamBattleEndPoints = new int[BattleMatchInfo.cMaxTeamCount];

		mEventScheduler = new BattleProgressEventScheduler();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(BattleMatchInfo pMatchInfo)
	{
		mMatchInfo = pMatchInfo;

		mBaseMpMilliGeneration = BattleConfig.get.mHeadquarterMpMilliGeneration;

		for (int iTeam = 0; iTeam < mMatchInfo.aTeamCount; ++iTeam)
		{
			mTeamMilliVps[iTeam] = BattleConfig.get.mTestStartVp * 1000;
			mLastTeamMilliVps[iTeam] = mTeamMilliVps[iTeam];
			mTeamMaxCommanderCounts[iTeam] = 0;
			mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.None;
			mTeamBattleEndPoints[iTeam] = 0;

			if (aOnVpChangeDelegate != null)
				aOnVpChangeDelegate(iTeam, mTeamMilliVps[iTeam], mTeamMaxVp);
		}

		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			BattleUserInfo lUserInfo = mMatchInfo.mUserInfos[iUser];

			mUserStates[iUser] = UserState.Playing;
			mUserMps[iUser] = lUserInfo.mStartMp;
			mUserExtraMilliMps[iUser] = 0;

			if (aOnMpChangeDelegate != null)
				aOnMpChangeDelegate(iUser, mUserMps[iUser], lUserInfo.mMaxMp);

			mTeamMaxCommanderCounts[lUserInfo.mTeamIdx] += lUserInfo.aValidCommanderItemCount;
		}

		mPlayTimer = pMatchInfo.mPlayTime;

		mEventScheduler.Reset();
		mEventScheduler.AddMpBuffEvent(BattleConfig.get.mMpBuffEventStartTime, BattleConfig.get.mMpBuffEventMilliRatio, _OnMpBuffEvent);

		mIsEnableBattleEnd = true;
		mIsUpdateProgress = true;
		mIsProcMpDic = new Dictionary<int, bool>();

		_UpdateLiveTeamCount();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 팀의 MP를 변화시킵니다.
	public bool ChangeMp(int pUserIdx, int pMpChange)
	{
		int lLastMp = mUserMps[pUserIdx];
		lLastMp += pMpChange;

		if (lLastMp < 0)
			lLastMp = 0;
		if (lLastMp > mMatchInfo.mUserInfos[pUserIdx].mMaxMp)
			lLastMp = mMatchInfo.mUserInfos[pUserIdx].mMaxMp;

		if (mUserMps[pUserIdx] != lLastMp)
		{
			mUserMps[pUserIdx] = lLastMp;

			if (aOnMpChangeDelegate != null)
				aOnMpChangeDelegate(pUserIdx, mUserMps[pUserIdx], mMatchInfo.mUserInfos[pUserIdx].mMaxMp);
			return true; // 변화 있음
		}
		else
			return true; // 변화 없음
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 팀의 MP를 변화시킵니다.
	public bool TryMpUsage(int pUserIdx, int pMpUsage)
	{
		if (mUserMps[pUserIdx] < pMpUsage)
			return false;

		return ChangeMp(pUserIdx, -pMpUsage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 팀의 MP를 1/1000 단위로 증가시킵니다.
	public void AddMilliMp(int pUserIdx, int pAddMilliMp)
	{
		//Debug.Log("AddMilliMp() pUserIdx=" + pUserIdx + " pAddMilliMp=" + pAddMilliMp);

		mUserExtraMilliMps[pUserIdx] += pAddMilliMp;
		if (mUserExtraMilliMps[pUserIdx] >= 1000)
		{
			int lAddMp = mUserExtraMilliMps[pUserIdx] / 1000;
			if (ChangeMp(pUserIdx, lAddMp))
				mUserExtraMilliMps[pUserIdx] -= lAddMp * 1000;
			else
				mUserExtraMilliMps[pUserIdx] = 1000;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// VP를 변화시킵니다.
	public bool ChangeMilliVp(int pTeamIdx, int pChangeMilliValue)
	{
		int lLastMilliVp = mTeamMilliVps[pTeamIdx];

		lLastMilliVp += pChangeMilliValue;
		if (lLastMilliVp < 0)
			lLastMilliVp = 0;
		else if (lLastMilliVp > (BattleConfig.get.mTestMaxVp * 1000))
			lLastMilliVp = BattleConfig.get.mTestMaxVp * 1000;

		if (mTeamMilliVps[pTeamIdx] != lLastMilliVp)
		{
			mTeamMilliVps[pTeamIdx] = lLastMilliVp;

			if (aOnVpChangeDelegate != null)
				aOnVpChangeDelegate(pTeamIdx, mTeamMilliVps[pTeamIdx], mTeamMaxVp);
			return true; // 변화 있음
		}
		else
			return true; // 변화 없음
	}
	//-------------------------------------------------------------------------------------------------------
	// 살아있는 지휘관 수를 갱신합니다.
	public void UpdateLiveCommanderCounts(BattleGrid pBattleGrid)
	{
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
		{
			mTeamLiveCommanderCounts[iTeam] = 0;
			mTeamMedalCounts[iTeam] = 0;
		}

		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			BattleUser lUser = pBattleGrid.aUsers[iUser];
			if (lUser == null)
				continue;
			if (mUserStates[iUser] == UserState.GiveUp)
				continue;

			mTeamLiveCommanderCounts[lUser.aTeamIdx] += lUser.aLiveCommanderCount;

			for (int iOtherTeam = 1; iOtherTeam < mMatchInfo.aTeamCount; ++iOtherTeam) // 0번 중립팀 제외
				if (iOtherTeam != lUser.aTeamIdx) // 이 유저의 팀을 제외한 나머지 팀에게
					mTeamMedalCounts[iOtherTeam] += lUser.aValidCommanderCount - lUser.aLiveCommanderCount; // 메달 포인트를 더함
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 포기 처리를 합니다.
	public void SetGiveUpUser(BattleUser pUser)
	{
		mUserStates[pUser.aUserIdx] = UserState.GiveUp;
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 종료를 지정합니다.
	public void SetBattleEnd(BattleGrid pBattleGrid, int pUserIdx, BattleProtocol.BattleEndCode pBattleEndCode)
	{
		BattleUser lUser = pBattleGrid.aUsers[pUserIdx];
		mTeamBattleEndCodes[lUser.aTeamIdx] = pBattleEndCode;

		EnableBattleEnd(true); // 스크립트 종료가 꺼져있을 수도 있으므로
		mIsUpdateProgress = false; // 갱신 종료
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 진행을 갱신합니다.
	public bool UpdateProgress(BattleGrid pBattleGrid, int pFrameDelta)
	{
		if (!mIsUpdateProgress)
			return false; // 전투 종료

		// 이벤트를 검사합니다.
		mEventScheduler.CheckEvent(pFrameDelta);

		// 거점 상태에 따른 MP와 VP 변화를 처리합니다.
		for (int iTeam = 0; iTeam < mMatchInfo.aTeamCount; ++iTeam)
		{
			mLastTeamMilliVps[iTeam] = mTeamMilliVps[iTeam];

			mTeamMilliMpGenerationSums[iTeam] = mBaseMpMilliGeneration; // 기본 MP 생산을 처리합니다.
			mTeamEnemyMilliVpDecreases[iTeam] = 0;
			mTeamMilliSelfVpDecreases[iTeam] = 0;
			mIsTeamCommanderExisting[iTeam] = false;
		}

		// 거점당 MP 생산량과 VP 감소량의 영향을 총합으로 얻습니다.
		List<BattleCheckPoint> lCheckPointList = pBattleGrid.aCheckPointList;
		for (int iPoint = 0; iPoint < lCheckPointList.Count; ++iPoint)
		{
			BattleCheckPoint lCheckPoint = lCheckPointList[iPoint];
			if (lCheckPoint.aTeamIdx > 0)
			{
				if (lCheckPoint.aMilliMpGeneration > 0) // 팀이 점령한 거점이 MP를 생산한다면
					mTeamMilliMpGenerationSums[lCheckPoint.aTeamIdx] += lCheckPoint.aMilliMpGeneration; // 팀의 MP 생산량 총합을 증가시킵니다.

				if (lCheckPoint.aEnemyMilliVpDecrease > 0) // 팀이 점령한 거점이 적 VP를 감소시킨다면
					mTeamEnemyMilliVpDecreases[lCheckPoint.aTeamIdx] += lCheckPoint.aEnemyMilliVpDecrease; // 적 VP 감소량을 지정합니다.
			}
		}

		// 유저별 MP와 VP 처리를 합니다.
		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			if (mUserStates[iUser] == UserState.GiveUp)
				continue;

			BattleUser lUser = pBattleGrid.aUsers[iUser];

			// 유저별로 MP 생산량을 더합니다.
			if (IsMpProc(iUser))
			{
				AddMilliMp(iUser, mTeamMilliMpGenerationSums[lUser.aTeamIdx] * pFrameDelta / 1000);
			}

			// 본부 거점을 뺏결을 때의 VP 감소를 더합니다.
			if ((lUser.aHeadquarter.aLinkCheckPoint != null) &&
				(lUser.aHeadquarter.aLinkCheckPoint.aTeamIdx != lUser.aTeamIdx) &&
				(lUser.aTeamIdx > 0)) // 중립이 아닌 다른 팀이 점령했을 때에만
				mTeamMilliSelfVpDecreases[lUser.aTeamIdx] = BattleConfig.get.mHeadquarterEnemyMilliVpDecrease;
		}

		// 팀별로 VP 감소량을 뺍니다.
		bool lIsLostTeam = false;
		int lWinningTeamIdx = -1;
		int lWinningTeamEnemyVpDecrease = mTeamEnemyMilliVpDecreases[0];
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if (mTeamBattleEndCodes[iTeam] == BattleProtocol.BattleEndCode.None)
			{
				if (lWinningTeamEnemyVpDecrease < mTeamEnemyMilliVpDecreases[iTeam]) // 해당 팀이 기존 승리 팀보다 더 많은 점수의 승리 거점을 가졌다면
				{
					lWinningTeamIdx = iTeam; // 승리팀 교체
					lWinningTeamEnemyVpDecrease = mTeamEnemyMilliVpDecreases[iTeam];
				}
				else if (lWinningTeamEnemyVpDecrease == mTeamEnemyMilliVpDecreases[iTeam]) // 해당 팀이 기존 승리 팀과 같은 점수의 승리 거점을 가졌다면
				{
					lWinningTeamIdx = -1; // 무승부로 승리팀 없음
				}
			}

		if (lWinningTeamIdx > 0)
		{
			for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
				if ((iTeam != lWinningTeamIdx) &&
					(mTeamBattleEndCodes[iTeam] == BattleProtocol.BattleEndCode.None))
				{
					ChangeMilliVp(iTeam, -lWinningTeamEnemyVpDecrease * pFrameDelta / 1000);

					if (mTeamMilliVps[iTeam] <= 0)
					{
						mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayLose;
						if (aOnSetTeamBattleEndDelegate != null)
							aOnSetTeamBattleEndDelegate(iTeam, mTeamBattleEndCodes[iTeam]);
						lIsLostTeam = true; // 패배한 팀 존재
					}
				}
		}

		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if ((mTeamMilliSelfVpDecreases[iTeam] > 0) &&
				(mTeamBattleEndCodes[iTeam] == BattleProtocol.BattleEndCode.None))
			{
				ChangeMilliVp(iTeam, -mTeamMilliSelfVpDecreases[iTeam] * pFrameDelta / 1000);

				if (mTeamMilliVps[iTeam] <= 0)
				{
					mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayLose;
					if (aOnSetTeamBattleEndDelegate != null)
						aOnSetTeamBattleEndDelegate(iTeam, mTeamBattleEndCodes[iTeam]);
					lIsLostTeam = true; // 패배한 팀 존재
				}
			}

		// 부대 전멸 검사를 합니다.
		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			if (mUserStates[iUser] == UserState.GiveUp)
				continue;

			BattleUser lUser = pBattleGrid.aUsers[iUser];
			if (lUser.aUserInfo.aValidCommanderItemCount > 0)
				mIsTeamCommanderExisting[lUser.aTeamIdx] = true;
		}
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if ((mTeamBattleEndCodes[iTeam] == BattleProtocol.BattleEndCode.None) &&
				!mIsTeamCommanderExisting[iTeam])
			{
				mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayLose;
				if (aOnSetTeamBattleEndDelegate != null)
					aOnSetTeamBattleEndDelegate(iTeam, mTeamBattleEndCodes[iTeam]);
				lIsLostTeam = true; // 패배한 팀 존재
			}

		// 살아남은 팀 개수를 갱신합니다.
		if (lIsLostTeam)
			_UpdateLiveTeamCount();

		// 플레이 타이머를 감소시킵니다.
		mPlayTimer -= pFrameDelta;
		if (mPlayTimer <= 0)
			mPlayTimer = 0;

		// 전투 종료를 처리합니다.
		if (mIsEnableBattleEnd)
		{
			if ((mLiveTeamCount <= 1) ||
				(mPlayTimer <= 0))
			{
				_SetBattleEndCodes(pBattleGrid); // 전투 종료 코드를 지정합니다.
				return false; // 전투 종료
			}
		}

		return true; // 전투 계속
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트 제어 및 전투로직에 의한 전투 종료를 설정합니다.
	public void EnableBattleEnd(bool pIsEnableBattleEnd)
	{
		mIsEnableBattleEnd = pIsEnableBattleEnd;
	}
	//-------------------------------------------------------------------------------------------------------
	// Mp 생산의 활성화 여부를 설정합니다.
	public void SetActiveMpProc(int pUserScriptId, bool pIsActive)
	{
		if (!mIsProcMpDic.ContainsKey(pUserScriptId))
			mIsProcMpDic.Add(pUserScriptId, pIsActive);
		else
			mIsProcMpDic[pUserScriptId] = pIsActive;
	}
	//-------------------------------------------------------------------------------------------------------
	// Mp 생산의 활성화 여부를 확인합니다.
	public bool IsMpProc(int pUserScriptId)
	{
		if (!mIsProcMpDic.ContainsKey(pUserScriptId))
			return true;
		else
			return mIsProcMpDic[pUserScriptId];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 플레이 중인 팀 개수를 갱신합니다.
	private void _UpdateLiveTeamCount()
	{
		mLiveTeamCount = 0;
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if ((mTeamMilliVps[iTeam] > 0) &&
				(mTeamBattleEndCodes[iTeam] == BattleProtocol.BattleEndCode.None))
				++mLiveTeamCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 종료 코드를 지정합니다.
	private void _SetBattleEndCodes(BattleGrid pBattleGrid)
	{
		// 팀별 전투 종료 점수를 VP로 초기화합니다.
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			mTeamBattleEndPoints[iTeam] = mTeamMilliVps[iTeam] * 10000; // 지휘관 점수 만의 자리, 거점 점수 십의 자리

		// 남아있는 지휘관 수당 100점씩 추가합니다.
		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			BattleUser lUser = pBattleGrid.aUsers[iUser];
			if (mTeamBattleEndCodes[lUser.aTeamIdx] != BattleProtocol.BattleEndCode.None)
				continue; // 이미 전투 종료한 팀의 유저

			mTeamBattleEndPoints[lUser.aTeamIdx] += lUser.aLiveCommanderCount * 100; // 남아있는 지휘관 수당 100점
		}

		// 점령한 거점당 1점씩 추가합니다.
		for (int iPoint = 0; iPoint < pBattleGrid.aCheckPointList.Count; ++iPoint)
		{
			BattleCheckPoint lCheckPoint = pBattleGrid.aCheckPointList[iPoint];
			if (mTeamBattleEndCodes[lCheckPoint.aTeamIdx] != BattleProtocol.BattleEndCode.None)
				continue; // 이미 전투 종료한 팀의 거점

			mTeamBattleEndPoints[lCheckPoint.aTeamIdx] += 1; // 거점당 1점
		}

		// 가장 높은 전투 종료 점수를 가진 팀과 그 개수를 얻습니다.
		int lMaxBattleEndPoint = 0;
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if (lMaxBattleEndPoint < mTeamBattleEndPoints[iTeam])
				lMaxBattleEndPoint = mTeamBattleEndPoints[iTeam];
		int lMaxBattleEndPointTeamCount = 0;
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
			if (lMaxBattleEndPoint == mTeamBattleEndPoints[iTeam])
				++lMaxBattleEndPointTeamCount;

		// 가장 높은 전투 종료 점수를 가지고 전투 종료 코드를 지정합니다.
		for (int iTeam = 1; iTeam < mMatchInfo.aTeamCount; ++iTeam) // 0번 중립팀 제외
		{
			if (mTeamBattleEndCodes[iTeam] != BattleProtocol.BattleEndCode.None)
				continue; // 이미 전투 종료한 팀은 제외

			if (mTeamBattleEndPoints[iTeam] >= lMaxBattleEndPoint) // 최고 점수라면
			{
				if (lMaxBattleEndPointTeamCount > 1) // 두 팀 이상이 최고 점수일 때에는
					mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayDraw; // 비김
				else // 유일한 최고 점수의 팀일 때에는
					mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayWin; // 승리
			}
			else // 최고 점수가 아니라면
			{
				mTeamBattleEndCodes[iTeam] = BattleProtocol.BattleEndCode.PlayLose; // 패배
			}

			if (aOnSetTeamBattleEndDelegate != null)
				aOnSetTeamBattleEndDelegate(iTeam, mTeamBattleEndCodes[iTeam]);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleProgressEvent.OnMpBuffEventDelegate
	private void _OnMpBuffEvent(int pMpBuffMilliRatio)
	{
		//Debug.Log("_OnMpBuffEvent() pMpBuffMilliRatio=" + pMpBuffMilliRatio);

		mBaseMpMilliGeneration
			= (int)((Int64)BattleConfig.get.mHeadquarterMpMilliGeneration
			* BattleConfig.get.mMpBuffEventMilliRatio
			/ 10000); // 만분율

		if (aOnMpBuffStartDelegate != null)
			aOnMpBuffStartDelegate(pMpBuffMilliRatio);		
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleMatchInfo mMatchInfo;

	private UserState[] mUserStates;
	private int[] mUserMps;
	private int[] mUserExtraMilliMps;

	private int mTeamMaxVp;
	private int mBaseMpMilliGeneration;
	private int[] mTeamMilliVps;
	private int[] mLastTeamMilliVps;
	private int[] mTeamMilliMpGenerationSums;
	private int[] mTeamEnemyMilliVpDecreases;
	private int[] mTeamMilliSelfVpDecreases;
	private bool[] mIsTeamCommanderExisting;
	private int[] mTeamMaxCommanderCounts;
	private int[] mTeamLiveCommanderCounts;
	private int[] mTeamMedalCounts;
	private int mLiveTeamCount;
	private BattleProtocol.BattleEndCode[] mTeamBattleEndCodes;
	private int[] mTeamBattleEndPoints;

	private int mPlayTimer;
	private BattleProgressEventScheduler mEventScheduler;

	private bool mIsEnableBattleEnd;
	private bool mIsUpdateProgress;
	private Dictionary<int, bool> mIsProcMpDic;
}
