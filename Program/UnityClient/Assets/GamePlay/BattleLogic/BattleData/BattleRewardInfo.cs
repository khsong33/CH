﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using CombatJson;

public class BattleRewardCommanderItemExp
{
	public int mItemIdx;
	public int mRank;
	public int mExp;
}

public class BattleRewardInfo
{
	public DeckNation mNation;
	public int mNationExp;
	public int mNationLevel;
	public List<BattleRewardCommanderItemExp> mCommanderItemExpList;
	public List<CommanderItemData> mAddCommanderItemDataList;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleRewardInfo()
	{
		mCommanderItemExpList = new List<BattleRewardCommanderItemExp>();
		mAddCommanderItemDataList = new List<CommanderItemData>();
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON으로 초기화합니다.
	public void InitFromJson(JSONObject pRewardInfoJson)
	{
		mNation = (DeckNation)pRewardInfoJson.GetField("Nation").n;
		mNationExp = (int)pRewardInfoJson.GetField("NationExp").n;
		mNationLevel = (int)pRewardInfoJson.GetField("NationLevel").n;

		JSONObject lCommanderExpJson = pRewardInfoJson.GetField("CommanderExp");
		if (lCommanderExpJson != null)
		{
			List<JSONObject> lCommanderItemExpListJson = lCommanderExpJson.list;
			int lCommanderRewardCount = lCommanderItemExpListJson.Count;
			for (int iExp = 0; iExp < lCommanderRewardCount; iExp++)
			{
				BattleRewardCommanderItemExp lItemExpInfo = new BattleRewardCommanderItemExp();

				lItemExpInfo.mItemIdx = Convert.ToInt32(lCommanderItemExpListJson[iExp].GetField("ItemId").n);
				lItemExpInfo.mExp = (int)lCommanderItemExpListJson[iExp].GetField("Exp").n;
				lItemExpInfo.mRank = (int)lCommanderItemExpListJson[iExp].GetField("Rank").n;

				mCommanderItemExpList.Add(lItemExpInfo);
			}
		}

		JSONObject lAddItemListJson = pRewardInfoJson.GetField("AddItemList");
		if (lAddItemListJson != null)
		{
			List<JSONObject> lAddCommanderItemDataListJson = lAddItemListJson.list;
			int lAddCommanderItemDataCount = lAddCommanderItemDataListJson.Count;
			for (int iCommander = 0; iCommander < lAddCommanderItemDataCount; iCommander++)
			{
				CommanderItemData lAddCommanderItemData = new CommanderItemData();

				lAddCommanderItemData.mCommanderItemIdx = (int)lAddCommanderItemDataListJson[iCommander].GetField("ItemId").n;
				lAddCommanderItemData.mCommanderSpecNo = (int)lAddCommanderItemDataListJson[iCommander].GetField("ItemNo").n;
				lAddCommanderItemData.mCommanderExpRank = 1;
				lAddCommanderItemData.mCommanderExp = 0;

				mAddCommanderItemDataList.Add(lAddCommanderItemData);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 객체를 만들어서 반환합니다.
	public JSONObject ToJson()
	{
		JSONObject lRewardInfoJson = new JSONObject();

		Debug.LogError("not implemented yet");

		return lRewardInfoJson;
	}
	//-------------------------------------------------------------------------------------------------------
	// 두 보상의 내용이 같은지 검사합니다.
	public static bool CheckIdentical(BattleRewardInfo pA, BattleRewardInfo pB)
	{
		if (pA.mNation != pB.mNation)
			return false;
		if (pA.mNationExp != pB.mNationExp)
			return false;
		if (pA.mNationLevel != pB.mNationLevel)
			return false;
		if (pA.mCommanderItemExpList.Count != pB.mCommanderItemExpList.Count)
			return false;
		for (int iExp = 0; iExp < pA.mCommanderItemExpList.Count; ++iExp)
		{
			BattleRewardCommanderItemExp lCommanderItemExpA = pA.mCommanderItemExpList[iExp];
			BattleRewardCommanderItemExp lCommanderItemExpB = pB.mCommanderItemExpList[iExp];

			if (lCommanderItemExpA.mItemIdx != lCommanderItemExpB.mItemIdx)
				return false;
			if (lCommanderItemExpA.mRank != lCommanderItemExpB.mRank)
				return false;
			if (lCommanderItemExpA.mExp != lCommanderItemExpB.mExp)
				return false;
		}
		if (pA.mAddCommanderItemDataList.Count != pB.mAddCommanderItemDataList.Count)
			return false;
		for (int iData = 0; iData < pA.mAddCommanderItemDataList.Count; ++iData)
		{
			CommanderItemData lCommanderItemDataA = pA.mAddCommanderItemDataList[iData];
			CommanderItemData lCommanderItemDataB = pB.mAddCommanderItemDataList[iData];

			if (lCommanderItemDataA.mCommanderItemIdx != lCommanderItemDataB.mCommanderItemIdx)
				return false;
			if (lCommanderItemDataA.mCommanderSpecNo != lCommanderItemDataB.mCommanderSpecNo)
				return false;
			if (lCommanderItemDataA.mCommanderExpRank != lCommanderItemDataB.mCommanderExpRank)
				return false;
			if (lCommanderItemDataA.mCommanderLevel != lCommanderItemDataB.mCommanderLevel)
				return false;
			if (lCommanderItemDataA.mCommanderExp != lCommanderItemDataB.mCommanderExp)
				return false;
			if (lCommanderItemDataA.mSpawnCount != lCommanderItemDataB.mSpawnCount)
				return false;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [디버그] 덤프 출력을 합니다.
	public void DebugDump(String pDumpFilePath)
	{
		using (FileStream lFileStream = new FileStream(pDumpFilePath, FileMode.Create))
		{
			StreamWriter lStreamWriter = new StreamWriter(lFileStream);

			lStreamWriter.WriteLine("mNation=" + mNation.ToString());
			lStreamWriter.WriteLine("mNationExp=" + mNationExp);
			lStreamWriter.WriteLine("mNationLevel=" + mNationLevel);
			lStreamWriter.WriteLine("mCommanderItemExpList.Count=" + mCommanderItemExpList.Count);
			for (int iExp = 0; iExp < mCommanderItemExpList.Count; ++iExp)
			{
				lStreamWriter.WriteLine("mCommanderItemExpList[" + iExp + "]");

				BattleRewardCommanderItemExp lCommanderItemExp = mCommanderItemExpList[iExp];

				lStreamWriter.WriteLine("\tmItemIdx=" + lCommanderItemExp.mItemIdx);
				lStreamWriter.WriteLine("\tmRank=" + lCommanderItemExp.mRank);
				lStreamWriter.WriteLine("\tmExp=" + lCommanderItemExp.mExp);
			}
			lStreamWriter.WriteLine("mAddCommanderItemDataList.Count=" + mAddCommanderItemDataList.Count);
			for (int iData = 0; iData < mAddCommanderItemDataList.Count; ++iData)
			{
				lStreamWriter.WriteLine("mAddCommanderItemDataList[" + iData + "]");

				CommanderItemData lCommanderItemData = mAddCommanderItemDataList[iData];

				lStreamWriter.WriteLine("\tmCommanderItemIdx=" + lCommanderItemData.mCommanderItemIdx);
				lStreamWriter.WriteLine("\tmCommanderSpecNo=" + lCommanderItemData.mCommanderSpecNo);
				lStreamWriter.WriteLine("\tmCommanderRank=" + lCommanderItemData.mCommanderExpRank);
				lStreamWriter.WriteLine("\tmCommanderLevel=" + lCommanderItemData.mCommanderLevel);
				lStreamWriter.WriteLine("\tmCommanderExp=" + lCommanderItemData.mCommanderExp);
				lStreamWriter.WriteLine("\tmSpawnCount=" + lCommanderItemData.mSpawnCount);
			}

			lStreamWriter.Close();
			lFileStream.Close();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
