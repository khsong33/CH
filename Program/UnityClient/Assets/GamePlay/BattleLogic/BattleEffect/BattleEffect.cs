using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class BattleEffect : FrameObject
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 발생 딜레이
	public int aActivationDelay
	{
		get { return mActivationDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 이펙트 인터페이스
	public abstract IStageEffect aStageEffect { get; }
	//-------------------------------------------------------------------------------------------------------
	// 그리드 안에서의 관리 노드
	public LinkedListNode<BattleEffect> aGridNode
	{
		get { return mGridNode; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleEffect(BattleLogic pBattleLogic)
	{
		mBattleLogic = pBattleLogic;
		mGridNode = new LinkedListNode<BattleEffect>(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateEffect(
		BattleGrid pBattleGrid,
		int pActivationDelay)
	{
		//Debug.Log("OnCreate() - " + this);

		base.OnCreateObject(pBattleGrid.aFrameUpdater);

		mBattleGrid = pBattleGrid;
		mActivationDelay = pActivationDelay;

		ReserveTask(_OnActivated, pActivationDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		if (mGridNode.List != null)
			mGridNode.List.Remove(mGridNode);

		base.OnDestroy();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트가 활성화될 때 호출됩니다.
	public abstract void OnActivateEffect();

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	protected void _OnActivated(FrameTask pTask, int pFrameDelta)
	{
		OnActivateEffect();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	protected BattleLogic mBattleLogic;
	private LinkedListNode<BattleEffect> mGridNode;

	protected BattleGrid mBattleGrid;
	protected int mActivationDelay;
}
