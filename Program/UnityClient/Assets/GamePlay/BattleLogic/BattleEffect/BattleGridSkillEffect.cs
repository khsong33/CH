using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGridSkillEffect : BattleEffect
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 효과
	public GroundEffectSpec aGroundEffectSpec
	{
		get { return mGroundEffectSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각
	public int aDirectionY
	{
		get { return mDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과를 받는 격자 타일
	public BattleGridTile aEffectGridTile
	{
		get { return mEffectGridTile; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과를 받는 격자 좌표
	public Coord2 aEffectGridCoord
	{
		get { return mEffectGridCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageGridSkillEffect aStageGridSkillEffect
	{
		get { return mStageGridSkillEffect; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 속성
	public override IStageEffect aStageEffect
	{
		get { return mStageGridSkillEffect; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 변경자
	public StatTableModifier<GroundStat> aGroundStatTableModifier
	{
		get { return mGroundStatTableModifier; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지속 시간
	public int aDurationDelay
	{
		get { return mDurationDelay; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleGridSkillEffect(aGuid:{0} aEffectGridTile:{1} aEffectGridCoord:{2})", aGuid, aEffectGridTile, aEffectGridCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleGridSkillEffect(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateGridSkillEffect(BattleGridTile pEffectGridTile, Coord2 pEffectGridCoord, int pDirectionY, GroundEffectSpec pGroundEffectSpec, int pActivationDelay, int pDurationDelay)
	{
		// 생성 인자를 저장합니다.
		mGroundEffectSpec = pGroundEffectSpec;
		mDirectionY = pDirectionY;
		mEffectGridTile = pEffectGridTile;
		mEffectGridCoord = pEffectGridCoord;
		mDurationDelay = pDurationDelay;

		// 상위 버전을 호출합니다.
		OnCreateEffect(pEffectGridTile.aBattleGrid, pActivationDelay);

		// 스테이지 이펙트를 생성합니다.
		mStageGridSkillEffect = aBattleGrid.aStage.CreateGridSkillEffect(this, pDirectionY);

		// 스탯 변경자를 초기화합니다.
		mGroundStatTableModifier = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지 이펙트를 제거합니다.
		mStageGridSkillEffect.DestroyEffect();
		mStageGridSkillEffect = null;

		// 격자 타일에서 효과를 제거합니다.
		if (mChainItem != null)
		{
			mEffectGridTile.RemoveSkillEffect(mChainItem);

			mChainItem = null;
		}

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyGridSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleEffect 콜백
	public override void OnActivateEffect()
	{
		//Debug.Log("OnActivateEffect() - " + this);

		// 스탯 변경자를 지정합니다.
		if (mGroundEffectSpec != null)
			mGroundStatTableModifier = mGroundEffectSpec.mGroundStatTableModifier;

		// 격자 타일에 효과를 적용합니다.
		mChainItem = mEffectGridTile.AddSkillEffect(this);

		// 스테이지에 표시합니다.
		if (mStageGridSkillEffect != null)
			mStageGridSkillEffect.Activate();

		// 지속 시간 제약이 있는 경우 비활성화를 예약합니다(토클 스킬의 경우는 Destroy()를 바로 호출).
		if (mDurationDelay > 0)
			ReserveTask(_OnDeActivated, mDurationDelay);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnDeActivated(FrameTask pTask, int pFrameDelta)
	{
		Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mDirectionY;
	private BattleGridTile mEffectGridTile;
	private Coord2 mEffectGridCoord;
	private GroundEffectSpec mGroundEffectSpec;
	private GuidObjectChainItem<BattleGridSkillEffect> mChainItem;

	private IStageGridSkillEffect mStageGridSkillEffect;

	private StatTableModifier<GroundStat> mGroundStatTableModifier;

	private int mDurationDelay;
}
