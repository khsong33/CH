using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGrid
{
	public enum TeamSelection
	{
		NonAllyTeam, // 적군과 중립군
		AllyTeam, // 아군
		NonNeutralTeam, // 중립이 아는 모든 팀
		EnemyTeam, // 적군
		NonEnemyTeam, // 아군과 중립군
		NeutralTeam, // 중립팀
	}
	public enum Touch
	{
		OnDragStart,
		OnDragMove,
		OnDragEnd,
		OnClick,
	}

	public interface ISearchComparator<TBattleObject> where TBattleObject : BattleObject
	{
		TBattleObject aResult { get; }

		bool Compare(TBattleObject pCompareObject, int pSqrObjectDistance); // 계속 다른 후보들을 찾아볼지 여부를 반환
	}

	public struct LayoutHeader
	{
		public enum Attribute
		{
			AStarHeuristic,
		}

		public int mAStarHeuristic;
	}

	private struct TileParam
	{
		public BattleGridTile mTile;
		public String mTileToken;
		public String[] mParamTokens;
	}

	public delegate bool OnObjectHandlingDelegate<TBattleObject>(TBattleObject pBattleObject, int pSqrObjectDistance) where TBattleObject : BattleObject;
	public delegate void OnSpawnCommanderDelegate(BattleCommander pCommander);
	public delegate void OnEventDelegate(BattleEventPoint pEventPoint, BattleEventPoint.EventType pEventType);

	public const int cDefaultAStarHeuristic = 50;

	public const int cMaxUserCommanderCount = BattleMatchInfo.cMaxUserCount * BattleUserInfo.cMaxCommanderCount;
	public const int cCheckPointCommanderItemStartIdx = 0; // 거점 지휘관은 0부터 시작
	public const int cMaxCheckPointCount = 24;
	public const int cEventPointCommanderItemStartIdx = cCheckPointCommanderItemStartIdx + cMaxCheckPointCount; // 이벤트 지휘관은 거점 지휘관 다음 번호부터 시작
	public const int cMaxEventPointCount = 24;
	public const int cMaxCommanderCount = cMaxUserCommanderCount + cMaxCheckPointCount + cMaxEventPointCount;

	public const int cUserCommanderGridIdxStart = 0;
	public const int cUserCommanderGridIdxEnd = cUserCommanderGridIdxStart + cMaxUserCommanderCount - 1;
	public const int cCheckPointCommanderGridIdxStart = cUserCommanderGridIdxEnd + 1;
	public const int cCheckPointCommanderGridIdxEnd = cCheckPointCommanderGridIdxStart + cMaxCheckPointCount - 1;
	public const int cEventPointCommanderGridIdxStart = cCheckPointCommanderGridIdxEnd + 1;
	public const int cEventPointCommanderGridIdxEnd = cEventPointCommanderGridIdxStart + cMaxEventPointCount - 1;

	public const int cMaxUserTroopCount = cMaxUserCommanderCount * CommanderTroopSet.cMaxTroopCount;

	public static readonly int[,] sEvenColNearOffsetsInDistanceOne = { { -1, 0 }, { 0, -1 }, { 1, -1 }, { 1, 0 }, { 1, 1 }, { 0, 1 } };
	public static readonly int[,] sOddColNearOffsetsInDistanceOne = { { -1, 0 }, { -1, -1 }, { 0, -1 }, { 1, 0 }, { 0, 1 }, { -1, 1 } };
	public static readonly int[,] sEvenColNearOffsetsInDistanceTwo = { { -2, 0 }, { -1, -1 }, { -1, -2 }, { 0, -2 }, { 1, -2 }, { 2, -1 }, { 2, 0 }, { 2, 1 }, { 1, 2 }, { 0, 2 }, { -1, 2 }, { -1, 1 } };
	public static readonly int[,] sOddColNearOffsetsInDistanceTwo = { { -2, 0 }, { -2, -1 }, { -1, -2 }, { 0, -2 }, { 1, -2 }, { 1, -1 }, { 2, 0 }, { 1, 1 }, { 1, 2 }, { 0, 2 }, { -1, 2 }, { -2, 1 } };

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStage aStage
	{
		get { return mStage; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 업데이터
	public FrameUpdater aFrameUpdater
	{
		get { return mFrameUpdater; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이아웃 이름
	public String aLayoutName
	{
		get { return mLayoutName; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 넓이
	public int aWidthX { get { return mMaxCoordX; } }
	public int aHeightZ { get { return mMaxCoordZ; } }
	//-------------------------------------------------------------------------------------------------------
	// 좌표 범위
	public int aMinCoordX { get { return mMinCoordX; } }
	public int aMaxCoordX { get { return mMaxCoordX; } }
	public int aMinCoordZ { get { return mMinCoordZ; } }
	public int aMaxCoordZ { get { return mMaxCoordZ; } }
	public Coord2 aCenter { get { return mCenter; } }
	public int aMaxSearchRange { get { return mMaxSearchRange; } }
	public int aEventCheckTileDistance { get { return mEventCheckTileDistance; } }
	//-------------------------------------------------------------------------------------------------------
	// 타일 정보
	public BattleGridTile[,] aTiles { get { return mTiles; } }

	public int aTileRowCount { get { return mTileRowCount; } }
	public int aTileColCount { get { return mTileColCount; } }

	public int aTileScaleX { get { return mTileScaleX; } }
	public int aTileScaleZ { get { return mTileScaleZ; } }
	public int aTileScaleMin { get { return mTileScaleMin; } }
	public int aTileScaleMax { get { return mTileScaleMax; } }

	public int aTileBlockScaleX { get { return mTileBlockScaleX; } }
	public int aTileBlockScaleZ { get { return mTileBlockScaleZ; } }

	public int aTileDiagonalScale { get { return mTileDiagonalScale; } }
	public int aTileHalfDiagonalScale { get { return mTileHalfDiagonalScale; } }
	//-------------------------------------------------------------------------------------------------------
	// 셀 정보
	public BattleGridCell[,] aCells { get { return mCells; } }

	public int aCellRowCount { get { return mCellRowCount; } }
	public int aCellColCount { get { return mCellColCount; } }

	public int aCellScaleX { get { return mCellScaleX; } }
	public int aCellScaleZ { get { return mCellScaleZ; } }

	public int aCellBlockScaleX { get { return mCellBlockScaleX; } }
	public int aCellBlockScaleZ { get { return mCellBlockScaleZ; } }
	//-------------------------------------------------------------------------------------------------------
	// 본부 리스트
	public List<BattleHeadquarter> aHeadquarterList
	{
		get { return mHeadquarterList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 리스트
	public List<BattleCheckPoint> aCheckPointList
	{
		get { return mCheckPointList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 리스트
	public List<BattleEventPoint> aEventPointList
	{
		get { return mEventPointList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드 리스트
	public List<BattleBarricade> aBarricadeList
	{
		get { return mBarricadeList; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 테이블
	public BattleUser[] aUsers
	{
		get { return mUsers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 유저 인덱스
	public int aMaxUserIdx
	{
		get { return mMaxUserIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 유저 테이블
	public BattleUser[][] aTeamUsers
	{
		get { return mTeamUsers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 유저수
	public int[] aTeamUserCounts
	{
		get { return mTeamUserCounts; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 테이블
	public BattleCommander[] aCommanders
	{
		get { return mCommanders; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 테이블
	public BattleTroop[] aTroops
	{
		get { return mTroops; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저별 지휘관 체인
	public GuidObjectChain<BattleCommander>[] aCommanderChains
	{
		get { return mCommanderChains; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 부대 체인
	public GuidObjectChain<BattleTroop>[] aTroopChains
	{
		get { return mTroopChains; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 타게팅 더미 체인
	public GuidObjectChain<BattleTargetingDummy>[] aTargetingDummyChains
	{
		get { return mTargetingDummyChains; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀별 탐지 오브젝트 체인
	public GuidObjectChain<BattleObject>[] aDetectionObjectChains
	{
		get { return mDetectionObjectChains; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 모두 탐지 가능함 플래그 배열
	public bool[] aIsAllDetectables
	{
		get { return mIsAllDetectables; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 아무도 탐지 불가능함 플래그 배열
	public bool[] aIsNoneDetectables
	{
		get { return mIsNoneDetectables; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 경로 찾기
	public BattleGridPathFinder aPathFinder
	{
		get { return mPathFinder; }
	}
	//-------------------------------------------------------------------------------------------------------
	// [개발용] 통계
	public BattleGridStatistics aStatistics
	{
		get { return mStatistics; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 매치 정보
	public BattleMatchInfo aMatchInfo
	{
		get { return mMatchInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 진행 정보
	public BattleProgressInfo aProgressInfo
	{
		get { return mProgressInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 팀 개수
	public int aTeamCount
	{
		get { return mTeamCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 동기화 랜덤
	public SyncRandom aSyncRandom
	{
		get { return mSyncRandom; }
	}
	//-------------------------------------------------------------------------------------------------------
	// AI 관련 정보 계산
	public BattleAiCollector aAiCollector
	{
		get { return mAiCollector; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관이 스폰될 때 호출되는 콜백
	public OnSpawnCommanderDelegate aOnSpawnCommanderDelegate
	{
		get { return mOnSpawnCommanderDelegate; }
		set { mOnSpawnCommanderDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트가 발생할 때 호출되는 콜백
	public OnEventDelegate aOnEventDelegate
	{
		get { return mOnEventDelegate; }
		set { mOnEventDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// [디버그] 모니터링 대상 오브젝트
	public uint aDebugTroopGuid { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이 객체를 파괴합니다.
	public void Destroy()
	{
		OnDestroy();

		mBattleLogic.DestroyGrid(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleGrid(BattleLogic pBattleLogic, String pLayoutName, String pLayoutCsvText)
	{
		mBattleLogic = pBattleLogic;
		
		mFrameUpdater = new FrameUpdater(
			BattleConfig.get.mFrameTime,
			BattleConfig.get.mFrameSliceCount,
			BattleConfig.get.mTaskFrameRange);

		mCommanders = new BattleCommander[cMaxCommanderCount];
		mTroops = new BattleTroop[cMaxCommanderCount * CommanderTroopSet.cMaxTroopCount];
		mEffectList = new LinkedList<BattleEffect>();
		mObserverDummyList = new LinkedList<BattleObserverDummy>();

		mCommanderChains = new GuidObjectChain<BattleCommander>[BattleMatchInfo.cMaxUserCount];
		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; ++iUser)
			mCommanderChains[iUser] = new GuidObjectChain<BattleCommander>();

		mTroopChains = new GuidObjectChain<BattleTroop>[BattleMatchInfo.cMaxTeamCount];
		mTargetingDummyChains = new GuidObjectChain<BattleTargetingDummy>[BattleMatchInfo.cMaxTeamCount];
		mDetectionObjectChains = new GuidObjectChain<BattleObject>[BattleMatchInfo.cMaxTeamCount];
		mIsAllDetectables = new bool[BattleMatchInfo.cMaxTeamCount];
		mIsNoneDetectables = new bool[BattleMatchInfo.cMaxTeamCount];
		for (int iTeam = 0; iTeam < BattleMatchInfo.cMaxTeamCount; ++iTeam)
		{
			mTroopChains[iTeam] = new GuidObjectChain<BattleTroop>();
			mTargetingDummyChains[iTeam] = new GuidObjectChain<BattleTargetingDummy>();
			mDetectionObjectChains[iTeam] = new GuidObjectChain<BattleObject>();
		}

		mPathFinder = new BattleGridPathFinder();
		mStatistics = new BattleGridStatistics();

		// 레이아웃을 로딩합니다(레이아웃는 한 번 생성되고서는 재활용되는 자원이므로 생성자에서 로딩함).
		CSVObject lLayoutCsvObject = new CSVObject();
		lLayoutCsvObject.LoadCSV(pLayoutCsvText, 0); // 헤더를 가지고 있다고 가정합니다.
		LoadLayout(pLayoutName, lLayoutCsvObject);

		mSyncRandom = new SyncRandom();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateGrid(BattleMatchInfo pMatchInfo, BattleProgressInfo pProgressInfo, IStage pStage)
	{
	#if UNITY_EDITOR
		aDebugTroopGuid = 0;
	#endif

		mMatchInfo = pMatchInfo;
		mProgressInfo = pProgressInfo;
		mStage = pStage;

		mTeamCount = pMatchInfo.aTeamCount;

		for (int iTeam = 0; iTeam < BattleMatchInfo.cMaxTeamCount; ++iTeam)
		{
			mIsAllDetectables[iTeam] = true;
			mIsNoneDetectables[iTeam] = false;
		}

		if (pMatchInfo.mRandomStates != null)
			mSyncRandom.RandomizeSync(pMatchInfo.mRandomStates, pMatchInfo.mRandomSeed);
		else
			mSyncRandom.RandomizeStatic(pMatchInfo.mRandomSeed);

		mAiCollector = mBattleLogic.CreateAiCollector(pMatchInfo.mRuleType);
		mAiCollector.SetBattleInfo(pMatchInfo, this, pProgressInfo);
		mAiCollector.SetUpdateFrequency(30);
		mAiCollector.PreUpdate();

		_CreateTileObjects(); // 타일 오브젝트를 생성합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public virtual void OnDestroy()
	{
		mMatchInfo = null;
		mProgressInfo = null;
		mStage = null;

		_ResetLayout();

		_DestroyTileObjects(); // 타일 오브젝트를 삭제합니다.

		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; ++iUser)
			mCommanderChains[iUser].Clear();
		for (int iTeam = 0; iTeam < BattleMatchInfo.cMaxTeamCount; ++iTeam)
		{
			mTroopChains[iTeam].Clear();
			mTargetingDummyChains[iTeam].Clear();
			mDetectionObjectChains[iTeam].Clear();
		}

		mAiCollector.Destroy();

		mFrameUpdater.Reset();
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이아웃을 로딩합니다.
	public void LoadLayout(String pLayoutName, CSVObject pLayoutCSVObject)
	{
		mLayoutName = pLayoutName;

		mTileParamList = new List<TileParam>();
		mHeadquarterList = new List<BattleHeadquarter>();
		mCheckPointList = new List<BattleCheckPoint>();
		mEventPointList = new List<BattleEventPoint>();
		mSameGuidEventPointIdxs = new int[cMaxEventPointCount];
		mBarricadeList = new List<BattleBarricade>();
		mHeadquarterLinkCheckPointGuidList = new List<uint>();

		_LoadHeader(pLayoutCSVObject);
		_LoadLayout(
			pLayoutCSVObject,
			BattleConfig.get.mGridTileBlockScale,
			BattleConfig.get.mGridCellBlockScale); // 레이아웃을 로딩합니다.

		mUsers = new BattleUser[BattleMatchInfo.cMaxUserCount];
		mMaxUserIdx = -1;
		mTeamUsers = new BattleUser[BattleMatchInfo.cMaxTeamCount][];
		mTeamUserCounts = new int[BattleMatchInfo.cMaxTeamCount];
		for (int iTeam = 0; iTeam < BattleMatchInfo.cMaxTeamCount; ++iTeam)
		{
			mTeamUsers[iTeam] = new BattleUser[BattleMatchInfo.cMaxUserCount];
			mTeamUserCounts[iTeam] = 0;
		}

		mPathFinder.Init(this, mLayoutHeader.mAStarHeuristic);
		mStatistics.ResetHistory();
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이아웃을 초기화합니다(내부 함수이지만 읽기 편하도록 이곳에 둠).
	private void _ResetLayout()
	{
		// 발사 이펙트를 삭제합니다.
		while (mEffectList.First != null)
		{
			LinkedListNode<BattleEffect> lFireEffectNode = mEffectList.First;
			mEffectList.RemoveFirst();
			lFireEffectNode.Value.Destroy();
		}

		// 관찰자 더미를 삭제합니다.
		while (mObserverDummyList.First != null)
		{
			LinkedListNode<BattleObserverDummy> lObserverDummyNode = mObserverDummyList.First;
			mObserverDummyList.RemoveFirst();
			lObserverDummyNode.Value.Destroy();
		}

		// HQ와 유저와의 연결을 끊습니다.
		for (int iHeadquarter = 0; iHeadquarter < mHeadquarterList.Count; ++iHeadquarter)
		{
			BattleHeadquarter lHeadquarter = mHeadquarterList[iHeadquarter];
			lHeadquarter.aLinkUser = null;
			lHeadquarter.aLinkCheckPoint = null;
		}

		// 유저를 삭제합니다.
		for (int iUser = 0; iUser < mUsers.Length; ++iUser)
			if (mUsers[iUser] != null)
			{
				mUsers[iUser].Destroy();
				mUsers[iUser] = null;
			}
		mMaxUserIdx = -1;

		// 거점 지휘관을 삭제합니다.
		int lCheckPointCommanderStartIdx = cMaxUserCommanderCount + cCheckPointCommanderItemStartIdx;
		int lCheckPointCommanderEndIdx = lCheckPointCommanderStartIdx + cMaxCheckPointCount;
		for (int iCommander = lCheckPointCommanderStartIdx; iCommander < lCheckPointCommanderEndIdx; ++iCommander)
			if (mCommanders[iCommander] != null)
			{
				mCommanders[iCommander].Destroy();
				mCommanders[iCommander] = null;
			}

		// 이벤트 지휘관을 삭제합니다.
		int lEventPointCommanderStartIdx = cMaxUserCommanderCount + cEventPointCommanderItemStartIdx;
		int lEventPointCommanderEndIdx = lEventPointCommanderStartIdx + cMaxEventPointCount;
		for (int iCommander = lEventPointCommanderStartIdx; iCommander < lEventPointCommanderEndIdx; ++iCommander)
			if (mCommanders[iCommander] != null)
			{
				mCommanders[iCommander].Destroy();
				mCommanders[iCommander] = null;
			}

		mOnSpawnCommanderDelegate = null;
		mOnEventDelegate = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저를 지정합니다.
	public void SetUser(int pUserIdx, BattleUser pUser)
	{
		if (mUsers[pUserIdx] != null)
			Debug.LogError("duplicate User id:" + pUserIdx + " - " + this);

		mUsers[pUserIdx] = pUser;
		if (mMaxUserIdx < pUserIdx)
			mMaxUserIdx = pUserIdx;

		BattleUser[] lTeamUsers = mTeamUsers[pUser.aTeamIdx];
		lTeamUsers[mTeamUserCounts[pUser.aTeamIdx]++] = pUser;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저를 초기화합니다.
	public void ResetUser(int pUserIdx)
	{
		BattleUser lResetUser = mUsers[pUserIdx];
		if (lResetUser == null)
			Debug.LogError("null User id:" + pUserIdx + " - " + this);

		mUsers[pUserIdx] = null;

		BattleUser[] lTeamUsers = mTeamUsers[lResetUser.aTeamIdx];
		int lTeamUserCount = 0;

		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; ++iUser)
			if ((mUsers[iUser] != null) &&
				(mUsers[iUser].aTeamIdx == lResetUser.aTeamIdx))
				lTeamUsers[lTeamUserCount++] = mUsers[iUser];

		mTeamUserCounts[lResetUser.aTeamIdx] = lTeamUserCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이아웃의 로딩과 유저 지정 등이 모두 끝나고 전투를 시작하기 전의 처리를 합니다.
	public void PreBattleSetUp()
	{
		// 방어군을 스폰합니다.
		for (int iCheckPoint = 0; iCheckPoint < mCheckPointList.Count; iCheckPoint++)
			mCheckPointList[iCheckPoint].SpawnDefaultDefender();

		// 이벤트를 초기화합니다.
		for (int iEventPoint = 0; iEventPoint < mEventPointList.Count; iEventPoint++) // 기본 초기화
			mEventPointList[iEventPoint].aIsActive = true;
		for (int iEventPoint = 0; iEventPoint < mEventPointList.Count; iEventPoint++) // 같은 그룹군의 이벤트는 하나만 활성화
		{
			BattleEventPoint lEventPoint = mEventPointList[iEventPoint];
			if (lEventPoint.aIsActive)
			{
				// 같은 번호의 이벤트를 찾습니다.
				int lSameGuidEventPointCount = 0;
				mSameGuidEventPointIdxs[lSameGuidEventPointCount++] = iEventPoint;
				for (int iSearch = (iEventPoint + 1); iSearch < mEventPointList.Count; iSearch++)
					if (mEventPointList[iSearch].aEventPointGuid == lEventPoint.aEventPointGuid)
						mSameGuidEventPointIdxs[lSameGuidEventPointCount++] = iSearch;

				// 같은 번호의 이벤트 중에서 하나만 활성화합니다.
				int lActiveEventPointIdx = mSyncRandom.Range(0, lSameGuidEventPointCount);
				for (int iSameGuidEventPoint = 0; iSameGuidEventPoint < lSameGuidEventPointCount; iSameGuidEventPoint++)
					mEventPointList[mSameGuidEventPointIdxs[iSameGuidEventPoint]].aIsActive = (iSameGuidEventPoint == lActiveEventPointIdx);
			}
		}
		for (int iEventPoint = 0; iEventPoint < mEventPointList.Count; iEventPoint++) // 활성화된 이벤트만 준비
		{
			if (mEventPointList[iEventPoint].aIsActive)
				mEventPointList[iEventPoint].ResetEvent();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표가 영역 안에 있는지 검사합니다.
	public bool Contains(Coord2 pCoord)
	{
		if ((pCoord.x < mMinCoordX) ||
			(pCoord.x > mMaxCoordX) ||
			(pCoord.z < mMinCoordZ) ||
			(pCoord.z > mMaxCoordZ))
			return false;
		else
			return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표를 영역 안에 있는 점으로 수정합니다.
	public Coord2 ClampCoord(Coord2 pCoord)
	{
		Coord2 lClampedCoord;
		if (pCoord.x < mMinCoordX)
			lClampedCoord.x = mMinCoordX;
		else if (pCoord.x > mMaxCoordX)
			lClampedCoord.x = mMaxCoordX;
		else
			lClampedCoord.x = pCoord.x;

		if (pCoord.z < mMinCoordZ)
			lClampedCoord.z = mMinCoordZ;
		else if (pCoord.z > mMaxCoordZ)
			lClampedCoord.z = mMaxCoordZ;
		else
			lClampedCoord.z = pCoord.z;

		return lClampedCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 인덱스를 구합니다.
	public RowCol GetTileIndex(Coord2 pCoord)
	{
		if ((pCoord.x < mMinCoordX) ||
			(pCoord.x > mMaxCoordX) ||
			(pCoord.z < mMinCoordZ) ||
			(pCoord.z > mMaxCoordZ))
			return new RowCol(-1, -1);

		int lColIndex = pCoord.x / mTileScaleX;
		int lRowIndex;

		if ((lColIndex & 0x01) == 0) // 짝수라면
			lRowIndex = pCoord.z / mTileScaleZ;
		else
			lRowIndex = (pCoord.z + mTileBlockScaleZ) / mTileScaleZ;

		return new RowCol(lRowIndex, lColIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표가 속한 타일을 얻습니다.
	public BattleGridTile GetTile(Coord2 pCoord)
	{
		if ((pCoord.x < mMinCoordX) ||
			(pCoord.x > mMaxCoordX) ||
			(pCoord.z < mMinCoordZ) ||
			(pCoord.z > mMaxCoordZ))
			return null;

		int lColIndex = pCoord.x / mTileScaleX;
		int lRowIndex;

		if ((lColIndex & 0x01) == 0) // 짝수라면
			lRowIndex = pCoord.z / mTileScaleZ;
		else
			lRowIndex = (pCoord.z + mTileBlockScaleZ) / mTileScaleZ;

		return mTiles[lRowIndex, lColIndex];
	}
	//-------------------------------------------------------------------------------------------------------
	// 제한 한도 안에서 해당 좌표가 속한 타일을 얻습니다.
	public BattleGridTile GetClampedTile(Coord2 pCoord)
	{
		Coord2 lClampedCoord;
		if (pCoord.x < mMinCoordX)
			lClampedCoord.x = mMinCoordX;
		else if (pCoord.x > mMaxCoordX)
			lClampedCoord.x = mMaxCoordX;
		else
			lClampedCoord.x = pCoord.x;

		if (pCoord.z < mMinCoordZ)
			lClampedCoord.z = mMinCoordZ;
		else if (pCoord.z > mMaxCoordZ)
			lClampedCoord.z = mMaxCoordZ;
		else
			lClampedCoord.z = pCoord.z;

		int lColIndex = lClampedCoord.x / mTileScaleX;
		int lRowIndex;

		if ((lColIndex & 0x01) == 0) // 짝수라면
			lRowIndex = lClampedCoord.z / mTileScaleZ;
		else
			lRowIndex = (lClampedCoord.z + mTileBlockScaleZ) / mTileScaleZ;

		return mTiles[lRowIndex, lColIndex];
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀 인덱스를 구합니다.
	public RowCol GetCellIndex(Coord2 pCoord)
	{
		if ((pCoord.x < mMinCoordX) ||
			(pCoord.x > mMaxCoordX) ||
			(pCoord.z < mMinCoordZ) ||
			(pCoord.z > mMaxCoordZ))
			return new RowCol(-1, -1);

		int lColIndex = pCoord.x / mCellScaleX;
		int lRowIndex;

		if ((lColIndex & 0x01) == 0) // 짝수라면
			lRowIndex = pCoord.z / mCellScaleZ;
		else
			lRowIndex = (pCoord.z + mCellBlockScaleZ) / mCellScaleZ;

		return new RowCol(lRowIndex, lColIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표가 속한 셀을 얻습니다.
	public BattleGridCell GetCell(Coord2 pCoord)
	{
		if ((pCoord.x < mMinCoordX) ||
			(pCoord.x > mMaxCoordX) ||
			(pCoord.z < mMinCoordZ) ||
			(pCoord.z > mMaxCoordZ))
			return null;

		int lColIndex = pCoord.x / mCellScaleX;
		int lRowIndex;

		if ((lColIndex & 0x01) == 0) // 짝수라면
			lRowIndex = pCoord.z / mCellScaleZ;
		else
			lRowIndex = (pCoord.z + mCellBlockScaleZ) / mCellScaleZ;

		return mCells[lRowIndex, lColIndex];
	}
	//-------------------------------------------------------------------------------------------------------
	// 두 셀 사이의 거리를 칸수로 구합니다.
	public static int GetCellDistance(BattleGridCell pFromCell, BattleGridCell pToCell)
	{
		int lRowDistance = pToCell.aCellIndex.row - pFromCell.aCellIndex.row;
		int lColDistance = pToCell.aCellIndex.col - pFromCell.aCellIndex.col;
		if ((lColDistance & 0x01) != 0) // 가로로 홀수 거리라면
		{
			if ((pToCell.aCellIndex.col & 0x01) == 0)
			{
				if (pToCell.aCellIndex.row >= pFromCell.aCellIndex.row)
					++lRowDistance;
			}
			else
			{
				if (pToCell.aCellIndex.row <= pFromCell.aCellIndex.row)
					--lRowDistance;
			}
		}
		return Mathf.Max(Mathf.Abs(lRowDistance), Mathf.Abs(lColDistance));
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표의 셀과 주변 6개 셀에 있는 기즈모 가운데에서 터치를 처리할 수 있는 가장 가까이 있는 기즈모를 찾습니다.
	public BattleObject SearchNearestTouchObject(int pSearchingTeamIdx, Coord2 pCoord, Touch pGridTouch)
	{
		RowCol lFindCellIndex = GetCellIndex(pCoord);
		//Debug.Log("lFindCellIndex=" + lFindCellIndex);
		if (lFindCellIndex.row < 0)
		{
			//Debug.Log("out of area");
			return null;
		}

		int lSqrObjectDistance = int.MaxValue;
		BattleObject lNearestObject = null;

		BattleGridCell lFindCell = mCells[lFindCellIndex.row, lFindCellIndex.col];
		if (lFindCell != null)
			lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance);

		// 주변 여섯 셀에서도 찾습니다.
		if ((lFindCellIndex.col & 0x01) == 0) // 짝수열이라면
		{
			if (lFindCellIndex.col > 0)
			{
				lFindCell = mCells[lFindCellIndex.row, lFindCellIndex.col - 1];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 왼쪽 아래
				if (lFindCellIndex.row < (mCellRowCount - 1))
				{
					lFindCell = mCells[lFindCellIndex.row + 1, lFindCellIndex.col - 1];
					if (lFindCell != null)
						lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 왼쪽 위
				}
			}
			if (lFindCellIndex.row > 0)
			{
				lFindCell = mCells[lFindCellIndex.row - 1, lFindCellIndex.col];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 아래
			}
			if (lFindCellIndex.row < (mCellRowCount - 1))
			{
				lFindCell = mCells[lFindCellIndex.row + 1, lFindCellIndex.col];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 위
			}
			if (lFindCellIndex.col < (mCellColCount - 1))
			{
				lFindCell = mCells[lFindCellIndex.row, lFindCellIndex.col + 1];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 오른쪽 아래
				if (lFindCellIndex.row < (mCellRowCount - 1))
				{
					lFindCell = mCells[lFindCellIndex.row + 1, lFindCellIndex.col + 1];
					if (lFindCell != null)
						lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 오른쪽 위
				}
			}
		}
		else // 홀수열이라면
		{
			if (lFindCellIndex.col > 0)
			{
				if (lFindCellIndex.row > 0)
				{
					lFindCell = mCells[lFindCellIndex.row - 1, lFindCellIndex.col - 1];
					if (lFindCell != null)
						lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 왼쪽 아래
				}
				lFindCell = mCells[lFindCellIndex.row, lFindCellIndex.col - 1];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 왼쪽 위
			}
			if (lFindCellIndex.row > 0)
			{
				lFindCell = mCells[lFindCellIndex.row - 1, lFindCellIndex.col];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 아래
			}
			if (lFindCellIndex.row < (mCellRowCount - 1))
			{
				lFindCell = mCells[lFindCellIndex.row + 1, lFindCellIndex.col];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 위
			}
			if (lFindCellIndex.col < (mCellColCount - 1))
			{
				if (lFindCellIndex.row > 0)
				{
					lFindCell = mCells[lFindCellIndex.row - 1, lFindCellIndex.col + 1];
					if (lFindCell != null)
						lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 오른쪽 아래
				}
				lFindCell = mCells[lFindCellIndex.row, lFindCellIndex.col + 1];
				if (lFindCell != null)
					lFindCell.SearchNearestTouchObject(pSearchingTeamIdx, pCoord, pGridTouch, lNearestObject, lSqrObjectDistance, out lNearestObject, out lSqrObjectDistance); // 오른쪽 위
			}
		}

		return lNearestObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 지휘관를 찾습니다.
	public BattleCommander FindCommanderByGuid(uint pCommanderGuid)
	{
		for (int iChain = 0; iChain < mCommanderChains.Length; ++iChain)
		{
			using (GuidObjectChainEnumerator<BattleCommander> lCommanderEnumerator = mCommanderChains[iChain].CreateEnumerator())
			{
				while (lCommanderEnumerator.MoveNext())
				{
					BattleCommander lCommander = lCommanderEnumerator.GetCurrent();
					if (lCommander.aGuid == pCommanderGuid)
						return lCommander;
				}
			}
		}

		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 오브젝트를 찾습니다.
	public BattleHeadquarter FindHeadquarterByGuid(uint pHeadquarterGuid)
	{
		for (int iHeadquarter = 0; iHeadquarter < mHeadquarterList.Count; ++iHeadquarter)
			if (mHeadquarterList[iHeadquarter].aHeadquarterGuid == pHeadquarterGuid)
				return mHeadquarterList[iHeadquarter];

		Debug.LogError("can't find pHeadquarterGuid:" + pHeadquarterGuid);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 오브젝트를 찾습니다.
	public BattleCheckPoint FindCheckPointByGuid(uint pCheckPointGuid)
	{
		for (int iCheckPoint = 0; iCheckPoint < mCheckPointList.Count; ++iCheckPoint)
			if (mCheckPointList[iCheckPoint].aCheckPointGuid == pCheckPointGuid)
				return mCheckPointList[iCheckPoint];

		Debug.LogError("can't find pCheckPointGuid:" + pCheckPointGuid);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 거점의 지휘관을 찾습니다.
	public BattleCommander FindCheckPointCommanderByGuid(uint pCheckPointGuid)
	{
		BattleCheckPoint lCheckPoint = FindCheckPointByGuid(pCheckPointGuid);
		if (lCheckPoint == null)
			return null;

		int lCommanderGridIdx = GetCommanderGridIdx(null, lCheckPoint.aCommanderSlotIdx);
		return mCommanders[lCommanderGridIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 오브젝트를 찾습니다.
	public BattleEventPoint FindEventPointByGuid(uint pEventPointGuid)
	{
		for (int iEventPoint = 0; iEventPoint < mEventPointList.Count; ++iEventPoint)
			if ((mEventPointList[iEventPoint].aEventPointGuid == pEventPointGuid) &&
				mEventPointList[iEventPoint].aIsActive) // 같은 GUID를 가졌지만 활성화 안 된 이벤트가 있을 수 있으므로
				return mEventPointList[iEventPoint];

		Debug.LogError("can't find pEventPointGuid:" + pEventPointGuid);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 이벤트 지점의 지휘관을 찾습니다.
	public BattleCommander FindEventPointCommanderByGuid(uint pEventPointGuid)
	{
		BattleEventPoint lEventPoint = FindEventPointByGuid(pEventPointGuid);
		if (lEventPoint == null)
			return null;

		int lCommanderGridIdx = GetCommanderGridIdx(null, lEventPoint.aCommanderSlotIdx);
		return mCommanders[lCommanderGridIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 오브젝트를 찾습니다.
	public BattleBarricade FindBarricadeByGuid(uint pBarricadeGuid)
	{
		for (int iBarricade = 0; iBarricade < mBarricadeList.Count; ++iBarricade)
			if (mBarricadeList[iBarricade].aBarricadeGuid == pBarricadeGuid)
				return mBarricadeList[iBarricade];

		Debug.LogError("can't find pBarricadeGuid:" + pBarricadeGuid);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 GUID를 가진 부대를 찾습니다.
	public BattleTroop FindTroopByGuid(uint pTroopGuid)
	{
		for (int iChain = 0; iChain < mTroopChains.Length; ++iChain)
		{
			using (GuidObjectChainEnumerator<BattleTroop> lTroopEnumerator = mTroopChains[iChain].CreateEnumerator())
			{
				while (lTroopEnumerator.MoveNext())
				{
					BattleTroop lTroop = lTroopEnumerator.GetCurrent();
					if (lTroop.aGuid == pTroopGuid)
						return lTroop;
				}
			}
		}

		Debug.LogError("can't find pTroopGuid:" + pTroopGuid);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관 정보를 가지고 지휘관 등록 인덱스를 얻습니다.
	public int GetCommanderGridIdx(BattleUser pUser, int pCommanderSlotIdx)
	{
		int lCommanderId;
		if (pUser != null)
			lCommanderId  = pUser.aUserIdx * BattleUserInfo.cMaxCommanderCount;
		else
			lCommanderId = cMaxUserCommanderCount;
		lCommanderId += pCommanderSlotIdx;
		return lCommanderId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 부대 지휘관인지 여부를 얻습니다.
	public bool CheckUserCommanderGridIdx(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cUserCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cUserCommanderGridIdxEnd))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관에게 연결된 유저를 얻습니다.
	public BattleUser GetCommanderGridIdxUser(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cUserCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cUserCommanderGridIdxEnd))
		{
			int lUserIdx = (pCommanderGridIdx - cUserCommanderGridIdxStart) / BattleUserInfo.cMaxCommanderCount;
			return mUsers[lUserIdx];
		}
		return null; // 찾지 못함
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 부대 지휘관인지 여부를 얻습니다.
	public bool CheckCheckPointCommanderGridIdx(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cCheckPointCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cCheckPointCommanderGridIdxEnd))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관에게 연결된 거점을 얻습니다.
	public BattleCheckPoint GetCommanderGridIdxCheckPoint(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cCheckPointCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cCheckPointCommanderGridIdxEnd))
		{
			int lCheckPointIdx = pCommanderGridIdx - cCheckPointCommanderGridIdxStart;
			return mCheckPointList[lCheckPointIdx];
		}
		return null; // 찾지 못함
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 부대 지휘관인지 여부를 얻습니다.
	public bool CheckEventPointCommanderGridIdx(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cEventPointCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cEventPointCommanderGridIdxEnd))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관에게 연결된 이벤트 지점을 얻습니다.
	public BattleEventPoint GetCommanderGridIdxEventPoint(int pCommanderGridIdx)
	{
		if ((pCommanderGridIdx >= cEventPointCommanderGridIdxStart) &&
			(pCommanderGridIdx <= cEventPointCommanderGridIdxEnd))
		{
			int lEventPointIdx = pCommanderGridIdx - cEventPointCommanderGridIdxStart;
			return mEventPointList[lEventPointIdx];
		}
		return null; // 찾지 못함
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스와 GUID를 가진 지휘관을 얻습니다.
	public BattleCommander GetCommander(int pCommanderId, uint pCommanderGuid)
	{
		if ((mCommanders[pCommanderId] != null) &&
			(mCommanders[pCommanderId].aGuid == pCommanderGuid))
			return mCommanders[pCommanderId];

		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 격자 인덱스의 지휘관 아이템을 얻습니다.
	public bool GetUserCommanderItem(int pCommanderGridIdx, out BattleUser oUser, out CommanderItem oCommanderItem)
	{
		int lUserIdx = pCommanderGridIdx / BattleUserInfo.cMaxCommanderCount;

		if ((pCommanderGridIdx >= BattleGrid.cMaxUserCommanderCount) ||
			(mUsers[lUserIdx] == null))
		{
			oUser = null;
			oCommanderItem = null;
			return false;
		}

		int lCommanderSlotIdx = pCommanderGridIdx % BattleUserInfo.cMaxCommanderCount;

		oUser = mUsers[lUserIdx];
		oCommanderItem = oUser.aUserInfo.mCommanderItems[lCommanderSlotIdx];
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 스크립트 유저 번호가 일반 유저인지를 검사합니다.
	public bool CheckScriptUser(int pScriptUserId)
	{
		return ((pScriptUserId >= 0) && (pScriptUserId < BattleMatchInfo.cMaxUserCount));
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 스크립트 유저 번호가 거점 부대의 가상 유저인지를 검사합니다.
	public bool CheckScriptCheckPoint(int pScriptUserId)
	{
		return (pScriptUserId == BattleCommander.cCheckPointUserId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 스크립트 유저 번호가 이벤트 부대의 가상 유저인지를 검사합니다.
	public bool CheckScriptEventPoint(int pScriptUserId)
	{
		return (pScriptUserId == BattleCommander.cEventPointUserId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 스크립트 유저 번호에 해당하는 유저를 얻습니다.
	public BattleUser GetScriptUser(int pScriptUserId)
	{
		if (pScriptUserId < BattleMatchInfo.cMaxUserCount)
			return mUsers[pScriptUserId];
		else
			return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 지휘관의 경우에 해당 스크립트 슬롯 번호에 해당하는 지휘관 슬롯 번호를 얻습니다.
	public int GetScriptUserCommanderSlotIdx(int pScriptSlotId)
	{
		return pScriptSlotId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 지휘관의 경우에 해당 스크립트 슬롯 번호에 해당하는 거점 고유 번호를 얻습니다.
	public uint GetScriptCheckPointGuid(int pScriptSlotId)
	{
		return (uint)pScriptSlotId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 지휘관의 경우에 해당 스크립트 슬롯 번호에 해당하는 거점 고유 번호를 얻습니다.
	public uint GetScriptEventPointGuid(int pScriptSlotId)
	{
		return (uint)pScriptSlotId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관의 부대에 대한 등록 인덱스를 얻습니다.
	public int GetCommanderTroopGridIdx(BattleCommander pCommander, int pTroopSlotIdx)
	{
		int lTroopId;
		if (pCommander.aUser != null)
			lTroopId = pCommander.aUser.aUserIdx * (BattleUserInfo.cMaxCommanderCount * CommanderTroopSet.cMaxTroopCount);
		else
			lTroopId = cMaxUserTroopCount;
		lTroopId += pCommander.aCommanderSlotIdx * CommanderTroopSet.cMaxTroopCount + pTroopSlotIdx;
		return lTroopId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스와 GUID를 가진 부대를 얻습니다.
	public BattleTroop GetTroop(int pTroopId, uint pTroopGuid)
	{
		if ((mTroops[pTroopId] != null) &&
			(mTroops[pTroopId].aGuid == pTroopGuid))
			return mTroops[pTroopId];

		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표를 중심으로 일정 거리 안에 있는 부대에 대해서 처리 함수를 실행합니다.
	public void CallTroopHandler(Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, TeamSelection pTeamSelection, OnObjectHandlingDelegate<BattleTroop> pOnObjectHandlingDelegate)
	{
		if (pSearchRange < mCellCheckRange) // 근접 셀 범위 미만이라면
		{
			mStatistics.IncrementCellCheckCount();

			// 셀을 중심으로한 탐색을 합니다.
			BattleGridCell lGridCell = GetCell(pSearchCoord);
			if (lGridCell == null)
				return;
			int lCheckCellDistance = 1 + (pSearchRange + BattleConfig.get.mMaxUnitRadius) / mCellDiameter;

			lGridCell.CallTroopHandler(lCheckCellDistance, pSearchCoord, pSearchRange, pSearchingTeamIdx, pTeamSelection, pOnObjectHandlingDelegate);
		}
		else // 근접 셀 범위를 벗어난 넓은 탐색이라면
		{
			mStatistics.IncrementChainCheckCount();

			// 전체 영역에 대해서 검사합니다.
			for (int iTeam = 0; iTeam < mTroopChains.Length; ++iTeam)
			{
				if (!CheckTeamSelection(pSearchingTeamIdx, iTeam, pTeamSelection))
					continue;

				using (GuidObjectChainEnumerator<BattleTroop> lTroopEnumerator = mTroopChains[iTeam].CreateEnumerator())
				{
					while (lTroopEnumerator.MoveNext())
					{
						BattleTroop lTroop = lTroopEnumerator.GetCurrent();

						int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
						int lSqrCheckRange = lCheckRange * lCheckRange;
						int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

						if (lSqrTargetDistance <= lSqrCheckRange)
							pOnObjectHandlingDelegate(lTroop, lSqrTargetDistance);
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 주어진 비교자를 가지고 제일 적합한 부대를 찾습니다.
	public BattleTroop SearchTroop(Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, TeamSelection pTeamSelection, ISearchComparator<BattleTroop> pComparator)
	{
		if (pSearchRange < mCellCheckRange) // 근접 셀 범위 미만이라면
		{
			mStatistics.IncrementCellCheckCount();

			// 셀을 중심으로한 탐색을 합니다.
			BattleGridCell lGridCell = GetCell(pSearchCoord);
			int lCheckCellDistance = 1 + (pSearchRange + BattleConfig.get.mMaxUnitRadius) / mCellDiameter;

			return lGridCell.SearchTroop(lCheckCellDistance, pSearchCoord, pSearchRange, pSearchingTeamIdx, pTeamSelection, pComparator);
		}
		else // 근접 셀 범위를 벗어난 넓은 탐색이라면
		{
			mStatistics.IncrementChainCheckCount();

			// 전체 영역에 대해서 검사합니다.
			for (int iTeam = 0; iTeam < mTroopChains.Length; ++iTeam)
			{
				if (!CheckTeamSelection(pSearchingTeamIdx, iTeam, pTeamSelection))
					continue;

				using (GuidObjectChainEnumerator<BattleTroop> lTroopEnumerator = mTroopChains[iTeam].CreateEnumerator())
				{
					while (lTroopEnumerator.MoveNext())
					{
						BattleTroop lTroop = lTroopEnumerator.GetCurrent();

						int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
						int lSqrCheckRange = lCheckRange * lCheckRange;
						int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

						if (lSqrTargetDistance <= lSqrCheckRange)
						{
							if ((pSearchingTeamIdx >= 0) &&
								!lTroop.aIsDetecteds[pSearchingTeamIdx])
								continue; // 탐지 안 된 부대는 건너뜀
							if (!pComparator.Compare(lTroop, lSqrTargetDistance))
								return pComparator.aResult; // 더이상 찾지 않고 현재 결과를 반환
						}
					}
				}
			}
		}

		return pComparator.aResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까운 부대를 찾습니다.
	public BattleTroop SearchNearestTroop(Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, TeamSelection pTeamSelection)
	{
		int lSqrNearestTroopDistance = int.MaxValue;
		BattleTroop lNearestTroop = null;

		if (pSearchRange < mCellCheckRange) // 근접 셀 범위 미만이라면
		{
			mStatistics.IncrementCellCheckCount();

			// 셀을 중심으로한 탐색을 합니다.
			BattleGridCell lGridCell = GetCell(pSearchCoord);
			int lCheckCellDistance = 1 + (pSearchRange + BattleConfig.get.mMaxUnitRadius) / mCellDiameter;

			return lGridCell.SearchNearestTroop(lCheckCellDistance, pSearchCoord, pSearchRange, pSearchingTeamIdx, pTeamSelection);
		}
		else // 근접 셀 범위를 벗어난 넓은 탐색이라면
		{
			mStatistics.IncrementChainCheckCount();

			// 전체 영역에 대해서 검사합니다.
			for (int iTeam = 0; iTeam < mTroopChains.Length; ++iTeam)
			{
				if (!CheckTeamSelection(pSearchingTeamIdx, iTeam, pTeamSelection))
					continue;

				using (GuidObjectChainEnumerator<BattleTroop> lTroopEnumerator = mTroopChains[iTeam].CreateEnumerator())
				{
					while (lTroopEnumerator.MoveNext())
					{
						BattleTroop lTroop = lTroopEnumerator.GetCurrent();

						int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
						int lSqrCheckRange = lCheckRange * lCheckRange;
						int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

						if (lSqrTargetDistance <= lSqrCheckRange)
						{
							if ((pSearchingTeamIdx >= 0) &&
								!lTroop.aIsDetecteds[pSearchingTeamIdx])
								continue; // 탐지 안 된 부대는 건너뜀
							if (lSqrNearestTroopDistance > lSqrTargetDistance)
							{
								lSqrNearestTroopDistance = lSqrTargetDistance;
								lNearestTroop = lTroop;
							}
						}
					}
				}
			}
		}

		return lNearestTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까운 타게팅 더미를 찾습니다.
	public BattleTargetingDummy SearchNearestTargetingDummy(Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx)
	{
		int lSqrNearestTargetingDummyDistance = int.MaxValue;
		BattleTargetingDummy lNearestTargetingDummy = null;

		mStatistics.IncrementChainCheckCount();

		// 전체 영역에 대해서 검사합니다.
		int lSqrCheckRange = pSearchRange * pSearchRange;
		for (int iTeam = 0; iTeam < mTargetingDummyChains.Length; ++iTeam)
		{
			if (!CheckTeamSelection(pSearchingTeamIdx, iTeam, TeamSelection.NonAllyTeam))
				continue;

			using (GuidObjectChainEnumerator<BattleTargetingDummy> lTargetingDummyEnumerator = mTargetingDummyChains[iTeam].CreateEnumerator())
			{
				while (lTargetingDummyEnumerator.MoveNext())
				{
					BattleTargetingDummy lTargetingDummy = lTargetingDummyEnumerator.GetCurrent();

					int lSqrTargetDistance = (lTargetingDummy.aCoord - pSearchCoord).SqrMagnitude();
					if (lSqrTargetDistance <= lSqrCheckRange)
					{
						if ((pSearchingTeamIdx >= 0) &&
							lTargetingDummy.aIsDetecteds[pSearchingTeamIdx]) // 탐지가 된 더미는 파악된 것이므로 오히려 피함                     
							continue; // 탐지 안 된 타게팅 더미는 건너뜀
						if (lSqrNearestTargetingDummyDistance > lSqrTargetDistance)
						{
							lSqrNearestTargetingDummyDistance = lSqrTargetDistance;
							lNearestTargetingDummy = lTargetingDummy;
						}
					}
				}
			}
		}

		return lNearestTargetingDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택하는 팀 입장에서 검사하는 팀이 팀 선택 옵션에 해당하는지를 얻습니다.
	public static bool CheckTeamSelection(int pSelectingTeamIdx, int pCheckedTeamIdx, TeamSelection pTeamSelection)
	{
		switch (pTeamSelection)
		{
		case TeamSelection.NonAllyTeam:
			{
				if (pCheckedTeamIdx != pSelectingTeamIdx)
					return true;
			}
			break;
		case TeamSelection.AllyTeam:
			{
				if (pCheckedTeamIdx == pSelectingTeamIdx)
					return true;
			}
			break;
		case TeamSelection.NonNeutralTeam:
			{
				if (pCheckedTeamIdx > 0)
					return true;
			}
			break;
		case TeamSelection.EnemyTeam:
			{
				if ((pCheckedTeamIdx != pSelectingTeamIdx) &&
					(pCheckedTeamIdx > 0))
					return true;
			}
			break;
		case TeamSelection.NonEnemyTeam:
			{
				if ((pCheckedTeamIdx == pSelectingTeamIdx) ||
					(pCheckedTeamIdx <= 0))
					return true;
			}
			break;
		case TeamSelection.NeutralTeam:
			{
				if (pCheckedTeamIdx == 0)
					return true;
			}
			break;
		default:
			Debug.LogError("not implemented in case:" + pTeamSelection);
			break;
		}
		return false; 
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 스폰합니다.
	public BattleCommander SpawnCommander(BattleUser pUser, CommanderItem pCommanderItem, Coord2 pSpawnCoord, int pSpawnDirectionY, BattleUser pTeamUser)
	{
		if (pUser != null)
		{
			if (!pUser.CheckSpawnable(pCommanderItem))
				return null; // 스폰 불가

			// MP를 소모합니다.
			mProgressInfo.ChangeMp(pUser.aUserIdx, -pCommanderItem.aSpawnMp); // MP 검사는 CheckSpawnable()에서 했음
		}

		// 지휘관 슬롯 인덱스를 얻습니다.
		int lCommanderSlotIdx;
		//if (pUser == null)
		//	lCommanderSlotIdx = pCommanderItem.mSlotIdx;
		//else
		//	lCommanderSlotIdx = pUser.aUserInfo.GetSpawnCommanderSlotIdx();

		// 지휘관 슬롯과 아이템 슬롯의 위치는 동일시 됩니다.
		lCommanderSlotIdx = pCommanderItem.mSlotIdx;

		// 스폰 방식을 정합니다.
		Coord2 lCreationCoord;
		int lCreationDirectionY;

		switch (pCommanderItem.mCommanderSpec.mSpawnType)
		{
		case CommanderSpawnType.Headquarter:
			{
				if (pUser != null)
				{
					lCreationCoord = pUser.aHeadquarter.aSpawnCoord;
					lCreationDirectionY = pUser.aHeadquarter.aDirectionY;
				}
				else // 유저가 없다면 스폰 좌표 그대로 사용
				{
					lCreationCoord = pSpawnCoord;
					lCreationDirectionY = pSpawnDirectionY;
				}
			}
			break;
		case CommanderSpawnType.Defense:
        case CommanderSpawnType.Aircraft:
        case CommanderSpawnType.TestTool:
		default:
			{
				lCreationCoord = pSpawnCoord;
				lCreationDirectionY = pSpawnDirectionY;
			}
			break;
		}

		// 지휘관을 생성합니다.
		BattleCommander lCommander = mBattleLogic.CreateCommander(
			this,
			pUser,
			pCommanderItem,
			lCommanderSlotIdx,
			lCreationCoord,
			lCreationDirectionY,
			pTeamUser);

		// 지휘관 아이템의 상태를 갱신합니다.
		pCommanderItem.OnSpawnCommander();

		// 지휘관 생성과 함께 부대도 스폰합니다.
		lCommander.SpawnTroops(lCommander.aCoord, lCommander.aDirectionY);

		// 최소 소환 mp정보를 갱신합니다.
		if (pUser != null)
			pUser.RefreshSpawnInfo();

		// 추가 이동을 합니다.
		switch (pCommanderItem.mCommanderSpec.mSpawnType)
		{
		case CommanderSpawnType.Headquarter:
			{
				lCommander.MoveTroops(pSpawnCoord, pSpawnDirectionY); // 스폰 지점으로 추가 이동합니다.
			}
			break;
		}

		// 콜백을 호출합니다.
		if (mOnSpawnCommanderDelegate != null)
			mOnSpawnCommanderDelegate(lCommander);

		return lCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 발사 이펙트를 스폰합니다.
	public BattleFireEffect SpawnFireEffect(BattleTroopTargetingInfo pTroopTargetingInfo)
	{
		BattleFireEffect lFireEffect = mBattleLogic.CreateFireEffect(pTroopTargetingInfo);
		mEffectList.AddLast(lFireEffect.aGridNode);

		return lFireEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 관찰자 더미를 스폰합니다.
	public BattleObserverDummy SpawnObserverDummy(Coord2 pCoord, int pDirectionY, int pDetectionRange, int pDetectingTeamIdx)
	{
		BattleObserverDummy lObserverDummy = mBattleLogic.CreateObserverDummy(
			this,
			pCoord,
			pDirectionY,
			pDetectingTeamIdx,
			pDetectionRange,
			BattleConfig.get.mAttackDetectionDelay);
		mObserverDummyList.AddLast(lObserverDummy.aGridNode);

		return lObserverDummy;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 헤더를 로딩합니다.
	private void _LoadHeader(CSVObject pLayoutCSVObject)
	{
		// 기본 초기화를 합니다.
		mLayoutHeader.mAStarHeuristic = cDefaultAStarHeuristic;

		// 헤더에서 읽어옵니다.
		String lHeaderDesc = pLayoutCSVObject.GetHeader(0); // 헤더(첫 줄)의 0번 컬럼에 레이아웃 정보를 담고 있습니다.
		String[] lHeaderAttributeDescs = lHeaderDesc.Split(BattleGridTile.cLineSplitChar);

		for (int iAttribute = 0; iAttribute < lHeaderAttributeDescs.Length; ++iAttribute)
		{
			if (lHeaderAttributeDescs[iAttribute].Length <= 1) // 기존에 '0'으로 입력했던 컬럼 허용
				continue;

			String[] lHeaderTokens = lHeaderAttributeDescs[iAttribute].Split(BattleGridTile.cAttributeTokenSplitChar);
			String lHeaderAttributeToken = lHeaderTokens[0];

			if (String.Compare(lHeaderAttributeToken, LayoutHeader.Attribute.AStarHeuristic.ToString()) == 0)
			{
				if (!int.TryParse(lHeaderTokens[1], out mLayoutHeader.mAStarHeuristic))
					Debug.LogError("AStarHeuristic parsing error - " + lHeaderDesc);
			}
			else
				Debug.LogError("invalid lHeaderAttributeToken - " + lHeaderAttributeToken);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이아웃을 로딩합니다.
	private void _LoadLayout(CSVObject pLayoutCSVObject, Coord2 pTileBlockScale, Coord2 pCellBlockScale)
	{
		mTileBlockScaleX = pTileBlockScale.x;
		mTileBlockScaleZ = pTileBlockScale.z;
		mTileScaleX = mTileBlockScaleX * 2;
		mTileScaleZ = mTileBlockScaleZ * 2;
		mTileScaleMin = Mathf.Min(mTileScaleX, mTileScaleZ);
		mTileScaleMax = Mathf.Max(mTileScaleX, mTileScaleZ);
		mTileDiagonalScale = (int)MathUtil.RoughSqrt(mTileScaleX * mTileScaleX + mTileScaleZ * mTileScaleZ);
		mTileHalfDiagonalScale = mTileDiagonalScale / 2;

		mTileBlockRowCount = pLayoutCSVObject.RowNum;
		mTileBlockColCount = pLayoutCSVObject.ColNum;
		//Debug.Log("mTileBlockRowCount=" + mTileBlockRowCount + " mTileBlockColCount=" + mTileBlockColCount);
		if ((mTileBlockRowCount % 2) == 0)
			Debug.LogError(String.Format("mTileBlockRowCount({0}) is not odd number - {1}", mTileBlockRowCount, this));
		if ((mTileBlockColCount % 2) != 0)
			Debug.LogError(String.Format("mTileBlockColCount({0}) is not even number - {1}", mTileBlockColCount, this));

		mMinCoordX = 0;
		mMaxCoordX = mTileBlockColCount * mTileBlockScaleX - 1; // [mMinCoordX, mMaxCoordX] 포함 범위
		mMinCoordZ = 0;
		mMaxCoordZ = mTileBlockRowCount * mTileBlockScaleZ - 1; // [mMinCoordZ, mMaxCoordZ] 포함 범위
		mCenter = new Coord2((mMinCoordX + mMaxCoordX) / 2, (mMinCoordZ + mMaxCoordZ) / 2);
		mMaxSearchRange = Mathf.Max(mMaxCoordX, mMaxCoordZ);

		int lOutsideCheckRange = (BattleConfig.get.mEventPointCheckRange - mTileScaleMin / 2);
		if (lOutsideCheckRange > 0)
			mEventCheckTileDistance = Mathf.Min(1 + lOutsideCheckRange / mTileScaleMin, 2);
		else
			mEventCheckTileDistance = 0;

		mTileRowCount = (mTileBlockRowCount + 1) / 2;
		mTileColCount = mTileBlockColCount / 2;

		mTiles = new BattleGridTile[mTileRowCount, mTileColCount];
		for (int iRow = 0; iRow < mTileRowCount; ++iRow)
			for (int iCol = 0; iCol < mTileColCount; ++iCol)
				mTiles[iRow, iCol] = new BattleGridTile(this, iRow, iCol);

		// 이를 기반으로 셀 정보를 구축합니다.
		_ConstructCells(pCellBlockScale);

		// 타일 속성을 읽어서 초기화를 합니다.
		for (int iRow = 0; iRow < mTileBlockRowCount; ++iRow)
		{
			for (int iCol = 0; iCol < mTileBlockColCount; ++iCol)
			{
				String lTileDesc = pLayoutCSVObject.Get(iRow, iCol);
				//Debug.Log(String.Format("[{0}{1}] lTileDesc={2}", iRow, iCol, lTileDesc));

				if ((lTileDesc != null) &&
					(lTileDesc.Length > 1))
				{
					int lTileBlockRow = mTileBlockRowCount - 1 - iRow; // lTileBlockRow는 테이블에서 밑에서부터 0이라서 반전시킴
					int lTileBlockCol = iCol; // lTileBlockCol은 왼쪽에서부터 0이므로 iCol과 같음

					//Debug.Log(String.Format("[{0},{1}] lTileDesc={2}", lTileBlockRow, lTileBlockCol, lTileDesc));
					int lTileCol = lTileBlockCol / 2;
					int lTileRow;
					if ((lTileCol & 0x01) == 0) // 짝수 컬럼의 타일이라면
						lTileRow = lTileBlockRow / 2; // 행은 2로 나눈 값 그대로
					else // 홀수 컬럼의 타일이라면
						lTileRow = (lTileBlockRow + 1) / 2; // 한 블록 아래로 내려가므로 1을 더하고서 2로 나누어야 행이 됨

					BattleGridTile lTile = mTiles[lTileRow, lTileCol];
					String[] lObjectDescs = lTileDesc.Split(BattleGridTile.cLineSplitChar);
					String[] lTileParamTokens = null;
					for (int iObject = 0; iObject < lObjectDescs.Length; ++iObject) // 타일 정보를 한 줄씩 읽는데...
					{
						String[] lTileTokens = lObjectDescs[iObject].Split(BattleGridTile.cAttributeTokenSplitChar);
						BattleGridTileAttributeType lTileAttributeType = BattleGridTile.ParseAttributeType(lTileTokens[0]); // String lTileAttributeToken = lTileTokens[0];
						if (lTileAttributeType != BattleGridTileAttributeType.Param)
							_LoadTileInfo(lTile, lTileAttributeType, lTileTokens); // Param을 제외한 나머지는 바로 읽고
						else
							lTileParamTokens = lTileTokens; // Param 정보는 기억해 두었다가
					}
					if (lTileParamTokens != null)
						_LoadTileInfo(lTile, BattleGridTileAttributeType.Param, lTileParamTokens); // 맨 나중에 읽습니다(타일의 방향각 등의 정보가 초기화 된 이후에 HQ 등을 생성하기 위함).
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 정보를 로딩합니다.
	private void _LoadTileInfo(BattleGridTile pTile, BattleGridTileAttributeType pTileAttributeType, String[] pTileTokens)
	{
		switch (pTileAttributeType)
		{
		case BattleGridTileAttributeType.Tile:
			{
				pTile.SetTileType(BattleGridTile.ParseTileType(pTileTokens[1])); // 두 번째 토큰에서 타일 타입을 얻어옵니다.
			}
			break;
		case BattleGridTileAttributeType.Ground:
			{
				pTile.InitGroundSpec(GroundTable.get.FindGroundSpec(pTileTokens[1])); // 두 번째 토큰에서 지형 타입 스펙을 얻어옵니다.
			}
			break;
		case BattleGridTileAttributeType.Weight:
			{
				pTile.aWeight = int.Parse(pTileTokens[1]); // 두 번째 토큰에서 웨이트를 얻어옵니다.
			}
			break;
		case BattleGridTileAttributeType.DirectionY:
			{
				pTile.aDirectionY = int.Parse(pTileTokens[1]); // 두 번째 토큰에서 방향각을 얻어옵니다.
			}
			break;
		case BattleGridTileAttributeType.Param:
			{
				TileParam lTileParam = new TileParam();
				lTileParam.mTile = pTile;
				lTileParam.mTileToken = pTileTokens[1];
				lTileParam.mParamTokens = pTileTokens[1].Split(BattleGridTile.cParamTokenSplitChar);
				mTileParamList.Add(lTileParam);
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 오브젝트를 생성합니다.
	private void _CreateTileObjects()
	{
		for (int iParam = 0; iParam < mTileParamList.Count; ++iParam)
		{
			TileParam lTileParam = mTileParamList[iParam];

			switch (lTileParam.mTile.aTileType)
			{
			case BattleGridTileType.CheckPoint:
				{
					//Debug.Log("CheckPoint pTileDesc=" + pTileDesc);

					uint lCheckPointGuid = uint.Parse(lTileParam.mParamTokens[BattleGridTile.cParam1Index]);
					//int lMilliMpGeneration = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam2Index]);
					BattleCheckPoint.CheckPointType lCheckPointType = (BattleCheckPoint.CheckPointType)int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam2Index]);
					int lUserIdx = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam3Index]);
					int lStarId = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam4Index]);
					String[] lDefenderCommanders = new String[(int)DeckNation.Count];
					lDefenderCommanders[(int)DeckNation.None] = lTileParam.mParamTokens[BattleGridTile.cExtraParamIndex + 0];
					lDefenderCommanders[(int)DeckNation.US] = lTileParam.mParamTokens[BattleGridTile.cExtraParamIndex + 1];
					lDefenderCommanders[(int)DeckNation.GE] = lTileParam.mParamTokens[BattleGridTile.cExtraParamIndex + 2];
					lDefenderCommanders[(int)DeckNation.USSR] = lTileParam.mParamTokens[BattleGridTile.cExtraParamIndex + 3];

					// 임시 변수
					int lMilliMpGeneration
						= (lCheckPointType == BattleCheckPoint.CheckPointType.MpGeneration)
						? BattleConfig.get.mCheckPointMpMilliGeneration : 0;
					int lMilliEnemyVpDecrease = 0;

					if (lStarId >= 0) // 본부 거점은 -1인데 이를 제외하기 위함
					{
						if (((lStarId > 0) && (lMilliEnemyVpDecrease <= 0)) ||
							((lStarId <= 0) && (lMilliEnemyVpDecrease > 0)))
							Debug.LogError("mis-matching VP and Star value - " + lTileParam.mTileToken);
					}

					_CreateCheckPoint(
						lTileParam.mTile,
						lCheckPointGuid,
						lMilliMpGeneration,
						lMilliEnemyVpDecrease,
						lStarId,
						lCheckPointType,
						lDefenderCommanders,
						lUserIdx);
				}
				break;
			case BattleGridTileType.EventPoint:
				{
					//Debug.Log("EventPoint pTileDesc=" + pTileDesc);

					uint lEventPointGuid = uint.Parse(lTileParam.mParamTokens[BattleGridTile.cParam1Index]);
					int lEventStartDelay = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam2Index]);
					int lEventRepeatCount = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam3Index]);
					int lEventRepeatDelay = int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam4Index]);

					int lEventCount = lTileParam.mParamTokens.Length - BattleGridTile.cExtraParamIndex;
					String[] lEventNames = new String[lEventCount];
					for (int iEvent = 0; iEvent < lEventCount; ++iEvent)
					{
						lEventNames[iEvent] = lTileParam.mParamTokens[BattleGridTile.cExtraParamIndex + iEvent];
						if (String.IsNullOrEmpty(lEventNames[iEvent]))
							Debug.LogError("invalid event names=" + lEventNames + " pTileDesc=" + lTileParam.mTileToken + " - EventPoint[Guid:" + lEventPointGuid + "]");
					}

					BattleEventPoint lCreatedEventPoint = _CreateEventPoint(
						lTileParam.mTile,
						lEventPointGuid,
						lEventStartDelay,
						lEventRepeatCount,
						lEventRepeatDelay,
						lEventNames);

					lTileParam.mTile.SetLinkEventPoint(lCreatedEventPoint);
				}
				break;
			case BattleGridTileType.Headquarter:
				{
					//Debug.Log("Headquarter pTileDesc=" + pTileDesc);

					uint lHeadquarterGuid = uint.Parse(lTileParam.mParamTokens[BattleGridTile.cParam1Index]);
					uint lLinkCheckPointGuid = uint.Parse(lTileParam.mParamTokens[BattleGridTile.cParam2Index]);
					Coord2 lSpawnCoord = new Coord2(int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam3Index]), int.Parse(lTileParam.mParamTokens[BattleGridTile.cParam4Index]));

					_CreateHeadquarter(
						lTileParam.mTile,
						lHeadquarterGuid,
						lLinkCheckPointGuid,
						lSpawnCoord);
				}
				break;
			}
		}

		// 타일 오브젝트가 생성된 이후의 추가 초기화를 합니다.
		for (int iHeadquarter = 0; iHeadquarter < mHeadquarterLinkCheckPointGuidList.Count; ++iHeadquarter)
			mHeadquarterList[iHeadquarter].aLinkCheckPoint = FindCheckPointByGuid(mHeadquarterLinkCheckPointGuidList[iHeadquarter]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 오브젝트를 삭제합니다.
	private void _DestroyTileObjects()
	{
		for (int iHeadquarter = 0; iHeadquarter < mHeadquarterList.Count; ++iHeadquarter)
			mHeadquarterList[iHeadquarter].Destroy();
		mHeadquarterList.Clear();

		for (int iCheckPoint = 0; iCheckPoint < mCheckPointList.Count; ++iCheckPoint)
			mCheckPointList[iCheckPoint].Destroy();
		mCheckPointList.Clear();

		for (int iEventPoint = 0; iEventPoint < mEventPointList.Count; ++iEventPoint)
			mEventPointList[iEventPoint].Destroy();
		mEventPointList.Clear();

		mHeadquarterLinkCheckPointGuidList.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀 정보를 구축합니다.
	private void _ConstructCells(Coord2 pCellBlockScale)
	{
		mCellBlockScaleX = pCellBlockScale.x;
		mCellBlockScaleZ = pCellBlockScale.z;
		mCellScaleX = mCellBlockScaleX * 2;
		mCellScaleZ = mCellBlockScaleZ * 2;
		mCellDiameter = Mathf.Min(mCellScaleX, mCellScaleZ);
		mCellCheckRange = BattleGridCell.cNearCellMaxDistance * mCellDiameter - BattleConfig.get.mMaxUnitRadius;

		mCellBlockRowCount = 1 + mMaxCoordZ / mCellBlockScaleZ;
		mCellBlockColCount = 1 + mMaxCoordX / mCellBlockScaleX;
		//Debug.Log("mCellBlockRowCount=" + mCellBlockRowCount + " mCellBlockColCount=" + mCellBlockColCount);
		if ((mCellBlockRowCount % 2) == 0)
			++mCellBlockRowCount; // 홀수로 맞춥니다.
		if ((mCellBlockColCount % 2) != 0)
			++mCellBlockColCount; // 짝수로 맞춥니다.

		mCellRowCount = (mCellBlockRowCount + 1) / 2;
		mCellColCount = mCellBlockColCount / 2;

		mCells = new BattleGridCell[mCellRowCount, mCellColCount];
		for (int iRow = 0; iRow < mCellRowCount; ++iRow)
			for (int iCol = 0; iCol < mCellColCount; ++iCol)
				mCells[iRow, iCol] = new BattleGridCell(this, iRow, iCol);

		for (int iRow = 0; iRow < mCellRowCount; ++iRow)
			for (int iCol = 0; iCol < mCellColCount; ++iCol)
				mCells[iRow, iCol].SetUpLink();
	}
	//-------------------------------------------------------------------------------------------------------
	// 본부를 생성합니다.
	private void _CreateHeadquarter(
		BattleGridTile pGridTile,
		uint pHeadquarterGuid,
		uint pLinkCheckPointGuid,
		Coord2 pSpawnCoord)
	{
		// 거점을 생성해서 테이블에 등록합니다.
		BattleHeadquarter lCreatedHeadquarter = mBattleLogic.CreateHeadquarter(
			pGridTile,
			mHeadquarterList.Count,
			pHeadquarterGuid,
			pSpawnCoord);

		mHeadquarterList.Add(lCreatedHeadquarter);
		mHeadquarterLinkCheckPointGuidList.Add(pLinkCheckPointGuid);
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점을 생성합니다.
	private void _CreateCheckPoint(
		BattleGridTile pGridTile,
		uint pCheckPointGuid,
		int pMilliMpGeneration,
		int pMilliEnemyVpDecrease,
		int pStarId,
		BattleCheckPoint.CheckPointType pCheckPointType,
		String[] pDefenderCommanders,
		int pUserIdx)
	{
		BattleCheckPoint lCreatedCheckPoint = mBattleLogic.CreateCheckPoint(
			pGridTile,
			mCheckPointList.Count,
			pCheckPointGuid,
			pMilliMpGeneration,
			pMilliEnemyVpDecrease,
			pStarId,
			pCheckPointType,
			pDefenderCommanders,
			pUserIdx);

		mCheckPointList.Add(lCreatedCheckPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트를 생성합니다.
	private BattleEventPoint _CreateEventPoint(
		BattleGridTile pGridTile,
		uint pEventPointGuid,
		int pEventStartDelay,
		int pEventRepeatCount,
		int pEventRepeatDelay,
		String[] pEventNames)
	{
		BattleEventPoint lCreatedEventPoint = mBattleLogic.CreateEventPoint(
			pGridTile,
			mEventPointList.Count,
			pEventPointGuid,
			pEventStartDelay,
			pEventRepeatCount,
			pEventRepeatDelay,
			pEventNames);

		mEventPointList.Add(lCreatedEventPoint);

		return lCreatedEventPoint;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;
	private IStage mStage;

	private FrameUpdater mFrameUpdater;

	private String mLayoutName;
	private LayoutHeader mLayoutHeader;

	private int mTileBlockScaleX;
	private int mTileBlockScaleZ;
	private int mTileBlockRowCount;
	private int mTileBlockColCount;
	private int mTileScaleX;
	private int mTileScaleZ;
	private int mTileScaleMin;
	private int mTileScaleMax;
	private int mTileDiagonalScale;
	private int mTileHalfDiagonalScale;
	private int mTileRowCount;
	private int mTileColCount;
	private BattleGridTile[,] mTiles;

	private int mCellBlockScaleX;
	private int mCellBlockScaleZ;
	private int mCellBlockRowCount;
	private int mCellBlockColCount;
	private int mCellScaleX;
	private int mCellScaleZ;
	private int mCellDiameter;
	private int mCellCheckRange;
	private int mCellRowCount;
	private int mCellColCount;
	private BattleGridCell[,] mCells;

	private int mMinCoordX;
	private int mMaxCoordX;
	private int mMinCoordZ;
	private int mMaxCoordZ;
	private Coord2 mCenter;
	private int mMaxSearchRange;
	private int mEventCheckTileDistance;

	private List<TileParam> mTileParamList;
	private List<BattleHeadquarter> mHeadquarterList;
	private List<BattleCheckPoint> mCheckPointList;
	private List<BattleEventPoint> mEventPointList;
	private int[] mSameGuidEventPointIdxs;
	private List<BattleBarricade> mBarricadeList;
	private List<uint> mHeadquarterLinkCheckPointGuidList;

	private BattleUser[] mUsers;
	private int mMaxUserIdx;
	private BattleUser[][] mTeamUsers;
	private int[] mTeamUserCounts;
	private BattleCommander[] mCommanders;
	private BattleTroop[] mTroops;
	private LinkedList<BattleEffect> mEffectList;
	private LinkedList<BattleObserverDummy> mObserverDummyList;
	private GuidObjectChain<BattleCommander>[] mCommanderChains;
	private GuidObjectChain<BattleTroop>[] mTroopChains;
	private GuidObjectChain<BattleTargetingDummy>[] mTargetingDummyChains;
	private GuidObjectChain<BattleObject>[] mDetectionObjectChains;
	private bool[] mIsAllDetectables;
	private bool[] mIsNoneDetectables;

	private BattleGridPathFinder mPathFinder;
	private BattleGridStatistics mStatistics;

	private BattleMatchInfo mMatchInfo;
	private BattleProgressInfo mProgressInfo;

	private int mTeamCount;
	private SyncRandom mSyncRandom;
	private BattleAiCollector mAiCollector;

	private OnSpawnCommanderDelegate mOnSpawnCommanderDelegate;
	private OnEventDelegate mOnEventDelegate;
}
