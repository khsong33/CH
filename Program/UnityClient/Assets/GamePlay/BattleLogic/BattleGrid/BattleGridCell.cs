using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGridCell
{
	public const int cNearCellMaxDistance = 2;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 그리드
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀 인덱스
	public RowCol aCellIndex
	{
		get { return mCellIndex; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 중심 좌표
	public Coord2 aCenterCoord
	{
		get { return mCenterCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주변 셀들
	public BattleGridCell[] GetNearCells(int pCellDistance)
	{
		return mNearCells[pCellDistance];
	}
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트 체인
	public GuidObjectChain<BattleObject> aObjectChain
	{
		get { return mObjectChain; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("BattleGridCell<{0},{1}>", mCellIndex.row, mCellIndex.col);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleGridCell(BattleGrid pBattleGrid, int lCellRowIndex, int lCellColIndex)
	{
		//Debug.Log("Awake() - " + this);

		mBattleGrid = pBattleGrid;
		mCellIndex = new RowCol(lCellRowIndex, lCellColIndex);
		mCenterCoord = _GetCenterCoord();

		mObjectChain = new GuidObjectChain<BattleObject>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀 연결 정보를 초기화합니다.
	public void SetUpLink()
	{
//		DebugUtil.Assert(cNearCellMaxDistance == 2, "cNearCellMaxDistance == 2");

		// 주변 셀을 연결합니다.
		mNearCells = new BattleGridCell[3][];
		mNearCells[0] = _ConstructNearCellsInDistanceZero();

		if ((mCellIndex.col & 0x01) == 0)
		{
			//Debug.Log("Even");
			mNearCells[1] = _ConstructNearCells(BattleGrid.sEvenColNearOffsetsInDistanceOne);
			mNearCells[2] = _ConstructNearCells(BattleGrid.sEvenColNearOffsetsInDistanceTwo);
		}
		else
		{
			//Debug.Log("Odd");
			mNearCells[1] = _ConstructNearCells(BattleGrid.sOddColNearOffsetsInDistanceOne);
			mNearCells[2] = _ConstructNearCells(BattleGrid.sOddColNearOffsetsInDistanceTwo);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀에 있는 기즈모 가운데에서 해당 좌표를 중심으로 반경 안에서 이벤트를 처리할 수 있는 가장 가까이 있는 기즈모를 찾습니다.
	public void SearchNearestTouchObject(int pSearchingTeamIdx, Coord2 pCoord, BattleGrid.Touch pGridTouch, BattleObject pNearestObject, int pSqrObjectDistance, out BattleObject oNearestObject, out int oSqrObjectDistance)
	{
		//Debug.Log("SearchNearestObject()");

		oNearestObject = pNearestObject;
		oSqrObjectDistance = pSqrObjectDistance;

		using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = mObjectChain.CreateEnumerator())
		{
			while (lObjectEnumerator.MoveNext())
			{
				BattleObject lObject = lObjectEnumerator.GetCurrent();
				if (!lObject.aIsDetecteds[pSearchingTeamIdx])
					continue;
				//Debug.Log("lObject.aCoord=" + lObject.aCoord + " - " + lObject);
				int lSqrDistance = (lObject.aCoord - pCoord).SqrMagnitude();
				if ((lSqrDistance < pSqrObjectDistance) &&
					(lSqrDistance < lObject.aStageObject.aSqrTouchRadius))
				{
					if (lObject.CheckTouch(pGridTouch))
					{
						oNearestObject = lObject;
						oSqrObjectDistance = lSqrDistance;
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 셀을 중심으로 일정 거리 안에 있는 오브젝트에 대해서 처리 함수를 실행합니다.
	public void CallObjectHandler<TBattleObject>(int pCheckCellDistance, Coord2 pSearchCoord, int pSearchRange, BattleGrid.OnObjectHandlingDelegate<TBattleObject> pOnObjectHandlingDelegate)
		where TBattleObject : BattleObject
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						TBattleObject lObject = lObjectEnumerator.Current as TBattleObject;
						if (lObject != null)
						{
							int lSqrCheckRange = pSearchRange * pSearchRange;
							int lSqrTargetDistance = (lObject.aCoord - pSearchCoord).SqrMagnitude();

							if (lSqrTargetDistance <= lSqrCheckRange)
							{
								if (!pOnObjectHandlingDelegate(lObject, lSqrTargetDistance))
									return;
							}
						}
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 주어진 비교자를 가지고 제일 적합한 오브젝트를 찾습니다.
	public TBattleObject SearchObject<TBattleObject, TSearchComparator>(int pCheckCellDistance, Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, TSearchComparator pComparator)
		where TBattleObject : BattleObject
		where TSearchComparator : BattleGrid.ISearchComparator<TBattleObject>
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						TBattleObject lObject = lObjectEnumerator.Current as TBattleObject;
						if (lObject != null)
						{
							int lSqrCheckRange = pSearchRange * pSearchRange;
							int lSqrTargetDistance = (lObject.aCoord - pSearchCoord).SqrMagnitude();

							if (lSqrTargetDistance <= lSqrCheckRange)
							{
								if (!lObject.aIsDetecteds[pSearchingTeamIdx])
									continue;
								if (!pComparator.Compare(lObject, lSqrTargetDistance))
									return pComparator.aResult; // 더이상 찾지 않고 현재 결과를 반환
							}
						}
					}
				}
			}
		}

		return pComparator.aResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 셀을 중심으로 일정 거리 안에 있는 부대에 대해서 처리 함수를 실행합니다.
	public void CallTroopHandler(int pCheckCellDistance, Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, BattleGrid.TeamSelection pTeamSelection, BattleGrid.OnObjectHandlingDelegate<BattleTroop> pOnObjectHandlingDelegate)
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						BattleTroop lTroop = lObjectEnumerator.Current as BattleTroop;
						if ((lTroop != null) &&
							BattleGrid.CheckTeamSelection(pSearchingTeamIdx, lTroop.aTeamIdx, pTeamSelection))
						{
							int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
							int lSqrCheckRange = lCheckRange * lCheckRange;
							int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

							if (lSqrTargetDistance <= lSqrCheckRange)
							{
								if (!pOnObjectHandlingDelegate(lTroop, lSqrTargetDistance))
									return;
							}
						}
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 셀을 중심으로 일정 거리 안에 있는 부대에 대해서 처리 함수를 실행합니다.
	public void CallTroopHandler(int pCheckCellDistance, BattleGrid.OnObjectHandlingDelegate<BattleTroop> pOnObjectHandlingDelegate)
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						BattleTroop lTroop = lObjectEnumerator.Current as BattleTroop;
						if (lTroop != null)
						{
							if (!pOnObjectHandlingDelegate(lTroop, 0))
								return;
						}
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 주어진 비교자를 가지고 제일 적합한 부대를 찾습니다.
	public BattleTroop SearchTroop(int pCheckCellDistance, Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, BattleGrid.TeamSelection pTeamSelection, BattleGrid.ISearchComparator<BattleTroop> pComparator)
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						BattleTroop lTroop = lObjectEnumerator.Current as BattleTroop;
						if ((lTroop != null) &&
							BattleGrid.CheckTeamSelection(pSearchingTeamIdx, lTroop.aTeamIdx, pTeamSelection))
						{
							int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
							int lSqrCheckRange = lCheckRange * lCheckRange;
							int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

							if (lSqrTargetDistance <= lSqrCheckRange)
							{
								if (!lTroop.aIsDetecteds[pSearchingTeamIdx])
									continue; // 탐지 안 된 부대는 건너뜀
								if (!pComparator.Compare(lTroop, lSqrTargetDistance))
									return pComparator.aResult; // 더이상 찾지 않고 현재 결과를 반환
							}
						}
					}
				}
			}
		}

		return pComparator.aResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까운 부대를 찾습니다.
	public BattleTroop SearchNearestTroop(int pCheckCellDistance, Coord2 pSearchCoord, int pSearchRange, int pSearchingTeamIdx, BattleGrid.TeamSelection pTeamSelection)
	{
		int lSqrNearestTroopDistance = int.MaxValue;
		BattleTroop lNearestTroop = null;

		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						BattleTroop lTroop = lObjectEnumerator.Current as BattleTroop;
						if ((lTroop != null) &&
							BattleGrid.CheckTeamSelection(pSearchingTeamIdx, lTroop.aTeamIdx, pTeamSelection))
						{
							int lCheckRange = pSearchRange + lTroop.aTroopSpec.mUnitRadius;
							int lSqrCheckRange = lCheckRange * lCheckRange;
							int lSqrTargetDistance = (lTroop.aCoord - pSearchCoord).SqrMagnitude();

							if (lSqrTargetDistance <= lSqrCheckRange)
							{
								if (!lTroop.aIsDetecteds[pSearchingTeamIdx])
									continue; // 탐지 안 된 부대는 건너뜀
								if (lSqrNearestTroopDistance > lSqrTargetDistance)
								{
									lSqrNearestTroopDistance = lSqrTargetDistance;
									lNearestTroop = lTroop;
								}
							}
						}
					}
				}
			}
		}

		return lNearestTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 주변에 해당 팀의 부대가 있는지 검사합니다.
	public bool CheckTroop(int pCheckCellDistance, int pCheckingTeamIdx, BattleGrid.TeamSelection pTeamSelection)
	{
		for (int iDistance = 0; iDistance <= pCheckCellDistance; ++iDistance)
		{
			BattleGridCell[] lSearchCells = mNearCells[iDistance];
			for (int iCell = 0; iCell < lSearchCells.Length; ++iCell)
			{
				using (GuidObjectChainEnumerator<BattleObject> lObjectEnumerator = lSearchCells[iCell].mObjectChain.CreateEnumerator())
				{
					while (lObjectEnumerator.MoveNext())
					{
						//Debug.Log("lObject.aCoord=" + lObject.aCoord);
						BattleTroop lTroop = lObjectEnumerator.Current as BattleTroop;
						if ((lTroop != null) &&
							BattleGrid.CheckTeamSelection(pCheckingTeamIdx, lTroop.aTeamIdx, pTeamSelection))
							return true;
					}
				}
			}
		}

		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 셀로 들어오려는 이동 벡터를 셀 경계에서 잘라내고 그 수정된 도착점을 반환합니다.
	public Coord2 CutCellMoveVector(RowCol pCellIndex, Coord2 pMoveStartCoord, Coord2 pMoveEndCoord)
	{
		Coord2 lCellMinCoord = new Coord2(mCenterCoord.x - mBattleGrid.aCellBlockScaleX, mCenterCoord.z - mBattleGrid.aCellBlockScaleZ);
		Coord2 lCellMaxCoord = new Coord2(mCenterCoord.x + mBattleGrid.aCellBlockScaleX, mCenterCoord.z + mBattleGrid.aCellBlockScaleZ);

		if ((pMoveEndCoord.x < lCellMinCoord.x) ||
			(pMoveEndCoord.x >= lCellMaxCoord.x) ||
			(pMoveEndCoord.z < lCellMinCoord.z) ||
			(pMoveEndCoord.z >= lCellMaxCoord.z))
			return pMoveEndCoord; // 셀 밖으로 가는 벡터는 통과합니다.

		Coord2 lCutEndCoord = pMoveEndCoord;
		if (pMoveStartCoord.x < lCellMinCoord.x)
			lCutEndCoord.x = lCellMinCoord.x - 1;
		else if (pMoveStartCoord.x >= lCellMaxCoord.x)
			lCutEndCoord.x = lCellMaxCoord.x;
		if (pMoveStartCoord.z < lCellMinCoord.z)
			lCutEndCoord.z = lCellMinCoord.z - 1;
		else if (pMoveStartCoord.z >= lCellMaxCoord.z)
			lCutEndCoord.z = lCellMaxCoord.z;
		return lCutEndCoord;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 중심 좌표를 구합니다.
	private Coord2 _GetCenterCoord()
	{
		int lCellCoordX = mCellIndex.col * mBattleGrid.aCellScaleX + mBattleGrid.aCellBlockScaleX;
		int lCellCoordZ;
		if ((mCellIndex.col & 0x01) == 0) // 짝수열이라면
			lCellCoordZ = mCellIndex.row * mBattleGrid.aCellScaleZ + mBattleGrid.aCellBlockScaleZ;
		else // 홀수열이라면
			lCellCoordZ = mCellIndex.row * mBattleGrid.aCellScaleZ;

		return new Coord2(lCellCoordX, lCellCoordZ);
	}
	//-------------------------------------------------------------------------------------------------------
	// 0칸 주변 셀을 얻습니다.
	private BattleGridCell[] _ConstructNearCellsInDistanceZero()
	{
		BattleGridCell[] lNearCells = new BattleGridCell[1];
		lNearCells[0] = this;

		return lNearCells;
	}
	//-------------------------------------------------------------------------------------------------------
	// 주변 셀을 얻습니다.
	private BattleGridCell[] _ConstructNearCells(int[,] pNearCellOffsets)
	{
		int lNearCellCount = pNearCellOffsets.GetLength(0);
		BattleGridCell[] lNearCells = new BattleGridCell[lNearCellCount];

		int lValidCellCount = 0;
		for (int iCell = 0; iCell < lNearCellCount; ++iCell)
		{
			int lCellRowIndex = mCellIndex.row + pNearCellOffsets[iCell, 0];
			if ((lCellRowIndex < 0) ||
				(lCellRowIndex >= mBattleGrid.aCellRowCount))
				continue;
			int lCellColIndex = mCellIndex.col + pNearCellOffsets[iCell, 1];
			if ((lCellColIndex < 0) ||
				(lCellColIndex >= mBattleGrid.aCellColCount))
				continue;

			lNearCells[lValidCellCount] = mBattleGrid.aCells[lCellRowIndex, lCellColIndex];
			if (lNearCells[lValidCellCount] != null)
				++lValidCellCount;
		}

		BattleGridCell[] lOptimizedNearCells = new BattleGridCell[lValidCellCount];
		for (int iCell = 0; iCell < lValidCellCount; ++iCell)
			lOptimizedNearCells[iCell] = lNearCells[iCell];

		return lOptimizedNearCells;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGrid mBattleGrid;
	private RowCol mCellIndex;
	private Coord2 mCenterCoord;

	private BattleGridCell[][] mNearCells;
	private GuidObjectChain<BattleObject> mObjectChain;
}
