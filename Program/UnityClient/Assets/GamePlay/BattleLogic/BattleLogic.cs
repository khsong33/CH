//#define BATTLE_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

#if BATTLE_VERIFY_TEST
public static class BattleLogicTestStatistics
{
    public class LogItem
    {
        public uint troopGuid;
        public string type;
        public object value;
    }
    public static List<LogItem> aItems = new List<LogItem>();
    public static void Reset()
    {
        aItems.Clear();
    }
    public static void Log(uint troopGuid, string type, object value)
    {
        aItems.Add(new LogItem() { troopGuid = troopGuid, type = type, value = value });
    }
}
#endif

public class BattleLogic
{
	public readonly bool cIsEnableGridPooling = true;
	public readonly bool cIsEnableHeadquarterPooling = true;
	public readonly bool cIsEnableCheckPointPooling = true;
	public readonly bool cIsEnableEventPointPooling = true;
	public readonly bool cIsEnableUserPooling = true;
	public readonly bool cIsEnableCommanderPooling = true;
	public readonly bool cIsEnableTroopPooling = true;
	public readonly bool cIsEnableObserverDummyPooling = true;
	public readonly bool cIsEnableTargetingDummyPooling = true;
	public readonly bool cIsEnableFireEffectPooling = true;
	public readonly bool cIsEnableTroopSkillEffectPooling = true;
	public readonly bool cIsEnableGridSkillEffectPooling = true;
	public readonly bool cIsEnableGridSkillEffectChainPooling = true;
	public readonly bool cIsEnableAiCollectorPooling = true;
	public readonly bool cIsEnableVerifierPooling = true;
	public readonly bool cIsEnableVerifyDataPooling = true;

	public readonly bool cIsEnableObjectPooling = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleLogic()
	{
		mGridPoolingQueueDictionary = new Dictionary<String, Queue<BattleGrid>>();
		mGridCount = 0;
		mHeadquarterPoolingQueue = new Queue<BattleHeadquarter>();
		mHeadquarterCount = 0;
		mCheckPointPoolingQueue = new Queue<BattleCheckPoint>();
		mCheckPointCount = 0;
		mEventPointPoolingQueue = new Queue<BattleEventPoint>();
		mEventPointCount = 0;
		mUserPoolingQueue = new Queue<BattleUser>();
		mUserCount = 0;
		mCommanderPoolingQueue = new Queue<BattleCommander>();
		mCommanderCount = 0;
		mTroopPoolingQueue = new Queue<BattleTroop>();
		mTroopCount = 0;
		mObserverDummyPoolingQueue = new Queue<BattleObserverDummy>();
		mObserverDummyCount = 0;
		mTargetingDummyPoolingQueue = new Queue<BattleTargetingDummy>();
		mTargetingDummyCount = 0;
		mFireEffectPoolingQueue = new Queue<BattleFireEffect>();
		mFireEffectCount = 0;
		mTroopSkillEffectPoolingQueue = new Queue<BattleTroopSkillEffect>();
		mTroopSkillEffectCount = 0;
		mGridSkillEffectPoolingQueue = new Queue<BattleGridSkillEffect>();
		mGridSkillEffectCount = 0;
		mGridSkillEffectChainPoolingQueue = new Queue<GuidObjectChain<BattleGridSkillEffect>>();
		mGridSkillEffectChainCount = 0;
		mAiCollectorPoolingQueueDictionary = new Dictionary<BattleMatchInfo.RuleType, Queue<BattleAiCollector>>();
		mAiCollectorCount = 0;
		mVerifierPoolingQueue = new Queue<BattleVerifier>();
		mVerifierCount = 0;
		mVerifyDataPoolingQueue = new Queue<BattleVerifyData>();
		mVerifyDataCount = 0;

		mSkillEffectGuidGenerator = 0;

		mTroopActionPool = new ObjectPool<BattleTroopAction>(cIsEnableObjectPooling);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleGrid CreateGrid(String pLayoutName, String pLayoutCsvText, BattleMatchInfo pMatchInfo, BattleProgressInfo pProgressInfo, IStage pStage)
	{
		Queue<BattleGrid> lGridPoolingQueue;
		if (!mGridPoolingQueueDictionary.TryGetValue(pLayoutName, out lGridPoolingQueue))
		{
			lGridPoolingQueue = new Queue<BattleGrid>();
			mGridPoolingQueueDictionary.Add(pLayoutName, lGridPoolingQueue); // 큐를 미리 확보합니다.
		}

		BattleGrid lGrid;
		if (lGridPoolingQueue.Count <= 0)
			lGrid = new BattleGrid(this, pLayoutName, pLayoutCsvText);
		else
			lGrid = lGridPoolingQueue.Dequeue();

		lGrid.OnCreateGrid(pMatchInfo, pProgressInfo, pStage);

		++mGridCount;
		return lGrid;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyGrid(BattleGrid pBattleGrid)
	{
		--mGridCount;
		if (cIsEnableGridPooling)
			mGridPoolingQueueDictionary[pBattleGrid.aLayoutName].Enqueue(pBattleGrid);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleHeadquarter CreateHeadquarter(
		BattleGridTile pGridTile,
		int pHeadquarterIdx,
		uint pHeadquarterGuid,
		Coord2 pSpawnCoord)
	{
		BattleHeadquarter lHeadquarter;
		if (mHeadquarterPoolingQueue.Count <= 0)
			lHeadquarter = new BattleHeadquarter(this);
		else
			lHeadquarter = mHeadquarterPoolingQueue.Dequeue();

		lHeadquarter.OnCreateHeadquarter(
			pGridTile,
			pHeadquarterIdx,
			pHeadquarterGuid,
			pSpawnCoord);
		lHeadquarter.UpdateCoord(lHeadquarter.aCoord, true);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pGridTile.aBattleGrid.aFrameUpdater.aFrameIdx + "] lHeadquarter=" + lHeadquarter);
	#endif
		++mHeadquarterCount;
		return lHeadquarter;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyHeadquarter(BattleHeadquarter pHeadquarter)
	{
		--mHeadquarterCount;
		if (cIsEnableHeadquarterPooling)
			mHeadquarterPoolingQueue.Enqueue(pHeadquarter);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleCheckPoint CreateCheckPoint(
		BattleGridTile pGridTile,
		int pCheckPointIdx,
		uint pCheckPointGuid,
		int pMilliMpGeneration,
		int pMilliEnemyVpDecrease,
		int pStarId,
		BattleCheckPoint.CheckPointType pCheckPointType,
		String[] pDefenderCommanders,
		int pUserIdx)
	{
		BattleCheckPoint lCheckPoint;
		if (mCheckPointPoolingQueue.Count <= 0)
			lCheckPoint = new BattleCheckPoint(this);
		else
			lCheckPoint = mCheckPointPoolingQueue.Dequeue();

		lCheckPoint.OnCreateCheckPoint(
			pGridTile,
			pCheckPointIdx,
			pCheckPointGuid,
			pMilliMpGeneration,
			pMilliEnemyVpDecrease,
			pStarId,
			pCheckPointType,
			pDefenderCommanders,
			pUserIdx);
		lCheckPoint.UpdateCoord(lCheckPoint.aCoord, true);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pGridTile.aBattleGrid.aFrameUpdater.aFrameIdx + "] lCheckPoint=" + lCheckPoint);
	#endif
		++mCheckPointCount;
		return lCheckPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCheckPoint(BattleCheckPoint pCheckPoint)
	{
		--mCheckPointCount;
		if (cIsEnableCheckPointPooling)
			mCheckPointPoolingQueue.Enqueue(pCheckPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleEventPoint CreateEventPoint(
		BattleGridTile pGridTile,
		int pEventPointIdx,
		uint pEventPointGuid,
		int pEventStartDelay,
		int pEventRepeatCount,
		int pEventRepeatDelay,
		String[] pEventNames)
	{
		BattleEventPoint lEventPoint;
		if (mEventPointPoolingQueue.Count <= 0)
			lEventPoint = new BattleEventPoint(this);
		else
			lEventPoint = mEventPointPoolingQueue.Dequeue();

		lEventPoint.OnCreateEventPoint(
			pGridTile,
			pEventPointIdx,
			pEventPointGuid,
			pEventStartDelay,
			pEventRepeatCount,
			pEventRepeatDelay,
			pEventNames);
		lEventPoint.UpdateCoord(lEventPoint.aCoord, true);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pGridTile.aBattleGrid.aFrameUpdater.aFrameIdx + "] lEventPoint=" + lEventPoint);
	#endif
		++mEventPointCount;
		return lEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyEventPoint(BattleEventPoint pEventPoint)
	{
		--mEventPointCount;
		if (cIsEnableEventPointPooling)
			mEventPointPoolingQueue.Enqueue(pEventPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleUser CreateUser(
		BattleGrid pBattleGrid,
		BattleUserInfo pUserInfo,
		BattleUser.ControlType pControlType)
	{
		BattleUser lUser;
		if (mUserPoolingQueue.Count <= 0)
			lUser = new BattleUser(this);
		else
			lUser = mUserPoolingQueue.Dequeue();

		lUser.OnCreateUser(
			pBattleGrid,
			pUserInfo,
			pControlType);

		++mUserCount;
		return lUser;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyUser(BattleUser pUser)
	{
		--mUserCount;
		if (cIsEnableUserPooling)
			mUserPoolingQueue.Enqueue(pUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleCommander CreateCommander(BattleGrid pBattleGrid, BattleUser pUser, CommanderItem pCommanderItem, int pCommanderSlotIdx, Coord2 pSpawnCoord, int pSpawnDirectionY, BattleUser pTeamUser)
	{
		BattleCommander lCommander;
		if (mCommanderPoolingQueue.Count <= 0)
			lCommander = new BattleCommander(this);
		else
			lCommander = mCommanderPoolingQueue.Dequeue();

		lCommander.OnCreateCommander(
			pBattleGrid,
			pUser,
			pCommanderItem,
			pCommanderSlotIdx,
			pSpawnCoord,
			pSpawnDirectionY,
			pTeamUser);

		pCommanderItem.mIsSpawnAvailable = false;

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pBattleGrid.aFrameUpdater.aFrameIdx + "] lCommander=" + lCommander);
	#endif
		++mCommanderCount;
		return lCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCommander(BattleCommander pBattleCommander)
	{
		--mCommanderCount;
		if (cIsEnableCommanderPooling)
			mCommanderPoolingQueue.Enqueue(pBattleCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleTroop CreateTroop(BattleGrid pBattleGrid, int pTroopGridIdx, TroopSpec pTroopSpec, Coord2 pSpawnCoord, int pSpawnDirectionY, BattleCommander pCommander, int pTroopSlotIdx, BattleTroop.StartState pStartState)
	{
		BattleTroop lTroop;
		if (mTroopPoolingQueue.Count <= 0)
			lTroop = new BattleTroop(this);
		else 
			lTroop = mTroopPoolingQueue.Dequeue();

		lTroop.OnCreateTroop(
			pBattleGrid,
			pTroopGridIdx,
			pTroopSpec,
			pSpawnCoord,
			pSpawnDirectionY,
			pCommander,
			pTroopSlotIdx,
			pStartState);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pBattleGrid.aFrameUpdater.aFrameIdx + "] lTroop=" + lTroop);
	#endif
		++mTroopCount;
		return lTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroop(BattleTroop pBattleTroop)
	{
		--mTroopCount;
		if (cIsEnableTroopPooling)
			mTroopPoolingQueue.Enqueue(pBattleTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleObserverDummy CreateObserverDummy(BattleGrid pBattleGrid, Coord2 pCoord, int pDirectionY, int pObserverTeamIdx, int pDetectionRange, int pShowDelay)
	{
		BattleObserverDummy lObserverDummy;
		if (mObserverDummyPoolingQueue.Count <= 0)
			lObserverDummy = new BattleObserverDummy(this);
		else
			lObserverDummy = mObserverDummyPoolingQueue.Dequeue();

		lObserverDummy.OnCreateObserverDummy(
			pBattleGrid,
			pCoord,
			pDirectionY,
			pObserverTeamIdx,
			pDetectionRange,
			pShowDelay);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pBattleGrid.aFrameUpdater.aFrameIdx + "] lObserverDummy=" + lObserverDummy);
	#endif
		++mObserverDummyCount;
		return lObserverDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyObserverDummy(BattleObserverDummy pObserverDummy)
	{
		--mObserverDummyCount;
		if (cIsEnableObserverDummyPooling)
			mObserverDummyPoolingQueue.Enqueue(pObserverDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleTargetingDummy CreateTargetingDummy(BattleGrid pBattleGrid, BattleTroop pTroop, int pTargetTeamIdx, int pShowDelay)
	{
		BattleTargetingDummy lTargetingDummy;
		if (mTargetingDummyPoolingQueue.Count <= 0)
			lTargetingDummy = new BattleTargetingDummy(this);
		else
			lTargetingDummy = mTargetingDummyPoolingQueue.Dequeue();

		lTargetingDummy.OnCreateTargetingDummy(
			pBattleGrid,
			pTroop,
			pTargetTeamIdx,
			pShowDelay);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pBattleGrid.aFrameUpdater.aFrameIdx + "] lTargetingDummy=" + lTargetingDummy);
	#endif
		++mTargetingDummyCount;
		return lTargetingDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTargetingDummy(BattleTargetingDummy pTargetingDummy)
	{
		--mTargetingDummyCount;
		if (cIsEnableTargetingDummyPooling)
			mTargetingDummyPoolingQueue.Enqueue(pTargetingDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleFireEffect CreateFireEffect(BattleTroopTargetingInfo pTroopTargetingInfo)
	{
		BattleFireEffect lFireEffect;
		if (mFireEffectPoolingQueue.Count <= 0)
			lFireEffect = new BattleFireEffect(this);
		else
			lFireEffect = mFireEffectPoolingQueue.Dequeue();

		lFireEffect.OnCreateFireEffect(pTroopTargetingInfo);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pTroopTargetingInfo.aAttackTroop.aBattleGrid.aFrameUpdater.aFrameIdx + "] lFireEffect=" + lFireEffect);
	#endif
		++mFireEffectCount;
		return lFireEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyFireEffect(BattleFireEffect pFireEffect)
	{
		--mFireEffectCount;
		if (cIsEnableFireEffectPooling)
			mFireEffectPoolingQueue.Enqueue(pFireEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과 고유 번호를 생성합니다.
	public uint GenerateSkillEffectGuid()
	{
		return ++mSkillEffectGuidGenerator;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleTroopSkillEffect CreateTroopSkillEffect(BattleTroop pEffectTroop, TroopEffectSpec pTroopEffectSpec, int pActivationDelay, int pDurationDelay, uint pSkillEffectGuid)
	{
		if (pEffectTroop.aLastSkillEffectGuid == pSkillEffectGuid)
			return null;

		BattleTroopSkillEffect lTroopSkillEffect;
		if (mTroopSkillEffectPoolingQueue.Count <= 0)
			lTroopSkillEffect = new BattleTroopSkillEffect(this);
		else
			lTroopSkillEffect = mTroopSkillEffectPoolingQueue.Dequeue();

		lTroopSkillEffect.OnCreateTroopSkillEffect(pEffectTroop, pTroopEffectSpec, pActivationDelay, pDurationDelay);
		pEffectTroop.aLastSkillEffectGuid = pSkillEffectGuid;

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pEffectTroop.aBattleGrid.aFrameUpdater.aFrameIdx + "] lTroopSkillEffect=" + lTroopSkillEffect);
	#endif
		++mTroopSkillEffectCount;
		return lTroopSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroopSkillEffect(BattleTroopSkillEffect pTroopSkillEffect)
	{
		--mTroopSkillEffectCount;
		if (cIsEnableTroopSkillEffectPooling)
			mTroopSkillEffectPoolingQueue.Enqueue(pTroopSkillEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleGridSkillEffect CreateGridSkillEffect(BattleGridTile pGridTile, Coord2 pEffectGridCoord, int pDirectionY, GroundEffectSpec pGroundEffectSpec, int pActivationDelay, int pDurationDelay, uint pSkillEffectGuid)
	{
		if (pGridTile.aLastSkillEffectGuid == pSkillEffectGuid)
			return null;
		pGridTile.aLastSkillEffectGuid = pSkillEffectGuid;

		BattleGridSkillEffect lGridSkillEffect;
		if (mGridSkillEffectPoolingQueue.Count <= 0)
			lGridSkillEffect = new BattleGridSkillEffect(this);
		else
			lGridSkillEffect = mGridSkillEffectPoolingQueue.Dequeue();

		lGridSkillEffect.OnCreateGridSkillEffect(pGridTile, pEffectGridCoord, pDirectionY, pGroundEffectSpec, pActivationDelay, pDurationDelay);

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log("[" + pGridTile.aBattleGrid.aFrameUpdater.aFrameIdx + "] lGridSkillEffect=" + lGridSkillEffect);
	#endif
		++mGridSkillEffectCount;
		return lGridSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyGridSkillEffect(BattleGridSkillEffect pGridSkillEffect)
	{
		--mGridSkillEffectCount;
		if (cIsEnableGridSkillEffectPooling)
			mGridSkillEffectPoolingQueue.Enqueue(pGridSkillEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public GuidObjectChain<BattleGridSkillEffect> CreateGridSkillEffectChain()
	{
		GuidObjectChain<BattleGridSkillEffect> lGridSkillEffectChain;
		if (mGridSkillEffectChainPoolingQueue.Count <= 0)
			lGridSkillEffectChain = new GuidObjectChain<BattleGridSkillEffect>();
		else
			lGridSkillEffectChain = mGridSkillEffectChainPoolingQueue.Dequeue();

		++mGridSkillEffectChainCount;
		return lGridSkillEffectChain;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyGridSkillEffectChain(GuidObjectChain<BattleGridSkillEffect> pGridSkillEffectChain)
	{
		--mGridSkillEffectChainCount;
		if (cIsEnableGridSkillEffectChainPooling)
			mGridSkillEffectChainPoolingQueue.Enqueue(pGridSkillEffectChain);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleAiCollector CreateAiCollector(BattleMatchInfo.RuleType pRuleType)
	{
		Queue<BattleAiCollector> lAiCollectorPoolingQueue;
		if (!mAiCollectorPoolingQueueDictionary.TryGetValue(pRuleType, out lAiCollectorPoolingQueue))
		{
			lAiCollectorPoolingQueue = new Queue<BattleAiCollector>();
			mAiCollectorPoolingQueueDictionary.Add(pRuleType, lAiCollectorPoolingQueue); // 큐를 미리 확보합니다.
		}

		BattleAiCollector lAiCollector;
		if (lAiCollectorPoolingQueue.Count <= 0)
		{
			switch (pRuleType)
			{
			case BattleMatchInfo.RuleType.Conquest:
				lAiCollector = new BattlePvpAiCollector(this);
				break;
			default:
				Debug.LogError("not yet implemented pRuleType:" + pRuleType);
				lAiCollector = null;
				break;
			}
		}
		else
			lAiCollector = lAiCollectorPoolingQueue.Dequeue();

		lAiCollector.OnCreate();	

		++mAiCollectorCount;
		return lAiCollector;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyAiCollector(BattleAiCollector pBattleAiCollector)
	{
		--mAiCollectorCount;
		if (cIsEnableAiCollectorPooling)
		{
			Queue<BattleAiCollector> lAiCollectorPoolingQueue;
			if (!mAiCollectorPoolingQueueDictionary.TryGetValue(pBattleAiCollector.aRuleType, out lAiCollectorPoolingQueue))
			{
				lAiCollectorPoolingQueue = new Queue<BattleAiCollector>();
				mAiCollectorPoolingQueueDictionary.Add(pBattleAiCollector.aRuleType, lAiCollectorPoolingQueue);
			}

			lAiCollectorPoolingQueue.Enqueue(pBattleAiCollector);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleVerifier CreateVerifier()
	{
		BattleVerifier lVerifier;
		if (mVerifierPoolingQueue.Count <= 0)
			lVerifier = new BattleVerifier(this);
		else
			lVerifier = mVerifierPoolingQueue.Dequeue();

		lVerifier.OnCreateVerifier();

		++mVerifierCount;
		return lVerifier;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyVerifier(BattleVerifier pBattleVerifier)
	{
		--mVerifierCount;
		if (cIsEnableVerifierPooling)
			mVerifierPoolingQueue.Enqueue(pBattleVerifier);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleVerifyData CreateVerifyData()
	{
		BattleVerifyData lVerifyData;
		if (mVerifyDataPoolingQueue.Count <= 0)
			lVerifyData = new BattleVerifyData(this);
		else
			lVerifyData = mVerifyDataPoolingQueue.Dequeue();

		lVerifyData.OnCreateVerifyData();

		++mVerifyDataCount;
		return lVerifyData;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyVerifyData(BattleVerifyData pBattleVerifyData)
	{
		--mVerifyDataCount;
		if (cIsEnableVerifyDataPooling)
			mVerifyDataPoolingQueue.Enqueue(pBattleVerifyData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 생성합니다.
	public BattleTroopAction AllocateTroopAction()
	{
		return mTroopActionPool.Allocate();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<String, Queue<BattleGrid>> mGridPoolingQueueDictionary;
	private int mGridCount;
	private Queue<BattleHeadquarter> mHeadquarterPoolingQueue;
	private int mHeadquarterCount;
	private Queue<BattleCheckPoint> mCheckPointPoolingQueue;
	private int mCheckPointCount;
	private Queue<BattleEventPoint> mEventPointPoolingQueue;
	private int mEventPointCount;
	private Queue<BattleUser> mUserPoolingQueue;
	private int mUserCount;
	private Queue<BattleCommander> mCommanderPoolingQueue;
	private int mCommanderCount;
	private Queue<BattleTroop> mTroopPoolingQueue;
	private int mTroopCount;
	private Queue<BattleObserverDummy> mObserverDummyPoolingQueue;
	private int mObserverDummyCount;
	private Queue<BattleTargetingDummy> mTargetingDummyPoolingQueue;
	private int mTargetingDummyCount;
	private Queue<BattleFireEffect> mFireEffectPoolingQueue;
	private int mFireEffectCount;
	private Queue<BattleTroopSkillEffect> mTroopSkillEffectPoolingQueue;
	private int mTroopSkillEffectCount;
	private Queue<BattleGridSkillEffect> mGridSkillEffectPoolingQueue;
	private int mGridSkillEffectCount;
	private Queue<GuidObjectChain<BattleGridSkillEffect>> mGridSkillEffectChainPoolingQueue;
	private int mGridSkillEffectChainCount;
	private Dictionary<BattleMatchInfo.RuleType, Queue<BattleAiCollector>> mAiCollectorPoolingQueueDictionary;
	private int mAiCollectorCount;
	private Queue<BattleVerifier> mVerifierPoolingQueue;
	private int mVerifierCount;
	private Queue<BattleVerifyData> mVerifyDataPoolingQueue;
	private int mVerifyDataCount;

	private uint mSkillEffectGuidGenerator;

	private ObjectPool<BattleTroopAction> mTroopActionPool;
}
