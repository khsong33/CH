using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleFrameTick : BattleFrameInputData
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에서 생성합니다.
	public static BattleFrameTick Create(int pTickNo)
	{
		BattleFrameTick lFrameTick;
		if (sPoolingQueue.Count <= 0)
			lFrameTick = new BattleFrameTick();
		else
			lFrameTick = sPoolingQueue.Dequeue();

		lFrameTick.OnCreate(pTickNo);

		return lFrameTick;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 팩토리에 돌려놓습니다.
	public void Destroy()
	{
		OnDestroy();

		sPoolingQueue.Enqueue(this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	private BattleFrameTick()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Queue<BattleFrameTick> sPoolingQueue = new Queue<BattleFrameTick>();
}
