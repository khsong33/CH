﻿//#define BATTLE_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System;

public class BattleProtocol
{
	public enum BattleEndCode
	{
		PlayWin,
		PlayLose,
		PlayDraw,
		TestWin,
		TestLose,

		None,
	}

	public delegate void OnBattleRoomEnteredDelegate(BattleMatchInfo pMatchInfo);
	public delegate void OnBattleStartedDelegate();
	public delegate void OnFrameTickDelegate(BattleFrameTick pFrameTick);
	public delegate void OnBattleEndedDelegate(BattleEndInfo pEndInfo);
	public delegate void OnLeaveUserDelegate(String pUserKey);

	public const int cTargetTroopMaxCount = 16;
	public const int cMaxClientCount = 6;

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 프레임 틱을 받았을 때의 처리를 합니다.
	public static void OnFrameTick(BattleFrameTick pFrameTick, IBattleFrameTickContext pFrameTickContext)
	{
		//Debug.Log("_OnFrameTick(): " + pFrameTick.mTickNo);

		// 서버에서 받은 프레임 입력을 처리합니다.
		for (int iInput = 0; iInput < pFrameTick.aFrameInputList.Count; ++iInput)
		{
			BattleFrameInput lFrameInput = pFrameTick.aFrameInputList[iInput];
			//Debug.Log("[" + pFrameTick.mTickNo + "] lFrameInput=" + lFrameInput);
		#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
			Logger.Log("[" + pFrameTickContext.aBattleGrid.aFrameUpdater.aFrameIdx + "] " + lFrameInput.ToString());
		#endif

			BattleFrameInputType lInputType = (BattleFrameInputType)lFrameInput.mType;
			switch (lInputType)
			{
			case BattleFrameInputType.SpawnCommander:
			case BattleFrameInputType.SpawnCommanderAndAttackTroop:
			case BattleFrameInputType.SpawnCommanderAndGrabCheckPoint:
				{
					int lInputUserIdx = lFrameInput.mA;
					int lSpawnCommanderItemSlotIdx = lFrameInput.mB;

					BattleUser lInputUser = pFrameTickContext.aUsers[lInputUserIdx];
					CommanderItem lSpawnCommanderItem = lInputUser.aUserInfo.mCommanderItems[lSpawnCommanderItemSlotIdx];
					if (!lInputUser.CheckSpawnable(lSpawnCommanderItem))
						break;

					Coord2 lSpawnMoveCoord
						= (lInputType == BattleFrameInputType.SpawnCommander)
						? new Coord2(lFrameInput.mX, lFrameInput.mY)
						: lInputUser.aHeadquarter.aSpawnCoord;
					int lSpawnDirectionY
						= (lSpawnCommanderItem.mCommanderSpec.mSpawnType == CommanderSpawnType.Headquarter)
						? BattleCommander.GetFormationMoveDirectionY(lSpawnMoveCoord, lInputUser.aHeadquarter.aSpawnCoord)
						: lInputUser.aHeadquarter.aDirectionY;

					int lLastTroopCount = lInputUser.aCurrentTroopCount;
					BattleCommander lSpawnCommander = pFrameTickContext.aBattleGrid.SpawnCommander(
						lInputUser,
						lSpawnCommanderItem,
						lSpawnMoveCoord,
						lSpawnDirectionY,
						lInputUser);
					if (lSpawnCommander == null)
						break;

					pFrameTickContext.OnFrameInputSpawnCommander(lSpawnCommander, lLastTroopCount);

					// 스폰 이후의 추가 처리를 합니다.
					switch (lInputType)
					{
					case BattleFrameInputType.SpawnCommanderAndAttackTroop:
						{
							int lTargetTroopId = lFrameInput.mX;
							uint lTargetTroopGuid = (uint)lFrameInput.mY;

							BattleTroop lTargetTroop = pFrameTickContext.aBattleGrid.GetTroop(lTargetTroopId, lTargetTroopGuid);
							if (lTargetTroop != null)
								lSpawnCommander.AttackTargetTroop(lTargetTroop);
						}
						break;
					case BattleFrameInputType.SpawnCommanderAndGrabCheckPoint:
						{
							int lGrabCheckPointIdx = lFrameInput.mX;

							BattleCheckPoint lCheckPoint = pFrameTickContext.aBattleGrid.aCheckPointList[lGrabCheckPointIdx];
							lSpawnCommander.GrabCheckPoint(lCheckPoint);
						}
						break;
					}
				}
				break;
			case BattleFrameInputType.MoveTroops:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;
					Coord2 lMoveCoord = new Coord2(lFrameInput.mX, lFrameInput.mY);
					int lMoveDirectionY = lFrameInput.mZ;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					if (lInputCommander.aIsFleeing)
						break; // 도망중이라면 입력 무시
					lInputCommander.Retreat(false); // 해당 입력을 받으면 퇴각 취소

					if (lMoveDirectionY >= 0) // 0이상일 경우 명시적 지정
						lInputCommander.MoveTroops(lMoveCoord, lMoveDirectionY);
					else
						lInputCommander.MoveTroops(lMoveCoord);
					lInputCommander.UngrabCheckPoint();
				}
				break;
			case BattleFrameInputType.AttackTroop:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;
					int lTargetTroopId = lFrameInput.mX;
					uint lTargetTroopGuid = (uint)lFrameInput.mY;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					if (lInputCommander.aIsFleeing)
						break; // 도망중이라면 입력 무시
					lInputCommander.Retreat(false); // 해당 입력을 받으면 퇴각 취소

					BattleTroop lTargetTroop = pFrameTickContext.aBattleGrid.GetTroop(lTargetTroopId, lTargetTroopGuid);
					if (lTargetTroop != null)
					{
						lInputCommander.AttackTargetTroop(lTargetTroop);
						lInputCommander.UngrabCheckPoint();
					}
				}
				break;
			case BattleFrameInputType.GrabCheckPoint:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;
					int lGrabCheckPointIdx = lFrameInput.mX;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					if (lInputCommander.aIsFleeing)
						break; // 도망중이라면 입력 무시
					lInputCommander.Retreat(false); // 해당 입력을 받으면 퇴각 취소

					BattleCheckPoint lCheckPoint = pFrameTickContext.aBattleGrid.aCheckPointList[lGrabCheckPointIdx];
					lInputCommander.GrabCheckPoint(lCheckPoint);
				}
				break;
			case BattleFrameInputType.RetreatCommander:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					lInputCommander.Retreat(true);
				}
				break;
			case BattleFrameInputType.CommanderSkill:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;
					bool lIsStartSkill = (lFrameInput.mC == 1);
					Coord2 lSkillTargetCoord = new Coord2(lFrameInput.mX, lFrameInput.mY);
					int lSkillDirectionY = lFrameInput.mZ;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					if (lInputCommander.aIsFleeing)
						break; // 도망중이라면 입력 무시
					lInputCommander.Retreat(false); // 해당 입력을 받으면 퇴각 취소

					if (lIsStartSkill)
						lInputCommander.aSkill.StartSkill(lSkillTargetCoord, lSkillDirectionY, 0);
					else
						lInputCommander.aSkill.StopSkill(0);
				}
				break;
			case BattleFrameInputType.GoodGame:
				{
					int lInputUserIdx = lFrameInput.mA;

					BattleUser lInputUser = pFrameTickContext.aUsers[lInputUserIdx];
					//Debug.Log("GoodGame - " + lInputUser);

					pFrameTickContext.aProgressInfo.SetGiveUpUser(lInputUser);

					pFrameTickContext.OnFrameInputGoodGame(lInputUser);
				}
				break;
			case BattleFrameInputType.ForceAi:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;
					bool lIsForceAi = (lFrameInput.mC == 1);

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					lInputCommander.UpdateAiState(lIsForceAi);
				}
				break;
			case BattleFrameInputType.DebugDestroyCommander:
				{
					int lCommanderId = lFrameInput.mA;
					uint lCommanderGuid = (uint)lFrameInput.mB;

					BattleCommander lInputCommander = pFrameTickContext.aBattleGrid.GetCommander(lCommanderId, lCommanderGuid);
					if (lInputCommander == null)
						break;

					lInputCommander.Destroy();
				}
				break;
			case BattleFrameInputType.CheatWin:
				{
					//int lTeamIdx = lFrameInput.mA;
					int lUserIdx = lFrameInput.mB;

					pFrameTickContext.aBattleGrid.aProgressInfo.SetBattleEnd(pFrameTickContext.aBattleGrid, lUserIdx, BattleEndCode.PlayWin);
				}
				break;
			case BattleFrameInputType.CheatLose:
				{
					//int lTeamIdx = lFrameInput.mA;
					int lUserIdx = lFrameInput.mB;

					pFrameTickContext.aBattleGrid.aProgressInfo.SetBattleEnd(pFrameTickContext.aBattleGrid, lUserIdx, BattleEndCode.PlayLose);
				}
				break;
			}
		}

		// 서버에서 받은 블리츠 입력을 처리합니다.
// 		for (int iInput = 0; iInput < pFrameTick.mBlitzInputCount; ++iInput)
// 		{
// 			BattleBlitzInput lBlitzInput = pFrameTick.mBlitzInputs[iInput];
// 			Debug.Log("[" + pFrameTick.mTickNo + "] lBlitzInput=" + lBlitzInput);
// 
// 			BattleTroop lInputTroop = vCombatGrid.aBattleGrid.FindTroopByGuid(lBlitzInput.mBlitzTroopId);
// 			if (lInputTroop != null)
// 			{
// 				// 부대에게 명령을 내립니다.
// 				lInputTroop.CommandBlitz(lBlitzInput);
// 			}
// 			else
// 				Debug.Log("lInputTroop is null lBlitzInput.mBlitzTroopId=" + lBlitzInput.mBlitzTroopId);
// 		}
	}
}
