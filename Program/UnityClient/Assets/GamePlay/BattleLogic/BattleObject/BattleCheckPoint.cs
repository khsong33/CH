using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleCheckPoint : BattleObject
{
	public delegate void OnTryTeamChangeDelegate(BattleCommander pTryCommander, BattleCheckPoint pCheckPoint, bool pIsTrying);
	public delegate void OnTeamChangeDelegate(BattleCheckPoint pCheckPoint);
	public delegate void OnTouchCheckPointDelegate(BattleCommander pBattleCommander, BattleCheckPoint pCheckPoint);

	internal const int cUpdateCancelCheckDelay = 1000;
	internal const String cTeamChangeText = "Raise Flag";

	internal enum FlagState
	{
		Neutral,
		NeutralToTeam,
		Team,
		TeamToNeutral,
	}

	public enum CheckPointType
	{
		Recruit = 0,
		Observatory = 1,
		Hospital = 2,
		Repair = 3,
		BroadCastTower = 4,
		MpGeneration = 5,
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 인덱스
	public int aCheckPointIdx
	{
		get { return mCheckPointIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// GUID
	public uint aCheckPointGuid
	{
		get { return mCheckPointGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령했을 경우 증가하는 MP량
	public int aMilliMpGeneration
	{
		get { return mMilliMpGeneration; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령했을 경우 적의 VP를 깍는 양
	public int aEnemyMilliVpDecrease
	{
		get { return mEnemyMilliVpDecrease; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 별 번호
	public int aStarId
	{
		get { return mStarId; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageCheckPoint aStageCheckPoint
	{
		get { return mStageCheckPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageCheckPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnTryTeamChangeDelegate aOnTryTeamChangeDelegate { get; set; }
	public OnTeamChangeDelegate aOnTeamChangeDelegate { get; set; }
	public OnTouchCheckPointDelegate aOnTouchCheckPointDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 연결 본부
	public BattleHeadquarter aLinkHeadquarter
	{
		get { return mLinkHeadquarter; }
		set
		{
			if (mLinkHeadquarter == value)
				return;

			mLinkHeadquarter = value;
			if (mLinkHeadquarter != null)
				UpdateTeam(mLinkHeadquarter.aTeamIdx); // 본부 거점의 팀으로 자신의 팀을 변경
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 점령 시도중인 부대
	public BattleCommander aTryCaptureCommander
	{
		get { return mTryCaptureCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제로 방어 가능한 상태인지 여부
	public bool aIsValidDefender
	{
		get { return ((mCurrentDefenderCommander != null) && (mCurrentDefenderCommander.aLiveTroopCount > 0)); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 타입
	public BattleCheckPoint.CheckPointType aCheckPointType
	{
		get { return mCheckPointType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 거점에 연결되어 있는 지휘관의 슬롯 번호
	public int aCommanderSlotIdx
	{
		get { return (BattleGrid.cCheckPointCommanderItemStartIdx + mCheckPointIdx); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 거점의 현재 방어군
	public BattleCommander aCurrentDefenderBattleCommander
	{
		get { return mCurrentDefenderCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("CP<{0},{1}>(aGuid:{2} aTeamIdx:{3})", aCoord.x, aCoord.z, aGuid, aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleCheckPoint(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateCheckPoint(
		BattleGridTile pGridTile,
		int pCheckPointIdx,
		uint pCheckPointGuid,
		int pMilliMpGeneration,
		int pEnemyMilliVpDecrease,
		int pStarId,
		CheckPointType pCheckPointType,
		String[] pDefenderCommanders,
		int pUserIdx)
	{
		OnCreateObject(
			pGridTile.aBattleGrid,
			false, // bool pIsCheckDetection
			false, // bool pIsDetecting
			pGridTile.aBattleGrid.aIsNoneDetectables, // bool pIsDetectables
			pGridTile.aCenterCoord,
			pGridTile.aDirectionY,
			0); // int pTeamIdx

		mCheckPointIdx = pCheckPointIdx;
		mCheckPointGuid = pCheckPointGuid;

		mCheckPointType = pCheckPointType;

		mMilliMpGeneration = pMilliMpGeneration;
		mEnemyMilliVpDecrease = pEnemyMilliVpDecrease;
		mStarId = pStarId;

		mFlagState = FlagState.Neutral;
		mFlagTeamIdx = aTeamIdx;
		mFlagTimer = 0; // 중립 팀으로 초기화

		RepeatUpdate(); // OnFrameUpdate()

		mDefenderCommanderItem = new CommanderItem[(int)DeckNation.Count];
		for (int iCommander = 0; iCommander < (int)DeckNation.Count; iCommander++)
		{
			if (pDefenderCommanders[iCommander].ToUpper().Equals("NONE"))
			{
				mDefenderCommanderItem[iCommander] = null;				
			}
			else
			{
				CommanderSpec lDefenderCommanderSpec = CommanderTable.get.FindCommanderSpec(pDefenderCommanders[iCommander]);
				mDefenderCommanderItem[iCommander] = new CommanderItem(BattleGrid.cCheckPointCommanderItemStartIdx + aCheckPointIdx);
				int lSpawnCount = 999; // 방어기지 지휘관은 무제한 소환
				mDefenderCommanderItem[iCommander].Init(CommanderItemType.DefenderCommander, lDefenderCommanderSpec, -1, 0, 1, lSpawnCount);
			}
		}
//		mIsNotRespawnDefender = (mMilliMpGeneration == 0 && mEnemyMilliVpDecrease == 0);

		if (mCaptureTryCommanderDic == null)
			mCaptureTryCommanderDic = new Dictionary<int, BattleCommander>();
		mCaptureTryCommanderDic.Clear();

		if (mCheckPointTryCommanderDic == null)
			mCheckPointTryCommanderDic = new Dictionary<int, BattleCommander>();
		mCheckPointTryCommanderDic.Clear();

		mTryCaptureCommander = null;
		mCurrentDefenderCommander = null;

		mStageCheckPoint = aBattleGrid.aStage.CreateCheckPoint(this);

		mDefaultUserIdx = pUserIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 스테이지에서 제거합니다.
		mStageCheckPoint.DestroyObject();
		mStageCheckPoint = null;

		// 이벤트 콜백을 삭제합니다.
		aOnTryTeamChangeDelegate  = null;
		aOnTeamChangeDelegate = null;
		aOnTouchCheckPointDelegate = null;

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyCheckPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateTeam(int pTeamIdx)
	{
		if (aTeamIdx == pTeamIdx)
			return;

		// 숨겨진 부대를 가지고 있던 지휘관은 삭제합니다.
		if (mCurrentDefenderCommander != null)
		{
			mCurrentDefenderCommander.Destroy();
			mCurrentDefenderCommander = null;
		}

		// 팀을 갱신합니다.
		base.UpdateTeam(pTeamIdx);

		mFlagState = (pTeamIdx <= 0) ? FlagState.Neutral : FlagState.Team;
		mFlagTeamIdx = pTeamIdx;
		mFlagTimer = (pTeamIdx <= 0) ? 0 : BattleConfig.get.mCheckPointTeamChangeDelay;

		// 연결 본부가 있다면 같이 팀을 변경합니다.
		if (mLinkHeadquarter != null)
			mLinkHeadquarter.UpdateTeam(aTeamIdx);

		// 콜백을 호출합니다.
		if (aOnTryTeamChangeDelegate != null)
			aOnTryTeamChangeDelegate(aTryCaptureCommander, this, false); // 점령 완료이므로 시도도 끝남

		if (aOnTeamChangeDelegate != null)
			aOnTeamChangeDelegate(this);

		if ((aTryCaptureCommander != null) && // 깃발 올리는 지휘관이 있고
			(aTeamIdx == aTryCaptureCommander.aTeamIdx)) // 거점이 해당 지휘관의 팀으로 완전히 바뀐 것이라면
			aTryCaptureCommander.UngrabCheckPoint(); // 지휘관의 거점 점령 시도를 해제합니다.

		// 팀이 바뀌면 보충 팀 예약은 모두 초기화됩니다.
		if(mCheckPointTryCommanderDic != null)
			mCheckPointTryCommanderDic.Clear();

		// 스테이지에 보입니다.
		mStageCheckPoint.ResetTeamChange(pTeamIdx);
		mStageCheckPoint.UpdateTeam(aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
	{
		return (pGridTouch == BattleGrid.Touch.OnClick);
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameObject 콜백
	public override void OnFrameUpdate(FrameTask pTask, int pFrameDelta)
	{
		_TryTeamChange(pFrameDelta);
		_TryExcuteType();
	}
	//-------------------------------------------------------------------------------------------------------
	// 기본 방어 부대를 생성합니다.
	public void SpawnDefaultDefender()
	{
		// 본부와 연결되어 있는 체크포인트만 유저의 부대를 생성.
		if (aLinkHeadquarter != null)
		{
			DeckNation lBattleNation = aLinkHeadquarter.aLinkUser.aUserInfo.mBattleNation;
			_SpawnDefenderTroop(mDefenderCommanderItem[(int)lBattleNation], aLinkHeadquarter.aLinkUser);
		}
		else
		{
			if (mDefaultUserIdx < 0)
			{
				_SpawnDefenderTroop(mDefenderCommanderItem[(int)DeckNation.None], null);
			}
			else
			{
				mConquerUser = aBattleGrid.aUsers[mDefaultUserIdx];
				UpdateTeam(mConquerUser.aTeamIdx);
				_SpawnDefenderTroop(mDefenderCommanderItem[(int)mConquerUser.aUserInfo.mBattleNation], mConquerUser);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 시도를 요청한 지휘관을 추가합니다.
	public void AddTryCaptureCheckPoint(BattleCommander pCommander)
	{
		if (!mCaptureTryCommanderDic.ContainsKey(pCommander.aId))
			mCaptureTryCommanderDic.Add(pCommander.aId, pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 시도를 요청한 지휘관을 제거합니다.
	public void RemoveTryCaptureCheckPoint(BattleCommander pCommander)
	{
		if (mCaptureTryCommanderDic.ContainsKey(pCommander.aId))
		{
			mCaptureTryCommanderDic.Remove(pCommander.aId);
		}

		if (mTryCaptureCommander != null &&
			mTryCaptureCommander.aId == pCommander.aId)
		{
			mTryCaptureCommander = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 충원을 예약합니다.
	public void AddTryRecruitTroop(BattleCommander pCommander)
	{
		if (!mCheckPointTryCommanderDic.ContainsKey(pCommander.aId))
			mCheckPointTryCommanderDic.Add(pCommander.aId, pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 충원을 예약을 해지합니다..
	public void RemoveTryRecruitTroop(BattleCommander pCommander)
	{
		if (mCheckPointTryCommanderDic.ContainsKey(pCommander.aId))
			mCheckPointTryCommanderDic.Remove(pCommander.aId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 가능한지 확인합니다.
	public bool IsConquer(int pTeamIdx)
	{
		if (aTeamIdx == pTeamIdx)
			return false;
		if (aIsValidDefender)
			return false;
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 충원 가능한지 확인합니다.
	public bool IsRecruit(int pTeamIdx)
	{
		if (aTeamIdx != pTeamIdx)
			return false;
		if (mCheckPointType != CheckPointType.Recruit)
			return false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까이 있는 방어 부대를 얻습니다.
	public BattleTroop GetNearestDefenderTroop(Coord2 pCoord, int pSearchingTeamIdx)
	{
		if (mCurrentDefenderCommander != null)
			return mCurrentDefenderCommander.GetNearestTroop(aCoord, pSearchingTeamIdx);
		else
			return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 방어 부대를 소환합니다.
	public void SpawnDefenderTroop(DeckNation pNation, BattleUser pTeamUser)
	{
		_SpawnDefenderTroop(mDefenderCommanderItem[(int)pNation], pTeamUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// 체크 포인트 클릭시 처리
	public void TouchCheckPoint(BattleCommander pBattleCommander)
	{
		if (aOnTouchCheckPointDelegate != null)
			aOnTouchCheckPointDelegate(pBattleCommander, this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 팀 변화를 시도합니다.
	private void _TryTeamChange(int pFrameDelta)
	{
		if ((aFrameUpdater.aFrameIdx % aBattleGrid.aCheckPointList.Count) == mCheckPointIdx) // 매 프레임마다 찾지 않고 한 프레임에 하나의 거점만 검사
		{
			sInterruptRangeTeamCount = 0;
			mTryTeamIdx = 0;
			mIsOwnerTeamCapturing = false;

			aBattleGrid.CallTroopHandler(
				aCoord,
				BattleConfig.get.mCheckPointInterruptRange,
				-1,
				BattleGrid.TeamSelection.NonNeutralTeam,
				_OnTryTeamChange);

			if (mTryTeamIdx > 0)
			{
				for (int iTeam = 0; iTeam < sInterruptRangeTeamCount; ++iTeam)
					if (sInterruptRangeTeamIdxs[iTeam] != mTryTeamIdx)
					{
						mTryTeamIdx = 0;
						break;
					}
			}
		}

		switch (mFlagState)
		{
		case FlagState.Neutral:
			{
				if (mTryTeamIdx > 0) // 점령 시도 팀이 있다면
				{
					mFlagTeamIdx = mTryTeamIdx;
					mFlagState = FlagState.NeutralToTeam; // 점령 진행 상태로

					mStageCheckPoint.ResetTeamChange(mFlagTeamIdx); // 스테이지에 보입니다.

					if (aOnTryTeamChangeDelegate != null)
						aOnTryTeamChangeDelegate(aTryCaptureCommander, this, true); // 콜백을 호출합니다.

					_ProceedNeutralToTeam(pFrameDelta);
				}
			}
			break;
		case FlagState.NeutralToTeam:
			{
				_ProceedNeutralToTeam(pFrameDelta);
			}
			break;
		case FlagState.Team:
			{
				if ((mTryTeamIdx > 0) && // 시도중인 팀이 있는데
					(mTryTeamIdx != mFlagTeamIdx)) // 깃발과 다르다면
				{
					mFlagState = FlagState.TeamToNeutral; // 탈취 진행 상태로

					if (aOnTryTeamChangeDelegate != null)
						aOnTryTeamChangeDelegate(aTryCaptureCommander, this, true); // 콜백을 호출합니다.

					_ProceedTeamToNeutral(pFrameDelta);
				}
			}
			break;
		case FlagState.TeamToNeutral:
			{
				_ProceedTeamToNeutral(pFrameDelta);
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 병력 증원을 진행합니다.
	private void _TryExcuteType()
	{
		if (aTeamIdx <= 0)
			return;

		if (mCheckPointType == CheckPointType.Recruit)
		{
			if ((aFrameUpdater.aFrameIdx % aBattleGrid.aCheckPointList.Count) == mCheckPointIdx) // 매 프레임마다 찾지 않고 한 프레임에 하나의 거점만 검사
				aBattleGrid.CallTroopHandler(
					aCoord,
					BattleConfig.get.mCheckPointInterruptRange,
					aTeamIdx,
					BattleGrid.TeamSelection.AllyTeam,
					_OnTryRecruitTroop);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnObjectHandlingDelegate<BattleTroop>
	private bool _OnTryRecruitTroop(BattleTroop pTryTroop, int lSqrTryTroopDistance)
	{
		if (!mCheckPointTryCommanderDic.ContainsKey(pTryTroop.aCommander.aId))
			return true;

		if (lSqrTryTroopDistance <= BattleConfig.get.mSqrCheckPointCaptureRange) // 점령 시도 가능 거리라면
		{
			pTryTroop.aCommander.OnRecruitTroop();
			RemoveTryRecruitTroop(pTryTroop.aCommander);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 진행을 처리합니다.
	private void _ProceedNeutralToTeam(int pFrameDelta)
	{
		if (mTryTeamIdx == mFlagTeamIdx) // 깃발과 같은 팀이 계속 시도중이라면
		{
			if (!mIsTryingTeamChange)
			{
				mIsTryingTeamChange = true;

				if (aOnTryTeamChangeDelegate != null)
					aOnTryTeamChangeDelegate(aTryCaptureCommander, this, true); // 시작 콜백을 호출합니다.
			}

			mFlagTimer += pFrameDelta; // 깃발은 점점 올라가고
			if (mFlagTimer < BattleConfig.get.mCheckPointTeamChangeDelay) // 그동안은
			{
				float lChangeFactor = (float)mFlagTimer / BattleConfig.get.mCheckPointTeamChangeDelay;
				mStageCheckPoint.TryTeamChange(mFlagTeamIdx, lChangeFactor); // 스테이지에 점령 진행을 보입니다.
			}
			else // 그러다가 일정 시간 후에는
			{
				UpdateTeam(mFlagTeamIdx); // 해당 팀이 점령합니다(내부에서 FlagState.Team으로 바뀜).
				_OnConquerCheckPoint();
				mConquerUser = null;
			}
		}
		else // 깃발과 다른 팀이라면
		{
			if (mIsTryingTeamChange)
			{	
				mIsTryingTeamChange = false;

				if (aOnTryTeamChangeDelegate != null)
					aOnTryTeamChangeDelegate(aTryCaptureCommander, this, false); // 중단 콜백을 호출합니다.
			}

			mFlagTimer -= pFrameDelta; // 깃발은 점점 내려가고
			if (mFlagTimer > 0) // 그동안은
			{
				float lChangeFactor = (float)mFlagTimer / BattleConfig.get.mCheckPointTeamChangeDelay;
				mStageCheckPoint.TryTeamChange(mFlagTeamIdx, lChangeFactor); // 스테이지에 점령 진행을 보입니다.
			}
			else // 완전히 내려가면
			{
				mFlagTimer = 0;
				mFlagState = FlagState.Neutral; // 팀 중립 상태로 돌아갑니다.

				mStageCheckPoint.ResetTeamChange(0); // 스테이지에 보입니다.
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 진행을 처리합니다.
	private void _ProceedTeamToNeutral(int pFrameDelta)
	{
		if ((mTryTeamIdx > 0) && // 시도중인 팀이 있는데
			(mTryTeamIdx != mFlagTeamIdx) && // 깃발과 다르고
			!mIsOwnerTeamCapturing) // 주인 팀이 없다면
		{
			if (!mIsTryingTeamChange)
			{
				mIsTryingTeamChange = true;

				if (aOnTryTeamChangeDelegate != null)
					aOnTryTeamChangeDelegate(aTryCaptureCommander, this, true); // 시작 콜백을 호출합니다.
			}

			mFlagTimer -= pFrameDelta; // 깃발은 점점 내려가고
			if (mFlagTimer > 0) // 그동안은
			{
				float lChangeFactor = (float)mFlagTimer / BattleConfig.get.mCheckPointTeamChangeDelay;
				mStageCheckPoint.TryTeamChange(mFlagTeamIdx, lChangeFactor); // 스테이지에 점령 진행을 보입니다.
			}
			else // 그러다가 일정 시간 후에는
			{
				UpdateTeam(0); // 팀을 갱신합니다(내부에서 FlagState.Team으로 바뀜).
			}
		}
		else // 다른 팀의 점령 시도가 없다면
		{
			if (mIsTryingTeamChange)
			{
				mIsTryingTeamChange = false;

				if (aOnTryTeamChangeDelegate != null)
					aOnTryTeamChangeDelegate(aTryCaptureCommander, this, false); // 중단 콜백을 호출합니다.
			}

			mFlagTimer += pFrameDelta; // 깃발은 점점 올라가고
			if (mFlagTimer < BattleConfig.get.mCheckPointTeamChangeDelay) // 그동안은
			{
				float lChangeFactor = (float)mFlagTimer / BattleConfig.get.mCheckPointTeamChangeDelay;
				mStageCheckPoint.TryTeamChange(mFlagTeamIdx, lChangeFactor); // 스테이지에 점령 진행을 보입니다.
			}
			else // 완전히 올라가면
			{
				mFlagTimer = BattleConfig.get.mCheckPointTeamChangeDelay;
				mFlagState = FlagState.Team; // 팀 점령 상태로 돌아갑니다.

				mStageCheckPoint.ResetTeamChange(mFlagTeamIdx); // 스테이지에 보입니다.
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnObjectHandlingDelegate<BattleTroop>
	private bool _OnTryTeamChange(BattleTroop pTryTroop, int lSqrTryTroopDistance)
	{
		if ((pTryTroop.aTeamIdx == aTeamIdx) && // 거점 팀이
			!pTryTroop.aIsNpc && // NPC가 아니고
			(lSqrTryTroopDistance <= BattleConfig.get.mSqrCheckPointCaptureRange)) // 점령 시도 가능 거리에 있다면
		{
			mIsOwnerTeamCapturing = true; // 플래그로 기억
			return false; // 다른 부대에 대한 처리를 할 필요 없음
		}

		if (mTryTeamIdx < 0)
			return false; // 점령 시도 범위에 여러 팀이 있는 상태로서 이후 처리가 불필요
		if (mTryTeamIdx == pTryTroop.aTeamIdx)
			return true; // 같은 팀이 이미 시도중
		if (pTryTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
			return true; // 혼란 상태의 부대는 영향 없음
		if (pTryTroop.aIsFallBehind)
			return true; // 복귀중인 부대는 영향 없음
		
		if (!mCaptureTryCommanderDic.ContainsKey(pTryTroop.aCommander.aId))
			return true; // 점령 시도를 하지 않은 부대에 대해선 영향 없음
		if ((mTryCaptureCommander != null) &&
			(mTryCaptureCommander.aId != pTryTroop.aCommander.aId))
			return true; // 점령중인 부대가 있을 때 해당 부대가 자신의 부대가 아니면 무시합니다.

		if (lSqrTryTroopDistance <= BattleConfig.get.mSqrCheckPointCaptureRange) // 점령 시도 가능 거리라면
		{
			if (mTryTeamIdx == 0) // 시도 팀이 없다면
			{
				if (pTryTroop.aTroopSpec.mIsCaptureType) // 점령이 가능한 부대라면
				{
					mTryTeamIdx = pTryTroop.aTeamIdx; // 점령 시도 팀으로 지정
					mConquerUser = pTryTroop.aCommander.aTeamUser;
					mTryCaptureCommander = pTryTroop.aCommander;
				}
				else // 점령이 불가능한 부대라면
				{
					_AddInterruptTeam(pTryTroop.aTeamIdx); // 이 팀을 방해 팀 리스트에 등록
				}
			}
			else // 이미 다른 팀의 시도가 있었다면
			{
				mTryTeamIdx = -1; // 어떤 팀도 점령 불가 상태
			}
		}
		else // 점령 방해만 가능한 거리라면
		{
			_AddInterruptTeam(pTryTroop.aTeamIdx); // 이 팀을 방해 팀 리스트에 등록
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [중간 계산용 함수] 방해 범위에 있는 팀 리스트에 등록합니다.
	private static void _AddInterruptTeam(int pTeamIdx)
	{
		for (int iTeam = 0; iTeam < sInterruptRangeTeamCount; ++iTeam)
			if (sInterruptRangeTeamIdxs[iTeam] == pTeamIdx)
				return; // 이미 있음

		sInterruptRangeTeamIdxs[sInterruptRangeTeamCount++] = pTeamIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// CheckPoint 점령을 완료합니다.
	private void _OnConquerCheckPoint()
	{
		if (mConquerUser == null)
			return;

		// 부대를 소환합니다.
		DeckNation lBattleNation = mConquerUser.aUserInfo.mBattleNation;
		_SpawnDefenderTroop(mDefenderCommanderItem[(int)lBattleNation], mConquerUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// 방어 부대를 소환합니다.
	private void _SpawnDefenderTroop(CommanderItem pDefenderCommanderItem, BattleUser pTeamUser)
	{
// [테스트] 방어 부대 소환 안 함
// 		return;

		if (pDefenderCommanderItem == null)
			return;

		mCurrentDefenderCommander = aBattleGrid.SpawnCommander(
			null,
			pDefenderCommanderItem,
			aCoord,
			aDirectionY,
			pTeamUser);
		mCurrentDefenderCommander.SetLinkCheckPoint(this);
		mCurrentDefenderCommander.aOnDestroyingDelegate += _OnDestroyingDefenderCommander;
		mCurrentDefenderCommander.aOnDestroyedTroopDelegate += _OnDestroyedDefenderCommanderTroop;
		mCurrentDefenderCommander.aIsDestroyOnNoTroop = false;

		// 방어 부대는 소환되면서 스킬을 바로 사용합니다.
		if (mCurrentDefenderCommander.aSkill.aSkillSpec != null)
			mCurrentDefenderCommander.aSkill.StartSkill(aCoord, aDirectionY, 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyingDelegate
	private void _OnDestroyingDefenderCommander(BattleCommander pCommander)
	{
		if(pCommander == mTryCaptureCommander)
			mTryCaptureCommander = null;

		mCurrentDefenderCommander = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroop.OnDestroyedTroopDelegate
	private void _OnDestroyedDefenderCommanderTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		//Debug.Log("_OnDestroyTroop() pTroop=" + pTroop);

		if (mCurrentDefenderCommander.aTroopSet.mTroopInfos[pTroopSlotIdx].mCommanderTroopType == CommanderTroopType.Npc)
			return;

		// 다른 NPC 부대를 파괴합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if (mCurrentDefenderCommander.aTroops[iTroop] != null)
			{
				BattleTroop lDefenderTroop = mCurrentDefenderCommander.aTroops[iTroop];
				if (lDefenderTroop.aIsNpc)
					lDefenderTroop.Destroy(); // NPC 부대 파괴
				else
					return; // 다른 방어 부대 존재
			}

		// 유저가 점령한 부대가 파괴된 자리에는 시야를 위한 더미 부대를 생성합니다.
		if (mCurrentDefenderCommander.aTeamUser != null)
		{
			CommanderTroopInfo lTroopInfo = mCurrentDefenderCommander.aTroopSet.mTroopInfos[pTroopSlotIdx];
			Coord2 lTroopSpawnOffset = Coord2.RoughRotationVector(
				mCurrentDefenderCommander.aTroopSet.mFormationPositionOffsets[pTroopSlotIdx],
				mCurrentDefenderCommander.aDirectionY);
			Coord2 lTroopSpawnCoord = mCurrentDefenderCommander.aCoord + lTroopSpawnOffset;

			int lTroopGridIdx = aBattleGrid.GetCommanderTroopGridIdx(mCurrentDefenderCommander, pTroopSlotIdx);

			aBattleLogic.CreateTroop(
				aBattleGrid,
				lTroopGridIdx,
				lTroopInfo.mTroopSpec,
				lTroopSpawnCoord,
				mCurrentDefenderCommander.aDirectionY,
				mCurrentDefenderCommander,
				pTroopSlotIdx,
				BattleTroop.StartState.Observer);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static int[] sInterruptRangeTeamIdxs = new int[BattleMatchInfo.cMaxTeamCount];
	private static int sInterruptRangeTeamCount;

	private int mCheckPointIdx;
	private uint mCheckPointGuid;

	private BattleCheckPoint.CheckPointType mCheckPointType;

	private int mMilliMpGeneration;
	private int mEnemyMilliVpDecrease;
	private int mStarId;

	private IStageCheckPoint mStageCheckPoint;

	private FlagState mFlagState;
	private int mFlagTeamIdx;
	private int mFlagTimer;

	private Dictionary<int, BattleCommander> mCaptureTryCommanderDic;
	private Dictionary<int, BattleCommander> mCheckPointTryCommanderDic;
	private int mTryTeamIdx;
	private bool mIsOwnerTeamCapturing;
	private bool mIsTryingTeamChange;

	private BattleHeadquarter mLinkHeadquarter;

	private BattleUser mConquerUser;
	private BattleCommander mTryCaptureCommander;
	private CommanderItem[] mDefenderCommanderItem;
	private BattleCommander mCurrentDefenderCommander;
	private int mDefaultUserIdx;
//	private bool mIsNotRespawnDefender; // 한번 생성된 후 전멸시 다시 생성되지 않는 방어 부대
}
