using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleCommander : BattleObject, IBattleAiCommanderData
{
	public enum BelongingType
	{
		User,
		CheckPoint,
		EventPoint,
	}
	public enum FormationState
	{
		DefaultFormation,
		SkillFormation,
	}
	public enum TimeOnTargetState
	{
		None,
		Wait,
		Ready,
		Fire,
	}

	public const int cCheckPointUserId = 98;
	public const int cEventPointUserId = 99;

	public delegate void OnSpawnTroopsDelegate(BattleCommander pCommander);
	public delegate void OnMoveStartDelegate(BattleCommander pCommander);
	public delegate void OnMoveStopDelegate(BattleCommander pCommander);
	public delegate void OnDestroyingDelegate(BattleCommander pCommander);
	public delegate void OnDestroyedDelegate(int pScriptUserId, int pScriptSlotId);
	public delegate void OnDestroyingTroopDelegate(BattleCommander pCommander, int pTroopSlotIdx);
	public delegate void OnDestroyedTroopDelegate(BattleCommander pCommander, int pTroopSlotIdx);
	public delegate void OnRecruitTroopsDelegate(BattleCommander pCommander);
	public delegate void OnVeterancyRankUpDelegate(BattleCommander pCommander);
	public delegate void OnVeterancyExpDelegate(BattleCommander pCommander);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aId
	{
		get { return mCommanderGridIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aAiUserIdx
	{
		get { return mAiUserIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public bool aIsActiveAi
	{
		get { return (mActiveAiManager != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public uint aClientGuid
	{
		get { return ((mUserInfo != null) ? mUserInfo.mClientGuid : 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	//public int aTeamIdx // BattleObject에서 구현
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public bool aIsAlive
	{
		get { return (mLiveTroopCount > 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 활중중인 부대 수
	public int aLiveTroopCount
	{
		get { return mLiveTroopCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public bool aIsCoolTime
	{
		get { return mIsCoolTime; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public bool aIsBattle { set; get; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aSpawnMp
	{
		get { return mCommanderItem.aSpawnMp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aHpPercent
	{
		get { return mCurTotalMilliHp * 100 / mMaxTotalMilliHp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
//	public Coord2 aCoord // BattleObject에서 구현
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public BattleTroop[] aTroops
	{
		get { return mTroops; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public BattleTroop aLeaderTroop
	{
		get { return mLeaderTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aSummonOrder
	{
		get { return mCommanderSpec.mSummonOrder; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public CommanderType aCommanderType
	{
		get { return mCommanderSpec.mType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public CommanderClass aCommanderClass
	{
		get { return mCommanderSpec.mClass; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public bool aIsCaptureCheckPoint
	{
		get { return mIsCaptureTypeTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aUpdateIndex { set; get; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 속성
	public int aOriginUpdateIndex { set; get; }
	//-------------------------------------------------------------------------------------------------------
	// 활성화된 AI (유저 지휘관이고 자동 플레이일 경우에만 존재)
	public BattleAiManager aActiveAiManager
	{
		get { return mActiveAiManager; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저
	public BattleUser aUser
	{
		get { return mUser; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀을 결정하는 유저
	public BattleUser aTeamUser
	{
		get { return mTeamUser; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보
	public BattleUserInfo aUserInfo
	{
		get { return mUserInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템
	public CommanderItem aCommanderItem
	{
		get { return mCommanderItem; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 스펙
	public CommanderSpec aCommanderSpec
	{
		get { return mCommanderSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 세트
	public CommanderTroopSet aTroopSet
	{
		get { return mTroopSet; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 세트 아이템
	public CommanderTroopSetItem aTroopSetItem
	{
		get { return mTroopSetItem; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 슬롯 인덱스
	public int aCommanderSlotIdx
	{
		get { return mCommanderSlotIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 그리드 (등록) 인덱
	public int aCommanderGridIdx
	{
		get { return mCommanderGridIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 지휘관의 소속 타입
	public BelongingType aBelongingType
	{
		get { return mBelongingType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트에서 인식하는 지휘관의 유저 번호
	public int aScriptUserId
	{
		get { return mScriptUserId; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트에서 인식하는 지휘관의 슬롯 번호
	public int aScriptSlotId
	{
		get { return mScriptSlotId; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태
	public BattleCommanderStatus aStatus
	{
		get { return mStatus; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 방향각 벡터 (크기는 cDirectionVectorSize)
	public Coord2 aDirectionVector
	{
		get { return mDirectionVector; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageCommander aStageCommander
	{
		get { return mStageCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태 변화 콜백
	public OnSpawnTroopsDelegate aOnSpawnTroopsDelegate { get; set; } // 부대가 소환될 때
	public OnMoveStartDelegate aOnMoveStartedDelegate { get; set; } // 부대 이동이 하나라도 시작되었을 때
	public OnMoveStopDelegate aOnMoveStoppedDelegate { get; set; } // 모든 부대의 이동이 끝났을 때
	public OnDestroyingDelegate aOnDestroyingDelegate { get; set; } // 지휘관이 파괴되려 할 때
	public OnDestroyedDelegate aOnDestroyedDelegate { get; set; } // 지휘관이 파괴되고나서
	public OnDestroyingTroopDelegate aOnDestroyingTroopDelegate { get; set; } // 지휘관 소속 부대가 파괴되려 할 때
	public OnDestroyedTroopDelegate aOnDestroyedTroopDelegate { get; set; } // 지휘관 소속 부대가 파괴되고나서
	public OnRecruitTroopsDelegate aOnRecruitTroopsDelegate { get; set; } // 부대 충원을 할 때
	public OnVeterancyRankUpDelegate aOnVeterancyRankUpDelegate { get; set; } // 베태랑 랭크업을 할 때
	public OnVeterancyExpDelegate aOnVeterancyExpDelegate { get; set; } // 베태랑 경험치를 얻었을 때
	//-------------------------------------------------------------------------------------------------------
	// 명시적 타겟 부대
	public BattleTroop aExplicitTargetTroop
	{
		get { return mExplicitTargetTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 명시적 타겟 부대에 대한 접근 거리(제곱값)
	public int aSqrExplicitTargetTroopEncounterDistance
	{
		get { return mSqrExplicitTargetTroopEncounterDistance; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 명시적 타겟 지점
	public Coord2 aExplicitTargetCoord
	{
		get { return mExplicitTargetCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 좌표
	public Coord2 aControlCoord
	{
		get { return mControlCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최고 이동 속도
	public int aMaxAdvanceSpeed
	{
		get { return mMaxAdvanceSpeed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 전체 HP
	public int aMaxTotalMilliHp
	{
		get { return mMaxTotalMilliHp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 전체 HP
	public int aCurTotalMilliHp
	{
		get { return mCurTotalMilliHp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 재소환 비용 MP
	public int aRecruitMp
	{
		get { return mRecruitMp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 시도 거점
	public BattleCheckPoint aTryCheckPoint
	{
		get { return mTryCheckPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환시 받는 부대 번호
	public int aCommanderNumber
	{
		get { return mCommanderItem.mSlotIdx + 1; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 소속 부대 종류별 개수
	public int[] aTroopKindCount
	{
		get { return mTroopKindCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 베테랑 레벨
	public int aVeterancyRank
	{
		get { return mVeterancyRank; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 베테랑 경험치
	public int aVeterancyExperience
	{
		get { return mVeterancyExperience; }
	}
	////-------------------------------------------------------------------------------------------------------
	//// 첫번째 부대 종류 정보
	//public TroopSpec aFirstKindTroopSpec
	//{
	//	get { return TroopTable.get.FindTroopSpec(mTroopSet.mTroopKindCountInfos[0].mTroopId); }
	//}
	////-------------------------------------------------------------------------------------------------------
	//// 두번째 부대 종류 정보
	//public TroopSpec aSecondKindTroopSpec
	//{
	//	get
	//	{
	//		if (mTroopSet.mTroopKindCountInfos[1].mTroopId < 0)
	//			return null;

	//		return TroopTable.get.FindTroopSpec(mTroopSet.mTroopKindCountInfos[1].mTroopId);
	//	}
	//}
	//-------------------------------------------------------------------------------------------------------
	// 부대가 사라지면 같이 사라질지 여부
	public bool aIsDestroyOnNoTroop
	{
		get { return mIsDestroyOnNoTroop; }
		set { mIsDestroyOnNoTroop = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 도망중 여부(취소 가능한 일반 퇴각과는 다름)
	public bool aIsFleeing
	{
		get { return mIsFleeing; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬
	public BattleCommanderSkill aSkill
	{
		get { return mSkill; }
	}
	//-------------------------------------------------------------------------------------------------------
	// TOT 상태
	public TimeOnTargetState aTimeOnTargetState
	{
		get { return mTimeOnTargetState; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 연결된 거점
	public BattleCheckPoint aLinkCheckPoint
	{
		get { return mLinkCheckPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 연결된 이벤트 지점
	public BattleEventPoint aLinkEventPoint
	{
		get { return mLinkEventPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 더미
	public BattleTargetingDummy aTargetingDummy
	{
		get { return mTargetingDummyLink.Get(); }
	}
		
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("BattleCommander<{0},{1}>(aUserIdx:{2} aTeamIdx:{3} aGuid:{4} name:{5} aSlotIdx:{6} aGridIdx:{7})", aCoord.x, aCoord.z, (mUser != null) ? mUser.aUserIdx : -1, aTeamIdx, aGuid, mCommanderSpec.mName, mCommanderSlotIdx, mCommanderGridIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleCommander(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		mStatus = new BattleCommanderStatus();

		mTroops = new BattleTroop[CommanderTroopSet.cMaxTroopCount];
		mTroopKindCount = new int[CommanderTroopSet.cMaxTroopKindCount];
		mTroopFormationIdxs = new int[CommanderTroopSet.cMaxTroopCount];

		mExplicitTargetingNode = new LinkedListNode<BattleCommander>(this);
		mExplicitTargetTroopLink = new GuidLink<BattleTroop>();
		mRecruitTroopWaitList = new List<int>();

		mSkill = new BattleCommanderSkill();

		mTroopTimeOnTargetStates = new TimeOnTargetState[CommanderTroopSet.cMaxTroopCount];
		mTroopFireDelays = new int[CommanderTroopSet.cMaxTroopCount];

		mCommanderAttackRefCounts = new int[BattleGrid.cMaxCommanderCount]; // 그리드 상의 모든 지휘관에 대한 공격 참조 카운터 정보
		mLastAttackCommanderGridIdxs = new int[CommanderTroopSet.cMaxTroopCount]; // 각 부대가 어떤 지휘관을 공격하던 중이었는지 정보

		mTargetingDummyLink = new GuidLink<BattleTargetingDummy>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateCommander(
		BattleGrid pBattleGrid,
		BattleUser pUser,
		CommanderItem pCommanderItem,
		int pCommanderSlotIdx,
		Coord2 pSpawnCoord,
		int pSpawnDirectionY,
		BattleUser pTeamUser)
	{
		OnCreateObject(
			pBattleGrid,
			false, // bool pIsCheckDetection
			false, // bool pIsDetecting
			pBattleGrid.aIsNoneDetectables, // bool pIsDetectables
			pSpawnCoord,
			pSpawnDirectionY,
			(pTeamUser != null) ? pTeamUser.aTeamIdx : 0);

		// 초기화를 합니다.
		mUser = pUser;
		mUserInfo = (pUser != null) ? pUser.aUserInfo : null;
		mTeamUser = pTeamUser;
		mCommanderItem = pCommanderItem;
		mTroopSet = mCommanderItem.mTroopSet;

		mFormationState = FormationState.DefaultFormation;

		mTroopSetItem = mCommanderItem.mTroopSetItem;
		mCommanderSpec = pCommanderItem.mCommanderSpec;
		mCommanderSlotIdx = pCommanderSlotIdx;
		mCommanderGridIdx = aBattleGrid.GetCommanderGridIdx(mUser, mCommanderSlotIdx);
		_SetTypeAndScriptId();

		aUpdateIndex = pCommanderSlotIdx; // 임시 처리, AI 업데이트 순서 결정하는 변수
		aOriginUpdateIndex = aUpdateIndex;

		mStatus.Init(this);

		mActiveAiManager = null;
		mAiUserIdx = -1;

		if (aBattleGrid.aCommanders[mCommanderGridIdx] != null)
			Debug.LogError("duplicate CommanderGridIdx :" + mCommanderGridIdx + " - " + this);
		aBattleGrid.aCommanders[mCommanderGridIdx] = this;

		mCommanderChainItem
			= (mUserInfo != null)
			? aBattleGrid.aCommanderChains[mUserInfo.mUserIdx].AddLast(this) : null;

		mStageCommander = aBattleGrid.aStage.CreateCommander(this);

		mLiveTroopCount = 0;
		mActiveTroopCount = 0;
		mIsCoolTime = false;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			mTroops[iTroop] = null;
		mFormationMoveCenteringOffset = Coord2.zero;
		mLeaderTroop = null;
		mFrontTroop = null;
		mIsCaptureTypeTroop = false;

		mControlCoord = aCoord;

		mMaxTotalMilliHp = 0;
		mCurTotalMilliHp = 0;

		mMaxAdvanceSpeed = int.MaxValue;

		if (mCommanderItem.mCommanderItemType != CommanderItemType.DefenderCommander) // 거점 지휘관인 경우 Battle Commander 업데이트를 하지 않습니다.
			RepeatUpdate(); // OnFrameUpdate()
		aIsBattle = false;

		mTryCheckPoint = null;
		mReserveGrabCheckPoint = null;

		mRecruitMp = 0;

		mIsMoving = false;
		mIsSomeTroopAttacking = false;
		mIsLeaderTroopAttacking = false;
		mIsDestroyOnNoTroop = true;
		mIsFleeing = false;

		mTimeOnTargetState = TimeOnTargetState.None;

		if (mUser != null)
		{
			mUser.aCurrentTroopCount += mTroopSet.mTroopCount;
			mUser.aCommanders[mCommanderSlotIdx] = this; // 유저에 등록
		}

		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (mTroopSet.mTroopKindCountInfos[iKind] == null)
				continue;
			mTroopKindCount[iKind] = mTroopSet.mTroopKindCountInfos[iKind].mTroopCount; // 병종별 개수를 초기화
		}

		for (int iCommander = 0; iCommander < mCommanderAttackRefCounts.Length; ++iCommander)
			mCommanderAttackRefCounts[iCommander] = 0; // 공격 참조 카운터를 초기화
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			mLastAttackCommanderGridIdxs[iTroop] = -1;
		mLastFoundEnemyCommanderGridIdx = -1;

		mVeterancyRank = mTroopSet.mStartVeterancyRank;
		if (mVeterancyRank > 0)
			mVeterancyExperience = mCommanderSpec.GetNextVeterancyExp(mVeterancyRank - 1); // 현재 랭크까지의 베테랑 경험치를 가지고 시작

		mSkill.Init(this);

        if (mCommanderItem.mCommanderItemType != CommanderItemType.TestCommander)
		    UpdateAiState(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		mVeterancyRank = 0;
		mVeterancyExperience = 0;

		mLinkCheckPoint = null;
		mLinkEventPoint = null;

		// AI 연결을 끕니다.
		_LinkUserAi(-1);

		// 파괴 전 콜백을 호출합니다.
		if (aOnDestroyingDelegate != null)
		{
			aOnDestroyingDelegate(this);
			aOnDestroyingDelegate = null;
		}

		// 스킬을 리셋합니다.
		mSkill.Reset();

		// 명시적 타게팅 하던 것을 취소합니다.
		_SetExplicitTargeting(null);

		// 유저에서의 등록을 해제합니다.
		if (mUser != null)
		{
			--mUser.aLiveCommanderCount; // 살아있는 지휘관 수 줄임

			mUser.RefreshSpawnInfo();
			mUser.aCommanders[mCommanderSlotIdx] = null;
		}

		// 지휘관 아이템의 상태를 변경합니다.
		if (mUserInfo != null)
			mUserInfo.RemoveCommanderItem(mCommanderItem.mSlotIdx);

		// 남아있는 부대를 파괴합니다.
		aOnDestroyingTroopDelegate = null;
		aOnDestroyedTroopDelegate = null;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if (mTroops[iTroop] != null)
				mTroops[iTroop].Destroy();

		// 스테이지에서 제거합니다.
		mStageCommander.DestroyObject();
		mStageCommander = null;

		// 지휘관 등록을 해제합니다.
		if (mCommanderChainItem != null)
			mCommanderChainItem.Empty();

		aBattleGrid.aCommanders[mCommanderGridIdx] = null;

		mRecruitTroopWaitList.Clear();

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 파괴 후 콜백을 호출합니다.
		if (aOnDestroyedDelegate != null)
		{
			aOnDestroyedDelegate(mScriptUserId, mScriptSlotId);
			aOnDestroyedDelegate = null;
		}

		// 등록된 콜백을 모두 해제합니다.
		aOnSpawnTroopsDelegate = null;
		aOnMoveStartedDelegate = null;
		aOnMoveStoppedDelegate = null;
		//aOnDestroyingDelegate = null;
		//aOnDestroyedDelegate = null;
		//aOnDestroyingTroopDelegate = null;
		//aOnDestroyedTroopDelegate = null;
		aOnRecruitTroopsDelegate = null;
		aOnVeterancyRankUpDelegate = null;
		aOnVeterancyExpDelegate = null;

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyCommander(this);

		// 연결된 태스크를 취소합니다.
		if (mOnRetreatDestroyTask != null)
		{
			mOnRetreatDestroyTask.Cancel();
			mOnRetreatDestroyTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameObject 콜백
	public override void OnFrameUpdate(FrameTask pTask, int pFrameDelta)
	{
// 		mStatus.UpdateFrame();

		// 지휘관 위치를 리더 부대의 위치로 갱신합니다.
		if (mLeaderTroop != null)
			_UpdateCommanderCoord(mLeaderTroop.aCoord, !mLeaderTroop.aMoveUpdater.aIsMoving);

		// TOT 상태를 갱신합니다.
		if (mTimeOnTargetState == TimeOnTargetState.Ready)
			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
				mTroopFireDelays[iTroop] -= pFrameDelta;

		// 리포팅 정보를 갱신합니다.
		mLastFoundEnemyCommanderGridIdx = -1;

		// 스킬을 갱신합니다.
		if (mSkill.aIsUpdating)
			mSkill.OnFrameUpdate(pFrameDelta);

		// IStageCommander을 갱신합니다.
		mStageCommander.OnFrameUpdate(pFrameDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		Debug.Log("UpdateCoord() - " + this);
		SetCoordAndDirectionY(pCoord, aDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateDirectionY(int pDirectionY)
	{
		Debug.Log("UpdateDirectionY() - " + this);
		SetCoordAndDirectionY(aCoord, pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표와 방향각을 초기화합니다.
	public void SetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		//Debug.Log("SetCoordAndDirectionY() - " + this);

		base.UpdateCoord(pCoord, true);
		base.UpdateDirectionY(pDirectionY);

		mDirectionVector = Coord2.RoughDirectionVector(pDirectionY, cDirectionVectorSize);

		// 스테이지에 보입니다.
		mStageCommander.OnSetCoordAndDirectionY(pCoord, pDirectionY);

		// 휘하 부대의 팀을 갱신합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if (mTroops[iTroop] != null)
			{
				Coord2 lFormationCoord = GetTroopFormationCoord(iTroop);
				mTroops[iTroop].SetCoordAndDirectionY(lFormationCoord, pDirectionY);
			}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateTeam(int pTeamIdx)
	{
		UpdateTeam(pTeamIdx, aBattleGrid.aTeamUsers[pTeamIdx][0]); // 해당 팀의 기본 유저를 팀 유저로 지정
	}
	//-------------------------------------------------------------------------------------------------------
	// 팀을 해당 유저의 팀으로 바꿉니다.
	public void UpdateTeam(int pTeamIdx, BattleUser pTeamUser)
	{
		if ((aTeamIdx == pTeamIdx) &&
			(mTeamUser == pTeamUser))
			return;

		// 팀을 갱신합니다.
		base.UpdateTeam(pTeamIdx);

		// 팀 유저를 지정합니다.
		mTeamUser = pTeamUser;
		UpdateAiState(false); // 유저가 바뀌었으므로 AI도 갱신

		// 스테이지에 보입니다.
		mStageCommander.OnUpdateTeam(aTeamIdx);

		// 휘하 부대의 팀을 갱신합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if (mTroops[iTroop] != null)
				mTroops[iTroop].UpdateTeam(aTeamIdx);

		// 색적을 갱신합니다.
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
			UpdateDetection(iTeam);
	}
	//-------------------------------------------------------------------------------------------------------
	// AI 상태를 갱신합니다.
	public void UpdateAiState(bool pIsForceAi)
	{
		int lNewAiUserIdx = (mTeamUser != null) ? mTeamUser.aUserIdx : -1;
		_LinkUserAi(lNewAiUserIdx);

		if ((mAiUserIdx >= 0) &&
			(mCommanderItem.mCommanderItemType != CommanderItemType.DefenderCommander)) // 방어군은 자체 AI 없음
		{
			mActiveAiManager
				= ((aBattleGrid.aUsers[mAiUserIdx].aControlType == BattleUser.ControlType.Auto) || pIsForceAi)
				? aBattleGrid.aUsers[mAiUserIdx].aAiManager : null;
		}
		else
			mActiveAiManager = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 메서드
	public void OnMoveCheckPoint(int pCheckPointIdx)
	{
		BattleCheckPoint lCheckPoint = aBattleGrid.aCheckPointList[pCheckPointIdx];
		GrabCheckPoint(lCheckPoint);	
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 메서드
	public void OnMoveHQ(int pHeadquarterUserIdx)
	{
		BattleUser lHeadquarterUser = aBattleGrid.aUsers[pHeadquarterUserIdx];
		if (lHeadquarterUser == null)
			Debug.LogError("invalid pHeadquarterUserIdx:" + pHeadquarterUserIdx + " - " + this);

		MoveTroops(lHeadquarterUser.aHeadquarter.aCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 메서드
	public void OnAttack(BattleTroop pTargetTroop)
	{
		// 기존에 처리하던 명령을 중지하고 공격 명령을 실행합니다.
		AttackTargetTroop(pTargetTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 메서드
	public void OnAttackMove(Coord2 pTargetCoord)
	{
		MoveTroops(pTargetCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleAiCommanderData 메서드
	public void OnAttackMoveDirection(Coord2 pTargetCoord, int pDirectionY)
	{
		MoveTroops(pTargetCoord, pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대를 스폰합니다.
	public void SpawnTroops(Coord2 pSpawnCoord, int pSpawnDirectionY)
	{
		if (mLiveTroopCount > 0)
			return;

		if (mTroopSetItem == null)
			return;

		mMaxTotalMilliHp = 0;
		mCurTotalMilliHp = 0;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
// [테스트] 몇 개의 부대만 스폰시키는 처리
// 		if ((iTroop < 1) || (mUser == null))
			_CreateTroop(iTroop, pSpawnCoord, pSpawnDirectionY, true);

		// 포메이션 순번을 갱신합니다.
		UpdateFormationOrder();

		// 색적을 갱신합니다.
		for (int iTeam = 0; iTeam < aBattleGrid.aTeamCount; ++iTeam)
			UpdateDetection(iTeam);

		// 콜백을 호출합니다.
		if (aOnSpawnTroopsDelegate != null)
			aOnSpawnTroopsDelegate(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대를 이동시킵니다.
	public void MoveTroops(Coord2 pMoveCoord)
	{
		MoveTroops(pMoveCoord, BattleCommander.GetFormationMoveDirectionY(pMoveCoord, aCoord));
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대를 이동시킵니다.
	public void MoveTroops(Coord2 pMoveCoord, int pMoveDirectionY)
	{
		mControlCoord = pMoveCoord;

		if (mSkill.aIsStarted &&
			!mSkill.aSkillSpec.mIsMovable &&
			(mFormationState != FormationState.SkillFormation))
			return;

		if (mLiveTroopCount <= 0)
			return;

		if (mExplicitTargetTroopLink.Get() != null)
			_SetExplicitTargeting(null);

		int lMoveDirectionY = MathUtil.AbsDegree(pMoveDirectionY);

		_UpdateCommanderDirectionY(lMoveDirectionY); // 이동 방향으로 지휘관의 방향을 바꿈

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;

			lTroop.aDefaultDirectionY = lMoveDirectionY;

			Coord2 lTroopMoveOffset = !mIsFleeing
				? Coord2.RoughRotationVector(
					mFormationMoveCenteringOffset + mTroopSet.mFormationPositionOffsets[mTroopFormationIdxs[iTroop]],
					lMoveDirectionY)
				: Coord2.zero;
			Coord2 lFormationCoord = aBattleGrid.ClampCoord(mControlCoord + lTroopMoveOffset);
			lTroop.aCommand.StartDstCoordAdvance(lFormationCoord, 0);
		}
		aStageCommander.ShowState(StageCommanderState.Move);
		aStageCommander.ShowState(StageCommanderState.AttackStop); // 이동시 공격은 정지됩니다.
		aStageCommander.ShowState(StageCommanderState.ConquerStop); // 이동시 점령은 정지됩니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 대상 부대에 대해 소유 부대들에게 공격 명령을 내립니다.
	public void AttackTargetTroop(BattleTroop pTargetTroop)
	{
		//Debug.Log("AttackTargetTroop() pTargetTroop=" + pTargetTroop);

		if (mExplicitTargetTroopLink.Get() != pTargetTroop)
			_SetExplicitTargeting(pTargetTroop);

		if (mLiveTroopCount <= 0)
			return;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;
			if (lTroop.aIsFallBehind)
				continue; // 복귀중인 부대는 명령 전달 안됨

			lTroop.aCommand.StartTargetTroopAttack(pTargetTroop, 0);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 대상 지점에 대해 소유 부대들에게 공격 명령을 내립니다.
	public void AttackTargetCoord(Coord2 pTargetCoord)
	{
		//Debug.Log("AttackTargetCoord() pTargetCoord=" + pTargetCoord);

		mExplicitTargetCoord = pTargetCoord;

		if (mLiveTroopCount <= 0)
			return;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;
			if (lTroop.aIsFallBehind)
				continue; // 복귀중인 부대는 명령 전달 안됨

			lTroop.aCommand.StartTargetCoordAttack(pTargetCoord, false, 0);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대 슬롯에 대한 포메이션 인덱스를 얻습니다.
	public int GetTroopFormationIdx(int pTroopSlotIdx)
	{
		return mTroopFormationIdxs[pTroopSlotIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대의 포메이션 좌표를 얻습니다.
	public Coord2 GetTroopFormationCoord(int pTroopSlotIdx)
	{
		if (mLeaderTroop == null)
			return aCoord; // 이런 상황은 없겠지만 예외 처리로서 리더가 없다면 지휘관 좌표를 반환

		Coord2 lTroopMoveOffset = Coord2.RoughRotationVector(
			mFormationMoveCenteringOffset + mTroopSet.mFormationPositionOffsets[mTroopFormationIdxs[pTroopSlotIdx]],
			aDirectionY); // 지휘관의 방향을 사용
		Coord2 lFormationCoord = aBattleGrid.ClampCoord(aCoord + lTroopMoveOffset);

		return lFormationCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리더 부대를 따라갈 때의 해당 부대의 포메이션 이동 좌표를 얻습니다.
	public Coord2 GetTroopFollowFormationMoveCoord(int pTroopSlotIdx)
	{
		if (mLeaderTroop == null)
			return aCoord; // 이런 상황은 없겠지만 예외 처리로서 리더가 없다면 지휘관 좌표를 반환

		Coord2 lTroopMoveOffset = Coord2.RoughRotationVector(
			mFormationMoveCenteringOffset + mTroopSet.mFormationPositionOffsets[mTroopFormationIdxs[pTroopSlotIdx]],
			mLeaderTroop.aDirectionY); // 리더 부대의 방향을 사용
		Coord2 lFormationCoord = aBattleGrid.ClampCoord(mFrontTroop.aCoord + lTroopMoveOffset);

		return lFormationCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 거점을 점령합니다.
	public void GrabCheckPoint(BattleCheckPoint pCheckPoint)
	{
		// 방어군이 있다면 먼저 이를 공격합니다.
		if (pCheckPoint.aTeamIdx != aTeamIdx)
		{
			BattleTroop lDefenseTroop = pCheckPoint.GetNearestDefenderTroop(aCoord, -1); // 보이지 않더라도 접근시도
			if (lDefenseTroop != null)
			{
				AttackTargetTroop(lDefenseTroop);
				if (mIsCaptureTypeTroop) // 점령 가능한 부대라면 방어군을 없애고 이어서 점령 시도를 할 수 있도록
					mReserveGrabCheckPoint = pCheckPoint; // 이 거점을 기억
				return; // 예약 처리에서 이후의 진행을 맡음
			}
		}

		// 해당 거점의 점령 시도 목록에 추가합니다.
		if (pCheckPoint.IsRecruit(aTeamIdx))
			pCheckPoint.AddTryRecruitTroop(this);
		else if(pCheckPoint.IsConquer(aTeamIdx))
			pCheckPoint.AddTryCaptureCheckPoint(this);
		mTryCheckPoint = pCheckPoint;

		// 부대를 거점으로 이동시킵니다.
		MoveTroops(pCheckPoint.aCoord);

		pCheckPoint.TouchCheckPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 점령을 취소합니다.
	public void UngrabCheckPoint()
	{
		if (mTryCheckPoint == null)
			return;

		if (mTryCheckPoint.aTryCaptureCommander == this)
		{
			mTryCheckPoint.RemoveTryCaptureCheckPoint(this);
			mTryCheckPoint.RemoveTryRecruitTroop(this);
			mTryCheckPoint = null;
			mReserveGrabCheckPoint = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 퇴각합니다.
	public void Retreat(bool pIsRetreat)
	{
		if ((mOnRetreatDestroyTask != null) == pIsRetreat) // 퇴각 파괴 태스크 여부로 퇴각중인지 판단
			return;

		if (pIsRetreat)
		{
			if (mTeamUser != null)
				MoveTroops(mTeamUser.aHeadquarter.aSpawnCoord); // 유저부대라면 본부 스폰 좌표(첫 등장 좌표)로 이동 시작

			mOnRetreatDestroyTask = ReserveTask(_OnRetreatDestroy, BattleConfig.get.mRetreatOrderDelay); // 퇴각 삭제 예약
		}
		else
		{
			mOnRetreatDestroyTask.Cancel();
			mOnRetreatDestroyTask = null;
		}

		// 스테이지에 보입니다.
		aStageCommander.ShowRetreat(pIsRetreat);
	}
	//-------------------------------------------------------------------------------------------------------
	// 명시적으로 타게팅하고 있는 부대가 사라질 때 호출합니다.
	public void OnExplicitTargetTroopDestroyed(BattleTroop pExplicitTargetTroop)
	{
		mExplicitTargetTroopLink.Set(null);

		// 거점 점령을 재시도 합니다.
		if (mReserveGrabCheckPoint != null)
			ReserveTask(_OnReserveGrabCheckPoint, 0); // 콜백 호출 흐름상 다음 프레임에 진행되도록 예약
		aStageCommander.ShowState(StageCommanderState.AttackComplete);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대가 이동 컨트롤 상태에 들어가거나 나올 때 호출됩니다.
	public void OnTroopMoveControl(int pTroopSlotIdx, bool pIsInMoveControl)
	{
		//if (aTeamIdx == 1) Debug.Log("OnTroopMoveControl() pTroopSlotIdx=" + pTroopSlotIdx + " pIsInMoveControl=" + pIsInMoveControl);

		if (pIsInMoveControl) // 이동을 시작했다면
		{
			if (!mIsMoving &&
				!mTroops[pTroopSlotIdx].aIsFallBehind)
			{
				mIsMoving = true;
				aStageCommander.ShowState(StageCommanderState.Move);
				//if (aTeamIdx == 1) Debug.Log("aOnMoveStartedDelegate - " + this);
				if (aOnMoveStartedDelegate != null)
					aOnMoveStartedDelegate(this);
			}
		}
		else // 이동이 끝났다면
		{
			// 유저 AI가 있는 경우라면 전투 종료 이벤트를 검사합니다.
			bool lIsMoveingTroop = false;
			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			{
				BattleTroop lTroop = mTroops[iTroop];
				if ((lTroop == null) ||
					lTroop.aIsFallBehind) // 복귀중인 부대는 고려 안 함
					continue;

				if (lTroop.aIsMoving)
				{
					lIsMoveingTroop = true;
					break;
				}
			}
			if (!lIsMoveingTroop &&
				mIsMoving)
			{
				mIsMoving = false;
				//if(mTryCheckPoint == null && mReserveGrabCheckPoint == null)
				//aStageCommander.ShowState(StageCommanderState.MoveComplete);
				aStageCommander.ShowState(StageCommanderState.MoveStop);
				//if (aTeamIdx == 1) Debug.Log("aOnMoveStoppedDelegate - " + this);
				if (aOnMoveStoppedDelegate != null)
					aOnMoveStoppedDelegate(this);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대가 공격 컨트롤 상태에 들어가거나 나올 때 호출됩니다.
	public void OnTroopAttackControlChange(int pTroopSlotIdx, bool pIsInAttackControl)
	{
		if ((mLeaderTroop != null) &&
			(pTroopSlotIdx == mLeaderTroop.aTroopSlotIdx))
			ReportLeaderTroopAttacking();

		if (pIsInAttackControl) // 공격을 시작했다면
		{
			if (!mIsSomeTroopAttacking)
			{
				mIsSomeTroopAttacking = true;
				aStageCommander.ShowState(StageCommanderState.Attack);
			}
		}
		else // 공격이 끝났다면
		{
			// 유저 AI가 있는 경우라면 전투 종료 이벤트를 검사합니다.
			bool lIsAttackingTroop = false;
			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			{
				BattleTroop lTroop = mTroops[iTroop];
				if (lTroop == null)
					continue;

				if (lTroop.aIsAttacking)
				{
					lIsAttackingTroop = true;
					break;
				}
			}
			if (!lIsAttackingTroop &&
				mIsSomeTroopAttacking)
			{
				mIsSomeTroopAttacking = false;
				//if(mTryCheckPoint == null && mReserveGrabCheckPoint == null)
					//aStageCommander.ShowState(StageCommanderState.AttackComplete);
						
				aStageCommander.ShowState(StageCommanderState.AttackComplete);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 휘하 부대의 타겟이 바뀔 때 호출됩니다(대상 부대가 null이면 공격 중단을 의미).
	public void OnTroopTargetChange(int pTroopSlotIdx, BattleTroop pTargetTroop)
	{
		int lOldTargetCommanderGridIdx = mLastAttackCommanderGridIdxs[pTroopSlotIdx];
		int lNewTargetCommanderGridIdx = (pTargetTroop != null) ? pTargetTroop.aCommander.aCommanderGridIdx : -1;

		if (lOldTargetCommanderGridIdx != lNewTargetCommanderGridIdx) // 타겟 지휘관이 달라졌다면
		{
			if (lOldTargetCommanderGridIdx >= 0)
			{
				--mCommanderAttackRefCounts[lOldTargetCommanderGridIdx];
				if ((mActiveAiManager != null) &&
					(mCommanderAttackRefCounts[lOldTargetCommanderGridIdx] <= 0)) // 이전 지휘관에 대한 마지막 공격 부대였다면
					mActiveAiManager.RemoveTargetCommander(lOldTargetCommanderGridIdx); // AI에 알림
			}

			if (lNewTargetCommanderGridIdx >= 0)
			{
				++mCommanderAttackRefCounts[lNewTargetCommanderGridIdx];
				if ((mActiveAiManager != null) &&
					(mCommanderAttackRefCounts[lNewTargetCommanderGridIdx] == 1)) // 새 지휘관에 대한 첫 공격 부대라면
					mActiveAiManager.AddTargetCommander(lNewTargetCommanderGridIdx); // AI에 알림
			}

			mLastAttackCommanderGridIdxs[pTroopSlotIdx] = lNewTargetCommanderGridIdx;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 전체 HP를 갱신합니다.
	public void UpdateTotalHp()
	{
		mCurTotalMilliHp = 0;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;

			mCurTotalMilliHp += lTroop.aStat.aCurMilliHp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐지 상태를 갱신합니다.
	public void UpdateDetection(int pDetectingTeamIdx)
	{
		aIsDetecteds[pDetectingTeamIdx] = false;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;

			aIsDetecteds[pDetectingTeamIdx] |= lTroop.aIsDetecteds[pDetectingTeamIdx];
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 지휘관 휘하의 부대들끼리 순서와 포메이션을 재설정 합니다.
	public void UpdateFormationOrder()
	{
		BattleTroop lLastLeaderTroop = mLeaderTroop;

		mLeaderTroop = null;
		mFrontTroop = null;
		mIsCaptureTypeTroop = false;
		mLiveTroopCount = 0; // 낙오 부대를 포함한 부대 수
		mActiveTroopCount = 0; // 낙오하지 않은 실제 전투 가능한 부대 수
		mIsFleeing = false;

		int lFirstGroupCount = 0;
		int lSecondGroupCount = 0;
		BattleTroop lPrecedingTroop = null;
		int lRestTroopIdxCount = 0;;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
			{
				if (mTroopSet.mTroopInfos[iTroop] != null) // 파괴되었지만 충원이 가능한 부대라면
					sRestTroopFormationIdxs[lRestTroopIdxCount++] = iTroop; // 나머지 부대 정보에 고려
				continue;
			}

			if (!lTroop.aIsNpc)
				++mLiveTroopCount; // 살아 있는 부대 수 증가

			if ((lTroop.aStat.aMoraleState != TroopMoraleState.PinDown) && // 핀다운 상태가 아니고
				!lTroop.aIsFallBehind) // 낙오 상태가 아니라면
			{
				if (mFrontTroop == null)
					mFrontTroop = lTroop;

				++mActiveTroopCount; // 활동 부대 수 증가

				if (lTroop.aTroopSpec.mIsCaptureType)
					mIsCaptureTypeTroop = true; // 거점 점령이 가능한 부대 있음

				if (mTroopSet.mFormationGroupIds[iTroop] == 0)
					mTroopFormationIdxs[iTroop] = mTroopSet.mFirstGroupIdxs[lFirstGroupCount++]; // 첫 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당
				else
					mTroopFormationIdxs[iTroop] = mTroopSet.mSecondGroupIdxs[lSecondGroupCount++]; // 두 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당

				if (mLeaderTroop == null)
				{
					if (mTroopSet.mFormationPositionOffsets[mTroopFormationIdxs[iTroop]] == Coord2.zero)
						mLeaderTroop = lTroop;
				}
			}
			else
				sRestTroopFormationIdxs[lRestTroopIdxCount++] = iTroop;

			lTroop.SetPrecedingTroop(lPrecedingTroop);
			lPrecedingTroop = lTroop;
		}

		// 복귀 상태 등을 대비해서 지정 안된 나머지 부대들도 포메이션 번호를 지정합니다.
		for (int iIdx = 0; iIdx < lRestTroopIdxCount; ++iIdx)
		{
			int lTroopIdx = sRestTroopFormationIdxs[iIdx];
			if (mTroopSet.mFormationGroupIds[lTroopIdx] == 0)
				mTroopFormationIdxs[lTroopIdx] = mTroopSet.mFirstGroupIdxs[lFirstGroupCount++]; // 첫 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당
			else
				mTroopFormationIdxs[lTroopIdx] = mTroopSet.mSecondGroupIdxs[lSecondGroupCount++]; // 두 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당
		}

		// 지정할 선봉 부대를 못찾았다면 사기가 0이 아닌 첫 번째 핀다운 상태의 부대를 선봉 부대로 지정합니다.
		if (mFrontTroop == null)
		{
			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			{
				BattleTroop lTroop = mTroops[iTroop];
				if (lTroop == null)
					continue;

				if ((lTroop.aStat.aMoraleState == TroopMoraleState.PinDown) &&
					!lTroop.aIsFallBehind &&
					(lTroop.aStat.aCurMilliMorale > 0))
				{
					mFrontTroop = lTroop;
					break;
				}
			}
			if (mFrontTroop == null) // 모든 부대의 사기가 0이거나 낙오 상태라면
			{
				for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
					if (mTroops[iTroop] != null)
					{
						mFrontTroop = mTroops[iTroop]; // 퇴각하더라도 선봉 부대는 어쨌든 필요하므로 존재하는 첫 번째 부대를 선봉 부대로 지정
						break;
					}
				if ((mLinkCheckPoint == null) &&
					(mFrontTroop != null) &&
					!mFrontTroop.aIsObserver)
					mIsFleeing = true;
			}
		}

		// 지정할 리더 부대를 못찾았다면 선봉 부대를 리더 부대로 지정합니다.
		if (mLeaderTroop == null)
			mLeaderTroop = mFrontTroop;

		// 포메이션 이동 오프셋을 구합니다.
		if (mFrontTroop != null)
			mFormationMoveCenteringOffset = -mTroopSet.mFormationPositionOffsets[mTroopFormationIdxs[mFrontTroop.aTroopSlotIdx]];
		else
			mFormationMoveCenteringOffset = Coord2.zero;

		// 리더 변경을 보입니다.
		if ((lLastLeaderTroop != mLeaderTroop) &&
			(mUser != null)) // 방어군은 제외
		{
			if (lLastLeaderTroop != null)
				lLastLeaderTroop.aStageTroop.OnUpdateLeaderState(false);
			if (mLeaderTroop != null)
				mLeaderTroop.aStageTroop.OnUpdateLeaderState(true);

			ReportLeaderTroopAttacking();
		}

		// 최고 이동 속도를 갱신합니다.
		if (!mIsFleeing)
			UpdateMaxAdvanceSpeed();

		// 퇴각 처리를 합니다.
		if (mIsFleeing)
			Retreat(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 지휘관 휘하의 부대들이 모두 같이 갈 수 있는 최고 진군 속도(제한)을 갱신합니다.
	public void UpdateMaxAdvanceSpeed()
	{
		//Debug.Log("UpdateMaxAdvanceSpeed() mMaxAdvanceSpeed=" + mMaxAdvanceSpeed + " - " + this);

		// 최고 이동 속도를 얻습니다.
		mMaxAdvanceSpeed = int.MaxValue;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;

			if ((lTroop.aStat.aMoraleState != TroopMoraleState.PinDown) && // 핀다운 상태가 아니고
				!lTroop.aIsFallBehind) // 낙오 상태가 아니라면
			{
				int lTroopSpeed = lTroop.aStat.aStatMoveSpeed;
				if ((mMaxAdvanceSpeed > lTroopSpeed) &&
					(lTroopSpeed > 0)) // 스킬 효과 등에 의해 멈춘 것이 아닌 경우에만 고려
					mMaxAdvanceSpeed = lTroopSpeed; // 지휘관 최고 이동 속도에 영향을 줍니다.
			}
		}

		// 모든 부대의 최고 이동 속도를 갱신합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop != null)
				lTroop.aStat.LimitAdvanceSpeed(
					!mIsFleeing
					? mMaxAdvanceSpeed // 도망이 아니라면 지휘관의 최대 속도로 제한
					: lTroop.aStat.aStatMoveSpeed); // 도망이라면 최대 속도 제한 없음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 잃은 부대를 충원합니다.
	public void OnRecruitTroop()
	{
		if (mRecruitTroopWaitList.Count <= 0)
			return;
		if (aUser.aCurrentMp < mRecruitMp)
			return;

		if (!aBattleGrid.aProgressInfo.TryMpUsage(aUser.aUserIdx, mRecruitMp))
			return;

		// 충원 방식을 정합니다.
		Coord2 lRecruitCoord;
		int lRecruitDirectionY;

		switch (mCommanderSpec.mSpawnType)
		{
		case CommanderSpawnType.Headquarter:
			{
				lRecruitCoord = (mUser != null) ? mUser.aHeadquarter.aSpawnCoord : aCoord;
				lRecruitDirectionY = (mUser != null) ? mUser.aHeadquarter.aDirectionY : aDirectionY;
			}
			break;
		case CommanderSpawnType.Defense:
		case CommanderSpawnType.Aircraft:
        case CommanderSpawnType.TestTool:
        default:
			{
				lRecruitCoord = aCoord;
				lRecruitDirectionY = aDirectionY;
			}
			break;
		}

		// 충원 부대를 생성합니다.
		for (int iRecruitTroop = 0; iRecruitTroop < mRecruitTroopWaitList.Count; ++iRecruitTroop)
		{
			int lTroopSlotIdx = mRecruitTroopWaitList[iRecruitTroop];
			_CreateTroop(lTroopSlotIdx, lRecruitCoord, lRecruitDirectionY, false);
		}

		// 충원해서 생긴 부대들에게 복귀 명령을 내립니다.
		for (int iRecruitTroop = 0; iRecruitTroop < mRecruitTroopWaitList.Count; ++iRecruitTroop)
			mTroops[mRecruitTroopWaitList[iRecruitTroop]].aCommand.StartReturnAdvance(0);

		// 모두 충원되었으므로 충원할 부대 리스트를 비웁니다.
		mRecruitTroopWaitList.Clear();
		mRecruitMp = 0;

		// 부대 종류별 수를 갱신합니다.
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (mTroopSet.mTroopKindCountInfos[iKind] == null)
				continue;

			mTroopKindCount[iKind] = mTroopSet.mTroopKindCountInfos[iKind].mTroopCount;
		}
		UpdateFormationOrder();

		if (aOnRecruitTroopsDelegate != null)
			aOnRecruitTroopsDelegate(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 재소환이 가능한지 검사합니다.
	public bool CheckRecruitable()
	{
		if (mRecruitTroopWaitList.Count <= 0)
			return false;
		if (aUser.aCurrentMp < mRecruitMp)
			return false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 가장 가까운 부대를 얻습니다.
	public BattleTroop GetNearestTroop(Coord2 pCoord, int pSearchingTeamIdx)
	{
		if (mLiveTroopCount <= 0)
			return null;

		BattleTroop lNearestTroop = null;
		int lNearestSqrDistance = int.MaxValue;

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue; // 없는 부대
			if (lTroop.aIsNpc)
				continue; // 더미 부대
			if ((pSearchingTeamIdx >= 0) && // 탐지 팀 인덱스가 음수라면 팀 탐지 무시
				!lTroop.aIsDetecteds[pSearchingTeamIdx])
				continue; // 탐지 안됨

			Coord2 lDistanceVector = lTroop.aCoord - pCoord;
			int lSqrDistance = lDistanceVector.SqrMagnitude();
			if (lNearestSqrDistance > lSqrDistance)
			{
				lNearestTroop = lTroop;
				lNearestSqrDistance = lSqrDistance;
			}
		}

		return lNearestTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 경험치를 획득합니다.
	public void UpdateExperience(int pExperience)
	{
		if (BattleConfig.get.mMaxVeterancyRank <= mVeterancyRank)
			return;

		mVeterancyExperience += pExperience;

		// 베테랑 랭크업
		bool lIsRankUp = false;
		while (mCommanderSpec.GetNextVeterancyExp(mVeterancyRank) <= mVeterancyExperience)
		{
			mVeterancyRank++;
			lIsRankUp = true;
			if (mVeterancyRank >= BattleConfig.get.mMaxVeterancyRank)
				break;
		}
		if (lIsRankUp)
		{
			if (aOnVeterancyRankUpDelegate != null)
				aOnVeterancyRankUpDelegate(this);
		}
		if (aOnVeterancyExpDelegate != null)
			aOnVeterancyExpDelegate(this);


		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;
			if (lIsRankUp)
			{
				lTroop.OnVeteranRankup(mVeterancyRank);
			}
			else
			{
				lTroop.OnVeteranExperience();
			}
		}
		if (lIsRankUp)
		{
			UpdateFormationOrder();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TOT 회수를 지정합니다.
	public void SetTimeOnTargetCount(int pTimeOnTargetCount)
	{
		if (pTimeOnTargetCount <= 0)
		{
			mTimeOnTargetState = TimeOnTargetState.None;
			return;
		}

		mTimeOnTargetCounter = pTimeOnTargetCount;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			mTroopTimeOnTargetStates[iTroop] = TimeOnTargetState.Wait; // 대기 상태로 초기화

		mTimeOnTargetState = TimeOnTargetState.Wait;

		mMaxTroopFireDelay = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대의 TOT 발사 준비를 알립니다.
	public void ReportTimeOnTargetReady(int pTroopSlotIdx, BattleTroopTargetingInfo pTroopTargetingInfo)
	{
		if (mTroopTimeOnTargetStates[pTroopSlotIdx] != TimeOnTargetState.Wait)
			return; // 대기 상태인 부대만 처리

		mTroopTimeOnTargetStates[pTroopSlotIdx] = TimeOnTargetState.Ready; // 준비 상태로 갱신

		int lBulletVelocity = pTroopTargetingInfo.aSelectedWeaponData.aWeaponStatTable.Get(WeaponStat.BulletVelocity);
		mTroopFireDelays[pTroopSlotIdx] = pTroopTargetingInfo.aTargetDistance * 1000 / lBulletVelocity; // lBulletVelocity가 1초당 거리이므로 밀리초 딜레이가 되도록 1000을 곱함
		if (mMaxTroopFireDelay < mTroopFireDelays[pTroopSlotIdx])
			mMaxTroopFireDelay = mTroopFireDelays[pTroopSlotIdx];

		bool lIsAllTroopReady = true;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if ((mTroops[iTroop] != null) &&
				mTroops[iTroop].aIsPrimaryAttacking && // 공격 진행중이고(TOT는 주무기에 대해서만 처리)
				(mTroopTimeOnTargetStates[iTroop] == TimeOnTargetState.Wait)) // 아직 발사 준비가 안 된 부대 존재
			{
				lIsAllTroopReady = false;
				break;
			}

		if (lIsAllTroopReady)
		{
			mTimeOnTargetState = TimeOnTargetState.Ready;

			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
				mTroopFireDelays[iTroop] = mMaxTroopFireDelay - mTroopFireDelays[iTroop]; // 먼 거리의 부대가 더 먼저 쏘도록
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대의 TOT 발사를 알립니다.
	public bool ReportTimeOnTargetFire(int pTroopSlotIdx)
	{
		if (mTroopTimeOnTargetStates[pTroopSlotIdx] != TimeOnTargetState.Ready)
			return false; // 준비 상태인 부대만 처리
		if (mTroopFireDelays[pTroopSlotIdx] > 0)
			return false; // 발사 간격 조정중

		mTroopTimeOnTargetStates[pTroopSlotIdx] = TimeOnTargetState.Fire; // 발사 상태로 갱신

		bool lIsAllTroopFired = true;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			if ((mTroops[iTroop] != null) &&
				mTroops[iTroop].aIsPrimaryAttacking && // 공격 진행중이고(TOT는 주무기에 대해서만 처리)
				(mTroopTimeOnTargetStates[iTroop]) == TimeOnTargetState.Ready) // 아직 발사 진행 중인 부대 존재
			{
				lIsAllTroopFired = false;
				break;
			}

		if (lIsAllTroopFired)
		{
			mTimeOnTargetState = TimeOnTargetState.Fire; // 아래에서 바로 상태가 바뀌지만 의미 전달상 이 코드를 둠

			--mTimeOnTargetCounter;
			if (mTimeOnTargetCounter <= 0)
				mTimeOnTargetState = TimeOnTargetState.None; // TOT 발사 해제 상태로
			else
				SetTimeOnTargetCount(mTimeOnTargetCounter); // TOT 초기화
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 적 부대 발견을 알립니다.
	public void ReportNewEnemy(int pTroopSlotIdx, BattleTroop pEnemyTroop)
	{
		if (mActiveAiManager != null)
		{
			if (mLastFoundEnemyCommanderGridIdx != pEnemyTroop.aCommander.aCommanderGridIdx) // 이번 프레임에서의 중복 보고 제외
			{
				mLastFoundEnemyCommanderGridIdx = pEnemyTroop.aCommander.aCommanderGridIdx;

				//Debug.Log(String.Format("ReqFindEnemy() {0}->{1}", mTroops[pTroopSlotIdx], pEnemyTroop));
				mActiveAiManager.ReqFindEnemy(
					mCommanderGridIdx,
					mTroops[pTroopSlotIdx].aCoord,
					mLastFoundEnemyCommanderGridIdx);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 리더 부대의 공격 상태 변화를 보고합니다.
	public void ReportLeaderTroopAttacking()
	{
		if (mActiveAiManager != null)
		{
			bool lIsLeaderTroopAttackingNow = (mLeaderTroop != null) ? mLeaderTroop.aIsAttacking : false;
			if (mIsLeaderTroopAttacking != lIsLeaderTroopAttackingNow)
			{
				mIsLeaderTroopAttacking = lIsLeaderTroopAttackingNow;
				if (mIsLeaderTroopAttacking)
					mActiveAiManager.ReqBattleStart(mCommanderGridIdx); // AI에 알립니다.
				else
					mActiveAiManager.ReqBattleEnd(mCommanderGridIdx); // AI에 알립니다.
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 명시적 타겟에 대한 접근 거리를 갱신합니다.
	public void UpdateExplicitTargetTroopEncounterDistance()
	{
		BattleTroop lExplicitTargetTroop = mExplicitTargetTroopLink.Get();
		if (lExplicitTargetTroop == null)
			return;

		mSqrExplicitTargetTroopEncounterDistance = int.MaxValue;
		//// 접근할 거리를 리더 부대의 사정거리 중간으로 초기화 합니다.
		//if (mLeaderTroop != null)
		//{
		//	int lEncounterDistance = (mLeaderTroop.aStat.aMinAttackRange + mLeaderTroop.aStat.aMaxAttackRange) / 2;
		//	mSqrExplicitTargetTroopEncounterDistance = lEncounterDistance * lEncounterDistance;
		//}
		//else
		//	mSqrExplicitTargetTroopEncounterDistance = int.MaxValue;

		// 타겟에 가장 가까이 다가간 부대보다 접근 거리를 멀리 하지 않습니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;

			Coord2 lTargetVector = lExplicitTargetTroop.aCoord - lTroop.aCoord;
			int lSqrTargetDistance = lTargetVector.SqrMagnitude();
			if (//(lSqrTargetDistance >= lTroop.aStat.aSqrSightRange) && // 시야 바깥 거리 중에서
				(mSqrExplicitTargetTroopEncounterDistance > lSqrTargetDistance)) // 가장 가까운 거리를 찾음
				mSqrExplicitTargetTroopEncounterDistance = lSqrTargetDistance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점을 연결합니다.
	public void SetLinkCheckPoint(BattleCheckPoint pCheckPoint)
	{
		mLinkCheckPoint = pCheckPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 지점을 연결합니다.
	public void SetLinkEventPoint(BattleEventPoint pEventPoint)
	{
		mLinkEventPoint = pEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타게팅 더미를 지정합니다.
	public void SetTargetingDummy(BattleTargetingDummy pTargetingDummy)
	{
		mTargetingDummyLink.Set(pTargetingDummy);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리더 부대의 공격 상태 변화를 보고합니다.
	public static int GetFormationMoveDirectionY(Coord2 pFormationCoord, Coord2 pCommanderCoord)
	{
		Coord2 lMoveVector = pFormationCoord - pCommanderCoord;
		return lMoveVector.RoughDirectionY();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대들이 다른 객체를 탐지 가능한지를 지정합니다.
	public void EnableTroopsDetecting(bool pIsDetecting)
	{
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;
			lTroop.EnableDetecting(pIsDetecting);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대들을 다른 객체가 탐지 가능한지를 지정합니다.
	public void EnableTroopsDetectables(bool pIsDetectableAll)
	{
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if (lTroop == null)
				continue;
			lTroop.EnableDetectables(pIsDetectableAll);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 휘하 부대를 고려하지 않고 이 지휘관 자체의 좌표를 갱신합니다.
	private void _UpdateCommanderCoord(Coord2 pCoord, bool pIsArrived)
	{
		base.UpdateCoord(pCoord, pIsArrived);

		// 스테이지 좌표를 갱신합니다.
		mStageCommander.OnMoveTo(pCoord, pIsArrived);
	}
	//-------------------------------------------------------------------------------------------------------
	// 휘하 부대를 고려하지 않고 이 지휘관 자체의 방향각을 갱신합니다.
	private void _UpdateCommanderDirectionY(int pDirectionY)
	{
		base.UpdateDirectionY(pDirectionY);

		mDirectionVector = Coord2.RoughDirectionVector(pDirectionY, cDirectionVectorSize);

		// 스테이지 방향각을 갱신합니다.
		mStageCommander.OnRotateToDirectionY(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 유저의 AI에 연결합니다.
	private void _LinkUserAi(int pAiUserIdx)
	{
		if (mAiUserIdx == pAiUserIdx)
			return;

		// 기존에 AI가 등록된 유저가 있었다면 먼저 해제합니다.
		if (mAiUserIdx >= 0)
		{
			for (int iUser = 0; iUser <= aBattleGrid.aMaxUserIdx; ++iUser)
				if (aBattleGrid.aUsers[iUser] != null)
					aBattleGrid.aUsers[iUser].aAiManager.RemoveCommander(this);

			if (mCommanderItem.mCommanderItemType != CommanderItemType.DefenderCommander) // 방어군은 자체 AI 없음
				aBattleGrid.aAiCollector.RemoveCommander(this);
		}

		// AI 정보를 갱신합니다.
		mAiUserIdx = pAiUserIdx;

		// AI 등록을 합니다.
		if (pAiUserIdx >= 0)
		{
			for (int iUser = 0; iUser <= aBattleGrid.aMaxUserIdx; ++iUser)
				if (aBattleGrid.aUsers[iUser] != null)
					aBattleGrid.aUsers[iUser].aAiManager.AddCommander(this);

			if (mCommanderItem.mCommanderItemType != CommanderItemType.DefenderCommander) // 방어군은 자체 AI 없음
				aBattleGrid.aAiCollector.AddCommander(this);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 슬롯의 부대를 생성합니다.
	private void _CreateTroop(int pTroopSlotIdx, Coord2 pFormationCoord, int pFormationDirectionY, bool pIsFirstCreation)
	{
		CommanderTroopInfo lTroopInfo = mTroopSet.mTroopInfos[pTroopSlotIdx];
		if (lTroopInfo == null)
			return;

		int lTroopGridIdx = aBattleGrid.GetCommanderTroopGridIdx(this, pTroopSlotIdx);
		Coord2 lTroopSpawnOffset = Coord2.RoughRotationVector(
			mTroopSet.mFormationPositionOffsets[pTroopSlotIdx],
			pFormationDirectionY);
		Coord2 lTroopSpawnCoord = pFormationCoord + lTroopSpawnOffset;
		BattleTroop.StartState lStartState = BattleTroop.StartState.Spawn; // 기본은 일반 스폰
		if (mCommanderItem.mIsTeamUndefined) // 팀이 안 정해져 있다면
		{
			lStartState = BattleTroop.StartState.Hold; // 멈춤 상태로 시작
		}
		else if (lTroopInfo.mCommanderTroopType != CommanderTroopType.Default) // 부대 타입이 기본이 아니라면
		{
			switch (lTroopInfo.mCommanderTroopType) // 부대 타입에 따라 시작 상태를 지정
			{
			case CommanderTroopType.Npc:
				lStartState = BattleTroop.StartState.Npc;
				break;
			}
		}

		BattleTroop lTroop = aBattleLogic.CreateTroop(
			aBattleGrid,
			lTroopGridIdx,
			lTroopInfo.mTroopSpec,
			lTroopSpawnCoord,
			pFormationDirectionY,
			this,
			pTroopSlotIdx,
			lStartState);

		lTroop.aOnDestroyingDelegate += _OnDestroyingTroop;
		lTroop.aOnDestroyedDelegate += _OnDestroyedTroop;

		lTroop.OnVeteranRankup(mVeterancyRank);

		if (pIsFirstCreation) // 첫 스폰
		{
			mMaxTotalMilliHp += lTroop.aStat.aMaxMilliHp; // 최대 HP는 첫 스폰에서만 계산
		}
		else // 부대 충원
		{
			aUser.aCurrentTroopCount++; // 첫 스폰 전에 이미 부대 수는 초기화 되어 있기에 충원일 때만 증가시킴
		}
		mCurTotalMilliHp += lTroop.aStat.aCurMilliHp;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroop.OnDestroyingDelegate
	private void _OnDestroyingTroop(BattleTroop pTroop)
	{
		// 부대 파괴 전 콜백을 호출합니다.
		if (aOnDestroyingTroopDelegate != null)
			aOnDestroyingTroopDelegate(this, pTroop.aTroopSlotIdx);

		// 리더 부대라면 그 참조를 없앱니다.
		if (mLeaderTroop == pTroop)
			mLeaderTroop = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroop.OnDestroyedDelegate
	private void _OnDestroyedTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		TroopSpec lTroopSpec = mTroopSet.mTroopInfos[pTroopSlotIdx].mTroopSpec;

		// 포메이션 순번을 갱신합니다.
		UpdateFormationOrder();

		// 유저 부대의 경우 충원 리스트에 넣습니다.
		if (aUser != null)
		{
			//Debug.Log("Add Recruit Wait List Slot Id - " + pTroopSlotIdx + "/Commander id - " + aId);
			mRecruitTroopWaitList.Add(pTroopSlotIdx);
//			Debug.Log("Add Recruit Troop List - " + mRecruitTroopWaitList[mRecruitTroopWaitList.Count - 1]);
			mRecruitMp += lTroopSpec.mStatTable.Get(TroopStat.SpawnMp);
			aUser.aCurrentTroopCount--;
		}

		// 부대 종류별 수를 갱신합니다.
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (mTroopSet.mTroopKindCountInfos[iKind] != null && 
                mTroopSet.mTroopKindCountInfos[iKind].mTroopIconAssetKey.aAssetPath.Equals(lTroopSpec.mFieldIconAssetKey.aAssetPath))
			{
				mTroopKindCount[iKind]--;
				break;
			}
		}

		// 부대가 모두 사라질 때의 처리를 합니다.
		if (mIsDestroyOnNoTroop)
		{
			if (mLiveTroopCount > 0)
			{
				// 낙오 부대만 있고 활동 부대가 없다면 낙오 부대까지 모두 파괴합니다.
				if (mActiveTroopCount <= 0)
					ReserveTask(_DestroyFallBehindTroops, 0); // 콜백 호출 흐름상 다음 프레임에 진행되도록 예약
			}
			else
			{
				// 모든 부대가 파괴되었다면 지휘관도 같이 파괴합니다.
				ReserveTask(_OnDestroyAllTroops, 0); // 콜백 호출 흐름상 다음 프레임에 진행되도록 예약
			}
		}

		// 부대 파괴 후 콜백을 호출합니다.
		if (aOnDestroyedTroopDelegate != null)
			aOnDestroyedTroopDelegate(this, pTroopSlotIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _DestroyFallBehindTroops(FrameTask pTask, int pFrameDelta)
	{
		//Debug.Log("_DestroyFallBehindTroops() - " + this);

		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mTroops[iTroop];
			if ((lTroop != null) &&
				lTroop.aIsFallBehind)
				lTroop.Destroy();
		}		
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnDestroyAllTroops(FrameTask pTask, int pFrameDelta)
	{
		//Debug.Log("_OnDestroyAllTroops() - " + this);

		Destroy();
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnCoolTimeEnd(FrameTask pTask, int pFrameDelta)
	{
		//Debug.Log("_OnCoolTimeEnd() - " + this);

		mIsCoolTime = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnReserveGrabCheckPoint(FrameTask pTask, int pFrameDelta)
	{
		if (mReserveGrabCheckPoint == null)
			return;

		GrabCheckPoint(mReserveGrabCheckPoint);
		mReserveGrabCheckPoint = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameTaskDelegate
	private void _OnRetreatDestroy(FrameTask pTask, int pFrameDelta)
	{
		mOnRetreatDestroyTask = null;

		Destroy();
	}	
	//-------------------------------------------------------------------------------------------------------
	// 명시적 타게팅을 지정합니다.
	private void _SetExplicitTargeting(BattleTroop pExplicitTargetTroop)
	{
		if (mExplicitTargetingNode.List != null)
			mExplicitTargetingNode.List.Remove(mExplicitTargetingNode);

		if (pExplicitTargetTroop != null)
		{
			pExplicitTargetTroop.aExplicitTargetingCommanderList.AddLast(mExplicitTargetingNode);
			mExplicitTargetTroopLink.Set(pExplicitTargetTroop);
			mSqrExplicitTargetTroopEncounterDistance = int.MaxValue;
		}
		else
		{
			mExplicitTargetTroopLink.Set(null);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트상의 번호를 지정합니다.
	private void _SetTypeAndScriptId()
	{
		if (mUser != null)
		{
			mBelongingType = BelongingType.User;
			mScriptUserId = mUser.aUserIdx;
			mScriptSlotId = mCommanderSlotIdx;
			return;
		}
		BattleCheckPoint lCheckPoint = aBattleGrid.GetCommanderGridIdxCheckPoint(mCommanderGridIdx);
		if (lCheckPoint != null)
		{
			mBelongingType = BelongingType.CheckPoint;
			mScriptUserId = cCheckPointUserId;
			mScriptSlotId = (int)lCheckPoint.aCheckPointGuid;
			return;
		}
		BattleEventPoint lEventPoint = aBattleGrid.GetCommanderGridIdxEventPoint(mCommanderGridIdx);
		if (lEventPoint != null)
		{
			mBelongingType = BelongingType.EventPoint;
			mScriptUserId = cEventPointUserId;
			mScriptSlotId = (int)lEventPoint.aEventPointGuid;
			return;
		}

		Debug.LogError("invalid BelongingType");
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleUser mUser;
	private BattleUser mTeamUser;
	private BattleUserInfo mUserInfo;
	private CommanderItem mCommanderItem;
	private CommanderSpec mCommanderSpec;
	private int mCommanderSlotIdx;
	private int mCommanderGridIdx;
	private BelongingType mBelongingType;
	private int mScriptUserId;
	private int mScriptSlotId;
	private BattleCommanderStatus mStatus;
	private BattleAiManager mActiveAiManager;
	private int mAiUserIdx;

	private GuidObjectChainItem<BattleCommander> mCommanderChainItem;

	private Coord2 mDirectionVector;

	private IStageCommander mStageCommander;

	private int mLiveTroopCount;
	private int mActiveTroopCount;
	private bool mIsCoolTime;
	private CommanderTroopSet mTroopSet;
	private FormationState mFormationState;
	private CommanderTroopSetItem mTroopSetItem;
	private BattleTroop[] mTroops;
	private int[] mTroopKindCount;
	private int[] mTroopFormationIdxs;
	private Coord2 mFormationMoveCenteringOffset;
	private BattleTroop mLeaderTroop;
	private BattleTroop mFrontTroop;
	private bool mIsCaptureTypeTroop;

	private LinkedListNode<BattleCommander> mExplicitTargetingNode;
	private GuidLink<BattleTroop> mExplicitTargetTroopLink;
	private int mSqrExplicitTargetTroopEncounterDistance;
	private Coord2 mExplicitTargetCoord;

	private Coord2 mControlCoord;

	private int mMaxAdvanceSpeed;
	private int mMaxTotalMilliHp;
	private int mCurTotalMilliHp;

	private BattleCheckPoint mTryCheckPoint;
	private BattleCheckPoint mReserveGrabCheckPoint;

	private List<int> mRecruitTroopWaitList;
	private int mRecruitMp; // 재소환시 필요한 MP

	private bool mIsMoving;
	private bool mIsSomeTroopAttacking;
	private bool mIsLeaderTroopAttacking;
	private bool mIsDestroyOnNoTroop;
	private FrameTask mOnRetreatDestroyTask;
	private bool mIsFleeing;

	private TimeOnTargetState mTimeOnTargetState;
	private int mTimeOnTargetCounter;
	private TimeOnTargetState[] mTroopTimeOnTargetStates;
	private int[] mTroopFireDelays;
	private int mMaxTroopFireDelay;

	private int[] mCommanderAttackRefCounts;
	private int[] mLastAttackCommanderGridIdxs;
	private int mLastFoundEnemyCommanderGridIdx;

	private int mVeterancyRank;
	private int mVeterancyExperience;

	private BattleCommanderSkill mSkill;

	private BattleCheckPoint mLinkCheckPoint;
	private BattleEventPoint mLinkEventPoint;

	private GuidLink<BattleTargetingDummy> mTargetingDummyLink;

	private static readonly int[] sRestTroopFormationIdxs = new int[CommanderTroopSet.cMaxTroopCount];
}
