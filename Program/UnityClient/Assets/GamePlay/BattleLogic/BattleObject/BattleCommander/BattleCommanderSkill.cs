using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleCommanderSkill
{
	public enum ActivationState
	{
		None,
		ReadyToUse,
		Use,
		ReadyFromUse,
		Cooldown,
	}

	public delegate void OnStartSkillDelegate(BattleCommander pCommander);
	public delegate void OnStopSkillDelegate(BattleCommander pCommander);

	private delegate void OnFrameUpdateDelegate(int pFrameDelta);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 지휘관
	public BattleCommander aCommander
	{
		get { return mCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 스펙
	public SkillSpec aSkillSpec
	{
		get { return mSkillSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// (지휘관 성장이 반영된) 스킬 발동 시간
	public int aReadyDelay
	{
		get { return mReadyDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 발동 타이머
	public int aReadyTimer
	{
		get { return mReadyTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// (지휘관 성장이 반영된) 스킬 사용 시간
	public int aUseDelay
	{
		get { return mUseDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 사용 타이머
	public int aUseTimer
	{
		get { return mUseTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// (지휘관 성장이 반영된) 스킬 쿨다운 시간
	public int aCooldownDelay
	{
		get { return mCooldownDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 쿨다운 타이머
	public int aCooldownTimer
	{
		get { return mCooldownTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과 고유 번호
	public uint aSkillEffectGuid
	{
		get { return mSkillEffectGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 시작 여부
	public bool aIsStarted
	{
		get { return mIsStarted; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 상태
	public ActivationState aActivationState
	{
		get { return mActivationState; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 타겟 좌표
	public Coord2 aSkillTargetCoord
	{
		get { return mSkillTargetCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 방향
	public int aSkillDirectionY
	{
		get { return mSkillDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 시작 콜백
	public OnStartSkillDelegate aOnStartSkillDelegate
	{
		set { mOnStartSkillDelegate = value; }
		get { return mOnStartSkillDelegate; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 중단 콜백
	public OnStopSkillDelegate aOnStopSkillDelegate
	{
		set { mOnStopSkillDelegate = value; }
		get { return mOnStopSkillDelegate; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 여부
	public bool aIsUpdating
	{
		get { return (mOnFrameUpdateDelegate != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 사용중
	public bool aIsUseSkill
	{
		get { return ((mReadyTimer > 0) || (mUseTimer > 0) || (mCooldownTimer > 0)); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 가능 여부
	public bool aIsMovable
	{
		get { return mIsMovable; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleCommander pCommander)
	{
		mCommander = pCommander;
		mBattleLogic = pCommander.aBattleLogic; // 코딩 편의적인 중복 정보
		mBattleGrid = pCommander.aBattleGrid; // 코딩 편의적인 중복 정보

		mSkillSpec = mCommander.aCommanderSpec.mSkillSpecs[0];

		if (mSkillSpec != null)
		{
			mReadyDelay = (int)((Int64)mSkillSpec.mReadyDelay * pCommander.aTroopSet.mSkillReadyDelayMultiplyRatio / 10000); // 지휘관 성장에 따른 스킬 시간 보정
			mUseDelay = (int)((Int64)mSkillSpec.mUseDelay * pCommander.aTroopSet.mSkillUseDelayMultiplyRatio / 10000); // 지휘관 성장에 따른 스킬 시간 보정
			mCooldownDelay = (int)((Int64)mSkillSpec.mCooldownDelay * pCommander.aTroopSet.mSkillCooldownDelayMultiplyRatio / 10000); // 지휘관 성장에 따른 스킬 시간 보정
		}

		mIsStarted = false;
		mActivationState = ActivationState.None;

		mReadyTimer = 0;
		mUseTimer = 0;
		mCooldownTimer = 0;
		mIsMovable = true; // 이동 가능 상태로 초기화
	}
	//-------------------------------------------------------------------------------------------------------
	// 리셋 함수
	public void Reset()
	{
		if (mIsStarted)
		{
			mReadyTimer = 0;
			mUseTimer = 0;
			mCooldownTimer = 0; // StopSkill()이 무조건 작동하도록
			StopSkill(0);
		}

		mCommander = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 시작합니다.
	public void StartSkill(Coord2 pSkillTargetCoord, int pSkillDirectionY, int pStartDelta)
	{
		if (mCommander.aIsFleeing)
			return;
		mCommander.Retreat(false); // 퇴각중이었다면 취소

		if (mIsStarted)
			return; // 명시적으로 StopSkill()을 먼저 호출해야만 함
		if (mSkillSpec == null)
			return;
		if (mReadyTimer > 0)
			return;
		if (mUseTimer > 0)
			return;
		if (mCooldownTimer > 0)
			return;

		mSkillEffectGuid = mBattleLogic.GenerateSkillEffectGuid();
		mIsStarted = true;
		mSkillTargetCoord = pSkillTargetCoord;
		mSkillDirectionY = pSkillDirectionY;
		mIsMovable = mSkillSpec.mIsMovable; // 스킬이 시작되면 스킬의 이동 여부에 따름

		// 위치 지정 사용 타입이라면 위치에 이펙트를 표시합니다.
		if (mSkillSpec.mUseType == SkillUseType.Position)
		{
			BattleGridTile lGridTile = mBattleGrid.GetTile(mSkillTargetCoord);
			if (lGridTile != null)
				mBattleLogic.CreateGridSkillEffect(
					lGridTile,
					mSkillTargetCoord,
					mSkillDirectionY,
					null,
					mReadyDelay,
					3000, // 임시로 [9/1/2015 jhpark]
					mSkillEffectGuid);
		}

		// 부대들에게 스킬 시작을 명령합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mCommander.aTroops[iTroop];
			if (lTroop != null)
				lTroop.aCommand.StartSkill(
					mSkillTargetCoord,
					mSkillDirectionY,
					mCommander.GetTroopFormationIdx(iTroop), // 스킬 사용 순간의 포메이션 자리 지정
					pStartDelta);
		}

		// 시작 콜백을 호출합니다.
		if (mOnStartSkillDelegate != null)
			mOnStartSkillDelegate(mCommander);

		// 상태 지정을 합니다.
		if (mReadyDelay > 0)
			_Start_ReadyToUse(pStartDelta);
		else
			_Start_Use(pStartDelta);

		// 활성화 스킬의 경우 시간이 지난 후의 자동 중단 예약을 합니다.
		if (mUseDelay > 0)
			mCommander.ReserveTask(_OnActiveSkillEnd, mReadyDelay + mUseDelay); // 준비 시간 지나고 사용까지 끝났을 때 호출
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 중단합니다.
	public void StopSkill(int pStopDelta)
	{
		if (mCommander.aIsFleeing)
			return;
		mCommander.Retreat(false); // 퇴각중이었다면 취소

		if (!mIsStarted)
			return;
		if (mUseTimer > 0)
			return;
		if (mUseTimer > 0)
			return;
		if (mCooldownTimer > 0)
			return;

		mIsStarted = false;

		// 부대들에게 스킬 중지를 명령합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = mCommander.aTroops[iTroop];
			if (lTroop != null)
				lTroop.aCommand.StopSkill(pStopDelta);
		}

		// 중단 콜백을 호출합니다.
		if (mOnStopSkillDelegate != null)
			mOnStopSkillDelegate(mCommander);

		// 상태 지정을 합니다.
		if (mUseDelay <= 0) // 즉, 토글 스킬이면
			_Start_ReadyFromUse(pStopDelta); // 레디 딜레이동안 사용 상태에서 빠져나옴
		else if (mCooldownDelay > 0)
			_Start_Cooldown(pStopDelta);
		else
			_Start_None();

		// 비활성화 상태 변환을 처리합니다.
		if (mReadyDelay >= 0) // 준비 시간이 필요한 스킬은
			mCommander.ReserveTask(_OnSkillDeActivated, mReadyDelay); // 비활성화도 마찬가지로 준비 시간이 지나야
		else // 준비 시간이 없는 스킬은
			_OnSkillDeActivated(null, pStopDelta); // 즉시 비활성화
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신을 합니다.
	public void OnFrameUpdate(int pFrameDelta)
	{
		if (mOnFrameUpdateDelegate != null)
			mOnFrameUpdateDelegate(pFrameDelta);

		if (mCooldownTimer > 0)
		{
			mCooldownTimer -= pFrameDelta;
			if (mCooldownTimer <= 0)
				mCooldownTimer = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 좌표에 스킬을 사용할 수 있는지를 검사합니다.
	public bool CheckValidTargetCoord(Coord2 pTargetCoord)
	{
		if (!mBattleGrid.Contains(pTargetCoord))
			return false;

		switch (mSkillSpec.mFunction)
		{
		case SkillFunction.GroundAttack:
			{
				bool lIsValid = false;
				for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
				{
					BattleTroop lTroop = mCommander.aTroops[iTroop];
					if (lTroop == null)
						continue;
					Coord2 lTargetCoordVector = pTargetCoord - lTroop.aCoord;
					int lSqrTargetCoordDistance = lTargetCoordVector.SqrMagnitude();
					if (lSqrTargetCoordDistance > lTroop.aStat.aSqrMaxAttackRange)
						continue; // 이 부대가 공격하기에는 너무 멀지만 다른 부대를 더 찾아봄
					if (lSqrTargetCoordDistance < lTroop.aStat.aSqrMinAttackRange)
						return false; // 이 부대가 공격하기에는 너무 가까우므로 사용 불가
					lIsValid = true;
				}
				return lIsValid;
			}
			//break;
		}

		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 해당 상태를 시작합니다.
	private void _Start_ReadyToUse(int pExtraUpdateTime)
	{
		mActivationState = ActivationState.ReadyToUse;
		mReadyTimer = mReadyDelay;
		mOnFrameUpdateDelegate = _OnFrameUpdate_ReadyToUse;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private void _OnFrameUpdate_ReadyToUse(int pFrameDelta)
	{
		mReadyTimer -= pFrameDelta;
		if (mReadyTimer <= 0)
		{
			_Start_Use(-mReadyTimer);
			mReadyTimer = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 상태를 시작합니다.
	private void _Start_Use(int pExtraUpdateTime)
	{
		mActivationState = ActivationState.Use;
		mUseTimer = mUseDelay - pExtraUpdateTime;
		mOnFrameUpdateDelegate = _OnFrameUpdate_Use;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private void _OnFrameUpdate_Use(int pFrameDelta)
	{
		if (mUseTimer > 0)
		{
			mUseTimer -= pFrameDelta;
			if (mUseTimer <= 0)
			{
				StopSkill(-mUseTimer);
				mUseTimer = 0;
			}
		}
		else
		{
			mOnFrameUpdateDelegate = null; // 중단할 때까지 갱신 없음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 상태를 시작합니다.
	private void _Start_ReadyFromUse(int pExtraUpdateTime)
	{
		mActivationState = ActivationState.ReadyFromUse;
		mReadyTimer = mReadyDelay - pExtraUpdateTime;
		mOnFrameUpdateDelegate = _OnFrameUpdate_ReadyFromUse;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private void _OnFrameUpdate_ReadyFromUse(int pFrameDelta)
	{
		mReadyTimer -= pFrameDelta;
		if (mReadyTimer <= 0)
		{
			if (mCooldownDelay > 0)
				_Start_Cooldown(-mReadyTimer);
			else
				_Start_None();
			mReadyTimer = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 상태를 시작합니다.
	private void _Start_Cooldown(int pExtraUpdateTime)
	{
		mActivationState = ActivationState.Cooldown;
		mCooldownTimer = mCooldownDelay;
		mOnFrameUpdateDelegate = _OnFrameUpdate_Cooldown;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private void _OnFrameUpdate_Cooldown(int pFrameDelta)
	{
		mCooldownTimer -= pFrameDelta;
		if (mCooldownTimer <= 0)
		{
			_Start_None();
			mCooldownTimer = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 상태를 시작합니다.
	private void _Start_None()
	{
		mActivationState = ActivationState.None;
		mOnFrameUpdateDelegate = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 액티브 스킬이 끝날 때 호출됩니다.
	private void _OnActiveSkillEnd(FrameTask pTask, int pFrameDelta)
	{
		StopSkill(0);
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 스킬이 완전히 비활성화 될 때 호출됩니다.
	private void _OnSkillDeActivated(FrameTask pTask, int pFrameDelta)
	{
		mIsMovable = true; // 스킬이 비활성화 되면 원래의 이동 가능 상태로
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCommander mCommander;
	private BattleLogic mBattleLogic;
	private BattleGrid mBattleGrid;
	private SkillSpec mSkillSpec;

	private int mReadyDelay;
	private int mUseDelay;
	private int mCooldownDelay;

	private bool mIsStarted;
	private ActivationState mActivationState;
	private uint mSkillEffectGuid;
	private int mReadyTimer;
	private int mUseTimer;
	private int mCooldownTimer;
	private Coord2 mSkillTargetCoord;
	private int mSkillDirectionY;
	private bool mIsMovable;

	private OnStartSkillDelegate mOnStartSkillDelegate;
	private OnStopSkillDelegate mOnStopSkillDelegate;

	private OnFrameUpdateDelegate mOnFrameUpdateDelegate;
}
