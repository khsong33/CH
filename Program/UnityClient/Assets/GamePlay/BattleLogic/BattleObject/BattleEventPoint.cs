using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleEventPoint : BattleObject
{
	public enum EventType
	{
		TriggerReady,
		TriggerActivated,
	}

	private delegate void OnTriggerReadyGroundEventDelegate(BattleEventPoint pThis);
	private delegate bool OnTriggerActivatedGroundEventDelegate(BattleEventPoint pThis, BattleTroop pEventTroop);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 인덱스
	public int aEventPointIdx
	{
		get { return mEventPointIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// GUID
	public uint aEventPointGuid
	{
		get { return mEventPointGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 여부
	public bool aIsActive
	{
		get { return mIsActive; }
		set
		{
			mIsActive = value;
			if (mOnTriggerReadyEventTask != null)
			{
				mOnTriggerReadyEventTask.Cancel();
				mOnTriggerReadyEventTask = null;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 시작 딜레이
	public int aEventStartDelay
	{
		get { return mEventStartDelay; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 그라운드 이벤트 스펙
	public GroundEventSpec aGroundEventSpec
	{
		get { return mGroundEventSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 속성
	public override IStageObject aStageObject
	{
		get { return mStageEventPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 인터페이스
	public IStageEventPoint aStageEventPoint
	{
		get { return mStageEventPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 발동 준비가 되었는지 여부
	public bool aIsTriggerReady
	{
		get { return (mGroundEventSpec != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 이벤트에 연결되어 있는 지휘관의 슬롯 번호
	public int aCommanderSlotIdx
	{
		get { return (BattleGrid.cEventPointCommanderItemStartIdx + mEventPointIdx); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 이벤트에 연결되어 있는 지휘관 아이템
	public CommanderItem aCommanderItem
	{
		get { return mEventCommanderItem; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("EP<{0},{1}>(aGuid:{2} aTeamIdx:{3})", aCoord.x, aCoord.z, aGuid, aTeamIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleEventPoint(BattleLogic pBattleLogic)
		: base(pBattleLogic)
	{
		if (!sIsSetUpGroundEventFunctionDelegates)
		{
			_SetUpGroundEventFunctionDelegates();
			sIsSetUpGroundEventFunctionDelegates = true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public void OnCreateEventPoint(
		BattleGridTile pGridTile,
		int pEventPointIdx,
		uint pEventPointGuid,
		int pEventStartDelay,
		int pEventRepeatCount,
		int pEventRepeatDelay,
		String[] pEventNames)
	{
		OnCreateObject(
			pGridTile.aBattleGrid,
			true, // bool pIsCheckDetection
			false, // bool pIsDetecting
			pGridTile.aBattleGrid.aIsNoneDetectables, // bool pIsDetectables (탐지 불가 상태에서 시작)
			pGridTile.aCenterCoord,
			pGridTile.aDirectionY,
			0); // int pTeamIdx (이벤트 지점은 중립팀으로 간주)

		mEventPointIdx = pEventPointIdx;
		mEventPointGuid = pEventPointGuid;
		mEventStartDelay = pEventStartDelay;
		mEventRepeatCount = pEventRepeatCount;
		mEventRepeatDelay = pEventRepeatDelay;
		mEventNames = pEventNames;

		mDetectionObjectChainItem = aBattleGrid.aDetectionObjectChains[aTeamIdx].AddLast(this);

		mStageEventPoint = aBattleGrid.aStage.CreateEventPoint(this);

		mEventCommanderItem = new CommanderItem(aCommanderSlotIdx);
		mEventSpawnCommanderLink = new GuidLink<BattleCommander>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	public override void OnDestroy()
	{
		//Debug.Log("OnDestroy() - " + this);

		// 이벤트와 연결된 부대가 있다면 파괴합니다.
		if (mEventSpawnCommanderLink.Get() != null)
			mEventSpawnCommanderLink.Get().Destroy();

		// 이벤트를 해제합니다.
		mGroundEventSpec = null;

		// 스테이지에서 제거합니다.
		mStageEventPoint.DestroyObject();
		mStageEventPoint = null;

		// 등록을 해제합니다.
		mDetectionObjectChainItem.Empty();

		// 상위 버전을 호출합니다.
		base.OnDestroy();

		// 풀에 돌려놓습니다.
		aBattleLogic.DestroyEventPoint(this);

		// 연결된 태스크를 취소합니다.
		if (mOnTriggerReadyEventTask != null)
		{
			mOnTriggerReadyEventTask.Cancel();
			mOnTriggerReadyEventTask = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 격자 이벤트의 처리가 가능한지 검사합니다.
	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
	{
		return (pGridTouch == BattleGrid.Touch.OnClick);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트를 초기화합니다.
	public void ResetEvent()
	{
		if (mEventNames.Length <= 0)
			return;

		mEventCommanderItem.Reset();

		mEventRepeatCounter = mEventRepeatCount;

		if (mEventStartDelay >= 0) // 시작 딜레이가 음수이면 수동으로 준비시켜야 함
			ReserveTask(_OnTriggerReady, mEventStartDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		base.UpdateCoord(pCoord, pIsArrived);

		// 스테이지 좌표를 갱신합니다.
		mStageEventPoint.UpdateCoord(pCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void UpdateDirectionY(int pDirectionY)
	{
		base.UpdateDirectionY(pDirectionY);

		// 스테이지 방향각을 갱신합니다.
		mStageEventPoint.UpdateDirectionY(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleObject 메서드
	public override void OnDetected(int pDetectingTeamIdx, bool pIsDetected, BattleObject pDetectingObject)
	{
		//Debug.Log("OnDetected() pDetectingTeamIdx=" + pDetectingTeamIdx + " pIsDetected=" + pIsDetected + " - " + this);

		// 탐지가 되는 경우, 탐지하는 팀이 적이라면 대응을 할 수 있도록 합니다.

		// 스테이지의 색적 변화를 갱신합니다.
		mStageEventPoint.SetDetected(pDetectingTeamIdx, pIsDetected);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대가 이벤트를 발동시킬 수 있느지를 검사합니다.
	public bool CheckEvent(BattleTroop pEventTroop)
	{
		if (pEventTroop.aIsFallBehind)
			return false; // 낙오 부대 제외

		Coord2 lEventVector = aCoord - pEventTroop.aCoord;
		if (lEventVector.SqrMagnitude() > BattleConfig.get.mSqrEventPointCheckRange)
			return false; // 범위 밖

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 트리거를 준비시킵니다.
	public void SetTriggerReady()
	{
		if (aIsTriggerReady)
			return;

		_OnTriggerReady(null, 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대에 대해서 트리거를 발동시킵니다.
	public void ActivateTrigger(BattleTroop pEventTroop)
	{
		if (mGroundEventSpec == null)
			return;

		if (_OnTriggerActivated(pEventTroop)) // 반환값은 반복처리 여부
		{
			if (mEventRepeatCounter > 0)
				ReserveTask(_OnTriggerReady, mEventRepeatDelay);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대에게 발견되었을 때의 처리를 합니다.
	public void OnDetectedByTroop(BattleTroop pDetectingTroop)
	{
		switch (mGroundEventSpec.mFunction)
		{
		case GroundEventFunction.Commander:
			{
				if (mEventSpawnCommanderLink.Get() != null)
				{
					mEventSpawnCommanderLink.Get().SetLinkEventPoint(null); // 이후로는 연결을 끊음
					ActivateTrigger(pDetectingTroop);
				}
			}
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 델리게이트 테이블을 초기화합니다.
	private static void _SetUpGroundEventFunctionDelegates()
	{
		sOnTriggerReadyGroundEventDelegates = new OnTriggerReadyGroundEventDelegate[GroundEventSpec.cGroundEventFunctionCount];
		sOnTriggerReadyGroundEventDelegates[(int)GroundEventFunction.MedKit] = _OnTriggerReadyGroundEvent_MedKit;
		sOnTriggerReadyGroundEventDelegates[(int)GroundEventFunction.Munition] = _OnTriggerReadyGroundEvent_Munition;
		sOnTriggerReadyGroundEventDelegates[(int)GroundEventFunction.Commander] = _OnTriggerReadyGroundEvent_Commander;
		sOnTriggerReadyGroundEventDelegates[(int)GroundEventFunction.RepairKit] = _OnTriggerReadyGroundEvent_RepairKit;

		sOnTriggerActivatedGroundEventDelegates = new OnTriggerActivatedGroundEventDelegate[GroundEventSpec.cGroundEventFunctionCount];
		sOnTriggerActivatedGroundEventDelegates[(int)GroundEventFunction.MedKit] = _OnTriggerActivatedGroundEvent_MedKit;
		sOnTriggerActivatedGroundEventDelegates[(int)GroundEventFunction.Munition] = _OnTriggerActivatedGroundEvent_Munition;
		sOnTriggerActivatedGroundEventDelegates[(int)GroundEventFunction.Commander] = _OnTriggerActivatedGroundEvent_Commander;
		sOnTriggerActivatedGroundEventDelegates[(int)GroundEventFunction.RepairKit] = _OnTriggerActivatedGroundEvent_RepairKit;

		sTeamSelectionIdxs = new int[BattleMatchInfo.cMaxUserCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerReadyGroundEventDelegate
	private static void _OnTriggerReadyGroundEvent_MedKit(BattleEventPoint pThis)
	{
		//Debug.Log("_OnTriggerReadyGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		//GroundEventSpec.MedKitParam lFunctionParam = pThis.mGroundEventSpec.mMedKitParam;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerActivatedGroundEventDelegate
	private static bool _OnTriggerActivatedGroundEvent_MedKit(BattleEventPoint pThis, BattleTroop pEventTroop)
	{
		//Debug.Log("_OnTriggerActivatedGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		GroundEventSpec.MedKitParam lFunctionParam = pThis.mGroundEventSpec.mMedKitParam;

		// 이벤트를 발생시킨 지휘관 휘하 부대의 HP를 증가시킵니다.
		BattleCommander lEventCommander = pEventTroop.aCommander;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = lEventCommander.aTroops[iTroop];
			if (lTroop == null)
				continue;
			if (lTroop.aTroopSpec.mType != TroopType.Infantry)
				continue; // 보병만 회복
			if (lTroop.aIsFallBehind)
				continue; // 낙오 부대 제외

			int lRecoverHp = (int)((Int64)lTroop.aStat.aMaxMilliHp * lFunctionParam.mHpRecoveryRatio / 10000);
			//Debug.Log("lRecoverHp=" + lRecoverHp + " - " + lTroop);
			lTroop.UpdateHp(TroopHpChangeType.Recover, lRecoverHp);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerReadyGroundEventDelegate
	private static void _OnTriggerReadyGroundEvent_Munition(BattleEventPoint pThis)
	{
		//Debug.Log("_OnTriggerReadyGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		//GroundEventSpec.MunitionParam lFunctionParam = pThis.mGroundEventSpec.mMunitionParam;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerActivatedGroundEventDelegate
	private static bool _OnTriggerActivatedGroundEvent_Munition(BattleEventPoint pThis, BattleTroop pEventTroop)
	{
		//Debug.Log("_OnTriggerActivatedGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		GroundEventSpec.MunitionParam lFunctionParam = pThis.mGroundEventSpec.mMunitionParam;

		// 이벤트를 발생시킨 부대 유저의 HP를 증가시킵니다.
		BattleUser lEventUser = pEventTroop.aUser;
		if (lEventUser != null)
		{
			//Debug.Log("lFunctionParam.mMpIncrement=" + lFunctionParam.mMpIncrement + " - " + lEventUser);
			pThis.aBattleGrid.aProgressInfo.ChangeMp(lEventUser.aUserIdx, lFunctionParam.mMpIncrement);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerReadyGroundEventDelegate
	private static void _OnTriggerReadyGroundEvent_Commander(BattleEventPoint pThis)
	{
		//Debug.Log("_OnTriggerReadyGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		GroundEventSpec.CommanderParam lFunctionParam = pThis.mGroundEventSpec.mCommanderParam;

		pThis.mEventCommanderItem.Init(CommanderItemType.EventCommander, lFunctionParam.mSpawnCommanderSpec, -1, 0, 1, 1);
		pThis.mEventCommanderItem.mIsTeamUndefined = true;

		BattleCommander lEventSpawnCommander = pThis._SpawnEventCommander(pThis.mEventCommanderItem, lFunctionParam.mTeamIdx);
		pThis.mEventSpawnCommanderLink.Set(lEventSpawnCommander);

		if (lFunctionParam.mIsAutoTeamUpdate)
			lEventSpawnCommander.SetLinkEventPoint(pThis); // 이벤트 연결

		lEventSpawnCommander.EnableTroopsDetecting(true);
		lEventSpawnCommander.EnableTroopsDetectables(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerActivatedGroundEventDelegate
	private static bool _OnTriggerActivatedGroundEvent_Commander(BattleEventPoint pThis, BattleTroop pEventTroop)
	{
		//Debug.Log("_OnTriggerActivatedGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		GroundEventSpec.CommanderParam lFunctionParam = pThis.mGroundEventSpec.mCommanderParam;

		BattleCommander lEventSpawnCommander = pThis.mEventSpawnCommanderLink.Get();
		if (lEventSpawnCommander == null)
			return true; // 부대 없음
		if (lEventSpawnCommander.aTeamIdx > 0)
			return false; // 팀이 이미 지정되어 있음

		// 이벤트를 발생시킨 부대의 국적에 따라 이벤트 부대의 팀을 바꿉니다.
		if (lEventSpawnCommander.aCommanderSpec.mDeckNation == pEventTroop.aCommander.aCommanderSpec.mDeckNation) // 서로 같은 국가라면
		{
			lEventSpawnCommander.UpdateTeam(pEventTroop.aTeamIdx, pEventTroop.aCommander.aTeamUser); // 이벤트 스폰 부대를 발견한 유저의 부대로 편입
			lEventSpawnCommander.UpdateAiState(lFunctionParam.mIsAi);
		}
		else // 서로 다른 국가라면
		{
			int lRandomUserCount = 0;
			for (int iUser = 0; iUser <= pThis.aBattleGrid.aMaxUserIdx; ++iUser)
			{
				BattleUser lUser = pThis.aBattleGrid.aUsers[iUser];
				if ((lUser.aUserInfo.mBattleNation == lEventSpawnCommander.aCommanderSpec.mDeckNation) && // 이벤트 스폰 커맨더와 같은 국가이고
					(lUser.aTeamIdx != pEventTroop.aTeamIdx)) // 이벤트 부대와 팀이 다르다면
					sTeamSelectionIdxs[lRandomUserCount++] = iUser; // 선택 후보
			}
			if (lRandomUserCount > 0) // 선택 후보가 있다면
			{
				int lTeamUserIdx = pThis.aBattleGrid.aSyncRandom.Range(0, lRandomUserCount);
				BattleUser lTeamUser = pThis.aBattleGrid.aUsers[lTeamUserIdx];
				lEventSpawnCommander.UpdateTeam(lTeamUser.aTeamIdx); // 이벤트 스폰 커맨더와 같은 국가중에 적팀인 국가로 편입(이때 유저 지정은 없음)
			}
			else // 선택 후보가 없다면
			{
				// 그대로 중립 부대로 남아서 모든 유저와 싸움
			}
		}

		return false; // 부대가 파괴될 때 이벤트를 재시작할 것이기에
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerReadyGroundEventDelegate
	private static void _OnTriggerReadyGroundEvent_RepairKit(BattleEventPoint pThis)
	{
		//Debug.Log("_OnTriggerReadyGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		//GroundEventSpec.RepairKitParam lFunctionParam = pThis.mGroundEventSpec.mRepairKitParam;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnTriggerActivatedGroundEventDelegate
	private static bool _OnTriggerActivatedGroundEvent_RepairKit(BattleEventPoint pThis, BattleTroop pEventTroop)
	{
		//Debug.Log("_OnTriggerActivatedGroundEvent_" + pThis.mGroundEventSpec.mFunction + "()");

		GroundEventSpec.RepairKitParam lFunctionParam = pThis.mGroundEventSpec.mRepairKitParam;

		// 이벤트를 발생시킨 지휘관 휘하 부대의 HP를 증가시킵니다.
		BattleCommander lEventCommander = pEventTroop.aCommander;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = lEventCommander.aTroops[iTroop];
			if (lTroop == null)
				continue;
			if (lTroop.aTroopSpec.mType == TroopType.Infantry)
				continue; // 보병 제외
			if (lTroop.aIsFallBehind)
				continue; // 낙오 부대 제외

			int lRecoverHp = (int)((Int64)lTroop.aStat.aMaxMilliHp * lFunctionParam.mHpRecoveryRatio / 10000);
			//Debug.Log("lRecoverHp=" + lRecoverHp + " - " + lTroop);
			lTroop.UpdateHp(TroopHpChangeType.Recover, lRecoverHp);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 부대를 소환합니다.
	private BattleCommander _SpawnEventCommander(CommanderItem pEventCommanderItem, int pTeamIdx)
	{
		BattleCommander lEventSpawnCommander = aBattleGrid.SpawnCommander(
			null,
			pEventCommanderItem,
			aCoord,
			aDirectionY,
			aBattleGrid.aTeamUsers[pTeamIdx][0]);
		//Debug.Log("_SpawnEventCommander() - " + lEventSpawnCommander);
		mEventSpawnCommanderLink.Set(lEventSpawnCommander);

		lEventSpawnCommander.aOnDestroyedDelegate += _OnDestroyedEventCommander;

		return lEventSpawnCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyedDelegate
	private void _OnDestroyedEventCommander(int pScriptUserId, int pScriptSlotId)
	{
		if (mEventRepeatCounter > 0)
			mOnTriggerReadyEventTask = ReserveTask(_OnTriggerReady, mEventRepeatDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTaskDelegate] 이벤트가 대기할 때 호출됩니다.
	private void _OnTriggerReady(FrameTask pTask, int pFrameDelta)
	{
		mOnTriggerReadyEventTask = null;

		--mEventRepeatCounter;

		// 발생할 이벤트를 결정합니다.
		int lEventIdx = aBattleGrid.aSyncRandom.Range(0, mEventNames.Length);
		mGroundEventSpec = GroundEventTable.get.FindGroundEventSpec(mEventNames[lEventIdx]);
		if (mGroundEventSpec == null)
			Debug.LogError("invalid GroundEventSpec:" + mEventNames[lEventIdx] + " - " + this);

		// 스테이지에 보이는 애셋이 있다면 탐지 가능 상태로 바꿉니다.
		if (mGroundEventSpec.mStageEventObjectAssetKey != null)
			EnableDetectables(true);

		// 스테이지에 보입니다.
		mStageEventPoint.OnTriggerReady();

		// 콜백을 호출합니다.
		if (aBattleGrid.aOnEventDelegate != null)
			aBattleGrid.aOnEventDelegate(this, EventType.TriggerReady);

		// 이벤트 대기 콜백을 호출합니다.
		if (sOnTriggerReadyGroundEventDelegates[(int)mGroundEventSpec.mFunction] != null)
			sOnTriggerReadyGroundEventDelegates[(int)mGroundEventSpec.mFunction](this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트가 발동할 때 호출됩니다.
	private bool _OnTriggerActivated(BattleTroop pEventTroop)
	{
		//Debug.Log("_OnTriggerActivated() pEventTroop=" + pEventTroop + " - " + this);

		// 이벤트 발동 콜백을 호출합니다.
		bool lIsRepeatEvent;
		if (sOnTriggerActivatedGroundEventDelegates[(int)mGroundEventSpec.mFunction] != null)
			lIsRepeatEvent = sOnTriggerActivatedGroundEventDelegates[(int)mGroundEventSpec.mFunction](this, pEventTroop);
		else
			lIsRepeatEvent = true;

		// 콜백을 호출합니다.
		if (aBattleGrid.aOnEventDelegate != null)
			aBattleGrid.aOnEventDelegate(this, EventType.TriggerActivated);

		// 이후로 탐지 불가로 바뀝니다.
		EnableDetectables(false);

		// 이벤트를 끕니다.
		mGroundEventSpec = null;

		// 스테이지에 보입니다.
		mStageEventPoint.OnTriggerActivated();

		// 발동된 이후로는 갱신을 멈춥니다.
		StopUpdate(); // OnFrameUpdate()

		return lIsRepeatEvent;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static bool sIsSetUpGroundEventFunctionDelegates = false;
	private static OnTriggerReadyGroundEventDelegate[] sOnTriggerReadyGroundEventDelegates;
	private static OnTriggerActivatedGroundEventDelegate[] sOnTriggerActivatedGroundEventDelegates;
	private static int[] sTeamSelectionIdxs;

	private int mEventPointIdx;
	private uint mEventPointGuid;
	private int mEventStartDelay;
	private int mEventRepeatCount;
	private int mEventRepeatDelay;
	private String[] mEventNames;

	private bool mIsActive;
	private int mEventRepeatCounter;
	private GroundEventSpec mGroundEventSpec;

	private GuidObjectChainItem<BattleObject> mDetectionObjectChainItem;

	private IStageEventPoint mStageEventPoint;

	private CommanderItem mEventCommanderItem;
	private GuidLink<BattleCommander> mEventSpawnCommanderLink;
	private FrameTask mOnTriggerReadyEventTask;
}
