using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopCommand
{
	public enum CommandType
	{
		None,

		DstCoordAdvance,
		TargetTroopAttack,
		TargetCoordAttack,
		ReturnAdvance,
		StartSkill,
		StopSkill,
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 명령 타입
	public CommandType aCommandType
	{
		get { return mCommandType; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopCommand()
	{
		mCommandTroopLink = new GuidLink<BattleTroop>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		mController = pController;
		mControlTroop = pController.aControlTroop;

		mCommandType = CommandType.None;
		mCommandTroopLink.Set(null);
	}
	//-------------------------------------------------------------------------------------------------------
	// 목표 좌표로의 이동을 시작합니다.
	public void StartDstCoordAdvance(Coord2 pDstCoord, int pStartDelta)
	{
		mCommandType = CommandType.DstCoordAdvance;
		mCommandCoord = pDstCoord;

		if (mControlTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
			return; // 핀다운 상태라면 무시
		if (mControlTroop.aIsFallBehind)
			return; // 낙오 상태라면 무시
		if (!mControlTroop.aInteraction.aIsAdvancable)
			return; // 상호 작용중이라서 무시

		mControlTroop.aControlState.mIsSetUpMode = (mControlTroop.aStat.aTroopStatTable.Get(TroopStat.SetUpDelay) > 0); // 타겟 좌표 지정이라면 설치 딜레이가 있을 경우 셋업 모드로
		mController.StartDstCoordAdvanceControl(pDstCoord, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대에 대한 공격을 시작합니다.
	public void StartTargetTroopAttack(BattleTroop pTargetTroop, int pStartDelta)
	{
		mCommandType = CommandType.TargetTroopAttack;
		mCommandTroopLink.Set(pTargetTroop);

		if (mControlTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
			return; // 핀다운 상태라면 무시
		if (mControlTroop.aIsFallBehind)
			return; // 낙오 상태라면 무시
		if (pTargetTroop == null)
			return; // 아무 반응 없음
		if (!mControlTroop.aInteraction.aIsAttackable)
			return; // 상호 작용중이라서 무시

		mControlTroop.aControlState.mIsSetUpMode = false; // 타겟 부대를 지정했다면 셋업 모드가 아님
		mController.StartTargetTroopAdvanceControl(pTargetTroop, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표에 대한 공격을 시작합니다.
	public void StartTargetCoordAttack(Coord2 pTargetCoord, bool pIsIgnoreRange, int pStartDelta)
	{
		mCommandType = CommandType.TargetCoordAttack;
		mCommandCoord = pTargetCoord;
		mIsCommandIgnoreRange = pIsIgnoreRange;

		if (mControlTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
			return; // 핀다운 상태라면 무시
		if (mControlTroop.aIsFallBehind)
			return; // 낙오 상태라면 무시
		if (!mControlTroop.aInteraction.aIsAttackable)
			return; // 상호 작용중이라서 무시

		mControlTroop.aControlState.mIsSetUpMode = false; // 타겟 좌표를 지정했다면 셋업 모드가 아님
		mController.StartTargetCoordAttackControl(pTargetCoord, pIsIgnoreRange, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 복귀 이동을 시작합니다.
	public void StartReturnAdvance(int pStartDelta)
	{
		// mCommandType = ??? 복귀 이동은 기록하지 않음

		Coord2 lCommanderVector = mControlTroop.aCommander.aCoord - mControlTroop.aCoord;
		if (lCommanderVector.SqrMagnitude() <= mControlTroop.aCommander.aTroopSet.mSqrGroupingRange) // 복귀 범위 이내라면
		{
			aControlTroop.aIsFallBehind = false; // 낙오 상태 해제
			StartLastCommand(pStartDelta); // 최근 명령을 바로 실행
		}
		else // 복귀 범위 밖이라면
		{
			aControlTroop.aIsFallBehind = true; // 낙오 상태 시작
			mController.StartReturnAdvanceControl(pStartDelta); // 복귀 이동 컨트롤로 전환
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 처리를 시작합니다.
	public void StartSkill(Coord2 pSkillTargetCoord, int pSkillDirectionY, int pSkillFormationIdx, int pStartDelta)
	{
		mCommandType = CommandType.StartSkill;
		mCommandCoord = pSkillTargetCoord;
		mCommandDirectionY = pSkillDirectionY;
		mCommandFormationIdx = pSkillFormationIdx;

		if (mControlTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
			return; // 핀다운 상태라면 무시
		if (mControlTroop.aIsFallBehind)
			return; // 낙오 상태라면 무시

		mControlTroop.aSkillProcess.StartProcess(
			mCommandCoord,
			mCommandDirectionY,
			mCommandFormationIdx,
			pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 처리를 중단합니다.
	public void StopSkill(int pExtraUpdateTime)
	{
		mCommandType = CommandType.StopSkill;

		// 핀다운 상태에서도 스킬 중단은 항상 허용
		//if (mControlTroop.aStat.aMoraleState == TroopMoraleState.PinDown)
		//	return; // 핀다운 상태라면 무시
		//if (mControlTroop.aIsFallBehind)
		//	return; // 낙오 상태라면 무시

		mControlTroop.aSkillProcess.StopProcess(false, pExtraUpdateTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// 최근 명령을 시작합니다.
	public void StartLastCommand(int pStartDelta)
	{
		switch (mCommandType)
		{
		case CommandType.DstCoordAdvance:
			StartDstCoordAdvance(mCommandCoord, pStartDelta);
			break;
		case CommandType.TargetTroopAttack:
			StartTargetTroopAttack(mCommandTroopLink.Get(), pStartDelta);
			break;
		case CommandType.TargetCoordAttack:
			StartTargetCoordAttack(mCommandCoord, mIsCommandIgnoreRange, pStartDelta);
			break;
		case CommandType.ReturnAdvance:
			StartReturnAdvance(pStartDelta);
			break;
		case CommandType.StartSkill:
			if (!mControlTroop.aSkillProcess.aIsStarted)
				StartSkill(mCommandCoord, mCommandDirectionY, mCommandFormationIdx, pStartDelta);
			break;
		case CommandType.StopSkill:
			if (mControlTroop.aSkillProcess.aIsStarted)
				StopSkill(pStartDelta);
			break;
		}

		if (mController.aReservedControl == null) // 별다른 컨트롤이 실행되지 않았다면
			mController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.AdvanceWhenNoEnemy,
				pStartDelta); // 탐색으로 전환
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopController mController;
	private BattleTroop mControlTroop;

	private CommandType mCommandType;
	private Coord2 mCommandCoord;
	private bool mIsCommandIgnoreRange;
	private int mCommandDirectionY;
	private GuidLink<BattleTroop> mCommandTroopLink;
	private int mCommandFormationIdx;
}
