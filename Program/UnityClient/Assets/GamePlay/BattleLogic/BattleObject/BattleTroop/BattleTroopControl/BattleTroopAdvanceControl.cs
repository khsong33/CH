using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopAdvanceControl : BattleTroopControl
{
	public enum AdvanceType
	{
		DstCoord, // 이동 목표 좌표로의 이동
		TargetTroop, // 공격 목표 부대로의 접근 이동
		SkillMove, // 스킬 이동
		Return, // 사기를 회복하고 부대 그룹에 복귀하는 이동으로 지휘관 위치를 향해 계속 좌표를 수정
	}

	internal enum PathMoveDirection
	{
		ForwardOnly,
		ForwardAndUrgentBackward,
	}
	internal enum PathMoveUrgent
	{
		NoUrgent,
		Urgent,
	}

	public class StartParam
	{
		internal AdvanceType mAdvanceType;

		internal Coord2 mDstCoord;
		internal GuidLink<BattleTroop> mTargetTroopLink;
		internal Coord2 mSkillCoord;
		internal BattleTroopMoveUpdater.MoveType mSkillMoveType;

		internal StartParam()
		{
			mTargetTroopLink = new GuidLink<BattleTroop>();
		}

		public void SetDstCoordAdvance(Coord2 pDstCoord)
		{
			mAdvanceType = AdvanceType.DstCoord;
			mDstCoord = pDstCoord;
		}
		public void SetTargetTroopAdvance(BattleTroop pTargetTroop)
		{
			mAdvanceType = AdvanceType.TargetTroop;
			mTargetTroopLink.Set(pTargetTroop);
		}
		public void SetReturnAdvance()
		{
			mAdvanceType = AdvanceType.Return;
		}
		public void SetSkillFormationAdvance(Coord2 pSkillFormationCoord)
		{
			mAdvanceType = AdvanceType.SkillMove;
			mSkillCoord = pSkillFormationCoord;
			mSkillMoveType = BattleTroopMoveUpdater.MoveType.FinalFormationMove;
		}
		public void SetSkillPositioningAdvance(Coord2 pSkillPositioningCoord)
		{
			mAdvanceType = AdvanceType.SkillMove;
			mSkillCoord = pSkillPositioningCoord;
			mSkillMoveType = BattleTroopMoveUpdater.MoveType.PositioningMove;
		}
	}

	internal const int cSetOnFrameUpdateCallLimit = 8;

	internal const int cReturnMovePathUpdateDelay = 1500;
	internal const int cAttackMovePathUpdateDelay = 1000;
	internal const int cLeadMovePathUpdateDelay = 2000;
	internal const int cFinalFormationMovePathUpdateDelay = 2000;
	internal const int cFollowMovePathUpdateDelay = 1000;

	internal const int cFolloweeTroopMoveWaitingDelay = 1;
	internal const int cEnemySearchDelay = 1000;

	public const int cSearchDelay = 1000;
	public const String cAdvanceMoveText = "Advance";
	public const String cAdvanceMoveHoldPrefix = "Advance Hold ";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Advance"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return true; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 타입
	public AdvanceType aAdvanceType
	{
		get { return mAdvanceType; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopAdvanceControl()
	{
		mStartParam = new StartParam();

		mFolloweeTroopLink = new GuidLink<BattleTroop>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		mFolloweeTroopLink.Set(null);

		SetController(pController);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (aControlTroopCommander == null)
			Debug.LogError("can't start control : aControlTroopCommander is null - " + this);

		if (!base.Start())
			return false;

		mMoveUpdaterRotationType = BattleTroopMoveUpdater.RotationType.RotateAndMove;

		mSetOnFrameUpdateCallCounter = 0;

		aControlTroop.aControlState.mIsInFormation = false;

		mAdvanceType = mStartParam.mAdvanceType;
		mElapsedTime = 0;

		switch (mAdvanceType)
		{
		case AdvanceType.DstCoord:
			_SetOnFrameUpdateDelegate(_OnStart_AdvanceDstCoord(), 0);
			break;
		case AdvanceType.TargetTroop:
			_SetOnFrameUpdateDelegate(_OnStart_AdvanceTargetTroop(), 0);
			break;
		case AdvanceType.SkillMove:
			_SetOnFrameUpdateDelegate(_OnStart_SkillMove(), 0);
			break;
		case AdvanceType.Return:
			_SetOnFrameUpdateDelegate(_OnStart_ReturnMove(), 0);
			break;
		}

		if (mOnFrameUpdateDelegate == null)
			return false; // 이동 없음

		aControlTroopCommander.OnTroopMoveControl(aControlTroop.aTroopSlotIdx, true); // 이동 컨트롤 시작

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool Stop(int pStopTime, int pStopDelta)
	{
		mElapsedTime += pStopDelta;

		if (!base.Stop(pStopTime, pStopDelta))
			return false;

		aControlTroop.aControlState.mIsInFormation = true;
		aControlTroop.aControlState.mIsWaitingFolloweeTroop = false; // 선행 부대 따라가기 상태 해제
		aControlTroop.aControlState.mIsAttackMoving = false; // 공격 이동 상태 해제

		aControlTroopCommander.OnTroopMoveControl(aControlTroop.aTroopSlotIdx, false); // 이동 컨트롤 종료

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		mElapsedTime += pUpdateDelta;

		// 핀 다운 상태에서는 탐색으로 돌아갑니다.
		if ((aControlTroopStat.aMoraleState == TroopMoraleState.PinDown) && // 핀다운인데
			!aControlTroopCommander.aIsFleeing && // 도망은 아니라면
			(mStartParam.mAdvanceType != AdvanceType.SkillMove)) // 스킬 이동도 예외
		{
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				pUpdateDelta); // 탐색으로 강제 전환
			return false;
		}

		// 프레임 초기화를 합니다.
		mSetOnFrameUpdateCallCounter = 0;

		return mOnFrameUpdateDelegate(pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnStopUpdate(int pStopTime, int pStopDelta, out int oExtraUpdateDelta)
	{
		if (aControlTroop.aMoveUpdater.aIsMoving)
			aControlTroop.aMoveUpdater.StopMove();

		return base.OnStopUpdate(pStopTime, pStopDelta, out oExtraUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표를 재지정합니다.
	public void ResetDstCoord(Coord2 pDstCoord, int pUpdateDelta)
	{
		aControlTroop.aControlState.mIsInFormation = false;

		mStartParam.SetDstCoordAdvance(pDstCoord);
		_SetOnFrameUpdateDelegate(_OnStart_AdvanceDstCoord(), pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 재지정합니다.
	public void ResetTargetTroop(BattleTroop pTargetTroop, int pUpdateDelta)
	{
		aControlTroop.aControlState.mIsInFormation = false;

		mStartParam.SetTargetTroopAdvance(pTargetTroop);
		_SetOnFrameUpdateDelegate(_OnStart_AdvanceTargetTroop(), pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수를 지정하고 갱신합니다.
	private bool _SetOnFrameUpdateDelegate(OnFrameUpdateDelegate pOnFrameUpdateDelegate, int pUpdateDelta)
	{
		aControlTroop.aControlState.mIsWaitingFolloweeTroop = false;

		mOnFrameUpdateDelegate = pOnFrameUpdateDelegate;

		if (++mSetOnFrameUpdateCallCounter >= cSetOnFrameUpdateCallLimit)
		{
			Debug.LogWarning("mSetOnFrameUpdateCallCounter:" + mSetOnFrameUpdateCallCounter + " overhead detected - " + this);
//			return true; // 과도한 재귀 호출
		}

		if (pUpdateDelta > 0)
			return mOnFrameUpdateDelegate(pUpdateDelta); // 갱신 진행

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_NoMove()
	{
		return _OnFrameUpdate_NoMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_NoMove(int pUpdateDelta)
	{
		aController.StartSearchControl(
			BattleTroopSearchControl.RetryOption.NoRetry,
			BattleTroopSearchControl.MoveOption.HoldPosition,
			aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 진군을 안한다면 탐색으로 전환
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceDstCoord()
	{
		// 이미 같은 좌표라면 이동을 하지 않습니다.
		if (mStartParam.mDstCoord == aControlTroop.aCoord)
			return _OnStart_NoMove();

		// 목표 좌표로의 이동중에는 적을 탐색합니다.
		mEnemySearchTimer = 0;

		// 목표 좌표로의 이동을 지정합니다.
		Coord2 lDstCoordVector = mStartParam.mDstCoord - aControlTroop.aCoord;
		int lDstCoordVectorSqrMagnitude = lDstCoordVector.SqrMagnitude();
		if (lDstCoordVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStartDistance) // 포메이션 좌표가 근처라면
		{
			return _OnStart_AdvanceDstCoord_FinalFormationMove(); // 바로 이동합니다.
		}
		else // 포메이션 좌표가 멀다면
		{
			BattleTroop lFolloweeTroop = _UpdateFolloweeTroop();
			if (lFolloweeTroop == null) // 따라갈 부대가 없다면
			{
				return _OnStart_AdvanceDstCoord_FrontMove(); // 앞장섭니다.
				//Debug.Log("lIsLead=" + lIsLead + " aControlTroop.aTroopSlotIdx=" + aControlTroop.aTroopSlotIdx + " lDstCoordVectorSqrMagnitude=" + lDstCoordVectorSqrMagnitude);
			}
			else // 선행 부대가 있다면
			{
				return _OnStart_AdvanceDstCoord_FollowMove(); // 따라갑니다.
				//Debug.Log("lIsLead=" + lIsLead + " aControlTroop.aTroopSlotIdx=" + aControlTroop.aTroopSlotIdx + " lDstCoordVectorSqrMagnitude=" + lDstCoordVectorSqrMagnitude);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceDstCoord_FinalFormationMove()
	{
		_SetPathMove(mStartParam.mDstCoord, cFinalFormationMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.FinalFormationMove, PathMoveDirection.ForwardAndUrgentBackward, PathMoveUrgent.NoUrgent);
		aControlTroop.aControlState.mIsInFormation = true;

		return _OnFrameUpdate_AdvanceDstCoord_FinalFormationMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceDstCoord_FinalFormationMove(int pUpdateDelta)
	{
		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;
		if (!aControlTroop.aMoveUpdater.aIsMoving) // 이동이 멈췄다면
		{
			//Debug.Log("StartSearchControl() in _OnFrameUpdate_AdvanceDstCoord_FinalFormationMove() - " + this);
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 진군을 안한다면 탐색으로 전환
			return false;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceDstCoord_FrontMove()
	{
		_SetPathMove(aControlTroopCommander.aControlCoord, cLeadMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.FrontMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);

		return _OnFrameUpdate_AdvanceDstCoord_FrontMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceDstCoord_FrontMove(int pUpdateDelta)
	{
		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;

		// AI 선봉 부대는 이동 도중에 적이 사정거리에 들어오는지를 파악하는 탐색을 합니다.
		if (aControlTroopCommander.aActiveAiManager != null)
			_UpdateTargetSearch(pUpdateDelta);

		// 목표 좌표에 충분히 가까워졌다면 포메이션 이동을 할지를 검사합니다.
		Coord2 lFormationCoordVector = mPathEndCoord - aControlTroop.aCoord;
		int lFormationCoordVectorSqrMagnitude = lFormationCoordVector.SqrMagnitude();
		if ((lFormationCoordVectorSqrMagnitude <= BattleConfig.get.mSqrFormationStartDistance) || // 포메이션 좌표가 근처거나
			!aControlTroop.aMoveUpdater.aIsMoving) // 이동이 멈췄다면
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceDstCoord_FinalFormationMove(), aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 포메이션 좌표로 이동합니다.

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceDstCoord_FollowMove()
	{
		BattleTroopMoveUpdater.MoveType lPathMoveType
			= aControlTroopStat.aIsFollowAdvance
			? BattleTroopMoveUpdater.MoveType.FollowAdvanceMove : BattleTroopMoveUpdater.MoveType.FollowFormationMove;
		_SetPathMove(mStartParam.mDstCoord, cFollowMovePathUpdateDelay, lPathMoveType, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent); // 실제 목표 좌표는 lFolloweeTroop.aCoord나 포메이션 좌표로 계속 갱신됨

		mIsResetFollowing = false;

		return _OnFrameUpdate_AdvanceDstCoord_FollowMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceDstCoord_FollowMove(int pUpdateDelta)
	{
		// 선행 부대를 얻습니다.
		BattleTroop lFolloweeTroop = mFolloweeTroopLink.Get();

		if ((lFolloweeTroop != null) && // 따라가는 부대가
			(lFolloweeTroop.aStat.aMoraleState == TroopMoraleState.PinDown)) // 핀 다운이 되면
			lFolloweeTroop = _UpdateFolloweeTroop(); // 따라갈 부대를 교체합니다.

		if ((lFolloweeTroop == null) || // 선행 부대가 사라졌거나
			lFolloweeTroop.aControlState.mIsInFormation) // 선행 부대가 포메이션에 들어갔다면
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceDstCoord_FrontMove(), pUpdateDelta); // 앞장섭니다.

		// 이동을 갱신합니다.
		Coord2 lDstCoordVector = mStartParam.mDstCoord - aControlTroop.aCoord;
		if (aControlTroopStat.aIsFollowAdvance)
		{
			// 선행 부대의 움직임을 기다릴 지를 판단합니다.
			aControlTroop.aControlState.mIsWaitingFolloweeTroop = lFolloweeTroop.aControlState.mIsWaitingFolloweeTroop; // 선행 부대의 상태를 받아와서 초기화
			int lFollowDirectionY;
			int lFollowVectorSqrMagnitude = -1; // 초기화 안됨 표시
			Coord2 lFollowVector = lFolloweeTroop.aCoord - aControlTroop.aCoord;
			if (!aControlTroop.aControlState.mIsWaitingFolloweeTroop) // 추가 체크
			{
				lFollowDirectionY = aControlTroop.aDirectionY; // 기본값
				int lVectorProduce = lFollowVector.x * lDstCoordVector.x + lFollowVector.z * lDstCoordVector.z;
				if (lVectorProduce < 0) // 목표 지점과는 반대 방향으로 따라갈 때
				{
					//lFollowVectorSqrMagnitude = lFollowVector.SqrMagnitude();
					//if (lFollowVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStopDistance) // 선행 부대와 아직 기준 거리 이상으로 멀어지지 않았다면
					{
						aControlTroop.aControlState.mIsWaitingFolloweeTroop = true; // 이 동안은 따라가기 대기 상태임
						lFollowDirectionY = lDstCoordVector.RoughDirectionY();
					}
				}
			}
			else
			{
				//Debug.Log("is waiting another waiting troop - " + this);
				lFollowDirectionY = lDstCoordVector.RoughDirectionY();
			}

			// 선행 부대의 움직임을 기다립니다.
			if (aControlTroop.aControlState.mIsWaitingFolloweeTroop)
			{
				// 차량은 회전이 느리므로 선행 부대를 기다리면서 멈춘 상태에서도 선행 부대를 향해 미리 회전을 해둡니다.
				if (aControlTroopSpec.mBody == TroopBody.Vehicle)
				{
					aControlTroop.aBodyRotationUpdater.SetDstRotation(
						lFollowDirectionY,
						aControlTroopStat.aTroopStatTable.Get(TroopStat.MaxRotationDelay),
						false); // 적을 공격 못한다면 기본 방향을 향함
					aControlTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);
				}

				//Debug.Log("is waiting - " + this);
				mIsResetFollowing = true; // 다음에 이동할 때에는 따라가기 초기화 필요
				aControlTroop.aMoveUpdater.HoldMove(true);
				return true; // 선행 부대가 자신을 지나쳐 가기를 기다립니다.
			}

			// 차량은 회전에 의해서 움직이지 않고 멈춰있는 경우가 많으므로 그렇게 회전하고 있는 선행 부대와의 거리가 너무 가까우면 다가가지 않고 기다립니다.
//			if (lFolloweeTroop.aTroopSpec.mBody == TroopBody.Vehicle)
			{
				if (lFollowVectorSqrMagnitude < 0)
					lFollowVectorSqrMagnitude = lFollowVector.SqrMagnitude(); // 초기화 안된 값 계산
				if (lFollowVectorSqrMagnitude <= BattleConfig.get.mSqrFollowWaitDistance) // 선행 부대와 아직 기준 거리 이상으로 멀어지지 않았다면
				{
					//Debug.Log("too close(is waiting) - " + this);
					mIsResetFollowing = true; // 다음에 이동할 때에는 따라가기 초기화 필요
					aControlTroop.aMoveUpdater.HoldMove(true);
					return true; // 너무 가까워서 기다립니다.
				}
			}

			// 따라가는 정보를 갱신합니다.
			if (mIsResetFollowing)
			{
				mIsResetFollowing = false;

				mPathUpdateTimer = 0; // 다음 경로 재갱신 필수

				int lFollowVectorY = lFollowVector.RoughDirectionY();
				int lDstCoordVectorY = lDstCoordVector.RoughDirectionY();
				int lMoveVectorY = MathUtil.RoughLerpDegree(lFollowVectorY, lDstCoordVectorY, 50, 100);
				Coord2 lMoveVector = Coord2.RoughDirectionVector(lMoveVectorY, cFollowMovePathUpdateDelay * aControlTroopStat.aAdvanceSpeed / 1000);
				mPathEndCoord = aControlTroop.aCoord + lMoveVector; // 선행 부대를 향한 방향과 목표 지점을 향한 방향의 중간 방향으로 이동
			}
			else
			{
				mPathEndCoord = lFolloweeTroop.aCoord; // 선행 부대의 뒤를 따라가기 위해 목표 좌표를 계속 갱신
			}
		}
		if (!_UpdatePathMove(pUpdateDelta))
			return false;

		// 목표 좌표에 이미 가까워진 상태라면 다음 갱신에서는 바로 이동합니다.
		int lDstCoordVectorSqrMagnitude = lDstCoordVector.SqrMagnitude();
		if (lDstCoordVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStopDistance)
		{
			//Debug.Log("lIsStopFollow = true - if (lDstCoordVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStopLimit)");
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceDstCoord_FinalFormationMove(), aControlTroop.aMoveUpdater.aExtraUpdateDelta);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceTargetTroop()
	{
		BattleTroop lTargetTroop = mStartParam.mTargetTroopLink.Get();

		// 타겟 부대가 사라졌다면 탐색으로 돌아갑니다.
		if (lTargetTroop == null)
		{
			//Debug.Log("StartSearchControl() in _OnStart_AdvanceTargetTroop() - " + this);
			aControlTroop.aDefaultDirectionY = aControlTroop.aDirectionY; // 타겟 부대 이동이 끝나면 현재 방향이 기본이 됨
			return _OnStart_NoMove();
		}

		// 탐지가 안된 적에 대한 접근이라면 조우 대응을 준비합니다.
		mIsEncounterTargetTroopAdvance = !lTargetTroop.aIsDetecteds[aControlTroop.aTeamIdx];

		// 타겟 부대를 공격할 가장 적절한 거리를 찾습니다.
		mBestTargetTroopAttackWeaponData = _GetBestTargetTroopAttackWeapon(lTargetTroop);
		mSqrTargetTroopAttackBestWeaponRange = mBestTargetTroopAttackWeaponData.aSqrMaxAttackRange;

		// 공격 이동 상태를 활성화합니다.
		aControlTroop.aControlState.mIsAttackMoving = true;

		// 목표 좌표로의 이동을 지정합니다.
		Coord2 lTargetTroopVector = lTargetTroop.aCoord - aControlTroop.aCoord;
		int lTargetTroopVectorSqrMagnitude = lTargetTroopVector.SqrMagnitude();
		if (lTargetTroopVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStartDistance) // 타겟 부대가 근처라면
		{
			return _OnStart_AdvanceTargetTroop_AttackMove(lTargetTroop); // 바로 공격합니다.
		}
		else // 타겟 부대가 멀다면
		{
			BattleTroop lFolloweeTroop = _UpdateFolloweeTroop();
			if (lFolloweeTroop == null) // 따라갈 부대가 없다면
			{
				return _OnStart_AdvanceTargetTroop_FrontMove(lTargetTroop); // 앞장섭니다.
				//Debug.Log("lIsLead=" + lIsLead + " aControlTroop.aTroopSlotIdx=" + aControlTroop.aTroopSlotIdx + " lTargetTroopVectorSqrMagnitude=" + lTargetTroopVectorSqrMagnitude);
			}
			else // 선행 부대가 있다면
			{
				return _OnStart_AdvanceTargetTroop_FollowMove(lTargetTroop); // 따라갑니다.
				//Debug.Log("lIsLead=" + lIsLead + " aControlTroop.aTroopSlotIdx=" + aControlTroop.aTroopSlotIdx + " lTargetTroopVectorSqrMagnitude=" + lTargetTroopVectorSqrMagnitude);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceTargetTroop_AttackMove(BattleTroop pTargetTroop)
	{
		// 타겟 부대를 공격할 수 있는 곳으로의 이동을 지정합니다.
		mLastTargetTroopCoord = pTargetTroop.aCoord;
		_SetTargetTroopAttackMove();

		aControlTroop.aControlState.mIsInFormation = true;

		return _OnFrameUpdate_AdvanceTargetTroop_AttackMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceTargetTroop_AttackMove(int pUpdateDelta)
	{
		BattleTroop lTargetTroop = mStartParam.mTargetTroopLink.Get();

		if (!_CheckTargetTroopAttack(lTargetTroop))
			return false;

		// 타겟 부대가 일정 거리 이상 움직이면 접근 좌표를 수정합니다.
		Coord2 lTargetTroopMoveVector = lTargetTroop.aCoord - mLastTargetTroopCoord;
		if (lTargetTroopMoveVector.SqrMagnitude() > BattleConfig.get.mSqrDetectionUpdateDegree)
		{
			mLastTargetTroopCoord = lTargetTroop.aCoord;
			_SetTargetTroopAttackMove();
		}

		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;
		if (!aControlTroop.aMoveUpdater.aIsMoving) // 이동이 멈췄다면
		{
			//Debug.Log("StartSearchControl() in _OnFrameUpdate_AdvanceTargetTroop() - " + this);
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 진군을 안한다면 탐색으로 전환
			return false;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceTargetTroop_FrontMove(BattleTroop pTargetTroop)
	{
		_SetPathMove(pTargetTroop.aCoord, cLeadMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.FrontMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);

		return _OnFrameUpdate_AdvanceTargetTroop_FrontMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceTargetTroop_FrontMove(int pUpdateDelta)
	{
		BattleTroop lTargetTroop = mStartParam.mTargetTroopLink.Get();

		if (!_CheckTargetTroopAttack(lTargetTroop))
			return false;

		// 타겟 부대가 일정 거리 이상 움직이면 접근 좌표를 수정합니다.
		Coord2 lTargetTroopMoveVector = lTargetTroop.aCoord - mLastTargetTroopCoord;
		if (lTargetTroopMoveVector.SqrMagnitude() > BattleConfig.get.mSqrDetectionUpdateDegree)
		{
			mLastTargetTroopCoord = lTargetTroop.aCoord;
			_SetPathMove(mLastTargetTroopCoord, cLeadMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.FrontMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);
		}

		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;

		// 타겟 부대에 충분히 가까워졌다면 공격 이동을 할지를 검사합니다.
		Coord2 lTargetTroopVector = mLastTargetTroopCoord - aControlTroop.aCoord;
		int lTargetTroopVectorSqrMagnitude = lTargetTroopVector.SqrMagnitude();
		if ((lTargetTroopVectorSqrMagnitude <= BattleConfig.get.mSqrFormationStartDistance) || // 포메이션 좌표가 근처거나
			!aControlTroop.aMoveUpdater.aIsMoving) // 이동이 멈췄다면
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceTargetTroop_AttackMove(lTargetTroop), aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 포메이션 좌표로 이동합니다.

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_AdvanceTargetTroop_FollowMove(BattleTroop pTargetTroop)
	{
		BattleTroopMoveUpdater.MoveType lPathMoveType
			= aControlTroopStat.aIsFollowAdvance
			? BattleTroopMoveUpdater.MoveType.FollowAdvanceMove : BattleTroopMoveUpdater.MoveType.FollowFormationMove;
		_SetPathMove(pTargetTroop.aCoord, cFollowMovePathUpdateDelay, lPathMoveType, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent); // 실제 목표 좌표는 lFolloweeTroop.aCoord나 포메이션 좌표로 계속 갱신됨

		return _OnFrameUpdate_AdvanceTargetTroop_FollowMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_AdvanceTargetTroop_FollowMove(int pUpdateDelta)
	{
		BattleTroop lTargetTroop = mStartParam.mTargetTroopLink.Get();

		if (!_CheckTargetTroopAttack(lTargetTroop))
			return false;

		// 선행 부대를 얻습니다.
		BattleTroop lFolloweeTroop = mFolloweeTroopLink.Get();

		if ((lFolloweeTroop != null) && // 따라가는 부대가
			(lFolloweeTroop.aStat.aMoraleState == TroopMoraleState.PinDown)) // 핀 다운이 되면
			lFolloweeTroop = _UpdateFolloweeTroop(); // 따라갈 부대를 교체합니다.

		if ((lFolloweeTroop == null) || // 선행 부대가 사라졌거나
			lFolloweeTroop.aControlState.mIsInFormation) // 선행 부대가 포메이션에 들어갔다면
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceTargetTroop_FrontMove(lTargetTroop), pUpdateDelta); // 앞장섭니다.

		// 이동을 갱신합니다.
		if (aControlTroopStat.aIsFollowAdvance)
			mPathEndCoord = lFolloweeTroop.aCoord; // 선행 부대의 뒤를 따라가기 위해 목표 좌표를 계속 갱신
		if (!_UpdatePathMove(pUpdateDelta))
			return false;

		// 타겟 부대에 충분히 가까워졌다면 공격 이동을 할지를 검사합니다.
		Coord2 lTargetTroopVector = lTargetTroop.aCoord - aControlTroop.aCoord;
		int lTargetTroopVectorSqrMagnitude = lTargetTroopVector.SqrMagnitude();
		if (lTargetTroopVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStopDistance)
		{
			//Debug.Log("lIsStopFollow = true - if (lTargetTroopVectorSqrMagnitude <= BattleConfig.get.mSqrFollowStopLimit)");
			return _SetOnFrameUpdateDelegate(_OnStart_AdvanceTargetTroop_AttackMove(lTargetTroop), aControlTroop.aMoveUpdater.aExtraUpdateDelta);
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_SkillMove()
	{
		// 스킬 좌표로의 이동을 지정합니다.
		_SetPathMove(mStartParam.mSkillCoord, 0, mStartParam.mSkillMoveType, PathMoveDirection.ForwardAndUrgentBackward, PathMoveUrgent.Urgent);

		return _OnFrameUpdate_SkillMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_SkillMove(int pUpdateDelta)
	{
		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;
		if (!aControlTroop.aMoveUpdater.aIsMoving) // 이동이 멈췄다면
		{
			int lExtraUpdateDelta = aControlTroop.aMoveUpdater.aExtraUpdateDelta;
			switch (mStartParam.mSkillMoveType)
			{
			case BattleTroopMoveUpdater.MoveType.FinalFormationMove:
				aControlTroop.aSkillProcess.OnSkillFormationAdvanceEnd(aControlTroop.aMoveUpdater.aExtraUpdateDelta, out lExtraUpdateDelta);
				break;
			case BattleTroopMoveUpdater.MoveType.PositioningMove:
				aControlTroop.aSkillProcess.OnSkillPositioningAdvanceEnd(mElapsedTime, out lExtraUpdateDelta);
				break;
			}

			// 위의 스킬 처리에서 별다른 컨트롤이 예약되지 않았다면 기본 처리를 합니다.
			if (aController.aReservedControl == null)
			{
				//Debug.Log("StartSearchControl() in _OnFrameUpdate_SkillMove() - " + this);
				aController.StartSearchControl(
					BattleTroopSearchControl.RetryOption.NoRetry,
					BattleTroopSearchControl.MoveOption.HoldPosition,
					lExtraUpdateDelta); // 스킬 처리를 안한다면 탐색으로 전환
			}
			return false;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate 초기화
	private OnFrameUpdateDelegate _OnStart_ReturnMove()
	{
		aControlTroop.aIsFallBehind = true; // 낙오 상태 시작

		// 지휘관으로의 이동을 지정합니다.
		mLastCommanderCoord = aControlTroopCommander.aCoord;
		_SetPathMove(mLastCommanderCoord, cReturnMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.ReturnMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);

		return _OnFrameUpdate_ReturnMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnFrameUpdateDelegate
	private bool _OnFrameUpdate_ReturnMove(int pUpdateDelta)
	{
		// 이동 좌표를 갱신합니다.
		Coord2 lCommanderMoveVector = aControlTroopCommander.aCoord - mLastCommanderCoord;
		if (lCommanderMoveVector.SqrMagnitude() > BattleConfig.get.mSqrDetectionUpdateDegree)
		{
			// 지휘관으로의 이동을 다시 지정합니다.
			mLastCommanderCoord = aControlTroopCommander.aCoord;
			_SetPathMove(mLastCommanderCoord, cReturnMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.ReturnMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);
		}

		// 이동을 갱신합니다.
		if (!_UpdatePathMove(pUpdateDelta))
			return false;

		// 복귀 완료를 검사합니다.
		Coord2 lCenterVector = mLastCommanderCoord - aControlTroop.aCoord;
		if (lCenterVector.SqrMagnitude() <= aControlTroopCommander.aTroopSet.mSqrGroupingRange)
		{
			aControlTroop.aIsFallBehind = false; // 낙오 상태 해제
			aControlTroop.aCommand.StartLastCommand(aControlTroop.aMoveUpdater.aExtraUpdateDelta); // 지휘관의 컨트롤로 전환
			return false;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 선행 부대 중에서 따라갈 부대를 갱신합니다.
	private BattleTroop _UpdateFolloweeTroop()
	{
		mFolloweeTroopLink.Reset();

		BattleTroop lFolloweeTroop = aControlTroop.aPrecedingTroop;
		while (lFolloweeTroop != null)
		{
			if ((lFolloweeTroop.aStat.aMoraleState != TroopMoraleState.PinDown) && // 핀 다운 상태가 아니고
				(lFolloweeTroop.aMoveUpdater.aMoveType != BattleTroopMoveUpdater.MoveType.ReturnMove)) // 복귀 이동 중이 아니라면
			{
				mFolloweeTroopLink.Set(lFolloweeTroop); // 이 부대를 선행 부대로 결정
				return lFolloweeTroop;
			}
			else
				lFolloweeTroop = lFolloweeTroop.aPrecedingTroop; // 더 선행 부대에서 찾음
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 경로 이동을 지정합니다.
	private void _SetPathMove(Coord2 pPathEndCoord, int pPathUpdateDelay, BattleTroopMoveUpdater.MoveType pPathMoveType, PathMoveDirection pPathMoveDirection, PathMoveUrgent pPathMoveUrgent)
	{
		mPathEndCoord = pPathEndCoord;
		mPathUpdateDelay = pPathUpdateDelay;
		mPathUpdateTimer = 0;
		mPathMoveType = pPathMoveType;
		mPathMoveDirection = pPathMoveDirection;
		mPathMoveUrgent = pPathMoveUrgent;

		// 상태 표시를 복구 합니다.
		aControlTroop.aStateText = cAdvanceMoveText;

		// 스킬 이동처럼 갱신이 없는 경우 이동을 여기서 한 번만 초기화합니다.
		if (pPathUpdateDelay <= 0)
		{
			aControlTroop.aMoveUpdater.SetMove(
				mPathEndCoord,
				mPathMoveType,
				0, // pSqrApproachRange
				mMoveUpdaterRotationType,
				true);

			mMoveUpdaterRotationType = BattleTroopMoveUpdater.RotationType.MovingRotation; // 한 번 이동을 시작한 이후로는 이동하며 회전
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 경로 이동을 갱신합니다.
	private bool _UpdatePathMove(int pUpdateDelta)
	{
		// 일정 주기로 경로를 갱신합니다.
		if (mPathUpdateDelay > 0)
		{
			// 이동 불가 상태가 되면 탐색으로 돌아갑니다.
			if (aControlTroopStat.aAdvanceSpeed <= 0)
			{
				aController.StartSearchControl(
					BattleTroopSearchControl.RetryOption.NoRetry,
					BattleTroopSearchControl.MoveOption.HoldPosition,
					0); // 탐색으로 전환
				return false; // 이동 불가
			}

			// 경로를 주기적으로 갱신합니다.
			mPathUpdateTimer -= pUpdateDelta;
			if (mPathUpdateTimer <= 0) // 경로 갱신 시간이 지났다면
			{
				bool lIsMovableBackward
					= (mPathMoveDirection == PathMoveDirection.ForwardAndUrgentBackward)
					&& (aControlTroop.aTroopSpec.mType == TroopType.Armored)
					&& (mPathMoveUrgent == PathMoveUrgent.Urgent);

				if (mPathMoveType == BattleTroopMoveUpdater.MoveType.FollowFormationMove)
					mPathEndCoord = aControlTroopCommander.GetTroopFollowFormationMoveCoord(aControlTroop.aTroopSlotIdx); // 포메이션 유지를 위해 목표 좌표를 계속 갱신

				aControlTroop.aMoveUpdater.SetMove(
					mPathEndCoord,
					mPathMoveType,
					0, // pSqrApproachRange
					mMoveUpdaterRotationType,
					lIsMovableBackward);

				mMoveUpdaterRotationType = BattleTroopMoveUpdater.RotationType.MovingRotation; // 한 번 이동을 시작한 이후로는 이동하며 회전

				mPathUpdateTimer += mPathUpdateDelay; // 타이머 오차 손실을 반영(+=)
			}
		}

		// 이동 중이라면 현재 이동을 갱신합니다.
		aControlTroop.aMoveUpdater.UpdateMove(pUpdateDelta);

		return true; // 이동 가능
	}
	//-------------------------------------------------------------------------------------------------------
	// 적 탐색을 갱신합니다.
	private void _UpdateTargetSearch(int pUpdateDelta)
	{
		// 경로를 주기적으로 갱신합니다.
		mEnemySearchTimer -= pUpdateDelta;
		if (mEnemySearchTimer <= 0) // 경로 갱신 시간이 지났다면
		{
			IBattleTarget lTarget = aControlTroop.SearchAnyTarget();
			BattleTroop lEnemyTroop = (lTarget != null) ? lTarget.aTargetTroop : null;
			if ((lEnemyTroop != null) &&
				(lEnemyTroop.aTeamIdx != aControlTroop.aTeamIdx))
				aControlTroopCommander.ReportNewEnemy(aControlTroop.aTroopSlotIdx, lEnemyTroop);

			mEnemySearchTimer += cEnemySearchDelay; // 타이머 오차 손실을 반영(+=)
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 공격할 수 있는 곳으로의 이동을 갱신합니다.
	private void _SetTargetTroopAttackMove()
	{
		Coord2 lTargetVector = mLastTargetTroopCoord - aControlTroop.aCoord;
		int lSqrTargetDistance = lTargetVector.SqrMagnitude();
		mIsTargetDistantiateMove = (lSqrTargetDistance < aControlTroopStat.aSqrMinAttackRange); // 적 부대가 최소 사정거리보다 가까이 있다면 회피 이동

		if (!mIsTargetDistantiateMove) // 적 부대가 최소 사정거리보다 가까이 있지 않다면
		{
			// 적 부대를 향해 이동합니다.
			_SetPathMove(mLastTargetTroopCoord, cAttackMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.AttackMove, PathMoveDirection.ForwardOnly, PathMoveUrgent.NoUrgent);
		}
		else // 적 부대가 최소 사정거리보다 가까이 있다면
		{
			// 적 부대에서 멀어지는 이동을 지정합니다.
			Coord2 lTargetDistantiateCoord = _GetTargetDistantiateCoord(lTargetVector);
			_SetPathMove(lTargetDistantiateCoord, cAttackMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.AttackMove, PathMoveDirection.ForwardAndUrgentBackward, PathMoveUrgent.Urgent);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대에서 공격할 수 있는 거리로 멀어지는 좌표를 얻습니다.
	private Coord2 _GetTargetDistantiateCoord(Coord2 lTargetVector)
	{
		int lDistantiateDistance = (aControlTroopStat.aMinAttackRange + aControlTroopStat.aMaxAttackRange) / 2; // 충분히 멀어지도록 중간 사정거리까지
		Coord2 lTargetDistantiateVector = Coord2.RoughNormalize(-lTargetVector, lDistantiateDistance);
		Coord2 lTargetDistantiateCoord = aBattleGrid.ClampCoord(mLastTargetTroopCoord + lTargetDistantiateVector);

		return lTargetDistantiateCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대에 대한 공격 변화 처리를 합니다.
	private bool _CheckTargetTroopAttack(BattleTroop pTargetTroop)
	{
		// 타겟 부대가 사라졌다면 탐색으로 돌아갑니다.
		if (pTargetTroop == null)
		{
			//Debug.Log("StartSearchControl() in _OnFrameUpdate_AdvanceTargetTroop() when pTargetTroop is null - " + this);
			aControlTroop.aDefaultDirectionY = aControlTroop.aDirectionY; // 타겟 부대 이동이 끝나면 현재 방향이 기본이 됨
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				0); // 진군을 안한다면 탐색으로 전환
			return false;
		}

		// 탐지된 타겟 부대가 공격 가능한 거리에 들어오면 공격합니다.
		if (pTargetTroop.aIsDetecteds[aControlTroop.aTeamIdx])
		{
			// 먼저 타겟과의 거리를 구합니다.
			Coord2 lTargetVector = pTargetTroop.aCoord - aControlTroop.aCoord;
			int lSqrTargetDistance = lTargetVector.SqrMagnitude();

			// 조우 대응 정보를 얻습니다.
			int lSqrMinApproachDistance;
			if (mIsEncounterTargetTroopAdvance &&
				(aControlTroopCommander.aExplicitTargetTroop == pTargetTroop))
			{
				lSqrMinApproachDistance = aControlTroopCommander.aSqrExplicitTargetTroopEncounterDistance; // 지휘관 휘하의 다른 부대들과 접근 거리를 맞춤
				if (lSqrMinApproachDistance == int.MaxValue)
				{
					aControlTroopCommander.UpdateExplicitTargetTroopEncounterDistance();
					lSqrMinApproachDistance = aControlTroopCommander.aSqrExplicitTargetTroopEncounterDistance;
				}
			}
			else
				lSqrMinApproachDistance = int.MaxValue;

			// 거리에 따른 처리를 합니다.
			if ((lSqrTargetDistance <= mSqrTargetTroopAttackBestWeaponRange) &&
				(lSqrTargetDistance >= aControlTroopStat.aSqrMinAttackRange) &&
				(lSqrTargetDistance <= lSqrMinApproachDistance)) // 공격 가능한 거리에 들어왔을 때
			{
				if (!aControlTroopStat.aTroopStatTable.aIsAttackDisabled) // 공격 가능하다면
				{
// 					// 그룹 타게팅 무기를 가졌다면 타겟 부대를 향한 그룹 타게팅 정보를 얻습니다. -> 이렇게 안 할 예정
// 					if (mBestTargetTroopAttackControl.aTargetingInfo.aIsGroupTargeting)
// 						aControlTroop.SearchTarget(
// 							pTargetTroop,
// 							mBestTargetTroopAttackControl.aTargetingInfo,
// 							true);

					if (aController.aWeaponAttackControls[mBestTargetTroopAttackWeaponData.mWeaponIdx] == aController.aBodyAttackControl) // 몸통 공격을 기준으로 할 경우에만 상태 전환
					{
						aControlTroop.aDefaultDirectionY = lTargetVector.RoughDirectionY(); // 타겟 부대를 향한 방향이 기본이 됨
						aController.StartTargetTroopAttackControl(pTargetTroop, false, 0);
						return false; // 공격을 시작
					}
					else
					{
						aController.StartSearchControl(
							BattleTroopSearchControl.RetryOption.NoRetry,
							BattleTroopSearchControl.MoveOption.HoldPosition,
							aControlTroop.aMoveUpdater.aExtraUpdateDelta);
						return false; // 터렛이 공격하도록 하고 탐색으로 전환
					}
				}
				else if (!aControlTroop.aTroopSpec.mIsCaptureType) // (공격 가능한 적이 아니고) 점령 타입이 아니라면 접근을 멈추고
				{
					aControlTroop.aDefaultDirectionY = aControlTroop.aDirectionY; // 타겟 부대 이동이 끝나면 현재 방향이 기본이 됨
					aController.StartSearchControl(
						BattleTroopSearchControl.RetryOption.NoRetry,
						BattleTroopSearchControl.MoveOption.HoldPosition,
						aControlTroop.aMoveUpdater.aExtraUpdateDelta);
					return false; // 탐색으로 전환
				}
			}
			else
			{
				if ((lSqrTargetDistance < aControlTroopStat.aSqrMinAttackRange) && // 최소 사정거리보다 더 접근했는데
					!mIsTargetDistantiateMove) // 회피 이동이 아니라면
				{
					// 적 부대에서 멀어지는 이동을 지정합니다.
					mLastTargetTroopCoord = pTargetTroop.aCoord;
					mIsTargetDistantiateMove = true;
					Coord2 lTargetDistantiateCoord = _GetTargetDistantiateCoord(lTargetVector);
					_SetPathMove(lTargetDistantiateCoord, cAttackMovePathUpdateDelay, BattleTroopMoveUpdater.MoveType.AttackMove, PathMoveDirection.ForwardAndUrgentBackward, PathMoveUrgent.Urgent);
				}
			}
		}
		else if (mIsTargetDistantiateMove || // 탐지가 안되었는데 회피 이동중이었거나(안보이는 적으로부터 도망갈 수 없음)
			pTargetTroop.aIsHiding) // 탐지 안 된 이유가 은폐때문이라면
		{
			aControlTroop.aDefaultDirectionY = aControlTroop.aDirectionY; // 타겟 부대 이동이 끝나면 현재 방향이 기본이 됨
			aController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				aControlTroop.aMoveUpdater.aExtraUpdateDelta);
			return false; // 탐색으로 전환
		}

		return true; // 컨트롤 전환 없음
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 공격할 가장 효과적인 타게팅 정보를 얻습니다.
	private BattleTroopStat.WeaponData _GetBestTargetTroopAttackWeapon(BattleTroop pTargetTroop)
	{
		BattleTroopStat.WeaponData lBestWeaponData = aControlTroopStat.aWeaponDatas[0];
		int lBestWeaponExpectedMilliDamage = BattleTroopTargetingInfo.GetExpectedMilliDamage(
			lBestWeaponData,
			pTargetTroop,
			aControlTroopStat.aSqrBestWeaponCheckRange);

		for (int iWeapon = 1; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			BattleTroopStat.WeaponData lWeaponData = aControlTroopStat.aWeaponDatas[iWeapon];
			if (lWeaponData == null)
				continue;

			int lCompareWeaponExpectedMilliDamage = BattleTroopTargetingInfo.GetExpectedMilliDamage(
				aControlTroopStat.aWeaponDatas[iWeapon],
				pTargetTroop,
				aControlTroopStat.aSqrBestWeaponCheckRange);
			if (lBestWeaponExpectedMilliDamage < lCompareWeaponExpectedMilliDamage)
			{
				lBestWeaponExpectedMilliDamage = lCompareWeaponExpectedMilliDamage;
				lBestWeaponData = lWeaponData;
			}
		}

		return lBestWeaponData;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;

	private OnFrameUpdateDelegate mOnFrameUpdateDelegate;
	private int mSetOnFrameUpdateCallCounter;

	private bool mIsEncounterTargetTroopAdvance;
	private BattleTroopStat.WeaponData mBestTargetTroopAttackWeaponData;
	private int mSqrTargetTroopAttackBestWeaponRange;
	private bool mIsTargetDistantiateMove;
	private Coord2 mLastTargetTroopCoord;
	private Coord2 mLastCommanderCoord;

//	private Coord2 mLastPrecedingTroopCoord;
	private BattleTroopMoveUpdater.RotationType mMoveUpdaterRotationType;
	private AdvanceType mAdvanceType;
	private int mElapsedTime;
	private Coord2 mPathEndCoord;
	private int mPathUpdateDelay;
	private int mPathUpdateTimer;
	private bool mIsResetFollowing;
	private BattleTroopMoveUpdater.MoveType mPathMoveType;
	private PathMoveDirection mPathMoveDirection;
	private PathMoveUrgent mPathMoveUrgent;
	private int mEnemySearchTimer;

	private GuidLink<BattleTroop> mFolloweeTroopLink;
}
