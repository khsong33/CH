using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class BattleTroopControl
{
	public delegate OnFrameUpdateDelegate OnStartDelegate();
	public delegate bool OnFrameUpdateDelegate(int pUpdateDelta);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 이름
	public abstract String aStateName { get; }

	// [상호 작용] 치료 가능 여부
	public abstract bool aIsRecoveryAvailable { get; }

	// 액션 가능 여부
	public abstract bool aIsActionAvailable { get; }

	//-------------------------------------------------------------------------------------------------------
	// 컨트롤러
	public BattleTroopController aController
	{
		get { return mController; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대 스펙
	public TroopSpec aControlTroopSpec
	{
		get { return mControlTroopSpec; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대 스탯
	public BattleTroopStat aControlTroopStat
	{
		get { return mControlTroopStat; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대 지휘관
	public BattleCommander aControlTroopCommander
	{
		get { return mControlTroopCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 여부
	public bool aIsStarted
	{
		get { return mIsStarted; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 델타
	public int aStartDelta
	{
		get { return mStartDelta; }
		set { mStartDelta = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 추과 갱신 델타
	public int aExtraUpdateDelta
	{
		get { return mExtraUpdateDelta; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopControl()
	{
		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public virtual bool Start()
	{
		if (mIsStarted)
		{
			Debug.Log("Start() mIsStarted=" + mIsStarted + " - " + this);
			return true;
		}
	
		if (aControlTroop.aIsLogControl)
			Debug.Log(this);

		mIsStarted = true;
		mExtraUpdateDelta = 0;
			
		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 종료 함수
	public virtual bool Stop(int pStopTime, int pStopDelta)
	{
		if (!OnStopUpdate(pStopTime, pStopDelta, out mExtraUpdateDelta))
			return false;

		mIsStarted = false;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수
	public abstract bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta);

	//-------------------------------------------------------------------------------------------------------
	// 갱신 종료시 호출되는 함수
	public virtual bool OnStopUpdate(int pStopTime, int pStopDelta, out int oExtraUpdateDelta)
	{
		oExtraUpdateDelta = pStopDelta;
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이름 반환
	public override string ToString()
	{
		return String.Format("{0}({1})", aStateName, mControlTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤러를 지정합니다.
	public void SetController(BattleTroopController pController)
	{
		mController = pController;
		mControlTroop = pController.aControlTroop;
		mControlTroopSpec = mControlTroop.aTroopSpec;
		mControlTroopStat = mControlTroop.aStat;
		mControlTroopCommander = mControlTroop.aCommander;
		mBattleGrid = mControlTroop.aBattleGrid;
	}
	//-------------------------------------------------------------------------------------------------------
	// 밀리 초를 초로 표시한 문자열을 얻습니다.
	public String GetSecText(int pMiliSeconds)
	{
		return ((pMiliSeconds - 1) / 1000 + 1).ToString() + "s";
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopController mController;
	private BattleTroop mControlTroop;
	private TroopSpec mControlTroopSpec;
	private BattleTroopStat mControlTroopStat;
	private BattleCommander mControlTroopCommander;
	private BattleGrid mBattleGrid;
	private bool mIsStarted;
	private int mStartDelta;
	private int mExtraUpdateDelta;
}
