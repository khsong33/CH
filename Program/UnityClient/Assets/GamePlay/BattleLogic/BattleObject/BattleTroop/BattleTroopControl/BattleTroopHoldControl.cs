using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopHoldControl : BattleTroopControl
{
	public class StartParam
	{
		public void Set()
		{
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Hold"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return true; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return true; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopHoldControl()
	{
		mStartParam = new StartParam();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		SetController(pController);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (!base.Start())
			return false;
	
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
// 	public override bool Stop()
// 	{
// 		if (!base.Stop())
// 			return false;
// 
// 		return true;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		return _OnFrameUpdate(pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수
	private bool _OnFrameUpdate(int pUpdateDelta)
	{
		// 아무런 동작을 하지 않고 제자리에 있습니다.
		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;
}
