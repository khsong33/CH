using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopSearchControl : BattleTroopControl
{
	public enum RetryOption
	{
		RetrySomeTimes,
		NoRetry,
	}
	public enum MoveOption
	{
		AdvanceWhenNoEnemy,
		HoldPosition,
	}

	public class StartParam
	{
		internal RetryOption mRetryOption;
		internal MoveOption mMoveOption;

		public void Set(RetryOption pRetryOption, MoveOption pMoveOption)
		{
			mRetryOption = pRetryOption;
			mMoveOption = pMoveOption;
		}
	}

	private const int cSearchDelay = 1000;
	private const int cSearchRetryCount = 3;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Search"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return true; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return true; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopSearchControl()
	{
		mStartParam = new StartParam();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		SetController(pController);

		mLastSearchFrameIdx = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (!base.Start())
			return false;

		if (mLastSearchFrameIdx != aControlTroop.aFrameUpdater.aFrameIdx)
		{
			mLastSearchFrameIdx = aControlTroop.aFrameUpdater.aFrameIdx;
			mSearchTimer = 0;
		}
		else
		{
			//Debug.LogWarning("recursive call - " + this);
			mSearchTimer = cSearchDelay; // 한 프레임에서는 한 번의 탐색만 가능하게(이번 프레임에서 찾은 적을 공격하기 위해 공격 컨트롤을 시작했는데 그 내부에서 공격을 처리하지 못하고 다시 탐색으로 돌아와 무한 호출 되는 상황 방지 목적)
		}
		mTarget = null;

		aControlTroop.aBodyRotationUpdater.SetDstRotation(
			aControlTroop.aDefaultDirectionY,
			aControlTroopStat.aTroopStatTable.Get(TroopStat.MaxRotationDelay),
			false); // 적을 공격 못한다면 기본 방향을 향함

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
// 	public override bool Stop()
// 	{
// 		if (!base.Stop())
// 			return false;
// 
// 		return true;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		// 핀 다운 상태에서는 아무런 처리를 하지 않습니다.
		if (aControlTroopStat.aMoraleState == TroopMoraleState.PinDown)
			return true;

		return _OnFrameUpdate(pUpdateDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수
	private bool _OnFrameUpdate(int pUpdateDelta)
	{
		if (!aControlTroopStat.aTroopStatTable.aIsAttackDisabled &&
			aControlTroop.aInteraction.aIsAttackable)
		{
			// 일정 주기로 탐색을 반복합니다.
			mSearchTimer -= pUpdateDelta;
			if (mSearchTimer <= 0)
			{
				// 타겟을 찾습니다.
				if ((aControlTroop.aTroopSpec.mBody != TroopBody.Soldier) || // 보병이 아니라면 치료중에도 전환 가능
					aControlTroop.aInteraction.aIsAttackable)
				{
					mTarget = aControlTroop.SearchTarget(
						null,
						aController.aBodyAttackControl.aTargetingInfo,
						aControlTroop.aIsPrimaryAttackBody);
					if (mTarget != null) // 타겟을 발견하면
					{
						// 공격을 시작합니다.
						//Debug.Log("mTargetTroop=" + mTargetTroop);
						if (mTarget.aTargetTroop != null)
							aController.StartTargetTroopAttackControl(mTarget.aTargetTroop, false, -mSearchTimer);
						else
							aController.StartTargetCoordAttackControl(mTarget.aTargetCoord, false, -mSearchTimer);
						return false;
					}
				}

				// 다시 탐색 딜레이만큼 기다립니다.
				mSearchTimer += cSearchDelay; // 타이머 오차 손실을 반영(+=)
			}
		}

		// 지휘관 방향으로 회전합니다.
		if (aControlTroop.aBodyRotationUpdater.aIsRotating &&
			!aControlTroop.aInteraction.aIsValid) // 상호 작용중에는 회전 안함
			aControlTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);

		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;
	private int mSearchTimer;
	private int mLastSearchFrameIdx;
	private IBattleTarget mTarget;
}
