using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopSpawnControl : BattleTroopControl
{
	public class StartParam
	{
		public void Set()
		{
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override String aStateName
	{
		get { return "Spawn"; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsRecoveryAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 속성
	public override bool aIsActionAvailable
	{
		get { return false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 파라미터
	public StartParam aStartParam
	{
		get { return mStartParam; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopSpawnControl()
	{
		mStartParam = new StartParam();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		SetController(pController);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 함수
	public override bool Start()
	{
		if (!base.Start())
			return false;

		switch (aControlTroopCommander.aCommanderSpec.mSpawnType)
		{
		case CommanderSpawnType.Aircraft:
			mSpawnTimer = BattleConfig.get.mAircraftSpawnDelay;
			break;
		default:
			mSpawnTimer = 0;
			break;
		}

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
// 	public override bool Stop()
// 	{
// 		if (!base.Stop())
// 			return false;
// 
// 		return true;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnFrameUpdate(int pUpdateTime, int pUpdateDelta)
	{
		return _OnFrameUpdate(pUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleTroopControl 메서드
	public override bool OnStopUpdate(int pStopTime, int pStopDelta, out int oExtraUpdateDelta)
	{
		// 스폰 시간이 끝나기를 기다립니다.
		if (mSpawnTimer > 0)
		{
			mSpawnTimer -= pStopDelta;
			if (mSpawnTimer > 0)
			{
				oExtraUpdateDelta = 0;
				return false; // 스폰 중
			}
			oExtraUpdateDelta = -mSpawnTimer; // 초과하고 남은 시간
		}
		else
		{
			oExtraUpdateDelta = pStopDelta; // 스폰 끝나는데 시간을 사용하지 않았으므로 갱신 시간 그대로 반환
		}

		// 타겟 탐색을 초기 갱신합니다.
		IBattleTarget lTarget = aControlTroop.SearchAnyTarget();
		BattleTroop lEnemyTroop = (lTarget != null) ? lTarget.aTargetTroop : null;
		if ((lEnemyTroop != null) &&
			(lEnemyTroop.aTeamIdx != aControlTroop.aTeamIdx))
			aControlTroopCommander.ReportNewEnemy(aControlTroop.aTroopSlotIdx, lEnemyTroop);

		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신 함수
	private bool _OnFrameUpdate(int pUpdateDelta)
	{
		// 스폰 시간이 끝나기를 기다립니다.
		mSpawnTimer -= pUpdateDelta;
		if (mSpawnTimer > 0)
			return true;

		// 탐지를 시작합니다.
		aController.StartSearchControl(
			BattleTroopSearchControl.RetryOption.NoRetry,
			BattleTroopSearchControl.MoveOption.AdvanceWhenNoEnemy,
			-mSpawnTimer); // 멈춤 탐색
		return false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private StartParam mStartParam;

	private int mSpawnTimer;
}
