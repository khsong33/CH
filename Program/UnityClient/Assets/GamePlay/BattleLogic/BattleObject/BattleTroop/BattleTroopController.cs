using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopController
{
	internal const int cControlStopBigTime = 1000000; // 시간 경과가 필요한 컨트롤을 완전히 종료시킬 충분히 큰 시간
	internal const int cControlStartCounterLogLimit = 5;
	internal const int cControlStartCounterStopLimit = 100;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화된 컨트롤
	public BattleTroopControl aActiveControl
	{
		get { return mActiveControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 예약된 컨트롤
	public BattleTroopControl aReservedControl
	{
		get { return mReservedControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 예약된 컨트롤을 즉시 시작해야 하는지 여부
	public bool aIsForcedReservedControl
	{
		get { return mIsForcedReservedControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주요 컨트롤 접근
	public BattleTroopAttackControl aBodyAttackControl
	{
		get { return mBodyAttackControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주요 컨트롤 접근
	public BattleTroopAttackControl aTurretAttackControl
	{
		get { return mTurretAttackControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주 공격 컨트롤 접근
	public BattleTroopAttackControl aMainAttackControl
	{
		get { return mMainAttackControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부 공격 컨트롤 접근
	public BattleTroopAttackControl aSubAttackControl
	{
		get { return mSubAttackControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기별 공격 컨트롤들
	public BattleTroopAttackControl[] aWeaponAttackControls
	{
		get { return mWeaponAttackControls; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표 공격 반복 회수
	public int aTargetCoordAttackRepeatCount
	{
		get { return mTargetCoordAttackRepeatCount; }
		set { mTargetCoordAttackRepeatCount = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 공격을 진행중인지 여부
	public bool aIsAttacking
	{
		get { return mIsAttacking; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 공격 중인 적 부대
	public BattleTroop aAttackingTargetTroop
	{
		get
		{
			if (mMainAttackControl.aIsAttacking)
				return mMainAttackControl.aTargetingInfo.aTargetTroop;
			else if ((mSubAttackControl != null) &&
				mSubAttackControl.aIsAttacking)
				return mSubAttackControl.aTargetingInfo.aTargetTroop;
			else
				return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopController()
	{
		mHoldControl = new BattleTroopHoldControl();
		mSpawnControl = new BattleTroopSpawnControl();
		mAdvanceControl = new BattleTroopAdvanceControl();
		mBarricadeControl = new BattleTroopBarricadeControl();
		mBodyAttackControl = new BattleTroopAttackControl();
		mTurretAttackControl = new BattleTroopAttackControl();
		mSearchControl = new BattleTroopSearchControl();
		mActionControl = new BattleTroopActionControl();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroop pControlTroop)
	{
		mControlTroop = pControlTroop;

		mHoldControl.Init(this);
		mSpawnControl.Init(this);
		mAdvanceControl.Init(this);
		mBarricadeControl.Init(this);
		mBodyAttackControl.Init(this, BattleTroopAttackControl.ControlWeapon.BodyWeapon);
		mTurretAttackControl.Init(this, BattleTroopAttackControl.ControlWeapon.TurretWeapon);

		UpdateWeaponControlInfo();

		mSearchControl.Init(this);
		mActionControl.Init(this);

		mActiveControl = null;
		mReservedControl = null;

		mTargetCoordAttackRepeatCount = 1;
		mIsAttacking = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 시작합니다.
	public void StartControl(BattleTroop.StartState pStartState)
	{
		switch (pStartState)
		{
		case BattleTroop.StartState.Spawn:
		case BattleTroop.StartState.Npc:
			{
				StartSpawnControl(0);
			}
			break;
		case BattleTroop.StartState.Observer:
		case BattleTroop.StartState.Hold:
			{
				StartHoldControl(0);
			}
			break;
		default:
			Debug.LogError("not implemented yet - " + this);
			break;
		}

		if (mTurretAttackControl.aTargetingInfo.aIsValid)
			mTurretAttackControl.Start();

		int oExtraUpdateTime;
		_StartReservedControl(0, out oExtraUpdateTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 중단합니다.
	public void StopControl()
	{
		if (aControlTroop.aSkillProcess.aIsStarted)
		{
			aControlTroop.aSkillProcess.StopProcess(true, cControlStopBigTime);
			if (aControlTroop.aSkillProcess.aIsStarted)
				Debug.LogError("aControlTroop.aSkillProcess.aIsStarted=" + aControlTroop.aSkillProcess.aIsStarted);
		}

		if (mActiveControl != null)
		{
			mActiveControl.Stop(mControlTroop.aFrameUpdater.aElapsedTime, cControlStopBigTime);
			if (mActiveControl.aIsStarted)
				Debug.LogError("mActiveControl.aIsStarted=" + mActiveControl.aIsStarted);
		}
		if (mTurretAttackControl.aIsStarted)
		{
			mTurretAttackControl.Stop(mControlTroop.aFrameUpdater.aElapsedTime, cControlStopBigTime);
			if (mTurretAttackControl.aIsStarted)
				Debug.LogError("mTurretAttackControl.aIsStarted=" + mTurretAttackControl.aIsStarted);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 재시작합니다.
	public void ResetControl()
	{
		if (mActiveControl != mHoldControl)
		{
			StopControl();

			StartSpawnControl(0);

			if (mTurretAttackControl.aTargetingInfo.aIsValid)
				mTurretAttackControl.Start();
		}
		else
		{
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 갱신합니다.
	public bool UpdateFrame(int pUpdateDelta)
	{
	#if UNITY_EDITOR
		if (mControlTroop.aBattleGrid.aDebugTroopGuid == mControlTroop.aGuid)
			mControlTroop.aBattleGrid.aDebugTroopGuid = mControlTroop.aGuid;
	#endif

		// 터렛 공격 컨트롤을 먼저 합니다.
		if (mTurretAttackControl.aIsStarted)
			mTurretAttackControl.OnFrameUpdate(mControlTroop.aFrameUpdater.aElapsedTime, pUpdateDelta);

		// 다른 곳에서 예약된 컨트롤을 시작합니다.
		if (mReservedControl != null)
		{
			int oExtraUpdateTime;
			_StartReservedControl(pUpdateDelta, out oExtraUpdateTime);
			pUpdateDelta = oExtraUpdateTime;
		}

		// 예약 컨트롤 처리가 끝났다면 컨트롤을 갱신합니다.
		if (mReservedControl == null)
		{
			if ((mActiveControl != null) &&
				(pUpdateDelta > 0))
			{
				if (!mActiveControl.OnFrameUpdate(mControlTroop.aFrameUpdater.aElapsedTime, pUpdateDelta)) // 컨트롤을 갱신
				{
					if (mReservedControl != null) // 이번 프레임 갱신에서 예약된 컨트롤을 시작합니다.
					{
						pUpdateDelta = mReservedControl.aStartDelta;
						mReservedControl.aStartDelta = 0;

						int oExtraUpdateTime;
						_StartReservedControl(pUpdateDelta, out oExtraUpdateTime);
						pUpdateDelta = oExtraUpdateTime;
					}
					else
					{
						Debug.LogError("no valid control after " + mActiveControl + " - " + this);
					}
				}
			}
		}

		// 컨트롤 중단을 처리합니다.
		if (mActiveControl == null)
			return false; // 컨트롤 없음

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 컨트롤 관련 정보를 갱신합니다.
	public void UpdateWeaponControlInfo()
	{
		mWeaponAttackControls = new BattleTroopAttackControl[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			if (mControlTroop.aStat.aWeaponDatas[iWeapon] != null)
				mWeaponAttackControls[iWeapon] = mControlTroop.aTroopSpec.mIsBodyWeapons[iWeapon] ? mBodyAttackControl : mTurretAttackControl;
			else
				mWeaponAttackControls[iWeapon] = null;
		mMainAttackControl = mWeaponAttackControls[TroopSpec.cPrimaryWeaponIdx];
		mSubAttackControl = mWeaponAttackControls[TroopSpec.cSecondaryWeaponIdx];
		if (mSubAttackControl == mMainAttackControl) // 보조 무기도 주 공격 컨트롤에 달려있다면
		{
			mSubAttackControl = mWeaponAttackControls[TroopSpec.cVeteranWeaponIdx]; // 베테랑 무기 컨트롤을 부 공격 컨트롤로
			if (mSubAttackControl == mMainAttackControl) // 베테알 무기도 주 공격 컨트롤에 달려있다면
				mSubAttackControl = null; // 부 공격 컨트롤은 없는 것으로 처리
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 홀드 컨트롤을 시작합니다.
	public void StartHoldControl(int pStartDelta)
	{
		mReservedControl = mHoldControl;
		mIsForcedReservedControl = false;

		mHoldControl.aStartParam.Set();
		mHoldControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스폰 컨트롤을 시작합니다.
	public void StartSpawnControl(int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		mReservedControl = mSpawnControl;
		mIsForcedReservedControl = false;

		mSpawnControl.aStartParam.Set();
		mSpawnControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 좌표 진군 컨트롤을 시작합니다.
	public void StartDstCoordAdvanceControl(Coord2 pDstCoord, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		if ((mActiveControl == mAdvanceControl) && // 이미 이동 컨트롤중이라면
			(mAdvanceControl.aAdvanceType == BattleTroopAdvanceControl.AdvanceType.DstCoord))
		{
			mReservedControl = null; // 기존 예약 컨트롤을 없애고
			mIsForcedReservedControl = false;

			mAdvanceControl.ResetDstCoord(pDstCoord, pStartDelta); // 좌표를 재지정
		}
		else // 아니라면
		{
			mReservedControl = mAdvanceControl; // 컨트롤을 예약
			mIsForcedReservedControl = false;

			mAdvanceControl.aStartParam.SetDstCoordAdvance(pDstCoord);
			mAdvanceControl.aStartDelta = pStartDelta;

			aControlTroop.aControlState.mIsInFormation = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대 진군 컨트롤을 시작합니다.
	public void StartTargetTroopAdvanceControl(BattleTroop pTargetTroop, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		if ((mActiveControl == mAdvanceControl) && // 이미 이동 컨트롤중이라면
			(mAdvanceControl.aAdvanceType == BattleTroopAdvanceControl.AdvanceType.TargetTroop))
		{
			mReservedControl = null; // 기존 예약 컨트롤을 없애고
			mIsForcedReservedControl = false;

			mAdvanceControl.ResetTargetTroop(pTargetTroop, pStartDelta); // 타겟 부대를 재지정
		}
		else // 아니라면
		{
			mReservedControl = mAdvanceControl; // 컨트롤을 예약
			mIsForcedReservedControl = false;

			mAdvanceControl.aStartParam.SetTargetTroopAdvance(pTargetTroop);
			mAdvanceControl.aStartDelta = pStartDelta;

			aControlTroop.aControlState.mIsInFormation = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 복귀 진군 컨트롤을 시작합니다.
	public void StartReturnAdvanceControl(int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		mReservedControl = mAdvanceControl;
		mIsForcedReservedControl = false;

		mAdvanceControl.aStartParam.SetReturnAdvance();
		mAdvanceControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 포메이션 진군 컨트롤을 시작합니다.
	public void StartSkillFormationAdvanceControl(Coord2 pSkillCoord, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		mReservedControl = mAdvanceControl;
		mIsForcedReservedControl = true; // 스킬은 강제

		mAdvanceControl.aStartParam.SetSkillFormationAdvance(pSkillCoord);
		mAdvanceControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 자리잡기 진군 컨트롤을 시작합니다.
	public void StartSkillPositioningAdvanceControl(Coord2 pSkillCoord, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		mReservedControl = mAdvanceControl;
		mIsForcedReservedControl = true; // 스킬은 강제

		mAdvanceControl.aStartParam.SetSkillPositioningAdvance(pSkillCoord);
		mAdvanceControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 바리케이드 주둔 컨트롤을 시작합니다.
	public void StartBarricadeControl(BattleBarricade pBarricade, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		mReservedControl = mBarricadeControl;
		mIsForcedReservedControl = false;

		mBarricadeControl.aStartParam.Set(pBarricade);
		mBarricadeControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 컨트롤을 시작합니다.
	public void StartTargetTroopAttackControl(BattleTroop pTargetTroop, bool pIsIgnoreRange, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		if (mActiveControl != mBodyAttackControl) // 현재 공격이 아닌 다른 컨트롤이라면
		{
			mReservedControl = mBodyAttackControl; // 컨트롤을 예약
			mIsForcedReservedControl = false;

			mBodyAttackControl.aStartParam.SetTargetTroopAttack(pTargetTroop, pIsIgnoreRange);
			mBodyAttackControl.aStartDelta = pStartDelta;
		}
		else // 이미 공격 컨트롤중이라면
		{
			mReservedControl = null; // 기존 예약 컨트롤을 없애고
			mIsForcedReservedControl = false;

			mBodyAttackControl.ResetTargetTroop(pTargetTroop, pIsIgnoreRange, pStartDelta); // 타겟을 재지정
		}

		if (mTurretAttackControl.aTargetingInfo.aIsValid)
			mTurretAttackControl.ResetTargetTroop(pTargetTroop, pIsIgnoreRange, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 컨트롤을 시작합니다.
	public void StartTargetCoordAttackControl(Coord2 pTargetCoord, bool pIsIgnoreRange, int pStartDelta)
	{
		if (aControlTroop.aInteraction.aIsValid)
			aControlTroop.aInteraction.StopInteraction();

		if (mActiveControl != mBodyAttackControl) // 현재 공격이 아닌 다른 컨트롤이라면
		{
			mReservedControl = mBodyAttackControl; // 컨트롤을 예약
			mIsForcedReservedControl = false;

			mBodyAttackControl.aStartParam.SetTargetCoordAttack(pTargetCoord, pIsIgnoreRange);
			mBodyAttackControl.aStartDelta = pStartDelta;
		}
		else // 이미 공격 컨트롤중이라면
		{
			mReservedControl = null; // 기존 예약 컨트롤을 없애고
			mIsForcedReservedControl = false;

			mBodyAttackControl.ResetTargetCoord(pTargetCoord, pIsIgnoreRange, pStartDelta); // 타겟을 재지정
		}

		if (mTurretAttackControl.aTargetingInfo.aIsValid)
			mTurretAttackControl.ResetTargetCoord(pTargetCoord, pIsIgnoreRange, pStartDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 탐색 컨트롤을 시작합니다.
	public void StartSearchControl(BattleTroopSearchControl.RetryOption pRetryOption, BattleTroopSearchControl.MoveOption pMoveOption, int pStartDelta)
	{
		// 셋업 모드의 경우에는 공격 컨트롤로 전환합니다.
		if (mControlTroop.aControlState.mIsSetUpMode)
		{
			StartTargetCoordAttackControl(mControlTroop.aCoord, false, pStartDelta);
			return;
		}

		mReservedControl = mSearchControl;
		mIsForcedReservedControl = false;

		mSearchControl.aStartParam.Set(pRetryOption, pMoveOption);
		mSearchControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 컨트롤을 시작합니다.
	public void StartActionControl(TroopActionType pActionType, int pActionDelay, int pStartDelta)
	{
		if (mActiveControl == mActionControl)
			return; // 기존 액션 중이면 이후 액션은 무시

		mReservedControl = mActionControl;
		mIsForcedReservedControl = false;

		mActionControl.aStartParam.Set(pActionType, pActionDelay);
		mActionControl.aStartDelta = pStartDelta;
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 상태를 갱신합니다.
	public void UpdateAttackingState()
	{
		bool lIsAttackingNow = (mBodyAttackControl.aIsAttacking || mTurretAttackControl.aIsAttacking);
		if (mIsAttacking == lIsAttackingNow)
			return;
		mIsAttacking = lIsAttackingNow;

		aControlTroop.aCommander.OnTroopAttackControlChange(aControlTroop.aTroopSlotIdx, mIsAttacking); // 공격 상태 변화
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 예약된 컨트롤을 시작합니다.
	private void _StartReservedControl(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		int lStartTime = mControlTroop.aFrameUpdater.aElapsedTime;
		int lStartControlTime = lStartTime;
		int lRemainingUpdateDelta = pUpdateDelta;
		int lControlStartCounter = 0;

		while (mReservedControl != null) // mActiveControl.Start()에서 mReservedControl이 다시 초기화될 수 있기에 while 루프에서 반복적으로 처리합니다.
		{
			if (mActiveControl != null)
			{
				if (!mActiveControl.Stop(lStartControlTime, lRemainingUpdateDelta))
				{
					oExtraUpdateDelta = 0;
					return; // 아직 종료중
				}
				else
				{
					lRemainingUpdateDelta = mActiveControl.aExtraUpdateDelta;
					if (lRemainingUpdateDelta > pUpdateDelta)
					{
						Debug.LogError("lRemainingUpdateDelta > pUpdateDelta"); // 버그 상황
						lRemainingUpdateDelta = pUpdateDelta;
					}
					lStartControlTime = lStartTime + (pUpdateDelta - lRemainingUpdateDelta);
				}
			}
			mActiveControl = mReservedControl;
			mReservedControl = null;
			mIsForcedReservedControl = false;

			if (!mActiveControl.aIsStarted)
			{
				if (mActiveControl.Start()) // 시작해서 정상 진행이 되었다면
				{
					// 여분 시간의 갱신을 진행합니다.
					if (lRemainingUpdateDelta > 0)
						mActiveControl.OnFrameUpdate(lStartControlTime, lRemainingUpdateDelta);
				}
				else // 시작해서 다른 컨트롤로 바뀌었다면
				{
					// 현재 컨트롤을 중단합니다.
					if (!mActiveControl.Stop(lStartControlTime, lRemainingUpdateDelta))
					{
						oExtraUpdateDelta = 0;
						return; // 아직 종료중
					}
					else
					{
						lRemainingUpdateDelta = mActiveControl.aExtraUpdateDelta;
						if (lRemainingUpdateDelta > pUpdateDelta)
						{
							Debug.LogError("lRemainingUpdateDelta > pUpdateDelta"); // 버그 상황
							lRemainingUpdateDelta = pUpdateDelta;
						}
						lStartControlTime = lStartTime + (pUpdateDelta - lRemainingUpdateDelta);
					}
					mActiveControl = null;
				}
			}

			if (++lControlStartCounter >= cControlStartCounterLogLimit)
			{
				Debug.LogError("lControlStartCounter  overflow - " + this);
			#if UNITY_EDITOR
				if (lControlStartCounter >= cControlStartCounterStopLimit) // 에디터에서는 디버깅을 위해 추가로 더 기다림
			#endif
					break; // 과도한 반복 호출
			}
		}

		oExtraUpdateDelta = lRemainingUpdateDelta;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mControlTroop;

	private BattleTroopHoldControl mHoldControl;
	private BattleTroopSpawnControl mSpawnControl;
	private BattleTroopAdvanceControl mAdvanceControl;
	private BattleTroopBarricadeControl mBarricadeControl;
	private BattleTroopAttackControl mBodyAttackControl;
	private BattleTroopAttackControl mTurretAttackControl;
	private BattleTroopSearchControl mSearchControl;
	private BattleTroopActionControl mActionControl;

	private BattleTroopAttackControl[] mWeaponAttackControls;
	private BattleTroopAttackControl mMainAttackControl;
	private BattleTroopAttackControl mSubAttackControl;

	private BattleTroopControl mActiveControl;
	private BattleTroopControl mReservedControl;
	private bool mIsForcedReservedControl;

	private int mTargetCoordAttackRepeatCount;
	private bool mIsAttacking;
}
