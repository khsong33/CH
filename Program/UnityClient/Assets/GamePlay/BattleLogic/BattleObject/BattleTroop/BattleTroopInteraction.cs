using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopInteraction
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 부대
	public BattleTroop aControlTroop
	{
		get { return mControlTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호 작용 타입
	public TroopInteractionType aTroopInteractionType
	{
		get { return mTroopInteractionType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호 작용중 여부
	public bool aIsValid
	{
		get { return (mTroopInteractionType != TroopInteractionType.None); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대와 상호 작용하고 있는 부대
	public BattleTroop aInteractionTroop
	{
		get { return mInteractionTroopLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 치료 가능 여부
	public bool aIsRecoveryAvailable
	{
		get { return mController.aActiveControl.aIsRecoveryAvailable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 가능 여부
	public bool aIsAdvancable
	{
		get { return true; } // 현재는 이동이 최우선
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 가능 여부
	public bool aIsAttackable
	{
		get
		{
			switch (mTroopInteractionType)
			{
			case TroopInteractionType.Recovery:
				return ((aControlTroop.aHpPercent >= BattleConfig.get.mRecoveryEndHpPercents[(int)mControlTroopBody]) || // 필수 회복 HP를 넘었거나
					(mControlTroop.aTroopSpec.mBody == TroopBody.Vehicle)); // 차량이라면 공격 가능
			default:
				return true;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopInteraction()
	{
		mInteractionTroopLink = new GuidLink<BattleTroop>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroopController pController)
	{
		mController = pController;
		mControlTroop = pController.aControlTroop;
		mControlTroopBody = aControlTroop.aTroopSpec.mBody;

		StopInteraction();
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호 작용을 시작합니다.
	public void StartInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pInteractionTroop, int pInteractionDelay)
	{
		mTroopInteractionType = pTroopInteractionType;
		mInteractionTroopLink.Set(pInteractionTroop);
		mInteractionTimer = pInteractionDelay;

		// 보병은 탐색으로 전환합니다.
		if (mControlTroopBody == TroopBody.Soldier)
			mController.StartSearchControl(
				BattleTroopSearchControl.RetryOption.NoRetry,
				BattleTroopSearchControl.MoveOption.HoldPosition,
				0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 상호 작용을 중단합니다.
	public void StopInteraction()
	{
		mTroopInteractionType = TroopInteractionType.None;
		mInteractionTroopLink.Set(null);
		mInteractionTimer = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신을 합니다.
	public void UpdateFrame(int pUpdateDelta)
	{
		// 상호 작용 타입을 갱신합니다.
		if (mTroopInteractionType != TroopInteractionType.None)
		{
			if (mInteractionTroopLink.Get() == null)
			{
				mTroopInteractionType = TroopInteractionType.None;
				mInteractionTimer = 0;
			}
			else if (mInteractionTimer > 0)
			{
				mInteractionTimer -= pUpdateDelta;
				if (mInteractionTimer <= 0)
					StopInteraction();
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopController mController;
	private BattleTroop mControlTroop;
	private TroopBody mControlTroopBody;

	private TroopInteractionType mTroopInteractionType;
	private GuidLink<BattleTroop> mInteractionTroopLink;
	private int mInteractionTimer;
}
