using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopMoveUpdater
{
	public enum MoveType
	{
		FrontMove, // 앞장서는 이동
		FollowAdvanceMove, // 앞 부대를 줄줄이 따라가는 이동
		FollowFormationMove, // 포메이션을 유지하며 따라가는 이동
		FinalFormationMove, // 최종 포메이션 좌표로 이동
		AttackMove, // 공격 대상을 향한 이동
		ReturnMove, // 복귀 이동
		BlitzMove, // 블리츠 상태 이동(제거 예정)
		PositioningMove, // (강제)자리잡기 이동

		None,
	}
	public enum StopType
	{
		Arrived,
		Hold,

		None,
	}
	public enum RotationType
	{
		MovingRotation,
		RotateAndMove,
	}

	private delegate OnUpdateMoveDelegate OnUpdateMoveDelegate(int pUpdateDelta, out int oExtraUpdateDelta);

	private const int cPathTileCount = 100;
	private const int cNextUpdateTileWaitLimit = 2000;
	private const int cMoveRotationAngleThreshold = 30;

	private const int cWaitMoveCountLimit = int.MaxValue;
	private const int cCorrectMoveCountLimit = 1;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 이동 갱신 여부
	public bool aIsMoving
	{
		get { return (mMoveType != MoveType.None); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 타입
	public MoveType aMoveType
	{
		get { return mMoveType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 멈춤 타입
	public StopType aStopType
	{
		get { return mStopType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 대기 여부
	public bool aIsHoldMove
	{
		get { return mIsHoldMove; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 시작 좌표
	public Coord2 aMoveStartCoord
	{
		get { return mMoveStartCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 목표 좌표
	public Coord2 aMoveEndCoord
	{
		get { return mMoveEndCoord; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 목표 타일
	public BattleGridTile aMoveEndTile
	{
		get { return mMoveEndTile; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 후진 가능 여부
	public bool aIsMovableBackward
	{
		get { return mIsMovableBackward; }
		set { mIsMovableBackward = false; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 이동 속도
	public int aUpdateMoveSpeed
	{
		get { return mUpdateMoveSpeed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 중에 있는 타일
	public BattleGridTile aEnteringTile
	{
		get { return mEnteringTile; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 이동하고 있는 경로 타일 배열
	public BattleGridTile[] aPathTiles
	{
		get { return mPathTiles; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 이동하고 있는 경로 타일 수(시작 타일 제외, 마지막 타일 제외)
	public int aPathTileCount
	{
		get { return mPathTileCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 이동이 몇 번째 이동인지
	public int aPathMoveIndex
	{
		get { return mPathMoveIndex; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 추가 갱신 델타
	public int aExtraUpdateDelta
	{
		get { return mExtraUpdateDelta; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 속도
	public int aMoveSpeed
	{
		get
		{
			if (mMoveType == MoveType.PositioningMove) // 자리잡기 이동일 경우
				return mUpdateTroop.aTroopSpec.mStatTable.Get(TroopStat.AdvanceSpeed); // 상태의 영향을 받지 않음
			else if (mMoveType == MoveType.FrontMove) // 앞장선 부대 이동일 경우
				return mUpdateTroop.aCommander.aMaxAdvanceSpeed; // 지휘관 속도로
			else // 그 외의 이동일 경우
				return mUpdateTroop.aStat.aAdvanceSpeed; // 자신의 속도로
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopMoveUpdater()
	{
		mPathTiles = new BattleGridTile[cPathTileCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void Init(BattleTroop pUpdateTroop)
	{
		mUpdateTroop = pUpdateTroop;
		mBattleGrid = pUpdateTroop.aBattleGrid;

		mMoveType = MoveType.None;
		mStopType = StopType.Arrived;
		mIsHoldMove = false;
		mMoveStartCoord = mUpdateTroop.aCoord;
		mMoveEndCoord = mMoveStartCoord;
		mPathTileCount = 0;
		mWaitMoveCount = 0;
		mUpdateMoveSpeed = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 초기화합니다.
	public void ResetMove()
	{
		mMoveType = MoveType.None;
		mStopType = StopType.Arrived;
		mIsHoldMove = false;
		mPathTileCount = 0;
		mWaitMoveCount = 0;
		mUpdateMoveSpeed = 0;

		mEnteringTile = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 지정합니다.
	public bool SetMove(Coord2 pDstCoord, MoveType pMoveType, int pSqrApproachRange, RotationType pRotationType, bool pIsMovableBackward)
	{
		if (mUpdateTroop.aCoord == pDstCoord)
		{
			mMoveType = MoveType.None;
			return false; // 이미 도착
		}

		mPathTileCount = 0;
		mWaitMoveCount = 0;
		mCorrectMoveCount = 0;

		mMoveType = pMoveType;
		mMoveStartCoord = mUpdateTroop.aCoord;
		mMoveEndCoord = pDstCoord;
		mIsMovableBackward = pIsMovableBackward;
		mSqrApproachRange = pSqrApproachRange;

		mMoveEndTile = mBattleGrid.GetTile(mMoveEndCoord);
		if (mMoveEndTile == null) // 존재하지 않는 타일로의 이동
		{
			mMoveType = MoveType.None;
			return false;
		}
		else if (mMoveEndTile == mUpdateTroop.aGridTile) // 같은 타일에서의 이동
		{
			mPathTiles[0] = mMoveEndTile;
			mPathTileCount = 1;
		}
		else // 다른 타일로의 이동
		{
			mBattleGrid.aPathFinder.SearchTilePath(
				mUpdateTroop.aGridTile,
				mMoveEndTile,
				_OnGetTileWeight,
				mPathTiles,
				out mPathTileCount);
			//Debug.Log("mPathTileCount=" + mPathTileCount);

			if ((mPathTileCount > 0) &&
				(mMoveEndTile != mPathTiles[mPathTileCount - 1])) // 갈 수 없는 타일로 이동해서 목표 지점이 변경
			{
				mMoveEndTile = mPathTiles[mPathTileCount - 1];
				mMoveEndCoord = mMoveEndTile.aCenterCoord;
			}
		}

		if (mPathTileCount <= 0)
		{
			mMoveType = MoveType.None;
			return false; // 경로 못찾음
		}

		mExtraUpdateDelta = 0;

		// 이동 갱신을 초기화합니다.
		mPathMoveCount = mPathTileCount - 1; // 이동 회수이므로 타일 수에서 1을 뺀다.
		mPathMoveIndex = 0;
		mOnUpdateMoveDelegate = _GetNextPathTileUpdate(pRotationType);

		return aIsMoving;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 갱신합니다.
	public void UpdateMove(int pUpdateDelta)
	{
		if (mMoveType == MoveType.None)
		{
			mExtraUpdateDelta = pUpdateDelta;
			return;
		}

		if (aMoveSpeed <= 0)
			return; // 갱신은 하되 변화 없음

		if (mIsHoldMove)
			HoldMove(false);

		mOnUpdateMoveDelegate = mOnUpdateMoveDelegate(pUpdateDelta, out mExtraUpdateDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 보류합니다.
	public void HoldMove(bool pIsHoldMove)
	{
		if (mIsHoldMove == pIsHoldMove)
			return;
		mIsHoldMove = pIsHoldMove;

		mUpdateTroop.aStageTroop.OnHoldMove(mIsHoldMove); // 스테이지에 보입니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 멈춥니다.
	public void StopMove()
	{
		if (mMoveType == MoveType.None)
			return;

		mMoveEndCoord = mUpdateTroop.aCoord;
		mMoveDelay = mMoveTimer; // 현재 경과 시간까지를 딜레이로 간주

		mUpdateTroop.aStageTroop.OnStopMove(mMoveEndCoord); // 스테이지에 보입니다.

		mMoveType = MoveType.None;
	}
	//-------------------------------------------------------------------------------------------------------
	// 경로 이동 좌표를 얻습니다.
	public Coord2 GetPathMoveCoord(int pPathMoveIndex, Coord2 pPrevPathCoord)
	{
		Coord2 lPathMoveCoord;
		int lNextPathLength;
		if (pPathMoveIndex < (mPathMoveCount - 1))
			_SetCrossTilePathCoord(pPathMoveIndex, out lPathMoveCoord, out lNextPathLength);
		else if (pPathMoveIndex < mPathMoveCount) // pPathMoveIndex == (mPathMoveCount - 1)
			_SetPreEndTilePathCoord(pPathMoveIndex, pPrevPathCoord.z, out lPathMoveCoord, out lNextPathLength);
		else
			lPathMoveCoord = mMoveEndCoord; // 최종 목표 좌표

		return lPathMoveCoord;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 타일 횡단하는 경우의 경로 좌표를 얻습니다.
	private void _SetCrossTilePathCoord(int pPathMoveIndex, out Coord2 oNextPathCoord, out int oNextPathLength)
	{
		BattleGridTile lCrossToTile = mPathTiles[pPathMoveIndex + 1];
		oNextPathCoord = (mPathTiles[pPathMoveIndex].aCenterCoord + lCrossToTile.aCenterCoord) / 2; // 다음 타일과 그 다음 타일과의 중점
		//Debug.Log("pPathMoveIndex=" + pPathMoveIndex + "/" + mPathMoveCount + " lNextPathLength=?start");

		if (pPathMoveIndex <= 1)
		{
			oNextPathLength = 0;
		}
		else
		{
			BattleGridTile lCrossFromTile = mPathTiles[pPathMoveIndex - 1];
			if (lCrossFromTile.aCol == lCrossToTile.aCol) // 세로로 가로지른다면
				oNextPathLength = mBattleGrid.aTileScaleZ; // 타일의 세로 길이
			else if (lCrossFromTile.aRow == lCrossToTile.aRow) // 가로로 가로지른다면
				oNextPathLength = mBattleGrid.aTileScaleX; // 타일의 가로 길이
			else // 사선으로 가로지른다면
				oNextPathLength = mBattleGrid.aTileHalfDiagonalScale; // 타일의 사선 길이
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 마지막 타일로 가기 직전의 경로 좌표를 얻습니다.
	private void _SetPreEndTilePathCoord(int pPathMoveIndex, int pPrevPathCoordZ, out Coord2 oNextPathCoord, out int oNextPathLength)
	{
		BattleGridTile lCrossFromTile = mPathTiles[pPathMoveIndex - 1];
		BattleGridTile lCrossingTile = mPathTiles[pPathMoveIndex];
		BattleGridTile lCrossToTile = mPathTiles[pPathMoveIndex + 1];

		int lCrossingTileBottom = lCrossingTile.aRow * mBattleGrid.aTileScaleZ; // 아래쪽 경계
		if ((lCrossingTile.aCol & 0x01) != 0) // 홀수열이라면
			lCrossingTileBottom -= mBattleGrid.aTileBlockScaleZ; // 절반 아래로
		int lCrossingTileCenter = lCrossingTileBottom + mBattleGrid.aTileBlockScaleZ; // 중간
		int lCrossingTileTop = lCrossingTileBottom + mBattleGrid.aTileScaleZ; // 위쪽 경계

		int lNextPathCoordX;
		int lNextPathCoordZ;
		if (lCrossFromTile.aCol == lCrossingTile.aCol) // 진입이 수직 이동이고
		{
			if (lCrossingTile.aCol < lCrossToTile.aCol) // 마지막이 오른쪽 이동이라면
			{
				// 오른쪽 중간 좌표를 경로점으로 합니다.
				lNextPathCoordX = (lCrossingTile.aCol + 1) * mBattleGrid.aTileScaleX;
				lNextPathCoordZ = lCrossingTileCenter; // 중간
			}
			else if (lCrossingTile.aCol > lCrossToTile.aCol) // 마지막이 왼쪽 이동이라면
			{
				// 왼쪽 중간 좌표를 경로점으로 합니다.
				lNextPathCoordX = lCrossingTile.aCol * mBattleGrid.aTileScaleX;
				lNextPathCoordZ = lCrossingTileCenter; // 중간
			}
			else // 마지막도 수직 이동이라면
			{
				// 목표점과 같은 X 좌표에 위나 아래쪽 경계를 경로점으로 합니다.
				lNextPathCoordX = mMoveEndCoord.x;
				if (lCrossFromTile.aRow < lCrossingTile.aRow) // 아래에서 위로 가면
					lNextPathCoordZ = lCrossingTileTop; // 위쪽 경계
				else // 위에서 아래으로 가면
					lNextPathCoordZ = lCrossingTileBottom; // 아래쪽 경계
			}
		}
		else // 진입이 가로 이동이고
		{
			if (lCrossingTile.aCol == lCrossToTile.aCol) // 마지막이 수직 이동이며
			{
				if (lCrossFromTile.aCol < lCrossingTile.aCol) // 진입이 그 중에서 오른쪽으로의 이동이면
					lNextPathCoordX = lCrossingTile.aCol * mBattleGrid.aTileScaleX; // 왼쪽 경계
				else // 진입이 그 중에서 왼쪽으로의 이동이면
					lNextPathCoordX = (lCrossingTile.aCol + 1) * mBattleGrid.aTileScaleX; // 오른쪽 경계
				lNextPathCoordZ = lCrossingTileCenter; // 중간
			}
			else // 마지막이 가로 이동이며
			{
				if (pPrevPathCoordZ < lCrossingTileBottom) // 한쪽 아래에서 올라와서
				{
					if (mMoveEndCoord.z < lCrossingTileCenter) // 다른 쪽 중간 아래로 간다면
						lNextPathCoordZ = lCrossingTileBottom; // 아래쪽 경계
					else // 다른 쪽 중간 위로 간다면
						lNextPathCoordZ = lCrossingTileCenter; // 중간
				}
				else if (pPrevPathCoordZ > lCrossingTileTop) // 한쪽 위에서 내려와서
				{
					if (mMoveEndCoord.z < lCrossingTileCenter) // 다른 쪽 중간 위로 간다면
						lNextPathCoordZ = lCrossingTileTop; // 위쪽 경계
					else // 다른 쪽 중간 아래로 간다면
						lNextPathCoordZ = lCrossingTileCenter; // 중간
				}
				else if (pPrevPathCoordZ < lCrossingTileCenter) // 한쪽 중간 아래에서 올라와서
				{
					if (mMoveEndCoord.z < lCrossingTileBottom) // 다른 쪽 아래로 간다면
						lNextPathCoordZ = lCrossingTileBottom; // 아래쪽 경계
					else if (mMoveEndCoord.z < lCrossingTileCenter) // 다른 쪽 중간 아래로 간다면
						lNextPathCoordZ = pPrevPathCoordZ; // 같은 높이
					else // 다른 쪽 중간 위로 간다면
						lNextPathCoordZ = lCrossingTileCenter; // 중간
				}
				else // 한쪽 중간 위에서 내려와서
				{
					if (mMoveEndCoord.z > lCrossingTileTop) // 다른 쪽 위로 간다면
						lNextPathCoordZ = lCrossingTileTop; // 위쪽 경계
					else if (mMoveEndCoord.z > lCrossingTileCenter) // 다른 쪽 중간 위로 간다면
						lNextPathCoordZ = pPrevPathCoordZ; // 같은 높이
					else // 다른 쪽 중간 아래로 간다면
						lNextPathCoordZ = lCrossingTileCenter; // 중간
				}
				lNextPathCoordX = lCrossingTile.aCol * mBattleGrid.aTileScaleX + mBattleGrid.aTileBlockScaleX; // 위의 모든 경우에 대해 X는 중간
			}
		}

		oNextPathCoord = new Coord2(lNextPathCoordX, lNextPathCoordZ);
		oNextPathLength = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 경로 노드로의 이동 갱신 함수를 얻습니다.
	private OnUpdateMoveDelegate _GetNextPathTileUpdate(RotationType pRotationType)
	{
		mPrevPathCoord = mUpdateTroop.aCoord;

		int lNextPathLength;

		++mPathMoveIndex;
		if (mPathMoveIndex < (mPathMoveCount - 1))
		{
			_SetCrossTilePathCoord(mPathMoveIndex, out mNextPathCoord, out lNextPathLength);
		}
		else if (mPathMoveIndex < mPathMoveCount) // mPathMoveIndex == (mPathMoveCount - 1)
		{
			_SetPreEndTilePathCoord(mPathMoveIndex, mPrevPathCoord.z, out mNextPathCoord, out lNextPathLength);
		}
		else
		{
			mNextPathCoord = mMoveEndCoord; // 최종 목표 좌표
			lNextPathLength = 0;
			//Debug.Log("mPathMoveIndex=" + mPathMoveIndex + "/" + mPathMoveCount + " lNextPathLength=?end");

// 			if (mMoveEndTile.aOccupyingTroop == null)
// 				mMoveEndTile.aOccupyingTroop = mUpdateTroop; // 미리 점유하기
		}

		mEnteringTile = mPathTiles[mPathMoveIndex];

		Coord2 lPathMoveVector = mNextPathCoord - mUpdateTroop.aCoord;
		if (lNextPathLength <= 0)
			lNextPathLength = lPathMoveVector.RoughMagnitude();

		mUpdateMoveSpeed = aMoveSpeed;
		if (mUpdateMoveSpeed <= 0)
			mMoveDelay = 0;
		else
			mMoveDelay = lNextPathLength * 1000 / mUpdateMoveSpeed;
		mMoveTimer = 0;

		StopType lStopType;
		if (mPathMoveIndex >= mPathMoveCount)
		{
			if ((mMoveType == MoveType.FollowAdvanceMove) ||
				(mMoveType == MoveType.FollowFormationMove))
				lStopType = StopType.Hold;
			else
				lStopType = StopType.Arrived;
		}
		else
			lStopType = StopType.None;
		mUpdateTroop.aStageTroop.OnSetDstCoord(mNextPathCoord, mMoveDelay, lStopType); // 스테이지에 보입니다.

		mUpdateTroop.aBodyRotationUpdater.SetDstRotation(
			lPathMoveVector.RoughDirectionY(),
			mUpdateTroop.aStat.aTroopStatTable.Get(TroopStat.MaxRotationDelay),
			mIsMovableBackward);

		switch (pRotationType)
		{
		case RotationType.MovingRotation:
			return _OnUpdateMove_MoveTile; // 바로 이동 시작
		case RotationType.RotateAndMove:
			if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
				return _OnUpdateMove_RotateBeforeStart; // 회전을 먼저 완료해야 이동합니다.
			else
				return _OnUpdateMove_MoveTile; // 바로 이동 시작
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 갱신 함수를 지정하고 갱신합니다.
	private OnUpdateMoveDelegate _SetOnUpdateMoveDelegate(OnUpdateMoveDelegate pOnUpdateMoveDelegate, int pUpdateDelta, out int oExtraUpdateDelta)
	{
		if (pUpdateDelta > 0)
		{
			mMoveTimer = 0;
			return pOnUpdateMoveDelegate(pUpdateDelta, out oExtraUpdateDelta);
		}
		else
		{
			oExtraUpdateDelta = 0;
			return pOnUpdateMoveDelegate;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateMoveDelegate
	private OnUpdateMoveDelegate _OnUpdateMove_RotateBeforeStart(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		mUpdateTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);
		if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
			return _SetOnUpdateMoveDelegate(_OnUpdateMove_RotateBeforeStart, 0, out oExtraUpdateDelta); // 시작 회전 계속

		return _SetOnUpdateMoveDelegate(_OnUpdateMove_MoveTile, mUpdateTroop.aBodyRotationUpdater.aExtraUpdateDelta, out oExtraUpdateDelta); // 이동 시작
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateMoveDelegate
	private OnUpdateMoveDelegate _OnUpdateMove_MoveTile(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		// 이동하면서 회전도 같이 합니다.
		if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
		{
			mUpdateTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);
			int lDegreeDistance = Mathf.Abs(MathUtil.DegreeVector(mUpdateTroop.aDirectionY, mUpdateTroop.aBodyRotationUpdater.aRotationEndDirectionY));
			if (lDegreeDistance > cMoveRotationAngleThreshold) // 임계치보다 각도 차이가 크면
				return _SetOnUpdateMoveDelegate(_OnUpdateMove_MoveTile, 0, out oExtraUpdateDelta); // 이동 안하고 회전만 갱신
		}

		// 좌표 보간을 해서 갱신 좌표를 얻습니다.
		mMoveTimer += pUpdateDelta;
		Coord2 lUpdateCoord;
		int lDelegateUpdateDelta;
		if (mMoveTimer < mMoveDelay) // 다음 타일로 이동 중이라면
		{
			lUpdateCoord = Coord2.RoughLerp(mPrevPathCoord, mNextPathCoord, mMoveTimer, mMoveDelay); // 좌표 보간을 해서 갱신 좌표를 얻습니다.
			lDelegateUpdateDelta = 0;
		}
		else // 이동 시간이 지났다면
		{
			lUpdateCoord = mNextPathCoord; // 목표 좌표가 바로 갱신 좌표가 됩니다.
			lDelegateUpdateDelta = mMoveTimer - mMoveDelay;
		}

		mNextUpdateTile = mBattleGrid.GetClampedTile(lUpdateCoord);

		// 다른 타일로 넘어가는 순간의 처리를 합니다.
		if (mNextUpdateTile != mUpdateTroop.aGridTile)
		{
			// 최종 목표 타일로의 진입을 검사합니다.
			if (mNextUpdateTile == mMoveEndTile)
			{
				if (!_CheckOccupiable(mNextUpdateTile)) // 점유할 수 없다면
					return _StopMove(false, lDelegateUpdateDelta, out oExtraUpdateDelta); // 이동 중단
			}

			// 타일 넘어가는 처리를 진행합니다.
			if (mCorrectMoveCount <= cCorrectMoveCountLimit) // 수정 이동 회수 제한 이내라면
			{
				if ((mNextUpdateTile == mPathTiles[mPathMoveIndex]) && // 경로 상의 타일이고(경로 타일을 옮겨가는 과정에서 잠시 다른 타일을 거칠 수 있는데, 이때는 점유 검사를 안 합니다)
					!_CheckPassable(mNextUpdateTile)) // 통과 불가능할 경우
				{
					// 기다리거나 돌아갑니다.
					if (_CheckWait(mNextUpdateTile)) // 당장은 통과할 수 없지만 기다리면 해결될 상황일 때
					{
						mNextUpdateTileWaitTimer = 0;
						++mWaitMoveCount;
						if (mWaitMoveCount <= cWaitMoveCountLimit) // 제한 회수 이내라면
						{
							return _SetOnUpdateMoveDelegate(_OnUpdateMove_WaitMove, 0, out oExtraUpdateDelta); // 기다립니다.
						}
						else if (!_CheckPassableAlly(mNextUpdateTile)) // 제한 회수를 넘었다면 그것이 다른 팀에 의한 것이라면
						{
							return _StopMove(false, lDelegateUpdateDelta, out oExtraUpdateDelta); // 이동 중단
						}
						else // 아군 팀에 의해 막히는 것이라면
						{
							// 제한 회수를 넘었을 경우 막힘을 무시하고 이대로 이동(아래의 부대 좌표 갱신으로 넘어감)
							//Debug.Log("move through ally pass - " + mUpdateTroop);
						}
					}
					else // 기다려도 해결될 상황이 아니라면
					{
						OnUpdateMoveDelegate lOnUpdateMoveDelegate = _CorrectMove(0, out oExtraUpdateDelta); // 새로운 경로를 찾아보는데
						if (lOnUpdateMoveDelegate != null) // 경로 수정의 결과가 있다면
						{
							return lOnUpdateMoveDelegate; // 경로 수정 결과를 반환(호출 아니고 델리게이트 반환)
						}
						else // 경로 수정의 결과가 없다면
						{
							// 새로운 경로를 찾아보았지만 같은 경로만 반환할 경우에는 다른 방법이 없으므로 점유 막힘을 무시하고 이대로 이동(아래의 부대 좌표 갱신으로 넘어감)
							//Debug.Log("move through blocked pass - " + mUpdateTroop);
						}
					}
				}
			}
			else // 수정 이동 회수를 넘어섰다면
			{
				if (_CheckStopMove(mNextUpdateTile)) // 이동을 멈춰야할 정도라면
					return _StopMove(false, lDelegateUpdateDelta, out oExtraUpdateDelta); // 이동 중단

				// 그렇지 않은 경우는 막힘을 무시하고 아래의 진행을 계속합니다.
			}
		}

		// 부대 좌표를 갱신합니다.
		bool lIsArrived = (mMoveTimer >= mMoveDelay) && (mPathMoveIndex >= mPathMoveCount);
		mUpdateTroop.UpdateCoord(lUpdateCoord, lIsArrived);

		mWaitMoveCount = 0; // 의미 있는 이동이 있었으므로 초기화

		// 다음 행동을 결정합니다.
		if (mMoveTimer < mMoveDelay) // 다음 타일로 이동 중이라면
		{
// 			if ((mPathMoveIndex >= mPathMoveCount) &&
// 				(mMoveEndTile.aOccupyingTroop == null))
// 					mMoveEndTile.aOccupyingTroop = mUpdateTroop; // 못했던 점유하기

			return _SetOnUpdateMoveDelegate(_OnUpdateMove_MoveTile, lDelegateUpdateDelta, out oExtraUpdateDelta); // 이동 계속
		}
		else // 다음 타일에 도착했다면
		{
			if (mPathMoveIndex < mPathMoveCount)
			{
				return _SetOnUpdateMoveDelegate(_GetNextPathTileUpdate(RotationType.MovingRotation), lDelegateUpdateDelta, out oExtraUpdateDelta); // 다음 이동 계속
			}
			else
			{
				return _StopMove(true, lDelegateUpdateDelta, out oExtraUpdateDelta); // 이동 종료
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateMoveDelegate
	private OnUpdateMoveDelegate _OnUpdateMove_WaitMove(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		// 기다리면서 회전은 합니다.
		if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
			mUpdateTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);

		// 다음 행동을 결정합니다.
		if (!_CheckPassable(mNextUpdateTile)) // 계속 막혀있다면
		{
			// 기다려봅니다.
			if (_CheckWait(mNextUpdateTile)) // 당장은 통과할 수 없지만 기다리면 해결될 상황일 때
			{
				mNextUpdateTileWaitTimer += pUpdateDelta;
				if (mNextUpdateTileWaitTimer < cNextUpdateTileWaitLimit) // 제한 시간이 지나지 않았다면
				{
					oExtraUpdateDelta = 0;
					return _OnUpdateMove_WaitMove; // 기다립니다(호출 아니고 델리게이트 반환).
				}
			}

			// 기다려도 해결될 상황이 아니라면 이 타일은 포기하고 이동 경로를 다시 짭니다.
			OnUpdateMoveDelegate lOnUpdateMoveDelegate = _CorrectMove(0, out oExtraUpdateDelta); // 새로운 경로를 찾아보는데
			if (lOnUpdateMoveDelegate != null) // 경로 수정의 결과가 있다면
			{
				return lOnUpdateMoveDelegate; // 경로 수정 결과를 반환(호출 아니고 델리게이트 반환)
			}
			else // 경로 수정의 결과가 없다면
			{
				return _SetOnUpdateMoveDelegate(_OnUpdateMove_MoveTile, pUpdateDelta, out oExtraUpdateDelta); // 무시하고 이동
			}
		}
		else // 뚫렸다면
		{
			// 이동을 재개합니다.
			return _SetOnUpdateMoveDelegate(_OnUpdateMove_MoveTile, pUpdateDelta, out oExtraUpdateDelta);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnUpdateMoveDelegate
	private OnUpdateMoveDelegate _OnUpdateMove_RotateBeforeEnd(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		mUpdateTroop.aBodyRotationUpdater.UpdateRotation(pUpdateDelta);
		if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
		{
			oExtraUpdateDelta = 0;
			return _OnUpdateMove_RotateBeforeEnd; // 종료 회전 계속(호출 아니고 델리게이트 반환)
		}

		oExtraUpdateDelta = mUpdateTroop.aBodyRotationUpdater.aExtraUpdateDelta;
		mMoveType = MoveType.None;
		return null; // 이동 후에 회전까지 다 해서 갱신 종료
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대를 통과할 수 있는지 여부를 얻습니다.
	private bool _CheckOccupiable(BattleGridTile pGridTile)
	{
		foreach (BattleTroop iOccupationTroop in pGridTile.aOccupyingTroopList)
		{
			if (iOccupationTroop.aTeamIdx != mUpdateTroop.aTeamIdx) // 다른 팀이라면
				return false; // 불가
			if (!iOccupationTroop.aMoveUpdater.aIsMoving) // 멈춰있다면
				return false; // 불가
		}
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부대를 통과할 수 있는지 여부를 얻습니다.
	private bool _CheckPassable(BattleGridTile pGridTile)
	{
		foreach (BattleTroop iOccupationTroop in pGridTile.aOccupyingTroopList)
		{
			if (iOccupationTroop.aTeamIdx != mUpdateTroop.aTeamIdx) // 다른 팀이라면
				return false; // 막힘

// 			if (iOccupationTroop == mUpdateTroop) // 자신이라면
// 				continue; // 통과
// 			if (iOccupationTroop.aTroopSpec.mBody == TroopBody.Soldier) // 보병이라면
// 				continue; // 통과
// 			if ((iOccupationTroop.aCommander == mUpdateTroop.aCommander) && // 같은 지휘관의 부대이고
// 				(iOccupationTroop.aTroopSlotIdx < mUpdateTroop.aTroopSlotIdx)) // 갱신 부대가 점유 부대보다 선행 부대라면
// 				continue; // 통과
// 
// 			return false; // 막힘
		}
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 아군 부대는 통과할 수 있는지 여부를 얻습니다.
	private bool _CheckPassableAlly(BattleGridTile pGridTile)
	{
		foreach (BattleTroop iOccupationTroop in pGridTile.aOccupyingTroopList)
		{
			if (iOccupationTroop.aTeamIdx != mUpdateTroop.aTeamIdx) // 다른 팀이라면
				return false; // 막힘
		}
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타일로의 이동을 기다릴지 여부를 얻습니다.
	private bool _CheckWait(BattleGridTile pGridTile)
	{
		foreach (BattleTroop iOccupationTroop in pGridTile.aOccupyingTroopList)
		{
			if ((iOccupationTroop.aMoveUpdater.mEnteringTile == null) || // 점유중인 부대가 완전히 멈추었거나
				(iOccupationTroop.aMoveUpdater.mEnteringTile == mUpdateTroop.aGridTile) || // 자신이 현재 있는 타일로 들어오려 하거나
				(iOccupationTroop.aMoveUpdater.aMoveEndTile == mNextUpdateTile)) // 그 타일을 최종 목표로 해서 이동중이라면
				return false; // 기다려도 소용 없음
		}
		return true; // 기다림
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타일로의 이동을 멈춰야 할지 여부를 얻습니다.
	private bool _CheckStopMove(BattleGridTile pGridTile)
	{
		foreach (BattleTroop iOccupationTroop in pGridTile.aOccupyingTroopList)
		{
			if (iOccupationTroop.aTeamIdx != mUpdateTroop.aTeamIdx) // 다른 팀이라면
				return true; // 멈춤
		}
		return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnGetNodeWeightDelegate
	private int _OnGetTileWeight(AStarNode pAStarNode)
	{
		BattleGridTile lGridTile = mBattleGrid.aTiles[pAStarNode.aRow, pAStarNode.aCol];

		if (lGridTile.aIsBlocked)
			return BattleGridPathFinder.cMaxWeight;

		if (lGridTile.aOccupyingTroopList.Count <= 0) // 점유 부대가 없으면 
			return lGridTile.aWeight; // 맵의 비중을 반환

		foreach (BattleTroop iOccupationTroop in lGridTile.aOccupyingTroopList)
		{
			if (iOccupationTroop == mUpdateTroop)
				continue; // 자기 자신은 고려 대상에서 제외

			if (iOccupationTroop.aTeamIdx != mUpdateTroop.aTeamIdx) // 다른 팀이 점유하고 있다면
				return BattleConfig.get.mTroopPathTileWeight; // 부대 점유 비중을 반환
// 			if (iOccupationTroop.aTroopSpec.mBody == TroopBody.Vehicle) // 차량이 점유하고 있다면
// 				return BattleConfig.get.mTroopPathTileWeight; // 부대 점유 비중을 반환
			if ((iOccupationTroop.aCommander == mUpdateTroop.aCommander) && // 같은 지휘관의 부대인데
				iOccupationTroop.aControlState.mIsAttackMoving) // 공격 이동 중이라면 언제 멈출지 모르므로
				return BattleConfig.get.mTroopPathTileWeight; // 부대 점유 비중을 반환
		}

		return lGridTile.aWeight; // 그 외의 경우는 맵의 비중을 반환
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 중단합니다.
	private OnUpdateMoveDelegate _StopMove(bool pIsSuccess, int pUpdateDelta, out int oExtraUpdateDelta)
	{
		if (!pIsSuccess) // 중단된 이동이라면
			mUpdateTroop.aStageTroop.OnMoveTo(mUpdateTroop.aCoord, true); // 제자리 멈춤 신호

		mEnteringTile = null; // 점유했으므로 진입 타일이었던 정보는 없앰

		if (mUpdateTroop.aBodyRotationUpdater.aIsRotating)
		{
			return _SetOnUpdateMoveDelegate(_OnUpdateMove_RotateBeforeEnd, pUpdateDelta, out oExtraUpdateDelta); // 회전까지 완료해야 갱신이 끝납니다.
		}
		else
		{
			mMoveType = MoveType.None;
			oExtraUpdateDelta = pUpdateDelta;
			return null; // 종료 회전을 할 필요가 없어서 갱신 바로 종료
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 같은 목표점에 대해서 이동을 다시 수정합니다.
	private OnUpdateMoveDelegate _CorrectMove(int pUpdateDelta, out int oExtraUpdateDelta)
	{
		//Debug.Log("_CorrectMove()");

		Coord2 lLastNextPathCoord = mNextPathCoord; // 현재의 다음 타일 좌표를 기억해 두고
		int lLastCorrectMoveCount = mCorrectMoveCount;
		if (SetMove(mMoveEndCoord, mMoveType, mSqrApproachRange, RotationType.MovingRotation, mIsMovableBackward))
		{
			mCorrectMoveCount = lLastCorrectMoveCount + 1; // 수정 이동 회수 증가

			if (mNextPathCoord != lLastNextPathCoord) // 새로운 경로가 이전과는 다른 타일 좌표를 주는 경우에는
			{
				return _SetOnUpdateMoveDelegate(mOnUpdateMoveDelegate, pUpdateDelta, out oExtraUpdateDelta); // 경로 변경 성공
			}
			else // 계속 (갈 수 없는) 같은 좌표를 주는 경우에는
			{
				if ((mNextUpdateTile == mMoveEndTile) && // 목표 타일이고 들어갈 수 없는데
					!_CheckPassableAlly(mNextUpdateTile)) // 그것이 다른 팀에 의한 것이라면
				{
					return _StopMove(false, pUpdateDelta, out oExtraUpdateDelta); // 이동을 중단합니다.
				}
				else
				{
					oExtraUpdateDelta = pUpdateDelta;
					return null; // 어쩔 수 없이 현재 경로로 가야한다는 의미로 null을 반환하고 이를 함수 호출한 곳에서 처리
				}
			}			
		}
		else
		{
			Debug.Log("_StopMove(false) - " + this);
			return _StopMove(false, pUpdateDelta, out oExtraUpdateDelta); // 경로 못 찾음
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGridTile[] mPathTiles;
	private int mPathTileCount;

	private BattleTroop mUpdateTroop;
	private BattleGrid mBattleGrid;

	private MoveType mMoveType;
	private StopType mStopType;
	private bool mIsHoldMove;
	private Coord2 mMoveStartCoord;
	private Coord2 mMoveEndCoord;
	private BattleGridTile mMoveEndTile;
	private int mSqrApproachRange;
	private bool mIsMovableBackward;
	private OnUpdateMoveDelegate mOnUpdateMoveDelegate;

	private int mUpdateMoveSpeed;
	private int mMoveDelay;
	private int mMoveTimer;
	private int mExtraUpdateDelta;
	private Coord2 mPrevPathCoord;
	private Coord2 mNextPathCoord;
	private int mPathMoveCount;
	private int mPathMoveIndex;
	private BattleGridTile mEnteringTile;
	private BattleGridTile mNextUpdateTile;
	private int mNextUpdateTileWaitTimer;
	private int mWaitMoveCount;
	private int mCorrectMoveCount;
}
