using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopRecord
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTroopRecord()
	{
		mDamagePerCommanders = new int[BattleGrid.cMaxUserCommanderCount];
		mDamageCommanderStartIdx = int.MaxValue;
		mDamageCommanderEndIdx = 0;

		for (int iCommander = 0; iCommander < BattleGrid.cMaxUserCommanderCount; ++iCommander)
			mDamagePerCommanders[iCommander] = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(BattleTroop pTroop)
	{
		mTroop = pTroop;
		mGainVeterancyMultiplyRatio = pTroop.aCommander.aTroopSet.mGainVeterancyMultiplyRatio;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리셋
	public void Reset()
	{
		for (int iCommander = mDamageCommanderStartIdx; iCommander < mDamageCommanderEndIdx; ++iCommander)
			mDamagePerCommanders[iCommander] = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관으로부터 입은 피해를 기록합니다.
	public void RecordCommanderDamage(BattleCommander pCommander, int pDamage)
	{
		if (pCommander.aUser == null)
			return; // 유저 지휘관만 경험치를 획득

		mDamagePerCommanders[pCommander.aCommanderGridIdx] += pDamage;
		if (mDamageCommanderStartIdx > pCommander.aCommanderGridIdx)
			mDamageCommanderStartIdx = pCommander.aCommanderGridIdx; // StartIdx는 포함
		if (mDamageCommanderEndIdx <= pCommander.aCommanderGridIdx)
			mDamageCommanderEndIdx = pCommander.aCommanderGridIdx + 1; // EndIdx는 포함 아님

		mAccumDamage += pDamage;
		// 막타를 친 부대의 지휘관을 저장합니다.
		//if (mTroop.aStat.aCurMilliHp <= 0)
		//{
		//	mFinalDamagedCommander = pCommander;
		//}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대에게 데미지를 입힌 지휘관들의 경험치를 갱신합니다.
	public void UpdateCommanderExp()
	{
		//Debug.Log("UpdateCommanderExp() - " + mTroop);

		for (int iCommander = mDamageCommanderStartIdx; iCommander < mDamageCommanderEndIdx; ++iCommander)
		{
			if (mDamagePerCommanders[iCommander] > 0)
			{
				BattleUser lBattleUser;
				BattleCommander lAttackCommander = mTroop.aBattleGrid.aCommanders[iCommander];
				CommanderItem lAttackCommanderItem;

				//int lFinalAttackBonusExp = ((lAttackCommander != null) && (lAttackCommander == mFinalDamagedCommander)) ? BattleConfig.get.mFinalAttackBonusExp : 0;
				int lExperience
					= (int)((Int64)mTroop.aExperience // 막타 경험치를 추가하려거든 (int)((Int64)(mTroop.aExperience + lFinalAttackBonusExp)
					* mDamagePerCommanders[iCommander]
					* mGainVeterancyMultiplyRatio // 지휘관의 성장에 따른 경험치 습득률 보정
					/ mAccumDamage / 10000);

				if (lAttackCommander != null)
					lAttackCommander.UpdateExperience(lExperience);

				// 차후 결과 경험치를 위한 처리
				if (mTroop.aBattleGrid.GetUserCommanderItem(iCommander, out lBattleUser, out lAttackCommanderItem))
				{
					if (lAttackCommanderItem.aIsUserCommanderItem)
						lAttackCommanderItem.AddItemExp(lExperience);
				}
			}
		}
		mAccumDamage = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mTroop;
	private int mGainVeterancyMultiplyRatio;

	private int[] mDamagePerCommanders;
	private int mDamageCommanderStartIdx;
	private int mDamageCommanderEndIdx;

	private int mAccumDamage;
	//private BattleCommander mFinalDamagedCommander;
}
