using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTroopStat
{
	public class WeaponData
	{
		public BattleTroop mTroop;
		public BattleTroopStat mTroopStat;
		public int mWeaponIdx;
		public WeaponSpec mWeaponSpec;

		internal MetaWeaponStatTable mBaseWeaponStatTable;
		internal MetaWeaponStatTable mModifiedWeaponStatTable;
		internal WeaponStatTable mFinalWeaponStatTable;
		internal MetaWeaponRangeStatTable[] mBaseWeaponRangeStatTables;
		internal MetaWeaponRangeStatTable[] mModifiedWeaponRangeStatTables;
		internal WeaponRangeStatTable[] mFinalWeaponRangeStatTables;
		internal MetaWeaponAoeStatTable[] mBaseWeaponAoeStatTables;
		internal MetaWeaponAoeStatTable[] mModifiedWeaponAoeStatTables;
		internal WeaponAoeStatTable[] mFinalWeaponAoeStatTables;

		internal int mAllySightWeaponAccuracyMultiplyRatio;
		internal int mAllySightWeaponScatterMultiplyRatio;

		internal int mUseDelayEndTime;

		public BattleTroop aTroop
		{
			get { return mTroop; }
		}
		public BattleTroopStat aTroopStat
		{
			get { return mTroopStat; }
		}
		public int aWeaponIdx
		{
			get { return mWeaponIdx; }
		}
		public WeaponSpec aWeaponSpec
		{
			get { return mWeaponSpec; }
		}
		public WeaponStatTable aWeaponStatTable
		{
			get { return mFinalWeaponStatTable; }
		}
		public WeaponRangeStatTable[] aWeaponRangeStatTables
		{
			get { return mFinalWeaponRangeStatTables; }
		}
		public WeaponAoeStatTable[] aWeaponAoeStatTables
		{
			get { return mFinalWeaponAoeStatTables; }
		}
		public int aMinAttackRange
		{
			get { return mFinalWeaponStatTable.aMinRange; }
		}
		public int aSqrMinAttackRange
		{
			get { return mFinalWeaponStatTable.aSqrMinRange; }
		}
		public int aMaxAttackRange
		{
			get { return mFinalWeaponRangeStatTables[WeaponSpec.cMaxRangeLevel].aRange; }
		}
		public int aSqrMaxAttackRange
		{
			get { return mFinalWeaponRangeStatTables[WeaponSpec.cMaxRangeLevel].aSqrRange; }
		}
		public int aMaxAoeRadius
		{
			get { return mFinalWeaponAoeStatTables[WeaponSpec.cMaxAoeLevel].aRadius; }
		}
		public int aSqrMaxAoeRadius
		{
			get { return mFinalWeaponAoeStatTables[WeaponSpec.cMaxAoeLevel].aSqrRadius; }
		}
		public int aAllySightWeaponAccuracyMultiplyRatio
		{
			get {return mAllySightWeaponAccuracyMultiplyRatio; }
		}
		public int aAllySightWeaponScatterMultiplyRatio
		{
			get { return mAllySightWeaponScatterMultiplyRatio; }
		}
		public int aUseDelayEndTime
		{
			get { return mUseDelayEndTime; }
		}

		public WeaponData()
		{
			mBaseWeaponStatTable = new MetaWeaponStatTable();
			mModifiedWeaponStatTable = new MetaWeaponStatTable();
			mFinalWeaponStatTable = new WeaponStatTable();

			mBaseWeaponRangeStatTables = new MetaWeaponRangeStatTable[WeaponSpec.cRangeLevelCount];
			mModifiedWeaponRangeStatTables = new MetaWeaponRangeStatTable[WeaponSpec.cRangeLevelCount];
			mFinalWeaponRangeStatTables = new WeaponRangeStatTable[WeaponSpec.cRangeLevelCount];
			for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
			{
				mBaseWeaponRangeStatTables[iRange] = new MetaWeaponRangeStatTable();
				mModifiedWeaponRangeStatTables[iRange] = new MetaWeaponRangeStatTable();
				mFinalWeaponRangeStatTables[iRange] = new WeaponRangeStatTable();
			}

			mBaseWeaponAoeStatTables = new MetaWeaponAoeStatTable[WeaponSpec.cAoeLevelCount];
			mModifiedWeaponAoeStatTables = new MetaWeaponAoeStatTable[WeaponSpec.cAoeLevelCount];
			mFinalWeaponAoeStatTables = new WeaponAoeStatTable[WeaponSpec.cAoeLevelCount];
			for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
			{
				mBaseWeaponAoeStatTables[iAoe] = new MetaWeaponAoeStatTable();
				mModifiedWeaponAoeStatTables[iAoe] = new MetaWeaponAoeStatTable();
				mFinalWeaponAoeStatTables[iAoe] = new WeaponAoeStatTable();
			}
		}

		public void Reset()
		{
			mWeaponSpec = null;
		}

		public void SetUp(BattleTroop pTroop, BattleTroopStat pTroopStat, int pWeaponIdx)
		{
			mTroop = pTroop;
			mTroopStat = pTroopStat;
			mWeaponIdx = pWeaponIdx;
			mWeaponSpec = pTroop.aTroopSpec.mWeaponSpecs[pWeaponIdx];
			if (mWeaponSpec == null)
				return; // 없는 무기

			TroopLevelSpec lTroopLevelSpec = pTroop.aLevelSpec;
			VeterancySpec lVeterancySpec = pTroop.aVeterancySpec;

			mBaseWeaponStatTable.SetUp(mWeaponSpec.mStatTable, mWeaponIdx, lTroopLevelSpec, pTroop.aLevel, lVeterancySpec);
			mModifiedWeaponStatTable.Copy(mBaseWeaponStatTable);
			mFinalWeaponStatTable.Copy(mBaseWeaponStatTable);

			for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
			{
				mBaseWeaponRangeStatTables[iRange].SetUp(mWeaponSpec.mRangeStatTables[iRange], mWeaponIdx, lTroopLevelSpec, pTroop.aLevel, lVeterancySpec);
				if (mBaseWeaponRangeStatTables[iRange].Get(WeaponRangeStat.Penetration) < 1)
				{
					Debug.LogError("WeaponRangeStat.Penetration < 1 - " + mTroop);
					mBaseWeaponRangeStatTables[iRange].Set(WeaponRangeStat.Penetration, 1);
				}
				if (mBaseWeaponRangeStatTables[iRange].Get(WeaponRangeStat.CooldownDelay) < WeaponRangeStatTable.cMinCooldownDelay)
				{
					Debug.LogError("WeaponRangeStat.CooldownDelay < WeaponRangeStatTable.cMinCooldownDelay - " + mTroop);
					mBaseWeaponRangeStatTables[iRange].Set(WeaponRangeStat.CooldownDelay, WeaponRangeStatTable.cMinCooldownDelay);
				}

				mModifiedWeaponRangeStatTables[iRange].Copy(mBaseWeaponRangeStatTables[iRange]);
				mFinalWeaponRangeStatTables[iRange].Copy(mBaseWeaponRangeStatTables[iRange]);
			}

			for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
			{
				mBaseWeaponAoeStatTables[iAoe].SetUp(mWeaponSpec.mAoeStatTables[iAoe], mWeaponIdx, lTroopLevelSpec, pTroop.aLevel, lVeterancySpec);
				mModifiedWeaponAoeStatTables[iAoe].Copy(mBaseWeaponAoeStatTables[iAoe]);
				mFinalWeaponAoeStatTables[iAoe].Copy(mBaseWeaponAoeStatTables[iAoe]);
			}

			mAllySightWeaponAccuracyMultiplyRatio = pTroop.aCommander.aTroopSet.mAllySightWeaponAccuracyMultiplyRatios[pWeaponIdx];
			mAllySightWeaponScatterMultiplyRatio = pTroop.aCommander.aTroopSet.mAllySightWeaponScatterMultiplyRatios[pWeaponIdx];

			mUseDelayEndTime = 0;
		}

		public void SetUseDelay(int pUseTime)
		{
			mUseDelayEndTime = pUseTime + aWeaponStatTable.Get(WeaponStat.UseDelay);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 부대 스탯 테이블
	public TroopStatTable aTroopStatTable
	{
		get { return mFinalTroopStatTable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 데이터들
	public WeaponData[] aWeaponDatas
	{
		get { return mWeaponDatas; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 주무기 공격 타입
	public WeaponAttackType aPrimaryWeaponAttackType
	{
		get { return mPrimaryWeaponAttackType; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 HP
	public int aMaxMilliHp
	{
		get { return mFinalTroopStatTable.Get(TroopStat.BaseMilliHp); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 HP
	public int aCurMilliHp
	{
		get { return mCurMilliHp; }
		set { mCurMilliHp = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스탯 상의 이동 속도
	public int aStatMoveSpeed
	{
		get { return mFinalTroopStatTable.Get(TroopStat.AdvanceSpeed); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 휘하의 다른 부대를 고려한 진군 속도
	public int aAdvanceSpeed
	{
		get { return mAdvanceSpeed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 행군 여부
	public bool aIsFollowAdvance
	{
		get { return mFinalTroopStatTable.aIsFollowAdvance; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시야 범위
	public int aSightRange
	{
		get { return mFinalTroopStatTable.aSightRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시야 범위
	public int aSqrSightRange
	{
		get { return mFinalTroopStatTable.aSqrSightRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 은폐 색적 범위
	public int aHidingDetectionRange
	{
		get { return mFinalTroopStatTable.aHidingDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 은폐 색적 범위
	public int aSqrHidingDetectionRange
	{
		get { return mFinalTroopStatTable.aSqrHidingDetectionRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대의 최소 사정 거리
	public int aMinAttackRange
	{
		get { return mMinAttackRangeWeaponData.aMinAttackRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대의 최소 사정 거리
	public int aSqrMinAttackRange
	{
		get { return mMinAttackRangeWeaponData.aSqrMinAttackRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대의 최대 사정 거리
	public int aMaxAttackRange
	{
		get { return mMaxAttackRangeWeaponData.aMaxAttackRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이 부대의 최대 사정 거리
	public int aSqrMaxAttackRange
	{
		get { return mMaxAttackRangeWeaponData.aSqrMaxAttackRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최적 무기를 찾는 기준이 되는 거리
	public int aSqrBestWeaponCheckRange
	{
		get { return mSqrBestWeaponCheckRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 조준각
	public int aMaxTrackingAngleHalf
	{
		get { return mMaxTrackingAngleHalf; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 사기치
	public int aMaxMilliMorale
	{
		get { return mFinalTroopStatTable.Get(TroopStat.MaxMilliMorale); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 사기치
	public int aCurMilliMorale
	{
		get { return mCurMilliMorale; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 위압 타이머
	public int aSuppressionTimer
	{
		get { return mSuppressionTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 사기 상태
	public TroopMoraleState aMoraleState
	{
		get { return mMoraleState; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과 체인
	public GuidObjectChain<BattleTroopSkillEffect> aSkillEffectChain
	{
		get { return mSkillEffectChain; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 블리츠 타이머
	public int aBlitzTimer
	{
		get { return mBlitzTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 블리츠 쿨타임 타이머
	public int aBlitzCoolTimer
	{
		get { return mBlitzCoolTimer; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자(숨겨짐)
	private BattleTroopStat()
	{
		mBaseTroopStatTable = new MetaTroopStatTable();
		mModifiedTroopStatTable = new MetaTroopStatTable();
		mFinalTroopStatTable = new TroopStatTable();

		mAllocatedWeaponDatas = new WeaponData[TroopSpec.cWeaponCount];
		mWeaponDatas = new WeaponData[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			mAllocatedWeaponDatas[iWeapon] = new WeaponData();

		mSkillEffectChain = new GuidObjectChain<BattleTroopSkillEffect>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 생성 함수
	public static BattleTroopStat Create(BattleTroop pTroop)
	{
		BattleTroopStat lTroopStat;
		if (sPoolingQueue.Count <= 0)
			lTroopStat = new BattleTroopStat();
		else
			lTroopStat = sPoolingQueue.Dequeue();
		lTroopStat.mRefCounter = 1; // 이 부대가 사용하기 시작하면서 1로 초기화

		lTroopStat.SetUp(pTroop, true); // 첫 생성시에는 베테랑 스펙이 없습니다.

		return lTroopStat;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 파괴 함수
	public void Destroy()
	{
		ReleaseRef(); // 참조 카운터만 감소
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화 함수
	public void SetUp(BattleTroop pTroop, bool pIsFirstSetup)
	{
		mTroop = pTroop;
		mTroopSpec = mTroop.aTroopSpec; // 코딩 편의적인 중복 정보
		mCommanderTroopSet = pTroop.aCommander.aTroopSet; // 코딩 편의적인 중복 정보

		int lCurHpRatio = 10000;
		if (!pIsFirstSetup)
			lCurHpRatio = (int)((Int64)mCurMilliHp * 10000 / aMaxMilliHp); // 베테랑 셋업 등의 경우 최대 HP가 갱신되기 전의 HP 비율을 기억

		mBaseTroopStatTable.SetUp(mTroopSpec.mStatTable, pTroop.aLevelSpec, pTroop.aLevel, pTroop.aVeterancySpec, mCommanderTroopSet);
		mModifiedTroopStatTable.Copy(mBaseTroopStatTable);
		mFinalTroopStatTable.Copy(mBaseTroopStatTable);

		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			if ((pTroop.aTroopSpec.mWeaponSpecs[iWeapon] != null) &&
				((iWeapon != TroopSpec.cVeteranWeaponIdx) || ((mTroop.aVeterancySpec != null) && mTroop.aVeterancySpec.mVeteranWeaponEnable))) // 베테랑 무기 사용 조건 검사
			{
				mAllocatedWeaponDatas[iWeapon].SetUp(pTroop, this, iWeapon);
				mWeaponDatas[iWeapon] = mAllocatedWeaponDatas[iWeapon];
			}
			else
				mWeaponDatas[iWeapon] = null;

		mPrimaryWeaponAttackType = mWeaponDatas[TroopSpec.cPrimaryWeaponIdx].mWeaponSpec.mAttackType;

		if (pIsFirstSetup)
			mCurMilliHp = aMaxMilliHp;
		else
			mCurMilliHp = (int)((Int64)aMaxMilliHp * lCurHpRatio / 10000); // 변경된 최대 HP에 대해서 이전과 같은 비율의 현재 HP를 적용

		mCurMilliMorale = aMaxMilliMorale;
		mSuppressionTimer = 0;
		mMoraleState = TroopMoraleState.Good;
		mMaxAdvanceSpeed = int.MaxValue;
		mAdvanceSpeed = aStatMoveSpeed;

		mBlitzTimer = 0;
		mBlitzCoolTimer = 0;

		mSkillEffectChain.Enumerate(_OnModifySkillEffectStat);

		_UpdateMoveSpeed();
		_UpdateSightRange();
		_UpdateWeapon();

		mLastSightRange = aSightRange;
		mLastAttackRange = aMaxAttackRange;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 갱신을 합니다.
	public void OnFrameUpdate(int pFrameDelta)
	{
		// 블리츠 타이머를 갱신합니다.
		if (mBlitzTimer > 0)
		{
			mBlitzTimer -= pFrameDelta;
// 			if (mBlitzTimer <= 0)
// 				DisableBlitz();
		}
		else if (mBlitzCoolTimer > 0)
		{
			mBlitzCoolTimer -= pFrameDelta;
		}

		// 위압 타이머를 갱신합니다.
		if (mSuppressionTimer > 0)
			mSuppressionTimer -= pFrameDelta;

		// 사기를 갱신합니다.
		if ((mCurMilliMorale < aMaxMilliMorale) &&
			!mTroop.aCommander.aIsFleeing)
			RecoverMoraleOnFrameUpdate(pFrameDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 참조를 증가시킵니다.
	public void AddRef()
	{
		++mRefCounter;
	}
	//-------------------------------------------------------------------------------------------------------
	// 참조를 감소시킵니다.
	public void ReleaseRef()
	{
		if (mRefCounter <= 0)
			Debug.LogError("ReleaseRef() called when mRefCounter=" + mRefCounter + " - " + this);

		--mRefCounter;

		// 참조가 없으면 진짜로 파괴하고 풀에 돌려놓습니다.
		if (mRefCounter <= 0)
		{
			_OnDestroy(); // 실제 파괴 처리

			BattleTroopStat.sPoolingQueue.Enqueue(this);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 공격 데미지를 적용합니다.
	public void AddAttackDamage(BattleTroop pAttackTroop, int pAttackId, int pAttackRatio)
	{
	#if UNITY_EDITOR
		if (mTroop.aIsNoDamage)
			return;
	#endif
	
// 		int lFinalAttackRatio = pAttackRatio * pAttackTroop.aStat.mAttackScale * mDefenseScale / 10000;
// 		//Debug.Log("AddAttackDamage() pAttackId=" + pAttackId + " pAttackRatio=" + pAttackRatio + " lFinalAttackRatio=" + lFinalAttackRatio + " pAttackTroop=" + pAttackTroop);
// 		int lFinalAttackDamage = pAttackTroop.aTroopSpec.mAttackMilliDamages[pAttackId] * lFinalAttackRatio / 100;
// 		if (lFinalAttackDamage <= 0)
// 			return; // 공격 효과 없음
// 		mCurMilliHp -= lFinalAttackDamage;
// 		//Debug.Log("lFinalAttackDamage=" + lFinalAttackDamage + " mCurMilliHp=" + mCurMilliHp);
	}
	//-------------------------------------------------------------------------------------------------------
	// 위협을 가합니다.
	public void AddSuppression(int pMilliSuppression)
	{
		int lAddMilliSuppression = (int)((Int64)pMilliSuppression * mCommanderTroopSet.mSuppressionMultiplyRatio / 10000); // 지휘관의 위협 보정 반영(만분율)

// [테스트] 특정 팀 위협 안 받게
// 		if (mTroop.aTeamIdx == 1)
// 			return;
// [테스트] 위압 강화
// 		lAddMilliSuppression *= 200;

		mSuppressionTimer = mFinalTroopStatTable.Get(TroopStat.NonBattleDelay); // 위압 타이머 초기화
		mTroop.SetBattleTimer();

		mCurMilliMorale -= lAddMilliSuppression;
		if (mCurMilliMorale < mCommanderTroopSet.mMinMorale)
			mCurMilliMorale = mCommanderTroopSet.mMinMorale; // 지휘관의 최소 사기치 보장

		switch (mMoraleState)
		{
		case TroopMoraleState.Good:
			{
				if (mCurMilliMorale <= mFinalTroopStatTable.Get(TroopStat.PinDownMilliActivate))
					_UpdateMoraleState(TroopMoraleState.PinDown);
				else if (mCurMilliMorale <= mFinalTroopStatTable.Get(TroopStat.SuppressedMilliActivate))
					_UpdateMoraleState(TroopMoraleState.Suppressed);
				else
					_UpdateMoraleState(mMoraleState);
			}
			break;
		case TroopMoraleState.Suppressed:
			{
				if (mCurMilliMorale <= mFinalTroopStatTable.Get(TroopStat.PinDownMilliActivate))
					_UpdateMoraleState(TroopMoraleState.PinDown);
				else
					_UpdateMoraleState(mMoraleState);
			}
			break;
		case TroopMoraleState.PinDown:
			{
				_UpdateMoraleState(mMoraleState);
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 단위의 사기를 회복합니다.
	public void RecoverMoraleOnFrameUpdate(int pFrameDelta)
	{
		if (mSuppressionTimer <= 0)
			mCurMilliMorale += (int)((Int64)mFinalTroopStatTable.Get(TroopStat.MoraleMilliRecovery) * pFrameDelta * mFinalTroopStatTable.Get(TroopStat.NonBattleMultiplyRatio) / (1000 * 10000)); // 만분율
		else
			mCurMilliMorale += mFinalTroopStatTable.Get(TroopStat.MoraleMilliRecovery) * pFrameDelta / 1000;
		if (mCurMilliMorale > aMaxMilliMorale)
		{
			mCurMilliMorale = aMaxMilliMorale;
		}

		switch (mMoraleState)
		{
		case TroopMoraleState.Good:
			{
				_UpdateMoraleState(mMoraleState);
			}
			break;
		case TroopMoraleState.Suppressed:
			{
				if (mCurMilliMorale >= mFinalTroopStatTable.Get(TroopStat.SuppressedMilliRecover))
					_UpdateMoraleState(TroopMoraleState.Good);
				else
					_UpdateMoraleState(mMoraleState);
			}
			break;
		case TroopMoraleState.PinDown:
			{
				if (mCurMilliMorale >= mFinalTroopStatTable.Get(TroopStat.SuppressedMilliRecover))
					_UpdateMoraleState(TroopMoraleState.Good);
				else if (mCurMilliMorale >= mFinalTroopStatTable.Get(TroopStat.PinDownMilliRecover))
					_UpdateMoraleState(TroopMoraleState.Suppressed);
				else
					_UpdateMoraleState(mMoraleState);

				if (mMoraleState != TroopMoraleState.PinDown) // 핀다운 상태에서 벗어날 때에는
					mTroop.aCommand.StartReturnAdvance(0); // 복귀 진군을 시작합니다.
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 진군 속도를 제한합니다.
	public void LimitAdvanceSpeed(int pMaxAdvanceSpeed)
	{
		mMaxAdvanceSpeed = pMaxAdvanceSpeed;
		mAdvanceSpeed = aStatMoveSpeed;
		if ((mAdvanceSpeed > pMaxAdvanceSpeed) &&
			!mTroop.aIsFallBehind) // 낙오 상태가 아닐 경우에만 속도 제한
			mAdvanceSpeed = pMaxAdvanceSpeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과를 추가합니다.
	public GuidObjectChainItem<BattleTroopSkillEffect> AddSkillEffect(BattleTroopSkillEffect pSkillEffect)
	{
		// 체인에 스킬 효과를 넣습니다.
		GuidObjectChainItem<BattleTroopSkillEffect> lSkillEffectChainItem = aSkillEffectChain.AddLast(pSkillEffect);

		// 스탯 변경을 적용합니다.
		int lLastStatMoveSpeed = aStatMoveSpeed;

		_OnModifySkillEffectStat(pSkillEffect);
		UpdateStat();

		// 지휘관의 최고 이동 속도를 갱신합니다.
		if (lLastStatMoveSpeed != aStatMoveSpeed)
			mTroop.aCommander.UpdateMaxAdvanceSpeed();

		return lSkillEffectChainItem;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과를 제거합니다.
	public void RemoveSkillEffect(GuidObjectChainItem<BattleTroopSkillEffect> pSkillEffectChainItem)
	{
		BattleTroopSkillEffect lSkillEffect = pSkillEffectChainItem.Value;
		if (lSkillEffect.aEffectTroop != mTroop)
		{
			Debug.LogError("incorrect skill effect for this BattleTroop - " + mTroop);
			return; // 다른 부대의 것이 들어옴
		}

		// 체인 아이템을 비웁니다.
		pSkillEffectChainItem.Empty();

		// 스탯 변경을 적용합니다.
		int lLastStatMoveSpeed = aStatMoveSpeed;

		_OnModifySkillEffectStat(lSkillEffect);
		UpdateStat();

		// 지휘관의 최고 이동 속도를 갱신합니다.
		if (lLastStatMoveSpeed != aStatMoveSpeed)
			mTroop.aCommander.UpdateMaxAdvanceSpeed();
	}
	//-------------------------------------------------------------------------------------------------------
	// 주요 스탯을 갱신합니다.
	public void UpdateStat()
	{
		_UpdateMoveSpeed();
		_UpdateSightRange();
		_UpdateWeapon();

		mTroop.aIsHiding = mFinalTroopStatTable.aIsHiding || mTroop.aAffectedGroundStatTable.aIsHiding; // 은폐 동기화

		// 스테이지에 보입니다.
		if (mLastSightRange != aSightRange)
		{
			mLastSightRange = aSightRange;
			mTroop.aStageTroop.OnUpdateSightRange();
			mTroop.UpdateDetection();
		}
		if (mLastAttackRange != aMaxAttackRange)
		{
			mLastAttackRange = aMaxAttackRange;
			mTroop.aStageTroop.OnUpdateAttackRange();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 파괴 때의 처리를 합니다.
	private void _OnDestroy()
	{
		// 스킬 효과 체인을 비웁니다.
		mSkillEffectChain.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 최소 공격 거리를 가진 무기를 얻습니다.
	private WeaponData _GetMinAttackRangeWeaponData()
	{
		int lSqrMinAttackRangeFound = int.MaxValue;
		int lMinAttackRangeWeaponIdx = -1;

		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			WeaponData lWeaponData = mWeaponDatas[iWeapon];
			if (lWeaponData == null)
				continue;

			int lSqrMinAttackRange = mWeaponDatas[iWeapon].mFinalWeaponStatTable.aSqrMinRange;
			if (lSqrMinAttackRangeFound > lSqrMinAttackRange)
			{
				lSqrMinAttackRangeFound = lSqrMinAttackRange;
				lMinAttackRangeWeaponIdx = iWeapon;
			}
		}

		if (lMinAttackRangeWeaponIdx >= 0)
			return mWeaponDatas[lMinAttackRangeWeaponIdx];
		else
			return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 최대 공격 거리를 가진 무기를 얻습니다.
	private WeaponData _GetMaxAttackRangeWeaponData()
	{
		int lSqrMaxAttackRangeFound = int.MinValue;
		int lMaxAttackRangeWeaponIdx = -1;

		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			WeaponData lWeaponData = mWeaponDatas[iWeapon];
			if (lWeaponData == null)
				continue;

			int lSqrMaxAttackRange = mWeaponDatas[iWeapon].mFinalWeaponRangeStatTables[WeaponSpec.cMaxRangeLevel].aSqrRange;
			if (lSqrMaxAttackRangeFound < lSqrMaxAttackRange)
			{
				lSqrMaxAttackRangeFound = lSqrMaxAttackRange;
				lMaxAttackRangeWeaponIdx = iWeapon;
			}
		}

		if (lMaxAttackRangeWeaponIdx >= 0)
			return mWeaponDatas[lMaxAttackRangeWeaponIdx];
		else
			return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 사기 상태를 지정합니다.
	private void _UpdateMoraleState(TroopMoraleState pMoraleState)
	{
		bool lIsChangeFormationOrder
			= ((pMoraleState == TroopMoraleState.PinDown) // 핀 다운 상태에 들어가거나
			|| (mMoraleState == TroopMoraleState.PinDown)); // 핀 다운 상태에서 나갈 경우

		mMoraleState = pMoraleState;

		_UpdateMoveSpeed();
		_UpdateWeapon();

		// 사기 상태 변화를 보입니다.
		mTroop.aStageTroop.OnUpdateMoraleState(mMoraleState);

		// 사기 변화에 따른 포메이션 변경을 처리합니다.
		if (lIsChangeFormationOrder)
			mTroop.aCommander.UpdateFormationOrder();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동속도를 갱신합니다.
	private void _UpdateMoveSpeed()
	{
		// 사기에 영향을 받는 이동 속도를 구합니다.
		int lMoveSpeed;
		if (mCurMilliMorale > 0)
		{
			switch (mMoraleState)
			{
			case TroopMoraleState.Good:
				{
					lMoveSpeed = mModifiedTroopStatTable.Get(TroopStat.AdvanceSpeed);
					break;
				}
			case TroopMoraleState.Suppressed:
				{
					// 이동 속도가 부대 '이동 속도' x '이동 속도 보정 비율'로 적용됩니다.
					lMoveSpeed
						= (int)((Int64)mModifiedTroopStatTable.Get(TroopStat.AdvanceSpeed)
						* mModifiedTroopStatTable.Get(TroopStat.SuppressedSpeedMultiplyRatio)
						/ 10000); // 만분율
					break;
				}
			case TroopMoraleState.PinDown:
			default:
				{
					lMoveSpeed = 0;
					break;
				}
			}
		}
		else
		{
			lMoveSpeed = mModifiedTroopStatTable.Get(TroopStat.AdvanceSpeed); // 도망 속도
		}

		// 지형 효과에 영향을 받는 이동 속도를 구합니다.
		if (mTroop.aAffectedGroundStatTable.CheckEffectAvailableType(mTroop.aTroopSpec.mType))
			lMoveSpeed = (int)((Int64)lMoveSpeed * mTroop.aAffectedGroundStatTable.Get(GroundStat.AdvanceSpeedRatio) / 10000); // 만분율

		// 테이블에 반영합니다.
		mFinalTroopStatTable.Set(TroopStat.AdvanceSpeed, lMoveSpeed);

		// 최소/최대 범위를 제한합니다.
		if (lMoveSpeed > mMaxAdvanceSpeed)
			mTroop.aCommander.UpdateMaxAdvanceSpeed(); // 이 함수 안에서 mAdvanceSpeed가 갱신됨
		else
			mAdvanceSpeed = lMoveSpeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시야 범위를 갱신합니다.
	private void _UpdateSightRange()
	{
		int lSightRange = mModifiedTroopStatTable.Get(TroopStat.SightRange);
		if (mTroop.aAffectedGroundStatTable.CheckEffectAvailableType(mTroop.aTroopSpec.mType))
			lSightRange += mTroop.aAffectedGroundStatTable.Get(GroundStat.SightRangeAdd);

		if (lSightRange < 0)
			lSightRange = 0;

		// 테이블에 반영합니다.
		mFinalTroopStatTable.Set(TroopStat.SightRange, lSightRange);
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 스탯을 갱신합니다.
	private void _UpdateWeapon()
	{
		int lWeaponRange = 0;
		int lWeaponMilliAccuracy = 0;
		int lWeaponPenetration = 0;

		// 사기 변화에 따른 스탯 변화를 적용합니다.
		switch (mMoraleState)
		{
			case TroopMoraleState.Good:
				{
					// 무기 상태를 복구합니다.
					for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
					{
						WeaponData lWeaponData = mWeaponDatas[iWeapon];
						if (lWeaponData == null)
							continue;

						for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
						{
							MetaWeaponRangeStatTable lModifiedWeaponRangeStatTable = lWeaponData.mModifiedWeaponRangeStatTables[iRange];
							WeaponRangeStatTable lFinalWeaponRangeStatTable = lWeaponData.mFinalWeaponRangeStatTables[iRange];

							// 명중률을 복구합니다.
							if (mTroop.aAffectedGroundStatTable.CheckEffectAvailableType(mTroop.aTroopSpec.mType))
							{
								lWeaponRange = (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Range) * mTroop.aAffectedGroundStatTable.Get(GroundStat.WeaponRangeRatio) / 10000); // 만분율
								lWeaponMilliAccuracy = (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.MilliAccuracy) * mTroop.aAffectedGroundStatTable.Get(GroundStat.AccuracyRatio) / 10000); // 만분율
								lWeaponPenetration = (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Penetration) * mTroop.aAffectedGroundStatTable.Get(GroundStat.PenetrationRatio) / 10000); // 만분율
							}
							else
							{
								lWeaponRange = lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Range);
								lWeaponMilliAccuracy = lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.MilliAccuracy);
								lWeaponPenetration = lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Penetration);
							}

							// 예외 처리
							if (lWeaponRange < 0) lWeaponRange = 0;
							if (lWeaponMilliAccuracy < 0) lWeaponMilliAccuracy = 0;
							if (lWeaponPenetration < 0) lWeaponPenetration = 0;

							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.Range, lWeaponRange);
							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.MilliAccuracy, lWeaponMilliAccuracy);
							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.Penetration, lWeaponPenetration);

							// 재장전 시간을 복구합니다.
							lFinalWeaponRangeStatTable.Copy(WeaponRangeStat.CooldownDelay, lModifiedWeaponRangeStatTable);
						}
					}
				}
				break;
			case TroopMoraleState.Suppressed:
				{
					// 무기 상태를 변경합니다.
					for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
					{
						WeaponData lWeaponData = mWeaponDatas[iWeapon];
						if (lWeaponData == null)
							continue;

						for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
						{
							MetaWeaponRangeStatTable lModifiedWeaponRangeStatTable = lWeaponData.mModifiedWeaponRangeStatTables[iRange];
							WeaponRangeStatTable lFinalWeaponRangeStatTable = lWeaponData.mFinalWeaponRangeStatTables[iRange];

							// 명중률이 '명중률' x '명중률 보정 비율'로 적용됩니다.
							int lMilliAccuracyinMorale
								= (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.MilliAccuracy)
								* mFinalTroopStatTable.Get(TroopStat.AccuracyMultiplyRatio)
								/ 10000); // 만분율

							if (mTroop.aAffectedGroundStatTable.CheckEffectAvailableType(mTroop.aTroopSpec.mType))
							{
								lWeaponRange = (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Range) * mTroop.aAffectedGroundStatTable.Get(GroundStat.WeaponRangeRatio) / 10000); // 만분율
								lWeaponMilliAccuracy = (int)((Int64)lMilliAccuracyinMorale * mTroop.aAffectedGroundStatTable.Get(GroundStat.AccuracyRatio) / 10000); // 만분율
								lWeaponPenetration = (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Penetration) * mTroop.aAffectedGroundStatTable.Get(GroundStat.PenetrationRatio) / 10000); // 만분율
							}
							else
							{
								lWeaponRange = lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Range);
								lWeaponMilliAccuracy = lMilliAccuracyinMorale;
								lWeaponPenetration = lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.Penetration);
							}

							// 예외 처리
							if (lWeaponRange < 0) lWeaponRange = 0;
							if (lWeaponMilliAccuracy < 0) lWeaponMilliAccuracy = 0;
							if (lWeaponPenetration < 0) lWeaponPenetration = 0;

							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.Range, lWeaponRange);
							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.MilliAccuracy, lWeaponMilliAccuracy);
							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.Penetration, lWeaponPenetration);

							// 재장전 시간이 무기 '재장전 시간' x '재장전 시간 보정 비율'로 적용됩니다.
							int lCooldownDelay
								= (int)((Int64)lModifiedWeaponRangeStatTable.Get(WeaponRangeStat.CooldownDelay)
								* mFinalTroopStatTable.Get(TroopStat.CooldownDelayMultiplyRatio)
								/ 10000); // 만분율
							lFinalWeaponRangeStatTable.Set(WeaponRangeStat.CooldownDelay, lCooldownDelay);
						}
					}
				}
				break;
			case TroopMoraleState.PinDown:
				{
					// 이동을 못하게 합니다. BattleTroopAdvanceControl 등에서 TroopMoraleState.PinDown 사용하는 곳 검색

					// 공격을 못하게 합니다. BattleTroopAttackControl 등에서 TroopMoraleState.PinDown 사용하는 곳 검색

					// 거점 점령을 불가능하게 하는 처리는 BattleCheckPoint._OnTryTeamChange()에서 합니다.
				}
				break;
		}

		mMinAttackRangeWeaponData = _GetMinAttackRangeWeaponData();
		mMaxAttackRangeWeaponData = _GetMaxAttackRangeWeaponData();

		mMaxTrackingAngleHalf = 0;
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			WeaponData lWeaponData = mWeaponDatas[iWeapon];
			if (lWeaponData == null)
				continue;
			if (mMaxTrackingAngleHalf < lWeaponData.mFinalWeaponStatTable.Get(WeaponStat.TrackingAngleHalf))
				mMaxTrackingAngleHalf = lWeaponData.mFinalWeaponStatTable.Get(WeaponStat.TrackingAngleHalf);
		}

		// 최적 무기를 찾는 기준이 되는 거리를 얻습니다.
		if (mCommanderTroopSet.mIsSelectBestWeaponOnAttack)
		{
			mSqrBestWeaponCheckRange = mWeaponDatas[0].aSqrMaxAttackRange;
			for (int iWeapon = 1; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			{
				WeaponData lWeaponData = mWeaponDatas[iWeapon];
				if (lWeaponData == null)
					continue;
				if (mSqrBestWeaponCheckRange > lWeaponData.aSqrMaxAttackRange)
					mSqrBestWeaponCheckRange = lWeaponData.aSqrMaxAttackRange;
			}
		}
		else
		{
			mSqrBestWeaponCheckRange = aMaxAttackRange;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate 스탯 변경을 적용합니다.
	private void _OnModifySkillEffectStat(BattleTroopSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aTroopStatTableModifier != null)
		{
			StatTableModifier<TroopStat> lApplyStatTableModifier = pSkillEffect.aTroopStatTableModifier;
			for (int iStat = 0; iStat < lApplyStatTableModifier.aModifyStatCount; ++iStat)
			{
				mModifyTroopStatIdx = lApplyStatTableModifier.aModifyStatIdxs[iStat];
				mModifiedTroopStatTable.Copy(mModifyTroopStatIdx, mBaseTroopStatTable); // 초기화
				mSkillEffectChain.Enumerate(_OnModifySkillEffectTroopStat);
				mFinalTroopStatTable.Copy(mModifyTroopStatIdx, mModifiedTroopStatTable);
			}
		}

		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			if (mWeaponDatas[iWeapon] == null)
				continue;
			mModifyWeaponIdx = iWeapon;

			if (pSkillEffect.aWeaponStatTableModifiers[iWeapon] != null)
			{
				StatTableModifier<WeaponStat> lApplyStatTableModifier = pSkillEffect.aWeaponStatTableModifiers[iWeapon];
				MetaWeaponStatTable lModifiedWeaponStatTable = mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponStatTable;
				WeaponStatTable lFinalWeaponStatTable = mWeaponDatas[mModifyWeaponIdx].mFinalWeaponStatTable;
				for (int iStat = 0; iStat < lApplyStatTableModifier.aModifyStatCount; ++iStat)
				{
					mModifyWeaponStatIdx = lApplyStatTableModifier.aModifyStatIdxs[iStat];
					lModifiedWeaponStatTable.Copy(mModifyWeaponStatIdx, mWeaponDatas[mModifyWeaponIdx].mBaseWeaponStatTable); // 초기화
					mSkillEffectChain.Enumerate(_OnModifySkillEffectWeaponStat);
					lFinalWeaponStatTable.Copy(mModifyWeaponStatIdx, lModifiedWeaponStatTable);
				}
			}

			if (pSkillEffect.aWeaponRangeStatTableModifiers[iWeapon] != null)
			{
				StatTableModifier<WeaponRangeStat> lApplyStatTableModifier = pSkillEffect.aWeaponRangeStatTableModifiers[iWeapon];
				MetaWeaponRangeStatTable[] lModifiedWeaponRangeStatTables = mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponRangeStatTables;
				WeaponRangeStatTable[] lFinalWeaponRangeStatTables = mWeaponDatas[mModifyWeaponIdx].mFinalWeaponRangeStatTables;
				for (int iStat = 0; iStat < lApplyStatTableModifier.aModifyStatCount; ++iStat)
				{
					mModifyWeaponRangeStatIdx = lApplyStatTableModifier.aModifyStatIdxs[iStat];
					for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
						lModifiedWeaponRangeStatTables[iRange].Copy(mModifyWeaponRangeStatIdx, mWeaponDatas[mModifyWeaponIdx].mBaseWeaponRangeStatTables[iRange]); // 초기화
					mSkillEffectChain.Enumerate(_OnModifySkillEffectWeaponRangeStat);
					for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
						lFinalWeaponRangeStatTables[iRange].Copy(mModifyWeaponRangeStatIdx, lModifiedWeaponRangeStatTables[iRange]);
				}
			}

			if (pSkillEffect.aWeaponAoeStatTableModifiers[iWeapon] != null)
			{
				StatTableModifier<WeaponAoeStat> lApplyStatTableModifier = pSkillEffect.aWeaponAoeStatTableModifiers[iWeapon];
				MetaWeaponAoeStatTable[] lModifiedWeaponAoeStatTables = mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponAoeStatTables;
				WeaponAoeStatTable[] lFinalWeaponAoeStatTables = mWeaponDatas[mModifyWeaponIdx].mFinalWeaponAoeStatTables;
				for (int iStat = 0; iStat < lApplyStatTableModifier.aModifyStatCount; ++iStat)
				{
					mModifyWeaponAoeStatIdx = lApplyStatTableModifier.aModifyStatIdxs[iStat];
					for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
						lModifiedWeaponAoeStatTables[iAoe].Copy(mModifyWeaponAoeStatIdx, mWeaponDatas[mModifyWeaponIdx].mBaseWeaponAoeStatTables[iAoe]); // 초기화
					mSkillEffectChain.Enumerate(_OnModifySkillEffectWeaponAoeStat);
					for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
						lFinalWeaponAoeStatTables[iAoe].Copy(mModifyWeaponAoeStatIdx, lModifiedWeaponAoeStatTables[iAoe]);
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifySkillEffectTroopStat(BattleTroopSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aTroopStatTableModifier != null)
			pSkillEffect.aTroopStatTableModifier.ModifyStat(mModifiedTroopStatTable, mModifyTroopStatIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifySkillEffectWeaponStat(BattleTroopSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aWeaponStatTableModifiers[mModifyWeaponIdx] != null)
			pSkillEffect.aWeaponStatTableModifiers[mModifyWeaponIdx].ModifyStat(mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponStatTable, mModifyWeaponStatIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifySkillEffectWeaponRangeStat(BattleTroopSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aWeaponRangeStatTableModifiers[mModifyWeaponIdx] != null)
			for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
				pSkillEffect.aWeaponRangeStatTableModifiers[mModifyWeaponIdx].ModifyStat(mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponRangeStatTables[iRange], mModifyWeaponRangeStatIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// OnEnumerateGuidObjectDelegate
	private void _OnModifySkillEffectWeaponAoeStat(BattleTroopSkillEffect pSkillEffect)
	{
		if (pSkillEffect.aWeaponAoeStatTableModifiers[mModifyWeaponIdx] != null)
			for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
				pSkillEffect.aWeaponAoeStatTableModifiers[mModifyWeaponIdx].ModifyStat(mWeaponDatas[mModifyWeaponIdx].mModifiedWeaponAoeStatTables[iAoe], mModifyWeaponAoeStatIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Queue<BattleTroopStat> sPoolingQueue = new Queue<BattleTroopStat>();

	private int mRefCounter;

	private BattleTroop mTroop;
	private TroopSpec mTroopSpec;
	private CommanderTroopSet mCommanderTroopSet;

	private MetaTroopStatTable mBaseTroopStatTable;
	private MetaTroopStatTable mModifiedTroopStatTable;
	private TroopStatTable mFinalTroopStatTable;

	private WeaponData[] mAllocatedWeaponDatas;
	private WeaponData[] mWeaponDatas;
	private WeaponAttackType mPrimaryWeaponAttackType;
	private WeaponData mMinAttackRangeWeaponData;
	private WeaponData mMaxAttackRangeWeaponData;
	private int mSqrBestWeaponCheckRange;
	private int mMaxTrackingAngleHalf;

	private int mLastSightRange;
	private int mLastAttackRange;

	private int mCurMilliHp;
	private int mCurMilliMorale;
	private int mSuppressionTimer;
	private TroopMoraleState mMoraleState;
	private int mMaxAdvanceSpeed;
	private int mAdvanceSpeed;

	private GuidObjectChain<BattleTroopSkillEffect> mSkillEffectChain;
	private int mModifyTroopStatIdx;
	private int mModifyWeaponIdx;
	private int mModifyWeaponStatIdx;
	private int mModifyWeaponRangeStatIdx;
	private int mModifyWeaponAoeStatIdx;
	private int mBlitzTimer;
	private int mBlitzCoolTimer;
}
