﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageCheckPoint : BattleVerifyStageObject, IStageCheckPoint
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleCheckPoint aBattleCheckPoint
	{
		get { return mBattleCheckPoint; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateCheckPoint(BattleVerifyStage pStage, BattleCheckPoint pBattleCheckPoint)
	{
		OnCreateObject(
			pStage,
			pBattleCheckPoint.aCoord,
			pBattleCheckPoint.aDirectionY);

		mBattleCheckPoint = pBattleCheckPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyCheckPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void ResetTeamChange(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void TryTeamChange(int pTeamIdx, float pChangeFactor)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void UpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void SetSelected(bool pIsSelected, int pAdvanceDirectionId)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void UpdateCommanderCaptureState(BattleCommander pUpdateCommander)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCheckPoint mBattleCheckPoint;
}
