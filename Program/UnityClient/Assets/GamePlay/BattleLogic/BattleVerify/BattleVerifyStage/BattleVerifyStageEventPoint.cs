﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageEventPoint : BattleVerifyStageObject, IStageEventPoint
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleEventPoint aBattleEventPoint
	{
		get { return mBattleEventPoint; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateEventPoint(BattleVerifyStage pStage, BattleEventPoint pBattleEventPoint)
	{
		OnCreateObject(
			pStage,
			pBattleEventPoint.aCoord,
			pBattleEventPoint.aDirectionY);

		mBattleEventPoint = pBattleEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyEventPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void SetSelected(bool pIsSelected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void UpdateCoord(Coord2 pCoord)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void OnTriggerReady()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void OnTriggerActivated()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleEventPoint mBattleEventPoint;
}
