﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleVerifyStageTroop : BattleVerifyStageObject, IStageTroop
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateTroop(BattleVerifyStage pStage, BattleTroop pBattleTroop)
	{
		OnCreateObject(
			pStage,
			pBattleTroop.aCoord,
			pBattleTroop.aDirectionY);

		mBattleTroop = pBattleTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyTroop(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
// 	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
// 	{
// 		return false;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DebugLog(String pLog)
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public void SetSelected(bool pIsSelected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void SetTargetable(bool pIsTargetable)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateTeam(int pTeamIdx)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateDetection(int pDetectingTeamIdx, bool pIsToDetected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateHp(int pCurHp, int pMaxHp, TroopHpChangeType pHpChangeType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateExp()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateVeteranRank(int pRank)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateSightRange()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateAttackRange()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateHiding(bool pIsHiding)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateReturnState(bool pIsFallBehind)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateLeaderState(bool pIsLeader)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateRecruitState()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnHoldMove(bool pIsHoldMove)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopMove(Coord2 pStopCoord)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnMoveTo(Coord2 pCoord, bool pIsArrived)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateBodyToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateBodyWeaponToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateTurretToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnInstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnTrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnWindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnAimWeapon(int pAimDelay)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnEndCooldown()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnGoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnComeBackInteraction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStartSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopSkill(BattleCommanderSkill pCommanderSkill)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStartAction(TroopActionType pActionType)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopAction()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowTroopScript(string pMessageKey, float pDuration)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mBattleTroop;
}
