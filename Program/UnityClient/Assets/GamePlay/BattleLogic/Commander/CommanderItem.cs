using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum CommanderItemType
{
	UserCommander,
	DefenderCommander,
    EventCommander,
    TestCommander, // with no ai
}

public enum CommanderItemState
{
	Wait,
	Spawned,
	Retreat,
}

public class CommanderItem
{
	public int mSlotIdx;

	public CommanderItemType mCommanderItemType;
	public CommanderSpec mCommanderSpec;
	public int mCommanderItemIdx;
	public int mCommanderExpRank;
	public int mCommanderLevel;
	public int mCommanderExp;
	public CommanderTroopSet mTroopSet;
	public CommanderTroopSetItem mTroopSetItem;

	public bool mIsTeamUndefined;
	public bool mIsSpawnAvailable;
	public int mCount;

	public CommanderItemState mCommanderItemState;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 유효성
	public bool aIsValid
	{
		get { return (mCommanderSpec != null); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환 Mp
	public int aSpawnMp
	{
		get { return mCommanderSpec.mSpawnMp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저의 지휘관 아이템 여부
	public bool aIsUserCommanderItem
	{
		get { return (mCommanderItemIdx >= 0); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템의 전투 누적 경험치
	public int aAccumExp
	{
		get { return mAccumExp; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템의 랭크
	public int aRank
	{
		get { return mCommanderSpec.mRank; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderItem(int pSlotIdx)
	{
		mSlotIdx = pSlotIdx;
		mTroopSetItem = new CommanderTroopSetItem();

		mIsTeamUndefined = false;
		mIsSpawnAvailable = true;
		mCount = 0;
		mCommanderItemState = CommanderItemState.Wait;

		mAccumExp = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(CommanderItemType pCommanderItemType, CommanderItemData pItemData)
	{
		Init(pCommanderItemType,
			CommanderTable.get.FindCommanderSpec(pItemData.mCommanderSpecNo),
			pItemData.mCommanderItemIdx,
			pItemData.mCommanderExp,
			pItemData.mCommanderLevel,
			pItemData.mSpawnCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(CommanderItemType pCommanderItemType, CommanderSpec pCommanderSpec, int pIdx, int pExp, int pLevel, int pSpawnCount)
	{
		mCommanderItemType = pCommanderItemType;
		mCommanderItemIdx = pIdx;
		mCommanderSpec = pCommanderSpec;
		mCommanderExpRank = mCommanderSpec.mRank;
		mCommanderLevel = pLevel;
		mCommanderExp = pExp;
		mTroopSet = mCommanderSpec.mRankTroopSets[mCommanderExpRank];
		mTroopSetItem.Init(mTroopSet);
		if (pSpawnCount <= 0)
			mCount = BattleConfig.get.mDefaultCommaderItemCount;
		else
			mCount = pSpawnCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(CommanderItem pCloneCommanderItem)
	{
		Init(
			pCloneCommanderItem.mCommanderItemType,
			pCloneCommanderItem.mCommanderSpec,
			pCloneCommanderItem.mCommanderItemIdx,
			pCloneCommanderItem.mCommanderExp,
			pCloneCommanderItem.mCommanderLevel,
			pCloneCommanderItem.mCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// 커맨드 아이템의 카운트를 줄입니다.
	public void OnSpawnCommander()
	{
		if (mCount <= 0)
		{
#if UNITY_EDITOR
			Debug.LogError("Not Enough Commander Item Count. Please Check Commander Item Count");
#endif
			return;
		}
		mCount--;
		mCommanderItemState = CommanderItemState.Spawned;
	}
	//-------------------------------------------------------------------------------------------------------
	// 커맨드 아이템의 카운트를 줄입니다.
	public void OnDestoryCommander()
	{
		if (mCount <= 0)
			mCommanderItemState = CommanderItemState.Retreat;
		else
			mCommanderItemState = CommanderItemState.Wait;
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환이 가능한 지휘관인지 확인
	public bool IsSpawnAvailable()
	{
		return (mCount > 0) && mIsSpawnAvailable;
	}
	//-------------------------------------------------------------------------------------------------------
	// 재사용을 위한 리셋
	public void Reset()
	{
		mCommanderSpec = null;

		mTroopSetItem.Reset();

		mIsSpawnAvailable = true;
		mCount = 0;

		mAccumExp = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 경험치를 추가합니다.
	public void AddItemExp(int pAddExp)
	{
		mAccumExp += pAddExp;

		if (mAccumExp > BattleConfig.get.mMaxCommanderExpRank[mCommanderExpRank])
		{
			mAccumExp = BattleConfig.get.mMaxCommanderExpRank[mCommanderExpRank];
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 경험치를 업데이트합니다.
	public void UpdateItemExp()
	{
		mCommanderExp += mAccumExp;
		mAccumExp = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 경험치를 업데이트합니다.
	public void UpdateItemExp(int pTotalExp)
	{
		mCommanderExp = pTotalExp;
		mAccumExp = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mAccumExp;
}
