using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public enum CommanderItemCountState
{
	Normal,
	AvailableLevelup,
	MaxCount,
	Count
}

public class CommanderItemData
{
	public int mCommanderItemIdx;
	public int mCommanderSpecNo;
	public int mCommanderExpRank;
	public int mCommanderLevel;
	public int mCommanderExp;
	public int mCommanderItemCount;
	public int mSpawnCount;
	public static Color[] CommanderItemStateColor = { ColorUtil.Color4ub(255, 255, 255, 255), ColorUtil.Color4ub(91, 236, 22, 255), ColorUtil.Color4ub(241, 105, 0, 255) };
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderItemData()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(int pSpecNo)
	{
		mCommanderItemIdx = -1;
		mCommanderSpecNo = pSpecNo;
		mCommanderExpRank = 1;
		mCommanderLevel = 1;
		mCommanderExp = 0;
		mCommanderItemCount = 0;
		mSpawnCount = 1;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(int pItemIdx, int pSpecNo)
	{
		mCommanderItemIdx = pItemIdx;
		mCommanderSpecNo = pSpecNo;
		mCommanderExpRank = 1;
		mCommanderLevel = 1;
		mCommanderExp = 0;
		mCommanderItemCount = 0;
		mSpawnCount = 1;
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템으로 초기화합니다.
	public void InitFromItem(CommanderItem pCommanderItem)
	{
		mCommanderItemIdx = pCommanderItem.mCommanderItemIdx;
		mCommanderSpecNo = pCommanderItem.mCommanderSpec.mNo;
		mCommanderExpRank = pCommanderItem.mCommanderExpRank;
		mCommanderLevel = pCommanderItem.mCommanderLevel;
		mCommanderExp = pCommanderItem.mCommanderExp;
		mCommanderItemCount = 1; // CommanderItem에서는 CommanderItemCount가 의미 없음.
		mSpawnCount = pCommanderItem.mCount;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void InitFromJson(JSONObject pItemDataJson)
	{
		mCommanderItemIdx = (int)pItemDataJson.GetField("ItemId").n;
		mCommanderSpecNo = (int)pItemDataJson.GetField("ItemNo").n;
		mCommanderExpRank = (int)pItemDataJson.GetField("ItemRank").n;
		mCommanderExp = (int)pItemDataJson.GetField("ItemExp").n;
		mCommanderLevel = (int)pItemDataJson.GetField("ItemLevel").n;
		mCommanderItemCount = (int)pItemDataJson.GetField("ItemCount").n; 

		mSpawnCount = 1;
	}
	//-------------------------------------------------------------------------------------------------------
	// JSON 객체를 만들어서 반환합니다.
	public JSONObject ToJson()
	{
		JSONObject lItemDataJson = new JSONObject();

		lItemDataJson.AddField("ItemId", mCommanderItemIdx);
		lItemDataJson.AddField("ItemNo", mCommanderSpecNo);
		lItemDataJson.AddField("ItemRank", mCommanderExpRank);
		lItemDataJson.AddField("ItemExp", mCommanderExp);
		lItemDataJson.AddField("ItemLevel", mCommanderLevel);
		lItemDataJson.AddField("ItemCount", mCommanderItemCount);

		return lItemDataJson;
	}
	//-------------------------------------------------------------------------------------------------------
	// 복사합니다.
	public static void Copy(CommanderItemData pFrom, CommanderItemData pTo)
	{
		pTo.mCommanderItemIdx = pFrom.mCommanderItemIdx;
		pTo.mCommanderSpecNo = pFrom.mCommanderSpecNo;
		pTo.mCommanderExpRank = pFrom.mCommanderExpRank;
		pTo.mCommanderLevel = pFrom.mCommanderLevel;
		pTo.mCommanderExp = pFrom.mCommanderExp;
		pTo.mCommanderItemCount = pFrom.mCommanderItemCount;
		pTo.mSpawnCount = pFrom.mSpawnCount;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
