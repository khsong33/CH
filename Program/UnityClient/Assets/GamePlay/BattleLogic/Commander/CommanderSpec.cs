using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum CommanderType
{
	Infantry,
	Armored,
	Artillery,
}

public enum CommanderClass
{
	SingleFire,
	MachineGun,
	TankGun,
	HeavyArtillery,
	AntiTankGun,
	Support,
	Medic,
	Repair
}

public enum CommanderSpawnType
{
	Headquarter,
	Aircraft,
	Defense,
    TestTool, // 테스트용 스폰
}

public class CommanderSpec
{
	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameKey;
	public String mDescKey;
	public String mEffectiveKey;
	public AssetKey mIconAssetKey;
	public AssetKey mMiniMapIconAssetKey;
	public AssetKey mMiniMapSelectIconAssetKey;
	public AssetKey mMarkIconAssetKey;
	public AssetKey mPortraitIconAssetKey;

	public DeckNation mDeckNation;
	public CommanderType mType;
	public CommanderClass mClass;
	public int mSpawnMp;
	public int mSummonOrder;
	public int[] mVeterancyExps;
	public CommanderSpawnType mSpawnType;
	public int mRank;

	public CommanderTroopSet[] mRankTroopSets;

	public SkillSpec[] mSkillSpecs;

	public int[] mRankNeedExps;

	public RankEffectSpec[] mRankEffectSpecs;

	// 파생 정보
	public const int cTypeEnumCount = 3;
	public const int cRankCount = 6; // 예외 0 + 1~5 랭크
	public const int cSkillCount = 1;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<CommanderSpec[{0}] Idx={1} Name={2}>", mIdx, mNo, mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(CommanderSpec pCommanderSpec)
	{
		if ((pCommanderSpec != null) &&
			(pCommanderSpec.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 미니맵 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetMiniMapIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mMiniMapIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 미니맵에서 선택된 상태의 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetMiniMapSelectIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mMiniMapSelectIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 마크 아이콘 텍스쳐를 얻어옵니다.
#if !BATTLE_VERIFY
	public Texture GetMarkIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mMarkIconAssetKey);
	}
#endif
#if !BATTLE_VERIFY
	public Texture GetPortraitIconTexture()
	{
		return TextureResourceManager.get.LoadTexture(mPortraitIconAssetKey);
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 다음 베테랑 레벨에 필요한 경험치를 얻어옵니다.
	public int GetNextVeterancyExp(int pCurrentVeteracyRank)
	{
		return mVeterancyExps[pCurrentVeteracyRank];
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
		DebugUtil.Assert(CommanderSpec.cTypeEnumCount == Enum.GetValues(typeof(CommanderType)).Length, "CommanderSpec.cTypeEnumCount mismatch");
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
