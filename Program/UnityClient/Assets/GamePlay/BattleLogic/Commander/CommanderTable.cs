using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "CommanderTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameKeyColumnName = "NameKey";
	public readonly String vDescTextColumnName = "DescKey";
	public readonly String vIconBundleColumnName = "IconBundle";
	public readonly String vIconPathColumnName = "IconPath";
	public readonly String vMiniMapIconPathColumnName = "MiniMapIconPath";
	public readonly String vMarkIconPathColumnName = "MarkerIconPath";
	public readonly String vPortraitIconPathColumnName = "PortraitIconPath";

	public readonly String vNationColumnName = "Nation";
	public readonly String vTypeColumnName = "Type";
	public readonly String vClassColumnName = "Class";
	public readonly String vSpawnMpColumnName = "SpawnMp";
	public readonly String vSummonOrderColumnName = "SummonOrder";
	public readonly String vVeterancyExpColumnName = "VeterancyExp";
	public readonly String vSpawnTypeColumnName = "SpawnType";
	public readonly String vRankColumnName = "Rank";
	public readonly String vEffectKeyColumnName = "EffectiveKey";

	public readonly String[] vRankTroopSetColumnNames = { "", "Rank1TroopSet", "Rank2TroopSet", "Rank3TroopSet", "Rank4TroopSet", "Rank5TroopSet" };

	public readonly String[] vSkillColumnNames = { "Skill1", "Skill2", "Skill3", "Skill4", "Skill5" };

	public readonly String vRankNeedExpConlumnName = "RankNeedExp";

	public readonly String[] vRankEffectColumnNames = { "Rank2Effect", "Rank3Effect", "Rank4Effect", "Rank5Effect" };

	public readonly bool vIsPreLoadAll = false;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CommanderTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CommanderTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		CommanderSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 스펙 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lCommanderName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lCommanderName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 부대 스펙 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("CommanderTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lCommanderName))
			{
				Debug.LogError(String.Format("CommanderTable[{0}] duplicate Name '{1}'", lSpecNo, lCommanderName));
				continue;
			}
			mNameToRowDictionary.Add(lCommanderName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mDeckNationDictionary = new Dictionary<String, DeckNation>();
		foreach (DeckNation iForm in Enum.GetValues(typeof(DeckNation)))
			mDeckNationDictionary.Add(iForm.ToString(), iForm);

		mCommanderTypeDictionary = new Dictionary<String, CommanderType>();
		foreach (CommanderType iForm in Enum.GetValues(typeof(CommanderType)))
			mCommanderTypeDictionary.Add(iForm.ToString(), iForm);

		mCommanderClassDictionary = new Dictionary<string, CommanderClass>();
		foreach (CommanderClass iForm in Enum.GetValues(typeof(CommanderClass)))
			mCommanderClassDictionary.Add(iForm.ToString(), iForm);

		mCommanderSpawnTypeDictionary = new Dictionary<String, CommanderSpawnType>();
		foreach (CommanderSpawnType iForm in Enum.GetValues(typeof(CommanderSpawnType)))
			mCommanderSpawnTypeDictionary.Add(iForm.ToString(), iForm);

		// 스펙 테이블을 초기화합니다.
		mCommanderSpecs = new CommanderSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllCommanderSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllCommanderSpec()
	{
		Debug.LogWarning("LoadAllCommanderSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mCommanderSpecs[iRow] == null) // _SetUpSpawnLinkCommanderInfo() 속에서 초기화되었을 수도 있습니다.
				mCommanderSpecs[iRow] = _LoadCommanderSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public CommanderSpec GetCommanderSpecByIdx(int pCommanderIdx)
	{
		if (mCommanderSpecs[pCommanderIdx] == null)
			mCommanderSpecs[pCommanderIdx] = _LoadCommanderSpec(mCSVObject.GetRow(pCommanderIdx), pCommanderIdx);
		return mCommanderSpecs[pCommanderIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public CommanderSpec FindCommanderSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (mCommanderSpecs[lRowIdx] == null)
				mCommanderSpecs[lRowIdx] = _LoadCommanderSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mCommanderSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public CommanderSpec FindCommanderSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mCommanderSpecs[lRowIdx] == null)
				mCommanderSpecs[lRowIdx] = _LoadCommanderSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mCommanderSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 에넘값을 찾습니다.
	public DeckNation FindDeckNation(String pDeckNationString)
	{
		DeckNation lDeckNation;
		if (!mDeckNationDictionary.TryGetValue(pDeckNationString, out lDeckNation))
		{
			Debug.LogError("invalid DeckNation:" + pDeckNationString);
			return DeckNation.US;
		}
		return lDeckNation;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 에넘값을 찾습니다.
	public CommanderSpawnType FindCommanderSpawnType(String pCommanderSpawnTypeString)
	{
		CommanderSpawnType lCommanderSpawnType;
		if (!mCommanderSpawnTypeDictionary.TryGetValue(pCommanderSpawnTypeString, out lCommanderSpawnType))
		{
			Debug.LogError("invalid CommanderSpawnType:" + pCommanderSpawnTypeString);
			return CommanderSpawnType.Headquarter;
		}
		return lCommanderSpawnType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mCommanderSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private CommanderSpec _LoadCommanderSpec(CSVRow pCSVRow, int pRowIdx)
	{
		CommanderSpec lCommanderSpec = new CommanderSpec();

		// 줄 인덱스
		lCommanderSpec.mIdx = pRowIdx;

		// Name
		lCommanderSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lCommanderSpec.mName);

		// SpecNo
		lCommanderSpec.mNo = Int32.Parse(pCSVRow.GetStringValue(vSpecNoColumnName));

		// NameKey
		lCommanderSpec.mNameKey = pCSVRow.GetStringValue(vNameKeyColumnName);

		// DescKey
		lCommanderSpec.mDescKey = pCSVRow.GetStringValue(vDescTextColumnName);

		// IconBundle, IconPath, MiniMapIconPath
		lCommanderSpec.mIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vIconPathColumnName));
		lCommanderSpec.mMiniMapIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vMiniMapIconPathColumnName));
		lCommanderSpec.mMiniMapSelectIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vMiniMapIconPathColumnName) + "_Select");
		lCommanderSpec.mMarkIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vMarkIconPathColumnName));
		lCommanderSpec.mPortraitIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vPortraitIconPathColumnName));

		// Nation
		String lNationString = pCSVRow.GetStringValue(vNationColumnName);
		if (!mDeckNationDictionary.TryGetValue(lNationString, out lCommanderSpec.mDeckNation))
			Debug.LogError(String.Format("CommanderTable[{0}] invalid Nation '{1}'", lCommanderSpec.mNo, lNationString));

		// Type
		String lTypeString = pCSVRow.GetStringValue(vTypeColumnName);
		if (!mCommanderTypeDictionary.TryGetValue(lTypeString, out lCommanderSpec.mType))
			Debug.LogError(String.Format("CommanderTable[{0}] invalid Type '{1}'", lCommanderSpec.mNo, lTypeString));

		// Class
		String lClassString = pCSVRow.GetStringValue(vClassColumnName);
		if(!mCommanderClassDictionary.TryGetValue(lClassString, out lCommanderSpec.mClass))
			Debug.LogError(String.Format("CommanderTable[{0}] invalid Class '{1}'", lCommanderSpec.mNo, lClassString));

		// SpawnMp
		pCSVRow.TryIntValue(vSpawnMpColumnName, out lCommanderSpec.mSpawnMp, 0); // 기본값 0
// 테스트로 바로 소환하려면
//		lCommanderSpec.mSpawnMp = 0;
		
		// SummonOrder
		pCSVRow.TryIntValue(vSummonOrderColumnName, out lCommanderSpec.mSummonOrder, 1); // 기본값 1

		// VerterancyExp
		String[] lExps = pCSVRow.GetStringValue(vVeterancyExpColumnName).Split('^');
		lCommanderSpec.mVeterancyExps = new int[lExps.Length];
		for (int iExp = 0; iExp < lExps.Length; iExp++)
			int.TryParse(lExps[iExp], out lCommanderSpec.mVeterancyExps[iExp]);

		// SpawnType
		String lSpawnTypeString = pCSVRow.GetStringValue(vSpawnTypeColumnName);
		lCommanderSpec.mSpawnType = CommanderTable.get.FindCommanderSpawnType(lSpawnTypeString);

		//Rank
		lCommanderSpec.mRank = pCSVRow.GetIntValue(vRankColumnName);
		
		// EffectiveKey
		lCommanderSpec.mEffectiveKey = pCSVRow.GetStringValue(vEffectKeyColumnName);

		// mRankTroopSets
		lCommanderSpec.mRankTroopSets = new CommanderTroopSet[CommanderSpec.cRankCount];
		for (int iRank = 1; iRank < CommanderSpec.cRankCount; ++iRank)
		{
			String lRankTroopSetName = pCSVRow.GetStringValue(vRankTroopSetColumnNames[iRank]);
			lCommanderSpec.mRankTroopSets[iRank] = CommanderTroopTable.get.FindCommanderTroopSet(lRankTroopSetName);
		}
		lCommanderSpec.mRankTroopSets[0] = lCommanderSpec.mRankTroopSets[1]; // 0 랭크는 1 랭크로 간주

		// mSkillSpecs
		lCommanderSpec.mSkillSpecs = new SkillSpec[CommanderSpec.cSkillCount];
		for (int iSkill = 0; iSkill < CommanderSpec.cSkillCount; ++iSkill)
		{
			String lSkillName;
			if (pCSVRow.TryStringValue(vSkillColumnNames[iSkill], out lSkillName, String.Empty)) // 기본값 빈 문자열
				lCommanderSpec.mSkillSpecs[iSkill] = SkillTable.get.FindSkillSpec(lSkillName);
		}

		String[] lNeedExps = pCSVRow.GetStringValue(vRankNeedExpConlumnName).Split('^');
		lCommanderSpec.mRankNeedExps = new int[CommanderSpec.cRankCount];
		for (int iRank = 1; iRank < CommanderSpec.cRankCount; iRank++)
		{
			int.TryParse(lNeedExps[iRank-1], out lCommanderSpec.mRankNeedExps[iRank]);
		}

		// RankEffect는 Rank2부터 시작합니다.
		// Rank Effects 
		lCommanderSpec.mRankEffectSpecs = new RankEffectSpec[CommanderSpec.cRankCount];
		for (int iRank = 2; iRank < CommanderSpec.cRankCount; ++iRank)
		{
			String lRankEffectName;
			if (pCSVRow.TryStringValue(vRankEffectColumnNames[iRank - 2], out lRankEffectName, String.Empty))
			{
				lCommanderSpec.mRankEffectSpecs[iRank] = RankEffectTable.get.FindRankEffectSpec(lRankEffectName);
			}
		}
		return lCommanderSpec; 
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CommanderTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, DeckNation> mDeckNationDictionary;
	private Dictionary<String, CommanderType> mCommanderTypeDictionary;
	private Dictionary<String, CommanderClass> mCommanderClassDictionary;
	private Dictionary<String, CommanderSpawnType> mCommanderSpawnTypeDictionary;

	private CommanderSpec[] mCommanderSpecs;
}
