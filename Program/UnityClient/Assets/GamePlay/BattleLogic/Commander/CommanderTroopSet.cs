using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum CommanderTroopType
{
	Default,
	Npc,
}

public class CommanderTroopInfo
{
	public TroopSpec mTroopSpec;
	public CommanderTroopType mCommanderTroopType;
}

public class CommanderTroopKindCountInfo
{
	public TroopSpec mTroopSpec;
	public AssetKey mTroopIconAssetKey;
	public int mTroopCount;

	public CommanderTroopKindCountInfo()
	{
		mTroopSpec = null;
		mTroopIconAssetKey = null;
		mTroopCount = 0;
	}
}

public class CommanderTroopSet
{
	public int mIdx;

	public int mNo;
	public String mName;

	public int mTroopCount;
	public CommanderTroopInfo[] mTroopInfos;
	public Coord2[] mFormationPositionOffsets;
	public int[] mFormationGroupIds;
	public int[] mFirstGroupIdxs;
	public int[] mSecondGroupIdxs;
	//public int mSpawnMp;
	public int mSpawnCoolTime;

	public bool mIsFollowAdvance;

	public int mTroopKindCount;
	public CommanderTroopKindCountInfo[] mTroopKindCountInfos;

	public int mMaxMoraleMultiplyRatio;
	public int mSuppressionMultiplyRatio;
	public int mMinMorale;
	public int[] mAllySightWeaponAccuracyMultiplyRatios;
	public int[] mAllySightWeaponScatterMultiplyRatios;
	public int mSkillReadyDelayMultiplyRatio;
	public int mSkillUseDelayMultiplyRatio;
	public int mSkillCooldownDelayMultiplyRatio;
	public int mGainVeterancyMultiplyRatio;
	public int mStartVeterancyRank;
	public bool mIsHidingMoveOnHit;
	public bool mIsConcentratedFire;
	public bool mIsSelectBestWeaponOnAttack;

	// 파생 정보
	public int mSqrGroupingRange;

	public const int cMaxTroopCount = 6;
	public const int cMaxTroopKindCount = 2;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<CommanderTroopSet[{0}] Idx={1} Name={2}>", mIdx, mNo, mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 세트인지 검사합니다.
	public static bool IsValid(CommanderTroopSet pCommanderTroopSet)
	{
		if ((pCommanderTroopSet != null) &&
			(pCommanderTroopSet.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
