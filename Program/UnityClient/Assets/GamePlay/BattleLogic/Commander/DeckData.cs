using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DeckData
{
	public int mDeckIdx;
	public DeckNation mDeckNation;
	public String mDeckName;
	public CommanderItemData[] mDeckItems;
	public DeckData()
	{
		mDeckIdx = -1;
		mDeckNation = DeckNation.None;
		mDeckName = String.Empty;
		mDeckItems = new CommanderItemData[BattleUserInfo.cMaxCommanderItemCount];
		
		mDicFindSlotIdFromItemIdx = new Dictionary<int, int>();
	}
	public static void Copy(DeckData pFrom, DeckData pTo)
	{
		pTo.mDeckIdx = pFrom.mDeckIdx;
		pTo.mDeckNation = pFrom.mDeckNation;
		pTo.mDeckName = pFrom.mDeckName;

		pTo.mDeckItems = new CommanderItemData[pFrom.mDeckItems.Length]; // Deck Count
		pTo.ResetData();
		for (int iDeckItem = 0; iDeckItem < pFrom.mDeckItems.Length; iDeckItem++)
		{
			if (pFrom.mDeckItems[iDeckItem] != null) 
			{
				pTo.mDeckItems[iDeckItem] = new CommanderItemData();
				CommanderItemData lTempCommanderItemData = new CommanderItemData();
				CommanderItemData.Copy(pFrom.mDeckItems[iDeckItem], lTempCommanderItemData);
				pTo.SetDeckItem(lTempCommanderItemData, iDeckItem);
			}
			else
			{
				pTo.SetDeckItem(null, iDeckItem);
			}
		}
	}
	public void ResetData()
	{
		mDicFindSlotIdFromItemIdx.Clear();
		for (int iCommanderItem = 0; iCommanderItem < BattleUserInfo.cMaxCommanderItemCount; iCommanderItem++)
		{
			mDeckItems[iCommanderItem] = null;
		}
	}
	public void SetDeckItem(CommanderItemData pData, int pSlotId)
	{
		if (pData == null)
		{
			if (mDeckItems[pSlotId] != null && mDicFindSlotIdFromItemIdx.ContainsKey(mDeckItems[pSlotId].mCommanderItemIdx))
			{
				mDicFindSlotIdFromItemIdx.Remove(mDeckItems[pSlotId].mCommanderItemIdx);
			}
		}
		else
		{
			mDicFindSlotIdFromItemIdx.Add(pData.mCommanderItemIdx, pSlotId);
		}
		mDeckItems[pSlotId] = pData;
	}
	public int FindSlotIdFromItemIdx(int pItemIdx)
	{
		if(mDicFindSlotIdFromItemIdx.ContainsKey(pItemIdx))
		{
			return mDicFindSlotIdFromItemIdx[pItemIdx];
		}
		return -1;
	}
	public int GetCommanderCount()
	{
		int lReslutCommanderCount = 0;
		for (int iCommander = 0; iCommander < mDeckItems.Length; iCommander++)
		{
			if (mDeckItems[iCommander] != null && mDeckItems[iCommander].mCommanderItemIdx >= 0)
				lReslutCommanderCount++;
		}
		return lReslutCommanderCount;
	}
	private Dictionary<int, int> mDicFindSlotIdFromItemIdx;
}
