using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum GroundEffectType
{
	SandBag,
	Trench,
	Smoke,
}

public class GroundEffectSpec
{
	public int mIndex;

	public String mName;
	public String mNameText;

	public String mEffectType;

	public StatTableModifier<GroundStat> mGroundStatTableModifier;
	public GroundArmorSpec mGroundArmorSpec;

	public const int cGroundEffectTypeCount = 3;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<GroundEffectSpec[{0}]>", mName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(GroundEffectSpec pGroundEffectSpec)
	{
		if (pGroundEffectSpec != null)
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
