using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GroundEffectTable
{
	public readonly String cCSVTextAssetPath = "GroundEffectTableCSV";

	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";

	public readonly String vEffectTypeColumnName = "EffectType";

	public readonly String vEffectAngleOverwriteColumnName = "EffectAngleOverwrite";
	public readonly String vTSightRangeAddOverwriteColumnName = "T_SightRangeAddOverwrite";
	public readonly String vTAdvanceSpeedMultiplierOverwriteColumnName = "T_AdvanceSpeedMultiplierOverwrite";
	public readonly String vIsHidingOverwriteColumnName = "T_HidingOverwrite";
	public readonly String vTArmorSizeOverwriteColumnName = "T_ArmorSizeOverwrite";
	public readonly String vTArmorAddOverwriteColumnName = "T_ArmorAddOverwrite";
	public readonly String vASuppressionMultiplierOverwriteColumnName = "A_SuppressionMultiplierOverwrite";
	public readonly String vWDistanceMultiplierOverwriteColumnName = "W_DistanceMultiplierOverwrite";
	public readonly String vWAccuracyMultiplierOverwriteColumnName = "W_AccuracyMultiplierOverwrite";
	public readonly String vWPenetrationMultiplierOverwriteColumnName = "W_PenetrationMultiplierOverwrite";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static GroundEffectTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new GroundEffectTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public GroundEffectTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		DebugUtil.Assert(GroundEffectSpec.cGroundEffectTypeCount == Enum.GetValues(typeof(GroundEffectType)).Length, "cGroundEffectTypeCount mismatch - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		GroundEffectSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TypeTable[{0}] duplicate Name '{1}'", lSpecName, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mGroundEffectTypeDictionary = new Dictionary<String, GroundEffectType>();
		foreach (GroundEffectType iForm in Enum.GetValues(typeof(GroundEffectType)))
			mGroundEffectTypeDictionary.Add(iForm.ToString(), iForm);

		// 스펙 테이블을 초기화합니다.
		mGroundEffectSpecs = new GroundEffectSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllGroundEffectType();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 정보를 모두 읽어옵니다.
	public void LoadAllGroundEffectType()
	{
		// Debug.LogWarning("LoadAllGroundEffectType() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mGroundEffectSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mGroundEffectSpecs[iRow] = _LoadGroundEffectSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public GroundEffectSpec FindGroundEffectSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIndex;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIndex))
		{
			if (mGroundEffectSpecs[lRowIndex] == null)
				mGroundEffectSpecs[lRowIndex] = _LoadGroundEffectSpec(mCSVObject.GetRow(lRowIndex), lRowIndex);
			return mGroundEffectSpecs[lRowIndex];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName);
			return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("GroundEffectTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private GroundEffectSpec _LoadGroundEffectSpec(CSVRow pCSVRow, int pRowIndex)
	{
		GroundEffectSpec lGroundEffectSpec = new GroundEffectSpec();

		// 줄 인덱스
		lGroundEffectSpec.mIndex = pRowIndex;

		// Name
		lGroundEffectSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lGroundEffectSpec.mName);

		// NameText
		lGroundEffectSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// EffectType
		lGroundEffectSpec.mEffectType = pCSVRow.GetStringValue(vEffectTypeColumnName);

		// mGroundStatTableModifier
		lGroundEffectSpec.mGroundStatTableModifier = new StatTableModifier<GroundStat>();

		StatTableModifier<GroundStat> lGroundStatTableModifier = lGroundEffectSpec.mGroundStatTableModifier;
		{
			int lModifyIntValue;
			bool lModifyBoolValue;

			// T_SightRangeAddOverwrite
			if (pCSVRow.TryIntValue(vTSightRangeAddOverwriteColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lGroundStatTableModifier.AddModifyIntStat(GroundStat.SightRangeAdd, StatModifyType.Overwrite, lModifyIntValue);

			// T_AdvanceSpeedMultiplierOverwrite
			if (pCSVRow.TryRatioValue(vTAdvanceSpeedMultiplierOverwriteColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lGroundStatTableModifier.AddModifyIntStat(GroundStat.AdvanceSpeedRatio, StatModifyType.Overwrite, lModifyIntValue);

			// IsHidingOverwrite
			if (pCSVRow.TryBoolValue(vIsHidingOverwriteColumnName, out lModifyBoolValue, false)) // 기본값 false(의미 없음)
				lGroundStatTableModifier.AddModifyBoolStat(GroundStat.IsHiding, StatModifyType.Overwrite, lModifyBoolValue);

			// W_DistanceMultiplier
			if (pCSVRow.TryRatioValue(vWDistanceMultiplierOverwriteColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lGroundStatTableModifier.AddModifyIntStat(GroundStat.WeaponRangeRatio, StatModifyType.Overwrite, lModifyIntValue);

			// W_AccuracyMultiplier
			if (pCSVRow.TryRatioValue(vWAccuracyMultiplierOverwriteColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lGroundStatTableModifier.AddModifyIntStat(GroundStat.AccuracyRatio, StatModifyType.Overwrite, lModifyIntValue);

			// W_PenetrationMultiplier
			if (pCSVRow.TryRatioValue(vWPenetrationMultiplierOverwriteColumnName, out lModifyIntValue, 0)) // 기본값 0(의미 없음)
				lGroundStatTableModifier.AddModifyIntStat(GroundStat.PenetrationRatio, StatModifyType.Overwrite, lModifyIntValue);
		}

		// mGroundArmorSpec
		int lEffectAngle;
		int lArmorSize;
		int lArmorAdd;
		int lSuppressionRatio;

		pCSVRow.TryIntValue(vEffectAngleOverwriteColumnName, out lEffectAngle, 0);
		pCSVRow.TryIntValue(vTArmorSizeOverwriteColumnName, out lArmorSize, 0); // 기본값 0(의미 없음)
		pCSVRow.TryIntValue(vTArmorAddOverwriteColumnName, out lArmorAdd, 0); // 기본값 0(의미 없음)
		pCSVRow.TryRatioValue(vASuppressionMultiplierOverwriteColumnName, out lSuppressionRatio, 10000); // 기본값 10000(100% 그대로이므로 의미 없음)

		int lEffectAngleHalf = lEffectAngle / 2; // Half

		bool lIsValidArmorAdd = (lArmorSize > 0) && (lArmorAdd > 0);
		if (!lIsValidArmorAdd &&
			((lArmorSize > 0) || (lArmorAdd > 0)))
			Debug.LogWarning("inconsistent armor values - " + lGroundEffectSpec.mNameText);
		bool lIsValidArmor = lIsValidArmorAdd || (lSuppressionRatio != 10000);
		if ((lEffectAngleHalf <= 0) && lIsValidArmor)
			Debug.LogWarning("inconsistent armor values - " + lGroundEffectSpec.mNameText);

		if ((lEffectAngleHalf > 0) &&
			lIsValidArmor) // 의미있는 경우에만 추가
		{
			lGroundEffectSpec.mGroundArmorSpec = new GroundArmorSpec();

			// mEffectAngleHalf
			lGroundEffectSpec.mGroundArmorSpec.mEffectAngleHalf = lEffectAngleHalf;

			// mArmorSize
			lGroundEffectSpec.mGroundArmorSpec.mArmorSize = lArmorSize;

			// mArmorAdd
			lGroundEffectSpec.mGroundArmorSpec.mArmorAdd = lArmorAdd;

			// mSuppressionRatio
			lGroundEffectSpec.mGroundArmorSpec.mSuppressionRatio = lSuppressionRatio;
		}

		return lGroundEffectSpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static GroundEffectTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, GroundEffectType> mGroundEffectTypeDictionary;

	private GroundEffectSpec[] mGroundEffectSpecs;
}
