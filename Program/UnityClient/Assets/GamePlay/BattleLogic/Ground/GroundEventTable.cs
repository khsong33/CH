﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GroundEventTable
{
	public readonly String cCSVTextAssetPath = "GroundEventTableCSV";

	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";

	public readonly String vStageEventObjectBundleColumnName = "StageEventObjectBundle";
	public readonly String vStageEventObjectPathColumnName = "StageEventObjectPath";
	public readonly String vMiniMapIconBundleColumnName = "MiniMapIconBundle";
	public readonly String vMiniMapIconPathColumnName = "MiniMapIconPath";

	public readonly String vFunctionColumnName = "Function";
	public readonly String[] vIntValueColumnNames = { "IntValue0", "IntValue1", "IntValue2", "IntValue3", "IntValue4" };
	public readonly String[] vRatioValueColumnNames = { "RatioValue0", "RatioValue1", "RatioValue2", "RatioValue3", "RatioValue4" };
	public readonly String[] vStringValueColumnNames = { "StringValue0", "StringValue1", "StringValue2", "StringValue3", "StringValue4" };
	public readonly String[] vBoolValueColumnNames = { "BoolValue0", "BoolValue1", "BoolValue2", "BoolValue3", "BoolValue4" };

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static GroundEventTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new GroundEventTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public GroundEventTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		DebugUtil.Assert(GroundEventSpec.cGroundEventFunctionCount == Enum.GetValues(typeof(GroundEventFunction)).Length, "cGroundEventFunctionCount mismatch - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		TroopSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TypeTable[{0}] duplicate Name '{1}'", lSpecName, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mGroundEventFunctionDictionary = new Dictionary<String, GroundEventFunction>();
		foreach (GroundEventFunction iEnum in Enum.GetValues(typeof(GroundEventFunction)))
			mGroundEventFunctionDictionary.Add(iEnum.ToString(), iEnum);

		// 스펙 테이블을 초기화합니다.
		mLoadingSpecs = new GroundEventSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllGroundEventFunction();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 정보를 모두 읽어옵니다.
	public void LoadAllGroundEventFunction()
	{
		// Debug.LogWarning("LoadAllGroundEventFunction() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mLoadingSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mLoadingSpecs[iRow] = _LoadGroundEventSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public GroundEventSpec FindGroundEventSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIndex;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIndex))
		{
			if (mLoadingSpecs[lRowIndex] == null)
				mLoadingSpecs[lRowIndex] = _LoadGroundEventSpec(mCSVObject.GetRow(lRowIndex), lRowIndex);
			return mLoadingSpecs[lRowIndex];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("GroundEventTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private GroundEventSpec _LoadGroundEventSpec(CSVRow pCSVRow, int pRowIndex)
	{
		GroundEventSpec lGroundEventSpec = new GroundEventSpec();

		// 줄 인덱스
		lGroundEventSpec.mIndex = pRowIndex;

		// Name
		lGroundEventSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lGroundEventSpec.mName);

		// NameKey
		lGroundEventSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// StageEventObjectBundle, StageEventObjectPath
		String lStageEventObjectBundleString;
		String lStageEventObjectPathString;
		if (pCSVRow.TryStringValue(vStageEventObjectBundleColumnName, out lStageEventObjectBundleString, String.Empty) &&
			pCSVRow.TryStringValue(vStageEventObjectPathColumnName, out lStageEventObjectPathString, String.Empty))
			lGroundEventSpec.mStageEventObjectAssetKey = AssetManager.get.CreateAssetKey(lStageEventObjectBundleString, lStageEventObjectPathString);

		// MiniMapIconBundle, MiniMapIconPath
		String lMiniMapIconBundleString;
		String lMiniMapIconPathString;
		if (pCSVRow.TryStringValue(vMiniMapIconBundleColumnName, out lMiniMapIconBundleString, String.Empty) &&
			pCSVRow.TryStringValue(vMiniMapIconPathColumnName, out lMiniMapIconPathString, String.Empty))
			lGroundEventSpec.mMiniMapIconAssetKey = AssetManager.get.CreateAssetKey(lMiniMapIconBundleString, lMiniMapIconPathString);

		// Function
		String lFunctionString;
		if (pCSVRow.TryStringValue(vFunctionColumnName, out lFunctionString, String.Empty)) // 기본값 빈 문자열
		{
			if (!mGroundEventFunctionDictionary.TryGetValue(lFunctionString, out lGroundEventSpec.mFunction))
				Debug.LogError(String.Format("GroundEventTable[{0}] invalid Function '{1}'", lGroundEventSpec.mName, lFunctionString));

			switch (lGroundEventSpec.mFunction)
			{
			case GroundEventFunction.MedKit:
				_LoadMedKitParam(pCSVRow, lGroundEventSpec);
				break;
			case GroundEventFunction.Munition:
				_LoadMunitionParam(pCSVRow, lGroundEventSpec);
				break;
			case GroundEventFunction.Commander:
				_LoadCommanderParam(pCSVRow, lGroundEventSpec);
				break;
			case GroundEventFunction.RepairKit:
				_LoadRepairKitParam(pCSVRow, lGroundEventSpec);
				break;
			default:
				Debug.LogError("not implemented vFunctionColumnName:" + lFunctionString);
				lGroundEventSpec.mFunction = GroundEventFunction.None;
				break;
			}
		}
		else
			lGroundEventSpec.mFunction = GroundEventFunction.None;

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lGroundEventSpec);

		return lGroundEventSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(GroundEventSpec pGroundEventSpec)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadMedKitParam(CSVRow pCSVRow, GroundEventSpec pGroundEventSpec)
	{
		GroundEventSpec.MedKitParam lFunctionParam = new GroundEventSpec.MedKitParam();
		pGroundEventSpec.mMedKitParam = lFunctionParam;

		// RatioValue0 : HP 회복 비율
		lFunctionParam.mHpRecoveryRatio = pCSVRow.GetRatioValue(vRatioValueColumnNames[0]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadMunitionParam(CSVRow pCSVRow, GroundEventSpec pGroundEventSpec)
	{
		GroundEventSpec.MunitionParam lFunctionParam = new GroundEventSpec.MunitionParam();
		pGroundEventSpec.mMunitionParam = lFunctionParam;

		// IntValue0 : MP 상승값
		lFunctionParam.mMpIncrement = pCSVRow.GetIntValue(vIntValueColumnNames[0]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadCommanderParam(CSVRow pCSVRow, GroundEventSpec pGroundEventSpec)
	{
		GroundEventSpec.CommanderParam lFunctionParam = new GroundEventSpec.CommanderParam();
		pGroundEventSpec.mCommanderParam = lFunctionParam;

		// IntValue1 : 팀 번호
		pCSVRow.TryIntValue(vIntValueColumnNames[0], out lFunctionParam.mTeamIdx, 0);

		// StringValue0 : 국가 정보
		String lDeckNationString = pCSVRow.GetStringValue(vStringValueColumnNames[0]);
		lFunctionParam.mDeckNation = CommanderTable.get.FindDeckNation(lDeckNationString);

		// StringValue1 : 지휘관 정보
		String lSpawnCommanderString = pCSVRow.GetStringValue(vStringValueColumnNames[1]);
		lFunctionParam.mSpawnCommanderSpec = CommanderTable.get.FindCommanderSpec(lSpawnCommanderString);

		// BoolValue0 : 자동 팀 갱신
		lFunctionParam.mIsAutoTeamUpdate = pCSVRow.GetBoolValue(vBoolValueColumnNames[0]);

		// BoolValue1 : AI 작동 여부
//		lFunctionParam.mIsAi = pCSVRow.GetBoolValue(vBoolValueColumnNames[1]);
		lFunctionParam.mIsAi = lFunctionParam.mIsAutoTeamUpdate; // 현재는 자동 팀 갱신 여부를 따라감
	}
	//-------------------------------------------------------------------------------------------------------
	// 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadRepairKitParam(CSVRow pCSVRow, GroundEventSpec pGroundEventSpec)
	{
		GroundEventSpec.RepairKitParam lFunctionParam = new GroundEventSpec.RepairKitParam();
		pGroundEventSpec.mRepairKitParam = lFunctionParam;

		// RatioValue0 : HP 회복 비율
		lFunctionParam.mHpRecoveryRatio = pCSVRow.GetRatioValue(vRatioValueColumnNames[0]);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static GroundEventTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, GroundEventFunction> mGroundEventFunctionDictionary;

	private GroundEventSpec[] mLoadingSpecs;
}
