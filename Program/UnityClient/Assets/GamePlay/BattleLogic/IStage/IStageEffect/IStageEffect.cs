using UnityEngine;
using System.Collections;
using System;

public interface IStageEffect
{
	// 스테이지 인터페이스
	IStage aIStage { get; }

	// 파괴합니다.
	void DestroyEffect();
}
