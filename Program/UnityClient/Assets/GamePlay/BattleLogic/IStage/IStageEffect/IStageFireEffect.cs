using UnityEngine;
using System.Collections;
using System;

public interface IStageFireEffect : IStageEffect
{
	void Activate(Coord2 pActivationCoord);
}
