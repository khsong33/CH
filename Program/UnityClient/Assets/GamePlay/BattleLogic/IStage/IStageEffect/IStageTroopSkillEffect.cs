using UnityEngine;
using System.Collections;
using System;

public interface IStageTroopSkillEffect : IStageEffect
{
	void Activate();
}
