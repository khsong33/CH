using UnityEngine;
using System.Collections;
using System;

public interface IStageCheckPoint : IStageObject
{
	// 팀 갱신을 시도합니다.
	void ResetTeamChange(int pTeamIdx);
	void TryTeamChange(int pTeamIdx, float pChangeFactor);

	// 팀을 갱신합니다.
	void UpdateTeam(int pTeamIdx);

	// 해당 지휘관에 대한 점령 상태를 갱신합니다.
	void UpdateCommanderCaptureState(BattleCommander pUpdateCommander);
}
