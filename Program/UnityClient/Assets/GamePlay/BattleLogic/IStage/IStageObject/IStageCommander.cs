using UnityEngine;
using System.Collections;
using System;

public enum StageCommanderState
{
	Attack,
	AttackStop,
	AttackComplete,
	Hit,
	ConquerCheckPoint,
	ConquerStop,
	ConquerComplete,
	Move,
	MoveStop,

	None,
}

public interface IStageCommander : IStageObject
{
	void SetSelected(bool pIsSelected);

	void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY);
	void OnFrameUpdate(int pDeltaTime);
	void OnUpdateTeam(int pTeamIdx);

	void OnMoveTo(Coord2 pCoord, bool pIsArrived);
	void OnRotateToDirectionY(int pDirectionY);

	void ShowState(StageCommanderState pCommanderState);
	void ShowRetreat(bool pIsRetreat);
}
