using UnityEngine;
using System.Collections;
using System;

public interface IStageGridCell
{
	// 스테이지 인터페이스
	IStage aIStage { get; }

	// 을 갱신합니다.
	void UpdateTeam(int pTeamIdx);
}
