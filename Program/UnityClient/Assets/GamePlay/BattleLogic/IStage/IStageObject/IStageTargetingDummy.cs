using UnityEngine;
using System.Collections;
using System;

public interface IStageTargetingDummy : IStageObject
{
	void SetDetected(int pDetectingTeamIdx, bool pIsToDetected);

	void UpdateCoord(Coord2 pCoord, bool pIsArrived);
	void UpdateDirectionY(int pDirectionY);
}
