using UnityEngine;
using System.Collections;
using System;

public interface IStageTroop : IStageObject
{
	void SetSelected(bool pIsSelected);
	void SetTargetable(bool pIsTargetable);

	void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY);
	void OnFrameUpdate(int pDeltaTime);
	void OnUpdateTeam(int pTeamIdx);
	void OnUpdateDetection(int pDetectingTeamIdx, bool pIsToDetected);
	void OnUpdateHp(int pCurHp, int pMaxHp, TroopHpChangeType pHpChangeType);
	void OnUpdateExp();
	void OnUpdateVeteranRank(int pRank);
	void OnUpdateSightRange();
	void OnUpdateAttackRange();
	void OnUpdateHiding(bool pIsHiding);
	void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState);
	void OnUpdateReturnState(bool pIsFallBehind);
	void OnUpdateLeaderState(bool pIsLeader);
	void OnUpdateRecruitState();

	void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType);
	void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay);
	void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay);
	void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay);
	void OnHoldMove(bool pIsHoldMove);
	void OnStopMove(Coord2 pStopCoord);

	void OnMoveTo(Coord2 pCoord, bool pIsArrived);
	void OnRotateBodyToDirectionY(int pDirectionY);
	void OnRotateBodyWeaponToDirectionY(int pDirectionY);
	void OnRotateTurretToDirectionY(int pDirectionY);

	void OnInstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay);
	void OnUnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay);
	void OnTrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay);
	void OnWindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay);
	void OnAimWeapon(int pAimDelay);
	void OnFireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim);
	void OnEndCooldown();

	void OnGoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop);
	void OnComeBackInteraction();

	void OnStartSkill(BattleCommanderSkill pCommanderSkill);
	void OnStopSkill(BattleCommanderSkill pCommanderSkill);

	void OnStartAction(TroopActionType pActionType);
	void OnStopAction();

	void ShowHit(TroopHitResult pTroopHitResult);
	void ShowRetreat(bool pIsRetreat);
	void ShowTroopScript(String pMessageKey, float pDuration);
}
