using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageSpec
{
	public int mIdx;

	public int mNo;
	public String mName;
	public String mNameText;

	public AssetKey mMapAssetKey;
	public AssetKey mMiniMapAssetKey;
	public AssetKey mGridLayoutAssetKey;
	public AssetKey mBattleScriptAssetKey;

	public bool mIsRenderTerrain;

	public String[] mCustomUserNames;
	public int[] mCustomUserDecks;

	public String mStageUIName;
	//-------------------------------------------------------------------------------------------------------
	// 속성

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public StageSpec()
	{
		mCustomUserNames = new String[BattleMatchInfo.cMaxUserCount];
		mCustomUserDecks = new int[BattleMatchInfo.cMaxUserCount];
		for (int iCustomUser = 0; iCustomUser < BattleMatchInfo.cMaxUserCount; iCustomUser++)
		{
			mCustomUserNames[iCustomUser] = String.Empty;
			mCustomUserDecks[iCustomUser] = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<StageSpec[{0}] Id={1} Name={2} Map={3} GridLayout={4}>", mIdx, mNo, mName, mMapAssetKey, mGridLayoutAssetKey);
	}
	//-------------------------------------------------------------------------------------------------------
	// 존재하는 스펙인지 검사합니다.
	public static bool IsValid(StageSpec pStageSpec)
	{
		if ((pStageSpec != null) &&
			(pStageSpec.mNo > 0))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	public static void SetUpStaticProperty()
	{
		_SetUpStaticProperty();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 UserIdx의 유저 이름을 얻어옵니다.
	public String GetCustomUserName(int lUserIdx)
	{
		return mCustomUserNames[lUserIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 UserIdx의 사용 Deckid얻어옵니다.
	public int GetCustomDeckId(int lUserIdx)
	{
		return mCustomUserDecks[lUserIdx];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 정적 속성을 초기화합니다.
	private static void _SetUpStaticProperty()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
