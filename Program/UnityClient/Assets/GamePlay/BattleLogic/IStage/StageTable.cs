using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "StageTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";

	public readonly String vMapBundleColumnName = "MapBundle";
	public readonly String vMapPathColumnName = "MapPath";
	public readonly String vMiniMapPathColumnName = "MiniMapPath";
	public readonly String vGridLayoutBundleColumnName = "GridLayoutBundle";
	public readonly String vGridLayoutPathColumnName = "GridLayoutPath";

	public readonly String vIsRenderTerrainColumnName = "IsRenderTerrain";

	public readonly String vBattleScriptBundleColumnName = "BattleScriptBundle";
	public readonly String vBattleScriptPathColumnName = "BattleScriptPath";

	public readonly String vCustomUserColumnName = "CustomUserInfos";

	public readonly String vStageUINameColumnName = "ScriptUI";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static StageTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new StageTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public StageTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 스펙 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lStageName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lStageName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 스테이지 스펙 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("StageTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 스테이지 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lStageName))
			{
				Debug.LogError(String.Format("StageTable[{0}] duplicate Name '{1}'", lSpecNo, lStageName));
				continue;
			}
			mNameToRowDictionary.Add(lStageName, iRow);
		}

		// 검색 테이블을 초기화합니다.

		// 스펙 테이블을 초기화합니다.
		mStageSpecs = new StageSpec[mCSVObject.RowNum];

		// 정적 테이블을 초기화합니다.
		StageSpec.SetUpStaticProperty();

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllStageSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 정보를 모두 읽어옵니다.
	public void LoadAllStageSpec()
	{
		Debug.LogWarning("LoadAllStageSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mStageSpecs[iRow] == null) // _SetUpSpawnLinkStageInfo() 속에서 초기화되었을 수도 있습니다.
				mStageSpecs[iRow] = _LoadStageSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public StageSpec GetStageSpecByIdx(int pStageIdx)
	{
		if (mStageSpecs[pStageIdx] == null)
			mStageSpecs[pStageIdx] = _LoadStageSpec(mCSVObject.GetRow(pStageIdx), pStageIdx);
		return mStageSpecs[pStageIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public StageSpec FindStageSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (mStageSpecs[lRowIdx] == null)
				mStageSpecs[lRowIdx] = _LoadStageSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mStageSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find stage id - " + pSpecNo);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public StageSpec FindStageSpec(String pStageName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pStageName, out lRowIdx))
		{
			if (mStageSpecs[lRowIdx] == null)
				mStageSpecs[lRowIdx] = _LoadStageSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mStageSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find stage name - " + pStageName);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mStageSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("StageTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙을 로딩합니다.
	private StageSpec _LoadStageSpec(CSVRow pCSVRow, int pRowIdx)
	{
		StageSpec lStageSpec = new StageSpec();
	
		// 줄 인덱스
		lStageSpec.mIdx = pRowIdx;

		// Name
		lStageSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lStageSpec.mName);

		// SpecNo
		lStageSpec.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// NameText
		lStageSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// MapBundle, MapPath, MiniMapPath
		lStageSpec.mMapAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vMapBundleColumnName),
			pCSVRow.GetStringValue(vMapPathColumnName));
		lStageSpec.mMiniMapAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vMapBundleColumnName),
			pCSVRow.GetStringValue(vMiniMapPathColumnName));

		// GridLayoutBundle, GridLayoutPath
		lStageSpec.mGridLayoutAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vGridLayoutBundleColumnName),
			pCSVRow.GetStringValue(vGridLayoutPathColumnName));

		// IsRenderTerrain
		lStageSpec.mIsRenderTerrain = pCSVRow.GetBoolValue(vIsRenderTerrainColumnName);

		// BattleScriptBundle, BattleScriptPath
		String lBattleScriptBundle = pCSVRow.GetStringValue(vBattleScriptBundleColumnName);
		String lBattleScriptPath = pCSVRow.GetStringValue(vBattleScriptPathColumnName);
		if (!String.IsNullOrEmpty(lBattleScriptBundle) && !String.IsNullOrEmpty(lBattleScriptPath))
		{
			lStageSpec.mBattleScriptAssetKey = AssetManager.get.CreateAssetKey(
				pCSVRow.GetStringValue(vBattleScriptBundleColumnName),
				pCSVRow.GetStringValue(vBattleScriptPathColumnName));
		}
		else
		{
			lStageSpec.mBattleScriptAssetKey = null;
		}

		// CustomUserInfos
		String lCustomUserInfos = pCSVRow.GetStringValue(vCustomUserColumnName);
		String[] lCustomUserSplit = lCustomUserInfos.Split('^');
		if (lCustomUserSplit.Length >= BattleMatchInfo.cMaxUserCount)
		{
			Debug.LogError("Over size user count - " + lCustomUserSplit.Length);
		}
		else if(lCustomUserSplit.Length > 1)
		{
			for (int iCustomUser = 0; iCustomUser < lCustomUserSplit.Length; iCustomUser++)
			{
				String[] lCustomUserInfoString = lCustomUserSplit[iCustomUser].Split(':');
				lStageSpec.mCustomUserNames[iCustomUser] = lCustomUserInfoString[0];
				if (!int.TryParse(lCustomUserInfoString[1], out lStageSpec.mCustomUserDecks[iCustomUser]))
					lStageSpec.mCustomUserDecks[iCustomUser] = 0;
			}
		}

		// StageUIName
		lStageSpec.mStageUIName = pCSVRow.GetStringValue(vStageUINameColumnName);

		return lStageSpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static StageTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private StageSpec[] mStageSpecs;
}
