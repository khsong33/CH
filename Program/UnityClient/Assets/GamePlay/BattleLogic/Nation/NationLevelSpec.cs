﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NationLevelSpec
{
	public int mLevel;
	public int mNeedExp;
	public int mMaxExp;
	public int mLimitGainExp;
	public int[] mLevelUpNationRewardSpecNo;

	public NationLevelSpec()
	{
		mLevelUpNationRewardSpecNo = new int[(int)DeckNation.Count];
	}
}
