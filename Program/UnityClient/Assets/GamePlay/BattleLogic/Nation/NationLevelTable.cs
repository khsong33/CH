﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NationLevelTable
{
	public static readonly String cCSVTextAssetPath = "NationLevelTableCSV";

	public static readonly String vLevelColumn = "Level";
	public static readonly String vNeedExpColumn = "NeedExp"; 
	public static readonly String vMaxExpColumn = "MaxExp";
	public static readonly String vLimitGainExpColumn = "GainLimitExp";
	public static readonly String vUSRewardColumnName = "USReward";
	public static readonly String vGERewardColumnName = "GEReward";
	public static readonly String vUSSRReardColumnName = "USSRReward";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static NationLevelTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new NationLevelTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public NationLevelTable()
	{
		Load(cCSVTextAssetPath);
	}
	public int aMaxLevel { get { return mMaxLevel; } }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mNationLevelSpecs = new Dictionary<int, NationLevelSpec>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			NationLevelSpec info = new NationLevelSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mLevel = lCSVRow.GetIntValue(vLevelColumn);
			info.mNeedExp = lCSVRow.GetIntValue(vNeedExpColumn);
			info.mMaxExp = lCSVRow.GetIntValue(vMaxExpColumn);
			info.mLimitGainExp = lCSVRow.GetIntValue(vLimitGainExpColumn);
			lCSVRow.TryIntValue(vUSRewardColumnName, out info.mLevelUpNationRewardSpecNo[(int)DeckNation.US], 0);
			lCSVRow.TryIntValue(vGERewardColumnName, out info.mLevelUpNationRewardSpecNo[(int)DeckNation.GE], 0);
			lCSVRow.TryIntValue(vUSSRReardColumnName, out info.mLevelUpNationRewardSpecNo[(int)DeckNation.USSR], 0);

			if (mNationLevelSpecs.ContainsKey(info.mLevel))
				Debug.LogError("already exist level info - " + info.mLevel);
			mNationLevelSpecs.Add(info.mLevel, info);
			if (info.mLevel > mMaxLevel)
			{
				mMaxLevel = info.mLevel;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨 데이터를 가져옵니다.
	public NationLevelSpec GetNationLevelSpec(int pLevel)
	{
		if (mNationLevelSpecs.ContainsKey(pLevel))
		{
			return mNationLevelSpecs[pLevel];
		}
		return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("NationLevelTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static NationLevelTable sInstance = null;

	private CSVObject mCSVObject;
	private Dictionary<int, NationLevelSpec> mNationLevelSpecs;
	private int mMaxLevel;
}
