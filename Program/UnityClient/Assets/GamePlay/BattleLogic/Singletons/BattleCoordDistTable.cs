﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleCoordDistTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "CoordinateDistanceCSV";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static BattleCoordDistTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new BattleCoordDistTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleCoordDistTable()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load()
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mMaxRow = mCSVObject.RowNum;
		mMaxCol = mCSVObject.ColNum-1;
		mDistData = new int[mMaxRow, mMaxCol];
		for (int iRow = 0; iRow < mMaxRow; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			for (int iCol = 0; iCol < mMaxCol; iCol++)
			{
				String lColName = iCol.ToString();
				lCSVRow.TryIntValue(lColName, out mDistData[iRow, iCol], 0);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Coord 좌표를 받아 두 값의 차이를 얻어옵니다.
	public int GetDistance(RowCol pCurrentRowCol, RowCol pTargetRowCol)
	{
		int lRow = Mathf.Abs(pCurrentRowCol.row - pTargetRowCol.row);
		int lCol = Mathf.Abs(pCurrentRowCol.col - pTargetRowCol.col);

		if (lRow < 0 || lRow >= mMaxRow)
		{
#if UNITY_EDITOR
			Debug.LogError("Overflow Row index - " + lRow);
#endif
			return 0;
		}
		if (lCol < 0 || lCol >= mMaxCol)
		{
#if UNITY_EDITOR
			Debug.LogError("Overflow Col index - " + lCol);
#endif
			return 0;
		}
		return mDistData[lRow, lCol];
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
		
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static BattleCoordDistTable sInstance = null;
	private CSVObject mCSVObject;

	private int[,] mDistData;
	private int mMaxRow;
	private int mMaxCol;
}

