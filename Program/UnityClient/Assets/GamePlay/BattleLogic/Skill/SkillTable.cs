using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SkillTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "SkillTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";
	public readonly String vNameKeyTextColumnName = "LocalNameKey";
	public readonly String vDescKeyTextColumnName = "LocalDescKey";
	public readonly String vControlKeyTextColumnName = "LocalControlKey";
	public readonly String vTypeKeyTextColumnName = "LocalTypeKey";

	public readonly String vIconBundleColumnName = "IconBundle";
	public readonly String vIconPathColumnName = "SkillIconPath";
	public readonly String vStateIconPathColumnName = "SkillStateIconPath";

	public readonly String vUseTypeColumnName = "UseType";
	public readonly String vReadyDelayColumnName = "ReadyDelay";
	public readonly String vUseDelayColumnName = "UseDelay";
	public readonly String vCooldownDelayColumnName = "CooldownDelay";

	public readonly String vTroopEffectColumnName = "TroopEffect";

	public readonly String[] vTroopPosColumnNames = { "Troop1Pos", "Troop2Pos", "Troop3Pos", "Troop4Pos", "Troop5Pos", "Troop6Pos" };

	public readonly String vFunctionColumnName = "Function";
	public readonly String[] vIntValueColumnNames = { "IntValue0", "IntValue1", "IntValue2", "IntValue3", "IntValue4" };
	public readonly String[] vRatioValueColumnNames = { "RatioValue0", "RatioValue1", "RatioValue2", "RatioValue3", "RatioValue4" };
	public readonly String[] vStringValueColumnNames = { "StringValue0", "StringValue1", "StringValue2", "StringValue3", "StringValue4" };
	public readonly String[] vBoolValueColumnNames = { "BoolValue0", "BoolValue1", "BoolValue2", "BoolValue3", "BoolValue4" };

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static SkillTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new SkillTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public SkillTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		DebugUtil.Assert(SkillSpec.cSkillFunctionCount == Enum.GetValues(typeof(SkillFunction)).Length, "cSkillFunctionCount mismatch - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		SkillSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 스펙 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lSkillName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSkillName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 이펙트 스펙 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("SkillTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 이펙트 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSkillName))
			{
				Debug.LogError(String.Format("SkillTable[{0}] duplicate Name '{1}'", lSpecNo, lSkillName));
				continue;
			}
			mNameToRowDictionary.Add(lSkillName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mSkillUseTypeDictionary = new Dictionary<String, SkillUseType>();
		foreach (SkillUseType iEnum in Enum.GetValues(typeof(SkillUseType)))
			mSkillUseTypeDictionary.Add(iEnum.ToString(), iEnum);

		mSkillFunctionDictionary = new Dictionary<String, SkillFunction>();
		foreach (SkillFunction iEnum in Enum.GetValues(typeof(SkillFunction)))
			mSkillFunctionDictionary.Add(iEnum.ToString(), iEnum);

		// 스펙 테이블을 초기화합니다.
		pSkillSpecs = new SkillSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllSkillSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 정보를 모두 읽어옵니다.
	public void LoadAllSkillSpec()
	{
		Debug.LogWarning("LoadAllSkillSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (pSkillSpecs[iRow] == null) // _SetUpSpawnLinkSkillInfo() 속에서 초기화되었을 수도 있습니다.
				pSkillSpecs[iRow] = _LoadSkillSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public SkillSpec GetSkillSpecByIdx(int pSkillIdx)
	{
		if (pSkillSpecs[pSkillIdx] == null)
			pSkillSpecs[pSkillIdx] = _LoadSkillSpec(mCSVObject.GetRow(pSkillIdx), pSkillIdx);
		return pSkillSpecs[pSkillIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public SkillSpec FindSkillSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (pSkillSpecs[lRowIdx] == null)
				pSkillSpecs[lRowIdx] = _LoadSkillSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return pSkillSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public SkillSpec FindSkillSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (pSkillSpecs[lRowIdx] == null)
				pSkillSpecs[lRowIdx] = _LoadSkillSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return pSkillSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return pSkillSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("SkillTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private SkillSpec _LoadSkillSpec(CSVRow pCSVRow, int pRowIdx)
	{
		SkillSpec lSkillSpec = new SkillSpec();
	
		// 줄 인덱스
		lSkillSpec.mIdx = pRowIdx;

		// Name
		lSkillSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lSkillSpec.mName);

		// SpecNo
		lSkillSpec.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// NameText
		lSkillSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// NameKey
		lSkillSpec.mNameKey = pCSVRow.GetStringValue(vNameKeyTextColumnName);

		// DescKeys
		lSkillSpec.mDescKey = pCSVRow.GetStringValue(vDescKeyTextColumnName);
		
		// LocalControlKey
		lSkillSpec.mControlKey = pCSVRow.GetStringValue(vControlKeyTextColumnName);

		// LocalTypeKey
		lSkillSpec.mTypeKey = pCSVRow.GetStringValue(vTypeKeyTextColumnName);

		// IconBundle, ButtonIconPath, TroopStateIconPath
		lSkillSpec.mIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vIconPathColumnName));
		lSkillSpec.mStateIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vStateIconPathColumnName));

		// UseType
		String lUseTypeString = pCSVRow.GetStringValue(vUseTypeColumnName);
		if (!mSkillUseTypeDictionary.TryGetValue(lUseTypeString, out lSkillSpec.mUseType))
			Debug.LogError(String.Format("SkillTable[{0}] invalid Type '{1}'", lSkillSpec.mNo, lUseTypeString));

		// ReadyDelay
		lSkillSpec.mReadyDelay = pCSVRow.GetIntValue(vReadyDelayColumnName);

		// UseDelay
		lSkillSpec.mUseDelay = pCSVRow.GetIntValue(vUseDelayColumnName);

		// CooldownDelay
		lSkillSpec.mCooldownDelay = pCSVRow.GetIntValue(vCooldownDelayColumnName);

		// TroopEffect
		String lTroopEffectString;
		if (pCSVRow.TryStringValue(vTroopEffectColumnName, out lTroopEffectString, String.Empty)) // 기본값 빈 문자열
			lSkillSpec.mTroopEffectSpec = TroopEffectTable.get.FindTroopEffectSpec(lTroopEffectString);
		else
			lSkillSpec.mTroopEffectSpec = null;

		// FormationInfos
		String lCoordString;
		if (pCSVRow.TryStringValue(vTroopPosColumnNames[0], out lCoordString, String.Empty)) // 기본값 빈 문자열
		{
			lSkillSpec.mFormationPositionOffsets = new Coord2[CommanderTroopSet.cMaxTroopCount];
			lSkillSpec.mIsFormationMove = true;

			for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
			{
				Coord2 lFormationOffsetValue;
				if (!pCSVRow.TryCoordValue(vTroopPosColumnNames[iTroop], out lFormationOffsetValue, Coord2.zero)) // 기본값 (0,0)
					break;
				lSkillSpec.mFormationPositionOffsets[iTroop] = new Coord2(-lFormationOffsetValue.x, -lFormationOffsetValue.z); // 0도 회전(-Z) 방향으로 오프셋을 저장
			}
		}
		else
			lSkillSpec.mIsFormationMove = false;

		// IsGoToTileCenter 초기화
		lSkillSpec.mIsGoToTileCenter = false;

		// Function
		String lFunctionString;
		if (pCSVRow.TryStringValue(vFunctionColumnName, out lFunctionString, String.Empty)) // 기본값 빈 문자열
		{
			if (!mSkillFunctionDictionary.TryGetValue(lFunctionString, out lSkillSpec.mFunction))
				Debug.LogError(String.Format("SkillTable[{0}] invalid Function '{1}'", lSkillSpec.mNo, lFunctionString));

			switch (lSkillSpec.mFunction)
			{
			case SkillFunction.GroundEffect:
				_LoadGroundEffectParam(pCSVRow, lSkillSpec);
				break;
			case SkillFunction.GroundAttack:
				_LoadGroundAttackParam(pCSVRow, lSkillSpec);
				break;
			case SkillFunction.TargetAttack:
				_LoadTargetAttackParam(pCSVRow, lSkillSpec);
				break;
			case SkillFunction.TroopSupplement:
				_LoadTroopSupplementParam(pCSVRow, lSkillSpec);
				break;
			case SkillFunction.FieldMedKit: // FieldRecovery 타입
				_LoadFieldRecoveryParam(pCSVRow, lSkillSpec);
				break;
			case SkillFunction.Repair: // FieldRecovery 타입
				_LoadFieldRecoveryParam(pCSVRow, lSkillSpec);
				break;
			default:
				Debug.LogError("not implemented vFunctionColumnName:" + lFunctionString);
				lSkillSpec.mFunction = SkillFunction.None;
				break;
			}
		}
		else
			lSkillSpec.mFunction = SkillFunction.None;

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lSkillSpec);

		return lSkillSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(SkillSpec pSkillSpec)
	{
		// mIsMovable
		pSkillSpec.mIsMovable = true;

		if (pSkillSpec.mTroopEffectSpec != null)
		{
			StatTableModifier<TroopStat> lTroopStatModifier = pSkillSpec.mTroopEffectSpec.mTroopStatTableModifier;

			switch (lTroopStatModifier.aStatModifyTypes[(int)TroopStat.AdvanceSpeed])
			{
			case StatModifyType.Overwrite:
			case StatModifyType.Ratio:
				if (lTroopStatModifier.aModifyValues[(int)TroopStat.AdvanceSpeed] == 0)
					pSkillSpec.mIsMovable = false;
				break;
			}
		}

		// mIsForceFormationMoveWhenSkill
		if (!pSkillSpec.mIsMovable && // 이동 불가 스킬인데
			pSkillSpec.mIsFormationMove) // 포메이션 이동이 있는 경우
			pSkillSpec.mIsForceFormationMoveWhenSkill = true; // 스킬 사용 전에 포메이션 이동을 하고 끝나면 원래로 돌아옴
		else
			pSkillSpec.mIsForceFormationMoveWhenSkill = false; // 스킬 포메이션이 있더라도 유저가 이동을 안하면 포메이션 적용 안함

		// mStartType
		if (pSkillSpec.mIsFormationMove && // 포메이션 이동이 있고
			!pSkillSpec.mIsMovable) // 스킬 효과중 이동 불가라면
			pSkillSpec.mStartType = SkillStartType.AfterFormation;
		else
			pSkillSpec.mStartType = SkillStartType.OnActivation;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadGroundEffectParam(CSVRow pCSVRow, SkillSpec pSkillSpec)
	{
		SkillSpec.GroundEffectParam lFunctionParam = new SkillSpec.GroundEffectParam();
		pSkillSpec.mGroundEffectParam = lFunctionParam;

		// StringValue0 : 지형 효과 종류
		String lGroundEffectString = pCSVRow.GetStringValue(vStringValueColumnNames[0]);
		lFunctionParam.mGroundEffectSpec = GroundEffectTable.get.FindGroundEffectSpec(lGroundEffectString);

		// IntValue0 : 지형 효과 적용 타일 범위
		lFunctionParam.mTileDistance = pCSVRow.GetIntValue(vIntValueColumnNames[0]) - 1; // 테이블에서 범위 1은 타일 거리 0

		// IntValue1 : 유지 시간
		lFunctionParam.mDurationDelay = pCSVRow.GetIntValue(vIntValueColumnNames[1]);

		// BoolValue0 : 부대가 설치되는 지형효과로 이동하는 여부
		pSkillSpec.mIsGoToTileCenter = pCSVRow.GetBoolValue(vBoolValueColumnNames[0]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadGroundAttackParam(CSVRow pCSVRow, SkillSpec pSkillSpec)
	{
		SkillSpec.GroundAttackParam lFunctionParam = new SkillSpec.GroundAttackParam();
		pSkillSpec.mGroundAttackParam = lFunctionParam;

		// IntValue0 : 공격 횟수
		lFunctionParam.mAttackCount = pCSVRow.GetIntValue(vIntValueColumnNames[0]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadTargetAttackParam(CSVRow pCSVRow, SkillSpec pSkillSpec)
	{
		SkillSpec.TargetAttackParam lFunctionParam = new SkillSpec.TargetAttackParam();
		pSkillSpec.mTargetAttackParam = lFunctionParam;

		// IntValue0 : 공격 횟수
		lFunctionParam.mAttackCount = pCSVRow.GetIntValue(vIntValueColumnNames[0]);

		// StringValue0 : 공격 대상
		String lAttackBodyString = pCSVRow.GetStringValue(vStringValueColumnNames[0]);
		lFunctionParam.mAttackBody = TroopTable.get.FindTroopType(lAttackBodyString);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadTroopSupplementParam(CSVRow pCSVRow, SkillSpec pSkillSpec)
	{
// 		SkillSpec.TroopSupplementParam lFunctionParam = new SkillSpec.TroopSupplementParam();
// 		pSkillSpec.mTroopSupplementParam = lFunctionParam;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 함수 파라미터를 테이블에서 읽어서 생성합니다.
	private void _LoadFieldRecoveryParam(CSVRow pCSVRow, SkillSpec pSkillSpec)
	{
		SkillSpec.FieldRecoveryParam lFunctionParam = new SkillSpec.FieldRecoveryParam();
		pSkillSpec.mFieldRecoveryParam = lFunctionParam;

		// IntValue0 : 치료 범위
		lFunctionParam.mRecoveryDistance = pCSVRow.GetIntValue(vIntValueColumnNames[0]);

		// IntValue1 : 치료 효과 시간
		lFunctionParam.mRecoveryEffectDelay = pCSVRow.GetIntValue(vIntValueColumnNames[0]);

		// IntValue2 : 치료 동작 시간
		lFunctionParam.mRecoveryActionDelay = pCSVRow.GetIntValue(vIntValueColumnNames[0]);

		// RatioValue0 : 초당 치료 비율 (치료 비율은 각 부대 최대 HP 를 기준)
		lFunctionParam.mRecoveryRatio = pCSVRow.GetRatioValue(vRatioValueColumnNames[0]);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static SkillTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, SkillUseType> mSkillUseTypeDictionary;
	private Dictionary<String, SkillFunction> mSkillFunctionDictionary;

	private SkillSpec[] pSkillSpecs;
}
