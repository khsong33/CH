using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TroopTable
{
	public readonly String cCSVTextAssetPath = "TroopTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";
	public readonly String vNameKeyColumnName = "NameKey";
	public readonly String vDescTextColumnName = "DescKey";
	public readonly String vEffectiveKeyColumnName = "EffectiveKey";
	public readonly String vIconBundleColumnName = "IconBundle";
	public readonly String vButtonIconPathColumnName = "ButtonIconPath";
	public readonly String vFieldIconPathColumnName = "FieldIconPath";
	public readonly String vPortraitIconPathColumnName = "PortraitIconPath";
	public readonly String vStageTroopBundleColumnName = "StageTroopBundle";
	public readonly String vStageTroopAssetKeyNameColumnName = "StageTroopAssetKey";
	public readonly String vStageTargetingDummyBundleColumnName = "StageTargetingDummyBundle";
	public readonly String vStageTargetingDummyPathColumnName = "StageTargetingDummyPath";

	public readonly String vTypeColumnName = "Type";
	public readonly String vPrimaryWeaponNameColumnName = "PrimaryWeapon";
	public readonly String vSecondaryWeaponNameColumnName = "SecondaryWeapon";
	public readonly String vVeteranWeaponNameColumnName = "VeteranWeapon";
	public readonly String vTurretTypeColumnName = "Turret";
	public readonly String vDefaultDefenseTypeColumnName = "DefaultDefenseType";
	public readonly String vUnitCountColumnName = "UnitCount";
	public readonly String vDetectionUpdateDegreeColumnName = "DetectionUpdateDegree";
	public readonly String vIsCaptureTypeColumnName = "CaptureType";

	public readonly String vSightRangeColumnName = "SightRange";
	public readonly String vSpawnMpColumnName = "SpawnMp";
	public readonly String vSpawnCoolTimeColumnName = "SpawnCoolTime";
	public readonly String vUnitSizeColumnName = "UnitSize";
	public readonly String vHitSizeColumnName = "HitSize";
	public readonly String vBaseHpColumnName = "BaseHp";
	public readonly String vArmorSizeColumnName = "ArmorSize";
	public readonly String vFrontArmorColumnName = "Armor";
	public readonly String vSideArmorColumnName = "SideArmor";
	public readonly String vRearArmorColumnName = "RearArmor";
	public readonly String vAdvanceSpeedColumnName = "AdvanceSpeed";
	public readonly String vMaxRotationDelayColumnName = "MaxRotationDelay";
//	public readonly String vMoveStartDelayColumnName = "MoveStartDelay";
	public readonly String vSetUpDelayColumnName = "SetUpDelay";

	public readonly String vMaxMoraleColumnName = "MaxMorale";
	public readonly String vMoraleRecoverColumnName = "MoraleRecover";
	public readonly String vSuppressedActivateColumnName = "SuppressedActivate";
	public readonly String vSuppressedRecoverColumnName = "SuppressedRecover";
	public readonly String vPinDownActivateColumnName = "PinDownActivate";
	public readonly String vPinDownRecoverColumnName = "PinDownRecover";
	public readonly String vSuppressedSpeedMultiplierColumnName = "AdvanceSpeedMultiplier";
	public readonly String vCooldownDelayMutiplierColumnName = "CooldownDelayMutiplier";
	public readonly String vAccuracyMutiplierColumnName = "AccuracyMutiplier";
	public readonly String vNonCombatMultiplierColumnName = "NonCombatMultiplier";
	public readonly String vNonCombatDelayColumnName = "NonCombatDelay";
	public readonly String vMoveOvercomeLevelColumnName = "MoveOvercome";
	public readonly String vAttackOvercomeLevelColumnName = "AttackOvercome";

	public readonly String vLevelUpgradeColumnName = "LevelUpgrade";
	public readonly String vExperienceColumnName = "Experience";
	public readonly String vVeterancySpecColumnName = "VeterancyEffect";

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static TroopTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new TroopTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public TroopTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		DebugUtil.Assert(TroopSpec.cTroopTypeCount == Enum.GetValues(typeof(TroopType)).Length, "cTroopTypeCount mismatch - " + this);
		DebugUtil.Assert(TroopSpec.cTroopBodyCount == Enum.GetValues(typeof(TroopBody)).Length, "cTroopBodyCount mismatch - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 정적 테이블을 초기화합니다.
		TroopSpec.SetUpStaticProperty();

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 스펙 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 부대 스펙 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("TroopTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 스펙 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("TroopTable[{0}] duplicate Name '{1}'", lSpecNo, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mTroopTypeDictionary = new Dictionary<String, TroopType>();
		foreach (TroopType iForm in Enum.GetValues(typeof(TroopType)))
			mTroopTypeDictionary.Add(iForm.ToString(), iForm);

		mTroopDefenseTypeDictionary = new Dictionary<String, TroopDefenseType>();
		foreach (TroopDefenseType iForm in Enum.GetValues(typeof(TroopDefenseType)))
			mTroopDefenseTypeDictionary.Add(iForm.ToString(), iForm);

		// 스펙 테이블을 초기화합니다.
		mTroopSpecs = new TroopSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllTroopSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllTroopSpec()
	{
		Debug.LogWarning("LoadAllTroopSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mTroopSpecs[iRow] == null) // _SetUpSpawnLinkTroopInfo() 속에서 초기화되었을 수도 있습니다.
				mTroopSpecs[iRow] = _LoadTroopSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public TroopSpec GetTroopSpecByIdx(int pTroopIdx)
	{
		if (mTroopSpecs[pTroopIdx] == null)
			mTroopSpecs[pTroopIdx] = _LoadTroopSpec(mCSVObject.GetRow(pTroopIdx), pTroopIdx);
		return mTroopSpecs[pTroopIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public TroopSpec FindTroopSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (mTroopSpecs[lRowIdx] == null)
				mTroopSpecs[lRowIdx] = _LoadTroopSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mTroopSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public TroopSpec FindTroopSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mTroopSpecs[lRowIdx] == null)
				mTroopSpecs[lRowIdx] = _LoadTroopSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mTroopSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 에넘값을 찾습니다.
	public TroopType FindTroopType(String pTroopTypeString)
	{
		TroopType lTroopType;
		if (!mTroopTypeDictionary.TryGetValue(pTroopTypeString, out lTroopType))
		{
			Debug.LogError("invalid TroopType:" + pTroopTypeString);
			return TroopType.Infantry;
		}
		return lTroopType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mTroopSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("TroopTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private TroopSpec _LoadTroopSpec(CSVRow pCSVRow, int pRowIdx)
	{
		TroopSpec lTroopSpec = new TroopSpec();
	
		// 줄 인덱스
		lTroopSpec.mIdx = pRowIdx;

		// Name
		lTroopSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lTroopSpec.mName);

		// SpecNo
		lTroopSpec.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// NameText
		lTroopSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// NameKey
		lTroopSpec.mNameKey = pCSVRow.GetStringValue(vNameKeyColumnName);

		// DescKey
		lTroopSpec.mDescText = pCSVRow.GetStringValue(vDescTextColumnName);

		// EffectiveKey
		lTroopSpec.mEffectiveText = pCSVRow.GetStringValue(vEffectiveKeyColumnName);

		// IconBundle, ButtonIconPath, FieldIconPath
		lTroopSpec.mButtonIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vButtonIconPathColumnName));
		lTroopSpec.mFieldIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vFieldIconPathColumnName));

		lTroopSpec.mPortraitIconAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vIconBundleColumnName),
			pCSVRow.GetStringValue(vPortraitIconPathColumnName));

		// StageTroopAssetKeyName
		lTroopSpec.mStageTroopAssetKeyName = pCSVRow.GetStringValue(vStageTroopAssetKeyNameColumnName);

		// StageTargetingDummyBundle, StageTargetingDummyPath
		lTroopSpec.mStageTargetingDummyAssetKey = AssetManager.get.CreateAssetKey(
			pCSVRow.GetStringValue(vStageTargetingDummyBundleColumnName),
			pCSVRow.GetStringValue(vStageTargetingDummyPathColumnName));

		// Type
		String lTypeString = pCSVRow.GetStringValue(vTypeColumnName);
		if (!mTroopTypeDictionary.TryGetValue(lTypeString, out lTroopSpec.mType))
			Debug.LogError(String.Format("TroopTable[{0}] invalid Type '{1}'", lTroopSpec.mNo, lTypeString));

		// BodyKind
		switch (lTroopSpec.mType)
		{
		case TroopType.Infantry:
			lTroopSpec.mBody = TroopBody.Soldier;
			break;
		default:
			lTroopSpec.mBody = TroopBody.Vehicle;
			break;
		}

		// UnitRadius
		int lUnitRadius = pCSVRow.GetIntValue(vUnitSizeColumnName) / 2; // 반지름
		if (lUnitRadius > BattleConfig.get.mMaxUnitRadius)
			Debug.LogError(String.Format("{0} oversized UnitRadius:{1}", _OnParsingLogPrefix(lTroopSpec.mName), lUnitRadius * 2));
		lTroopSpec.mUnitRadius = lUnitRadius;

		// HitSize
		lTroopSpec.mHitSize = pCSVRow.GetIntValue(vHitSizeColumnName);

		// WeaponSpec
		lTroopSpec.mWeaponSpecs = new WeaponSpec[TroopSpec.cWeaponCount];
		{
			String lPrimaryWeaponName = pCSVRow.GetStringValue(vPrimaryWeaponNameColumnName);
			lTroopSpec.mWeaponSpecs[TroopSpec.cPrimaryWeaponIdx] = WeaponTable.get.FindWeaponSpec(lPrimaryWeaponName);

			String lSecondaryWeaponName = pCSVRow.GetStringValue(vSecondaryWeaponNameColumnName);
			lTroopSpec.mWeaponSpecs[TroopSpec.cSecondaryWeaponIdx] = WeaponTable.get.FindWeaponSpec(lSecondaryWeaponName);

			String lVeteranWeaponName = pCSVRow.GetStringValue(vVeteranWeaponNameColumnName);
			lTroopSpec.mWeaponSpecs[TroopSpec.cVeteranWeaponIdx] = WeaponTable.get.FindWeaponSpec(lVeteranWeaponName);
		}

		// IsBodyWeapons, IsTurretWeapons
		lTroopSpec.mIsBodyWeapons = new bool[TroopSpec.cWeaponCount];
		lTroopSpec.mIsTurretWeapons = new bool[TroopSpec.cWeaponCount];
		{
			bool lIsPrimaryTurret;
			bool lIsSecondaryTurret;
			bool lIsVeteranTurret;

			String lTurretType = pCSVRow.GetStringValue(vTurretTypeColumnName);
			if (String.Compare(lTurretType, "None") == 0)
			{
				lIsPrimaryTurret = false;
				lIsSecondaryTurret = false;
				lIsVeteranTurret = false;
			}
			else if (String.Compare(lTurretType, "Primary") == 0)
			{
				lIsPrimaryTurret = true;
				lIsSecondaryTurret = false;
				lIsVeteranTurret = false;
			}
			else if (String.Compare(lTurretType, "Secondary") == 0)
			{
				lIsPrimaryTurret = false;
				lIsSecondaryTurret = true;
				lIsVeteranTurret = false;
			}
			else if (String.Compare(lTurretType, "PrimarySecondary") == 0)
			{
				lIsPrimaryTurret = true;
				lIsSecondaryTurret = true;
				lIsVeteranTurret = false;
			}
			else if (String.Compare(lTurretType, "Veteran") == 0)
			{
				lIsPrimaryTurret = false;
				lIsSecondaryTurret = false;
				lIsVeteranTurret = true;
			}
			else if (String.Compare(lTurretType, "PrimaryVeteran") == 0)
			{
				lIsPrimaryTurret = true;
				lIsSecondaryTurret = false;
				lIsVeteranTurret = true;
			}
			else if (String.Compare(lTurretType, "SecondaryVeteran") == 0)
			{
				lIsPrimaryTurret = false;
				lIsSecondaryTurret = true;
				lIsVeteranTurret = true;
			}
			else if (String.Compare(lTurretType, "PrimarySecondaryVeteran") == 0)
			{
				lIsPrimaryTurret = true;
				lIsSecondaryTurret = true;
				lIsVeteranTurret = true;
			}
			else
			{
				Debug.LogError(String.Format("TroopTable[{0}] invalid Turret '{1}'", lTroopSpec.mNo, lTurretType));
				lIsPrimaryTurret = false;
				lIsSecondaryTurret = false;
				lIsVeteranTurret = false;
			}

			lTroopSpec.mIsBodyWeapons[TroopSpec.cPrimaryWeaponIdx] = !lIsPrimaryTurret;
			lTroopSpec.mIsBodyWeapons[TroopSpec.cSecondaryWeaponIdx] = !lIsSecondaryTurret;
			lTroopSpec.mIsBodyWeapons[TroopSpec.cVeteranWeaponIdx] = !lIsVeteranTurret;

			lTroopSpec.mIsTurretWeapons[TroopSpec.cPrimaryWeaponIdx] = lIsPrimaryTurret;
			lTroopSpec.mIsTurretWeapons[TroopSpec.cSecondaryWeaponIdx] = lIsSecondaryTurret;
			lTroopSpec.mIsTurretWeapons[TroopSpec.cVeteranWeaponIdx] = lIsVeteranTurret;

			for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
				if (lTroopSpec.mWeaponSpecs[iWeapon] == null)
				{
					lTroopSpec.mIsBodyWeapons[iWeapon] = false;
					lTroopSpec.mIsTurretWeapons[iWeapon] = false;
				}
		}

		lTroopSpec.mMainBodyWeaponIdx = -1;
		lTroopSpec.mMainTurretWeaponIdx = -1;
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
		{
			if ((lTroopSpec.mIsBodyWeapons[iWeapon]) &&
				(lTroopSpec.mMainBodyWeaponIdx < 0))
				lTroopSpec.mMainBodyWeaponIdx = iWeapon;

			if ((lTroopSpec.mIsTurretWeapons[iWeapon]) &&
				(lTroopSpec.mMainTurretWeaponIdx < 0))
				lTroopSpec.mMainTurretWeaponIdx = iWeapon;
		}

		// DefaultDefenseType
		String lDefaultDefenseType = pCSVRow.GetStringValue(vDefaultDefenseTypeColumnName);
		if (!mTroopDefenseTypeDictionary.TryGetValue(lDefaultDefenseType, out lTroopSpec.mDefaultDefenseType))
			Debug.LogError(String.Format("{0} invalid DefaultDefenseType '{1}'", _OnParsingLogPrefix(lTroopSpec.mName), lDefaultDefenseType));

		// UnitCount
		lTroopSpec.mUnitCount = pCSVRow.GetIntValue(vUnitCountColumnName);

		// IsCaptureType
		lTroopSpec.mIsCaptureType = pCSVRow.GetBoolValue(vIsCaptureTypeColumnName);

		// TroopStat
		lTroopSpec.mStatTable = new TroopStatTable();
		{
			TroopStatTable lTroopStatTable = lTroopSpec.mStatTable;

			// IsAttackDisabled
			lTroopStatTable.Set((int)TroopStat.IsAttackDisabled, 0); // 기본값으로 초기화

			// IsHiding
			lTroopStatTable.Set((int)TroopStat.IsHiding, 0); // 기본값으로 초기화

			// IsFollowAdvance
			lTroopStatTable.Set((int)TroopStat.IsFollowAdvance, 0); // 기본값으로 초기화

			// SightRange
			int lSightRange = pCSVRow.GetIntValue(vSightRangeColumnName);
			lTroopStatTable.Set((int)TroopStat.SightRange, lSightRange);

			// SpawnMp
			lTroopStatTable.Set((int)TroopStat.SpawnMp, pCSVRow.GetIntValue(vSpawnMpColumnName));

			// SpawnCoolTime
			lTroopStatTable.Set((int)TroopStat.SpawnCoolTime, pCSVRow.GetIntValue(vSpawnCoolTimeColumnName));

			// BaseHp
			lTroopStatTable.Set((int)TroopStat.BaseMilliHp, pCSVRow.GetMilliIntValue(vBaseHpColumnName));

			// ArmorSize
			lTroopStatTable.Set((int)TroopStat.ArmorSize, pCSVRow.GetIntValue(vArmorSizeColumnName));

			// FrontArmor
			lTroopStatTable.Set((int)TroopStat.FrontArmor, pCSVRow.GetIntValue(vFrontArmorColumnName));

			// SideArmor
			lTroopStatTable.Set((int)TroopStat.SideArmor, pCSVRow.GetIntValue(vSideArmorColumnName));

			// RearArmor
			lTroopStatTable.Set((int)TroopStat.RearArmor, pCSVRow.GetIntValue(vRearArmorColumnName));

			// AdvanceSpeed
			lTroopStatTable.Set((int)TroopStat.AdvanceSpeed, pCSVRow.GetIntValue(vAdvanceSpeedColumnName));

			// MaxRotationDelay
			lTroopStatTable.Set((int)TroopStat.MaxRotationDelay, pCSVRow.GetIntValue(vMaxRotationDelayColumnName));

			// MoveStartDelay
//			lTroopStatTable.Set((int)TroopStat.MoveStartDelay, pCSVRow.GetIntValue(vMoveStartDelayColumnName));
//			lTroopStatTable.Set((int)TroopStat.MoveStartDelay, 500); // 임시로 0.5초로 초기화

			// SetUpDelay
			lTroopStatTable.Set((int)TroopStat.SetUpDelay, pCSVRow.GetIntValue(vSetUpDelayColumnName));

			// MaxMorale
			lTroopStatTable.Set((int)TroopStat.MaxMilliMorale, pCSVRow.GetMilliIntValue(vMaxMoraleColumnName));

			// MoraleRecover
			lTroopStatTable.Set((int)TroopStat.MoraleMilliRecovery, pCSVRow.GetMilliIntValue(vMoraleRecoverColumnName));

			// SuppressedActivate
			lTroopStatTable.Set((int)TroopStat.SuppressedMilliActivate, pCSVRow.GetMilliIntValue(vSuppressedActivateColumnName));

			// SuppressedRecover
			lTroopStatTable.Set((int)TroopStat.SuppressedMilliRecover, pCSVRow.GetMilliIntValue(vSuppressedRecoverColumnName));

			// PinDownActivate
			lTroopStatTable.Set((int)TroopStat.PinDownMilliActivate, pCSVRow.GetMilliIntValue(vPinDownActivateColumnName));

			// PinDownRecover
			lTroopStatTable.Set((int)TroopStat.PinDownMilliRecover, pCSVRow.GetMilliIntValue(vPinDownRecoverColumnName));

			// AdvanceSpeedMultiplier
			lTroopStatTable.Set((int)TroopStat.SuppressedSpeedMultiplyRatio, pCSVRow.GetRatioValue(vSuppressedSpeedMultiplierColumnName));

			// CooldownDelayMutiplier
			lTroopStatTable.Set((int)TroopStat.CooldownDelayMultiplyRatio, pCSVRow.GetRatioValue(vCooldownDelayMutiplierColumnName));

			// AccuracyMultiplyRatio
			lTroopStatTable.Set((int)TroopStat.AccuracyMultiplyRatio, pCSVRow.GetRatioValue(vAccuracyMutiplierColumnName));

			// NonBattleMultiplyRatio
			lTroopStatTable.Set((int)TroopStat.NonBattleMultiplyRatio, pCSVRow.GetRatioValue(vNonCombatMultiplierColumnName));

			// NonBattleDelay
			lTroopStatTable.Set((int)TroopStat.NonBattleDelay, pCSVRow.GetIntValue(vNonCombatDelayColumnName));

			// MoveOvercomeLevel
			lTroopStatTable.Set((int)TroopStat.MoveOvercomeLevel, pCSVRow.GetIntValue(vMoveOvercomeLevelColumnName));

			// AttackOvercomeLevel
			lTroopStatTable.Set((int)TroopStat.AttackOvercomeLevel, pCSVRow.GetIntValue(vAttackOvercomeLevelColumnName));

			// Experience
			lTroopStatTable.Set((int)TroopStat.Experience, pCSVRow.GetIntValue(vExperienceColumnName));

			// HitRateMilliAdd
			lTroopStatTable.Set((int)TroopStat.HitRateMilliAdd, 0); // 기본값이 0

			// LevelSpec
			lTroopSpec.mRankLevelSpecs = new TroopLevelSpec[CommanderSpec.cRankCount];
			String lLevelSpecNamesField = pCSVRow.GetStringValue(vLevelUpgradeColumnName);
			if (!String.IsNullOrEmpty(lLevelSpecNamesField))
			{
				String[] lLevelSpecNames = lLevelSpecNamesField.Split('^');
				for (int iRank = 1; iRank < lLevelSpecNames.Length; iRank++) // 1랭크부터 시작
					lTroopSpec.mRankLevelSpecs[iRank] = TroopLevelTable.get.FindTroopLevelSpec(lLevelSpecNames[iRank]);
				for (int iRank = lLevelSpecNames.Length; iRank < CommanderSpec.cRankCount; iRank++)
					lTroopSpec.mRankLevelSpecs[iRank] = lTroopSpec.mRankLevelSpecs[iRank - 1]; // 이전 랭크의 스펙 사용
			}
			else
			{
				for (int iRank = 0; iRank < CommanderSpec.cRankCount; iRank++)
					lTroopSpec.mRankLevelSpecs[iRank] = null;
			}

			// VeterancySpec
			lTroopSpec.mVeterancySpecs = new VeterancySpec[BattleConfig.get.mMaxVeterancyRank + 1];
			String lVeteranSpecNamesField = pCSVRow.GetStringValue(vVeterancySpecColumnName);
			if (!String.IsNullOrEmpty(lVeteranSpecNamesField))
			{
				lTroopSpec.mVeterancySpecs[0] = null;

				String[] lVeteranSpecNames = lVeteranSpecNamesField.Split('^');
				for (int iSpec = 0; iSpec < lVeteranSpecNames.Length; iSpec++) // <iRank> == <iSpec + 1>
					lTroopSpec.mVeterancySpecs[iSpec + 1] = VeterancyTable.get.FindVeterancySpec(lVeteranSpecNames[iSpec]);
				for (int iSpec = lVeteranSpecNames.Length; iSpec < BattleConfig.get.mMaxVeterancyRank; iSpec++) // <iRank> == <iSpec + 1>
					lTroopSpec.mVeterancySpecs[iSpec + 1] = lTroopSpec.mVeterancySpecs[iSpec]; // 이전 랭크의 스펙 사용
			}
			else
			{
				for (int iRank = 0; iRank <= BattleConfig.get.mMaxVeterancyRank; iRank++)
					lTroopSpec.mVeterancySpecs[iRank] = null;
			}
		}

		// 파생 정보를 초기화합니다.
// 		_InitExtraInfo(lTroopSpec);

		return lTroopSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
// 	private void _InitExtraInfo(TroopSpec pTroopSpec)
// 	{
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static TroopTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, TroopType> mTroopTypeDictionary;
	private Dictionary<String, TroopDefenseType> mTroopDefenseTypeDictionary;

	private TroopSpec[] mTroopSpecs;
}
