using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class WeaponTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "WeaponTableCSV";

	public readonly String vSpecNoColumnName = "No";
	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";
	public readonly String vNameKeyColumnName = "NameKey";

	public readonly String vAttackTypeColumnName = "AttackType";
//	public readonly String vIsSetUpWeaponColumnName = "IsSetUpWeapon";
	public readonly String vMaxTrackingDelayColumnName = "MaxAimDelay";
	public readonly String vWindupDelayColumnName = "WindupDelay";
	public readonly String vUseDelayColumnName = "UseDelay";
	public readonly String vBulletVelocityColumnName = "BulletVelocity";
	public readonly String vMinRangeColumnName = "MinRange";
	public readonly String vScatterAngleColumnName = "ScatterAngle";
	public readonly String vScatterOffsetColumnName = "ScatterDistance";
	public readonly String vFireConeAngleColumnName = "FireConeAngle";
	public readonly String vTrackingAngleColumnName = "TrackingAngle";
	public readonly String[] vRangeSuppressionPointColumnNames = { "SuppressionPointN", "SuppressionPointM", "SuppressionPointF" };
	public readonly String vSuppressionRangeColumnName = "SuppressionRange";
	public readonly String vSuppressionMultiplierColumnName = "SuppressionMultiplier";

	public readonly String[] vDistanceColumnNames = { "DistanceN", "DistanceM", "DistanceF" };
	public readonly String[] vAimDelayColumnNames = { "FireAimDelayN", "FireAimDelayM", "FireAimDelayF" };
	public readonly String[] vCooldownDelayColumnNames = { "CooldownDelayN", "CooldownDelayM", "CooldownDelayF" };
	public readonly String[] vAccuracyColumnNames = { "AccuracyN", "AccuracyM", "AccuracyF" };
	public readonly String[] vPenetrationColumnNames = { "PenetrationN", "PenetrationM", "PenetrationF" };
	public readonly String[] vAttackDamageColumnNames = { "AttackDamageN", "AttackDamageM", "AttackDamageF" };
	public readonly String[] vSupPointColumnNames = { "SupPointN", "SupPointM", "SupPointF" };
	public readonly String[] vAoeDistanceColumnNames = { "AoeDistanceN", "AoeDistanceM", "AoeDistanceF" };
	public readonly String[] vAoeDamageMultiplierColumnNames = { "AoeDamageMultiplierN", "AoeDamageMultiplierM", "AoeDamageMultiplierF" };
	public readonly String[] vAoePenetrationMultiplierColumnNames = { "AoePenetrationMultiplierN", "AoePenetrationMultiplierM", "AoePenetrationMultiplierF" };

	public readonly bool vIsPreLoadAll = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static WeaponTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new WeaponTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public WeaponTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		DebugUtil.Assert(WeaponSpec.cAttackTypeEnumCount == Enum.GetValues(typeof(WeaponAttackType)).Length, "cAttackTypeEnumCount mismatch - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNoToRowDictionary = new Dictionary<int, int>();
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lSpecNo;
			if (!lCSVRow.TryIntValue(vSpecNoColumnName, out lSpecNo, 0))
				continue; // 번호가 숫자가 아니라면 이 줄을 무시합니다.

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 번호에 대한 줄 인덱스 사전을 구축합니다.
			if (mNoToRowDictionary.ContainsKey(lSpecNo))
			{
				Debug.LogError(String.Format("WeaponTable[{0}] duplicate SpecNo '{1}'", lSpecNo, lSpecNo));
				continue;
			}
			mNoToRowDictionary.Add(lSpecNo, iRow);

			// 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("WeaponTable[{0}] duplicate Name '{1}'", lSpecNo, lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 검색 테이블을 초기화합니다.
		mWeaponAttackTypeDictionary = new Dictionary<String, WeaponAttackType>();
		foreach (WeaponAttackType iForm in Enum.GetValues(typeof(WeaponAttackType)))
			mWeaponAttackTypeDictionary.Add(iForm.ToString(), iForm);

		mGroundAttackFlags = new bool[WeaponSpec.cAttackTypeEnumCount];
		for (int iFlag = 0; iFlag < WeaponSpec.cAttackTypeEnumCount; ++iFlag)
			mGroundAttackFlags[iFlag] = false;
		mGroundAttackFlags[(int)WeaponAttackType.HighAngleGun] = true;

		mAllyTargetingFlags = new bool[WeaponSpec.cAttackTypeEnumCount];
		for (int iFlag = 0; iFlag < WeaponSpec.cAttackTypeEnumCount; ++iFlag)
			mAllyTargetingFlags[iFlag] = false;
		mAllyTargetingFlags[(int)WeaponAttackType.MedKit] = true;
		mAllyTargetingFlags[(int)WeaponAttackType.RepairKit] = true;

		mTroopInteractionTypes = new TroopInteractionType[WeaponSpec.cAttackTypeEnumCount];
		for (int iFlag = 0; iFlag < WeaponSpec.cAttackTypeEnumCount; ++iFlag)
			mTroopInteractionTypes[iFlag] = TroopInteractionType.None;
		mTroopInteractionTypes[(int)WeaponAttackType.MedKit] = TroopInteractionType.Recovery;
		mTroopInteractionTypes[(int)WeaponAttackType.RepairKit] = TroopInteractionType.Recovery;

		// 스펙 테이블을 초기화합니다.
		mWeaponSpecs = new WeaponSpec[mCSVObject.RowNum];

		// 정적 테이블을 초기화합니다.
		WeaponSpec.SetUpStaticProperty();

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllWeaponSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 무기 정보를 모두 읽어옵니다.
	public void LoadAllWeaponSpec()
	{
		Debug.LogWarning("LoadAllWeaponSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mWeaponSpecs[iRow] == null) // _SetUpSpawnLinkWeaponInfo() 속에서 초기화되었을 수도 있습니다.
				mWeaponSpecs[iRow] = _LoadWeaponSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 인덱스의 스펙을 얻습니다.
	public WeaponSpec GetWeaponSpecByIdx(int pWeaponIdxx)
	{
		if (mWeaponSpecs[pWeaponIdxx] == null)
			mWeaponSpecs[pWeaponIdxx] = _LoadWeaponSpec(mCSVObject.GetRow(pWeaponIdxx), pWeaponIdxx);
		return mWeaponSpecs[pWeaponIdxx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 스펙을 찾습니다.
	public WeaponSpec FindWeaponSpec(int pSpecNo)
	{
		int lRowIdx;
		if (mNoToRowDictionary.TryGetValue(pSpecNo, out lRowIdx))
		{
			if (mWeaponSpecs[lRowIdx] == null)
				mWeaponSpecs[lRowIdx] = _LoadWeaponSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mWeaponSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pSpecNo:" + pSpecNo + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public WeaponSpec FindWeaponSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mWeaponSpecs[lRowIdx] == null)
				mWeaponSpecs[lRowIdx] = _LoadWeaponSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mWeaponSpecs[lRowIdx];
		}
		else if (String.Compare(pSpecName, "None") == 0)
		{
			return null;
		}
		else
		{
			Debug.LogError("can't find pSpecName:" + pSpecName + " - " + this);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mWeaponSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("WeaponTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private WeaponSpec _LoadWeaponSpec(CSVRow pCSVRow, int pRowIdx)
	{
		WeaponSpec lWeaponSpec = new WeaponSpec();
	
		// 줄 인덱스
		lWeaponSpec.mIdx = pRowIdx;

		// Name
		lWeaponSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lWeaponSpec.mName);

		// SpecNo
		lWeaponSpec.mNo = pCSVRow.GetIntValue(vSpecNoColumnName);

		// NameText
		lWeaponSpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// NameKey
		lWeaponSpec.mNameKey = pCSVRow.GetStringValue(vNameKeyColumnName);

		// AttackType
		String lAttackType = pCSVRow.GetStringValue(vAttackTypeColumnName);
		if (!mWeaponAttackTypeDictionary.TryGetValue(lAttackType, out lWeaponSpec.mAttackType))
			Debug.LogError(String.Format("{0} invalid AttackType '{1}'", _OnParsingLogPrefix(lWeaponSpec.mName), lAttackType));

		// IsSetUpWeapon
//		lWeaponSpec.mIsSetUpWeapon = pCSVRow.GetBoolValue(vIsSetUpWeaponColumnName);
//		lWeaponSpec.mIsSetUpWeapon = false;
//		lWeaponSpec.mIsSetUpWeapon = (lWeaponSpec.mAttackType == WeaponAttackType.MachineGun);

		// WeaponStat
		lWeaponSpec.mStatTable = new WeaponStatTable();
		{
			WeaponStatTable lWeaponStatTable = lWeaponSpec.mStatTable;

			// MaxTrackingDelay
			lWeaponStatTable.Set((int)WeaponStat.MaxTrackingDelay, pCSVRow.GetIntValue(vMaxTrackingDelayColumnName));

			// WindupDelay
			lWeaponStatTable.Set((int)WeaponStat.WindupDelay, pCSVRow.GetIntValue(vWindupDelayColumnName));

			// UseDelay
			lWeaponStatTable.Set((int)WeaponStat.UseDelay, pCSVRow.GetIntValue(vUseDelayColumnName));

			// BulletVelocity
			lWeaponStatTable.Set((int)WeaponStat.BulletVelocity, pCSVRow.GetIntValue(vBulletVelocityColumnName));

			// MinRange
			int lMinRange = pCSVRow.GetIntValue(vMinRangeColumnName);
			lWeaponStatTable.Set((int)WeaponStat.MinRange, lMinRange);

			// ScatterAngleHalf
			lWeaponStatTable.Set((int)WeaponStat.ScatterAngleHalf, pCSVRow.GetIntValue(vScatterAngleColumnName) / 2); // Half

			// ScatterOffset
			lWeaponStatTable.Set((int)WeaponStat.ScatterOffset, pCSVRow.GetIntValue(vScatterOffsetColumnName) / 2); // 컬럼에 있는 ScatterDistance의 절반이 오프셋 값이다.

			// FireConeAngleHalf
			lWeaponStatTable.Set((int)WeaponStat.FireConeAngleHalf, pCSVRow.GetIntValue(vFireConeAngleColumnName) / 2); // Half

			// TrackingAngleHalf
			lWeaponStatTable.Set((int)WeaponStat.TrackingAngleHalf, pCSVRow.GetIntValue(vTrackingAngleColumnName) / 2); // Half

					lWeaponStatTable.Set((int)WeaponStat.MoveHoldLevel, 1);
					lWeaponStatTable.Set((int)WeaponStat.MoveHoldDelay, 1);
					lWeaponStatTable.Set((int)WeaponStat.AttackHoldLevel, 1);
					lWeaponStatTable.Set((int)WeaponStat.AttackHoldDelay, 1);

			// SuppressionRange
			lWeaponStatTable.Set((int)WeaponStat.SuppressionRange, pCSVRow.GetIntValue(vSuppressionRangeColumnName));

			// SuppressionMultiplyRatio
			lWeaponStatTable.Set((int)WeaponStat.SuppressionMultiplyRatio, pCSVRow.GetRatioValue(vSuppressionMultiplierColumnName));
		}

		// WeaponRangeStat
		lWeaponSpec.mRangeStatTables = new WeaponRangeStatTable[WeaponSpec.cRangeLevelCount];
		for (int iRange = 0; iRange < WeaponSpec.cRangeLevelCount; ++iRange)
		{
			WeaponRangeStatTable lRangeStatTable = new WeaponRangeStatTable();
			lWeaponSpec.mRangeStatTables[iRange] = lRangeStatTable;

			// Range
			int lRange = pCSVRow.GetIntValue(vDistanceColumnNames[iRange]);
			lRangeStatTable.Set((int)WeaponRangeStat.Range, lRange);

			// AttackDelayMultiplyRatio
			lRangeStatTable.Set((int)WeaponRangeStat.AttackDelayMultiplyRatio, 10000);

			// AimDelay
			lRangeStatTable.Set((int)WeaponRangeStat.AimDelay, pCSVRow.GetIntValue(vAimDelayColumnNames[iRange]));

			// CooldownDelay
			lRangeStatTable.Set((int)WeaponRangeStat.CooldownDelay, pCSVRow.GetIntValue(vCooldownDelayColumnNames[iRange]));
			if (lRangeStatTable.Get((int)WeaponRangeStat.CooldownDelay) < WeaponRangeStatTable.cMinCooldownDelay)
				Debug.LogError(String.Format("{0} invalid or too small CooldownDelay '{1}'", _OnParsingLogPrefix(lWeaponSpec.mName), vCooldownDelayColumnNames[iRange]));

			// MilliAccuracy
			lRangeStatTable.Set((int)WeaponRangeStat.MilliAccuracy, pCSVRow.GetMilliIntValue(vAccuracyColumnNames[iRange]));

			// Penetration
			lRangeStatTable.Set((int)WeaponRangeStat.Penetration, pCSVRow.GetIntValue(vPenetrationColumnNames[iRange]));
			if (lRangeStatTable.Get((int)WeaponRangeStat.Penetration) <= 0)
				Debug.LogError(String.Format("{0} invalid Penetration '{1}'", _OnParsingLogPrefix(lWeaponSpec.mName), vPenetrationColumnNames[iRange]));

			// MilliDamage
			lRangeStatTable.Set((int)WeaponRangeStat.MilliDamage, pCSVRow.GetMilliIntValue(vAttackDamageColumnNames[iRange]));

			// RangeSuppressionMilliPoint
			lRangeStatTable.Set((int)WeaponRangeStat.RangeSuppressionMilliPoint, pCSVRow.GetMilliIntValue(vRangeSuppressionPointColumnNames[iRange]));
		}

		// WeaponAoeStat
		lWeaponSpec.mAoeStatTables = new WeaponAoeStatTable[WeaponSpec.cAoeLevelCount];
		for (int iAoe = 0; iAoe < WeaponSpec.cAoeLevelCount; ++iAoe)
		{
			WeaponAoeStatTable lAoeStatTable = new WeaponAoeStatTable();
			lWeaponSpec.mAoeStatTables[iAoe] = lAoeStatTable;

			// Radius
			int lRadius = pCSVRow.GetIntValue(vAoeDistanceColumnNames[iAoe]);
			lAoeStatTable.Set((int)WeaponAoeStat.Radius, lRadius);

			// DamageMultiplyRatio
			lAoeStatTable.Set((int)WeaponAoeStat.DamageMultiplyRatio, pCSVRow.GetRatioValue(vAoeDamageMultiplierColumnNames[iAoe]));

			// AoeSuppressionMilliPoint
			lAoeStatTable.Set((int)WeaponAoeStat.AoeSuppressionMilliPoint, pCSVRow.GetMilliIntValue(vSupPointColumnNames[iAoe]));

			// PenetrationMultiplyRatio
			lAoeStatTable.Set((int)WeaponAoeStat.PenetrationMultiplyRatio, pCSVRow.GetRatioValue(vAoePenetrationMultiplierColumnNames[iAoe]));
		}

		// 파생 정보를 초기화합니다.
		_InitExtraInfo(lWeaponSpec);

		return lWeaponSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파생 정보를 초기화합니다.
	private void _InitExtraInfo(WeaponSpec pWeaponSpec)
	{
		// WeaponTargeting
		switch (pWeaponSpec.mAttackType)
		{
		case WeaponAttackType.MedKit:
			
			pWeaponSpec.mTargeting = WeaponTargeting.Soldier;
			break;
		case WeaponAttackType.RepairKit:
			pWeaponSpec.mTargeting = WeaponTargeting.Vehicle;
			break;
		default:
			pWeaponSpec.mTargeting = WeaponTargeting.All;
			break;
		}

		// IsGroundAttack
		pWeaponSpec.mIsGroundAttack = mGroundAttackFlags[(int)pWeaponSpec.mAttackType];

		// IsAllyTargeting
		pWeaponSpec.mIsAllyTargeting = mAllyTargetingFlags[(int)pWeaponSpec.mAttackType];

		// TroopInteractionType
		pWeaponSpec.mTroopInteractionType = mTroopInteractionTypes[(int)pWeaponSpec.mAttackType];
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static WeaponTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mNoToRowDictionary;
	private Dictionary<String, int> mNameToRowDictionary;

	private Dictionary<String, WeaponAttackType> mWeaponAttackTypeDictionary;

	private WeaponSpec[] mWeaponSpecs;

	private bool[] mGroundAttackFlags;
	private bool[] mAllyTargetingFlags;
	private TroopInteractionType[] mTroopInteractionTypes;
}
