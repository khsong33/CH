﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TroopLevelTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "TroopUpgradeTableCSV";

	public readonly String vNameColumnName = "Name";
	// TroopStat
	public readonly String vSightRangeRatioColumnName = "T_SightRangeMultiplier";
	public readonly String vBaseHpRatioColumnName = "T_BaseHpMultiplier";
	public readonly String vArmorRatioColumn = "T_ArmorMultiplier";
	public readonly String vAdvanceSpeedRatioColumn = "T_AdvanceSpeedMultiplier";
	public readonly String vMaxRotationDelayRatioColumn = "T_MaxRotationDelayMultiplier";
	public readonly String vSetUpDelayRatioColumn = "T_SetUpDelayMultiplier";
	public readonly String vMaxMoraleRatioColumn = "T_MaxMoraleMultiplier";
	public readonly String vMoraleRecoverRatioColumn = "T_MoraleRecoverMultiplier";
	// WeaponStat
	public readonly String vPrimaryDamageRatioColumnName = "PW_DamageMultiplier";
	public readonly String vSecondaryDamageRatioColumnName = "SW_DamageMultiplier";
	public readonly String vPrimaryWeaponMaxTrackingDelayRatioColumnName = "PW_MaxAimDelayMultiplier";
	public readonly String vSecondaryWeaponMaxTrackingDelayRatioColumnName = "SW_MaxAimDelayMultiplier";
	public readonly String vPrimaryWindupDelayRatioColumnName = "PW_WindupDelayMultiplier";
	public readonly String vSecondaryWindupDelayRatioColumnName = "SW_WindupDelayMultiplier";
	public readonly String vPrimaryScatterDistanceRatioColumnName = "PW_ScatterDistanceMultiplier";
	public readonly String vSecondaryScatterDistanceRatioColumnName = "SW_ScatterDistanceMultiplier";
	// WeaponRangeStat
	public readonly String vPrimaryCooldownDelayRatioColumnName = "PW_CooldownDelayMultiplier";
	public readonly String vSecondaryCooldownDelayRatioColumnName = "SW_CooldownDelayMultiplier";
	public readonly String vPrimaryAccuracyMilliRatioColumnName = "PW_AccuracyMultiplier";
	public readonly String vSecondaryAccuracyMilliRatioColumnName = "SW_AccuracyMultiplier";
	public readonly String vPrimaryPenetrationRatioColumnName = "PW_PenetrationMultiplier";
	public readonly String vSecondaryPenetrationRatioColumnName = "SW_PenetrationMultiplier";

	public readonly bool vIsPreLoadAll = false;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static TroopLevelTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new TroopLevelTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public TroopLevelTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("duplicate spec Name '{0}'", lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 스펙 테이블을 초기화합니다.
		mTroopLevelSpecs = new TroopLevelSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllTroopLevelSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllTroopLevelSpec()
	{
		Debug.LogWarning("LoadAllTroopLevelSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mTroopLevelSpecs[iRow] == null) // _SetUpSpawnLinkCommanderInfo() 속에서 초기화되었을 수도 있습니다.
				mTroopLevelSpecs[iRow] = _LoadTroopLevelSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public TroopLevelSpec FindTroopLevelSpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mTroopLevelSpecs[lRowIdx] == null)
				mTroopLevelSpecs[lRowIdx] = _LoadTroopLevelSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mTroopLevelSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find spec name - " + pSpecName);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mTroopLevelSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("TroopLevelTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private TroopLevelSpec _LoadTroopLevelSpec(CSVRow pCSVRow, int pRowIdx)
	{
		TroopLevelSpec lTroopLevelSpec = new TroopLevelSpec();

		// 줄 인덱스
		lTroopLevelSpec.mIdx = pRowIdx;

		// Name
		lTroopLevelSpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lTroopLevelSpec.mName);

		// StatTableModifier<TroopStat>
		lTroopLevelSpec.mTroopStatTableModifier = new StatTableModifier<TroopStat>(TroopLevelSpec.cTroopMaxLevel);

		StatTableModifier<TroopStat> lTroopStatTableModifier = lTroopLevelSpec.mTroopStatTableModifier;
		{
			int lModifyValue;

			// T_SightRangeAdd
			if (pCSVRow.TryRatioValue(vSightRangeRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SightRange, StatModifyType.Ratio, lModifyValue);

			// T_BaseHpAdd
			if (pCSVRow.TryRatioValue(vBaseHpRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.BaseMilliHp, StatModifyType.Ratio, lModifyValue);

			// T_ArmorAdd
			if (pCSVRow.TryRatioValue(vArmorRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
			{
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.FrontArmor, StatModifyType.Ratio, lModifyValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SideArmor, StatModifyType.Ratio, lModifyValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.RearArmor, StatModifyType.Ratio, lModifyValue);
			}

			// T_AdvanceSpeedAdd
			if (pCSVRow.TryRatioValue(vAdvanceSpeedRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.AdvanceSpeed, StatModifyType.Ratio, lModifyValue);

			// T_MaxRotationDelayAdd
			if (pCSVRow.TryRatioValue(vMaxRotationDelayRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MaxRotationDelay, StatModifyType.Ratio, lModifyValue);

			// T_SetUpDelayAdd
			if (pCSVRow.TryRatioValue(vSetUpDelayRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SetUpDelay, StatModifyType.Ratio, lModifyValue);

			// T_MaxMoraleAdd
			if (pCSVRow.TryRatioValue(vMaxMoraleRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MaxMilliMorale, StatModifyType.Ratio, lModifyValue);

			// T_MoraleRecoverAdd
			if (pCSVRow.TryRatioValue(vMoraleRecoverRatioColumn, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MoraleMilliRecovery, StatModifyType.Ratio, lModifyValue);
		}

		// StatTableModifier<WeaponStat>
		lTroopLevelSpec.mWeaponStatTableModifiers = new StatTableModifier<WeaponStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopLevelSpec.mWeaponStatTableModifiers[iWeapon] = new StatTableModifier<WeaponStat>(TroopLevelSpec.cTroopMaxLevel);

 		StatTableModifier<WeaponStat> lPrimaryWeaponStatTableModifier = lTroopLevelSpec.mWeaponStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
 		StatTableModifier<WeaponStat> lSecondaryWeaponStatTableModifier = lTroopLevelSpec.mWeaponStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
 		{
 			int lModifyValue;

			// PW_MaxTrackingDelayAdd
			if (pCSVRow.TryRatioValue(vPrimaryWeaponMaxTrackingDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyValue);
			//SW_MaxTrackingDelayAdd
			if (pCSVRow.TryRatioValue(vSecondaryWeaponMaxTrackingDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyValue);

			// PW_WindupDelayAdd
			if (pCSVRow.TryRatioValue(vPrimaryWindupDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.WindupDelay, StatModifyType.Ratio, lModifyValue);
			// SW_WindupDelayAdd
			if (pCSVRow.TryRatioValue(vSecondaryWindupDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.WindupDelay, StatModifyType.Ratio, lModifyValue);

			// PW_ScatterDistanceAdd
			if (pCSVRow.TryRatioValue(vPrimaryScatterDistanceRatioColumnName, out lModifyValue, 0))
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterOffset, StatModifyType.Ratio, lModifyValue / 2);
			// SW_ScatterDistanceAdd
			if (pCSVRow.TryRatioValue(vSecondaryScatterDistanceRatioColumnName, out lModifyValue, 0))
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterOffset, StatModifyType.Ratio, lModifyValue / 2);
		}

		// StatTableModifier<WeaponRangeStat>
		lTroopLevelSpec.mWeaponRangeStatTableModifiers = new StatTableModifier<WeaponRangeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopLevelSpec.mWeaponRangeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponRangeStat>(TroopLevelSpec.cTroopMaxLevel);

 		StatTableModifier<WeaponRangeStat> lPrimaryWeaponRangeStatTableModifier = lTroopLevelSpec.mWeaponRangeStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
 		StatTableModifier<WeaponRangeStat> lSecondaryWeaponRangeStatTableModifier = lTroopLevelSpec.mWeaponRangeStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
 		{
 			int lModifyValue;

			//PW_CooldownDelayAdd
			if (pCSVRow.TryRatioValue(vPrimaryCooldownDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.CooldownDelay, StatModifyType.Ratio, lModifyValue);
			// SW_CooldownDelayAdd
			if (pCSVRow.TryRatioValue(vSecondaryCooldownDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.CooldownDelay, StatModifyType.Ratio, lModifyValue);

			// PW_AccuracyAdd
			if (pCSVRow.TryRatioValue(vPrimaryAccuracyMilliRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyValue);
			// SW_AccuracyAdd
			if (pCSVRow.TryRatioValue(vSecondaryAccuracyMilliRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyValue);

			// PW_PenetrationAdd
			if (pCSVRow.TryRatioValue(vPrimaryPenetrationRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyValue);
			// SW_PenetrationAdd
			if (pCSVRow.TryRatioValue(vSecondaryPenetrationRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyValue);
		}

		// StatTableModifier<WeaponAoeStat>
		lTroopLevelSpec.mWeaponAoeStatTableModifiers = new StatTableModifier<WeaponAoeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lTroopLevelSpec.mWeaponAoeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponAoeStat>(TroopLevelSpec.cTroopMaxLevel);

		return lTroopLevelSpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static TroopLevelTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private TroopLevelSpec[] mTroopLevelSpecs;
}
