﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public class TutorialSpec
{
	public int mIndex;
	public int mNo;
	public int mStageNo;
	public int mOrder;
	public CommanderItemData mCommanderItemData;
	public List<AssetKey> mGuideAssetKeyList;

	public TutorialSpec()
	{
		mGuideAssetKeyList = new List<AssetKey>();
	}
}

public class TutorialTable
{
	public readonly String cCSVTextAssetPath = "TutorialTableCSV";

	public static readonly String vNoColumnName = "NO";
	public static readonly String vStageNoColumnName = "StageId";
	public static readonly String vOrderColumnName = "Order";
	public static readonly String vCommanderSpecNoColumnName = "CommanderId";
	public static readonly String vGuideBundleColumnName = "GuideBundle";
	public static readonly String vGuidePathColumnName = "GuidePath";
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static TutorialTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 갯수
	public int aTutorialCount { get { return mTutorialSpecList.Count; } }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new TutorialTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public TutorialTable()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load() // 예외적으로 pIsPreLoadAll=true로 가정
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 미리 로딩합니다.
		mTutorialSpecList = new List<TutorialSpec>();
		mDicStageNoToSpec = new Dictionary<int, TutorialSpec>();
		LoadAllTutorial();
	}
	//-------------------------------------------------------------------------------------------------------
	// 정보를 모두 읽어옵니다.
	public void LoadAllTutorial()
	{
		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			TutorialSpec lTutorialSpec = _LoadTutorialSpec(mCSVObject.GetRow(iRow), iRow);
			mTutorialSpecList.Add(lTutorialSpec);
			mDicStageNoToSpec.Add(lTutorialSpec.mStageNo, lTutorialSpec);
		}
		// Order 순 정렬
//		mTutorialSpecList.Sort((lData, rData) =>
		SortUtil.SortList<TutorialSpec>(mTutorialSpecList, (lData, rData) =>
		{
			if (lData.mOrder > rData.mOrder)
				return 1;
			else if (lData.mOrder < rData.mOrder)
				return -1;
			return 0;
		});

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 서버에서 얻은 완료 Index로 튜토리얼 정보를 얻어옵니다.
	public TutorialSpec GetTutorialSpecByClearIdx(int pClearIndex)
	{
		if (pClearIndex < 0)
			return null;
		else if (pClearIndex >= mTutorialSpecList.Count)
			return null;

		return mTutorialSpecList[pClearIndex];
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 아이디로 튜토리얼 정보를 얻어옵니다.
	public TutorialSpec GetTutorialSpecByStageNo(int pStageNo)
	{
		if (!mDicStageNoToSpec.ContainsKey(pStageNo))
		{
			//Debug.LogError("Not Exist Tutorial Stage Id - " + pStageNo);
			return null;
		}
		return mDicStageNoToSpec[pStageNo];
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 확인
	public bool IsTutorial(int pStageNo)
	{
		return mDicStageNoToSpec.ContainsKey(pStageNo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("TutorialTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 로드합니다.
	private TutorialSpec _LoadTutorialSpec(CSVRow pCSVRow, int pRowIndex)
	{
		TutorialSpec lTutorialSpec = new TutorialSpec();

		// 줄 인덱스
		lTutorialSpec.mIndex = pRowIndex;
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, pRowIndex.ToString());

		// No
		lTutorialSpec.mNo = pCSVRow.GetIntValue(vNoColumnName);
		// StageNo
		lTutorialSpec.mStageNo = pCSVRow.GetIntValue(vStageNoColumnName);
		// Order
		lTutorialSpec.mOrder = pCSVRow.GetIntValue(vOrderColumnName);
		// CommanderId
		int lCommanderSpecNo = pCSVRow.GetIntValue(vCommanderSpecNoColumnName);
		lTutorialSpec.mCommanderItemData = new CommanderItemData();
		lTutorialSpec.mCommanderItemData.Init(lCommanderSpecNo);
		// Guide Asset key
		String[] lGuidePathColumn = vGuidePathColumnName.Split('^');
		for (int iPath = 0; iPath < lGuidePathColumn.Length; iPath++)
		{
			AssetKey lGuidePathAssetKey = AssetManager.get.CreateAssetKey(
												pCSVRow.GetStringValue(vGuideBundleColumnName),
												pCSVRow.GetStringValue(vGuidePathColumnName));
			lTutorialSpec.mGuideAssetKeyList.Add(lGuidePathAssetKey); 
		}
		
		return lTutorialSpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static TutorialTable sInstance = null;

	private CSVObject mCSVObject;
	private List<TutorialSpec> mTutorialSpecList;
	private Dictionary<int, TutorialSpec> mDicStageNoToSpec;
}
