﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VeterancySpec
{
	public int mIdx;

	public String mName;
	public String mNameText;
	public String mDescKey;

	public bool mVeteranWeaponEnable;

	public StatTableModifier<TroopStat> mTroopStatTableModifier;
	public StatTableModifier<WeaponStat>[] mWeaponStatTableModifiers;
	public StatTableModifier<WeaponRangeStat>[] mWeaponRangeStatTableModifiers;
	public StatTableModifier<WeaponAoeStat>[] mWeaponAoeStatTableModifiers;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString()
	public override string ToString()
	{
		return String.Format("<VeterancySpec[{0}] Name={1}>", mIdx, mName);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
