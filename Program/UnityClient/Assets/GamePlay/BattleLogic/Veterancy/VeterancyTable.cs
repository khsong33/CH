﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class VeterancyTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "VeteranEffectTableCSV";

	public readonly String vNameColumnName = "Name";
	public readonly String vNameTextColumnName = "NameText";
	public readonly String vDescKeyColumnName = "DescKey";

	public readonly String vVeteranWeaponEnableColumnName = "T_VeteranWeaponEnable";

	public readonly String vMaxRotationDelayRatioColumnName = "T_MaxRotationDelayMultiplier";
	public readonly String vAdvanceSpeedRatioColumnName = "T_AdvanceSpeedMultiplier";
	public readonly String vArmorRatioColumnName = "T_ArmorMultiplier";
	public readonly String vSightRangeRatioColumnName = "T_SightRangeMultiplier";
	public readonly String vBaseHpRatioColumnName = "T_BaseHpMultiplier";
	public readonly String vSetUpDelayAddColumnName = "T_SetUpDelayAdd";
	public readonly String vHitRateAddColumnName = "C_HitRateAdd";

	public readonly String vPrimaryWeaponMaxTrackingDelayRatioColumnName = "PW_MaxAimDelayMultiplier";
	public readonly String vSecondaryWeaponMaxTrackingDelayRatioColumnName = "SW_MaxAimDelayMultiplier";
	public readonly String vPrimaryWeaponScatterAngleRatioColumnName = "PW_ScatterAngleMultiplier";
	public readonly String vSecondaryWeaponScatterAngleRatioColumnName = "SW_ScatterAngleMultiplier";

	public readonly String vPrimaryWeaponRangeRatioColumnName = "PW_DistanceMultiplier";
	public readonly String vSecondaryWeaponRangeRatioColumnName = "SW_DistanceMultiplier";
	public readonly String vPrimaryWeaponAccuracyRatioColumnName = "PW_AccuracyMultiplier";
	public readonly String vSecondaryWeaponAccuracyRatioColumnName = "SW_AccuracyMultiplier";
	public readonly String vPrimaryWeaponAttackDelayRatioColumnName = "PW_CooldownMultiplier";
	public readonly String vSecondaryWeaponAttackDelayRatioColumnName = "SW_CooldownMultiplier";
	public readonly String vPrimaryWeaponAttackDamageRatioColumnName = "PW_AttackDamageMultiplier";
	public readonly String vSecondaryWeaponAttackDamageRatioColumnName = "SW_AttackDamageMultiplier";
	public readonly String vPrimaryWeaponPenetrationRatioColumnName = "PW_PenetrationMultiplier";
	public readonly String vSecondaryWeaponPenetrationRatioColumnName = "SW_PenetrationMultiplier";

	public readonly String vPrimaryWeaponSuppressionPointRatioColumnName = "PW_SuppressionPointMultiplier";
	public readonly String vSecondaryWeaponSuppressionPointRatioColumnName = "SW_SuppressionPointMultiplier";

	public readonly bool vIsPreLoadAll = false;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static VeterancyTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
						

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new VeterancyTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public VeterancyTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mNameToRowDictionary = new Dictionary<String, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			String lSpecName;
			if (!lCSVRow.TryStringValue(vNameColumnName, out lSpecName, String.Empty))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mNameToRowDictionary.ContainsKey(lSpecName))
			{
				Debug.LogError(String.Format("duplicate spec Name '{0}'", lSpecName));
				continue;
			}
			mNameToRowDictionary.Add(lSpecName, iRow);
		}

		// 스펙 테이블을 초기화합니다.
		mVeterancySpecs = new VeterancySpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllVeterancySpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllVeterancySpec()
	{
		Debug.LogWarning("LoadAllVeterancySpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mVeterancySpecs[iRow] == null) // _SetUpSpawnLinkCommanderInfo() 속에서 초기화되었을 수도 있습니다.
				mVeterancySpecs[iRow] = _LoadVeterancySpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public VeterancySpec FindVeterancySpec(String pSpecName)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mNameToRowDictionary.TryGetValue(pSpecName, out lRowIdx))
		{
			if (mVeterancySpecs[lRowIdx] == null)
				mVeterancySpecs[lRowIdx] = _LoadVeterancySpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mVeterancySpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find spec name - " + pSpecName);
			return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mVeterancySpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("VeterancyTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유니트 스펙을 로딩합니다.
	private VeterancySpec _LoadVeterancySpec(CSVRow pCSVRow, int pRowIdx)
	{
		VeterancySpec lVeterancySpec = new VeterancySpec();

		// 줄 인덱스
		lVeterancySpec.mIdx = pRowIdx;

		// Name
		lVeterancySpec.mName = pCSVRow.GetStringValue(vNameColumnName);
		pCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, lVeterancySpec.mName);

		// NameText
		lVeterancySpec.mNameText = pCSVRow.GetStringValue(vNameTextColumnName);

		// DescKey
		lVeterancySpec.mDescKey = pCSVRow.GetStringValue(vDescKeyColumnName);

		// T_VeteranWeaponEnable
		lVeterancySpec.mVeteranWeaponEnable = pCSVRow.GetBoolValue(vVeteranWeaponEnableColumnName);

		// StatTableModifier<TroopStat>
		lVeterancySpec.mTroopStatTableModifier = new StatTableModifier<TroopStat>();

		StatTableModifier<TroopStat> lTroopStatTableModifier = lVeterancySpec.mTroopStatTableModifier;
		{
			int lModifyValue;

			// T_MaxRotationDelayMultiplier
			if (pCSVRow.TryRatioValue(vMaxRotationDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.MaxRotationDelay, StatModifyType.Ratio, lModifyValue);

			// T_AdvanceSpeedMultiplier
			if (pCSVRow.TryRatioValue(vAdvanceSpeedRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.AdvanceSpeed, StatModifyType.Ratio, lModifyValue);

			// T_ArmorMultiplier
			if (pCSVRow.TryRatioValue(vArmorRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
			{
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.FrontArmor, StatModifyType.Ratio, lModifyValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SideArmor, StatModifyType.Ratio, lModifyValue);
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.RearArmor, StatModifyType.Ratio, lModifyValue);
			}

			// T_SightRangeMultiplier
			if (pCSVRow.TryRatioValue(vSightRangeRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SightRange, StatModifyType.Ratio, lModifyValue);

			// T_BaseHpMultiplier
			if (pCSVRow.TryRatioValue(vBaseHpRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.BaseMilliHp, StatModifyType.Ratio, lModifyValue);

			// T_SetUpDelayAdd
			if (pCSVRow.TryIntValue(vSetUpDelayAddColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.SetUpDelay, StatModifyType.Add, lModifyValue);

			// C_HitRateAdd
			if (pCSVRow.TryMilliIntValue(vHitRateAddColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lTroopStatTableModifier.AddModifyIntStat(TroopStat.HitRateMilliAdd, StatModifyType.Add, lModifyValue);
		}

		// StatTableModifier<WeaponStat>
		lVeterancySpec.mWeaponStatTableModifiers = new StatTableModifier<WeaponStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lVeterancySpec.mWeaponStatTableModifiers[iWeapon] = new StatTableModifier<WeaponStat>();

		StatTableModifier<WeaponStat> lPrimaryWeaponStatTableModifier = lVeterancySpec.mWeaponStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponStat> lSecondaryWeaponStatTableModifier = lVeterancySpec.mWeaponStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyValue;

			// PW_MaxTrackingDelayMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponMaxTrackingDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyValue);
			//SW_MaxTrackingDelayMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponMaxTrackingDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.MaxTrackingDelay, StatModifyType.Ratio, lModifyValue);

			// PW_ScatterAngleMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponScatterAngleRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterAngleHalf, StatModifyType.Ratio, lModifyValue);
			// SW_ScatterAngleMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponScatterAngleRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponStatTableModifier.AddModifyIntStat(WeaponStat.ScatterAngleHalf, StatModifyType.Ratio, lModifyValue);
		}

		// StatTableModifier<WeaponRangeStat>
		lVeterancySpec.mWeaponRangeStatTableModifiers = new StatTableModifier<WeaponRangeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lVeterancySpec.mWeaponRangeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponRangeStat>();

		StatTableModifier<WeaponRangeStat> lPrimaryWeaponRangeStatTableModifier = lVeterancySpec.mWeaponRangeStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponRangeStat> lSecondaryWeaponRangeStatTableModifier = lVeterancySpec.mWeaponRangeStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyValue;

			// PW_DistanceMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponRangeRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Range, StatModifyType.Ratio, lModifyValue);
			// SW_DistanceMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponRangeRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Range, StatModifyType.Ratio, lModifyValue);

			// PW_AccuracyMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAccuracyRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyValue);
			// SW_AccuracyMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAccuracyRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliAccuracy, StatModifyType.Ratio, lModifyValue);

			// PW_CooldownMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAttackDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.CooldownDelay, StatModifyType.Ratio, lModifyValue);
			// SW_CooldownMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAttackDelayRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.CooldownDelay, StatModifyType.Ratio, lModifyValue);

			// PW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponSuppressionPointRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.RangeSuppressionMilliPoint, StatModifyType.Ratio, lModifyValue);
			// SW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponSuppressionPointRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.RangeSuppressionMilliPoint, StatModifyType.Ratio, lModifyValue);

			// PW_AttackDamageMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponAttackDamageRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliDamage, StatModifyType.Ratio, lModifyValue);
			// SW_AttackDamageMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponAttackDamageRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.MilliDamage, StatModifyType.Ratio, lModifyValue);

			// PW_PenetrationMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponPenetrationRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyValue);
			// SW_PenetrationMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponPenetrationRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponRangeStatTableModifier.AddModifyIntStat(WeaponRangeStat.Penetration, StatModifyType.Ratio, lModifyValue);
		}

		// StatTableModifier<WeaponAoeStat>
		lVeterancySpec.mWeaponAoeStatTableModifiers = new StatTableModifier<WeaponAoeStat>[TroopSpec.cWeaponCount];
		for (int iWeapon = 0; iWeapon < TroopSpec.cWeaponCount; ++iWeapon)
			lVeterancySpec.mWeaponAoeStatTableModifiers[iWeapon] = new StatTableModifier<WeaponAoeStat>();

		StatTableModifier<WeaponAoeStat> lPrimaryWeaponAoeStatTableModifier = lVeterancySpec.mWeaponAoeStatTableModifiers[TroopSpec.cPrimaryWeaponIdx];
		StatTableModifier<WeaponAoeStat> lSecondaryWeaponAoeStatTableModifier = lVeterancySpec.mWeaponAoeStatTableModifiers[TroopSpec.cSecondaryWeaponIdx];
		{
			int lModifyValue;

			// PW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vPrimaryWeaponSuppressionPointRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lPrimaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.AoeSuppressionMilliPoint, StatModifyType.Ratio, lModifyValue);
			// SW_SuppressionPointMultiplier
			if (pCSVRow.TryRatioValue(vSecondaryWeaponSuppressionPointRatioColumnName, out lModifyValue, 0)) // 기본값 0(의미 없음)
				lSecondaryWeaponAoeStatTableModifier.AddModifyIntStat(WeaponAoeStat.AoeSuppressionMilliPoint, StatModifyType.Ratio, lModifyValue);
		}

		return lVeterancySpec;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static VeterancyTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<String, int> mNameToRowDictionary;

	private VeterancySpec[] mVeterancySpecs;
}
