using UnityEngine;
using System.Collections;
using System;

public class BattleNetwork : MonoBehaviour
{
	public const int cMaxClientCount = 6;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattlePlay vBattlePlay;
	public BattleProtocolLocal vBattleProtocolLocal;
	public BattleProtocolNodeJS vBattleProtocolNodeJS;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 접속 프로토콜
	public IConnectionProtocol aIConnectionProtocol
	{
		get { return mIConnectionProtocol; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 프로토콜
	public IBattleProtocol aIBattleProtocol
	{
		get { return mIBattleProtocol; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 프레임 핑
	public BattleFramePing aNextFramePing
	{
		get { return mNextFramePing; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 클라이언트 GUID
	public uint aClientGuid { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);
	
		if (vBattlePlay == null)
			Debug.LogError("vBattlePlay is empty - " + this);

		if (vBattleProtocolLocal == null)
			Debug.LogError("vBattleProtocolLocal is empty - " + this);
		if (vBattleProtocolNodeJS == null)
			Debug.LogError("vBattleProtocolNodeJS is empty - " + this);

		bool lIsLocalProtocol = String.IsNullOrEmpty(GameData.get.aBattleServerURL);

		vBattleProtocolLocal.gameObject.SetActive(lIsLocalProtocol);
		vBattleProtocolNodeJS.gameObject.SetActive(!lIsLocalProtocol);

		if (lIsLocalProtocol)
		{
			mIConnectionProtocol = vBattleProtocolLocal;
			mIBattleProtocol = vBattleProtocolLocal;
		}
		else
		{
			mIConnectionProtocol = vBattleProtocolNodeJS;
			mIBattleProtocol = vBattleProtocolNodeJS;
		}

		mNextFrameNo = 1; // 서버에서 처음 받는 틱이 프레임 번호 1번부터 오는데, 그 틱에 대한 핑이므로 같은 1부터 시작합니다.
		mNextFramePing = BattleFramePing.Create(mNextFrameNo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 다음 프레임 핑을 준비합니다.
	public void PrepareNextFramePing()
	{
		mNextFramePing.Destroy();
		mNextFramePing = BattleFramePing.Create(++mNextFrameNo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_SpawnCommander(BattleUser pUser, CommanderItem pCommanderItem, Coord2 pMoveCoord)
	{
		int lInputIdx = _GetCommanderItemInputIdx(pCommanderItem, BattleFrameInputType.SpawnCommander);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.SpawnCommander, // mType
			pMoveCoord.x, // [mX]
			pMoveCoord.z, // [mY]
			0, // [mZ]
			pUser.aUserIdx, // [mA]
			pCommanderItem.mSlotIdx, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_SpawnCommanderAndAttackTroop(BattleUser pUser, CommanderItem pCommanderItem, BattleTroop pTargetTroop)
	{
		int lInputIdx = _GetCommanderItemInputIdx(pCommanderItem, BattleFrameInputType.SpawnCommanderAndAttackTroop);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.SpawnCommanderAndAttackTroop, // mType
			pTargetTroop.aTroopGridIdx, // [mX]
			(int)pTargetTroop.aGuid, // [mY]
			0, // [mZ]
			pUser.aUserIdx, // [mA]
			pCommanderItem.mSlotIdx, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_SpawnCommanderAndGrabCheckPoint(BattleUser pUser, CommanderItem pCommanderItem, BattleCheckPoint pGrabCheckPoint)
	{
		int lInputIdx = _GetCommanderItemInputIdx(pCommanderItem, BattleFrameInputType.SpawnCommanderAndGrabCheckPoint);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.SpawnCommanderAndGrabCheckPoint, // mType
			pGrabCheckPoint.aCheckPointIdx, // [mX]
			0, // [mY]
			0, // [mZ]
			pUser.aUserIdx, // [mA]
			pCommanderItem.mSlotIdx, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_MoveTroops(BattleCommander pCommander, Coord2 pMoveCoord)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.MoveTroops);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.MoveTroops, // mType
			pMoveCoord.x, // [mX]
			pMoveCoord.z, // [mY]
			-1, // [mZ] 0보다 작으면 명시적 방향 지정 없음
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_MoveTroops(BattleCommander pCommander, Coord2 pMoveCoord, int pMoveDirectionY)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.MoveTroops);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.MoveTroops, // mType
			pMoveCoord.x, // [mX]
			pMoveCoord.z, // [mY]
			MathUtil.AbsDegree(pMoveDirectionY),  // [mZ] 0이상의 값으로 보정해서 명시적으로 방향을 지정
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_AttackTroop(BattleCommander pCommander, BattleTroop pTargetTroop)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.AttackTroop);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.AttackTroop, // mType
			pTargetTroop.aTroopGridIdx, // [mX]
			(int)pTargetTroop.aGuid, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_GrabCheckPoint(BattleCommander pCommander, BattleCheckPoint pGrabCheckPoint)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.GrabCheckPoint);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.GrabCheckPoint, // mType
			pGrabCheckPoint.aCheckPointIdx, // [mX]
			0, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_RetreatCommander(BattleCommander pCommander)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.RetreatCommander);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.RetreatCommander, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_CommanderSkillStart(BattleCommander pCommander, Coord2 pSkillTargetCoord, int pSkillDirectionY)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.CommanderSkill);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.CommanderSkill, // mType
			pSkillTargetCoord.x, // [mX]
			pSkillTargetCoord.z, // [mY]
			pSkillDirectionY, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			1); // [mC] 1=Start
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_CommanderSkillStop(BattleCommander pCommander)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.CommanderSkill);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.CommanderSkill, // mType
			pCommander.aCoord.x, // [mX]
			pCommander.aCoord.z, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC] 0=Stop
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_GoodGame(BattleUser pUser)
	{
		int lInputIdx = _GetUserInputIdx(pUser, BattleFrameInputType.GoodGame);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.GoodGame, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pUser.aUserIdx, // [mA]
			0, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_ForceAi(BattleCommander pCommander, bool pIsForceAi)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.ForceAi);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.ForceAi, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			pIsForceAi ? 1 : 0); // [mC] 1=true
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력을 넣습니다.
	public void SetFrameInput_DebugDestroyCommander(BattleCommander pCommander)
	{
		int lInputIdx = _GetCommanderInputIdx(pCommander, BattleFrameInputType.DebugDestroyCommander);

		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.DebugDestroyCommander, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pCommander.aId, // [mA]
			(int)pCommander.aGuid, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 치트 승리를 넣습니다.
	public void SetFrameInput_CheatWin(BattleUser pUser)
	{
		int lInputIdx = _GetUserInputIdx(pUser, BattleFrameInputType.CheatWin);
		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.CheatWin, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pUser.aTeamIdx, // [mA]
			(int)pUser.aUserInfo.mUserIdx, // [mB]
			0); // [mC]
	}
	//-------------------------------------------------------------------------------------------------------
	// 치트 패배를 넣습니다.
	public void SetFrameInput_CheatLose(BattleUser pUser)
	{
		int lInputIdx = _GetUserInputIdx(pUser, BattleFrameInputType.CheatLose);
		_SetFrameInput(
			lInputIdx,
			(int)BattleFrameInputType.CheatLose, // mType
			0, // [mX]
			0, // [mY]
			0, // [mZ]
			pUser.aTeamIdx, // [mA]
			(int)pUser.aUserInfo.mUserIdx, // [mB]
			0); // [mC]
	}


	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 프레임 입력을 알립니다.
	private void _SetFrameInput(int pOverwriteInputIdx, int pInputType, int pInputX, int pInputY, int pInputZ, int pInputA, int pInputB, int pInputC)
	{
		mNextFramePing.AddFrameInput(pOverwriteInputIdx, aClientGuid, pInputType, pInputX, pInputY, pInputZ, pInputA, pInputB, pInputC);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타입의 입력에 대해 기존에 있던 입력이 있다면 그 입력의 번호를 반환하고 없다면 -1을 반환합니다.
	private int _GetCommanderItemInputIdx(CommanderItem pCommanderItem, BattleFrameInputType pFrameInputType)
	{
		int lInputIdx = -1;
		for (int iInput = 0; iInput < mNextFramePing.aFrameInputList.Count; ++iInput)
			if (mNextFramePing.aFrameInputList[iInput].mType == (int)pFrameInputType)
			{
				int lCommanderItemSlotIdx = mNextFramePing.aFrameInputList[iInput].mB;
				if (lCommanderItemSlotIdx == pCommanderItem.mSlotIdx)
				{
					lInputIdx = iInput; // 중복 입력
					break;
				}
			}
		return lInputIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타입의 입력에 대해 기존에 있던 입력이 있다면 그 입력의 번호를 반환하고 없다면 -1을 반환합니다.
	private int _GetCommanderInputIdx(BattleCommander pCommander, BattleFrameInputType pFrameInputType)
	{
		int lInputIdx = -1;
		for (int iInput = 0; iInput < mNextFramePing.aFrameInputList.Count; ++iInput)
			if (mNextFramePing.aFrameInputList[iInput].mType == (int)pFrameInputType)
			{
				uint lCommanderGuid = (uint)mNextFramePing.aFrameInputList[iInput].mB;
				if (lCommanderGuid == pCommander.aGuid)
				{
					lInputIdx = iInput; // 중복 입력
					break;
				}
			}
		return lInputIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 타입의 입력에 대해 기존에 있던 입력이 있다면 그 입력의 번호를 반환하고 없다면 -1을 반환합니다.
	private int _GetUserInputIdx(BattleUser pUser, BattleFrameInputType pFrameInputType)
	{
		int lInputIdx = -1;
		for (int iInput = 0; iInput < mNextFramePing.aFrameInputList.Count; ++iInput)
			if (mNextFramePing.aFrameInputList[iInput].mType == (int)pFrameInputType)
			{
				lInputIdx = iInput; // 중복 입력
				break;
			}
		return lInputIdx;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private IConnectionProtocol mIConnectionProtocol;
	private IBattleProtocol mIBattleProtocol;

	private int mNextFrameNo;
	private BattleFramePing mNextFramePing;
}
