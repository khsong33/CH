using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class BattleProtocolLocal : MonoBehaviour, IConnectionProtocol, IBattleProtocol
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnConnectNetworkDelegate aOnConnectNetworkDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnCloseNetworkDelegate aOnCloseNetworkDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnErrorNetworkDelegate aOnErrorNetworkDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleRoomEnteredDelegate aOnBattleRoomEnteredDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleStartedDelegate aOnBattleStartedDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnFrameTickDelegate aOnFrameTickDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleEndedDelegate aOnBattleEndedDelegate { get; set; }
	
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
//		mBlitzInputs = new BattleBlitzInput[BattleProtocol.cMaxClientCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		aOnCloseNetworkDelegate += _OnCloseNetwork;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		aOnCloseNetworkDelegate -= _OnCloseNetwork;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void InitNetwork()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void ConnectNetwork()
	{
		// IConnectionProtocol 콜백을 호출합니다.
		if (aOnConnectNetworkDelegate != null)
			aOnConnectNetworkDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void CloseNetwork()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_EnterBattleRoom()
	{
		// 대전 정보를 초기화합니다.
		BattleMatchInfo lMatchInfo;
		switch (GameData.get.aCurrentGamePhase)
		{
		case GamePhase.Battle:
			{
				lMatchInfo = new BattleMatchInfo();
				lMatchInfo.InitFromStage(GameData.get.aBattleTestInfo.aStageNo);
			}
			break;
		case GamePhase.Replay:
			{
				lMatchInfo = new BattleMatchInfo();
				lMatchInfo.InitFromClone(GameData.get.aBattleVerifyData.aMatchInfo);
			}
			break;
		default:
			{
				Debug.LogError("invalid GameData.get.aCurrentGamePhase:" + GameData.get.aCurrentGamePhase);
				lMatchInfo = null;
			}
			break;
		}

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnBattleRoomEnteredDelegate != null)
			aOnBattleRoomEnteredDelegate(lMatchInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_ReadyToPlay()
	{
//		mFrameTickQueue = new Queue<BattleFrameTick>();

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnBattleStartedDelegate != null)
			aOnBattleStartedDelegate();

		// 플레이를 시작합니다.		
		mNextTickNo = 1;
		mVerifyDataFrameInputIdx = 0;
		mNextFrameTick = BattleFrameTick.Create(mNextTickNo++);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public BattleFrameTick PopFrameTick(int pTickNo)
	{
		BattleFrameTick lFrameTick = mNextFrameTick; // 기존 틱을 반환할 준비
		mNextFrameTick = BattleFrameTick.Create(mNextTickNo++); // 새 틱을 대기

		// 리플레이일 경우는 검증 데이터의 핑 정보를 가지고 틱을 초기화합니다.
		if (GameData.get.aCurrentGamePhase == GamePhase.Replay)
		{
			while (mVerifyDataFrameInputIdx < GameData.get.aBattleVerifyData.aFrameInputList.Count)
			{
				BattleFrameInput lFrameInputCursor = GameData.get.aBattleVerifyData.aFrameInputList[mVerifyDataFrameInputIdx];
				if (lFrameInputCursor.mTickNo != pTickNo)
					break;

				lFrameTick.AddFrameInput(lFrameInputCursor);
				++mVerifyDataFrameInputIdx;
			}
		}

		// 내부 동시 입력을 입력 순서대로 정렬합니다.
		if (lFrameTick.aFrameInputList.Count > 1)
//			lFrameTick.aFrameInputList.Sort();
			SortUtil.SortList<BattleFrameInput>(lFrameTick.aFrameInputList);

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnFrameTickDelegate != null)
			aOnFrameTickDelegate(lFrameTick);

		return lFrameTick; // 프레임 틱의 삭제는 인출해간 쪽에서 합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_FramePing(BattleFramePing pFramePing)
	{
		if (GameData.get.aCurrentGamePhase == GamePhase.Replay)
			mNextFrameTick.aFrameInputList.Clear(); // 재생일 때에는 입력 삭제

		// 핑 입력을 틱 정보에 옮겨 담습니다.
		BattleFrameInputData.Copy(pFramePing, mNextFrameTick, pFramePing.aTickNo + 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_BattleEnd(BattleProtocol.BattleEndCode pBattleEndCode)
	{
// 		if (GameData.get.aCurrentGamePhase == GamePhase.Replay)
// 			return; // 재생일 때에는 처리 불가

		// 보상 정보를 로컬에서 만듭니다.
		BattleEndInfo lRewardInfo = new BattleEndInfo(pBattleEndCode);

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnBattleEndedDelegate != null)
			aOnBattleEndedDelegate(lRewardInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 콜백
	private void _OnCloseNetwork()
	{
		mNextFrameTick.Destroy();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mNextTickNo;
	private BattleFrameTick mNextFrameTick;
//	private Queue<BattleFrameTick> mFrameTickQueue;
//	private BattleBlitzInput[] mBlitzInputs;

	private int mVerifyDataFrameInputIdx;
}

