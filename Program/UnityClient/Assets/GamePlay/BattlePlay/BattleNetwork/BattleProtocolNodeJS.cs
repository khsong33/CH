using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;
using SocketIO;
using CombatJson;

public class BattleProtocolNodeJS : ConnectionProtocolNodeJS, IBattleProtocol
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleRoomEnteredDelegate aOnBattleRoomEnteredDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleStartedDelegate aOnBattleStartedDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnFrameTickDelegate aOnFrameTickDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnBattleEndedDelegate aOnBattleEndedDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 속성
	public BattleProtocol.OnLeaveUserDelegate aOnLeaveUserDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//------------------------------------------------------------------------------------------------------
	// ConnectionProtocolNodeJS 메서드
	protected override void InitURL()
	{
		vSocket.url = GameData.get.aBattleServerURL; // vURL[(int)vNetworkState];
	}
	//------------------------------------------------------------------------------------------------------
	// ConnectionProtocolNodeJS 메서드
	protected override void RegistSocketEvent()
	{
		base.RegistSocketEvent();

		//vSocket.On("sc_req_clientid", _OnRequestClientId);
		vSocket.On("sc_battle_connection", _OnBattleRoomEntered);
		vSocket.On("sc_start_play", _OnBattleStarted);
		vSocket.On("sc_recv_tick", _OnFrameTick);
		vSocket.On("sc_battle_end2", _OnBattleEnded);
		vSocket.On("sc_leave_user", _OnLeaveUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_EnterBattleRoom()
	{
		// 유저의 로비 서버 소켓 Id를 전송
		JSONObject lConnectionJson = new JSONObject();
		lConnectionJson.AddField("UserKey", GameData.get.aPlayerInfo.mUserKey);
		vSocket.Emit("cs_connection", lConnectionJson);

		// 리팩토링 서버
		JSONObject lBattleConnectionJson = new JSONObject();
		String lRoomKey = String.Empty;
		JSONObject lRoomNameJSON = GameData.get.aBattleMatchInfoJson.GetField("Room");
		if(lRoomNameJSON == null)
			lRoomKey = MathUtil.Md5Sum(GameData.get.aBattleMatchInfoJson.ToString());
		else
			lRoomKey = (String)lRoomNameJSON.str;
		lBattleConnectionJson.AddField("RoomKey", lRoomKey);
		lBattleConnectionJson.AddField("UserKey", GameData.get.aPlayerInfo.mUserKey);
		vSocket.Emit("cs_battle_connection", lBattleConnectionJson);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_ReadyToPlay()
	{
		//Debug.Log("Send Ready To Play");

		mFrameTickQueue = new Queue<BattleFrameTick>();

		vSocket.Emit("cs_ready_to_play");
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public BattleFrameTick PopFrameTick(int pTickNo)
	{
		if (mFrameTickQueue.Count <= 0)
			return null;
		BattleFrameTick lFrameTick = mFrameTickQueue.Dequeue();
		if (lFrameTick.aTickNo != pTickNo)
			Debug.LogError("frame id mismatch lFrameTick.aTickNo=" + lFrameTick.aTickNo + " pTickNo=" + pTickNo + " - " + this);

		// 내부 동시 입력을 입력 순서대로 정렬합니다.
		if (lFrameTick.aFrameInputList.Count > 1)
//			lFrameTick.aFrameInputList.Sort();
			SortUtil.SortList<BattleFrameInput>(lFrameTick.aFrameInputList);

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnFrameTickDelegate != null)
			aOnFrameTickDelegate(lFrameTick);

		return lFrameTick; // 프레임 틱의 삭제는 인출해간 쪽에서 합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_FramePing(BattleFramePing pBattleFramePing)
	{
		//if (pBattleFramePing.aFrameInputList.Count > 0)
		//	Debug.Log("Ping input count - " + pBattleFramePing.aFrameInputList.Count);
		// 여기서 핑 정보를 패킷에 담아 보냅니다. [10/27/2014 jhpark]
		JSONObject llInputFieldJSONList = new JSONObject();
		StringBuilder llInputFieldStringBuilder = new StringBuilder();
		for (int iInputCount = 0; iInputCount < pBattleFramePing.aFrameInputList.Count; ++iInputCount)
		{
			JSONObject llInputFieldJson = new JSONObject();
			llInputFieldStringBuilder.Length = 0;
			pBattleFramePing.aFrameInputList[iInputCount].AppendInputFieldJsonString(llInputFieldStringBuilder);
			llInputFieldJson.AddField("Data", llInputFieldStringBuilder.ToString());

			if (!llInputFieldJSONList.HasField("PingData"))
			{
				llInputFieldJSONList.AddField("PingData", new JSONObject());
			}
			JSONObject lPingDataJSON = llInputFieldJSONList.GetField("PingData");
			lPingDataJSON.Add(llInputFieldJson);
		}
		llInputFieldJSONList.AddField("Frame", pBattleFramePing.aTickNo.ToString());
		llInputFieldJSONList.AddField("InputCount", pBattleFramePing.aFrameInputList.Count.ToString()); // Debug.Log("JSON Data check - " + llInputFieldJSONList);

		vSocket.Emit("cs_frame_ping", llInputFieldJSONList);
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 메서드
	public void SendPacket_BattleEnd(BattleProtocol.BattleEndCode pBattleEndCode)
	{
		// 임시 전송 코드
		JSONObject lBattleEndDataJSON = new JSONObject();
		lBattleEndDataJSON.AddField("UniqueId", (int)GameData.get.aPlayerInfo.mUniqueId);
		lBattleEndDataJSON.AddField("BattleCode", (int)pBattleEndCode);
		lBattleEndDataJSON.AddField("UserKey", GameData.get.aPlayerInfo.mUserKey);
		vSocket.Emit("cs_battle_end", lBattleEndDataJSON);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [OnReceivePacket] 배틀 서버 접속을 확인합니다.
	private void _OnBattleRoomEntered(SocketIOEvent pEvent)
	{
		//Debug.Log("On New Battle Server Connect");

		// 대전 정보를 초기화합니다.
		BattleMatchInfo lMatchInfo = new BattleMatchInfo();
		lMatchInfo.InitFromJson(GameData.get.aBattleMatchInfoJson);

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnBattleRoomEnteredDelegate != null)
			aOnBattleRoomEnteredDelegate(lMatchInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// [OnReceivePacket] 서버는 대전에 참가할 클라들이 모두 준비가 되었다면 대전 시작을 알립니다.
	private void _OnBattleStarted(SocketIOEvent pEvent)
	{
		//Debug.Log("Recv Ready To Play");

		// IBattleProtocol 콜백을 호출합니다.
		if (aOnBattleStartedDelegate != null)
			aOnBattleStartedDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// [OnReceivePacket] 서버는 매 프레임 틱에서 그 프레임에 대한 대전 참가 클라들의 핑이 모두 도착했는지 검사하고, 모두 도착했다면 틱을 보냅니다. 틱에는 핑에 들어있던 유저 입력이 추가로 들어있을 수 있습니다.
	private void _OnFrameTick(SocketIOEvent pEvent)
	{
		int lTickNo = (int)pEvent.data.GetField("Frame").n;
		BattleFrameTick lFrameTick = BattleFrameTick.Create(lTickNo);
		//Debug.Log("Frame Id - " + lFrameTick.mTickNo);
		JSONObject lDatasJSON = pEvent.data.GetField("Datas");
		if (lDatasJSON != null)
		{
			int lDataCount = lDatasJSON.list.Count;
			for (int iInput = 0; iInput < lDataCount; iInput++)
			{
				String lHeader = lDatasJSON.list[iInput].GetField("Header").str;
				String lPingData = lDatasJSON.list[iInput].GetField("Data").str;
				if (lHeader.ToUpper().Equals("INPUT"))
				{
					BattleFrameInput lFrameInput = BattleFrameInput.Create(lTickNo);
					lFrameInput.FromInputFieldJsonString(lPingData);
					lFrameTick.AddFrameInput(lFrameInput);
				}
			}
		}

		// 받은 프레임 틱을 인출 큐에 넣습니다.
		mFrameTickQueue.Enqueue(lFrameTick);
	}
	//-------------------------------------------------------------------------------------------------------
	// [OnReceivePacket] BattleServer2용 전투 종료 패킷
	private void _OnBattleEnded(SocketIOEvent pEvent)
	{
		String lUserKey = pEvent.data.GetField("UserKey").str;
		int lBattleCode = (int)pEvent.data.GetField("BattleCode").n;
		if (GameData.get.aPlayerInfo.mUserKey != lUserKey)
		{
			if (lBattleCode == (int)BattleProtocol.BattleEndCode.PlayWin)
				lBattleCode = (int)BattleProtocol.BattleEndCode.PlayLose;
			else if (lBattleCode == (int)BattleProtocol.BattleEndCode.PlayLose)
				lBattleCode = (int)BattleProtocol.BattleEndCode.PlayWin;
		}

		JSONObject lLogDataJson = pEvent.data.GetField("LogKey");
		if (lLogDataJson != null)
		{
			ReplayData lReplayData = new ReplayData();
			lReplayData.mIndex = GameData.get.aPlayerInfo.mReplayDataList.Count;
			lReplayData.mLogKey = pEvent.data.GetField("LogKey").str;
			lReplayData.mLogTime = DateTime.Parse(pEvent.data.GetField("LogTime").str);
			GameData.get.aPlayerInfo.mReplayDataList.Insert(0, lReplayData);
		}

		BattleEndInfo lBattleEndInfo = new BattleEndInfo((BattleProtocol.BattleEndCode)lBattleCode);
		if (aOnBattleEndedDelegate != null)
			aOnBattleEndedDelegate(lBattleEndInfo); // 클라이언트 정보 그대로 사용
	}
	//-------------------------------------------------------------------------------------------------------
	// [OnReceivePacket] BattleServer2용 유저 이탈
	private void _OnLeaveUser(SocketIOEvent pEvent)
	{
		String lLeaveUserKey = pEvent.data.GetField("UserKey").str;
		if (aOnLeaveUserDelegate != null)
			aOnLeaveUserDelegate(lLeaveUserKey);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Queue<BattleFrameTick> mFrameTickQueue;
}
