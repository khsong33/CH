//#define BATTLE_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class BattlePlay : MonoBehaviour, IBattleFrameTickContext
{
	public delegate void OnStartBattleDelegate();
	public delegate void OnEndBattleDelegate();

	public const float cStartCountdownSec = 3.0f;
	public const float cBattleEndDelaySec = 3.0f;
	public const float cGGDelaySec = 3.0f;

	public static BattlePlay main = null;
	private static BattlePlay main_destroyed = null;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Stage vStage;
	public BattleNetwork vBattleNetwork;
	public BattleLuaController vBattleLuaController;
	public BattleUIControlPanel vControlPanel;
	public BattleUILoadingPanel vLoadingPanel;

	public BattleUser.ControlType[] vUserControlTypes = { BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual, BattleUser.ControlType.Manual };
	public bool vIsLogAllUserAis = false;
	public bool[] vIsLogUserAis = { false, false, false, false, false, false, false, false };

	public Color[] vTeamColors = { ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(33, 140, 216), ColorUtil.Color3ub(216, 75, 33), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255) };
	public Color[] vUserColors = { ColorUtil.Color3ub(33, 140, 216), ColorUtil.Color3ub(216, 75, 33), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255) };
	public Color[] vEventUserColors = { ColorUtil.Color3ub(33, 140, 216), ColorUtil.Color3ub(216, 75, 33), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255), ColorUtil.Color3ub(255, 255, 255) };
	public Color vNpcColor = Color.gray;

	public String vFileLogPath = "Log/Log.txt";
	public GamePhase vTestGamePhase = GamePhase.Battle;
	public int vTestStageNo = 1;
	public String vTestReplayFilePath = "Replay/TestReplay.txt";
	public String vTestPlayerId = "default";
	public int vTestPlayerUserIdx = 0;
	public String vTestEnemyId = "default";
	public int vTestRandomSeed = 1;

	public int vDebugControlUserIdx = 1;
	public uint vDebugTroopGuid = 0;

	public bool vIsShowSearchCount = false;
	public bool vIsLogFrameUpdaterStatistics = false;
	public bool vIsLogBattleGridStatistics = false;
	public bool vIsShowCommanderGridIdx = false;
	public bool vIsSaveInstantReplayData = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 로직
	public BattleLogic aBattleLogic
	{
		get { return mBattleLogic; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 대전 정보
	public BattleMatchInfo aMatchInfo
	{
		get { return mMatchInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 진행 정보
	public BattleProgressInfo aProgressInfo
	{
		get { return mProgressInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 격자
	public BattleGrid aBattleGrid
	{
		get { return mBattleGrid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 클라이언트의 유저
	public BattleUser aClientUser
	{
		get { return mClientUser; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 테이블
	public BattleUser[] aUsers
	{
		get { return mUsers; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투가 시작될 때 호출됩니다.
	public OnStartBattleDelegate aOnStartBattleDelegate
	{
		get { return mOnStartBattleDelegate; }
		set { mOnStartBattleDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투가 끝날 때 호출됩니다.
	public OnEndBattleDelegate aOnEndBattleDelegate
	{
		get { return mOnEndBattleDelegate; }
		set { mOnEndBattleDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 즉석 리플레이 데이터
	public BattleVerifyData aInstantReplayData
	{
		get { return mInstantReplayData; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 중지 여부
	public bool aIsPauseBattle
	{
		get { return mIsPauseBattle; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		// 전역 참조를 지정합니다.
		if ((main == null) ||
			(main == main_destroyed))
			main = this;

		if (vStage == null)
			Debug.LogError("vStage is empty - " + this);
		if (vBattleNetwork == null)
			Debug.LogError("vBattleNetwork is empty - " + this);
		if (vBattleLuaController == null)
			Debug.LogError("vBattleLuaController is empty - " + this);
		if (vControlPanel == null)
			Debug.LogError("vControlPanel is empty - " + this);
		if (vLoadingPanel == null)
			Debug.LogError("vLoadingPanel is empty - " + this);

		if (vUserControlTypes.Length != BattleMatchInfo.cMaxUserCount)
			Debug.LogError("vUserControlTypes.Length mismatch - " + this);
		if (vTeamColors.Length != BattleMatchInfo.cMaxTeamCount)
			Debug.LogError("vTeamColors.Length mismatch - " + this);
		if (vUserColors.Length != BattleMatchInfo.cMaxUserCount)
			Debug.LogError("vUserColors.Length mismatch - " + this);
		if (vEventUserColors.Length != BattleMatchInfo.cMaxUserCount)
			Debug.LogError("vEventUserColors.Length mismatch - " + this);

		vBattleNetwork.vBattlePlay = this;

		// 나중에 로딩하지 않고 시작 시점에서 싱글톤 객체의 생성을 보장합니다.
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		mFileLogger = new FileLogger(vFileLogPath);
		Logger.SetInstance(mFileLogger);
	#endif

		Config.CreateSingleton();
		LocalizedTable.CreateSingleton();
		GameData.CreateSingleton();
		BattleConfig.CreateSingleton();

		PlayTime.CreateSingletonObject();
		AssetManager.CreateSingletonObject();
		Sound.CreateSingletonObject();

		GroundEventTable.CreateSingleton();
		GroundEffectTable.CreateSingleton();
		TroopEffectTable.CreateSingleton();
		GroundTable.CreateSingleton();
		SkillTable.CreateSingleton();
		TroopLevelTable.CreateSingleton(); // Troop Table 에서 레벨 정보를 사용하기에 Troop Table보다 우선로딩되어야 함.
		VeterancyTable.CreateSingleton(); // Troop Table 에서 베테랑 정보를 사용하기에 Troop Table보다 우선로딩되어야 함.
		WeaponTable.CreateSingleton();
		TroopTable.CreateSingleton();
		CommanderTroopTable.CreateSingleton();
		CommanderTable.CreateSingleton();
		StageTable.CreateSingleton();

		BattleCoordDistTable.CreateSingleton();

		CustomBattleCommanderItemTable.CreateSingleton();

		// 게임 단계 초기화를 합니다.
		if (GameData.get.aPrevGamePhase == GamePhase.None)
		{
			GameData.get.aBattleTestInfo.aIsTesting = true;
			if (GameData.get.aBattleTestInfo.aStageNo == 0) // 테스트 스테이지가 지정이 안 되어 있다면
				GameData.get.aBattleTestInfo.aStageNo = vTestStageNo; // 인스펙터 지정값을 사용
		}
		if (GameData.get.aCurrentGamePhase == GamePhase.None)
		{
			switch (vTestGamePhase)
			{
			case GamePhase.Battle:
				{
					GameData.get.EnterBattleTestPhase(GameData.get.aBattleTestInfo.aStageNo, false);
				}
				break;
			case GamePhase.Replay:
				{
					BattleVerifyData lVerifyData = new BattleVerifyData(null);
					lVerifyData.LoadFile(vTestReplayFilePath);

// [테스트] 리플레이 보상을 계산하고 출력합니다.
// 					int lUserIdx = 0;
// 					BattleRewardInfo lExpectedRewardInfo = lVerifyData.DebugComputeRewardInfo(lUserIdx);
// 					lExpectedRewardInfo.DebugDump("Log/Reward.txt");

					GameData.get.EnterReplayPhase(lVerifyData, false);
				}
				break;
			default:
				{
					Debug.LogError("invalid vTestGamePhase:" + vTestGamePhase);
				}
				break;
			}
		}
		switch (GameData.get.aCurrentGamePhase)
		{
		case GamePhase.Battle:
			{
			#if UNITY_EDITOR
				mInstantReplayData = new BattleVerifyData(null);
			#endif
			}
			break;
		case GamePhase.Replay:
			{
			#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
				GameData.get.aBattleVerifyData.SaveFile(vTestReplayFilePath);
			#endif
			}
			break;
		}

		// 로직을 생성합니다.
		mBattleLogic = new BattleLogic();
		mProgressInfo = new BattleProgressInfo();
		mUsers = new BattleUser[BattleMatchInfo.cMaxUserCount];
		mUpdateScheduler = new BattleUpdateScheduler();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		if (main == this)
		{
			mSleepTimeoutSave = Screen.sleepTimeout;
			Screen.sleepTimeout = SleepTimeout.NeverSleep;
		}

		vLoadingPanel.Show(); // 로딩 화면을 보입니다.

		// 전투 서버에 접속합니다.
		_ConnectNetwork();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
#if UNITY_EDITOR
	void Update()
	{
		// 개발용 세팅을 반영합니다.
		BattleConfig.get.aDebugSetting.mIsShowSearchCount = vIsShowSearchCount;

		// 즉석 리플레이 데이터를 파일로 저장합니다.
		if (vIsSaveInstantReplayData)
		{
			vIsSaveInstantReplayData = false;
			if (mInstantReplayData != null)
				mInstantReplayData.SaveFile(vTestReplayFilePath);
			else
				Debug.LogWarning("no mInstantReplayData");
		}
	}
#endif
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestroy()
	{
		// 전역 참조를 해제합니다.
		if (main == this)
		{
			Screen.sleepTimeout = mSleepTimeoutSave;

			main_destroyed = this;
		}

	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		mFileLogger.Dispose();
		Logger.SetInstance(null);
	#endif
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 전투를 잠시 멈춥니다.
	public void PauseBattle(bool pIsPauseBattle)
	{
		mIsPauseBattle = pIsPauseBattle;
		mBattleGrid.aFrameUpdater.StopUpdate(pIsPauseBattle);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투를 끝냅니다.
	public void EndBattle(bool pIsProgressEnded)
	{
		StartCoroutine(_EndBattle(pIsProgressEnded));
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투를 파괴합니다.
	public void DestroyBattle()
	{
		if (mBattleGrid != null)
		{
			for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; ++iUser)
				if (mUsers[iUser] != null)
				{
					mUsers[iUser].Destroy();
					mUsers[iUser] = null;
				}

			mBattleGrid.Destroy();
			mBattleGrid = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameTickContext 메서드
	public void OnFrameInputSpawnCommander(BattleCommander pSpawnCommander, int pLastTroopCount)
	{
		vControlPanel.SetCommanderControl(pSpawnCommander);

		if (pSpawnCommander.aUser == vControlPanel.vClientUserControl.aControlUser) // 스폰 지휘관이 컨트롤 유저라면
		{
			vControlPanel.FocusCommander(pSpawnCommander, true, vControlPanel.vDefaultFocusingCurve, vControlPanel.vDefaultFocusingDelay); // 포커스를 줍니다.
			if ((pLastTroopCount <= 0) && // 아무 부대도 없다가 스폰되는 경우이고
				BattlePlay.main.vControlPanel.aIsApproachFocusing) // 포커싱이 유지되는 상태라면
				vControlPanel.FocusCommander(pSpawnCommander, true, vControlPanel.vDefaultFocusingCurve, vControlPanel.vDefaultFocusingDelay); // 다시 포커스를 주어서 카메라를 접근시킵니다.
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameTickContext 메서드
	public void OnFrameInputGoodGame(BattleUser pInputUser)
	{
		vControlPanel.PromptMessage(pInputUser.aUserInfo.mUserName + " \"GG\"", cGGDelaySec);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander를 통해 화면에 표시될 색을 얻어옵니다.
	public Color GetUserColor(BattleCommander pCommander)
	{
		Color lUserColor;
		if (pCommander.aTeamUser != null)
		{
			if (pCommander.aBelongingType != BattleCommander.BelongingType.EventPoint)
				lUserColor = BattlePlay.main.vUserColors[pCommander.aTeamUser.aUserIdx];
			else
				lUserColor = BattlePlay.main.vEventUserColors[pCommander.aTeamUser.aUserIdx];
		}
		else
		{
			lUserColor = BattlePlay.main.vNpcColor;
		}
		return lUserColor;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 네트워크에 접속합니다.
	private void _ConnectNetwork()
	{
		vBattleNetwork.aIConnectionProtocol.InitNetwork();

		vBattleNetwork.aIConnectionProtocol.aOnConnectNetworkDelegate += _OnConnectNetwork;
		vBattleNetwork.aIConnectionProtocol.aOnCloseNetworkDelegate += _OnCloseNetwork;

		vBattleNetwork.aIBattleProtocol.aOnBattleRoomEnteredDelegate += _OnBattleRoomEntered;
		vBattleNetwork.aIBattleProtocol.aOnBattleStartedDelegate += _OnBattleStarted;		
		vBattleNetwork.aIBattleProtocol.aOnBattleEndedDelegate += _OnBattleEnded;

		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnBattleReward += _OnBattleReward;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnBattleReward2 += _OnBattleReward2;
		}


		vBattleNetwork.aIConnectionProtocol.ConnectNetwork();
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 콜백
	private void _OnConnectNetwork()
	{
		// 서버에 전투 방 입장을 요청합니다.
		vBattleNetwork.aIBattleProtocol.SendPacket_EnterBattleRoom();
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 콜백
	private void _OnCloseNetwork()
	{
		vBattleNetwork.aIConnectionProtocol.aOnConnectNetworkDelegate -= _OnConnectNetwork;
		vBattleNetwork.aIConnectionProtocol.aOnCloseNetworkDelegate -= _OnCloseNetwork;

		vBattleNetwork.aIBattleProtocol.aOnBattleRoomEnteredDelegate -= _OnBattleRoomEntered;
		vBattleNetwork.aIBattleProtocol.aOnBattleStartedDelegate -= _OnBattleStarted;
		vBattleNetwork.aIBattleProtocol.aOnBattleEndedDelegate -= _OnBattleEnded;

		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnBattleReward -= _OnBattleReward;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnBattleReward2 -= _OnBattleReward2;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 콜백
	private void _OnBattleRoomEntered(BattleMatchInfo pMatchInfo)
	{
		mMatchInfo = pMatchInfo;

		StartCoroutine(_LoadStage());
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 스테이지를 로딩합니다.
	private IEnumerator _LoadStage()
	{
		//Debug.Log("_LoadStage()");

		yield return null;

		// 클라이언트 정보 초기화를 합니다.
		BattleUserInfo lClientUserInfo;
		if (GameData.get.aPlayerInfo != null)
		{
			lClientUserInfo = mMatchInfo.FindClientUserInfo(GameData.get.aPlayerInfo.mUniqueId);
			if (lClientUserInfo == null)
				Debug.LogError("can't find user of client guid: " + GameData.get.aPlayerInfo.mUniqueId);
		}
		else
		{
			lClientUserInfo = mMatchInfo.mUserInfos[vTestPlayerUserIdx]; // vTestPlayerUserIdx 유저를 이 클라이언트의 유저로 간주합니다.

			// 테스트용 초기화를 합니다.
			GameData.get.aPlayerInfo = new GameUserInfo();
			GameData.get.aPlayerInfo.Reset();
			GameData.get.aPlayerInfo.mUniqueId = lClientUserInfo.mClientGuid;
			for (int iCommander = 0; iCommander < lClientUserInfo.mCommanderItems.Length; ++iCommander)
				if (lClientUserInfo.mCommanderItems[iCommander].aIsValid)
				{
					CommanderItemData lCommanderItemData = new CommanderItemData();
					lCommanderItemData.InitFromItem(lClientUserInfo.mCommanderItems[iCommander]);
					GameData.get.aPlayerInfo.AddCommanderItem(lCommanderItemData);
				}
		}

		// 맵을 로딩하기 전에 스테이지 정보를 표시합니다.
		vLoadingPanel.SetStageInfo(mMatchInfo.aStageSpec);
		vLoadingPanel.SetBattleUserInfo(mMatchInfo);

		yield return null;

		// 맵을 로딩합니다.
		AssetKey lMapAssetKey
			= (GameData.get.aBattleTestInfo.aIsTesting && (GameData.get.aBattleTestInfo.aMapAssetKey != null))
			? GameData.get.aBattleTestInfo.aMapAssetKey : mMatchInfo.aStageSpec.mMapAssetKey;
		vStage.LoadMap(lMapAssetKey);

		// 진행 정보를 초기화합니다.
		mProgressInfo.Init(mMatchInfo);
		mIsPauseBattle = false;

		// 전투 격자를 생성합니다.
		AssetKey lGridLayoutAssetKey
			= (GameData.get.aBattleTestInfo.aIsTesting && (GameData.get.aBattleTestInfo.aGridLayoutAssetKey != null))
			? GameData.get.aBattleTestInfo.aGridLayoutAssetKey : mMatchInfo.aStageSpec.mGridLayoutAssetKey;
		TextAsset lGridLayoutTextAsset = AssetManager.get.Load<TextAsset>(lGridLayoutAssetKey);
		mBattleGrid = mBattleLogic.CreateGrid(
			lGridLayoutAssetKey.ToString(),
			lGridLayoutTextAsset.text,
			mMatchInfo,
			mProgressInfo,
			vStage);

		vStage.SetViewCenter(mBattleGrid.aCenter);
		if (vStage.vObserverTeamIdx < 0) // 관찰 팀이 지정되어 있지 않다면
			vStage.vObserverTeamIdx = lClientUserInfo.mTeamIdx; // 클라이언트 팀을 관찰 팀으로 초기화

		// 유저를 생성합니다.
		for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
		{
			BattleUserInfo lUserInfo = mMatchInfo.mUserInfos[iUser];

			BattleUser.ControlType lControlType;
			if ((GameData.get.aPrevGamePhase == GamePhase.Lobby) || // 로비로부터 정상적으로 시작한 전투이거나
				(GameData.get.aCurrentGamePhase == GamePhase.Replay)) // 리플레이라면
			{
				lControlType = lUserInfo.mIsAi ? BattleUser.ControlType.Auto : BattleUser.ControlType.Manual; // AI 여부에 따라 컨트롤을 지정합니다.
			}
			else // 로컬에서의 테스트 전투라면
			{
				lControlType = vUserControlTypes[iUser]; // 인스펙터에 설정된 값을 따릅니다.
				lUserInfo.mIsAi = (lControlType == BattleUser.ControlType.Auto);
			}
			vUserControlTypes[iUser] = lControlType;

			mUsers[iUser] = mBattleLogic.CreateUser(
				mBattleGrid,
				lUserInfo,
				lControlType);
		}
		mProgressInfo.UpdateLiveCommanderCounts(mBattleGrid);

		// 이 클라이언트의 유저를 지정합니다.
		mClientUser = mUsers[lClientUserInfo.mUserIdx];
		vBattleNetwork.aClientGuid = mClientUser.aUserInfo.mClientGuid;

		// 갱신 스케쥴을 잡습니다.
		mUpdateScheduler.Init(mMatchInfo, mBattleGrid.aFrameUpdater);

		// 전투 시작 전 처리를 합니다.
		mBattleGrid.PreBattleSetUp();
	
		// 컨트롤 패널을 초기화합니다.
		vControlPanel.SetUpControl(mClientUser);
		
		// 디버깅 정보를 표시합니다.
	#if UNITY_EDITOR
		if ((vDebugControlUserIdx >= 0) &&
			(mClientUser.aUserIdx != vDebugControlUserIdx))
			vControlPanel.SetDebugControlUser(vDebugControlUserIdx);
	#endif

		// 스크립트를 초기화합니다.
		vBattleLuaController.Init(mMatchInfo.aStageSpec);

		// 즉석 리플레이 데이터를 초기화합니다.
		if (mInstantReplayData != null)
			mInstantReplayData.aMatchInfo.InitFromClone(mMatchInfo);

		// 컨트롤을 시작합니다.
		yield return StartCoroutine(vControlPanel.StartControl());

		// 시작전 처리
		vBattleLuaController.PrePlayLua();

		yield return null;
		// 서버에 플레이 준비 완료를 알립니다.
		vBattleNetwork.aIBattleProtocol.SendPacket_ReadyToPlay();

		// 로딩 화면을 숨깁니다.
		yield return StartCoroutine(vLoadingPanel.Hide()); // 로딩 화면을 숨깁니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 콜백
	private void _OnBattleStarted()
	{
		//Debug.Log("_OnBattleStarted()");

		// 플레이를 시작합니다.
		StartCoroutine(_StartBattle());
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 플레이를 시작합니다.
	private IEnumerator _StartBattle()
	{
		//Debug.Log("_StartBattle()");

		yield return null;

		// 카운트 다운을 시작합니다.
		if (!TutorialTable.get.IsTutorial(mMatchInfo.aStageSpec.mNo))
			yield return StartCoroutine(vControlPanel.ShowCountdown(cStartCountdownSec));

		// 이후로 프레임 갱신을 합니다.
		mBattleGrid.aFrameUpdater.aOnFrameSliceChangeDelegate += _OnFrameSliceChange;

		// 콜백을 호출합니다.
		if (mOnStartBattleDelegate != null)
			mOnStartBattleDelegate();

		// 플레이 갱신을 시작합니다.
		StartCoroutine(_Update());

		// 스폰을 시작합니다.
		vControlPanel.StartSpawn();

		// 전투 시작 스크립트를 실행합니다.
		vBattleLuaController.PlayLua();
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투를 끝냅니다.
	private IEnumerator _EndBattle(bool pIsProgressEnded)
	{
		// 콜백을 호출합니다.
		if (mOnEndBattleDelegate != null)
			mOnEndBattleDelegate();

		// 프레임 갱신을 중단합니다.
		mBattleGrid.aFrameUpdater.StopUpdate(true);
		while (!mBattleGrid.aFrameUpdater.aIsStopped)
			yield return null; // 프레임 갱신이 완전히 끝날 때까지 기다림(StopUpdate() 이후에 딜레이 있을 수 있음)

		mBattleGrid.aFrameUpdater.aOnFrameSliceChangeDelegate -= _OnFrameSliceChange;
		PauseBattle(true);

		if (pIsProgressEnded)
		{
			// 서버에 전투 종료를 알립니다.
			BattleProtocol.BattleEndCode lBattleEndCode = mProgressInfo.aTeamBattleEndCodes[mClientUser.aTeamIdx];
			GameData.get.UpdateBattleEnd(lBattleEndCode);
			vBattleNetwork.aIBattleProtocol.SendPacket_BattleEnd(lBattleEndCode); // 전투 종료 패킷을 보냅니다.

			// 메시지를 출력하고 연출을 위해서 잠시 기다립니다(이 연출 시간 동안 서버에서 검증 처리를 합니다).
			vControlPanel.PromptMessage("Battle End", cBattleEndDelaySec);
			yield return new WaitForSeconds(cBattleEndDelaySec);

			// 리플레이가 아닌 전투일 경우의 진행을 처리합니다.
			if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
			{
				// 전투 보상을 받을 때까지 기다립니다(검증 처리가 특별히 오래 걸리지 않았다면 추가적으로 더 기다리는 경우는 없습니다).
				//while (mBattleRewardInfo == null)
				//	yield return null;
				while (mBattleResultData == null)
					yield return null;

				// 보상 팝업 창을 엽니다.
				//GamePopupBattleReward lGamePopupBattleReward = (GamePopupBattleReward)GamePopupUIManager.aInstance.OpenPopup("Popup Battle Reward");
				//lGamePopupBattleReward.SetRewardInfo(mMatchInfo, mBattleEndInfo, mBattleRewardInfo);
				GamePopupBattleResult lPopup = (GamePopupBattleResult)GamePopupUIManager.aInstance.OpenPopup("Popup Battle Result");
				lPopup.SetBattleResultData(mBattleGrid, mBattleResultData);

				// 튜토리얼일 경우 로비서버로 Tutorial 완료 패킷을 전송합니다.
				if (TutorialTable.get.IsTutorial(mMatchInfo.aStageSpec.mNo) &&
					(GameData.get.aLobbyControl != null))
					GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_EndTutorial();
			}
		}

		// 컨트롤 패널을 해제합니다.
		vControlPanel.ResetControl();

		// 리플레이라면 로비로 돌아갑니다(차후에 완료 팝업 등을 통해서 방식을 바꿀 예정).
		yield return null;
		if (GameData.get.aCurrentGamePhase == GamePhase.Replay)
			GameData.get.EnterLobbyPhase();
	}
	//-------------------------------------------------------------------------------------------------------
	// IBattleProtocol 콜백
	private void _OnBattleEnded(BattleEndInfo pBattleEndInfo)
	{
		//mBattleEndInfo = pBattleEndInfo;

		if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
		{
			//mBattleRewardInfo = null;
			mBattleResultData = null;

			if (GameData.get.aLobbyControl != null)
				GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_BattleReward(mMatchInfo.aStageSpec.mNo);
			else
				_OnBattleReward2(null);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// LobbyNetwork.OnBattleReward2 콜백
	private void _OnBattleReward2(BattleResultData pBattleResultData)
	{
		mBattleResultData = pBattleResultData;
	}
	//-------------------------------------------------------------------------------------------------------
	// LobbyNetwork.OnBattleReward 콜백
	private void _OnBattleReward(BattleRewardInfo pBattleRewardInfo)
	{
		mBattleRewardInfo = pBattleRewardInfo;

		BattleRewardInfo lExpectedRewardInfo = null;
		if (mInstantReplayData != null)
		{
			lExpectedRewardInfo = mInstantReplayData.DebugComputeRewardInfo(mClientUser.aUserIdx);

		#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
			mInstantReplayData.SaveFile(vTestReplayFilePath);
			lExpectedRewardInfo.DebugDump("Log/Reward.txt");
		#endif

			if (mBattleRewardInfo != null)
			{
				if (!BattleRewardInfo.CheckIdentical(mBattleRewardInfo, lExpectedRewardInfo))
				{
					Debug.LogError("BattleRewardInfo.CheckIdentical() failed");
				#if UNITY_EDITOR && !BATTLE_REWARD_VERIFY_TEST // 아직 덤프를 하지 않았다면
					mBattleRewardInfo.DebugDump("Log/RewardInfo_result.txt");
					lExpectedRewardInfo.DebugDump("Log/RewardInfo_expected.txt");
				#endif
				}
			#if UNITY_EDITOR && BATTLE_REWARD_VERIFY_TEST
				else
				{
					Debug.Log("BattleRewardInfo is identical");
				}
			#endif
			}
			else
			{
				mBattleRewardInfo = lExpectedRewardInfo;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// FrameUpdater.OnFrameSliceChange
	private void _OnFrameSliceChange(int pFrameIdx, int pFrameSliceIdx, int pFrameDelta)
	{
		//Debug.Log("_OnFrameSliceChange() pFrameIdx=" + pFrameIdx + " pFrameSliceIdx=" + pFrameSliceIdx);

		// 전투 진행을 갱신합니다.
		if (pFrameSliceIdx == mUpdateScheduler.aBattleProgressFrameSliceIdx)
		{
			if (!mProgressInfo.UpdateProgress(mBattleGrid, pFrameDelta))
				EndBattle(true);

			// 컨트롤 패널에서 진행 정보 변화를 반영합니다.
			vControlPanel.UpdateFrame();

			// 루아를 갱신합니다.
			vBattleLuaController.UpdateFrame();
		}

		// 각 조각에서는 유저별 갱신을 합니다.
		for (int iProcess = 0; iProcess < mUpdateScheduler.aFrameSliceUpdateUserProcessCount[pFrameSliceIdx]; ++iProcess)
		{
			int iProcessUserIdx = mUpdateScheduler.aFrameSliceUpdateProcessUserIdxs[pFrameSliceIdx, iProcess];
			mUsers[iProcessUserIdx].UpdateFrame();
		}

		// AI 콜렉터 갱신을 합니다.
		if (pFrameSliceIdx == mUpdateScheduler.aAiCollectorFrameSliceIdx)
		{
			if (mBattleGrid.aAiCollector != null)
				mBattleGrid.aAiCollector.UpdateFrame();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 갱신 콜백
	private IEnumerator _Update()
	{
		yield return null; // 아래의 첫 번째 틱에서 딜레이가 있을 수 있으므로 카운트다운 시작이 일단 보여지도록 하기 위함

		for (;;)
		{
			if (mBattleGrid == null)
				yield break;

			if (mBattleGrid.aFrameUpdater.aIsStopped)
			{
				yield return null;
				continue;
			}

			// 개발용 상태 변화를 처리합니다.
		#if UNITY_EDITOR
			mBattleGrid.aDebugTroopGuid = vDebugTroopGuid;
		#endif

			// 개체 업데이터의 프레임 번호에 맞는 틱을 기다립니다.
			//Debug.Log("mUpdateScheduler.aBattleFrameTickNo=" + mUpdateScheduler.aBattleFrameTickNo + " mBattleGrid.aFrameUpdater.aFrameIdx=" + mBattleGrid.aFrameUpdater.aFrameIdx + " mUpdateScheduler.aFrameIdxUpdateLimit=" + mUpdateScheduler.aFrameIdxUpdateLimit);
			if (mBattleGrid.aFrameUpdater.aFrameIdx == mUpdateScheduler.aFrameIdxUpdateLimit)
			{
				BattleFrameTick lFrameTick = vBattleNetwork.aIBattleProtocol.PopFrameTick(mUpdateScheduler.aBattleFrameTickNo);
				if (lFrameTick != null)
				{
					// 즉석 리플레이 데이터가 있다면 저장합니다.
					if (mInstantReplayData != null)
						mInstantReplayData.RecordFrameTick(lFrameTick);

					// 프레임 틱을 처리합니다.
					BattleProtocol.OnFrameTick(lFrameTick, this);
					lFrameTick.Destroy();

					// 서버에 핑을 보냅니다.
					if (GameData.get.aCurrentGamePhase != GamePhase.Replay)
					{
						vBattleNetwork.aIBattleProtocol.SendPacket_FramePing(vBattleNetwork.aNextFramePing);
						vBattleNetwork.PrepareNextFramePing();
					}

					// 틱을 갱신합니다.
					mUpdateScheduler.UpdateTick();
				}
			}

			// 전투 로직을 갱신합니다.
			int lFrameDelta = (int)(Time.deltaTime * 1000.0f);
			mBattleGrid.aFrameUpdater.UpdateFrameDelta(lFrameDelta, mUpdateScheduler.aFrameIdxUpdateLimit);
			mBattleGrid.aFrameUpdater.aStatistics.UpdateHistroy();

			// 개발용 통계를 남깁니다.
			if (vIsLogFrameUpdaterStatistics)
			{
				mBattleGrid.aFrameUpdater.aStatistics.LogHistory();
				vIsLogFrameUpdaterStatistics = false; // 한 프레임 출력하고 플래그를 끔
			}

			if (vIsLogBattleGridStatistics)
			{
				mBattleGrid.aStatistics.LogHistory();
				mBattleGrid.aStatistics.ResetHistory();
				vIsLogBattleGridStatistics = false; // 한 프레임 출력하고 플래그를 끔
			}

			// UI 입력을 처리합니다.
			if (Input.GetKeyDown(KeyCode.Backspace) ||
				Input.GetKeyDown(KeyCode.Escape))
			{
				if (GamePopupUIManager.aInstance.aCurrentPopup != null)
					GamePopupUIManager.aInstance.BackPopup();
			}

			// AI 모드 변화를 처리합니다.
			for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
				if (mUsers[iUser].aControlType != vUserControlTypes[iUser])
					mUsers[iUser].aControlType = vUserControlTypes[iUser];

			// 개발용 상태 변화를 처리합니다.
		#if UNITY_EDITOR
			for (int iUser = 0; iUser < mMatchInfo.mUserCount; ++iUser)
				mUsers[iUser].aIsLogAi = vIsLogAllUserAis || vIsLogUserAis[iUser];
		#endif

			yield return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleLogic mBattleLogic;
	private BattleMatchInfo mMatchInfo;
	private BattleProgressInfo mProgressInfo;
	private BattleGrid mBattleGrid;
	private BattleUser[] mUsers;
	private BattleUser mClientUser;

	private BattleUpdateScheduler mUpdateScheduler;

	private int mSleepTimeoutSave;
	private bool mIsPauseBattle;

	private OnStartBattleDelegate mOnStartBattleDelegate;
	private OnEndBattleDelegate mOnEndBattleDelegate;

#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
	private FileLogger mFileLogger;
#endif
	private BattleVerifyData mInstantReplayData;
	
	//private BattleEndInfo mBattleEndInfo;
	private BattleRewardInfo mBattleRewardInfo;
	private BattleResultData mBattleResultData;
}
