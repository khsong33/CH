using UnityEngine;
using System.Collections;
using System;

public class BattlePlayScript_ActionTest : BattlePlayScript
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		BattlePlay.main.aOnStartBattleDelegate += OnStartBattle;
		BattlePlay.main.aOnEndBattleDelegate += OnEndBattle;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnStartBattleDelegate
	public void OnStartBattle()
	{
		StartCoroutine(UpdateScript());
	}
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnEndBattleDelegate
	public void OnEndBattle()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트를 갱신합니다.
	public IEnumerator UpdateScript()
	{
		Debug.LogWarning("UpdateScript() - " + this);

		BattleGrid lBattleGrid = BattlePlay.main.aBattleGrid;
		BattleUser lClientUser = BattlePlay.main.aUsers[0];

		lBattleGrid.aProgressInfo.AddMilliMp(lClientUser.aUserIdx, 1000000);

		BattleCommander lRiflemanCommander = lBattleGrid.SpawnCommander(
			lClientUser,
			lClientUser.aUserInfo.mCommanderItems[0],
			lClientUser.aHeadquarter.aCoord + new Coord2(-1000, 1000),
			lClientUser.aHeadquarter.aDirectionY,
			lClientUser);
		for (int iTroop = 0; iTroop < lRiflemanCommander.aTroops.Length; ++iTroop)
			if (lRiflemanCommander.aTroops[iTroop] != null)
			{
				lRiflemanCommander.aTroops[iTroop].UpdateHp(TroopHpChangeType.DirectDamage, -lRiflemanCommander.aTroops[iTroop].aStat.aMaxMilliHp * 9 / 10);

// 				StageTroop lStageTroop = lRiflemanCommander.aTroops[iTroop].aStageTroop as StageTroop;
// 				lStageTroop.vIsLogControl = true;
			}
		for (int iTroop = 1; iTroop < lRiflemanCommander.aTroops.Length; ++iTroop)
			if (lRiflemanCommander.aTroops[iTroop] != null)
				lRiflemanCommander.aTroops[iTroop].Destroy();

		BattleCommander lMedicCommander = lBattleGrid.SpawnCommander(
			lClientUser,
			lClientUser.aUserInfo.mCommanderItems[3],
			lClientUser.aHeadquarter.aCoord + new Coord2(-100, 200),
			lClientUser.aHeadquarter.aDirectionY,
			lClientUser);
		for (int iTroop = 0; iTroop < lRiflemanCommander.aTroops.Length; ++iTroop)
			if (lMedicCommander.aTroops[iTroop] != null)
			{
				lMedicCommander.aTroops[iTroop].UpdateHp(TroopHpChangeType.DirectDamage, -lMedicCommander.aTroops[iTroop].aStat.aMaxMilliHp / 2);
			}
// 		for (int iTroop = 1; iTroop < lMedicCommander.aTroops.Length; ++iTroop)
// 			if (lMedicCommander.aTroops[iTroop] != null)
// 				lMedicCommander.aTroops[iTroop].Destroy();

		yield return StartCoroutine(WaitForFrameSeconds(4.0f));

		lRiflemanCommander.MoveTroops(lClientUser.aHeadquarter.aCoord + new Coord2(1000, 1000));

		yield return StartCoroutine(WaitForFrameSeconds(1.0f));

		lMedicCommander.aSkill.StartSkill(lMedicCommander.aLeaderTroop.aCoord, lMedicCommander.aLeaderTroop.aDirectionY, 0);
		yield return StartCoroutine(WaitForFrameSeconds(4.5f));

// 		lRiflemanCommander.aSkill.StartSkill(lRiflemanCommander.aLeaderTroop.aCoord, lRiflemanCommander.aLeaderTroop.aDirectionY, 0);
// 		yield return StartCoroutine(WaitForFrameSeconds(4.5f));

		yield return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
