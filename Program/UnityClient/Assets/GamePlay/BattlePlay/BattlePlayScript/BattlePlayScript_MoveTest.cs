using UnityEngine;
using System.Collections;
using System;

public class BattlePlayScript_MoveTest : BattlePlayScript
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		BattlePlay.main.aOnStartBattleDelegate += OnStartBattle;
		BattlePlay.main.aOnEndBattleDelegate += OnEndBattle;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnStartBattleDelegate
	public void OnStartBattle()
	{
		StartCoroutine(UpdateScript());
	}
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnEndBattleDelegate
	public void OnEndBattle()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트를 갱신합니다.
	public IEnumerator UpdateScript()
	{
		Debug.LogWarning("UpdateScript() - " + this);

		BattleGrid lBattleGrid = BattlePlay.main.aBattleGrid;
		BattleUser lClientUser = BattlePlay.main.aUsers[0];

		lBattleGrid.aProgressInfo.AddMilliMp(lClientUser.aUserIdx, 1000000);

//		BattleCommander lRiflemanCommander = lBattleGrid.SpawnCommander(
		lBattleGrid.SpawnCommander(
			lClientUser,
			lClientUser.aUserInfo.mCommanderItems[0],
			lClientUser.aHeadquarter.aSpawnCoord + new Coord2(1000, 0),
			lClientUser.aHeadquarter.aDirectionY,
			lClientUser);

//		lRiflemanCommander.MoveTroops(lClientUser.aHeadquarter.aCoord + new Coord2(1000, 0));

// 		lRiflemanCommander.aSkill.StartSkill(lRiflemanCommander.aLeaderTroop.aCoord, lRiflemanCommander.aLeaderTroop.aDirectionY, 0);
// 		yield return StartCoroutine(WaitForFrameSeconds(4.5f));

		yield return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
