using UnityEngine;
using System.Collections;
using System;

public class BattlePlayScript_SkillTest : BattlePlayScript
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		BattlePlay.main.aOnStartBattleDelegate += OnStartBattle;
		BattlePlay.main.aOnEndBattleDelegate += OnEndBattle;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnStartBattleDelegate
	public void OnStartBattle()
	{
		StartCoroutine(UpdateScript());
	}
	//-------------------------------------------------------------------------------------------------------
	// BattlePlay.OnEndBattleDelegate
	public void OnEndBattle()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크립트를 갱신합니다.
	public IEnumerator UpdateScript()
	{
		Debug.LogWarning("UpdateScript() - " + this);

		BattleGrid lBattleGrid = BattlePlay.main.aBattleGrid;
		BattleUser lClientUser = BattlePlay.main.aUsers[0];

		BattleCommander lCommander = lBattleGrid.SpawnCommander(
			lClientUser,
			lClientUser.aUserInfo.mCommanderItems[0],
			lClientUser.aHeadquarter.aCoord + new Coord2(-100, 400),
			lClientUser.aHeadquarter.aDirectionY,
			lClientUser);

		yield return StartCoroutine(WaitForFrameSeconds(2.0f));

// 		lCommander.aSkill.StartSkill(lCommander.aLeaderTroop.aCoord, lCommander.aLeaderTroop.aDirectionY);
// 		yield return StartCoroutine(WaitForFrameSeconds(4.5f));

		for (int iTroop = 0; iTroop < lCommander.aTroops.Length; ++iTroop)
			if (lCommander.aTroops[iTroop] != null)
				lCommander.aTroops[iTroop].aStat.AddSuppression(1000000 - 1);
		yield return StartCoroutine(WaitForFrameSeconds(1.0f));

		lCommander.aSkill.StartSkill(lCommander.aLeaderTroop.aCoord, lCommander.aLeaderTroop.aDirectionY, 0);
		yield return StartCoroutine(WaitForFrameSeconds(4.5f));

		yield return null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
