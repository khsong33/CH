﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using MoonSharp.Interpreter;

public class BattleLuaController : MonoBehaviour
{
	public enum CheckAddRemoverType
	{
		Conquest,
		UseSkill,
		Hp,
		TouchCheckPoint,
		TroopNum,
		CommanderMove,
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public TextAsset vTextAsset;
	public List<GameObject> vScriptTouchChecker;
	public GameObject vScriptUIRootObject;

	public bool vIsLogControl = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (mUIInfo == null)
			mUIInfo = gameObject.GetComponent<BattleLuaUIInfo>();
		if (mCurveInfo == null)
			mCurveInfo = gameObject.GetComponent<BattleLuaCurveInfo>();

		if (vScriptTouchChecker.Count <= 0)
			Debug.LogError("vScriptTouchChecker is empty - " + this);
		if (vScriptUIRootObject == null)
			Debug.LogError("vScriptUIRootObject is empty - " + this);

		_SetActiveTouchChecker(false);
		mChecker = new Dictionary<CheckAddRemoverType, List<int>>();
		mChecker.Add(CheckAddRemoverType.Conquest, new List<int>());
		mChecker.Add(CheckAddRemoverType.UseSkill, new List<int>());
		mChecker.Add(CheckAddRemoverType.Hp, new List<int>());
		mChecker.Add(CheckAddRemoverType.TouchCheckPoint, new List<int>());
		mChecker.Add(CheckAddRemoverType.TroopNum, new List<int>());
		mChecker.Add(CheckAddRemoverType.CommanderMove, new List<int>());

		mIsCheckGameTimer = false;
		mGameTimerValue = 0.0f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// BattleScript에 필요한 정보를 초기화합니다.
	public void Init(StageSpec pStageSpec)
	{
		if (pStageSpec == null)
		{
			Debug.LogError("Not Exist StageSpec - " + this);
			return;
		}
		AssetKey lBattleScriptAssetKey = pStageSpec.mBattleScriptAssetKey;
		if (lBattleScriptAssetKey == null)
		{
			mIsPlayLua = false;
			return;
		}

		vTextAsset = AssetManager.get.Load<TextAsset>(lBattleScriptAssetKey);
		List<BattleCheckPoint> lCheckPointList = BattlePlay.main.aBattleGrid.aCheckPointList;
		for (int iCheckPoint = 0; iCheckPoint < lCheckPointList.Count; iCheckPoint++)
		{
			lCheckPointList[iCheckPoint].aOnTeamChangeDelegate += _OnCheckPointTeamChanged;
		}
		BattlePlay.main.aBattleGrid.aOnSpawnCommanderDelegate += _OnSpawnCommander;
		BattlePlay.main.vControlPanel.vClientUserControl.aOnSelectCommander += _OnSelectCommander;
		BattlePlay.main.vStage.vStageTouchControl.aOnAttackTroopDelegate += _OnAttackTroop;
		BattlePlay.main.vStage.vStageTouchControl.aOnMoveTroopsDelegate += _OnMoveTroop;
		BattlePlay.main.vStage.vStageTouchControl.aOnGrabCheckPointDelegate += _OnTouchCheckPoint;

		mIsPlayLua = true;

		_LoadStageUI(pStageSpec.mStageUIName);

		_InitLua();

		if (BattleLuaUIManager.main != null)
		{
			BattleLuaUIManager.main.aOnClick += _BattleLuaUIClick;
			BattleLuaUIManager.main.aOnCallbackFunc += _BattleLuaUICallback;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 진행 갱신시 체크할 내용 업데이트
	public void UpdateFrame()
	{
		_CheckHp();
		//_CheckGameTimer();
	}
	//-------------------------------------------------------------------------------------------------------
	// Lua에서 사용될 함수를 초기화합니다.
	private void _InitLua()
	{
		mLuaScript = new Script();

		mLuaScript.Globals["CommanderSpawn"] = (Func<int, int, string, string, bool>)_CommanderSpawn;
		mLuaScript.Globals["CommanderMove"] = (Func<int, int, string, string, bool>)_CommanderMove;
		mLuaScript.Globals["ConquestCheckPoint"] = (Func<int, int, bool>)_ConquestCheckPoint;
		mLuaScript.Globals["SetMp"] = (Func<int, int, bool>)_SetMp;
		mLuaScript.Globals["Sleep"] = (Func<float, string, bool>)_Sleep;
		mLuaScript.Globals["ShowUI"] = (Func<string, bool>)_ShowUI;
		mLuaScript.Globals["HideUI"] = (Func<string, bool>)_HideUI;
		mLuaScript.Globals["ActiveUI"] = (Func<string, bool, bool>)_ActiveUI;
		mLuaScript.Globals["MoveUI"] = (Func<string, float, float, bool>)_MoveUI;
		mLuaScript.Globals["TextPrint"] = (Func<string, string, bool>)_TextPrint;
		mLuaScript.Globals["BattlePlay"] = (Func<bool>)_BattlePlay;
		mLuaScript.Globals["BattlePause"] = (Func<bool>)_BattlePause;
		mLuaScript.Globals["Touch"] = (Func<string, bool>)_Touch;
		mLuaScript.Globals["SetCamera"] = (Func<string[], float, string, bool>)_SetCamera;
		mLuaScript.Globals["CommanderScript"] = (Func<int, int, string, float, bool>)_CommanderScript;
		mLuaScript.Globals["SetLockCamera"] = (Func<bool, bool>)_SetLockCamera;
		mLuaScript.Globals["FocusCamera"] = (Func<int, int, bool>)_FocusCamera;
		mLuaScript.Globals["EventTroopTeamSet"] = (Func<int, int, bool>)_EventTroopTeamSet;
		mLuaScript.Globals["StartSkill"] = (Func<int, int, string, string, bool>)_StartSkill;
		mLuaScript.Globals["EndSkill"] = (Func<int, int, bool>)_EndSkill;
		mLuaScript.Globals["BattleEnd"] = (Func<int, bool>)_BattleEnd;
		mLuaScript.Globals["BattleEndByScript"] = (Func<bool, bool>)_BattleEndByScript;
		mLuaScript.Globals["SetActiveMpProc"] = (Func<int, bool, bool>)_SetActiveMpProc;
		mLuaScript.Globals["CommanderPosition"] = (Func<int, int, string, string, bool>)_CommanderPosition;
		mLuaScript.Globals["GoClickUI"] = (Func<String, bool>)_GoClickUI;
		mLuaScript.Globals["StartGameTimer"] = (Func<bool>)_StartGameTimer;
		mLuaScript.Globals["EndGameTimer"] = (Func<bool>)_EndGameTimer;
		mLuaScript.Globals["CommanderAIActive"] = (Func<int, int, bool, bool>)_CommanderAIActive;
		
		// Check Adder and Remover
		mLuaScript.Globals["AddCheckConquest"] = (Func<int, int, bool>)_AddCheckConquest;
		mLuaScript.Globals["RemoveCheckConquest"] = (Func<int, int, bool>)_RemoveCheckConquest;
		mLuaScript.Globals["AddCheckSkill"] = (Func<int, int, bool>)_AddCheckSkill;
		mLuaScript.Globals["RemoveCheckSkill"] = (Func<int, int, bool>)_RemoveCheckSkill;
		mLuaScript.Globals["AddCheckHP"] = (Func<int, int, bool>)_AddCheckHp;
		mLuaScript.Globals["RemoveCheckHP"] = (Func<int, int, bool>)_RemoveCheckHp;
		mLuaScript.Globals["AddTouchCheckPoint"] = (Func<int, int, int, bool>)_AddTouchCheckPoint;
		mLuaScript.Globals["RemoveTouchCheckPoint"] = (Func<int, int, int, bool>)_RemoveTouchCheckPoint;
		mLuaScript.Globals["AddCheckTroopNum"] = (Func<int, int, bool>)_AddCheckTroopNum;
		mLuaScript.Globals["RemoveCheckTroopNum"] = (Func<int, int, bool>)_RemoveCheckTroopNum;
		mLuaScript.Globals["AddCheckCommanderMove"] = (Func<int, int, bool>)_AddCheckCommanderMove;
		mLuaScript.Globals["RemoveCheckCommanderMove"] = (Func<int, int, bool>)_RemoveCheckCommanderMove;

		mLuaScript.Globals["Log"] = (Func<string, bool>)_DebugLog;

		mLuaScript.DoString(vTextAsset.text);
	}

	//-------------------------------------------------------------------------------------------------------
	// 게임 시작 전 Lua스크립트를 실행합니다.
	public void PrePlayLua()
	{
		if (!mIsPlayLua)
			return;

		mLuaScript.Call(mLuaScript.Globals["Awake"]);
	}
	//-------------------------------------------------------------------------------------------------------
	// Lua스크립트를 실행합니다.
	public void PlayLua()
	{
		if (!mIsPlayLua)
			return;

		mLuaScript.Call(mLuaScript.Globals["Start"]);
		//StartCoroutine(_UpdateChecker());
	}
	//-------------------------------------------------------------------------------------------------------
	// Lua스크립트를 실행시 체크해야 되는 부분.
	//private IEnumerator _UpdateChecker()
	//{
	//	while (true)
	//	{
	//		_CheckHp();
	//		yield return null;
	//	}
	//}
	//-------------------------------------------------------------------------------------------------------
	// HP 체크
	private void _CheckHp()
	{
		if (mChecker[CheckAddRemoverType.Hp].Count <= 0)
			return;
		
		int lUserIdx = 0;
		int lSlotIdx = 0;
		float lHpRatio = 0;
		int lTrash = 0;
		BattleCommander lBattleCommander = null;

		List<int> lHpCheckList = mChecker[CheckAddRemoverType.Hp];
		for (int iChecker = 0; iChecker < lHpCheckList.Count; iChecker++)
		{
			_ConvertCheckerIdToParams(lHpCheckList[iChecker], out lUserIdx, out lSlotIdx, out lTrash);
			lBattleCommander = _GetBattleCommander(lUserIdx, lSlotIdx);
			if (lBattleCommander == null)
				continue;

			lHpRatio = (float)lBattleCommander.aCurTotalMilliHp / lBattleCommander.aMaxTotalMilliHp;
			mLuaScript.Call(mLuaScript.Globals["CheckCommanderHP"], lUserIdx, lSlotIdx, lHpRatio);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 타이머 전송
	private IEnumerator _CheckGameTimer()
	{
		while (mIsCheckGameTimer)
		{
			if (!BattlePlay.main.aIsPauseBattle) // Battle Pause일경우엔 시간을 보내지 않습니다.
			{
				mGameTimerValue += Time.deltaTime;
				int lGameTimer = (int)mGameTimerValue;
				mLuaScript.Call(mLuaScript.Globals["CheckGameTimer"], lGameTimer);
			}
			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Touch를 받은 후 처리입니다.
	public void OnTouchLua()
	{
		_SetActiveTouchChecker(false);
		string lTouchCallString = mTouchCallbackFuncString;
		mTouchCallbackFuncString = string.Empty;
		if (!string.IsNullOrEmpty(lTouchCallString))
		{
			mTouchCallbackFuncString = string.Empty;
			mLuaScript.Call(mLuaScript.Globals[lTouchCallString]);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// Stage에서 사용될 UI를 불러옵니다.
	private void _LoadStageUI(String pStageUIName)
	{
		if (String.IsNullOrEmpty(pStageUIName))
			return;

		GameObject lResourceObject = ResourceUtil.Load<GameObject>("ScriptUI/" + pStageUIName);
		GameObject lStageUIObject = (GameObject)GameObject.Instantiate(lResourceObject);
		lStageUIObject.transform.parent = vScriptUIRootObject.transform;
		lStageUIObject.transform.localPosition = Vector3.zero;
		lStageUIObject.transform.localScale = Vector3.one;

		BattleLuaExpandUIInfo lExpandUIInfo = (BattleLuaExpandUIInfo)lStageUIObject.GetComponent<BattleLuaExpandUIInfo>();
		if (lExpandUIInfo != null)
			mUIInfo.InitExpandStageUI(lExpandUIInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 지휘관 소환
	private bool _CommanderSpawn(int pScriptUserId, int pScriptSlotId, string pTargetType, string pTargetInfo)
	{
		if (vIsLogControl)
			Debug.Log("CommanderSpawn - UserIdx : " + pScriptUserId + "/CommanderSlotId : " + pScriptSlotId + "/TargetType : " + pTargetType + "/TargetInfo : " + pTargetInfo);

		BattleGrid lBattleGrid = BattlePlay.main.aBattleGrid;

		BattleCommander lBattleCommander = null;
		if (lBattleGrid.CheckScriptUser(pScriptUserId))
		{
			BattleUser lBattleUser = BattlePlay.main.aUsers[pScriptUserId];
			CommanderItem lCommanderItem = lBattleUser.aUserInfo.mCommanderItems[pScriptSlotId];

			Coord2 lSpawnCoord;
			int lSpawnDirectionY;
			_GetSpawnPosition(lBattleUser, lCommanderItem, pTargetType, pTargetInfo, out lSpawnCoord, out lSpawnDirectionY);
			lBattleCommander = lBattleGrid.SpawnCommander(lBattleUser,
														  lCommanderItem,
														  lSpawnCoord,
														  lSpawnDirectionY,
														  lBattleUser);
		}
		else if (lBattleGrid.CheckScriptEventPoint(pScriptUserId))
		{
			uint lEventPointGuid = lBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			BattleEventPoint lEventPoint = lBattleGrid.FindEventPointByGuid(lEventPointGuid);
			if (lEventPoint == null)
			{
				Debug.LogError("invalid event commander id:" + pScriptSlotId);
				return true; // 이벤트 못 찾음
			}

			int lCommanderGridIdx = lBattleGrid.GetCommanderGridIdx(null, lEventPoint.aCommanderSlotIdx);

			if (lBattleGrid.aCommanders[lCommanderGridIdx] != null)
			{
				Debug.LogError("already existing event commander id:" + pScriptSlotId);
				return true; // 이미 스폰된 지휘관 있음
			}
			if (!lEventPoint.aIsTriggerReady)
			{
				lEventPoint.SetTriggerReady();
				if (!lEventPoint.aCommanderItem.aIsValid)
				{
					Debug.LogError("can't find commander info from event commander id:" + pScriptSlotId);
					return true; // 지휘관이 등록 안 된 이벤트임
				}
			}

			// lEventPoint.SetTriggerReady()에 의해서 지휘관이 스폰된 상태입니다.
			lBattleCommander = lBattleGrid.aCommanders[lCommanderGridIdx];
		}
		else
		{
			Debug.LogError("Do not spawn check point defender by Lua Script");
		}
		_GoMoveByLua(lBattleCommander, pTargetType, pTargetInfo);
		return true;
	}

	//-------------------------------------------------------------------------------------------------------
	// [Lua] 지휘관 이동
	private bool _CommanderMove(int pScriptUserId, int pScriptSlotId, string pTargetType, string pTargetInfo)
	{
		if (vIsLogControl)
			Debug.Log("CommanderMove - UserIdx : " + pScriptUserId + "/CommanderSlotId : " + pScriptSlotId + "/TargetType : " + pTargetType + "/TargetInfo : " + pTargetInfo);

		BattleCommander lBattleCommander = null;
		if (BattlePlay.main.aBattleGrid.CheckScriptUser(pScriptUserId))
		{
			BattleUser lBattleUser = BattlePlay.main.aUsers[pScriptUserId];
			lBattleCommander = lBattleUser.aCommanders[pScriptSlotId];
		}
		else if (BattlePlay.main.aBattleGrid.CheckScriptEventPoint(pScriptUserId))
		{
			uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
		}
		_GoMoveByLua(lBattleCommander, pTargetType, pTargetInfo);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] CheckPoint 점령
	private bool _ConquestCheckPoint(int pCheckPointGuid, int pUserIdx)
	{
		BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid((uint)pCheckPointGuid);
		BattleUser lBattleUser = BattlePlay.main.aUsers[pUserIdx];

		lCheckPoint.UpdateTeam(lBattleUser.aTeamIdx);
		lCheckPoint.SpawnDefenderTroop(lBattleUser.aUserInfo.mBattleNation, lBattleUser);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] MP 변경
	private bool _SetMp(int pUserIdx, int pMp)
	{
		BattlePlay.main.aProgressInfo.ChangeMp(pUserIdx, pMp);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 시간 지연 이벤트
	private bool _Sleep(float pSec, string pCallbackString)
	{
		StartCoroutine(_OnSleep(pSec, pCallbackString));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 시간 지연 이벤트 처리
	private IEnumerator _OnSleep(float pSec, string pCallbackString)
	{
// 		FrameUpdater lFrameUpdater = BattlePlay.main.aBattleGrid.aFrameUpdater;
// 
// 		if (!BattlePlay.main.aIsPauseBattle)
// 		{
// 			int lCurrentFrameNo = lFrameUpdater.aFrameNo;
// 			int lWaitFrameCount = (int)(pSec * 1000.0f) / lFrameUpdater.aFrameDelta;
// 			int lWaitFrameNo = lCurrentFrameNo + lWaitFrameCount;
// 
// 			while (lFrameUpdater.aFrameNo < lWaitFrameNo)
// 			{
// 				yield return null;
// 			}
// 		}
// 		else
		{
			yield return new WaitForSeconds(pSec);
		}

		if(!string.IsNullOrEmpty(pCallbackString))
			mLuaScript.Call(mLuaScript.Globals[pCallbackString]);
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI를 표시합니다.
	private bool _ShowUI(string pKey)
	{
		mUIInfo.ShowUI(pKey);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI를 숨깁니다.
	private bool _HideUI(string pKey)
	{
		mUIInfo.HideUI(pKey);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI를 활성/비활성화 시킵니다.
	private bool _ActiveUI(string pKey, bool pActive)
	{
		mUIInfo.ActiveUI(pKey, pActive);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI의 위치를 변경합니다.
	private bool _MoveUI(string pKey, float pX, float pY)
	{
		mUIInfo.MoveUI(pKey, pX, pY);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI에 텍스트를 작성합니다.
	private bool _TextPrint(string pKey, string pPrintTextKey)
	{
		mUIInfo.PrintText(pKey, pPrintTextKey);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 전투 중지
	private bool _BattlePlay()
	{
		BattlePlay.main.PauseBattle(false);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 전투 재개
	private bool _BattlePause()
	{
		BattlePlay.main.PauseBattle(true);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 터치 처리
	private bool _Touch(string pCallbackString)
	{
		_SetActiveTouchChecker(true);
		mTouchCallbackFuncString = pCallbackString;
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 스킬 사용
	private bool _StartSkill(int pScriptUserId, int pScriptSlotId, string pTargetType, string pTargetInfo)
	{
		BattleCommander lBattleCommander = _GetBattleCommander(pScriptUserId, pScriptSlotId);
		if (lBattleCommander == null)
			return true;

		Coord2 lSkillCoord = new Coord2();
		bool lExplicitSkillDirectionY = false;
		int lSkillDirectionY = 0;

		switch (pTargetType.ToUpper())
		{
			case "CHECKPOINT":
				uint lCheckPointId = 0;
				uint.TryParse(pTargetInfo, out lCheckPointId);
				BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointId);
				lSkillCoord = lCheckPoint.aCoord;
				break;
			case "COORDINATE":
				string[] lCoordString = pTargetInfo.Split('^');
				lSkillCoord = new Coord2(int.Parse(lCoordString[0]), int.Parse(lCoordString[1]));
				if (lCoordString.Length >= 3)
				{
					lExplicitSkillDirectionY = true;
					lSkillDirectionY = int.Parse(lCoordString[2]); // 명시적으로 지정된 방향각을 사용
				}
				break;
			case "COMMANDER":
				string[] lTargetCommanderString = pTargetInfo.Split('^');
				int lTargetUserIdx = int.Parse(lTargetCommanderString[0]);
				int lTargetCommanderSlotId = int.Parse(lTargetCommanderString[1]);
				BattleCommander lTargetCommander = BattlePlay.main.aBattleGrid.aUsers[lTargetUserIdx].aCommanders[lTargetCommanderSlotId];
				lSkillCoord = lTargetCommander.aCoord;
				break;
			default:
				break;
		}

		if (!lExplicitSkillDirectionY) // 명시적으로 지정된 방향각이 없을 때
		{
			if (lSkillCoord != lBattleCommander.aCoord) // 스킬 사용 좌표가 지휘관 좌표와 다르다면
				lSkillDirectionY = BattleCommander.GetFormationMoveDirectionY(lSkillCoord, lBattleCommander.aCoord); // 스킬 좌표를 향하는 방향으로
			else // 같은 좌표라면
				lSkillDirectionY = lBattleCommander.aDirectionY; // 현재 지휘관 방향으로
		}

		lBattleCommander.aSkill.StartSkill(lSkillCoord, lSkillDirectionY, 0);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 스킬 중지
	private bool _EndSkill(int pScriptUserId, int pScriptSlotId)
	{
		BattleCommander lBattleCommander = _GetBattleCommander(pScriptUserId, pScriptSlotId);
		if (lBattleCommander == null)
			return true;

		lBattleCommander.aSkill.StopSkill(0);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 전투 종료
	private bool _BattleEnd(int pBattleEndCode)
	{
		BattlePlay.main.aProgressInfo.SetBattleEnd(
			BattlePlay.main.aBattleGrid,
			BattlePlay.main.aClientUser.aUserIdx,
			(BattleProtocol.BattleEndCode)pBattleEndCode);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 전투 종료 스크립트 제어
	private bool _BattleEndByScript(bool pUseScript)
	{
		BattlePlay.main.aProgressInfo.EnableBattleEnd(!pUseScript);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] Mp 생산
	private bool _SetActiveMpProc(int pScriptUserId, bool pIsActive)
	{
		BattlePlay.main.aProgressInfo.SetActiveMpProc(pScriptUserId, pIsActive);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua]부대 위치 순간 이동
	private bool _CommanderPosition(int pScriptUserId, int pScriptSlotId, string pTargetType, string pTargetInfo)
	{
		BattleCommander lBattleCommander = null;
		if (BattlePlay.main.aBattleGrid.CheckScriptUser(pScriptUserId))
		{
			BattleUser lBattleUser = BattlePlay.main.aUsers[pScriptUserId];
			lBattleCommander = lBattleUser.aCommanders[pScriptSlotId];
		}
		else
		{
			uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
		}
		Coord2 lCoord = new Coord2();
		int lDirectionY = 0;
		switch (pTargetType.ToUpper())
		{
			case "CHECKPOINT":
				uint lCheckPointId = 0;
				uint.TryParse(pTargetInfo, out lCheckPointId);
				BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointId);
				lCoord = lCheckPoint.aCoord;
				lDirectionY = lCheckPoint.aDirectionY;
				break;
			case "COORDINATE":
				string[] lCoordString = pTargetInfo.Split('^');
				lCoord = new Coord2(int.Parse(lCoordString[0]), int.Parse(lCoordString[1]));
				if (lCoordString.Length >= 3)
				{
					lDirectionY = int.Parse(lCoordString[2]);
				}
				else
				{
					lDirectionY = BattleCommander.GetFormationMoveDirectionY(lCoord, lBattleCommander.aCoord);
				}
				break;
			case "COMMANDER":
				string[] lTargetCommanderString = pTargetInfo.Split('^');
				int lTargetUserIdx = int.Parse(lTargetCommanderString[0]);
				int lTargetCommanderSlotId = int.Parse(lTargetCommanderString[1]);
				BattleCommander lTargetCommander = BattlePlay.main.aBattleGrid.aUsers[lTargetUserIdx].aCommanders[lTargetCommanderSlotId];
				lCoord = lTargetCommander.aCoord;
				lDirectionY = lTargetCommander.aDirectionY;
				break;
			default:
				break;
		}
		lBattleCommander.SetCoordAndDirectionY(lCoord, lDirectionY);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] UI 클릭
	private bool _GoClickUI(string pUIName)
	{
		if (vIsLogControl)
			Debug.Log("Click UI - " + pUIName);

		BattleLuaUIManager.main.GoClick(pUIName);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] GameTimer 체크 시작
	private bool _StartGameTimer()
	{
		if (vIsLogControl)
			Debug.Log("Start Game Timer");

		if (mIsCheckGameTimer)
			return false;

		mIsCheckGameTimer = true;
		StartCoroutine(_CheckGameTimer());
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] GameTimer 체크 정지
	private bool _EndGameTimer()
	{
		if (vIsLogControl)
			Debug.Log("End Game Timer");

		mIsCheckGameTimer = false;
		mGameTimerValue = 0.0f;
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] Commander AI 제어
	private bool _CommanderAIActive(int pScriptUserId, int pScriptSlotId, bool pActive)
	{
		BattleCommander lBattleCommander = null;
		if (BattlePlay.main.aBattleGrid.CheckScriptUser(pScriptUserId))
		{
			BattleUser lBattleUser = BattlePlay.main.aUsers[pScriptUserId];
			lBattleCommander = lBattleUser.aCommanders[pScriptSlotId];
		}
		else if(BattlePlay.main.aBattleGrid.CheckScriptEventPoint(pScriptUserId))
		{
			uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
		}
		else
		{
			Debug.LogError("Not allow user script id - " + pScriptUserId);
			lBattleCommander = null;
		}

		if (lBattleCommander != null)
			BattlePlay.main.vBattleNetwork.SetFrameInput_ForceAi(lBattleCommander, pActive);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua] 로그
	private bool _DebugLog(string pLog)
	{
		if (vIsLogControl)
			Debug.Log("Lua Log : " + pLog);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 체크포인트의 주인이 바뀔 시 호출
	private void _OnCheckPointTeamChanged(BattleCheckPoint pCheckPoint)
	{
		if (mChecker[CheckAddRemoverType.Conquest].Count <= 0)
			return;

		int lCheckerId = _GetCheckId((int)pCheckPoint.aCheckPointGuid, pCheckPoint.aTeamIdx);
		bool isExist = mChecker[CheckAddRemoverType.Conquest].Exists(inCheckId => inCheckId == lCheckerId);
		if (isExist)
		{
			mLuaScript.Call(mLuaScript.Globals["CheckConquestCheckPoint"], pCheckPoint.aCheckPointGuid, pCheckPoint.aTeamIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 체크포인트 선택시
	private void _OnTouchCheckPoint(BattleCommander pCommander, BattleCheckPoint pCheckPoint)
	{
		if (pCommander.aUser == null)
			return;
		if (mChecker[CheckAddRemoverType.TouchCheckPoint].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId, (int)pCheckPoint.aCheckPointGuid);
		bool isExist = mChecker[CheckAddRemoverType.TouchCheckPoint].Exists(inCheckId => inCheckId == lCheckerId);
		if (isExist)
		{
			mLuaScript.Call(mLuaScript.Globals["CheckTouchCheckPoint"], pCommander.aScriptUserId, pCommander.aScriptSlotId, (int)pCheckPoint.aCheckPointGuid);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 소환시 호출
	private void _OnSpawnCommander(BattleCommander pCommander)
	{
		mLuaScript.Call(mLuaScript.Globals["CheckCommanderSpawn"], pCommander.aScriptUserId, pCommander.aScriptSlotId);

		// 스킬 사용 여부를 확인합니다.
		pCommander.aSkill.aOnStartSkillDelegate += _OnStartSkill;
		pCommander.aSkill.aOnStopSkillDelegate += _OnStopSkill;
		pCommander.aOnVeterancyRankUpDelegate += _OnVeterancyRankUpDelegate;
		pCommander.aOnRecruitTroopsDelegate += _OnRecruitTroop;
		pCommander.aOnDestroyedTroopDelegate += _OnDestoryedTroop;
		pCommander.aOnDestroyedDelegate += _OnDestoryedCommander;
		pCommander.aOnMoveStartedDelegate += _OnStartedMove;
		pCommander.aOnMoveStoppedDelegate += _OnStoppedMove;
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 재소환 시 콜백
	private void _OnRecruitTroop(BattleCommander pCommander)
	{
		if (mChecker[CheckAddRemoverType.TroopNum].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		bool isExist = mChecker[CheckAddRemoverType.TroopNum].Exists(inCheckId => inCheckId == lCheckerId);
	
		if (isExist)
			mLuaScript.Call(mLuaScript.Globals["CheckTroopNum"], pCommander.aScriptUserId, pCommander.aScriptSlotId, pCommander.aLiveTroopCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 부대 소멸시 콜백
	private void _OnDestoryedTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		if (mChecker[CheckAddRemoverType.TroopNum].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		bool isExist = mChecker[CheckAddRemoverType.TroopNum].Exists(inCheckId => inCheckId == lCheckerId);
		if(isExist)
			mLuaScript.Call(mLuaScript.Globals["CheckTroopNum"], pCommander.aScriptUserId, pCommander.aScriptSlotId, pCommander.aLiveTroopCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 전멸시 콜백
	private void _OnDestoryedCommander(int pScriptUserId, int pScriptSlotId)
	{
		mLuaScript.Call(mLuaScript.Globals["CheckCommanderRetreat"], pScriptUserId, pScriptSlotId);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 부대 이동시 콜백
	private void _OnStartedMove(BattleCommander pCommander)
	{
		if (mChecker[CheckAddRemoverType.CommanderMove].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		bool isExist = mChecker[CheckAddRemoverType.CommanderMove].Exists(inCheckId => inCheckId == lCheckerId);

		if (isExist)
			mLuaScript.Call(mLuaScript.Globals["CheckCommanderMove"], pCommander.aScriptUserId, pCommander.aScriptSlotId, 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 부대 정지시 콜백
	private void _OnStoppedMove(BattleCommander pCommander)
	{
		if (mChecker[CheckAddRemoverType.CommanderMove].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		bool isExist = mChecker[CheckAddRemoverType.CommanderMove].Exists(inCheckId => inCheckId == lCheckerId);

		if (isExist)
			mLuaScript.Call(mLuaScript.Globals["CheckCommanderMove"], pCommander.aScriptUserId, pCommander.aScriptSlotId, 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 선택시 호출
	private void _OnSelectCommander(BattleCommander pCommander)
	{
		if (pCommander.aUser == null)
			return;

		mLuaScript.Call(mLuaScript.Globals["CheckSelectCommander"], pCommander.aScriptUserId, pCommander.aScriptSlotId);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 타겟 지휘관 선택시 호출
	private void _OnAttackTroop(BattleCommander pCommander, BattleTroop pTargetTroop)
	{
		if (pCommander.aUser == null)
			return;

		mLuaScript.Call(mLuaScript.Globals["CheckTargetCommander"], 
						pCommander.aScriptUserId, pCommander.aScriptSlotId,
						pTargetTroop.aCommander.aScriptUserId, pTargetTroop.aCommander.aScriptSlotId);
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 지면터치(이동)시 호출
	private void _OnMoveTroop(BattleCommander pCommander, Coord2 pMoveCoord, int pMoveDirectionY)
	{
		if (pCommander.aUser == null)
			return;

		mLuaScript.Call(mLuaScript.Globals["CheckTouchMove"], pCommander.aScriptUserId, pCommander.aScriptSlotId, pMoveCoord.x, pMoveCoord.z, MathUtil.AbsDegree(pMoveDirectionY));
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 스킬 사용시 호출
	private void _OnStartSkill(BattleCommander pCommander)
	{
		if (mChecker[CheckAddRemoverType.UseSkill].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		if (mChecker[CheckAddRemoverType.UseSkill].Exists(isCheckId => isCheckId == lCheckerId))
		{
			mLuaScript.Call(mLuaScript.Globals["CheckCommanderSkill"], pCommander.aScriptUserId, pCommander.aScriptSlotId, 1);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 지휘관 스킬 완료시 호출
	private void _OnStopSkill(BattleCommander pCommander)
	{
		if (mChecker[CheckAddRemoverType.UseSkill].Count <= 0)
			return;

		int lCheckerId = _GetCheckId(pCommander.aScriptUserId, pCommander.aScriptSlotId);
		if (mChecker[CheckAddRemoverType.UseSkill].Exists(isCheckId => isCheckId == lCheckerId))
		{
			mLuaScript.Call(mLuaScript.Globals["CheckCommanderSkill"], pCommander.aScriptUserId, pCommander.aScriptSlotId, 0);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [콜백] 베테랑 랭크업시 호출
	private void _OnVeterancyRankUpDelegate(BattleCommander pCommander)
	{
		mLuaScript.Call(mLuaScript.Globals["CheckVeterancy"], pCommander.aScriptUserId, pCommander.aScriptSlotId, pCommander.aVeterancyRank);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleLuaUIComponent가 등록된 Component에서 클릭 메시지를 호출한 내용을 처리합니다.
	private void _BattleLuaUIClick(GameObject pObject)
	{
		mLuaScript.Call(mLuaScript.Globals["ClickUI"], pObject.name);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleLuaUIComponent가 등록된 Component에서 범용으로 호출되는 내용을 추가합니다.
	private void _BattleLuaUICallback(GameObject pObject, String pCallbackName)
	{
		mLuaScript.Call(mLuaScript.Globals[pCallbackName], pObject.name);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 부대 팀 설정
	private bool _EventTroopTeamSet(int pEventSlotIdx, int pTeamIdx)
	{
		uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pEventSlotIdx);
		BattleCommander lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);

		lBattleCommander.UpdateTeam(pTeamIdx);

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동을 타입별로 진행합니다.
	private void _GoMoveByLua(BattleCommander pBattleCommander, string pTargetType, string pTargetInfo)
	{
		if (pBattleCommander == null)
			return;

		switch (pTargetType.ToUpper())
		{
			case "CHECKPOINT":
				uint lCheckPointId = 0;
				uint.TryParse(pTargetInfo, out lCheckPointId);
				BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointId);
				if (lCheckPoint.aLinkHeadquarter != null
					&& lCheckPoint.aTeamIdx == pBattleCommander.aTeamIdx) // 아군 본부라면
				{
					BattlePlay.main.vBattleNetwork.SetFrameInput_RetreatCommander(pBattleCommander);
				}
				else // 거점 이동을 합니다.
				{
					BattlePlay.main.vBattleNetwork.SetFrameInput_GrabCheckPoint(pBattleCommander, lCheckPoint);
				}
				break;
			case "COORDINATE":
				string[] lCoordString = pTargetInfo.Split('^');
				Coord2 lCoord = new Coord2(int.Parse(lCoordString[0]), int.Parse(lCoordString[1]));
				int lDirectionY = 0;
				if (lCoordString.Length >= 3)
				{
					lDirectionY = int.Parse(lCoordString[2]);
				}
				else
				{
					lDirectionY = BattleCommander.GetFormationMoveDirectionY(lCoord, pBattleCommander.aCoord);
				}
				BattlePlay.main.vBattleNetwork.SetFrameInput_MoveTroops(pBattleCommander, lCoord, lDirectionY);
				break;
			case "COMMANDER":
				string[] lTargetCommanderString = pTargetInfo.Split('^');
				int lTargetUserIdx = int.Parse(lTargetCommanderString[0]);
				int lTargetCommanderSlotId = int.Parse(lTargetCommanderString[1]);
				BattleCommander lTargetCommander;
				if (BattlePlay.main.aBattleGrid.CheckScriptEventPoint(lTargetUserIdx))
				{
					uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(lTargetCommanderSlotId);
					lTargetCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
					if (lTargetCommander == null)
					{
						Debug.LogError("can't find event commander:" + lTargetCommanderSlotId);
					}
				}
				else if (BattlePlay.main.aBattleGrid.CheckScriptCheckPoint(lTargetUserIdx))
				{
					uint lCheckPointGuid = BattlePlay.main.aBattleGrid.GetScriptCheckPointGuid(lTargetCommanderSlotId);
					lTargetCommander = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointGuid).aCurrentDefenderBattleCommander;
					if (lTargetCommander == null)
					{
						Debug.LogError("can't find defender commander:" + lTargetCommanderSlotId);
					}
				}
				else
				{
					lTargetCommander = BattlePlay.main.aBattleGrid.aUsers[lTargetUserIdx].aCommanders[lTargetCommanderSlotId];
					if (lTargetCommander == null)
					{
						Debug.LogError("can't find user commander:" + lTargetCommanderSlotId);
					}
				}

				BattleTroop lTargetTroop = lTargetCommander.aLeaderTroop;
				// 그 부대를 공격합니다.
				BattlePlay.main.vBattleNetwork.SetFrameInput_AttackTroop(pBattleCommander, lTargetTroop);
				break;
			default:
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua]카메라 이동을 설정합니다.
	private bool _SetCamera(string[] pParam, float pSec, string pAccel)
	{
		string lStartTypeString = pParam[0];
		string lStartPointString = pParam[1];
		string lEndTypeString = pParam[2];
		string lEndPointString = pParam[3];

		Coord2 lStartCoord = _GetCameraCoord(lStartTypeString, lStartPointString);
		Coord2 lTargetCoord = _GetCameraCoord(lEndTypeString, lEndPointString);

		StartCoroutine(_PlayCamera(lStartCoord, lTargetCoord, pSec, pAccel));

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라 이동을 실행합니다.
	private IEnumerator _PlayCamera(Coord2 pStartCoord, Coord2 pTargetCoord, float pSec, string pAccel)
	{
		if (pStartCoord.x != -1 && pStartCoord.z != -1)
		{
			BattlePlay.main.vControlPanel.FocusCoord(pStartCoord); // 점근 없이 바로 포커싱
		}
		yield return null;

		AnimationCurve lAccelCurve = mCurveInfo.GetCurve(pAccel);
		BattlePlay.main.vControlPanel.FocusCoord(pTargetCoord, lAccelCurve, pSec);

	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라 이동할 위치를 설정합니다.
	private Coord2 _GetCameraCoord(string pType, string pPoint)
	{
		Coord2 lResultCoord = new Coord2();
		switch (pType)
		{
			case "Commander":
				string[] lUserInfoString = pPoint.Split('^');
				int lScriptUserId = int.Parse(lUserInfoString[0]);
				int lScriptSlotId = int.Parse(lUserInfoString[1]);
				BattleCommander lBattleCommander = _GetBattleCommander(lScriptUserId, lScriptSlotId);
				lResultCoord = lBattleCommander.aCoord;
				break;
			case "Coordinate":
				string[] lCoordString = pPoint.Split('^');
				lResultCoord.x = int.Parse(lCoordString[0]);
				lResultCoord.z = int.Parse(lCoordString[1]);
				break;
			case "CheckPoint":
				uint lCheckPointGuid = (uint)int.Parse(pPoint);
				BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointGuid);
				lResultCoord = lCheckPoint.aCoord;
				break;
			default:
				lResultCoord.x = -1;
				lResultCoord.z = -1;
				break;
		}
		return lResultCoord;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua]지휘관 대사를 처리합니다.
	private bool _CommanderScript(int pScriptUserId, int pScriptSlotId, string pMessageKey, float pDuration)
	{
		BattleCommander lBattleCommander = null;
		if (BattlePlay.main.aBattleGrid.CheckScriptEventPoint(pScriptUserId))
		{
			uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
			if (lBattleCommander == null)
			{
				Debug.LogError("can't find event commander:" + pScriptSlotId);
				return true;
			}
		}
		else if (BattlePlay.main.aBattleGrid.CheckScriptCheckPoint(pScriptUserId))
		{
			uint lCheckPointGuid = BattlePlay.main.aBattleGrid.GetScriptCheckPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointGuid).aCurrentDefenderBattleCommander;
			if (lBattleCommander == null)
			{
				Debug.LogError("can't find defender commander:" + pScriptSlotId);
				return true;
			}
		}
		else
		{
			lBattleCommander = BattlePlay.main.aUsers[pScriptUserId].aCommanders[pScriptSlotId];
			if (lBattleCommander == null)
			{
				Debug.LogError("can't find user commander:" + pScriptSlotId);
				return true;
			}
		}
		if (lBattleCommander.aLeaderTroop != null)
			lBattleCommander.aLeaderTroop.aStageTroop.ShowTroopScript(pMessageKey, pDuration);
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua]카메라를 잠금/해제 합니다.
	private bool _SetLockCamera(bool pIsLock)
	{
		BattlePlay.main.vControlPanel.vClientUserControl.vTouchPanel.vIsEnableDrag = !pIsLock;
		BattlePlay.main.vControlPanel.vClientUserControl.vTouchPanel.vIsEnableZoom = !pIsLock;

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// [Lua]카메라가 지휘관을 쫓아갑니다.
	private bool _FocusCamera(int pScriptUserId, int pScriptSlotId)
	{
		BattleCommander lBattleCommander = _GetBattleCommander(pScriptUserId, pScriptSlotId);

		BattlePlay.main.vControlPanel.FocusCommander(
			lBattleCommander,
			false,
			BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
			BattlePlay.main.vControlPanel.vDefaultFocusingDelay); // 퇴각하는 부대로 카메라 이동

		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// Adder and Remover
	//-------------------------------------------------------------------------------------------------------
	// 점령 확인
	private bool _AddCheckConquest(int pCheckPointGuid, int pTeamIdx)
	{
		mChecker[CheckAddRemoverType.Conquest].Add(_GetCheckId(pCheckPointGuid, pTeamIdx));
		return true;
	}
	private bool _RemoveCheckConquest(int pCheckPointGuid, int pTeamIdx)
	{
		mChecker[CheckAddRemoverType.Conquest].Remove(_GetCheckId(pCheckPointGuid, pTeamIdx));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 사용 확인
	private bool _AddCheckSkill(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.UseSkill].Add(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	private bool _RemoveCheckSkill(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.UseSkill].Remove(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// Hp 확인
	private bool _AddCheckHp(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.Hp].Add(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	private bool _RemoveCheckHp(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.Hp].Remove(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// CheckPoint 선택 확인
	private bool _AddTouchCheckPoint(int pUserIdx, int pSlotIdx, int pCheckPointGuid)
	{
		mChecker[CheckAddRemoverType.TouchCheckPoint].Add(_GetCheckId(pUserIdx, pSlotIdx, pCheckPointGuid));
		return true;
	}
	private bool _RemoveTouchCheckPoint(int pUserIdx, int pSlotIdx, int pCheckPointGuid)
	{
		mChecker[CheckAddRemoverType.TouchCheckPoint].Remove(_GetCheckId(pUserIdx, pSlotIdx, pCheckPointGuid));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 살아있는 부대수 확인	
	private bool _AddCheckTroopNum(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.TroopNum].Add(_GetCheckId(pUserIdx, pSlotIdx, 0));
		return true;
	}
	private bool _RemoveCheckTroopNum(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.TroopNum].Remove(_GetCheckId(pUserIdx, pSlotIdx, 0));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 이동 시작 및 완료 확인	
	private bool _AddCheckCommanderMove(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.CommanderMove].Add(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	private bool _RemoveCheckCommanderMove(int pUserIdx, int pSlotIdx)
	{
		mChecker[CheckAddRemoverType.CommanderMove].Remove(_GetCheckId(pUserIdx, pSlotIdx));
		return true;
	}
	//-------------------------------------------------------------------------------------------------------
	// Checker Id 발급
	private int _GetCheckId(int pParam1, int pParam2, int pParam3 = 0)
	{
		return (pParam1 * 100000) + (pParam2 * 1000) + pParam3;
	}
	//-------------------------------------------------------------------------------------------------------
	// Checker Id 원래값으로 변경
	private void _ConvertCheckerIdToParams(int pCheckerId, out int pParam1, out int pParam2, out int pParam3)
	{
		pParam1 = pCheckerId / 100000;
		pParam2 = (pCheckerId - (pParam1 * 100000)) / 1000;
		pParam3 = (pCheckerId - (pParam1 * 100000) - (pParam2 * 1000));
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander 획득
	private BattleCommander _GetBattleCommander(int pScriptUserId, int pScriptSlotId)
	{
		BattleCommander lBattleCommander = null;
		if (BattlePlay.main.aBattleGrid.CheckScriptEventPoint(pScriptUserId))
		{
			uint lEventPointGuid = BattlePlay.main.aBattleGrid.GetScriptEventPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindEventPointCommanderByGuid(lEventPointGuid);
		}
		else if (BattlePlay.main.aBattleGrid.CheckScriptCheckPoint(pScriptUserId))
		{
			uint lCheckPointGuid = BattlePlay.main.aBattleGrid.GetScriptCheckPointGuid(pScriptSlotId);
			lBattleCommander = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointGuid).aCurrentDefenderBattleCommander;
		}
		else
		{
			lBattleCommander = BattlePlay.main.aUsers[pScriptUserId].aCommanders[pScriptSlotId];
		}
		return lBattleCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// Script를 통한 Spawn 위치 및 방향 획득
	private void _GetSpawnPosition(BattleUser pBattleUser, CommanderItem pCommanderItem, String pTargetType, String pTargetInfo, out Coord2 outSpawnCoord, out int outSpawnDirectionY)
	{
		outSpawnCoord = new Coord2();
		outSpawnDirectionY = 0;

		if (pCommanderItem.mCommanderSpec.mSpawnType == CommanderSpawnType.Aircraft
			|| pCommanderItem.mCommanderSpec.mSpawnType == CommanderSpawnType.Defense)
		{
			switch (pTargetType.ToUpper())
			{
				case "CHECKPOINT":
					uint lCheckPointId = 0;
					uint.TryParse(pTargetInfo, out lCheckPointId);
					BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid(lCheckPointId);
					outSpawnCoord = lCheckPoint.aCoord;
					outSpawnDirectionY = lCheckPoint.aDirectionY;
					break;
				case "COORDINATE":
					string[] lCoordString = pTargetInfo.Split('^');
					outSpawnCoord = new Coord2(int.Parse(lCoordString[0]), int.Parse(lCoordString[1]));
					if (lCoordString.Length >= 3)
					{
						outSpawnDirectionY = int.Parse(lCoordString[2]);
					}
					else
					{
						outSpawnDirectionY = pBattleUser.aHeadquarter.aDirectionY;
					}
					break;
				case "COMMANDER":
					string[] lTargetCommanderString = pTargetInfo.Split('^');
					int lTargetUserIdx = int.Parse(lTargetCommanderString[0]);
					int lTargetCommanderSlotId = int.Parse(lTargetCommanderString[1]);
					BattleCommander lTargetCommander = BattlePlay.main.aBattleGrid.aUsers[lTargetUserIdx].aCommanders[lTargetCommanderSlotId];
					outSpawnCoord = lTargetCommander.aCoord;
					outSpawnDirectionY = lTargetCommander.aDirectionY;
					break;
				default:
					break;
			}
		}
		else
		{
			outSpawnCoord = pBattleUser.aHeadquarter.aCoord;
			outSpawnDirectionY = pBattleUser.aHeadquarter.aDirectionY;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Touch Checker 셋팅
	private void _SetActiveTouchChecker(bool pIsActive)
	{
		for (int iTouch = 0; iTouch < vScriptTouchChecker.Count; iTouch++)
		{
			vScriptTouchChecker[iTouch].SetActive(pIsActive);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Script mLuaScript;

	private BattleLuaUIInfo mUIInfo;
	private BattleLuaCurveInfo mCurveInfo;
	
	private bool mIsPlayLua;
	private string mTouchCallbackFuncString;
	private Dictionary<CheckAddRemoverType, List<int>> mChecker;
	private bool mIsCheckGameTimer;
	private float mGameTimerValue;
}
