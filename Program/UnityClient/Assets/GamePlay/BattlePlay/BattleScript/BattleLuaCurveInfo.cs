﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//-------------------------------------------------------------------------------------------------------
// UI키와 object를 pair로 관리합니다.
public class BattleLuaCurveInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public String[] vKeys;
	public AnimationCurve[] vCurves;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mDatas = new Dictionary<String, AnimationCurve>();
		for (int iCurve = 0; iCurve < vCurves.Length; iCurve++)
		{
			mDatas.Add(vKeys[iCurve], vCurves[iCurve]);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Get Curve
	public AnimationCurve GetCurve(string pKey)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist curve key - " + pKey);
			return null;
		}
		return mDatas[pKey];
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<String, AnimationCurve> mDatas;
}