﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//-------------------------------------------------------------------------------------------------------
// UI키와 object를 pair로 관리합니다.
public class BattleLuaUIInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public String[] vKeys;
	public GameObject[] vObjects;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mDatas = new Dictionary<String, GameObject>();
		mOriginLocalPosition = new Dictionary<String, Vector3>();
		mUIShow = new Dictionary<string, bool>();
		for (int iObject = 0; iObject < vObjects.Length; iObject++)
		{
			Vector3 lLocalPosition = new Vector3(vObjects[iObject].transform.localPosition.x, 
												 vObjects[iObject].transform.localPosition.y,
												 vObjects[iObject].transform.localPosition.z);
			mOriginLocalPosition.Add(vKeys[iObject], lLocalPosition);
			mDatas.Add(vKeys[iObject], vObjects[iObject]);
			mUIShow.Add(vKeys[iObject], true);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Add Expand Stage UI
	public void InitExpandStageUI(BattleLuaExpandUIInfo pExpandUIInfo)
	{
		if (pExpandUIInfo == null)
			return;

		for (int iObject = 0; iObject < pExpandUIInfo.vObjects.Count; iObject++)
		{
			Vector3 lLocalPosition = new Vector3(pExpandUIInfo.vObjects[iObject].transform.localPosition.x,
												 pExpandUIInfo.vObjects[iObject].transform.localPosition.y,
												 pExpandUIInfo.vObjects[iObject].transform.localPosition.z);

			mOriginLocalPosition.Add(pExpandUIInfo.vKeys[iObject], lLocalPosition);
			mDatas.Add(pExpandUIInfo.vKeys[iObject], pExpandUIInfo.vObjects[iObject]);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Show UI
	public void ShowUI(String pKey)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist Key - " + pKey);
			return;
		}
		if (mUIShow[pKey])
			return;

		mDatas[pKey].transform.localPosition = new Vector3(mOriginLocalPosition[pKey].x, 
														   mOriginLocalPosition[pKey].y,
														   mOriginLocalPosition[pKey].z);

		mUIShow[pKey] = true;
	}	
	//-------------------------------------------------------------------------------------------------------
	// Hide UI
	public void HideUI(String pKey)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist Key - " + pKey);
			return;
		}
		if (!mUIShow[pKey])
			return;

		mOriginLocalPosition[pKey] = new Vector3(mDatas[pKey].transform.localPosition.x,
											     mDatas[pKey].transform.localPosition.y,
												 mDatas[pKey].transform.localPosition.z);

		mDatas[pKey].transform.localPosition = new Vector3(mDatas[pKey].transform.localPosition.x,
														   mDatas[pKey].transform.localPosition.y + 5000,
														   mDatas[pKey].transform.localPosition.z);
		mUIShow[pKey] = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// Active UI
	public void ActiveUI(String pKey, bool pActive)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist Key - " + pKey);
			return;
		}

		UITweener lTweener = mDatas[pKey].GetComponent<UITweener>();
		if (lTweener != null && !pActive)
		{
			lTweener.ResetToBeginning();
			lTweener.enabled = true;
		}

		mDatas[pKey].SetActive(pActive);	
	}
	//-------------------------------------------------------------------------------------------------------
	// Move UI
	public void MoveUI(String pKey, float pX, float pY)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist Key - " + pKey);
			return;
		}
		mDatas[pKey].transform.localPosition = new Vector3(pX, pY, mDatas[pKey].transform.localPosition.z);
	}
	//-------------------------------------------------------------------------------------------------------
	// Print Text
	public void PrintText(String pKey, String pTextKey)
	{
		if (!mDatas.ContainsKey(pKey))
		{
			Debug.LogError("Not Exist Key - " + pKey);
			return;
		}
		UILabel lTextLabel = mDatas[pKey].GetComponent<UILabel>();
		if(lTextLabel == null)
		{
			Debug.LogError("Not Exist UILabel - " + mDatas[pKey].name);
			return;
		}
		lTextLabel.text = LocalizedTable.get.GetLocalizedText(pTextKey);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<String, GameObject> mDatas;
	private Dictionary<String, bool> mUIShow;
	private Dictionary<String, Vector3> mOriginLocalPosition;
}