﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//-------------------------------------------------------------------------------------------------------
// Scene에 상관없이 갖고 있어야할 데이터를 저장하는 클래스
//-------------------------------------------------------------------------------------------------------
public class BattleLuaUIManager : MonoBehaviour
{
	public delegate void OnClickUI(GameObject pObject);
	public delegate void OnCallbackFunc(GameObject pObject, String pCallbackName); // 범용성 있는 콜백 함수
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public OnClickUI aOnClick { get; set; }
	public OnCallbackFunc aOnCallbackFunc { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 인스턴스
	//-------------------------------------------------------------------------------------------------------
	public static BattleLuaUIManager main = null;
	private static BattleLuaUIManager main_destroyed = null;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		// 전역 참조를 지정합니다.
		if ((main == null) ||
			(main == main_destroyed))
			main = this;
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestory()
	{
		if (main == this)
			main_destroyed = this;
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 클릭시 처리
	public void OnClick(GameObject pObject)
	{
		if (aOnClick != null)
			aOnClick(pObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 각 UI별 범용 콜백 처리
	public void OnCallbackUI(GameObject pObject, String pCallbackName)
	{
		if (aOnCallbackFunc != null)
			aOnCallbackFunc(pObject, pCallbackName);
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 클릭
	public void GoClick(String pUIName)
	{
		if (!mDicLuaUi.ContainsKey(pUIName))
		{
			Debug.LogError("Not Exist UI Name - " + pUIName);
			return;
		}
		GameObject lObject = mDicLuaUi[pUIName];
		if (lObject != null)
			lObject.SendMessage("OnClick");
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 등록
	public void RegisteLuaUI(GameObject pObject)
	{
		if(	mDicLuaUi == null)
			mDicLuaUi = new Dictionary<String, GameObject>();

 		// Debug.Log("Registe in RegisteLuaUI - " + pObject.name);
		if (!mDicLuaUi.ContainsKey(pObject.name))
		{
			mDicLuaUi.Add(pObject.name, pObject);
		}
		else
		{
			Debug.LogError("Do Not Registe UI name - " + pObject.name);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<String, GameObject> mDicLuaUi;
}