﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleScriptController : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public static readonly String[] sSeparatorLines = { "\n", "\r\n", "\0", "\t" };
	public static readonly String[] sSeparatorFunc = { "(", ")", ",", ":"};

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		mExecuteTrigger = new Dictionary<String, List<String>>();
		mBattleScriptInfo = new BattleScriptInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// TextAsset을 로드합니다.
	public void Load()
	{
		String[] lBattleScriptAll = mBattleScript.text.Split(sSeparatorLines, StringSplitOptions.RemoveEmptyEntries);
		bool lIsTriggerExecute = false;
		String lTriggerString = String.Empty;
		List<String> lTempScript = new List<String>();
		for (int iScript = 0; iScript < lBattleScriptAll.Length; iScript++)
		{
			if (lBattleScriptAll[iScript].Equals("{"))
			{
				lTriggerString = lTempScript[iScript - 1];
				lIsTriggerExecute = true;
				if (!mExecuteTrigger.ContainsKey(lTriggerString))
				{
					mExecuteTrigger.Add(lTriggerString, new List<String>());
				}
			}
			else if (lBattleScriptAll[iScript].Equals("}"))
			{
				lIsTriggerExecute = false;
			}

			else if (lIsTriggerExecute)
			{
				mExecuteTrigger[lTriggerString].Add(lBattleScriptAll[iScript]);
			}
			lTempScript.Add(lBattleScriptAll[iScript]);
		}

		_TranslateTrigger();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 트리거를 해석합니다.
	private void _TranslateTrigger()
	{
		Dictionary<String, List<String>>.Enumerator lIterTrigger = mExecuteTrigger.GetEnumerator();
		while (lIterTrigger.MoveNext())
		{
			String[] lFuncParam = lIterTrigger.Current.Key.Split(sSeparatorFunc, StringSplitOptions.RemoveEmptyEntries);
			switch (lFuncParam[0])
			{
				case "Start" :
					_ExecuteFunc(lIterTrigger.Current.Value);
					break;
				case "ConquestCheckPoint" :
					int lCheckPointGuid;
					int lCaptureTeamId;
					int lTriggerCount;
					int.TryParse(lFuncParam[1], out lCheckPointGuid);
					int.TryParse(lFuncParam[2], out lCaptureTeamId);
					int.TryParse(lFuncParam[3], out lTriggerCount);
					BattleCheckPoint lCheckPoint = BattlePlay.main.aBattleGrid.FindCheckPointByGuid((uint)lCheckPointGuid);
					mBattleScriptInfo.AddConquestCheckPoint(lCheckPoint, lCaptureTeamId, lTriggerCount);
					break;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제 트리거를 실행합니다.
	private void _ExecuteFunc(List<String> pExecuteFuncList)
	{
		for (int iFunc = 0; iFunc < pExecuteFuncList.Count; iFunc++)
		{
			Debug.Log("Execute - " + pExecuteFuncList[iFunc]);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	public TextAsset mBattleScript;
	private List<String> mExecuteScript;
	private Dictionary<String, List<String>> mExecuteTrigger;
	private BattleScriptInfo mBattleScriptInfo;
}
