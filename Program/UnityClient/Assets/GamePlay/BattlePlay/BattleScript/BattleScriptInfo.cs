﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

//-------------------------------------------------------------------------------------------------------
// 스크립트에 필요한 정보는 이 클래스를 통해 접근합니다.
public class BattleScriptInfo
{
	public List<BattleScriptConquestCheckPoint> mBattleScriptConquestCheckPoint;
	
	public BattleScriptInfo()
	{
		mBattleScriptConquestCheckPoint = new List<BattleScriptConquestCheckPoint>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 점령 완료 트리거를 추가합니다.
	public void AddConquestCheckPoint(BattleCheckPoint pCheckPoint, int pCaptureTeamIdx, int pTriggerCount)
	{
		BattleScriptConquestCheckPoint lConquestCheckPoint = new BattleScriptConquestCheckPoint();
		lConquestCheckPoint.SetCheckPoint(pCheckPoint, pCaptureTeamIdx, pTriggerCount);
		mBattleScriptConquestCheckPoint.Add(lConquestCheckPoint);
	}
}
//-------------------------------------------------------------------------------------------------------
// 점령 트리거 데이터
public class BattleScriptConquestCheckPoint
{
	public int mScriptCount;
	public int mConquestTeamIdx;
	public BattleCheckPoint mCheckPoint;

	public BattleScriptConquestCheckPoint()
	{
		mConquestTeamIdx = -1;
		mScriptCount = -1;
		mCheckPoint = null;
	}
	public void SetCheckPoint(BattleCheckPoint pCheckPoint, int pCaptureTeamIdx, int pTriggerCount)
	{
		mCheckPoint = pCheckPoint;
		mConquestTeamIdx = pCaptureTeamIdx;
		mScriptCount = pTriggerCount;
		mCheckPoint.aOnTeamChangeDelegate += _OnTeamChange;
	}
	private void _OnTeamChange(BattleCheckPoint pCheckPoint)
	{
		if (pCheckPoint.aTeamIdx == mConquestTeamIdx)
		{
			mScriptCount--;
		}
		if (mScriptCount <= 0)
			mCheckPoint.aOnTeamChangeDelegate -= _OnTeamChange;
	}
}