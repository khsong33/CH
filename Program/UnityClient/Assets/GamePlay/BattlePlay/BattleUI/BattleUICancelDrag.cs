﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUICancelDrag : MonoBehaviour
{
	public delegate void OnCancelAreaEnter();
	public delegate void OnCancelAreaExit();

	public OnCancelAreaEnter aOnCancelAreaEnter { get; set; }
	public OnCancelAreaExit aOnCancelAreaExit { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public String[] vCancelObjectNames = new String[] {"Commander Drag Object", "Drag Object"};

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	private void OnTriggerEnter(Collider pOther)
	{
		for (int iCancelObject = 0; iCancelObject < vCancelObjectNames.Length; iCancelObject++)
		{
			if (pOther.name.Equals(vCancelObjectNames[iCancelObject]))
			{
				if(aOnCancelAreaEnter != null)
					aOnCancelAreaEnter();
				return;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	private void OnTriggerExit(Collider pOther)
	{
		for (int iCancelObject = 0; iCancelObject < vCancelObjectNames.Length; iCancelObject++)
		{
			if (pOther.name.Equals(vCancelObjectNames[iCancelObject]))
			{
				if (aOnCancelAreaExit != null)
					aOnCancelAreaExit();
				return;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsFirst;
}
