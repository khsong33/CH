using UnityEngine;
using System.Collections;
using System;

public class BattleUICommanderCountGauge : MonoBehaviour
{
	public enum ShowInfo
	{
		MedelCount,
		LiveCommanderCount,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public ShowInfo vShowInfo = ShowInfo.MedelCount;
	public UISlider vCountSlider;
	public UILabel vCountLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vCountSlider == null)
			Debug.LogError("vCountSlider is empty - " + this);
		if (vCountLabel == null)
			Debug.LogError("vCountLabel is empty - " + this);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 개수를 갱신합니다.
	public void UpdateCount(int pTeamIdx)
	{
		int lGaugeValue;
		int lGaugeLength;
		switch (vShowInfo)
		{
		case ShowInfo.MedelCount:
			{
				lGaugeValue = BattlePlay.main.aProgressInfo.aTeamMedalCounts[pTeamIdx];
				lGaugeLength = BattlePlay.main.aProgressInfo.aTeamMaxCommanderCounts[pTeamIdx];
			}
			break;
		case ShowInfo.LiveCommanderCount:
		default:
			{
				lGaugeValue = BattlePlay.main.aProgressInfo.aTeamLiveCommanderCounts[pTeamIdx];
				lGaugeLength = BattlePlay.main.aProgressInfo.aTeamMaxCommanderCounts[pTeamIdx];
			}
			break;
		}

		vCountSlider.value = (float)lGaugeValue / lGaugeLength;
		vCountLabel.text = lGaugeValue.ToString();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
