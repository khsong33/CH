using UnityEngine;
using System.Collections;
using System;

public class BattleUICommanderInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIconTexture;
	public UISprite vRankSprite;
	public UISprite[] vRankIconSprites;
	public UILabel vCommanderNameLabel;
	//public BattleUITroopMark[] vTroopMarks;

	public GameObject[] vLinkObjects;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 표시 지휘관
	public BattleCommander aDisplayCommander
	{
		get { return mDisplayCommander; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vRankSprite == null)
			Debug.LogError("vRankSprite is empty - " + this);
		if (vRankIconSprites.Length != (CommanderSpec.cRankCount - 1))
			Debug.LogError("vRankIconSprites.Length mismatch - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		//if (vTroopMarks.Length != CommanderTroopSet.cMaxTroopCount)
		//	Debug.LogError("vTroopMarks.Length mismatch - " + this);
		//for (int iMark = 0; iMark < CommanderTroopSet.cMaxTroopCount; ++iMark)
		//	if (vTroopMarks[iMark] == null)
		//		Debug.LogError("vTroopMarks is empty - " + this);

		mDefaultCommanderIconTexture = vCommanderIconTexture.mainTexture;

		mIsHide = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 표시할 지휘관을 지정합니다.
	public void SetDisplayCommander(BattleCommander pDisplayCommander)
	{
		if (pDisplayCommander == null)
		{
			if (!mIsHide)
				_Hide(true);
			return;
		}
		else
		{
			if (mIsHide)
				_Hide(false);
		}

		mDisplayCommander = pDisplayCommander;
		//mTroopInfos = mDisplayCommander.aCommanderItem.mTroopSet.mTroopInfos;

		vCommanderIconTexture.mainTexture = mDisplayCommander.aCommanderSpec.GetIconTexture();

//		vRankSprite.color = BattleConfig.get.mRankColors[mDisplayCommander.aCommanderItem.mCommanderRank];

		int lRankSpriteIndex = mDisplayCommander.aCommanderItem.mCommanderExpRank - 1;
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			vRankIconSprites[iSprite].gameObject.SetActive(iSprite == lRankSpriteIndex);

		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mDisplayCommander.aCommanderSpec.mNameKey);

		//for (int iMark = 0; iMark < vTroopMarks.Length; ++iMark)
		//{
		//	vTroopMarks[iMark].gameObject.SetActive(mTroopInfos[iMark] != null);
		//	if (mTroopInfos[iMark] != null)
		//		vTroopMarks[iMark].SetDisplayTroop(mDisplayCommander, mTroopInfos[iMark].mTroopSpec);
		//}
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public void UpdateFrame()
	{
		if (mDisplayCommander == null)
			return;

		//for (int iMark = 0; iMark < vTroopMarks.Length; ++iMark)
		//{
		//	if (mTroopInfos[iMark] != null)
		//	{
		//		BattleTroop lTroop = mDisplayCommander.aTroops[iMark];
		//		if (lTroop != null)
		//			vTroopMarks[iMark].UpdateHp(lTroop.aStat.aCurMilliHp, lTroop.aStat.aMaxMilliHp);
		//		else
		//			vTroopMarks[iMark].UpdateHp(0, 1);
		//	}
		//}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 숨김 여부를 지정합니다.
	private void _Hide(bool pIsHide)
	{
		mIsHide = pIsHide;

		if (pIsHide)
			vCommanderIconTexture.mainTexture = mDefaultCommanderIconTexture;

		//for (int iObject = 0; iObject < vLinkObjects.Length; ++iObject)
		//	vLinkObjects[iObject].SetActive(!mIsHide);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsHide;
	private BattleCommander mDisplayCommander;
	private Texture mDefaultCommanderIconTexture;
	//private CommanderTroopInfo[] mTroopInfos;
}
