using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUIControlPanel : MonoBehaviour
{
	internal delegate void OnApproachFocusingPosition();

	internal const float cFocusApproachFactor = 0.1f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vControlTeamCount = 3;

	public BattleUIUserControl vClientUserControl;
	public BattleUIUserControl vDebugUserControl;

	public UISlider[] vVpSliders;
	public BattleUITeamHpGauge[] vTeamHpGauges;
	public BattleUICommanderCountGauge[] vCommanderCountGauges;
	public UISprite[] vCheckPointStars;

	public QuaterViewCameraSet vQuaterViewCameraSet;
	public StageTouchControl vStageTouchControl;
	public UISlider vZoomSlider;
	public BattleUIMiniMap vMiniMap;

	public UILabel vPlayTimerLabel;
	public CountdownLabel vCountdownLabel;
	public PromptLabel vPromptLabel;

	public GameObject vVpWarningObject;

	public float vCountdownStartDelay = 1.0f;
	public float vStartMessageDelay = 2.0f;
	public AnimationCurve vDefaultFocusingCurve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 1f));
	public float vDefaultFocusingDelay = 0.5f;
	public AnimationCurve vZoomCurve = new AnimationCurve(new Keyframe(0.0f, 0.0f), new Keyframe(1.0f, 1f));
	public float vZoomDelay = 0.5f;
	public float vReadyToPlayDelay = 2.0f;
	public bool vIsFocusingLock = false;
	public bool vIsSelectionFocusing = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 드래깅 여부
	public bool aIsDraggingStage
	{
		get { return mIsDraggingStage; }
		set { mIsDraggingStage = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 바라보는 지휘관
	public BattleCommander aFocusingCommander
	{
		get { return mFocusingCommanderLink.Get(); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커싱 상태 여부
	public bool aIsApproachFocusing
	{
		get { return mIsApproachFocusing; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 기본값
	public float aDefaultZoomSliderValue
	{
		get { return mDefaultZoomSliderValue; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 점근 타이머(0이면 점근 없음)
	public float aZoomApproachTimer
	{
		get { return mZoomApproachTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 조작하는 팀 인덱스
	public int aControlTeamIdx
	{
		get { return mControlTeamIdx; }
		set
		{
			if (mControlTeamIdx == value)
				return;

			_SetControlTeam(value);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vClientUserControl == null)
			Debug.LogError("vClientUserControl is empty - " + this);
		if (vDebugUserControl == null)
			Debug.LogError("vDebugUserControl is empty - " + this);

// 		if (vVpSliders.Length != vControlTeamCount)
// 			Debug.LogError("vVpSliders.Length mismatch - " + this);
// 		for (int iSlider = 1; iSlider < vVpSliders.Length; ++iSlider)
// 			if (vVpSliders[iSlider] == null)
// 				Debug.LogError("vVpSliders[" + iSlider + " ] is empty - " + this);
		if (vTeamHpGauges.Length != vControlTeamCount)
			Debug.LogError("vTeamHpGauges.Length mismatch - " + this);
		for (int iGauge = 1; iGauge < vTeamHpGauges.Length; ++iGauge)
			if (vTeamHpGauges[iGauge] == null)
				Debug.LogError("vTeamHpGauges[" + iGauge + " ] is empty - " + this);
		if (vCommanderCountGauges.Length != vControlTeamCount)
			Debug.LogError("vCommanderCountGauges.Length mismatch - " + this);
		for (int iGauge = 1; iGauge < vCommanderCountGauges.Length; ++iGauge)
			if (vCommanderCountGauges[iGauge] == null)
				Debug.LogError("vCommanderCountGauges[" + iGauge + " ] is empty - " + this);
		if (vCheckPointStars.Length != BattleMatchInfo.cMaxCheckPointStarCount)
			Debug.LogError("vCheckPointStars.Length mismatch - " + this);
		for (int iStar = 0; iStar < vCheckPointStars.Length; ++iStar)
			if (vCheckPointStars[iStar] == null)
				Debug.LogError("vCheckPointStars[" + iStar + " ] is empty - " + this);

		if (vQuaterViewCameraSet == null)
			Debug.LogError("vQuaterViewCameraSet is empty - " + this);
		if (vStageTouchControl == null)
			Debug.LogError("vStageTouchControl is empty - " + this);
		if (vZoomSlider == null)
			Debug.LogError("vZoomSlider is empty - " + this);
		if (vMiniMap == null)
			Debug.LogError("vMiniMap is empty - " + this);

		if (vPlayTimerLabel == null)
			Debug.LogError("vPlayTimerLabel is empty - " + this);
		if (vCountdownLabel == null)
			Debug.LogError("vCountdownLabel is empty - " + this);
		if (vPromptLabel == null)
			Debug.LogError("vPromptLabel is empty - " + this);

// 		if (vVpWarningObject == null)
// 			Debug.LogError("vVpWarningObject is empty - " + this);

		mUIPanel = GetComponent<UIPanel>();
		if (mUIPanel == null)
			Debug.LogError("can't find UIPanel - " + this);

		mFocusingCommanderLink = new GuidLink<BattleCommander>();
		mApproachFocusingCommanderLink = new GuidLink<BattleCommander>();

		mControlTeamIdx = -1;

		vStageTouchControl.aIsCommandable = false; // 처음에는 지휘 입력을 받지 않습니다.

	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		_InitZoom();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		// 줌 슬라이더 상태를 반영합니다.
		if (vZoomSlider.gameObject.activeInHierarchy)
			vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance = Mathf.Lerp(
				vQuaterViewCameraSet.vPerspectiveZoomInfo.vCloseViewDistance,
				vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewDistance,
				vZoomSlider.value);

		// 포커싱 애니메이션을 처리합니다.
		if (!mIsDraggingStage &&
			(mOnApproachFocusingPosition != null)) // mIsApproachFocusing이 false이더라도 mOnApproachFocusingPosition는 진행중일 수 있음
			mOnApproachFocusingPosition();

		// 줌 애니메이션을 처리합니다.
		if (mZoomApproachTimer > 0.0f)
			_ApproachZoom();

		// VP 변화를 처리합니다.
// 		if (BattlePlay.main.aClientUser != null)
// 		{
// 			bool lIsVpDecreased = (BattlePlay.main.aProgressInfo.aTeamMilliVps[BattlePlay.main.aClientUser.aTeamIdx] < BattlePlay.main.aProgressInfo.aLastTeamMilliVps[BattlePlay.main.aClientUser.aTeamIdx]);
// 			if (vVpWarningObject.gameObject.activeInHierarchy != lIsVpDecreased)
// 				vVpWarningObject.gameObject.SetActive(lIsVpDecreased);
// 		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 초기화합니다.
	public void SetUpControl(BattleUser pClientUser)
	{
		mClientUser = pClientUser;
		mDebugUser = null;

		mIsDraggingStage = false;
		mFocusingCommanderLink.Set(null);
		mIsApproachFocusing = true; // 기본은 포커싱을 유지하려고 함
		mApproachFocusingCommanderLink.Set(null);

		mControlTeamIdx = mClientUser.aTeamIdx;
		vClientUserControl.SetControlUser(mClientUser);


		_SetUpViewArea();

		vMiniMap.LoadMap();

		_UpdatePlayTimer();

		// 이벤트 콜백을 등록합니다.
// 		BattlePlay.main.aProgressInfo.aOnVpChangeDelegate += _OnVpChange;
		BattlePlay.main.aProgressInfo.aOnMpBuffStartDelegate += _OnMpBuffStart;

		List<BattleCheckPoint> lCheckPointList = BattlePlay.main.aBattleGrid.aCheckPointList;
		for (int iPoint = 0; iPoint < lCheckPointList.Count; ++iPoint)
		{
			BattleCheckPoint lCheckPoint = lCheckPointList[iPoint];

			if (lCheckPoint.aStarId > 0)
			{
				int lCheckPointStarIndex = lCheckPoint.aStarId - 1;
				vCheckPointStars[lCheckPointStarIndex].color = BattlePlay.main.vTeamColors[lCheckPoint.aTeamIdx];
			}
			lCheckPointList[iPoint].aOnTryTeamChangeDelegate += _OnCheckPointTryTeamChange;
			lCheckPointList[iPoint].aOnTeamChangeDelegate += _OnCheckPointTeamChange;
		}

		vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;

		vClientUserControl.EnableSpawn(false); // 카운트 다운이 끝나기 전까지는 스폰 불가

		Coord2 lStartCameraCoord
			= (vClientUserControl.aControlUser.aHeadquarter.aLinkCheckPoint.aCoord + vClientUserControl.aControlUser.aHeadquarter.aCoord)
			/ 2;
		vQuaterViewCameraSet.vViewPlaneCenter = Coord2.ToWorld(lStartCameraCoord);

		for (int iTeam = 1; iTeam < vTeamHpGauges.Length; ++iTeam)
		{
			vTeamHpGauges[iTeam].StartUpdate(iTeam);
			vCommanderCountGauges[iTeam].UpdateCount(iTeam);
		}

		BattlePlay.main.aBattleGrid.aOnSpawnCommanderDelegate += _OnGridSpawnCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤을 해제합니다.
	public void ResetControl()
	{
		// 이벤트 콜백 등록을 해제합니다.
// 		BattlePlay.main.aProgressInfo.aOnVpChangeDelegate -= _OnVpChange;

		List<BattleCheckPoint> lCheckPointList = BattlePlay.main.aBattleGrid.aCheckPointList;
		for (int iPoint = 0; iPoint < lCheckPointList.Count; ++iPoint)
		{
			lCheckPointList[iPoint].aOnTryTeamChangeDelegate -= _OnCheckPointTryTeamChange;
			lCheckPointList[iPoint].aOnTeamChangeDelegate -= _OnCheckPointTeamChange;
		}

		vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 컨트롤을 시작합니다.
	public IEnumerator StartControl()
	{
		FocusCommander(mClientUser.aCommanders[0], true, vDefaultFocusingCurve, vDefaultFocusingDelay);
		FocusCommander(mClientUser.aCommanders[0], true, vDefaultFocusingCurve, vDefaultFocusingDelay); // 처음에는 부대가 없을 수 있으므로 점근 애니를 하도록 두 번 호출한 효과를 줌
		vClientUserControl.UpdateButtonSelection();

		yield return new WaitForSeconds(vReadyToPlayDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public void UpdateFrame()
	{
		vClientUserControl.UpdateFrame();

		_UpdatePlayTimer();

		_UpdateFocusingCommander();

		if (mDebugUser != null)
			vDebugUserControl.UpdateFrame();
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 카운트다운을 보여줍니다.
	public IEnumerator ShowCountdown(float pCountdownSec)
	{
		yield return new WaitForSeconds(vCountdownStartDelay);

		vCountdownLabel.gameObject.SetActive(true);
		vCountdownLabel.StartCountdown(pCountdownSec);
		yield return new WaitForSeconds(pCountdownSec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스폰 시작을 처리합니다.
	public void StartSpawn()
	{
		vStageTouchControl.aIsCommandable = true; // 이후로 지휘 입력을 받습니다.

		BattleCommander lClientUserFirstCommander = vClientUserControl.aControlUser.aCommanders[0];
		if (lClientUserFirstCommander != null)
			FocusCommander(lClientUserFirstCommander, true, vDefaultFocusingCurve, vDefaultFocusingDelay); // 첫 지휘관에 카메라 포커싱을 다시 주어서 이동시킵니다.

		vClientUserControl.EnableSpawn(true); // 이후로 스폰 가능

		PromptMessage("Start", vStartMessageDelay); // 시작 메시지를 잠시 납깁니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 첫 지휘관을 스폰합니다.
	public void SpawnFirstCommander()
	{
		BattleUser lSpawnUser = vClientUserControl.aControlUser;

		BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommander(
			lSpawnUser,
			lSpawnUser.aUserInfo.mCommanderItems[lSpawnUser.aUserInfo.mFirstCommanderItemIdx],
			lSpawnUser.aHeadquarter.aCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메시지를 보입니다.
	public void PromptMessage(String pPromptMessage, float pPromptTime)
	{
		vPromptLabel.gameObject.SetActive(true);
		vPromptLabel.PromptMessage(this, pPromptMessage, pPromptTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임을 종료합니다.
	public void ExitGame()
	{
		Application.Quit();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 컨트롤을 지정합니다.
	public void SetCommanderControl(BattleCommander pCommander)
	{
		pCommander.aOnDestroyingDelegate += _OnControlCommanderDestroying;
		pCommander.aOnDestroyedDelegate += _OnControlCommanderDestroyed;
		pCommander.aOnDestroyedTroopDelegate += _OnControlCommanderDestroyedTroop;
		pCommander.aOnRecruitTroopsDelegate += _OnControlCommanderRecruitTroop;
		pCommander.aOnVeterancyRankUpDelegate += _OnControlCommanderVeteranRankUp;
		pCommander.aOnVeterancyExpDelegate += _OnControlCommanderVeteranExp;

		if (vClientUserControl.aControlUser == pCommander.aUser)
		{
			vClientUserControl.SetCommanderButton(pCommander.aCommanderSlotIdx, pCommander);
			if (pCommander == null)
				vClientUserControl.SetCommanderItemButton(pCommander.aCommanderSlotIdx, vClientUserControl.aControlUser.aUserInfo.mCommanderItems[pCommander.aCommanderSlotIdx]);
			//vClientUserControl.SetCommanderItemButton(pCommander.aCommanderItem.mSlotIdx, pCommander.aCommanderItem);

			if(!vClientUserControl.aControlUser.IsSpawnAvailable())
				vClientUserControl.ShowSpawnCommanderPanel(false);
		}
		if (vDebugUserControl.aControlUser == pCommander.aUser)
		{
			vDebugUserControl.SetCommanderButton(pCommander.aCommanderSlotIdx, pCommander);
			//vDebugUserControl.SetCommanderItemButton(pCommander.aCommanderItem.mSlotIdx, pCommander.aCommanderItem);
			if (pCommander == null)
				vDebugUserControl.SetCommanderItemButton(pCommander.aCommanderSlotIdx, vDebugUserControl.aControlUser.aUserInfo.mCommanderItems[pCommander.aCommanderSlotIdx]);
		}

		_UpdateUserCommanderPopCount(pCommander.aUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지점을 포커싱합니다.
	public void FocusCoord(Coord2 pFocusingCoord)
	{
		FocusCoord(pFocusingCoord, null, 0.0f);
	}

	//-------------------------------------------------------------------------------------------------------
	// 해당 지점을 포커싱합니다.
	public void FocusCoord(Coord2 pFocusingCoord, AnimationCurve pFocusingCurve, float pFocusingDelay)
	{
		_EnableApproachFocusingCoord(pFocusingCoord, pFocusingCurve, pFocusingDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관을 포커싱합니다.
	public void FocusCommander(BattleCommander pNewFocusingCommander, bool pIsSelection)
	{
		FocusCommander(pNewFocusingCommander, pIsSelection, null, 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관을 포커싱합니다.
	public void FocusCommander(BattleCommander pNewFocusingCommander, bool pIsSelection, AnimationCurve pFocusingCurve, float pFocusingDelay)
	{
		BattleCommander lCurrentFocusingCommander = mFocusingCommanderLink.Get();
		if (lCurrentFocusingCommander == pNewFocusingCommander)
		{
			if (lCurrentFocusingCommander == null)
				return;

			if (pIsSelection)
			{
				if (vClientUserControl.aControlUser == pNewFocusingCommander.aUser)
					vClientUserControl.SelectCommander(pNewFocusingCommander);
				if (vDebugUserControl.aControlUser == pNewFocusingCommander.aUser)
					vDebugUserControl.SelectCommander(pNewFocusingCommander);

				lCurrentFocusingCommander.aStageCommander.SetSelected(true); // 선택 상태를 확인
			}

// 			if (lCurrentFocusingCommander.aIsAlive &&
// 				(mOnApproachFocusingPosition != null))
// 				_EnableZoom(pFocusingCurve != null, mDefaultZoomSliderValue); // 반복 선택이라면 기본 줌을 회복(줌 애니는 애니 사용 여부에 따라)

			_EnableApproachFocusingCommander(lCurrentFocusingCommander, pFocusingCurve, vDefaultFocusingDelay);

			return;
		}

		mFocusingCommanderLink.Set(pNewFocusingCommander);

		if (pIsSelection)
		{
			mOnApproachFocusingPosition = null;

			if (lCurrentFocusingCommander != null)
				lCurrentFocusingCommander.aStageCommander.SetSelected(false); // 기존 지휘 부대의 선택 상태를 해제합니다.

			if (pNewFocusingCommander == null)
			{
				vClientUserControl.SelectCommander(null);
				vDebugUserControl.SelectCommander(null);

				aControlTeamIdx = -1;
				return;
			}

			pNewFocusingCommander.aStageCommander.SetSelected(true); // 새 지휘 부대를 선택 상태로 합니다.
			aControlTeamIdx = pNewFocusingCommander.aTeamIdx;

			if (pNewFocusingCommander.aUser == vClientUserControl.aControlUser)
			{
				vClientUserControl.SelectCommander(pNewFocusingCommander);
				vDebugUserControl.SelectCommander(null);
			}
			else if (pNewFocusingCommander.aUser == mDebugUser)
			{
				vClientUserControl.SelectCommander(null);
				vDebugUserControl.SelectCommander(pNewFocusingCommander);
			}

			if (vIsSelectionFocusing)
				_EnableApproachFocusingCommander(pNewFocusingCommander, pFocusingCurve, vDefaultFocusingDelay);
		}
		else
		{
			_EnableZoom(pFocusingCurve != null, mDefaultZoomSliderValue);
			_EnableApproachFocusingCommander(pNewFocusingCommander, pFocusingCurve, vDefaultFocusingDelay);
		}
		
// 		if (mIsApproachFocusingPosition) // 포커싱 중이었다면
// 			_EnableApproachFocusingPosition(pIsAnimatingFocus); // 지휘관이 바뀌어도 포커싱을 유지합니다.
//		ResetFocusingApproach();
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커싱 접근을 해제합니다.
	public void ResetFocusingApproach()
	{
		if (!vIsFocusingLock)
			mIsApproachFocusing = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 점근을 시작합니다.
	public void SlideZoom(float pZoomSliderValue)
	{
		_EnableZoom(true, pZoomSliderValue);
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 컨트롤 유저를 지정합니다.
	public void SetDebugControlUser(int pUserIdx)
	{
		//Debug.Log("SetDebugControlUser() pUserIdx=" + pUserIdx);

		mDebugUser = (pUserIdx >= 0) ? BattlePlay.main.aUsers[pUserIdx] : null;
		vDebugUserControl.SetControlUser(mDebugUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 UI에 표시될 알람을 설정합니다.
	public void ShowCommanderState(BattleCommander pCommander, StageCommanderState pCommanderState)
	{
		if (mClientUser == null)
			return;
		if (mClientUser.aTeamIdx != pCommander.aTeamIdx)
			return;
		if (pCommander.aUser == null)
			return;

		BattleUICommanderButton lCommanderButton = vClientUserControl.vCommanderButtons[pCommander.aCommanderSlotIdx];
		switch (pCommanderState)
		{
			case StageCommanderState.Move:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Moveing, true, false);
				break;
			case StageCommanderState.MoveStop:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Moveing, false, false);
				break;
			case StageCommanderState.Attack:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Battle, true, false);
				break;
			case StageCommanderState.AttackStop:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Battle, false, false);
				break;
			case StageCommanderState.AttackComplete:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Battle, false, false);
				break;
			case StageCommanderState.Hit:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Battle, true, true);
				break;
			case StageCommanderState.ConquerCheckPoint:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Conquer, true, false);
				break;
			case StageCommanderState.ConquerStop:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Conquer, false, false);
				break;
			case StageCommanderState.ConquerComplete:
				lCommanderButton.SetStateAlarm(StageCommander.CommanderAlarm.Conquer, false, false);
				break;
			//case StageCommanderState.None:
			//	lCommanderButton.ClearAlarm();
			//	break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 플레이 타이머를 갱신합니다.
	private void _UpdatePlayTimer()
	{
		int lMinutes = BattlePlay.main.aProgressInfo.aPlayTimer / 60000;
		int lSeconds = BattlePlay.main.aProgressInfo.aPlayTimer / 1000 - lMinutes * 60;
		//Debug.Log("_UpdateLabel() lMinutes=" + lMinutes + " lSeconds=" + lSeconds + " ==>" + String.Format("{0}:{1:00}", lMinutes, lSeconds));
		vPlayTimerLabel.text = String.Format("{0}:{1:00}", lMinutes, lSeconds);
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커싱된 지휘관에 대한 처리를 진행합니다.
	private void _UpdateFocusingCommander()
	{
		BattleCommander lFocusingCommander = mFocusingCommanderLink.Get();

		List<BattleCheckPoint> lCheckPointList = mClientUser.aBattleGrid.aCheckPointList;
		for (int iCheckPoint = 0; iCheckPoint < lCheckPointList.Count; iCheckPoint++)
			lCheckPointList[iCheckPoint].aStageCheckPoint.UpdateCommanderCaptureState(lFocusingCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCheckPoint.OnTryTeamChangeDelegate
	private void _OnCheckPointTryTeamChange(BattleCommander pTryCommander, BattleCheckPoint pCheckPoint, bool pIsTrying)
	{
		//Debug.Log("_OnCheckPointTeamChange() pCheckPoint=" + pCheckPoint + " pIsTrying=" + pIsTrying);

		if (pIsTrying)
		{
			vMiniMap.StartCapture(pCheckPoint);
			if (pTryCommander != null)
				pTryCommander.aStageCommander.ShowState(StageCommanderState.ConquerCheckPoint);
		}
		else
		{
			vMiniMap.StopCapture(pCheckPoint);
			if (pTryCommander != null)
				pTryCommander.aStageCommander.ShowState(StageCommanderState.ConquerComplete);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCheckPoint.OnTeamChangeDelegate
	private void _OnCheckPointTeamChange(BattleCheckPoint pCheckPoint)
	{
		//Debug.Log("_OnCheckPointTeamChange() pCheckPoint=" + pCheckPoint);

		vMiniMap.UpdateCheckPointTeamChange(pCheckPoint);

		String lMessagePrefix;
		if (pCheckPoint.aTeamIdx == mClientUser.aTeamIdx)
			lMessagePrefix = "[Ally] ";
		else if (pCheckPoint.aTeamIdx > 0)
			lMessagePrefix = "[Enemy] ";
		else
			lMessagePrefix = String.Empty;

		if (pCheckPoint.aLinkHeadquarter != null)
		{
			if (pCheckPoint.aTeamIdx > 0)
			{
				String lCheckPointCaptureString = lMessagePrefix + "Headquarter Captured";
				PromptMessage(LocalizedTable.get.GetLocalizedText(lCheckPointCaptureString), 3.0f);
			}
			else
			{
				String lCheckPointCaptureString = lMessagePrefix + "Headquarter Freed";
				PromptMessage(LocalizedTable.get.GetLocalizedText(lCheckPointCaptureString), 3.0f);
			}
		}
		//else if (pCheckPoint.aStarId > 0)
		//{
		//	int lCheckPointStarIndex = pCheckPoint.aStarId - 1;
		//	vCheckPointStars[lCheckPointStarIndex].color = BattlePlay.main.vTeamColors[pCheckPoint.aTeamIdx];

		//	if (pCheckPoint.aTeamIdx > 0)
		//		PromptMessage(lMessagePrefix + "Victory Point Captured", 3.0f);
		//	else
		//		PromptMessage(lMessagePrefix + "Victory Point Freed", 3.0f);
		//}
		else if (pCheckPoint.aCheckPointType == BattleCheckPoint.CheckPointType.Recruit)
		{
			if (pCheckPoint.aTeamIdx > 0)
			{
				String lCheckPointCaptureString = lMessagePrefix + "MP Point Captured";
				PromptMessage(LocalizedTable.get.GetLocalizedText(lCheckPointCaptureString), 3.0f);
			}
			else
			{
				String lCheckPointCaptureString = lMessagePrefix + "MP Point Freed";
				PromptMessage(LocalizedTable.get.GetLocalizedText(lCheckPointCaptureString), 3.0f);
			}
		}
		_UpdateUserCommanderPopCount(mClientUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleProgressInfo.OnVpChangeDelegate
// 	private void _OnVpChange(int pTeamIdx, int pCurrentVp, int pMaxVp)
// 	{
// 		vVpSliders[pTeamIdx].value = (float)pCurrentVp / pMaxVp;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// BattleProgressInfo.OnMpBuffStartDelegate
	private void _OnMpBuffStart(int pMpBuffMilliRatio)
	{
		String lPromptString = "MP buff time starts";
		PromptMessage(LocalizedTable.get.GetLocalizedText(lPromptString), 3.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 관련 초기화를 합니다.
	private void _InitZoom()
	{
		_OnZoomChanged();
		mDefaultZoomSliderValue = vZoomSlider.value;
		mZoomApproachTimer = 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// QuaterViewCameraSet.OnZoomChangedDelegate
	private void _OnZoomChanged()
	{
		QuaterViewCameraSet.PerspectiveZoomInfo lPerspectiveZoomInfo = vQuaterViewCameraSet.vPerspectiveZoomInfo;
		float lZoomInterval = lPerspectiveZoomInfo.vFarViewDistance - lPerspectiveZoomInfo.vCloseViewDistance;
		float lZoomValue = lPerspectiveZoomInfo.vViewDistance - lPerspectiveZoomInfo.vCloseViewDistance;
		vZoomSlider.value = lZoomValue / lZoomInterval;
	}
	//-------------------------------------------------------------------------------------------------------
	// 뷰 영역을 초기화합니다.
	private void _SetUpViewArea()
	{
		Coord2 lBattleGridSize = new Coord2(
			BattlePlay.main.aBattleGrid.aMaxCoordX - BattlePlay.main.aBattleGrid.aMinCoordX,
			BattlePlay.main.aBattleGrid.aMaxCoordZ - BattlePlay.main.aBattleGrid.aMinCoordZ);
		Rect lPlayArea = new Rect(0, 0, lBattleGridSize.x * Coord2.cToWorld, lBattleGridSize.z * Coord2.cToWorld);
		vQuaterViewCameraSet.vPerspectiveZoomInfo.vCloseViewArea = lPlayArea;
		vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewArea = lPlayArea;
		vQuaterViewCameraSet.vViewPlaneCenter = new Vector3(lPlayArea.width * 0.5f, 0.0f, lPlayArea.height * 0.5f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커싱 지휘관으로 점근을 활성화합니다.
	private void _EnableApproachFocusingCommander(BattleCommander pApproachFocusingCommander, AnimationCurve pFocusingCurve, float pFocusingDelay)
	{
		//Debug.Log("_EnableApproachFocusingPosition() pIsAnimatingFocus=" + pIsAnimatingFocus);

		mIsApproachFocusing = true;
		mApproachFocusingCommanderLink.Set(pApproachFocusingCommander);
		mApproachFocusingStartPosition = vQuaterViewCameraSet.aViewCenter;
		mOnApproachFocusingPosition = _OnApproachFocusingCommander;
		mApproachFocusingCurve = pFocusingCurve;
		mApproachFocusingDelay = pFocusingDelay;
		mApproachFocusingTimer = (pFocusingCurve != null) ? pFocusingDelay : 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnApproachFocusingPosition
	private void _OnApproachFocusingCommander()
	{
		if (!mIsApproachFocusing)
		{
			mOnApproachFocusingPosition = null; // 포커싱 하던 지휘관 사라져서 접근 포커싱 중단
			return;
		}

		BattleCommander lApproachFocusingCommander = mApproachFocusingCommanderLink.Get();
		if (lApproachFocusingCommander == null)
		{
			mOnApproachFocusingPosition = null; // 포커싱 하던 지휘관 사라져서 접근 포커싱 중단
			return;
		}

		Vector3 lApproachEndPosition = lApproachFocusingCommander.aStageCommander.aWorldPosition;

		if (mApproachFocusingTimer > 0.0f)
		{
			mApproachFocusingTimer -= Time.deltaTime;
			if (mApproachFocusingTimer < 0.0f)
				mApproachFocusingTimer = 0.0f;

			float lLerpFactor = mApproachFocusingCurve.Evaluate(1.0f - mApproachFocusingTimer / mApproachFocusingDelay);
			//Debug.Log("_OnApproachFocusingCommander() lLerpFactor=" + lLerpFactor + " mApproachFocusingTimer=" + mApproachFocusingTimer);
			vQuaterViewCameraSet.aViewCenter = Vector3.Lerp(vQuaterViewCameraSet.aViewCenter, lApproachEndPosition, lLerpFactor);
		}
		else
		{
			vQuaterViewCameraSet.aViewCenter = Vector3.Lerp(vQuaterViewCameraSet.aViewCenter, lApproachEndPosition, cFocusApproachFactor);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커싱 좌표로 점근을 활성화합니다.
	private void _EnableApproachFocusingCoord(Coord2 pApproachFocusingCoord, AnimationCurve pFocusingCurve, float pFocusingDelay)
	{
		//Debug.Log("_EnableApproachFocusingPosition() pIsAnimatingFocus=" + pIsAnimatingFocus);

		mIsApproachFocusing = true;
		mOnApproachFocusingPosition = _OnApproachFocusingCoord;
		mApproachFocusingStartPosition = vQuaterViewCameraSet.aViewCenter;
		mApproachFocusingEndPosition = Coord2.ToWorld(pApproachFocusingCoord);
		mApproachFocusingCurve = pFocusingCurve;
		mApproachFocusingDelay = pFocusingDelay;
		mApproachFocusingTimer = (pFocusingCurve != null) ? pFocusingDelay : 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// OnApproachFocusingPosition
	private void _OnApproachFocusingCoord()
	{
		if (mApproachFocusingTimer > 0.0f)
		{
			mApproachFocusingTimer -= Time.deltaTime;
			if (mApproachFocusingTimer < 0.0f)
				mApproachFocusingTimer = 0.0f;

			float lLerpFactor = mApproachFocusingCurve.Evaluate(1.0f - mApproachFocusingTimer / mApproachFocusingDelay);
			//Debug.Log("_OnApproachFocusingCoord() lLerpFactor=" + lLerpFactor + " mApproachFocusingTimer=" + mApproachFocusingTimer);
			vQuaterViewCameraSet.aViewCenter = Vector3.Lerp(mApproachFocusingStartPosition, mApproachFocusingEndPosition, lLerpFactor);
		}
		else
		{
			vQuaterViewCameraSet.aViewCenter = mApproachFocusingEndPosition;

			mOnApproachFocusingPosition = null; // 점근했으면 종료
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 점근을 활성화합니다.
	private void _EnableZoom(bool pIsAnimatingZoom, float pApproachZoomSliderValue)
	{
		if (!pIsAnimatingZoom)
		{
			vZoomSlider.value = pApproachZoomSliderValue;
			return;
		}
			
		mZoomApproachTimer = vZoomDelay;
		mApproachZoomSliderValue = pApproachZoomSliderValue;
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 점근을 처리합니다.
	private void _ApproachZoom()
	{
		//Debug.Log("_ApproachZoom()");

		if (mZoomApproachTimer > 0.0f)
		{
			mZoomApproachTimer -= Time.deltaTime;
			if (mZoomApproachTimer < 0.0f)
				mZoomApproachTimer = 0.0f;

			float lLerpFactor = 1.0f - mZoomApproachTimer / vZoomDelay;
			float lZoomFactor = Mathf.Lerp(vZoomSlider.value, mApproachZoomSliderValue, lLerpFactor);
			if (vZoomSlider.gameObject.activeInHierarchy)
				vZoomSlider.value = lZoomFactor;
			else
				vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance = Mathf.Lerp(
					vQuaterViewCameraSet.vPerspectiveZoomInfo.vCloseViewDistance,
					vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewDistance,
					lZoomFactor);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnSpawnCommanderDelegate
	private void _OnGridSpawnCommander(BattleCommander pCommander)
	{
		pCommander.aOnDestroyedDelegate += _OnGridCommanderDestroyed;
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyedDelegate
	private void _OnGridCommanderDestroyed(int pScriptUserId, int pScriptSlotId)
	{
		BattleUser lUser = BattlePlay.main.aBattleGrid.GetScriptUser(pScriptUserId);

		// 유저 지휘관일 경우 UI에 반영합니다.
		if (lUser != null)
		{
			for (int iTeam = 1; iTeam < vCommanderCountGauges.Length; ++iTeam)
				vCommanderCountGauges[iTeam].UpdateCount(iTeam);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyingDelegate
	private void _OnControlCommanderDestroying(BattleCommander pCommander)
	{
		// 파괴된 부대의 지휘관을 포커싱 중이라면 현재 카메라에서 가장 가까운 부대의 지휘관으로 포커싱을 자동 변경합니다.
		if (mFocusingCommanderLink.Get() == pCommander)
		{
			BattleCommander lNearsetCommander = pCommander.aUser.SearchNearestCommander(Coord2.ToCoord(vQuaterViewCameraSet.aViewCenter));
			if (lNearsetCommander != null)
			{
				FocusCommander(lNearsetCommander, false, vDefaultFocusingCurve, vDefaultFocusingDelay); // 인접 지휘관으로 포커싱
			}
			else
			{
				pCommander.aStageCommander.SetSelected(false); // 인접 지휘관이 없다면 현재 부대의 선택 상태라도 해제
				vClientUserControl.SelectCommander(null);
				vDebugUserControl.SelectCommander(null);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyedDelegate
	private void _OnControlCommanderDestroyed(int pScriptUserId, int pScriptSlotId)
	{
		BattleUser lUser = BattlePlay.main.aBattleGrid.GetScriptUser(pScriptUserId);

		// 유저 지휘관일 경우의 처리를 합니다.
		if (lUser != null)
		{
			int lCommanderSlotIdx = BattlePlay.main.aBattleGrid.GetScriptUserCommanderSlotIdx(pScriptSlotId);
			lUser.aUserInfo.RestoreCommanderSlotIdx(lCommanderSlotIdx); // 슬롯 사용함

			if (vClientUserControl.aControlUser == lUser)
			{
				vClientUserControl.SetCommanderButton(lCommanderSlotIdx, null); // 버튼 복구
				vClientUserControl.SetCommanderItemButton(lCommanderSlotIdx, vClientUserControl.aControlUser.aUserInfo.mCommanderItems[lCommanderSlotIdx]);
			}
			if (vDebugUserControl.aControlUser == lUser)
			{
				vDebugUserControl.SetCommanderButton(lCommanderSlotIdx, null); // 버튼 복구
				vDebugUserControl.SetCommanderItemButton(lCommanderSlotIdx, vDebugUserControl.aControlUser.aUserInfo.mCommanderItems[lCommanderSlotIdx]);
			}
		}

		//// 지휘관 버튼의 쿨타임을 돌립니다.
		//if (vClientUserControl.aControlUser == pCommander.aUser)
		//{
		//	//vClientUserControl.UpdateCommanderItemButton(pCommander.aCommanderItem.mSlotIdx);
		//	//vClientUserControl.ActivateCommanderCoolTime(lCommanderSlotIdx);
		//}
		//if (vDebugUserControl.aControlUser == pCommander.aUser)
		//{
		//	//vDebugUserControl.UpdateCommanderItemButton(pCommander.aCommanderItem.mSlotIdx);
		//	//vDebugUserControl.ActivateCommanderCoolTime(lCommanderSlotIdx);
		//}

		_UpdateUserCommanderPopCount(lUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnDestroyedTroopDelegate
	private void _OnControlCommanderDestroyedTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		_UpdateUserCommanderPopCount(pCommander.aUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnRecruitTroopsDelegate
	private void _OnControlCommanderRecruitTroop(BattleCommander pCommander)
	{
		_UpdateUserCommanderPopCount(pCommander.aUser);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnVeterancyRankUpDelegate
	private void _OnControlCommanderVeteranRankUp(BattleCommander pCommander)
	{
		if(pCommander.aUser == vClientUserControl.aControlUser)
			vClientUserControl.OnVeteranRankUp(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.OnVeterancyExpDelegate 
	private void _OnControlCommanderVeteranExp(BattleCommander pCommander)
	{
		if (pCommander.aUser == vClientUserControl.aControlUser)
			vClientUserControl.OnVeteranExp(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤하는 팀을 지정합니다.
	private void _SetControlTeam(int pControlTeamIdx)
	{
		//Debug.Log("_SetControlTeam() " + mControlTeamIdx + " -> " + pControlTeamIdx);

		mControlTeamIdx = pControlTeamIdx;

		BattleTroop[] lTroops = BattlePlay.main.aBattleGrid.aTroops;
		for (int iTroop = 0; iTroop < lTroops.Length; ++iTroop)
		{
			BattleTroop lTroop = lTroops[iTroop];
			if (lTroop == null)
				continue;

			lTroop.aStageTroop.SetTargetable(BattlePlay.main.vControlPanel.aFocusingCommander != lTroop.aCommander); // 조종하는 지휘관과 다른 지휘관의 부대일 경우만 타게팅 가능
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Commander의 갯수를 업데이트합니다.
	private void _UpdateUserCommanderPopCount(BattleUser pUser)
	{
		if (vClientUserControl.aControlUser == pUser)
			vClientUserControl.UpdateCommanderPopCount();
		if (vDebugUserControl.aControlUser == pUser)
			vDebugUserControl.UpdateCommanderPopCount();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIPanel mUIPanel;

	private BattleUser mClientUser;
	private BattleUser mDebugUser;

	private bool mIsDraggingStage;
	private bool mIsApproachFocusing;
	private Vector3 mApproachFocusingStartPosition;
	private Vector3 mApproachFocusingEndPosition;
	private GuidLink<BattleCommander> mFocusingCommanderLink;
	private GuidLink<BattleCommander> mApproachFocusingCommanderLink;
	private OnApproachFocusingPosition mOnApproachFocusingPosition;
	private AnimationCurve mApproachFocusingCurve;
	private float mApproachFocusingDelay;
	private float mApproachFocusingTimer;
//	private bool mIsApproachFocusingPosition;
	private float mDefaultZoomSliderValue;
	private float mZoomApproachTimer;
	private float mApproachZoomSliderValue;

	private int mControlTeamIdx;
}
