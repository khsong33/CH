using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUILoadingPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vLoadingScreenTexture;
	public UILabel vStageNameLabel;

	public float vMinLoadingDelay = 2.0f;
	public float vFadingDelay = 1.0f;

	public UITexture[] vBattleUserDeckTexture;
	public UILabel[] vBattleUserNameLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vLoadingScreenTexture == null)
			Debug.LogError("vLoadingScreenTexture is empty - " + this);
		if (vStageNameLabel == null)
			Debug.LogError("vStageNameLabel is empty - " + this);

		mUIPanel = GetComponent<UIPanel>();
		if (mUIPanel == null)
			Debug.LogError("can't find UIPanel - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로딩 화면을 보입니다.
	public void Show()
	{
		gameObject.SetActive(true);
		mUIPanel.alpha = 1.0f;
		
		mShowTime = Time.realtimeSinceStartup;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 로딩 화면을 숨깁니다.
	public IEnumerator Hide()
	{
		float lLoadingDelay = vMinLoadingDelay - vFadingDelay;
		while ((Time.realtimeSinceStartup - mShowTime) < lLoadingDelay)
			yield return null;

		float lFadintTimer = Time.deltaTime;
		while (lFadintTimer < vFadingDelay)
		{
			float lLerpFactor = lFadintTimer / vFadingDelay;
			mUIPanel.alpha = 1.0f - lLerpFactor;

			yield return null;
			lFadintTimer += Time.deltaTime;
		}

		gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 정보를 지정합니다.
	public void SetStageInfo(StageSpec pStageSpec)
	{
		vStageNameLabel.text = (GameData.get.aBattleTestInfo.aMapAssetKey == null)
//			? LocalizedTable.get.GetLocalizedText(pStageSpec.aNameKey);
			? pStageSpec.mName
			: ("Testing Map: " + GameData.get.aBattleTestInfo.aMapAssetKey);
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 전투정보를 저장합니다.
	public void SetBattleUserInfo(BattleMatchInfo pMatchInfo)
	{
		if (pMatchInfo == null)
			return;

		for(int iUser = 0; iUser < pMatchInfo.mUserCount; iUser++)
		{
			NationSpec lNationSpec = NationTable.get.GetNationSpec(pMatchInfo.mUserInfos[iUser].mBattleNation);
			vBattleUserDeckTexture[iUser].mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mBackgroundIcon);
			vBattleUserNameLabel[iUser].text = pMatchInfo.mUserInfos[iUser].mUserName;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIPanel mUIPanel;

	private float mShowTime;
}
