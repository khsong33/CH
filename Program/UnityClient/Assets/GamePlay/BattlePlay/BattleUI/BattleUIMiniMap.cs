using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUIMiniMap : MonoBehaviour
{
	private class CommanderWidget
	{
		internal BattleCommander mBattleCommander;
		internal UITexture mCommanderIcon;
		internal UITexture mCommanderSelectIcon;
		internal Transform mTransform;
		internal bool mIsShow;

		internal LinkedListNode<CommanderWidget> mNode;

		internal CommanderWidget(UITexture pCommanderIcon, UITexture pCommanderSelectIcon)
		{
			mCommanderIcon = pCommanderIcon;
			mTransform = mCommanderIcon.transform;
			mCommanderSelectIcon = pCommanderSelectIcon;
			mCommanderSelectIcon.transform.parent = mTransform;
			mNode = new LinkedListNode<CommanderWidget>(this);

			mIsShow = mCommanderIcon.gameObject.activeInHierarchy;
		}

		internal void Show(bool pIsShow)
		{
			mIsShow = pIsShow;
			mCommanderIcon.gameObject.SetActive(pIsShow);
		}
	}
	private class NotifyEffect
	{
		internal Transform mEffectTransform;
		internal LinkedList<NotifyEffect> mEffectUpdateList;
		internal LinkedListNode<NotifyEffect> mNode;
		internal float mNotifyTimer;

		internal NotifyEffect(Transform pEffectTransform, LinkedList<NotifyEffect> pEffectUpdateList)
		{
			mEffectTransform = pEffectTransform;
			mEffectTransform.gameObject.SetActive(false); // 비활성 상태로 시작

			mEffectUpdateList = pEffectUpdateList;
			if (pEffectUpdateList != null)
				mNode = new LinkedListNode<NotifyEffect>(this);
		}
		internal void Notify(float pNotifyDelay)
		{
			mNotifyTimer = pNotifyDelay;

			if ((pNotifyDelay > 0.0f) && // 시간 제한이 있고
				(mEffectUpdateList != null) && // 갱신 리스트가 있다면
				(mNode.List == null))
				mEffectUpdateList.AddLast(mNode); // 갱신 리스트에 등록

			mEffectTransform.gameObject.SetActive(true);
		}
		internal void Hide()
		{
			mEffectTransform.gameObject.SetActive(false);

			if ((mEffectUpdateList != null) &&
				(mNode.List != null))
				mEffectUpdateList.Remove(mNode);
		}
		internal void Update(float pUpdateDelta)
		{
			if (mNotifyTimer > 0.0f)
			{
				mNotifyTimer -= pUpdateDelta;
				if (mNotifyTimer <= 0.0f)
					Hide();
			}
		}
	}

	internal const float cUpdateMapDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vMapImage;
	public UITexture[] vSectorTextures;
	public BattleUIMiniMapCollider vMapCollider;

	public UISprite vMpCheckPointIconPrefab;
	public UISprite vVpCheckPointIconPrefab;
	public UISprite vHeadquarterIconPrefab;
	public UISprite[] vCheckPointIconPrefabs;
	public UITexture vEventPointIconPrefab;
	public UITexture vCommanderIconPrefab;

	public GameObject vCaptureEffectPrefab;
	public GameObject vEventReadyEffectPrefab;
	public float vEventReadyNotifyDelay = 3.0f;

	public UISprite vViewAreaSprite;
	public float vViewAreaCloseScale = 0.5f;
	public float vViewAreaFarScale = 3.0f;
	public Vector3 vViewAreaCenterCloseOffset = new Vector3(0.0f, 0.0f, 0.0f);
	public Vector3 vViewAreaCenterFarOffset = new Vector3(0.0f, 0.0f, 12.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Awake()
	{
		if (vMapImage == null)
			Debug.LogError("vMapImage is empty - " + this);
		if (vMapCollider == null)
			Debug.LogError("vMapCollider is empty - " + this);

		if (vMpCheckPointIconPrefab == null)
			Debug.LogError("vMpCheckPointIconPrefab is empty - " + this);
		if (vVpCheckPointIconPrefab == null)
			Debug.LogError("vVpCheckPointIconPrefab is empty - " + this);
		if (vHeadquarterIconPrefab == null)
			Debug.LogError("vHeadquarterIconPrefab is empty - " + this);
		if (vEventPointIconPrefab == null)
			Debug.LogError("vEventPointIconPrefab is empty - " + this);
		if (vCommanderIconPrefab == null)
			Debug.LogError("vCommanderIconPrefab is empty - " + this);

		if (vCaptureEffectPrefab == null)
			Debug.LogError("vCaptureEffectPrefab is empty - " + this);
		if (vEventReadyEffectPrefab == null)
			Debug.LogError("vEventReadyEffectPrefab is empty - " + this);

		if (vViewAreaSprite == null)
			Debug.LogError("vViewAreaSprite is empty - " + this);

		//Debug.Log("transform.position=" + transform.position + " - " + this);
		//Debug.Log("transform.localPosition=" + transform.localPosition + " - " + this);

		mCommanderWidgetPoolingQueue = new Queue<CommanderWidget>();
		mCommanderWidgetList = new LinkedList<CommanderWidget>();
		mCommanderWidgets = new CommanderWidget[BattleGrid.cMaxCommanderCount];

		mNotifyEffectUpdateList = new LinkedList<NotifyEffect>();

		mViewAreaSpriteTransform = vViewAreaSprite.transform;
		mViewAreaSpriteBaseScale = mViewAreaSpriteTransform.localScale;
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 미니맵을 초기화합니다.
	public void LoadMap()
	{
		BattleGrid lBattleGrid = BattlePlay.main.aBattleGrid;

		// 맵 이미지를 로딩합니다.
		mMiniMapSize = new Coord2(vMapImage.width, vMapImage.height);
		//Debug.Log("mMiniMapSize=" + mMiniMapSize);
		mGridMapSize = new Coord2(lBattleGrid.aWidthX, lBattleGrid.aHeightZ);
		//Debug.Log("mGridMapSize=" + mGridMapSize);
		mWidgetPositionScale = new Vector2((float)mMiniMapSize.x / mGridMapSize.x, (float)mMiniMapSize.z / mGridMapSize.z);

		//Debug.Log("BattlePlay.main.aMatchInfo.aStageSpec.mMiniMapAssetKey=" + BattlePlay.main.aMatchInfo.aStageSpec.mMiniMapAssetKey);
		vMapImage.mainTexture = TextureResourceManager.get.LoadTexture(BattlePlay.main.aMatchInfo.aStageSpec.mMiniMapAssetKey);

		// 거점 위젯을 생성합니다.
		mCheckPointIcons = new UISprite[lBattleGrid.aCheckPointList.Count];
		mCaptureNotifyEffects = new NotifyEffect[lBattleGrid.aCheckPointList.Count];
		for (int iCheckPoint = 0; iCheckPoint < lBattleGrid.aCheckPointList.Count; ++iCheckPoint)
		{
			// 거점 아이콘을 생성합니다.
			BattleCheckPoint lCheckPoint = lBattleGrid.aCheckPointList[iCheckPoint];
			if (lCheckPoint.aLinkHeadquarter != null)
				mCheckPointIcons[iCheckPoint] = _CreateWidget<UISprite>(vHeadquarterIconPrefab, lCheckPoint.aCoord);
			else if (lCheckPoint.aStarId > 0)
				mCheckPointIcons[iCheckPoint] = _CreateWidget<UISprite>(vVpCheckPointIconPrefab, lCheckPoint.aCoord);
			else
				mCheckPointIcons[iCheckPoint] = _CreateWidget<UISprite>(vCheckPointIconPrefabs[(int)lCheckPoint.aCheckPointType], lCheckPoint.aCoord);

			_UpdateCheckPointColor(lBattleGrid.aCheckPointList[iCheckPoint]); // 거점 색 초기화

			// 거점 점령 효과를 생성합니다.
			Transform lCaptureNotifyEffectTransform = _CreateWidget(vCaptureEffectPrefab, lCheckPoint.aCoord);
			mCaptureNotifyEffects[iCheckPoint] = new NotifyEffect(lCaptureNotifyEffectTransform, null);
		}

		// 이벤트 지점 위젯을 생성합니다.
		mEventPointIcons = new UITexture[lBattleGrid.aEventPointList.Count];
		mEventReadyNotifyEffects = new NotifyEffect[lBattleGrid.aEventPointList.Count];
		for (int iEventPoint = 0; iEventPoint < lBattleGrid.aEventPointList.Count; ++iEventPoint)
		{
			// 이벤트 지점 아이콘을 생성합니다.
			BattleEventPoint lEventPoint = lBattleGrid.aEventPointList[iEventPoint];
			mEventPointIcons[iEventPoint] = _CreateWidget<UITexture>(vEventPointIconPrefab, lEventPoint.aCoord);
			mEventPointIcons[iEventPoint].gameObject.SetActive(false); // 안 보이는 상태에서 시작

			// 이벤트 등장 효과를 생성합니다.
			Transform lEventReadyNotifyEffectTransform = _CreateWidget(vEventReadyEffectPrefab, lEventPoint.aCoord);
			mEventReadyNotifyEffects[iEventPoint] = new NotifyEffect(lEventReadyNotifyEffectTransform, mNotifyEffectUpdateList);
		}
		lBattleGrid.aOnEventDelegate += _OnEvent; // 이벤트 콜백을 등록

		// 맵 충돌체를 초기화합니다.
		vMapCollider.OnLoadMap();

		// 맵 갱신을 시작합니다.
		StartCoroutine(_UpdateMap());
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점의 팀 변화를 갱신합니다.
	public void UpdateCheckPointTeamChange(BattleCheckPoint pCheckPoint)
	{
		_UpdateCheckPointColor(pCheckPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 생성합니다.
	public void CreateCommander(BattleCommander pBattleCommander)
	{
		if (pBattleCommander.aTeamUser == null)
			return;

		CommanderWidget lCommanderWidget;
		if (mCommanderWidgetPoolingQueue.Count <= 0)
		{
			UITexture lCommanderIcon = _CreateWidget<UITexture>(vCommanderIconPrefab, pBattleCommander.aCoord);
			UITexture lCommanderSelectIcon = _CreateWidget<UITexture>(vCommanderIconPrefab, pBattleCommander.aCoord);
			lCommanderWidget = new CommanderWidget(lCommanderIcon, lCommanderSelectIcon);
		}
		else
		{
			lCommanderWidget = mCommanderWidgetPoolingQueue.Dequeue();
			lCommanderWidget.mCommanderIcon.gameObject.SetActive(true);
			lCommanderWidget.mCommanderSelectIcon.gameObject.SetActive(false);
			lCommanderWidget.mTransform.localPosition = _GetWidgetPosition(pBattleCommander.aCoord);
		}

		lCommanderWidget.mBattleCommander = pBattleCommander;
		lCommanderWidget.mCommanderIcon.mainTexture = pBattleCommander.aCommanderSpec.GetMiniMapIconTexture();
		lCommanderWidget.mCommanderSelectIcon.mainTexture = pBattleCommander.aCommanderSpec.GetMiniMapSelectIconTexture();
		ColorLib.SetWidgetColor(lCommanderWidget.mCommanderIcon, BattlePlay.main.GetUserColor(pBattleCommander));

		mCommanderWidgetList.AddLast(lCommanderWidget.mNode);
		mCommanderWidgets[pBattleCommander.aCommanderGridIdx] = lCommanderWidget;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 삭제합니다.
	public void DestroyCommander(BattleCommander pBattleCommander)
	{
		CommanderWidget lCommanderWidget = mCommanderWidgets[pBattleCommander.aCommanderGridIdx];
		if (lCommanderWidget == null)
			return;

		lCommanderWidget.mIsShow = lCommanderWidget.mCommanderIcon.gameObject.activeInHierarchy;

		mCommanderWidgets[pBattleCommander.aCommanderGridIdx] = null;
		mCommanderWidgetList.Remove(lCommanderWidget.mNode);

		lCommanderWidget.mCommanderIcon.gameObject.SetActive(false);
		mCommanderWidgetPoolingQueue.Enqueue(lCommanderWidget);
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 점령을 시작합니다.
	public void StartCapture(BattleCheckPoint pCheckPoint)
	{
		mCaptureNotifyEffects[pCheckPoint.aCheckPointIdx].Notify(0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 삭제합니다.
	public void StopCapture(BattleCheckPoint pCheckPoint)
	{
		mCaptureNotifyEffects[pCheckPoint.aCheckPointIdx].Hide();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 맵 상황을 갱신합니다.
	private IEnumerator _UpdateMap()
	{
		for (;;)
		{
			// 지휘관 아이콘을 움직입니다.
			int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
			foreach (CommanderWidget lCommanderWidget in mCommanderWidgetList)
			{
				bool lIsShowCommanderWidget
					= lCommanderWidget.mBattleCommander.aIsDetecteds[lObserverTeamIdx]
					| (lCommanderWidget.mBattleCommander.aTargetingDummy != null);

				if (lCommanderWidget.mIsShow != lIsShowCommanderWidget)
					lCommanderWidget.Show(lIsShowCommanderWidget) ;

				if (lCommanderWidget.mIsShow)
					lCommanderWidget.mTransform.localPosition = _GetWidgetPosition(lCommanderWidget.mBattleCommander.aCoord);

				if (lCommanderWidget.mBattleCommander == BattlePlay.main.vControlPanel.aFocusingCommander)
					lCommanderWidget.mCommanderSelectIcon.gameObject.SetActive(true);
				else
					lCommanderWidget.mCommanderSelectIcon.gameObject.SetActive(false);
			}

			// 알림 이펙트를 갱신합니다.
			LinkedListNode<NotifyEffect> lEventReadyNotifyEffectNode = mNotifyEffectUpdateList.First;
			while (lEventReadyNotifyEffectNode != null)
			{
				LinkedListNode<NotifyEffect> lNextNode = lEventReadyNotifyEffectNode.Next;
				lEventReadyNotifyEffectNode.Value.Update(Time.deltaTime);
				lEventReadyNotifyEffectNode = lNextNode;					
			}			

			// 카메라 영역을 움직입니다.
			QuaterViewCameraSet lQuaterViewCameraSet = BattlePlay.main.vControlPanel.vQuaterViewCameraSet;
			float lViewAreaSpriteScale = Mathf.Lerp(vViewAreaCloseScale, vViewAreaFarScale, lQuaterViewCameraSet.aZoomLerpFactor);
			mViewAreaSpriteTransform.localScale = mViewAreaSpriteBaseScale * lViewAreaSpriteScale;
			Vector3 lViewAreaCenterOffset = Vector3.Lerp(vViewAreaCenterCloseOffset, vViewAreaCenterFarOffset, lViewAreaSpriteScale);
			Vector3 lViewAreaSpritePosition = lQuaterViewCameraSet.aCameraCenter + lViewAreaCenterOffset;
			mViewAreaSpriteTransform.localPosition = _GetWidgetPosition(Coord2.ToCoord(lViewAreaSpritePosition));

			yield return null;
//			yield return new WaitForSeconds(cUpdateMapDelay);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 미니맵 상의 아이콘을 생성해서 배치합니다.
	private T_Widget _CreateWidget<T_Widget>(T_Widget pWidgetPrefab, Coord2 pGridCoord) where T_Widget : UIWidget
	{
		T_Widget lWidget = NGUIUtil.InstantiateWidget<T_Widget>(gameObject, pWidgetPrefab);

		lWidget.transform.localPosition = _GetWidgetPosition(pGridCoord);

		return lWidget;
	}
	//-------------------------------------------------------------------------------------------------------
	// 미니맵 상의 아이콘 오브젝트를 생성해서 배치합니다.
	private Transform _CreateWidget(GameObject pWidgetPrefab, Coord2 pGridCoord)
	{
		GameObject lWidgetObject = NGUITools.AddChild(gameObject, pWidgetPrefab);
		Transform lWidgetTransform = lWidgetObject.transform;

		lWidgetTransform.localPosition = _GetWidgetPosition(pGridCoord);

		return lWidgetTransform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 그리드 좌표에 대한 미니맵 상에서의 위젯 좌표를 구합니다.
	private Vector3 _GetWidgetPosition(Coord2 pCoord)
	{
		Vector3 lWidgetPosition = new Vector3(mWidgetPositionScale.x * pCoord.x, mWidgetPositionScale.y * pCoord.z, 0.0f);
		return lWidgetPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// 거점 색을 갱신합니다.
	private void _UpdateCheckPointColor(BattleCheckPoint pCheckPoint)
	{
		ColorLib.SetWidgetColor(mCheckPointIcons[pCheckPoint.aCheckPointIdx], BattlePlay.main.vTeamColors[pCheckPoint.aTeamIdx]);

		if (pCheckPoint.aCheckPointIdx < vSectorTextures.Length)
		{
			UITexture lSectorTexture = vSectorTextures[pCheckPoint.aCheckPointGuid];
			if (lSectorTexture != null)
				ColorLib.SetWidgetColor(lSectorTexture, BattlePlay.main.vTeamColors[pCheckPoint.aTeamIdx]);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleGrid.OnEventDelegate
	private void _OnEvent(BattleEventPoint pEventPoint, BattleEventPoint.EventType pEventType)
	{
		//Debug.Log("_OnEvent() pEventPoint=" + pEventPoint + " pEventType=" + pEventType);

		if (pEventPoint.aGroundEventSpec.mMiniMapIconAssetKey == null)
			return; // 미니맵에 표시 안 함

		switch (pEventType)
		{
		case BattleEventPoint.EventType.TriggerReady:
			{				
				mEventPointIcons[pEventPoint.aEventPointIdx].mainTexture = pEventPoint.aGroundEventSpec.GetMiniMapIconTexture();
				mEventPointIcons[pEventPoint.aEventPointIdx].gameObject.SetActive(true);

				mEventReadyNotifyEffects[pEventPoint.aEventPointIdx].Notify(vEventReadyNotifyDelay);
			}
			break;
		case BattleEventPoint.EventType.TriggerActivated:
			{
				mEventPointIcons[pEventPoint.aEventPointIdx].gameObject.SetActive(false);

				mEventReadyNotifyEffects[pEventPoint.aEventPointIdx].Hide();
			}
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Coord2 mMiniMapSize;
	private Coord2 mGridMapSize;
	private Vector2 mWidgetPositionScale;

	private UISprite[] mCheckPointIcons;
	private NotifyEffect[] mCaptureNotifyEffects;

	private UITexture[] mEventPointIcons;
	private NotifyEffect[] mEventReadyNotifyEffects;

	private LinkedList<NotifyEffect> mNotifyEffectUpdateList;

	private Queue<CommanderWidget> mCommanderWidgetPoolingQueue;
	private LinkedList<CommanderWidget> mCommanderWidgetList;
	private CommanderWidget[] mCommanderWidgets;

	private Transform mViewAreaSpriteTransform;
	private Vector3 mViewAreaSpriteBaseScale;
}
