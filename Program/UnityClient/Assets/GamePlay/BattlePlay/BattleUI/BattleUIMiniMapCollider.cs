using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUIMiniMapCollider : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UIWidget vMiniMapWidget;

	public Transform vAnchorTransform;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vMiniMapWidget == null)
			Debug.LogError("vMiniMapWidget is empty - " + this);

		if (vAnchorTransform == null)
			Debug.LogError("vAnchorTransform is empty - " + this);

		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnPress(bool pIsDown)
	{
		//Debug.Log("OnPress() pIsDown=" + pIsDown + " UICamera.lastEventPosition=" + UICamera.lastEventPosition);

		if (pIsDown)
			mTouchID = UICamera.currentTouchID;
		else
			mTouchID = -1;

		//Debug.Log("_GetLastTouchCoord()=" + _GetLastTouchCoord());
		if (pIsDown)
		{
			Coord2 lFocusingCoord = _GetLastTouchCoord();
			BattlePlay.main.vControlPanel.FocusCoord(
				lFocusingCoord,
				BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
				BattlePlay.main.vControlPanel.vDefaultFocusingDelay);
		}
		else
		{
			BattlePlay.main.vControlPanel.ResetFocusingApproach();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnDrag(Vector2 pDelta)
	{
		if (mTouchID != UICamera.currentTouchID)
			return; // 첫 터치 조작만 받음

		//Debug.Log("OnDrag() pDelta=" + pDelta + " UICamera.lastEventPosition=" + UICamera.lastEventPosition);

		//Debug.Log("_GetLastTouchCoord()=" + _GetLastTouchCoord());
		Coord2 lFocusingCoord = _GetLastTouchCoord();
		BattlePlay.main.vControlPanel.FocusCoord(lFocusingCoord);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 터치 영역을 초기화합니다.
	public void OnLoadMap()
	{
		mUIRoot = NGUITools.FindInParents<UIRoot>(gameObject);

		//Debug.Log("mTransform.position=" + mTransform.position + " - " + this);
		//Debug.Log("mTransform.localCorners[0]=" + vMiniMapWidget.localCorners[0]);
		//Debug.Log("mTransform.worldCorners[0]=" + vMiniMapWidget.worldCorners[0]);
		mLeftBottomPosition = NGUIUtil.GetLocalPositionFromParent(mTransform, vAnchorTransform);
		//Debug.Log("mLeftBottomPosition=" + mLeftBottomPosition);

		mMiniMapSize = new Coord2(vMiniMapWidget.width, vMiniMapWidget.height);
		//Debug.Log("mMiniMapSize=" + mMiniMapSize);
		mGridMapSize = new Coord2(BattlePlay.main.aBattleGrid.aWidthX, BattlePlay.main.aBattleGrid.aHeightZ);
		//Debug.Log("mGridMapSize=" + mGridMapSize);
		mGridCoordScale = new Vector2((float)mGridMapSize.x / mMiniMapSize.x, (float)mGridMapSize.z / mMiniMapSize.z);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 최근 터치 좌표에 대한 그리드 좌표를 얻습니다.
	private Coord2 _GetLastTouchCoord()
	{
		Vector3 lUIPoint = NGUIUtil.ScreenToUIPoint(mUIRoot, UICamera.lastEventPosition);
		//Debug.Log("lUIPoint=" + lUIPoint + " <- UICamera.lastEventPosition=" + UICamera.lastEventPosition);
		Vector3 lTouchPosition = lUIPoint - mLeftBottomPosition;
		//Debug.Log("lTouchPosition=" + lTouchPosition);

		Coord2 lTouchCoord = new Coord2((int)(mGridCoordScale.x * lTouchPosition.x), (int)(mGridCoordScale.y * lTouchPosition.y));
		return lTouchCoord;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private UIRoot mUIRoot;
	private Vector3 mLeftBottomPosition;

	private Coord2 mMiniMapSize;
	private Coord2 mGridMapSize;
	private Vector2 mGridCoordScale;

	private int mTouchID;
}
