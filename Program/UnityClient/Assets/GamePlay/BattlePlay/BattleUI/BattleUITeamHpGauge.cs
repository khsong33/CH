using UnityEngine;
using System.Collections;
using System;

public class BattleUITeamHpGauge : MonoBehaviour
{
	internal float cHpSliderUpdateDelayMin = 1.0f;
	internal float cHpSliderUpdateDelayMax = 1.2f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UISlider vHpSlider;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vHpSlider == null)
			Debug.LogError("vHpSlider is empty - " + this);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 갱신을 시작합니다.
	public void StartUpdate(int pTeamIdx)
	{
		StartCoroutine(_UpdateHpSlider(pTeamIdx)); // HP 갱신 시작
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] HP 슬라이더를 갱신합니다.
	private IEnumerator _UpdateHpSlider(int pTeamIdx)
	{
		BattleMatchInfo lMatchInfo = BattlePlay.main.aMatchInfo;
		BattleProgressInfo lProgressInfo = BattlePlay.main.aProgressInfo;

		for (;;)
		{
			if (BattlePlay.main.aIsPauseBattle)
				yield return null;

			int lValidCommanderCountSum = 0;
			float lHpRatioSum = 0.0f;

			for (int iUser = 0; iUser < lMatchInfo.mUserCount; ++iUser)
			{
				BattleUser lUser = BattlePlay.main.aUsers[iUser];
				if (lUser.aTeamIdx != pTeamIdx)
					continue;

				lValidCommanderCountSum += lUser.aValidCommanderCount;

				if (lProgressInfo.aUserStates[iUser] == BattleProgressInfo.UserState.Playing)
					for (int iCommander = 0; iCommander < lUser.aValidCommanderCount; ++iCommander)
					{
						BattleCommander lCommander = lUser.aCommanders[iCommander];
						if (lCommander == null)
						{
							if (lUser.aUserInfo.mCommanderItems[iCommander].mCount > 0)
								lHpRatioSum += 1.0f; // 출격 안한 지휘관은 HP 100%로 간주
						}
						else
						{
							lHpRatioSum += (float)lCommander.aCurTotalMilliHp / lCommander.aMaxTotalMilliHp;
						}
					}
			}

			vHpSlider.value = lHpRatioSum / lValidCommanderCountSum;

			yield return new WaitForSeconds(UnityEngine.Random.Range(cHpSliderUpdateDelayMin, cHpSliderUpdateDelayMax));

		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
