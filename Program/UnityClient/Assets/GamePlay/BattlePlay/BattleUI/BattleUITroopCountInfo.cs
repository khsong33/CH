﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUITroopCountInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture[] vTroopIcon;
	public UILabel[] vTroopCount;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 부대 수를 셋팅합니다.
	public void SetTroopCountInfo(CommanderTroopSet pTroopSet)
	{
		if (vTroopIcon.Length != pTroopSet.mTroopKindCount)
			Debug.LogError("Not same what Icon Length and troop set kind count int set troop count info function");

		for (int iKind = 0; iKind < pTroopSet.mTroopKindCount; iKind++)
		{
			vTroopIcon[iKind].mainTexture = TextureResourceManager.get.LoadTexture(pTroopSet.mTroopKindCountInfos[iKind].mTroopIconAssetKey);
			vTroopIcon[iKind].color = Color.white; 

			vTroopCount[iKind].text = pTroopSet.mTroopKindCountInfos[iKind].mTroopCount.ToString();
			vTroopCount[iKind].color = Color.white;
		}
		mTroopSet = pTroopSet;
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 수를 갱신합니다.
	public void UpdateTroopCountInfo(int[] pTroopKindCount)
	{
		for (int iKind = 0; iKind < pTroopKindCount.Length; iKind++)
		{
			if (vTroopCount.Length <= iKind)
				break;

			vTroopCount[iKind].text = pTroopKindCount[iKind].ToString();
			if (mTroopSet.mTroopKindCountInfos[iKind].mTroopCount != pTroopKindCount[iKind])
			{
				vTroopIcon[iKind].color = BattleConfig.get.mTroopMoraleStateColors[(int)TroopMoraleState.PinDown];
				vTroopCount[iKind].color = BattleConfig.get.mTroopMoraleStateColors[(int)TroopMoraleState.PinDown];
			}
			else
			{
				vTroopIcon[iKind].color = Color.white;
				vTroopCount[iKind].color = Color.white;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderTroopSet mTroopSet;
}
