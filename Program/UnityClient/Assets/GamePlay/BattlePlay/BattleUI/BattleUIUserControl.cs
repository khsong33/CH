using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BattleUIUserControl : MonoBehaviour
{
	public delegate void OnSelectCommander(BattleCommander pBattleCommander);
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vButtonId;	
	public BattleUIControlPanel vControlPanel;

	public GameObject vSpawnButtonRoot;
	public TweenColor vSpawnPanelButtonAlarm;
	public BattleUICommanderButton[] vSpawnButtons;
	public BattleUICommanderButton[] vCommanderButtons;
	public bool vIsAutoSpawn = false;
	public BattleUICommanderButton[] vSpawnOrderCommanderButtons;
	public int[] vCommanderButtonsPosIndex;
	public BattleUIMpGauge vMpGauge;
	public UILabel vCommanderPopCountLabel;
	public BattleUICommanderInfo vCommanderInfo;
	public BattleUISkillButton vSkillButton;
	public UILabel vDebugUserInfoLabel;

	public InfiniteListPopulator vSpawnCommanderScrollList;

	public GameObject vCommanderToopTip;
	public GameObject vSkillToolTip;
	public bool vIsActionToolTip;

	public TouchPanel vTouchPanel;
	public BattleUICancelDrag vCancelDragArea;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 유저
	public BattleUser aControlUser
	{
		get { return mControlUser; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택 지휘관 콜백
	public OnSelectCommander aOnSelectCommander { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vControlPanel == null)
			Debug.LogError("vControlPanel is empty - " + this);
		//if (vSpawnButtons.Length != BattleUserInfo.cMaxCommanderItemCount)
		//	Debug.LogError("vSpawnButtons.Length mismatch - " + this);
		//for (int iButton = 0; iButton < vSpawnButtons.Length; ++iButton)
		//{
		//	if (vSpawnButtons[iButton] == null)
		//		Debug.LogError("vSpawnButtons is empty - " + this);
		//	vSpawnButtons[iButton].vButtonIndex = iButton;
		//}
		if (vCommanderButtons.Length != BattleUserInfo.cMaxCommanderCount)
			Debug.LogError("vCommanderButtons.Length mismatch - " + this);
		
		mCommanderButtonPosition = new Vector3[vCommanderButtons.Length];
		vCommanderButtonsPosIndex = new int[vCommanderButtons.Length];
		for (int iButton = 0; iButton < vCommanderButtons.Length; ++iButton)
		{
			if (vCommanderButtons[iButton] == null)
				Debug.LogError("vCommanderButtons is empty - " + this);
			vCommanderButtons[iButton].vButtonIndex = iButton;
			vCommanderButtons[iButton].vSlotIdx = iButton;
			vCommanderButtons[iButton].vIsCommanderPanelButton = true;
			mCommanderButtonPosition[iButton] = vCommanderButtons[iButton].transform.localPosition;
			vCommanderButtonsPosIndex[iButton] = vCommanderButtons[iButton].vSlotIdx;
		}
		if (vMpGauge == null)
			Debug.LogError("vMpGauge is empty - " + this);
		if (vCommanderPopCountLabel == null)
			Debug.LogError("vCommanderPopCountLabel is empty - " + this);
		if (vCommanderInfo == null)
			Debug.LogError("vCommanderInfo is empty - " + this);
		if (vSkillButton == null)
			Debug.LogError("vSkillButton is empty - " + this);

		if (vTouchPanel == null)
			Debug.LogError("vTouchPanel is empty - " + this);
		if (vCancelDragArea == null)
			Debug.LogError("vCancelDragArea is empty - " + this);

		mCommanderButtonCount = 0;
		mCommanderItemPreviewButtonCount = 0;
		//mSortPreviewCommanderIndex = 0;

		mIsEnableSpawn = true;

		vIsActionToolTip = false;

		SetActiveDragCancelArea(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 컨트롤 할 유저를 지정합니다.
	public void SetControlUser(BattleUser pControlUser)
	{
		vCommanderInfo.SetDisplayCommander(null);
		vSkillButton.SetSkillInfo(null);

		mControlUser = pControlUser;
		gameObject.SetActive(pControlUser != null);
		if (pControlUser == null)
			return; // 비활성화로 끝냄

		//mSpawnSortButtonList = new List<CommanderItem>();
		//for (int iButton = 0; iButton < vSpawnButtons.Length; ++iButton)
		//{
		//	if (mControlUser.aUserInfo.mCommanderItems[iButton].mCommanderSpec == null)
		//	{
		//		vSpawnButtons[iButton].SetUserControl(this);
		//		vSpawnButtons[iButton].SetCommanderItem(null);
		//	}
		//	else
		//	{
		//		vSpawnButtons[iButton].SetUserControl(this);
		//		vSpawnButtons[iButton].SetCommanderItem(mControlUser.aUserInfo.mCommanderItems[iButton]);
		//		mSpawnSortButtonList.Add(mControlUser.aUserInfo.mCommanderItems[iButton]);
		//	}
		//	vSpawnButtons[iButton].vIsCommanderPanelButton = false;
		//}
//		//mSpawnSortButtonList.Sort((lData, rData) =>
		//SortUtil.SortList<CommanderItem>(mSpawnSortButtonList, (lData, rData) =>
		//{
		//	if (lData.aSpawnMp > rData.aSpawnMp )
		//		return 1;
		//	else if (lData.aSpawnMp < rData.aSpawnMp)
		//		return -1;
		//	return 0;
		//});

		for (int iButton = 0; iButton < vCommanderButtons.Length; ++iButton)
		{
			vCommanderButtons[iButton].SetUserControl(this);
			SetCommanderButton(iButton, null);
			SetCommanderItemButton(iButton, mControlUser.aUserInfo.mCommanderItems[iButton]);

			//if(mControlUser.aUserInfo.mCommanderItems[iButton].mCommanderSpec == null)
			//{

			//	vCommanderButtons[iButton].SetUserControl(this);
			//	vCommanderButtons[iButton].SetCommanderItem(null);
			//}
			//else
			//{
			//	vCommanderButtons[iButton].SetUserControl(this);
			//	vCommanderButtons[iButton].SetCommanderItem(mControlUser.aUserInfo.mCommanderItems[iButton]);
			//}
		}

		vSkillButton.SetUserControl(this);

		vMpGauge.SetMp(mControlUser.aCurrentMp, false);
		vMpGauge.SetExtraMpRatio(mControlUser.aCurrentExtraMilliMp / 10); // 밀리 / 10은 백분율 값

		if (vDebugUserInfoLabel != null)
			vDebugUserInfoLabel.text = String.Format("USER {0}", mControlUser.aUserIdx);

		//// 스크롤 리스트 셋팅
		//if (vSpawnCommanderScrollList.gameObject.activeInHierarchy)
		//{
		//	ArrayList lSpawnButtonArrayList = new ArrayList();
		//	for (int iSpawn = 0; iSpawn < mSpawnSortButtonList.Count; iSpawn++)
		//	{
		//		CommanderItemListData lSpawnCommanderButton = new CommanderItemListData();
		//		lSpawnCommanderButton.mControlUser = this;
		//		lSpawnCommanderButton.mCommanderItem = mSpawnSortButtonList[iSpawn];
		//		lSpawnButtonArrayList.Add(lSpawnCommanderButton);
		//	}
		//	vSpawnCommanderScrollList.InitTableView(lSpawnButtonArrayList, null, 0);
		//}

		UpdateCommanderPopCount();

		mLastSpawnButtonIdx = 0;
		mLastSpawnOrderIdx = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public void UpdateFrame()
	{
		//for (int iButton = 0; iButton < vSpawnButtons.Length; ++iButton)
		//	vSpawnButtons[iButton].UpdateActivation();
		for (int iButton = 0; iButton < vCommanderButtons.Length; ++iButton)
		{
			vCommanderButtons[iButton].UpdateAlarm();
			vCommanderButtons[iButton].UpdateActivation();
		}

		if (vIsAutoSpawn &&
			(mLastSpawnOrderIdx < vCommanderButtons.Length))
		{
			if (vSpawnOrderCommanderButtons[mLastSpawnOrderIdx].aClickMode == BattleUICommanderButton.ClickMode.Spawn)
				vSpawnOrderCommanderButtons[mLastSpawnOrderIdx].OnClick();
			else if (vSpawnOrderCommanderButtons[mLastSpawnOrderIdx].aClickMode == BattleUICommanderButton.ClickMode.Focus)
				++mLastSpawnOrderIdx;
		}

		bool lIsSpawnAvailable = aControlUser.IsSpawnAvailable() && mIsEnableSpawn;
		if (vSpawnPanelButtonAlarm.enabled != lIsSpawnAvailable)
		{
			vSpawnPanelButtonAlarm.ResetToBeginning();
			vSpawnPanelButtonAlarm.enabled = lIsSpawnAvailable;
		}
		vMpGauge.SetMp(mControlUser.aCurrentMp, true);
		vMpGauge.SetExtraMpRatio(mControlUser.aCurrentExtraMilliMp / 10); // 밀리 / 10은 백분율 값

		vCommanderInfo.UpdateFrame();
		vSkillButton.UpdateFrame();
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환 가능 여부를 지정합니다.
	public void EnableSpawn(bool pIsEnableSpawn)
	{
		if (mIsEnableSpawn == pIsEnableSpawn)
			return;
		mIsEnableSpawn = pIsEnableSpawn;

		//if (mIsEnableSpawn)
		//{
		//	UpdateSpawnPreviewButton();
		//}
		//else
		//{
		//	ResetSpawnPreviewButton();
		//	ShowSpawnCommanderPanel(false);
		//}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 버튼을 지정합니다.
	public void SetCommanderItemButton(int pSlotIdx, CommanderItem pCommanderItem)
	{
		//vSpawnButtons[pSlotIdx].SetCommanderItem(pCommanderItem);
		vCommanderButtons[pSlotIdx].SetCommanderItem(pCommanderItem);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환 버튼을 갱신합니다.
	public void UpdateCommanderItemButton(int pSlotIdx)
	{
		//vSpawnButtons[pSlotIdx].UpdateCommanderItemButtonState();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 버튼을 지정합니다.
	public void SetCommanderButton(int pSlotIdx, BattleCommander pCommander)
	{
		vCommanderButtons[pSlotIdx].SetCommander(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 미리보기를 초기화합니다.
	public void ResetSpawnPreviewButton()
	{
		for (int iButton = mCommanderButtonCount; iButton < vCommanderButtons.Length; iButton++)
		{
			if (mCommanderItemPreviewButtonCount <= 0)
				break;

			SetPreviewCommanderItemButton(iButton, null);
		}
		//mSortPreviewCommanderIndex = 0;
		if (mCommanderItemPreviewButtonCount > 0)
		{
			Debug.LogError("Do not empty Commander preview button slot. check that code! - " + this);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 미리보기를 업데이트합니다.
	public void UpdateSpawnPreviewButton()
	{
		if (!mIsEnableSpawn)
			return;

		//for (int iCommanderItem = mSortPreviewCommanderIndex; iCommanderItem < mSpawnSortButtonList.Count; iCommanderItem++)
		//{
		//	if (mCommanderButtonCount + mCommanderItemPreviewButtonCount >= vCommanderButtons.Length)
		//		break;

		//	if(mSpawnSortButtonList[iCommanderItem].IsSpawnAvailable() &&
		//		mSpawnSortButtonList[iCommanderItem].aSpawnMp <= mControlUser.aCurrentMp &&
		//		aControlUser.IsSpawnAvailableTroopNum(mSpawnSortButtonList[iCommanderItem]))
		//	{
		//		SetPreviewCommanderItemButton(mCommanderButtonCount + mCommanderItemPreviewButtonCount, mSpawnSortButtonList[iCommanderItem]);
		//		mSortPreviewCommanderIndex = iCommanderItem + 1;
		//	}
		//}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 비리보기 버튼을 지정합니다.
	public void SetPreviewCommanderItemButton(int pButtonIndex, CommanderItem pCommanderItem)
	{
		if (pCommanderItem == null)
		{
			mCommanderItemPreviewButtonCount--;
		}
		else
		{
			mCommanderItemPreviewButtonCount++;
		}
		int lSlotIdx = vCommanderButtonsPosIndex[pButtonIndex];
		vCommanderButtons[lSlotIdx].SetCommanderItem(pCommanderItem);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 위치의 slotid를 갱신합니다.
	public void UpdatePositionToSlotIdx(int pButtonPosIndex, int pSlotIdx)
	{
		vCommanderButtonsPosIndex[pButtonPosIndex] = pSlotIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 선택합니다.
	public void SelectCommander(BattleCommander pCommander)
	{
		//Debug.Log("SelectCommander() pCommander=" + pCommander);

		if (pCommander != null) // 선택할 지휘관이 있다면
		{
			vCommanderInfo.SetDisplayCommander(pCommander.aIsAlive ? pCommander : null); // 지휘관 정보를 자신의 것으로 변경
			vSkillButton.SetSkillInfo(pCommander.aIsAlive ? pCommander.aSkill : null);

			if (aOnSelectCommander != null)
				aOnSelectCommander(pCommander);
		}
		else // 선택할 지휘관이 없다면
		{
			if ((vCommanderInfo.aDisplayCommander != null) && // 정보를 보이고 있는 지휘관이 있고
				(vCommanderInfo.aDisplayCommander.aUser == mControlUser)) // 그 지휘관이 컨트롤 유저의 것일 경우에만
			{
				vCommanderInfo.SetDisplayCommander(null); // 지휘관 정보를 초기화
				vSkillButton.SetSkillInfo(null);
			}
		}

		UpdateButtonSelection();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관을 포커싱합니다.
	public void FocusCommander(int pCommanderId)
	{
		BattleCommander lFocusCommander = mControlUser.aCommanders[pCommanderId];
		if (lFocusCommander != null)
		{
			SelectCommander(lFocusCommander);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 선택 상태를 갱신합니다.
	public void UpdateButtonSelection()
	{
		for (int iButton = 0; iButton < vCommanderButtons.Length; ++iButton)
			vCommanderButtons[iButton].UpdateButtonSelection(vCommanderButtons[iButton].aCommander == vControlPanel.aFocusingCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 쿨타임을 활성화합니다.
	public void ActivateCommanderCoolTime(int pCommanderSlotIdx)
	{
		vCommanderButtons[pCommanderSlotIdx].ActivateCoolTime();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환 판넬을 토글합니다.
	public void OnActiveSpawnCommanderPanel()
	{
		ShowSpawnCommanderPanel(mIsSpawnPanelActive ^ true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환 판넬을 보입니다.
	public void ShowSpawnCommanderPanel(bool pIsShow)
	{
		if (mIsSpawnPanelActive == pIsShow)
			return;
		if (mTweenPosition != null)
			return;

		Vector3 lCloseOffset = new Vector3(0.0f, -305.0f, 0.0f);
		Vector3 lOpenOffset = new Vector3(0.0f, 120.0f, 0.0f);
		float lActiveDuration = 0.1f;
		float lStartDelay = 0.01f;
		if (pIsShow)
		{
			mTweenPosition = vSpawnButtonRoot.AddComponent<TweenPosition>();
			mTweenPosition.duration = lActiveDuration;
			mTweenPosition.from = lCloseOffset;
			mTweenPosition.to = lOpenOffset;
			mTweenPosition.delay = lStartDelay;
		}
		else
		{
			mTweenPosition = vSpawnButtonRoot.AddComponent<TweenPosition>();
			mTweenPosition.duration = lActiveDuration;
			mTweenPosition.from = lOpenOffset;
			mTweenPosition.to = lCloseOffset;
			mTweenPosition.delay = lStartDelay;
		}
		mTweenPosition.SetOnFinished(OnFinishedSpawnPanel);
		mIsSpawnPanelActive = pIsShow;
	}
	public void OnFinishedSpawnPanel()
	{
		if (mTweenPosition != null)
		{
			Destroy(mTweenPosition);
			mTweenPosition = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 랭크를 처리합니다..
	public void OnVeteranRankUp(BattleCommander pCommander)
	{
		vCommanderButtons[pCommander.aCommanderSlotIdx].ShowVeteranRank(pCommander.aVeterancyRank);
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 경험치를 처리합니다..
	public void OnVeteranExp(BattleCommander pCommander)
	{
		vCommanderButtons[pCommander.aCommanderSlotIdx].UpdateVeteranExp();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 툴팁을 엽니다.
	public void OpenCommanderToolTip(BattleUICommanderButton pCommanderButton)
	{
		vIsActionToolTip = true;

		vCommanderToopTip.transform.localPosition = new Vector3(pCommanderButton.aTooltipPivotPosition.x,
										pCommanderButton.aTooltipPivotPosition.y,
										pCommanderButton.aTooltipPivotPosition.z);

		vCommanderToopTip.SetActive(true);

		BattleUIToolTipAction lAction = vCommanderToopTip.GetComponent<BattleUIToolTipAction>();
		if (lAction == null)
			Debug.LogError("BattleUIToolTipAction Component is empty - " + vCommanderToopTip.name);

		Vector3 lStartPos = vCommanderToopTip.transform.localPosition;
		Vector3 lEndPos = new Vector3(lStartPos.x, lStartPos.y + 240.0f, lStartPos.z);

		lAction.OpenToolTip(lStartPos, lEndPos, 0.2f);

		BattleUICommanderToolTip vToolTipInfo = vCommanderToopTip.GetComponent<BattleUICommanderToolTip>();
		if (vToolTipInfo == null)
			Debug.LogError("BattleUICommanderToolTip component is empty - " + vCommanderToopTip.name);

		vToolTipInfo.SetCommanderInfo(pCommanderButton.aCommanderItem);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 툴팁을 닫습니다.
	public void CloseCommanderToolTip()
	{
		StopCoroutine("_OpenCommanderToopTip");

		BattleUIToolTipAction lAction = vCommanderToopTip.GetComponent<BattleUIToolTipAction>();
		if(lAction == null)
			Debug.LogError("BattleUIToolTipAction Component is empty - " + vCommanderToopTip.name);

		vCommanderToopTip.SetActive(false);

		lAction.CloseToolTip();
		vIsActionToolTip = false; 
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 툴팁을 엽니다.
	public void OpenSkillToolTip(BattleUISkillButton pSkillButton)
	{
		if (vIsActionToolTip)
			return;

		vIsActionToolTip = true;

		vSkillToolTip.transform.localPosition = new Vector3(pSkillButton.aTooltipPivotPosition.x,
										pSkillButton.aTooltipPivotPosition.y,
										pSkillButton.aTooltipPivotPosition.z);


		vSkillToolTip.SetActive(true);

		BattleUIToolTipAction lAction = vSkillToolTip.GetComponent<BattleUIToolTipAction>();
		if (lAction == null)
			Debug.LogError("BattleUIToolTipAction Component is empty - " + vSkillToolTip.name);

		Vector3 lStartPos = vSkillToolTip.transform.localPosition;
		Vector3 lEndPos = new Vector3(lStartPos.x, lStartPos.y + 150.0f, lStartPos.z);

		lAction.OpenToolTip(lStartPos, lEndPos, 0.2f);

		//BattleUICommanderToolTip vToolTipInfo = vSkillToolTip.GetComponent<BattleUICommanderToolTip>();
		//if (vToolTipInfo == null)
		//	Debug.LogError("BattleUICommanderToolTip component is empty - " + vSkillToolTip.name);

		// vToolTipInfo.SetCommanderInfo(pCommanderButton.aCommanderItem);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 툴팁을 닫습니다.
	public void CloseSkillToolTip()
	{
		StopCoroutine("_OpenSkillToopTip");

		if (!vIsActionToolTip)
			return;

		BattleUIToolTipAction lAction = vCommanderToopTip.GetComponent<BattleUIToolTipAction>();
		if (lAction == null)
			Debug.LogError("BattleUIToolTipAction Component is empty - " + vCommanderToopTip.name);

		vSkillToolTip.SetActive(false);

		lAction.CloseToolTip();
		vIsActionToolTip = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// Commander의 갯수를 업데이트합니다.
	public void UpdateCommanderPopCount()
	{
		vCommanderPopCountLabel.text = String.Format("{0} / {1}", mControlUser.aCurrentTroopCount, mControlUser.aMaxTroopCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소환시 버튼 위치를 업데이트합니다.
	public void UpdateButtonUISpawnCommander(BattleUICommanderButton pCommanderButton)
	{
		int lOriginSpawnButtonIndex = pCommanderButton.vButtonIndex;
		pCommanderButton.UpdateButtonUIPosition(mCommanderButtonPosition[lOriginSpawnButtonIndex], mCommanderButtonPosition[mLastSpawnButtonIdx], mLastSpawnButtonIdx);
		for (int iButton = mLastSpawnButtonIdx; iButton < lOriginSpawnButtonIndex; iButton++)
		{
			int lChangeSlotIdx = vCommanderButtonsPosIndex[iButton];
			vCommanderButtons[lChangeSlotIdx].UpdateButtonUIPosition(mCommanderButtonPosition[iButton], mCommanderButtonPosition[iButton + 1], iButton + 1);
		}
		mLastSpawnButtonIdx++;
		_RefreshButtonIndex();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소멸시 버튼 위치를 업데이트합니다.
	public void UpdateButtonUIDestroyCommander(BattleUICommanderButton pCommanderButton)
	{
		int lOriginDestroyButtonIndex = pCommanderButton.vButtonIndex;
		int lLastPositionIndex = vCommanderButtons.Length - 1;
		pCommanderButton.UpdateButtonUIPosition(mCommanderButtonPosition[lOriginDestroyButtonIndex], mCommanderButtonPosition[lLastPositionIndex], lLastPositionIndex);
		for (int iButton = lOriginDestroyButtonIndex+1; iButton <= lLastPositionIndex; iButton++)
		{
			int lChangeSlotIdx = vCommanderButtonsPosIndex[iButton];
			vCommanderButtons[lChangeSlotIdx].UpdateButtonUIPosition(mCommanderButtonPosition[iButton], mCommanderButtonPosition[iButton - 1], iButton - 1);
		}
		mLastSpawnButtonIdx--;
		_RefreshButtonIndex();
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 관련 취소 영역을 업데이트합니다.
	public void SetActiveDragCancelArea(bool pIsActive)
	{
		vCancelDragArea.gameObject.SetActive(pIsActive);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 인덱스를 재갱신합니다.
	private void _RefreshButtonIndex()
	{
		for (int iButton = 0; iButton < vCommanderButtons.Length; iButton++)
		{
			vCommanderButtonsPosIndex[vCommanderButtons[iButton].vButtonIndex] = vCommanderButtons[iButton].vSlotIdx;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleUser mControlUser;
	//private List<CommanderItem> mSpawnSortButtonList;
	private TweenPosition mTweenPosition;
	private bool mIsEnableSpawn;
	private bool mIsSpawnPanelActive = false;
	private Vector3[] mCommanderButtonPosition;
	private int mCommanderButtonCount;
	private int mCommanderItemPreviewButtonCount;
	//private int mSortPreviewCommanderIndex; // 미리보기 부대를 어디까지 표시했는지 확인하는 용도로 이용합니다.

	private int mLastSpawnButtonIdx; // 현재 부대 소환창에서 몇번째 Button index까지 소환이 됐는지 확인하는 용도로 이용합니다.
	private int mLastSpawnOrderIdx;
}
