using UnityEngine;
using System.Collections;
using System;

public class BattleUIAutoToggleButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public int vUserIdx = -1;
	public BattleUser.ControlType vControlType;
	
	public UILabel vButtonLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		gameObject.SetActive(BattlePlay.main.aUsers[vUserIdx] != null);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		mButton = GetComponent<UIButton>();
		if (mButton == null)
			Debug.LogError("can't find UIButton - " + this);

		if (vButtonLabel == null)
			Debug.LogError("vButtonLabel is empty - " + this);

		_UpdateButtonState();
	}
// 	//-------------------------------------------------------------------------------------------------------
// 	// 갱신 콜백
// 	void Update()
// 	{
// 		if (vControlType != BattlePlay.main.aUsers[vUserIdx].aControlType)
// 			_UpdateButtonState();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		switch (BattlePlay.main.aUsers[vUserIdx].aControlType)
		{
		case BattleUser.ControlType.Auto:
			BattlePlay.main.vUserControlTypes[vUserIdx] = BattleUser.ControlType.Manual;
			break;
		case BattleUser.ControlType.Manual:
			BattlePlay.main.vUserControlTypes[vUserIdx] = BattleUser.ControlType.Auto;
			break;
		}
		_UpdateButtonState();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 상태를 갱신합니다.
	private void _UpdateButtonState()
	{
		vControlType = BattlePlay.main.aUsers[vUserIdx].aControlType;
		vButtonLabel.text = vControlType.ToString();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIButton mButton;
}
