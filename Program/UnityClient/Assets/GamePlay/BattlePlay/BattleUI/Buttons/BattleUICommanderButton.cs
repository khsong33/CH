using UnityEngine;
using System.Collections;
using System;

public class BattleUICommanderButton : BattleUIDraggableButton
{
	public enum ClickMode
	{
		Spawn,
		Focus,

		None,
	}

	private const float cHpSliderUpdateDelayMin = 1.0f;
	private const float cHpSliderUpdateDelayMax = 1.2f;

	private const float cFlushAnimationTime = 0.5f;
	private const float cScrollAnimationTime = 0.4f;

	private static readonly String sDragColliderName = "Commander Drag Object";

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vSlotIdx = 0; // 고정 SLotId 변하지 않습니다.
	public int vButtonIndex = 0; // 위치에 따라 변경 가능한 버튼 인덱스

	public GameObject vSelectObjectGroup;
	public float vSelectObjectPositionYOffset;

	public UITexture vCommanderIconTexture;
	public UILabel vCommanderNameLabel;
	public UISprite[] vRankIconSprites;
	public UILabel vRankLabel;
	public UISprite vSpawnMpSprite;
	public UILabel vSpawnMpLabel;
	public UISprite vCoolTimeSprite;
	public UILabel vCoolTimeLabel;
	public String vCoolTimeUnit = "s";
	public UISprite vSelectionFrameSprite;
	public UISlider vHpSlider;
	public UILabel vCommanderNumLabel;
	public UILabel vSupplementMpLabel;

	public GameObject vWaitObject;
	public GameObject vSpawnedObject;
	public GameObject vRetreatObject;
	public GameObject vSpawnAvailableObject;
	public GameObject vSupplementAvailableObject;

	public GameObject vCommanderStateObject;
	public UISprite vCommanderAlarmSprite;

	public bool vIsCommanderPanelButton;

	public BattleUITroopCountInfo[] vTroopKindCountInfo;

	public GameObject vVeteranObject;
	public UISprite vVeteranRankSprite;
	public UISlider vVeteranExpSlider;

	public float vAlarmActiveTimeSec = 5.0f;

	public GameObject vSkillObject;
	public UITexture vSkillStateIcon;

	public UISprite vRankColorSprite;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override String aDragColliderName
	{
		get { return sDragColliderName; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override bool aIsDraggable
	{
		get { return mIsDragSpawn; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override bool aIsPressable
	{
		get { return ((mCommander != null) || (mCommanderItem != null)); }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override BattleUIUserControl aUserControl
	{
		get { return mUserControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override StageTouchControl.DragMode aDragMode
	{
		get { return (mCommander == null) ? StageTouchControl.DragMode.SpawnDrag : StageTouchControl.DragMode.MoveDrag; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override StageDragCursor.OnCheckValidPositionDelegate aOnCheckValidPositionDelegate
	{
		get { return _OnCheckValidPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관
	public BattleCommander aCommander
	{
		get { return mCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템
	public CommanderItem aCommanderItem
	{
		get { return mCommanderItem; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 활성화 여부
	public bool aIsButtonEnabled
	{
		get { return mButton.isEnabled; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 모드
	public ClickMode aClickMode
	{
		get { return mClickMode; }
	}
	//-------------------------------------------------------------------------------------------------------
	// Transform
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// ToolTip 중심 위치
	public Vector3 aTooltipPivotPosition
	{
		get { return aTransform.localPosition + aTransform.parent.localPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		OnAwake();

		if (vSelectObjectGroup == null)
			Debug.LogError("vSelectObjectGroup is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if (vRankIconSprites.Length != (CommanderSpec.cRankCount - 1))
			Debug.LogError("vRankIconSprites.Length mismatch - " + this);
		if(vRankLabel == null)
			Debug.LogError("vRankLabel is empty - " + this);
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			if (vRankIconSprites[iSprite] == null)
				Debug.LogError("vRankIconSprites is empty - " + this);
		if (vSpawnMpSprite == null)
			Debug.LogError("vSpawnMpSprite is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCoolTimeSprite == null)
			Debug.LogError("vCoolTimeSprite is empty - " + this);
		if (vCoolTimeLabel == null)
			Debug.LogError("vCoolTimeLabel is empty - " + this);
		if (vSelectionFrameSprite == null)
			Debug.LogError("vSelectionFrameSprite is empty - " + this);
		if (vHpSlider == null)
			Debug.LogError("vHpSlider is empty - " + this);
		if (vCommanderNumLabel == null)
			Debug.LogError("vCommanderNumLabel is empty - " + this);
		if (vSpawnAvailableObject == null)
			Debug.LogError("vSpawnAvailableObject is empty - " + this);
		if (vVeteranRankSprite == null)
			Debug.LogError("vVeteranRankSprite is empty - " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);

		mTransform = transform;
		mButton = GetComponent<UIButton>();
		if (mButton == null)
			Debug.Log("can't find UIButton - " + this);

		mCooldownTimer = 0.0f;

		mIconTextureSize = new Vector2(vCommanderIconTexture.width, vCommanderIconTexture.height);

		mIsCommanderAlarmActive = new bool[(int)StageCommander.CommanderAlarm.Count];
		mIsTimeCheckAlarm = false;

		mIsDragSpawn = true;
		//mTooltipPivotPosition = aTransform.localPosition + aTransform.parent.localPosition;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void OnPressUpAction()
	{
		if (!mIsDragSpawn)
			return;

		if (mCommander == null)
			BattlePlay.main.vStage.vStageTouchControl.HideDragCursorAndSpawn(mControlUser, mCommanderItem);
		else
			BattlePlay.main.vStage.vStageTouchControl.HideDragCursorAndMove(mCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void OnClickAction()
	{
		switch (mClickMode)
		{
		case ClickMode.Focus:
			{
				BattlePlay.main.vStage.vStageTouchControl.FocusCommander(
					mCommander,
					true,
					BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
					BattlePlay.main.vControlPanel.vDefaultFocusingDelay); // 지휘관을 포커싱하고
			}
			break;
		case ClickMode.Spawn:
			{
				BattlePlay.main.vStage.vStageTouchControl.SpawnCommander(
					mControlUser,
					mCommanderItem,
					mControlUser.aHeadquarter.aCoord);
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void ShowTooltip(bool pIsShow)
	{
		if (pIsShow)
		{
			if (!mUserControl.vIsActionToolTip)
				mUserControl.OpenCommanderToolTip(this);
		}
		else
		{
			if (mUserControl.vIsActionToolTip)
				mUserControl.CloseCommanderToolTip();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 컨트롤에 연결합니다.
	public void SetUserControl(BattleUIUserControl pUserControl)
	{
		mUserControl = pUserControl;
		mControlUser = pUserControl.aControlUser;

		SetCommander(null);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 초기화합니다.
	public void SetCommanderItem(CommanderItem pCommanderItem)
	{
		mCommanderItem = pCommanderItem;
		//bool lIsValidButton = ((mCommanderItem != null) && mCommanderItem.IsSpawnAvailable());
		bool lIsValidButton = (mCommanderItem.mCommanderSpec != null);

		vCommanderIconTexture.gameObject.SetActive(lIsValidButton);
		int lRankSpriteIndex = lIsValidButton ? (mCommanderItem.mCommanderExpRank - 1) : -1;
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			vRankIconSprites[iSprite].gameObject.SetActive(iSprite == lRankSpriteIndex);

		vCommanderNameLabel.gameObject.SetActive(lIsValidButton);
		if (mCommanderItem.mCommanderSpec != null)
			vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		vRankLabel.text = mCommanderItem.mCommanderExpRank.ToString();

		if (mCommanderItem != null)
			UpdateCommanderItemButtonState();

		if (lIsValidButton)
		{
			mSpawnMp = mCommanderItem.aSpawnMp;
			mSpawnCoolTime = mCommanderItem.mTroopSet.mSpawnCoolTime * 0.001f;

			vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();
			vSpawnMpLabel.text = mSpawnMp.ToString();

// 			mButton.defaultColor = BattleConfig.get.mRankColors[mCommanderItem.mCommanderRank];
// 			mButton.hover = mButton.defaultColor;

			int lOpenIndex = pCommanderItem.mTroopSet.mTroopKindCount - 1;
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				if (iKind == lOpenIndex)
				{
					vTroopKindCountInfo[iKind].gameObject.SetActive(true);
					mCurrentTroopKindCountInfo = vTroopKindCountInfo[iKind];
				}
				else
				{
					vTroopKindCountInfo[iKind].gameObject.SetActive(false);
				}
			}
			mCurrentTroopKindCountInfo.SetTroopCountInfo(pCommanderItem.mTroopSet);
		}
		else
		{
			vSpawnAvailableObject.SetActive(false); // 미리보기시에 없어지는 행동 설정
			vSpawnMpSprite.gameObject.SetActive(false);
			vSpawnMpLabel.gameObject.SetActive(false);
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
				mCurrentTroopKindCountInfo = null;
			}
		}

		mIsUpdatingHpSlider = false;

		_EnableButton(lIsValidButton);

		if (mCommanderItem != null)
			SetClickMode(ClickMode.Spawn);

		vSupplementAvailableObject.SetActive(false);
		_ClearAlarm();

		// 소환 아이템인 경우 veteran rank mark가 없도록 합니다.
		ShowVeteranRank(0);
		UpdateVeteranExp();
		
		// 랭크 색을 표시합니다.
		if(mCommanderItem.mCommanderSpec != null)
			vRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태에 따른 버튼 변경
	public void UpdateCommanderItemButtonState()
	{
		if (mCommanderItem.mCommanderItemState == CommanderItemState.Wait)
		{
			vWaitObject.SetActive(true);
			vSpawnedObject.SetActive(false);
			vRetreatObject.SetActive(false);
			vSpawnMpSprite.gameObject.SetActive(true);
			vSpawnMpLabel.gameObject.SetActive(true);
		}
		else if (mCommanderItem.mCommanderItemState == CommanderItemState.Spawned)
		{
			vWaitObject.SetActive(false);
			vSpawnedObject.SetActive(true);
			vRetreatObject.SetActive(false);
			vSpawnMpSprite.gameObject.SetActive(false);
			vSpawnMpLabel.gameObject.SetActive(false);
		}
		else if (mCommanderItem.mCommanderItemState == CommanderItemState.Retreat)
		{
			vWaitObject.SetActive(false);
			vSpawnedObject.SetActive(false);
			vRetreatObject.SetActive(true);
			vSpawnMpSprite.gameObject.SetActive(false);
			vSpawnMpLabel.gameObject.SetActive(false);
		}
		//vSelectionFrameSprite.gameObject.SetActive(false);
		_SetSelectCommanderButton(false);
		vHpSlider.gameObject.SetActive(false);
		vSpawnAvailableObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 정보를 초기화합니다.
	public void SetCommander(BattleCommander pCommander)
	{
		if (mCommander != null && pCommander == null)
		{
			mCommander.aSkill.aOnStartSkillDelegate -= _OnStartSkill;
			mCommander.aSkill.aOnStopSkillDelegate -= _OnStopSkill;
		}
		mCommander = pCommander;
		if (pCommander == null)
			mCommanderItem = null;
		else
			mCommanderItem = pCommander.aCommanderItem;

		bool lIsValidButton = (mCommander != null);

		vCommanderIconTexture.gameObject.SetActive(lIsValidButton);
		int lRankSpriteIndex = lIsValidButton ? (mCommander.aCommanderItem.mCommanderExpRank - 1) : -1;
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			vRankIconSprites[iSprite].gameObject.SetActive(iSprite == lRankSpriteIndex);
		vSpawnMpSprite.gameObject.SetActive(lIsValidButton);
		vSpawnMpLabel.gameObject.SetActive(lIsValidButton);
		//vSelectionFrameSprite.gameObject.SetActive(false);
		_SetSelectCommanderButton(false);
		vHpSlider.gameObject.SetActive(false);
		vSpawnAvailableObject.SetActive(false);

		if (lIsValidButton)
		{
			mSpawnMp = mCommander.aCommanderItem.aSpawnMp;
			mSpawnCoolTime = mCommander.aCommanderItem.mTroopSet.mSpawnCoolTime * 0.001f;

			vCommanderIconTexture.mainTexture = mCommander.aCommanderSpec.GetIconTexture();
			vSpawnMpLabel.text = mSpawnMp.ToString();

// 			mButton.defaultColor = BattleConfig.get.mRankColors[mCommander.aCommanderItem.mCommanderRank];
// 			mButton.hover = mButton.defaultColor;

			//vCommanderNumLabel.text = pCommander.aCommanderNumber.ToString();

			mCommander.aOnDestroyedTroopDelegate += _OnDestroyedCommanderTroop;
			mCommander.aOnDestroyingDelegate += _OnDestroyingCommander;
			mCommander.aOnRecruitTroopsDelegate += _OnRecruitTroop;


			int lOpenIndex = pCommander.aTroopSet.mTroopKindCount - 1;
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				if (iKind == lOpenIndex)
				{
					vTroopKindCountInfo[iKind].gameObject.SetActive(true);
					mCurrentTroopKindCountInfo = vTroopKindCountInfo[iKind];
				}
				else
				{
					vTroopKindCountInfo[iKind].gameObject.SetActive(false);
				}
			}
			mCurrentTroopKindCountInfo.SetTroopCountInfo(pCommander.aTroopSet);

			mCommander.aSkill.aOnStartSkillDelegate += _OnStartSkill;
			mCommander.aSkill.aOnStopSkillDelegate += _OnStopSkill;

			mUserControl.UpdateButtonUISpawnCommander(this);
		}
		else
		{
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
				mCurrentTroopKindCountInfo = null;
			}
			// commander가 사라질 경우 베테랑 마크도 사라지게합니다.
			ShowVeteranRank(0);
		}
		UpdateVeteranExp();
		mIsUpdatingHpSlider = false;

		_EnableButton(lIsValidButton);

		if (mCommander != null)
			SetClickMode(ClickMode.Focus);

		vSupplementAvailableObject.SetActive(false);
		_ClearAlarm();
		
		// 스킬 사용 상태를 초기화시킵니다.
		vSkillObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 활성화 여부를 갱신합니다.
	public void UpdateActivation()
	{
		switch (mClickMode)
		{
			case ClickMode.Spawn:
				{
					if (mCommanderItem == null)
						break; // 활성화 된 적 없음

					bool lIsSpawnable = (
						(mSpawnMp <= mControlUser.aCurrentMp) &&
						(mCooldownTimer <= 0.0f) &&
						mCommanderItem.IsSpawnAvailable() &&
						mControlUser.aUserInfo.IsSpawnAvailableSlot() &&
						mControlUser.IsSpawnAvailableTroopNum(mCommanderItem));
					if (vSpawnAvailableObject.activeInHierarchy != lIsSpawnable)
					{
						_UpdateSpawnableState(lIsSpawnable);
					}
					mIsDragSpawn = lIsSpawnable;
				}
				break;
			case ClickMode.Focus:
				{
					if (mCommander == null)
						break; // 활성화 된 적 없음

					if (mButton.isEnabled != mCommander.aIsAlive)
						_EnableButton(mCommander.aIsAlive);

					mIsDragSpawn = true;
				}
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 알람 시간을 갱신합니다.
	public void UpdateAlarm()
	{
		if (!mIsTimeCheckAlarm)
			return;

		if (vAlarmActiveTimeSec <= mAlarmActiveCurrentTime)
		{
			_ClearAlarm();
		}
		else
		{
			mAlarmActiveCurrentTime += Time.deltaTime;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 쿨타임을 돌립니다.
	public void ActivateCoolTime()
	{
		// 쿨타임 갱신을 시작합니다.
		vSpawnMpSprite.gameObject.SetActive(true);
		vSpawnMpLabel.gameObject.SetActive(true);
		vHpSlider.gameObject.SetActive(false);

		mCooldownTimer = mSpawnCoolTime;
		StartCoroutine(_ShowCoolTime());
	}
	//-------------------------------------------------------------------------------------------------------
	// 스폰 모드 여부를 지정합니다.
	public void SetClickMode(ClickMode pClickMode)
	{
		mClickMode = pClickMode;

		bool lIsSpawned = (mCommander != null) ? mCommander.aIsAlive : false;
		if (mCommander != null)
		{
			vSpawnMpSprite.gameObject.SetActive(!lIsSpawned);
			vSpawnMpLabel.gameObject.SetActive(!lIsSpawned);
			vHpSlider.gameObject.SetActive(lIsSpawned);
		}

		if (lIsSpawned)
		{
			StartCoroutine(_UpdateHpSlider());
		}
		else
		{
			mIsUpdatingHpSlider = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 선택 상태를 갱신합니다.
	public void UpdateButtonSelection(bool pIsFocusing)
	{
		//vSelectionFrameSprite.gameObject.SetActive((mCommander != null) && mCommander.aIsAlive && pIsFocusing);
		_SetSelectCommanderButton((mCommander != null) && mCommander.aIsAlive && pIsFocusing);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 이동을 시작합니다.
	public void UpdateButtonUIPosition(Vector3 pFrom, Vector3 pTo, int pMoveButtonIndex)
	{
		//Debug.Log(String.Format("Update Button Index From {0} To {1}", vButtonIndex, pMoveButtonIndex));
		vButtonIndex = pMoveButtonIndex;

		StartCoroutine(_OnUpdateButtonUIPotiontion(pFrom, pTo, pMoveButtonIndex));
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 이동을 즉시 처리합니다.
	public void UpdateButtonUIPositionFlush(Vector3 pTo, int pMoveButtonIndex)
	{
		mTransform.localPosition = pTo;
		vButtonIndex = pMoveButtonIndex;

		if (vCommanderIconTexture != null)
			StartCoroutine(_OnUpdateButtonUIPositionFlush());
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태 알람을 표시합니다.
	public void SetStateAlarm(StageCommander.CommanderAlarm pAlarm, bool pIsActive, bool pIsTimeCheck)
	{
		StageCommander.CommanderAlarm lCurrentActiveAlarm = StageCommander.CommanderAlarm.Count;
		mIsCommanderAlarmActive[(int)pAlarm] = pIsActive;
		for (int iAlarm = 0; iAlarm < (int)StageCommander.CommanderAlarm.Count; iAlarm++)
		{
			if (mIsCommanderAlarmActive[iAlarm])
			{
				lCurrentActiveAlarm = (StageCommander.CommanderAlarm)iAlarm;
				break;
			}
		}
		switch (lCurrentActiveAlarm)
		{
			case StageCommander.CommanderAlarm.Battle:
				{
					_ShowAlarm("state_attack", pIsTimeCheck);
				}
				break;
			case StageCommander.CommanderAlarm.Moveing:
				{
					_ShowAlarm("state_move", pIsTimeCheck);
				}
				break;
			case StageCommander.CommanderAlarm.Conquer:
				{
					_ShowAlarm("state_capture", pIsTimeCheck);
				}
				break;
			default:
				{
					_ClearAlarm();
				}
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 랭크 정보를 출력합니다.
	public void ShowVeteranRank(int pRank)
	{
		if (mCommander == null)
		{
			vVeteranObject.SetActive(false);
		}

		if (pRank <= 0)
		{
			vVeteranObject.SetActive(false);
		}
		else
		{
			vVeteranObject.SetActive(true);

			DeckNation lTroopNation = (DeckNation)mCommander.aUserInfo.mBattleNation;
			vVeteranRankSprite.spriteName = String.Format("{0}_Veteran_{1}", lTroopNation.ToString(), pRank);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 랭크 경험치 정보를 출력합니다.
	public void UpdateVeteranExp()
	{
		if (mCommander == null)
		{
			vVeteranExpSlider.gameObject.SetActive(false);
			return;
		}

		vVeteranExpSlider.gameObject.SetActive(true);

		int lVeteranRank = aCommander.aVeterancyRank;
		int lVeteranExperience = aCommander.aVeterancyExperience;
		CommanderSpec lCommanderSpec = aCommander.aCommanderSpec;
		float lResultExperienceRatio = 0.0f;
		if (lVeteranRank <= 0)
			lResultExperienceRatio = (float)lVeteranExperience / lCommanderSpec.GetNextVeterancyExp(lVeteranRank);
		else if (lVeteranRank >= BattleConfig.get.mMaxVeterancyRank)
			lResultExperienceRatio = 1.0f;
		else
		{
			int lPreRankUpExp = lCommanderSpec.GetNextVeterancyExp(lVeteranRank - 1);
			int lCurrentRankUpExp = lCommanderSpec.GetNextVeterancyExp(lVeteranRank);
			lResultExperienceRatio = (float)(lVeteranExperience - lPreRankUpExp) / (lCurrentRankUpExp - lPreRankUpExp);
		}

		if (lResultExperienceRatio >= 1.0f)
			lResultExperienceRatio = 1.0f;

		vVeteranExpSlider.value = lResultExperienceRatio;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴]소환 처리 효과를 적용합니다.
	private IEnumerator _OnUpdateButtonUIPositionFlush()
	{
		float lFactor = 0.0f;
		float lCurrentTime = 0.0f;
		float lTextureWidth = mIconTextureSize.x;
		float lTextureHeight = mIconTextureSize.y;
		while (lFactor < 1.0f)
		{
			lFactor = lCurrentTime / cFlushAnimationTime;
			if (lFactor >= 1.0f)
				lFactor = 1.0f;

			float lTextureWidthSize = Mathf.Lerp(1.0f, lTextureWidth, lFactor);
			float lTextureHeightSize = Mathf.Lerp(1.0f, lTextureHeight, lFactor);

			vCommanderIconTexture.width = (int)lTextureWidthSize;
			vCommanderIconTexture.height = (int)lTextureHeightSize;

			lCurrentTime += Time.deltaTime;
			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴]버튼의 이동을 시작합니다.
	private IEnumerator _OnUpdateButtonUIPotiontion(Vector3 pFrom, Vector3 pTo, int pMoveButtonIndex)
	{
		yield return null;

		float lFactor = 0.0f;
		float lCurrentTime = 0.0f;
		while (lFactor < 1.0f)
		{
			lFactor = lCurrentTime / cScrollAnimationTime;
			if (lFactor >= 1.0f)
				lFactor = 1.0f;

			mTransform.localPosition = Vector3.Lerp(pFrom, pTo, lFactor);
			lCurrentTime += Time.deltaTime;
			yield return null;
		}

		mTransform.localPosition = pTo;

		BattleLuaUIComponent lLuaComponent = GetComponent<BattleLuaUIComponent>();
		if (lLuaComponent != null)
			lLuaComponent.GoLuaCallbackFunc();
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 활성화 여부를 지정합니다.
	private void _EnableButton(bool pIsEnabled)
	{
		mButton.isEnabled = false;
		mButton.isEnabled = pIsEnabled;

		//bool lIsShowCoolTime = !pIsEnabled && (mCooldownTimer > 0.0f);
		//vCoolTimeSprite.gameObject.SetActive(lIsShowCoolTime || !pIsEnabled);
		//vCoolTimeLabel.gameObject.SetActive(lIsShowCoolTime);
		//vSelectionFrameSprite.gameObject.SetActive(false);
		_SetSelectCommanderButton(false);

		if (!pIsEnabled)
			vCoolTimeSprite.fillAmount = 1.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 쿨 타임을 보입니다.
	private IEnumerator _ShowCoolTime()
	{
		//Debug.Log("_ShowCoolTime()");

		_EnableButton(false);

		while (mCooldownTimer > 0.0f)
		{
			vCoolTimeSprite.fillAmount = (float)mCooldownTimer / mSpawnCoolTime;

			vCoolTimeLabel.text = String.Format("{0}{1}", Mathf.CeilToInt((float)mCooldownTimer), vCoolTimeUnit);

			mCooldownTimer -= Time.deltaTime;
			yield return null;
		}

		_EnableButton(mSpawnMp <= mControlUser.aCurrentMp);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] HP 슬라이더를 갱신합니다.
	private IEnumerator _UpdateHpSlider()
	{
		mIsUpdatingHpSlider = true;
		do
		{
			vHpSlider.value = (float)mCommander.aCurTotalMilliHp / mCommander.aMaxTotalMilliHp;

			yield return new WaitForSeconds(UnityEngine.Random.Range(cHpSliderUpdateDelayMin, cHpSliderUpdateDelayMax));

		} while (mIsUpdatingHpSlider);
	}
	//-------------------------------------------------------------------------------------------------------
	// 소환 가능 상태를 표시합니다.
	private void _UpdateSpawnableState(bool pActive)
	{
		vSpawnAvailableObject.SetActive(pActive);
		vWaitObject.SetActive(!pActive);
		//vSpawnMpSprite.gameObject.SetActive(!pActive);
		//vSpawnMpLabel.gameObject.SetActive(!pActive);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 재소환시 콜백입니다.
	private void _OnRecruitTroop(BattleCommander pCommander)
	{
		//mUserControl.ResetSpawnPreviewButton();
		//mUserControl.UpdateSpawnPreviewButton();
		if (mCurrentTroopKindCountInfo != null)
			mCurrentTroopKindCountInfo.UpdateTroopCountInfo(pCommander.aTroopKindCount);

		vSupplementAvailableObject.SetActive(false);
		pCommander.aLeaderTroop.aStageTroop.OnUpdateRecruitState(); // 부대 마크에도 반영
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소멸시 콜백입니다.
	private void _OnDestroyingCommander(BattleCommander pCommander)
	{
		mUserControl.UpdateButtonUIDestroyCommander(this);

		pCommander.aOnRecruitTroopsDelegate -= _OnRecruitTroop;
		pCommander.aOnDestroyingDelegate -= _OnDestroyingCommander;
		pCommander.aOnDestroyedTroopDelegate -= _OnDestroyedCommanderTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 소속 부대 소멸시 콜백입니다.
	private void _OnDestroyedCommanderTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		if (mCurrentTroopKindCountInfo != null)
			mCurrentTroopKindCountInfo.UpdateTroopCountInfo(pCommander.aTroopKindCount);

		vSupplementAvailableObject.SetActive(true);
		vSupplementMpLabel.text = pCommander.aRecruitMp.ToString();

		if (pCommander.aLeaderTroop != null)
			pCommander.aLeaderTroop.aStageTroop.OnUpdateRecruitState(); // 부대 마크에도 반영

		_ClearAlarm();
	}
	//-------------------------------------------------------------------------------------------------------
	// 알람을 모두 숨깁니다.
	private void _ClearAlarm()
	{
		vCommanderStateObject.SetActive(false);
		for (int iAlarm = 0; iAlarm < (int)StageCommander.CommanderAlarm.Count; iAlarm++)
		{
			mIsCommanderAlarmActive[iAlarm] = false;
		}
		mAlarmActiveCurrentTime = 0.0f;
		mIsTimeCheckAlarm = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 알람을 보여줍니다.
	private void _ShowAlarm(String pSpriteName, bool pIsTimeCheckAlarm)
	{
		vCommanderStateObject.SetActive(true);
		vCommanderAlarmSprite.spriteName = pSpriteName;
		mAlarmActiveCurrentTime = 0.0f;
		mIsTimeCheckAlarm = pIsTimeCheckAlarm;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 시작합니다.
	private void _OnStartSkill(BattleCommander pCommander)
	{
		if(mCommander == null)
			return;

		vSkillObject.SetActive(true);
		vSkillStateIcon.mainTexture = mCommander.aSkill.aSkillSpec.GetStateIconTexture();
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 정지합니다.
	private void _OnStopSkill(BattleCommander pCommander)
	{
		if (mCommander == null)
			return;

		vSkillObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 선택시 표시합니다.
	private void _SetSelectCommanderButton(bool pIsSelected)
	{
		if (pIsSelected)
		{
			vSelectObjectGroup.transform.localPosition = new Vector3(0.0f, vSelectObjectPositionYOffset, 0.0f);
		}
		else
		{
			vSelectObjectGroup.transform.localPosition = Vector3.zero;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// StageDragCursor.OnCheckValidPositionDelegate
	public bool _OnCheckValidPosition(Vector3 pPosition)
	{
		if (BattlePlay.main.aBattleGrid.Contains(Coord2.ToCoord(pPosition)))
			return true;
		else
			return false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private UIButton mButton;

	private BattleUIUserControl mUserControl;
	private BattleUser mControlUser;
	private CommanderItem mCommanderItem;
	private BattleCommander mCommander;
	private int mSpawnMp;
	private float mSpawnCoolTime;
	private float mCooldownTimer;
	private ClickMode mClickMode;
	private bool mIsUpdatingHpSlider;

	private BattleUITroopCountInfo mCurrentTroopKindCountInfo;

	private Vector2 mIconTextureSize;

	private bool[] mIsCommanderAlarmActive;
	private bool mIsTimeCheckAlarm;
	private float mAlarmActiveCurrentTime;

	private bool mIsDragSpawn;
	private bool mIsDragSpawnActive;
	//private Vector3 mTooltipPivotPosition;
}
