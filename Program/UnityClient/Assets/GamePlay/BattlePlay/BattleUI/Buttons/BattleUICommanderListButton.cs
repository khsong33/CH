﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUICommanderListButton : InfiniteItemBehavior
{
	public enum ClickMode
	{
		Spawn,
		Focus,

		None,
	}

	private float cHpSliderUpdateDelayMin = 1.0f;
	private float cHpSliderUpdateDelayMax = 1.2f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vButtonId = 0;

	public UITexture vCommanderIconTexture;
	public UISprite[] vRankIconSprites;
	public UISprite vSpawnMpSprite;
	public UILabel vSpawnMpLabel;
	public UISprite vCoolTimeSprite;
	public UILabel vCoolTimeLabel;
	public String vCoolTimeUnit = "s";
	public UISprite vSelectionFrameSprite;
	public UISlider vHpSlider;

	public GameObject vSpawnAvailableObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 속한 유저 컨트롤
	public BattleUIUserControl aUserControl
	{
		get { return mUserControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관
	public BattleCommander aCommander
	{
		get { return mCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 활성화 여부
	public bool aIsButtonEnabled
	{
		get { return mButton.isEnabled; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 모드
	public ClickMode aClickMode
	{
		get { return mClickMode; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vRankIconSprites.Length != (CommanderSpec.cRankCount - 1))
			Debug.LogError("vRankIconSprites.Length mismatch - " + this);
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			if (vRankIconSprites[iSprite] == null)
				Debug.LogError("vRankIconSprites is empty - " + this);
		if (vSpawnMpSprite == null)
			Debug.LogError("vSpawnMpSprite is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCoolTimeSprite == null)
			Debug.LogError("vCoolTimeSprite is empty - " + this);
		if (vCoolTimeLabel == null)
			Debug.LogError("vCoolTimeLabel is empty - " + this);
		if (vSelectionFrameSprite == null)
			Debug.LogError("vSelectionFrameSprite is empty - " + this);
		if (vHpSlider == null)
			Debug.LogError("vHpSlider is empty - " + this);

		if (vSpawnAvailableObject == null)
			Debug.LogError("vSpawnAvailableObject is empty - " + this);

		mButton = GetComponent<UIButton>();
		if (mButton == null)
			Debug.Log("can't find UIButton - " + this);

		mCooldownTimer = 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnClick()
	{
		switch (mClickMode)
		{
			case ClickMode.Focus:
				{
					BattlePlay.main.vStage.vStageTouchControl.FocusCommander(
						mCommander,
						true,
						BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
						BattlePlay.main.vControlPanel.vDefaultFocusingDelay); // 지휘관을 포커싱하고
				}
				break;
			case ClickMode.Spawn:
				{
					BattlePlay.main.vStage.vStageTouchControl.SpawnCommander(
						mControlUser,
						mCommanderItem,
						mControlUser.aHeadquarter.aCoord);
				}
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 입력합니다.
	public override void SetItemData(object pData)
	{
		CommanderItemListData lData = pData as CommanderItemListData;

		SetUserControl(lData.mControlUser);
		SetCommanderItem(lData.mCommanderItem);
	}

	//-------------------------------------------------------------------------------------------------------
	// 유저 컨트롤에 연결합니다.
	public void SetUserControl(BattleUIUserControl pUserControl)
	{
		if (mButton == null)
			mButton = GetComponent<UIButton>();

		mUserControl = pUserControl;
		mControlUser = pUserControl.aControlUser;

		SetCommander(null);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 초기화합니다.
	public void SetCommanderItem(CommanderItem pCommanderItem)
	{
		mCommanderItem = pCommanderItem;
		bool lIsValidButton = ((mCommanderItem != null) && mCommanderItem.IsSpawnAvailable());

		vCommanderIconTexture.gameObject.SetActive(lIsValidButton);
		int lRankSpriteIndex = lIsValidButton ? (mCommanderItem.mCommanderExpRank - 1) : -1;
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			vRankIconSprites[iSprite].gameObject.SetActive(iSprite == lRankSpriteIndex);
		vSpawnMpSprite.gameObject.SetActive(lIsValidButton);
		vSpawnMpLabel.gameObject.SetActive(lIsValidButton);
		vSelectionFrameSprite.gameObject.SetActive(false);
		vHpSlider.gameObject.SetActive(false);
		vSpawnAvailableObject.SetActive(false);

		if (lIsValidButton)
		{
			mSpawnMp = mCommanderItem.aSpawnMp;
			mSpawnCoolTime = mCommanderItem.mTroopSet.mSpawnCoolTime * 0.001f;

			vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();
			vSpawnMpLabel.text = mSpawnMp.ToString();

			mButton.defaultColor = BattleConfig.get.mRankColors[mCommanderItem.mCommanderExpRank];
			mButton.hover = mButton.defaultColor;
		}
		else
		{
			mButton.isEnabled = false;
		}

		mIsUpdatingHpSlider = false;

		_EnableButton(true);

		if (mCommanderItem != null)
			SetClickMode(ClickMode.Spawn);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 정보를 초기화합니다.
	public void SetCommander(BattleCommander pCommander)
	{
		mCommander = pCommander;
		bool lIsValidButton = (mCommander != null);

		vCommanderIconTexture.gameObject.SetActive(lIsValidButton);
		int lRankSpriteIndex = lIsValidButton ? (mCommander.aCommanderItem.mCommanderExpRank - 1) : -1;
		for (int iSprite = 0; iSprite < vRankIconSprites.Length; ++iSprite)
			vRankIconSprites[iSprite].gameObject.SetActive(iSprite == lRankSpriteIndex);
		vSpawnMpSprite.gameObject.SetActive(lIsValidButton);
		vSpawnMpLabel.gameObject.SetActive(lIsValidButton);
		vSelectionFrameSprite.gameObject.SetActive(false);
		vHpSlider.gameObject.SetActive(false);
		vSpawnAvailableObject.SetActive(false);

		if (lIsValidButton)
		{
			mSpawnMp = mCommander.aCommanderItem.aSpawnMp;
			mSpawnCoolTime = mCommander.aCommanderItem.mTroopSet.mSpawnCoolTime * 0.001f;

			vCommanderIconTexture.mainTexture = mCommander.aCommanderSpec.GetIconTexture();
			vSpawnMpLabel.text = mSpawnMp.ToString();

			mButton.defaultColor = BattleConfig.get.mRankColors[mCommander.aCommanderItem.mCommanderExpRank];
			mButton.hover = mButton.defaultColor;
		}
		else
		{
			mButton.isEnabled = false;
		}

		mIsUpdatingHpSlider = false;

		_EnableButton(false);

		if (mCommander != null)
			SetClickMode(ClickMode.Focus);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼의 활성화 여부를 갱신합니다.
	public void UpdateActivation()
	{
		switch (mClickMode)
		{
			case ClickMode.Spawn:
				{
					if (mCommanderItem == null)
						break; // 활성화 된 적 없음

					bool lIsSpawnable = (
						(mSpawnMp <= mControlUser.aCurrentMp) &&
						(mCooldownTimer <= 0.0f) &&
						mCommanderItem.IsSpawnAvailable() &&
						mControlUser.aUserInfo.IsSpawnAvailableSlot());
					if (vSpawnAvailableObject.activeInHierarchy != lIsSpawnable)
						vSpawnAvailableObject.SetActive(lIsSpawnable);
				}
				break;
			case ClickMode.Focus:
				{
					if (mCommander == null)
						break; // 활성화 된 적 없음

					if (mButton.isEnabled != mCommander.aIsAlive)
						_EnableButton(mCommander.aIsAlive);
				}
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 쿨타임을 돌립니다.
	public void ActivateCoolTime()
	{
		// 쿨타임 갱신을 시작합니다.
		vSpawnMpSprite.gameObject.SetActive(true);
		vSpawnMpLabel.gameObject.SetActive(true);
		vHpSlider.gameObject.SetActive(false);

		mCooldownTimer = mSpawnCoolTime;
		StartCoroutine(_ShowCoolTime());
	}
	//-------------------------------------------------------------------------------------------------------
	// 스폰 모드 여부를 지정합니다.
	public void SetClickMode(ClickMode pClickMode)
	{
		mClickMode = pClickMode;

		bool lIsSpawned = (mCommander != null) ? mCommander.aIsAlive : false;
		vSpawnMpSprite.gameObject.SetActive(!lIsSpawned);
		vSpawnMpLabel.gameObject.SetActive(!lIsSpawned);
		vHpSlider.gameObject.SetActive(lIsSpawned);

		if (lIsSpawned)
			StartCoroutine(_UpdateHpSlider());
		else
			mIsUpdatingHpSlider = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 선택 상태를 갱신합니다.
	public void UpdateButtonSelection(bool pIsFocusing)
	{
		vSelectionFrameSprite.gameObject.SetActive((mCommander != null) && mCommander.aIsAlive && pIsFocusing);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 활성화 여부를 지정합니다.
	private void _EnableButton(bool pIsEnabled)
	{
		mButton.isEnabled = pIsEnabled;

		bool lIsShowCoolTime = !pIsEnabled && (mCooldownTimer > 0.0f);
		vCoolTimeSprite.gameObject.SetActive(lIsShowCoolTime || !pIsEnabled);
		vCoolTimeLabel.gameObject.SetActive(lIsShowCoolTime);
		vSelectionFrameSprite.gameObject.SetActive(false);

		if (!pIsEnabled)
			vCoolTimeSprite.fillAmount = 1.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 쿨 타임을 보입니다.
	private IEnumerator _ShowCoolTime()
	{
		//Debug.Log("_ShowCoolTime()");

		_EnableButton(false);

		while (mCooldownTimer > 0.0f)
		{
			vCoolTimeSprite.fillAmount = (float)mCooldownTimer / mSpawnCoolTime;

			vCoolTimeLabel.text = String.Format("{0}{1}", Mathf.CeilToInt((float)mCooldownTimer), vCoolTimeUnit);

			mCooldownTimer -= Time.deltaTime;
			yield return null;
		}

		_EnableButton(mSpawnMp <= mControlUser.aCurrentMp);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] HP 슬라이더를 갱신합니다.
	private IEnumerator _UpdateHpSlider()
	{
		mIsUpdatingHpSlider = true;
		do
		{
			mCommander.UpdateTotalHp();
			//Debug.Log("mCommander.aCurTotalMilliHp=" + mCommander.aCurTotalMilliHp + " - " + this);
			vHpSlider.value = (float)mCommander.aCurTotalMilliHp / mCommander.aMaxTotalMilliHp;

			yield return new WaitForSeconds(UnityEngine.Random.Range(cHpSliderUpdateDelayMin, cHpSliderUpdateDelayMax));

		} while (mIsUpdatingHpSlider);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIButton mButton;

	private BattleUIUserControl mUserControl;
	private BattleUser mControlUser;
	private CommanderItem mCommanderItem;
	private BattleCommander mCommander;
	private int mSpawnMp;
	private float mSpawnCoolTime;
	private float mCooldownTimer;
	private ClickMode mClickMode;
	private bool mIsUpdatingHpSlider;
}

public class CommanderItemListData
{
	public CommanderItem mCommanderItem;
	public BattleUIUserControl mControlUser;
}