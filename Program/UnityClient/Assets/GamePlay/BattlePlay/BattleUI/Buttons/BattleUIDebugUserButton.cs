using UnityEngine;
using System.Collections;
using System;

public class BattleUIDebugUserButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public int vDebugUserIdx = -1;

	public UILabel vButtonLabel;
	public String vDisplayAlwaysText = "Display Always";
	public String vDisplayOnText = "Display On";
	public String vDisplayOffText = "Display Off";

	public BattleUIDebugUserButton[] vExclusiveRadioBussons;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vButtonLabel == null)
			Debug.LogError("vButtonLabel is empty - " + this);

		if (vDebugUserIdx >= 0)
			gameObject.SetActive(BattlePlay.main.aUsers[vDebugUserIdx] != null);
		else
			gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		UIButton lButton = GetComponent<UIButton>();
		if (lButton == null)
			Debug.LogError("can't find UIButton - " + this);

		lButton.isEnabled = (BattlePlay.main.aClientUser.aUserIdx != vDebugUserIdx);

		_UpdateButtonState();
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		BattleUser lCurrentDebugUser = BattlePlay.main.vControlPanel.vDebugUserControl.aControlUser;
		int lCurrentDebugUserIdx = (lCurrentDebugUser != null) ? lCurrentDebugUser.aUserIdx : -1;
		int lNewDebugUserIdx = (lCurrentDebugUserIdx != vDebugUserIdx) ? vDebugUserIdx : -1; // 토글
		BattlePlay.main.vControlPanel.SetDebugControlUser(lNewDebugUserIdx);

		_UpdateButtonState(lNewDebugUserIdx);
		for (int iButton = 0; iButton < vExclusiveRadioBussons.Length; ++iButton)
			vExclusiveRadioBussons[iButton]._UpdateButtonState(lNewDebugUserIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 상태를 갱신합니다.
	private void _UpdateButtonState()
	{
		BattleUser lCurrentDebugUser = BattlePlay.main.vControlPanel.vDebugUserControl.aControlUser;
		int lCurrentDebugUserIdx = (lCurrentDebugUser != null) ? lCurrentDebugUser.aUserIdx : -1;
		_UpdateButtonState(lCurrentDebugUserIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 상태를 갱신합니다.
	private void _UpdateButtonState(int pDisplayDebugUserIdx)
	{
		if (BattlePlay.main.aClientUser.aUserIdx == vDebugUserIdx)
			vButtonLabel.text = vDisplayAlwaysText;
		else
			vButtonLabel.text = (vDebugUserIdx == pDisplayDebugUserIdx) ? vDisplayOnText : vDisplayOffText;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
