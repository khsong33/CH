using UnityEngine;
using System.Collections;
using System;

public abstract class BattleUIDraggableButton : MonoBehaviour
{
	private const float cDragStartDelay = 0.3f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vDragCollider;
	public GameObject vDragMarkObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 하위 구현용 속성
	protected abstract String aDragColliderName { get; }
	protected abstract bool aIsDraggable { get; }
	protected abstract bool aIsPressable { get; }
	protected abstract BattleUIUserControl aUserControl { get; }
	protected abstract StageTouchControl.DragMode aDragMode { get; }
	protected abstract StageDragCursor.OnCheckValidPositionDelegate aOnCheckValidPositionDelegate { get; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	protected void OnAwake()
	{
		if (vDragCollider == null)
			Debug.LogError("vDragCollider is empty - " + this);
		if (vDragMarkObject == null)
			Debug.LogError("vDragMarkObject is empty - " + this);
		vDragMarkObject.SetActive(false);

		vDragCollider.name = aDragColliderName;
		vDragCollider.gameObject.SetActive(false);

		mIsDraggingActive = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnPress(bool pIsDown)
	{
		if (!aIsPressable)
			return;

		if (pIsDown)
		{
			aUserControl.vCancelDragArea.aOnCancelAreaEnter += OnCancelAreaEnter;
			aUserControl.vCancelDragArea.aOnCancelAreaExit += OnCancelAreaExit;

			//aUserControl.OpenCommanderToolTip(this);
			//vDragCollider.gameObject.SetActive(true);
			StartCoroutine("_OnPressDrag", UICamera.currentTouch.pos);
		}
		else
		{
			ShowTooltip(false);

			//aUserControl.CloseCommanderToolTip();
			aUserControl.SetActiveDragCancelArea(false);
			vDragCollider.gameObject.SetActive(false);

			if (mIsDraggingActive)
				OnPressUpAction();

			_ResetCollider();
			mIsDraggingActive = false;

			aUserControl.vCancelDragArea.aOnCancelAreaEnter -= OnCancelAreaEnter;
			aUserControl.vCancelDragArea.aOnCancelAreaExit -= OnCancelAreaExit;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnDrag(Vector2 pDelta)
	{
		if (!aIsDraggable)
			return;

		StopCoroutine("_OnPressDrag");

		BattlePlay.main.vStage.vStageTouchControl.MoveDragCursor(UICamera.currentTouch.pos, aOnCheckValidPositionDelegate);

		aUserControl.SetActiveDragCancelArea(true);
		vDragCollider.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnClick()
	{
		if (mIsDragStarted)
			return;

		StopCoroutine("_OnPressDrag");

		OnClickAction();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 떼어졌을 때의 처리를 합니다.
	protected abstract void OnPressUpAction();
	//-------------------------------------------------------------------------------------------------------
	// 클릭했을 때의 처리를 합니다.
	protected abstract void OnClickAction();
	//-------------------------------------------------------------------------------------------------------
	// 툴팁을 보입니다.
	protected abstract void ShowTooltip(bool pIsShow);
	//-------------------------------------------------------------------------------------------------------
	// 취소 영역에 들어갈 때 호출됩니다.
	protected virtual void OnCancelAreaEnter()
	{
		if (!aIsDraggable)
			return;

		BattlePlay.main.vStage.vStageTouchControl.HideDragCursor();
		mIsDraggingActive = false;
		vDragMarkObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 취소 영역에서 나올 때 들어갈 때 호출됩니다.
	protected virtual void OnCancelAreaExit()
	{
		if (!aIsDraggable)
			return;

		ShowTooltip(false);

		BattlePlay.main.vStage.vStageTouchControl.ShowDragCursor(aDragMode, aOnCheckValidPositionDelegate);

		mIsDraggingActive = true;
		vDragMarkObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 버튼을 누르고서 잠시 뒤에  드래그 상태로 전환시키는 처리를 합니다.
	private IEnumerator _OnPressDrag(Vector2 lCurrentTouchPos)
	{
		mIsDragStarted = false;

		yield return new WaitForSeconds(cDragStartDelay);

		mIsDragStarted = true;

		ShowTooltip(true);

		if (aIsDraggable)
		{
			BattlePlay.main.vStage.vStageTouchControl.MoveDragCursor(lCurrentTouchPos, aOnCheckValidPositionDelegate);

			vDragCollider.gameObject.SetActive(true);
			vDragMarkObject.SetActive(true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 충돌 박스를 초기화합니다.
	private void _ResetCollider()
	{
		vDragCollider.localPosition = Vector3.zero;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsDragStarted;
	private bool mIsDraggingActive;
}
