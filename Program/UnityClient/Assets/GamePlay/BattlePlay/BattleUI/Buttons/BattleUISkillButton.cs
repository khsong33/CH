using UnityEngine;
using System.Collections;
using System;

public class BattleUISkillButton : BattleUIDraggableButton
{
	private static readonly String sDragColliderName = "Drag Object";

	private delegate void OnFrameUpdateDelegate();

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattleUIUserControl vUserControl;

	public UITexture vSkillIconTexture;
	public UILabel vSkillNameLabel;
	public UISprite vReadyTimeSprite;
	public UILabel vReadyTimeLabel;
	public UISprite vUseTimeSprite;
	public UILabel vUseTimeLabel;
	public UISprite vCoolTimeSprite;
	public UILabel vCoolTimeLabel;

	public BattleUISkillToolTip vSkillToolTip;

	public GameObject vDragAvailbleObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override String aDragColliderName
	{
		get { return sDragColliderName; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override bool aIsDraggable
	{
		get { return ((mCurrentCommanderSkill != null) && (!mCurrentCommanderSkill.aIsStarted)); }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override bool aIsPressable
	{
		get { return ((mCurrentCommanderSkill != null) && !mCurrentCommanderSkill.aIsUseSkill); }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override BattleUIUserControl aUserControl
	{
		get { return mUserControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override StageTouchControl.DragMode aDragMode
	{
		get { return StageTouchControl.DragMode.SkillDrag; }
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 속성
	protected override StageDragCursor.OnCheckValidPositionDelegate aOnCheckValidPositionDelegate
	{
		get { return _OnCheckValidPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// ToolTip 중심 위치
	public Vector3 aTooltipPivotPosition
	{
		get { return mTooltipPivotPosition; }
	}
	//-------------------------------------------------------------------------------------------------------
	// Transform
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		OnAwake();

		if (vUserControl == null)
			Debug.LogError("vUserControl is empty - " + this);
		if (vSkillIconTexture == null)
			Debug.LogError("vSkillIconTexture is empty - " + this);
		if (vSkillNameLabel == null)
			Debug.LogError("vSkillNameLabel is empty - " + this);
		if (vReadyTimeSprite == null)
			Debug.LogError("vReadyTimeSprite is empty - " + this);
		if (vReadyTimeLabel == null)
			Debug.LogError("vReadyTimeLabel is empty - " + this);
		if (vUseTimeSprite == null)
			Debug.LogError("vUseTimeSprite is empty - " + this);
		if (vUseTimeLabel == null)
			Debug.LogError("vUseTimeLabel is empty - " + this);
		if (vCoolTimeSprite == null)
			Debug.LogError("vCoolTimeSprite is empty - " + this);
		if (vCoolTimeLabel == null)
			Debug.LogError("vCoolTimeLabel is empty - " + this);

		if (vDragAvailbleObject == null)
			Debug.LogError("vDragAvailbleObject is empty - " + this);
		vDragAvailbleObject.gameObject.SetActive(false);

		vReadyTimeSprite.gameObject.SetActive(true);
		vReadyTimeLabel.gameObject.SetActive(false);
		vUseTimeSprite.gameObject.SetActive(true);
		vUseTimeLabel.gameObject.SetActive(false);
		vCoolTimeSprite.gameObject.SetActive(true);
		vCoolTimeLabel.gameObject.SetActive(false);

		mCurrentCommanderSkill = null;
		mTransform = transform;
		mTooltipPivotPosition = new Vector3(
			aTransform.localPosition.x + aTransform.parent.localPosition.x + 150,
			aTransform.localPosition.y + aTransform.parent.localPosition.y,
			aTransform.localPosition.z + aTransform.parent.localPosition.z);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 갱신합니다.
	public void UpdateFrame()
	{
		if (mCurrentCommanderSkill == null)
			return;

		if (mActivationState != mCurrentCommanderSkill.aActivationState)
			ResetUpdate();

		if (mOnFrameUpdateDelegate != null)
			mOnFrameUpdateDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신을 초기화합니다.
	public void ResetUpdate()
	{
		if (mCurrentCommanderSkill == null)
			return;

		mActivationState = mCurrentCommanderSkill.aActivationState;

		switch (mActivationState)
		{
		case BattleCommanderSkill.ActivationState.ReadyToUse:
			{
				mOnFrameUpdateDelegate = _UpdateFrame_ReadyToUse;
				mUpdateSprite = vReadyTimeSprite;
				mUpdateLabel = vReadyTimeLabel;
			}
			break;
		case BattleCommanderSkill.ActivationState.Use:
			{
				mOnFrameUpdateDelegate = _UpdateFrame_Use;
				mUpdateSprite = vUseTimeSprite;
				if (mCurrentCommanderSkill.aUseTimer > 0)
				{
					mUpdateLabel = vUseTimeLabel;
				}
				else
				{
					vUseTimeSprite.fillAmount = 1.0f;
					mUpdateLabel = null;
				}
			}
			break;
		case BattleCommanderSkill.ActivationState.ReadyFromUse:
			{
				mOnFrameUpdateDelegate = _UpdateFrame_ReadyFromUse;
				mUpdateSprite = vUseTimeSprite;
				mUpdateLabel = vReadyTimeLabel;
			}
			break;
		case BattleCommanderSkill.ActivationState.Cooldown:
			{
				mOnFrameUpdateDelegate = _UpdateFrame_Cooldown;
				mUpdateSprite = vCoolTimeSprite;
				mUpdateLabel = vCoolTimeLabel;
			}
			break;
		case BattleCommanderSkill.ActivationState.None:
		default:
			{
				mOnFrameUpdateDelegate = null;
				mUpdateSprite = null;
				mUpdateLabel = null;
			}
			break;
		}

		vReadyTimeSprite.gameObject.SetActive(mUpdateSprite == vReadyTimeSprite);
		vReadyTimeLabel.gameObject.SetActive(mUpdateLabel == vReadyTimeLabel);
		vUseTimeSprite.gameObject.SetActive(mUpdateSprite == vUseTimeSprite);
		vUseTimeLabel.gameObject.SetActive(mUpdateLabel == vUseTimeLabel);
		vCoolTimeSprite.gameObject.SetActive(mUpdateSprite == vCoolTimeSprite);
		vCoolTimeLabel.gameObject.SetActive(mUpdateLabel == vCoolTimeLabel);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void OnClickAction()
	{
		if (mCurrentCommanderSkill == null)
			return;
		if (mCurrentCommanderSkill.aIsUseSkill)
			return;

		if (!mCurrentCommanderSkill.aIsStarted)
		{
			if (mCurrentCommanderSkill.aSkillSpec.mUseType != SkillUseType.Position) // 위치 지정이 아닌 스킬에 한해서만
				BattlePlay.main.vStage.vStageTouchControl.StartSkill(
					mSkillCommander,
					mSkillCommander.aCoord,
					mSkillCommander.aDirectionY); // 스킬을 시작
		}
		else
		{
			BattlePlay.main.vStage.vStageTouchControl.StopSkill(mSkillCommander);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void ShowTooltip(bool pIsShow)
	{
		if (pIsShow)
		{
			if (!mUserControl.vIsActionToolTip)
				mUserControl.OpenSkillToolTip(this);
		}
		else
		{
			if (mUserControl.vIsActionToolTip)
				mUserControl.CloseSkillToolTip();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 컨트롤에 연결합니다.
	public void SetUserControl(BattleUIUserControl pUserControl)
	{
		mUserControl = pUserControl;
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 정보를 버튼에 셋팅합니다.
	public void SetSkillInfo(BattleCommanderSkill pCommanderSkill)
	{
		vReadyTimeSprite.gameObject.SetActive(false);
		vUseTimeSprite.gameObject.SetActive(false);
		vCoolTimeSprite.gameObject.SetActive(false);

		if (mCurrentCommanderSkill != null)
		{
			mCurrentCommanderSkill.aOnStartSkillDelegate -= _OnStartSkill;
			mCurrentCommanderSkill.aOnStopSkillDelegate -= _OnStopSkill;
		}

		if (pCommanderSkill == null)
		{
			vSkillIconTexture.mainTexture = null;
			vSkillNameLabel.text = String.Empty;
			mSkillCommander = null;
		}
		else
		{
			vSkillIconTexture.mainTexture = pCommanderSkill.aSkillSpec.GetIconTexture();
			vSkillNameLabel.text = LocalizedTable.get.GetLocalizedText(pCommanderSkill.aSkillSpec.mNameKey);
			mSkillCommander = pCommanderSkill.aCommander;
		}

		mCurrentCommanderSkill = pCommanderSkill;
		if (mCurrentCommanderSkill != null)
		{
			mCurrentCommanderSkill.aOnStartSkillDelegate += _OnStartSkill;
			mCurrentCommanderSkill.aOnStopSkillDelegate += _OnStopSkill;

			vDragAvailbleObject.SetActive(mCurrentCommanderSkill.aSkillSpec.mUseType == SkillUseType.Position);

			if (vSkillToolTip != null)
				vSkillToolTip.SetSkillInfo(mCurrentCommanderSkill);
		}

		// 버튼 상태를 갱신합니다.
		mActivationState = BattleCommanderSkill.ActivationState.None;
		mOnFrameUpdateDelegate = null;
		ResetUpdate();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// BattleCommanderSkill.aOnStartSkillDelegate
	private void _OnStartSkill(BattleCommander pCommander)
	{
		SetSkillInfo(mCurrentCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommanderSkill.aOnStopSkillDelegate
	private void _OnStopSkill(BattleCommander pCommander)
	{
		SetSkillInfo(mCurrentCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// StageDragCursor.OnCheckValidPositionDelegate
	public bool _OnCheckValidPosition(Vector3 pPosition)
	{
		if (mCurrentCommanderSkill != null)
			return mCurrentCommanderSkill.CheckValidTargetCoord(Coord2.ToCoord(pPosition));
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	private void _UpdateFrame_ReadyToUse()
	{
		float lReadyTimeFactor = 1.0f - (float)mCurrentCommanderSkill.aReadyTimer / mCurrentCommanderSkill.aReadyDelay;
		vReadyTimeSprite.fillAmount = lReadyTimeFactor;
		vReadyTimeLabel.text = String.Format("{0}s", ((int)mCurrentCommanderSkill.aReadyTimer / 1000) + 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	private void _UpdateFrame_Use()
	{
		if (mCurrentCommanderSkill.aUseTimer > 0)
		{
			float lUseTimeFactor = (float)mCurrentCommanderSkill.aUseTimer / mCurrentCommanderSkill.aUseDelay;
			vUseTimeSprite.fillAmount = lUseTimeFactor;
			vUseTimeLabel.text = String.Format("{0}s", ((int)mCurrentCommanderSkill.aUseTimer / 1000) + 1);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	private void _UpdateFrame_ReadyFromUse()
	{
		float lReadyTimeFactor = (float)mCurrentCommanderSkill.aReadyTimer / mCurrentCommanderSkill.aReadyDelay;
		vUseTimeSprite.fillAmount = lReadyTimeFactor;
		vReadyTimeLabel.text = String.Format("{0}s", ((int)mCurrentCommanderSkill.aReadyTimer / 1000) + 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	private void _UpdateFrame_Cooldown()
	{
		float lCoolTimeFactor = (float)mCurrentCommanderSkill.aCooldownTimer / mCurrentCommanderSkill.aCooldownDelay;
		vCoolTimeSprite.fillAmount = lCoolTimeFactor;
		vCoolTimeLabel.text = String.Format("{0}s", ((int)mCurrentCommanderSkill.aCooldownTimer / 1000) + 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleUIDraggableButton 메서드
	protected override void OnPressUpAction()
	{
		BattlePlay.main.vStage.vStageTouchControl.HideDragCursorAndUseSkill(mCurrentCommanderSkill);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;

	private BattleUIUserControl mUserControl;
	private BattleCommander mSkillCommander;
	private BattleCommanderSkill mCurrentCommanderSkill;
	private Vector3 mTooltipPivotPosition;

	private BattleCommanderSkill.ActivationState mActivationState;
	private OnFrameUpdateDelegate mOnFrameUpdateDelegate;
	private UISprite mUpdateSprite;
	private UILabel mUpdateLabel;
}
