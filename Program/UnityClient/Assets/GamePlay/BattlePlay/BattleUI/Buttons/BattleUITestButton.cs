using UnityEngine;
using System.Collections;
using System;

public class BattleUITestButton : MonoBehaviour
{
	public enum TestFunction
	{
		LoadLevel,
		Win,
		Lose,
		GoodGame,
		InstantReplay,
		Restart,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public TestFunction vTestFunction = TestFunction.LoadLevel;

	public String vLevelName;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		if (GamePopupUIManager.aInstance.aCurrentPopup != null)
			GamePopupUIManager.aInstance.BackPopup();

		switch (vTestFunction)
		{
		case TestFunction.LoadLevel:
			_LoadLevel();
			break;
		case TestFunction.Win:
			{
				if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
					BattlePlay.main.vBattleNetwork.SetFrameInput_CheatWin(BattlePlay.main.aClientUser);
				else
					BattlePlay.main.EndBattle(false); // 강제 종료라서 pIsProgressEnded=false
			}
			break;
		case TestFunction.Lose:
			{
				if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
					BattlePlay.main.vBattleNetwork.SetFrameInput_CheatLose(BattlePlay.main.aClientUser);
				else
					BattlePlay.main.EndBattle(false); // 강제 종료라서 pIsProgressEnded=false
			}
			break;
		case TestFunction.GoodGame:
			{
				if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
					BattlePlay.main.vBattleNetwork.SetFrameInput_GoodGame(BattlePlay.main.aClientUser);
				else
					BattlePlay.main.EndBattle(false); // 강제 종료라서 pIsProgressEnded=false
			}
			break;
		case TestFunction.InstantReplay:
			_InstantReplay();
			break;
		case TestFunction.Restart:
			_Restart();
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 레벨을 로딩합니다.
	private void _LoadLevel()
	{
		String lLoadLevel = vLevelName;
		if (String.IsNullOrEmpty(lLoadLevel))
			lLoadLevel = Application.loadedLevelName;
		Application.LoadLevel(lLoadLevel);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투를 마칩니다.
	private void _BattleEnd(BattleProtocol.BattleEndCode pBattleEndCode, String pPromptMessage)
	{
//		BattlePlay.main.vBattleControlPanel.PromptMessage(pPromptMessage, 0.0f);
		GameData.get.UpdateBattleEnd(pBattleEndCode);
		BattlePlay.main.vBattleNetwork.aIBattleProtocol.SendPacket_BattleEnd(pBattleEndCode);
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨을 로딩합니다.
	private void _InstantReplay()
	{
		if (GameData.get.aCurrentGamePhase == GamePhase.Battle)
		{
			if (BattlePlay.main.aInstantReplayData != null)
				GameData.get.EnterReplayPhase(BattlePlay.main.aInstantReplayData, true);
		}
		else if (GameData.get.aBattleVerifyData != null)
		{
			GameData.get.EnterReplayPhase(GameData.get.aBattleVerifyData, true);
		}
		else
			Debug.LogError("no BattleVerifyData");
	}
	//-------------------------------------------------------------------------------------------------------
	// 재시작 합니다.
	private void _Restart()
	{
		Application.LoadLevel(Application.loadedLevelName);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
