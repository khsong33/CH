using UnityEngine;
using System.Collections;
using System;

public class BattleUIToggleButton : MonoBehaviour
{
	public enum TestFunction
	{
		LoadLevel,
		Win,
		Lose,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public BattlePlay vBattlePlay;

	public TestFunction vTestFunction = TestFunction.LoadLevel;

	public String vLevelName;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vBattlePlay == null)
		{
			vBattlePlay = BattlePlay.main;
			if (vBattlePlay == null)
				Debug.LogError("can't find vBattlePlay - " + this);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		switch (vTestFunction)
		{
		case TestFunction.LoadLevel:
			_LoadLevel();
			break;
		case TestFunction.Win:
			_BattleEnd(BattleProtocol.BattleEndCode.TestWin, "WIN");
			break;
		case TestFunction.Lose:
			_BattleEnd(BattleProtocol.BattleEndCode.TestLose, "LOSE");
			break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 레벨을 로딩합니다.
	private void _LoadLevel()
	{
		String lLoadLevel = vLevelName;
		if (String.IsNullOrEmpty(lLoadLevel))
			lLoadLevel = Application.loadedLevelName;
		Application.LoadLevel(lLoadLevel);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투를 마칩니다.
	private void _BattleEnd(BattleProtocol.BattleEndCode pBattleEndCode, String pPromptMessage)
	{
//		vBattlePlay.vBattleControlPanel.PromptMessage(pPromptMessage, 0.0f);
		GameData.get.UpdateBattleEnd(pBattleEndCode);
		vBattlePlay.vBattleNetwork.aIBattleProtocol.SendPacket_BattleEnd(pBattleEndCode);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
