using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleUITroopMark : MonoBehaviour
{
	public static readonly String cMoraleStateSpriteName = "Icons_minimap_mm_manpower_point";
	public static readonly String cReturnStateSpriteName = "Icons_minimap_mm_return";

	private float cHpWarningToggleDelay = 0.4f;
	private float cHpWarningToggleAlpha = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UISprite vBackgroundSprite;
	public UISlider vHpSlider;
	public UISprite vHpSliderGaugeSprite;
	public UISlider vExpSlider;
	public UISprite vMoraleStateSprite;
	public UISlider vMoraleSlider;
	public TweenAlpha vMoraleStateTweenAlpha;
	public UISprite vReturnStateSprite;
	public UISprite vRetreatStateSprite;
	public UILabel vRetreateTimeLabel;
	public UITexture vIconTexture;
	public UILabel vCommanderNumLabel;
	public UITexture vCommanderIconTexture;
	public UISprite vRecruitIconSprite;
	public UILabel vRecruitMpLabel;
	public UISprite vVeteranSprite;
	public UISprite vCaptureType;
	public UILabel vRankLabel;
	public UISprite vRankColorSprite;

	public BattleUITroopCountInfo[] vTroopKindCountInfo;

	public Transform vCommanderInfoRoot;

	public UILabel vBattleCommanderGridIdxDebugLabel;

	public StageTroopScript vTroopScript;

	public GameObject vScreenInMarkObject;
	public GameObject vScreenOutMarkPrefabs;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 부대
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 전투 부대
	public StageTroop aStageTroop
	{
		get { return mStageTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 표시 여부
	public bool aIsShowCommanderInfo
	{
		get { return mIsShowCommanderInfo; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		if (vBackgroundSprite == null)
			Debug.LogError("vBackgroundSprite is empty - " + this);
		if (vHpSlider == null)
			Debug.LogError("vHpSlider is empty - " + this);
		if (vHpSliderGaugeSprite == null)
			Debug.LogError("vHpSliderGaugeSprite is empty - " + this);
		if(vExpSlider == null)
			Debug.LogError("vExpSlider is empty - " + this);
		if (vReturnStateSprite == null)
			Debug.LogError("vReturnStateSprite is empty - " + this);
		if(vRetreatStateSprite == null)
			Debug.LogError("vRetreateTimeLabel is empty - " + this);
		if(vRetreateTimeLabel == null)
			Debug.LogError("vRetreateTimeLabel is empty - " + this);
		if (vIconTexture == null)
			Debug.LogError("vIconTexture is empty - " + this);
		if (vCommanderNumLabel == null)
			Debug.LogError("vCommanderNumLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vRecruitIconSprite == null)
			Debug.LogError("vRecruitIconSprite is empty - " + this);
		if (vRecruitMpLabel == null)
			Debug.LogError("vRecruitMpLabel is empty - " + this);
		if (vVeteranSprite == null)
			Debug.LogError("vVeteranSprite is empty - " + this);
		if (vCaptureType == null)
			Debug.LogError("vCaptureType is empty - " + this);
		if (vRankLabel == null)
			Debug.LogError("vRankLabel is empty - " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);

		if (vCommanderInfoRoot == null)
			Debug.LogError("vCommanderInfoRoot is empty - " + this);

		if (vTroopScript == null)
			Debug.LogError("vTroopScript is empty - " + this);

		Vector3 lIconTextureLocalPosition = vCommanderIconTexture.transform.localPosition;

		float lXOffset = lIconTextureLocalPosition.x;
		float lYOffset = lIconTextureLocalPosition.y;

		mRoot = NGUITools.FindInParents<UIRoot>(this.gameObject);

		float lUIPixelHeightScale = (float)mRoot.activeHeight / mRoot.manualHeight;

		mScreenMaxSize = new Vector2(mRoot.manualWidth / 2.0f - lXOffset - 50.0f, 
									 mRoot.activeHeight / 2.0f - lYOffset - 50.0f);

		mScreenMinSize = new Vector2(-1 * (mRoot.manualWidth / 2.0f + lXOffset - 50.0f), 
									 -1 * (mRoot.activeHeight / 2.0f + lYOffset - 220.0f));

		mTransform = transform;

		Transform lScreenOutObject = Instantiate<GameObject>(vScreenOutMarkPrefabs).transform;
		mScreenOutObject = lScreenOutObject.GetComponent<StageScreenOutMark>();
		mScreenOutObject.transform.parent = mTransform.parent;
		mScreenOutObject.transform.localScale = Vector3.one;
		mScreenOutObject.transform.localPosition = Vector3.zero;
		_SetActiveScreenOutObject(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		vCommanderInfoRoot.gameObject.SetActive(false);

		ShowRetreat(false);

		mIsShowCommanderInfo = !(mBattleTroop == mBattleTroop.aCommander.aLeaderTroop); // ShowCommanderInfo()가 실제로 작동하도록 반대로
		ShowCommanderInfo(!mIsShowCommanderInfo);

		mIsShowRecruitInfo = true; // HideRecruitInfo()가 실제로 작동하도록
		HideRecruitInfo();

		mIsHpWarningUpdate = false;

		HideTroopScript();
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (vCommanderInfoRoot.gameObject.activeInHierarchy && aStageTroop.aIsSelected)
		{
			_UpdateScreenOutMark();
		}
		else
		{
			_SetActiveScreenOutObject(false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Screen out mark 업데이트
	private void _UpdateScreenOutMark()
	{
		float lPositionX = mTransform.localPosition.x;
		float lPositionY = mTransform.localPosition.y;
		bool lIsScreenOut = false;
		if (mTransform.localPosition.x < mScreenMinSize.x)
		{
			lPositionX = mScreenMinSize.x;
			lIsScreenOut = true;
		}
		else if (mTransform.localPosition.x > mScreenMaxSize.x)
		{
			lPositionX = mScreenMaxSize.x;
			lIsScreenOut = true;
		}
		if (mTransform.localPosition.y < mScreenMinSize.y)
		{
			lPositionY = mScreenMinSize.y;
			lIsScreenOut = true;
		}
		else if (mTransform.localPosition.y > mScreenMaxSize.y)
		{
			lPositionY = mScreenMaxSize.y;
			lIsScreenOut = true;
		}
		bool lIsShowScreenOut = lIsScreenOut & vCommanderInfoRoot.gameObject.activeInHierarchy & mStageTroop.aIsSelected;
		_SetActiveScreenOutObject(lIsShowScreenOut);
		if (lIsShowScreenOut)
		{
			mScreenOutObject.transform.localPosition = new Vector3(lPositionX, lPositionY, mScreenOutObject.transform.localPosition.z);
			Vector3 lDirectVector = mTransform.localPosition - mScreenOutObject.transform.localPosition;
			float lZDegree = Mathf.Atan2(lDirectVector.x, lDirectVector.y) * Mathf.Rad2Deg;
			mScreenOutObject.GoRotationMark(-lZDegree);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 초기화합니다.
	public void SetDisplayTroop(BattleTroop pBattleTroop, StageTroop pStageTroop)
	{
		mBattleTroop = pBattleTroop;
		mStageTroop = pStageTroop;

		//Color lUserColor
		//	= (pBattleTroop.aCommander.aTeamUser != null)
		//	? BattlePlay.main.vUserColors[pBattleTroop.aCommander.aTeamUser.aUserIdx] : BattlePlay.main.vNpcColor;

		Color lUserColor = BattlePlay.main.GetUserColor(pBattleTroop.aCommander);

		ColorLib.SetWidgetColor(vBackgroundSprite, lUserColor);
		ColorLib.SetWidgetColor(vHpSliderGaugeSprite, lUserColor);
		ColorLib.SetWidgetAlpha(vHpSliderGaugeSprite, 1.0f);

		vIconTexture.mainTexture = pBattleTroop.aTroopSpec.GetFieldIconTexture();

		vHpSlider.gameObject.SetActive(false); // HP를 숨긴 상태에서 시작

		vCommanderIconTexture.mainTexture = pBattleTroop.aCommander.aCommanderSpec.GetIconTexture();

		UpdateReturnState(pBattleTroop.aIsFallBehind);

		vRecruitIconSprite.gameObject.SetActive(false);
		vRecruitMpLabel.gameObject.SetActive(false);

		vCaptureType.gameObject.SetActive(pBattleTroop.aTroopSpec.mIsCaptureType);

		CommanderItem lCommanderItem = pBattleTroop.aCommander.aCommanderItem;

		int lOpenIndex = lCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(true);
				mCurrentTroopKindCountInfo = vTroopKindCountInfo[iKind];
			}
			else
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
			}
		}
		mCurrentTroopKindCountInfo.SetTroopCountInfo(lCommanderItem.mTroopSet);

		mBattleCommander = pBattleTroop.aCommander;

		mBattleCommander.aOnDestroyedTroopDelegate += _OnDestroyedCommanderTroop;
		mBattleCommander.aOnRecruitTroopsDelegate += _OnRecruitTroop;

		if (BattlePlay.main.vIsShowCommanderGridIdx)
		{
			vBattleCommanderGridIdxDebugLabel.gameObject.SetActive(true);
			vBattleCommanderGridIdxDebugLabel.text = mBattleCommander.aCommanderGridIdx.ToString();
		}
		else
		{
			vBattleCommanderGridIdxDebugLabel.gameObject.SetActive(false);
		}
		vRankLabel.text = mBattleCommander.aCommanderItem.mCommanderExpRank.ToString();

		if (mBattleCommander.aCommanderSpec != null)
		{
			vRankColorSprite.color = BattleConfig.get.mRankColors[mBattleCommander.aCommanderItem.aRank];
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// HP를 갱신합니다.
	public void UpdateHp(int pCurHp, int pMaxHp)
	{
		vHpSlider.value = (float)pCurHp / pMaxHp;

		if (vHpSlider.gameObject.activeInHierarchy != (pCurHp > 0))
			vHpSlider.gameObject.SetActive(pCurHp > 0);

		if (mBattleTroop.aIsWounded &&
			!mIsHpWarningUpdate &&
			gameObject.activeInHierarchy)
			StartCoroutine(_UpdateHpWarning());
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 경험치를 갱신합니다.
	public void UpdateExp()
	{
		int lVeteranRank = aBattleTroop.aCommander.aVeterancyRank;
		int lVeteranExperience = aBattleTroop.aCommander.aVeterancyExperience;
		CommanderSpec lCommanderSpec = aBattleTroop.aCommander.aCommanderSpec;
		float lResultExperienceRatio = 0.0f;
		if (lVeteranRank <= 0)
			lResultExperienceRatio = (float)lVeteranExperience / lCommanderSpec.GetNextVeterancyExp(lVeteranRank);
		else if (lVeteranRank >= BattleConfig.get.mMaxVeterancyRank)
			lResultExperienceRatio = 1.0f;
		else
		{
			int lPreRankUpExp = lCommanderSpec.GetNextVeterancyExp(lVeteranRank - 1);
			int lCurrentRankUpExp = lCommanderSpec.GetNextVeterancyExp(lVeteranRank);
			lResultExperienceRatio = (float)(lVeteranExperience - lPreRankUpExp) / (lCurrentRankUpExp - lPreRankUpExp);
		}

		if (lResultExperienceRatio >= 1.0f)
			lResultExperienceRatio = 1.0f;

		vExpSlider.value = lResultExperienceRatio;
	}
	//-------------------------------------------------------------------------------------------------------
	// 사기 상태를 갱신합니다.
	public void UpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
		if (vMoraleStateSprite != null)
			vMoraleStateSprite.color = BattleConfig.get.mTroopMoraleStateColors[(int)pTroopMoraleState];

		if (vMoraleStateTweenAlpha != null)
			vMoraleStateTweenAlpha.enabled = (pTroopMoraleState != TroopMoraleState.Good);

		if (vMoraleSlider != null)
		{
			vMoraleSlider.alpha = pTroopMoraleState != TroopMoraleState.Good ? vMoraleStateTweenAlpha.value : 1.0f;
			vMoraleSlider.value = (float)mBattleTroop.aStat.aCurMilliMorale / mBattleTroop.aStat.aMaxMilliMorale;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 복귀 상태를 갱신합니다.
	public void UpdateReturnState(bool pIsFallBehind)
	{
		vReturnStateSprite.gameObject.SetActive(pIsFallBehind);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 퇴각 정보를 보입니다.
	public void ShowRetreat(bool pIsRetreat)
	{
		vRetreatStateSprite.gameObject.SetActive(pIsRetreat);
		vRetreateTimeLabel.gameObject.SetActive(pIsRetreat);
		if (pIsRetreat)
		{
			if(isActiveAndEnabled)
				StartCoroutine(_UpdateRetreatTimer());
		}
		else
		{
			mIsRetreatTimerUpdate = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 보입니다.
	public void ShowCommanderInfo(bool pIsShow)
	{
		if (mIsShowCommanderInfo == pIsShow)
			return;
		mIsShowCommanderInfo = pIsShow;

		vCommanderInfoRoot.gameObject.SetActive(mIsShowCommanderInfo && (mBattleTroop.aCommander.aUser != null));
	}
	//-------------------------------------------------------------------------------------------------------
	// 충원 MP 정보를 보입니다.
	public void ShowRecruitInfo(int pRecruitMp)
	{
		if (!mIsShowRecruitInfo)
		{
			mIsShowRecruitInfo = true;
			vRecruitIconSprite.gameObject.SetActive(mIsShowRecruitInfo);
			vRecruitMpLabel.gameObject.SetActive(mIsShowRecruitInfo);
		}
		vRecruitMpLabel.text = pRecruitMp.ToString();
	}
	//-------------------------------------------------------------------------------------------------------
	// 충원 MP 정보를 숨깁니다.
	public void HideRecruitInfo()
	{
		if (mIsShowRecruitInfo)
		{
			mIsShowRecruitInfo = false;
			vRecruitIconSprite.gameObject.SetActive(mIsShowRecruitInfo);
			vRecruitMpLabel.gameObject.SetActive(mIsShowRecruitInfo);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 랭크 정보를 출력합니다.
	public void ShowVeteranRank(int pRank)
	{
		if (mBattleTroop.aUser == null)
		{
			vVeteranSprite.gameObject.SetActive(false);
			return;
		}
		
		if (pRank <= 0)
		{
			vVeteranSprite.gameObject.SetActive(false);
		}
		else
		{
			vVeteranSprite.gameObject.SetActive(true);
			
			DeckNation lTroopNation = (DeckNation)mBattleTroop.aUser.aUserInfo.mBattleNation;
			vVeteranSprite.spriteName = String.Format("{0}_Veteran_{1}", lTroopNation.ToString(), pRank);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 대사창을 출력합니다.
	public void ShowTroopScript(String pMessageKey, float pDuration)
	{
		if (mBattleCommander.aLeaderTroop == mBattleTroop)
		{
			vTroopScript.gameObject.SetActive(true);
			vTroopScript.ShowScript(pMessageKey, pDuration);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 대사창을 숨깁니다.
	public void HideTroopScript()
	{
		vTroopScript.gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.aOnRecruitTroopsDelegate
	private void _OnRecruitTroop(BattleCommander pCommander)
	{
		if (mCurrentTroopKindCountInfo != null)
			mCurrentTroopKindCountInfo.UpdateTroopCountInfo(mBattleCommander.aTroopKindCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// BattleCommander.aOnDestroyedTroopDelegate
	private void _OnDestroyedCommanderTroop(BattleCommander pCommander, int pTroopSlotIdx)
	{
		if(mCurrentTroopKindCountInfo != null)
			mCurrentTroopKindCountInfo.UpdateTroopCountInfo(mBattleCommander.aTroopKindCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] HP 경고 표시를 합니다.
	private IEnumerator _UpdateHpWarning()
	{
		mIsHpWarningUpdate = true;
		
		bool lIsShowHpSliderGaugeSprite = false;

		while (mBattleTroop.aIsWounded)
		{
			ColorLib.SetWidgetAlpha(
				vHpSliderGaugeSprite,
				lIsShowHpSliderGaugeSprite ? 1.0f : cHpWarningToggleAlpha);
			lIsShowHpSliderGaugeSprite ^= true;

			yield return new WaitForSeconds(cHpWarningToggleDelay);
		}
		ColorLib.SetWidgetAlpha(vHpSliderGaugeSprite, 1.0f);

		mIsHpWarningUpdate = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 퇴각 타이머 표시를 갱신합니다.
	private IEnumerator _UpdateRetreatTimer()
	{
		float lRetreatTimer = BattleConfig.get.mRetreatOrderDelay * 0.001f;
		mIsRetreatTimerUpdate = true;
		while (lRetreatTimer >= 0)
		{
			if (!mIsRetreatTimerUpdate)
				yield break;

			lRetreatTimer -= Time.deltaTime;
			if (lRetreatTimer < 0.0f)
				lRetreatTimer = 0.0f;

			int lDisplayTime = (int)Mathf.Ceil(lRetreatTimer);
			vRetreateTimeLabel.text = lDisplayTime.ToString();

			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 스크린 안밖의 표시를 처리합니다.
	private void _SetActiveScreenOutObject(bool pIsActive)
	{
		if (pIsActive)
		{
			mScreenOutObject.SetCommanderItem(mBattleCommander);
		}
		mScreenOutObject.gameObject.SetActive(pIsActive);
		vScreenInMarkObject.SetActive(!pIsActive);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCommander mBattleCommander;
	private BattleTroop mBattleTroop;
	private StageTroop mStageTroop;

	private bool mIsShowCommanderInfo;
	private bool mIsShowRecruitInfo;

	private BattleUITroopCountInfo mCurrentTroopKindCountInfo;

	private bool mIsHpWarningUpdate;
	private bool mIsRetreatTimerUpdate;

	private Vector2 mScreenMinSize;
	private Vector2 mScreenMaxSize;

	private Transform mTransform;

	private UIRoot mRoot;

	private StageScreenOutMark mScreenOutObject;
}
