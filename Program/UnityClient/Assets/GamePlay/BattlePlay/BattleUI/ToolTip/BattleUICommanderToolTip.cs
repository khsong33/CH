﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUICommanderToolTip : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------	
	public UITexture vCommanderIcon;
	public UILabel vCommanderNameLabel;
	public UILabel vCommanderDescLabel;

	public BattleUITroopCountInfo[] vTroopKindCountInfo;
	public BattleUITroopToolTip[] vTroopToolTips;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		if(vCommanderIcon == null)
			Debug.LogError("vCommanderIcon is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if(vCommanderDescLabel == null)
			Debug.LogError("vCommanderDescLabel is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 처리
	public void SetCommanderInfo(CommanderItem pCommanderItem)
	{
		vCommanderIcon.mainTexture = pCommanderItem.mCommanderSpec.GetIconTexture();
		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(pCommanderItem.mCommanderSpec.mNameKey);
		vCommanderDescLabel.text = LocalizedTable.get.GetLocalizedText(pCommanderItem.mCommanderSpec.mDescKey);

		int lOpenIndex = pCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			// 병사 종류 아이콘
			if (iKind == lOpenIndex)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(true);
				mCurrentTroopKindCountInfo = vTroopKindCountInfo[iKind];
			}
			else
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
			}

			// 병사 툴팁 정보
			if (iKind <= lOpenIndex)
			{
				vTroopToolTips[iKind].gameObject.SetActive(true);
				vTroopToolTips[iKind].SetTroopToolTip(pCommanderItem.mTroopSet.mTroopKindCountInfos[iKind].mTroopSpec);
			}
			else
			{
				vTroopToolTips[iKind].gameObject.SetActive(false);
			}
		}
		mCurrentTroopKindCountInfo.SetTroopCountInfo(pCommanderItem.mTroopSet);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleUITroopCountInfo mCurrentTroopKindCountInfo;
}

