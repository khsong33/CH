﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUISkillToolTip : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------	
	public UITexture vSkillIcon;
	public UILabel vSkillNameLabel;
	public UILabel vCoolDownTimeLabel;
	public UILabel vSkillDescLabel;
	public UILabel vUseTipLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		if(vSkillIcon == null)
			Debug.LogError("vSkillIcon is empty - " + this);
		if(vSkillNameLabel == null)
			Debug.LogError("vSkillNameLabel is empty - " + this);
		if(vCoolDownTimeLabel == null)
			Debug.LogError("vCoolDownTimeLabel is empty - " + this);
		if(vSkillDescLabel == null)
			Debug.LogError("vSkillDescLabel is empty - " + this);
		if(vUseTipLabel == null)
			Debug.LogError("vUseTipLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 스킬 정보 처리
	public void	SetSkillInfo(BattleCommanderSkill pCommanderSkill)
	{
		vSkillIcon.mainTexture = pCommanderSkill.aSkillSpec.GetIconTexture();
		vSkillNameLabel.text = LocalizedTable.get.GetLocalizedText(pCommanderSkill.aSkillSpec.mNameKey);
		vSkillDescLabel.text = LocalizedTable.get.GetLocalizedText(pCommanderSkill.aSkillSpec.mDescKey);
		vCoolDownTimeLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("CoolDownTime{0}"), (int)pCommanderSkill.aCooldownDelay / 1000);
		String lUseTipText = String.Empty;
		if(pCommanderSkill.aSkillSpec.aIsActiveSkill)
		{
			if(pCommanderSkill.aSkillSpec.mUseType == SkillUseType.Default)
			{
				lUseTipText = LocalizedTable.get.GetLocalizedText("LocalUsetipKeySkillDefault");
			}
			else
			{
				lUseTipText = LocalizedTable.get.GetLocalizedText("LocalUsetipKeySkillPosition");
			}
		}
		else //if(pCommanderSkill.aSkillSpec.aIsToggleSkill)
		{
			if (!pCommanderSkill.aIsStarted)
			{
				lUseTipText = LocalizedTable.get.GetLocalizedText("LocalUsetipKeySkillToggleOn");
			}
			else
			{
				lUseTipText = LocalizedTable.get.GetLocalizedText("LocalUsetipKeySkillToggleOff");
			}
		}
		vUseTipLabel.text = lUseTipText;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}

