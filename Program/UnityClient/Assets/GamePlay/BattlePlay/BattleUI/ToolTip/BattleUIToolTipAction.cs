﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUIToolTipAction : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------	
	public AnimationCurve vOpenToopTipAnim = new AnimationCurve(new Keyframe(0f, 0f, 0f, 1f), new Keyframe(1f, 1f, 1f, 0f));

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		_Reset();
		mUIPanel = gameObject.GetComponent<UIPanel>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 툴팁 활성화 애니메이션 처리
	public void OpenToolTip(Vector3 pFrom, Vector3 pTo, float pDuration)
	{
		mTweenPosition = gameObject.AddComponent<TweenPosition>();
		mTweenPosition.animationCurve = vOpenToopTipAnim;
		mTweenPosition.duration = pDuration;
		mTweenPosition.from = pFrom;
		mTweenPosition.to = pTo;
		mTweenPosition.SetOnFinished(_OnFinishedOpenToolTip);

		StartCoroutine(_OpenFadein(pDuration));
	}
	private IEnumerator _OpenFadein(float pDuration)
	{
		mUIPanel.alpha = 0.2f;
		float lCurrentTime = 0.0f;
		float lFactor = 0.0f;
		while (lFactor < 1.0f)
		{
			yield return null;
			lFactor = lCurrentTime / pDuration;
			mUIPanel.alpha = Mathf.Lerp(0.2f, 1.0f, lFactor);
			lCurrentTime += Time.deltaTime;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 툴팁을 닫습니다.
	public void CloseToolTip()
	{
		if (mTweenPosition != null)
		{
			Destroy(mTweenPosition);
			mTweenPosition = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	private void _Reset()
	{
		mTweenPosition = null;
	}

	private void _OnFinishedOpenToolTip()
	{
		if (mTweenPosition != null)
		{
			Destroy(mTweenPosition);
			mTweenPosition = null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private TweenPosition mTweenPosition;
	private UIPanel mUIPanel;
}
	
