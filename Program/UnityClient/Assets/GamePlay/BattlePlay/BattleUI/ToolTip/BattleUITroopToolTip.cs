﻿using UnityEngine;
using System.Collections;
using System;

public class BattleUITroopToolTip : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------	
	public UITexture vTroopFieldIcon;
	public UITexture vTroopIcon;
	public UILabel vTroopNameLabel;
	public UILabel vTroopDescLabel;
	public UILabel vTroopEffectiveLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		if(vTroopFieldIcon == null)
			Debug.LogError("vTroopFieldIcon is empty - " + this);
		if (vTroopIcon == null)
			Debug.LogError("vTroopIcon is empty - " + this);
		if (vTroopNameLabel == null)
			Debug.LogError("vTroopNameLabel is empty - " + this);
		if (vTroopDescLabel == null)
			Debug.LogError("vTroopDescLabel is empty - " + this);
		if (vTroopEffectiveLabel == null)
			Debug.LogError("vTroopEffectiveLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 병사용 툴팁 을 셋팅합니다.
	public void SetTroopToolTip(TroopSpec pTroopSpec)
	{
		vTroopFieldIcon.mainTexture = pTroopSpec.GetFieldIconTexture();
		vTroopIcon.mainTexture = pTroopSpec.GetButtonIconTexture();
		vTroopNameLabel.text = LocalizedTable.get.GetLocalizedText(pTroopSpec.mNameKey);
		vTroopDescLabel.text = LocalizedTable.get.GetLocalizedText(pTroopSpec.mDescText);
		vTroopEffectiveLabel.text = LocalizedTable.get.GetLocalizedText(pTroopSpec.mEffectiveText);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}

