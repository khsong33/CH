﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using CombatJson;

public class BattleResultData
{
	public BattleProtocol.BattleEndCode mBattleEndCode;
	public int mGainRating;
	public ChestIconSpec mChestIconSpec;
	public int mGainMedal;
}
