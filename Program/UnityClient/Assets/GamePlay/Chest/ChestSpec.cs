﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;
public class ChestSpec
{
	public int mNo;
	public int mArea;
	public ChestType mChestType;
	public int mGoldMin;
	public int mGoldMax;
	public int mTotalCardCount;
	public int mRank2CardCount;
	public int mRank3CardCount;

	// ToString()
	public override string ToString()
	{
		return String.Format("<ChestSpec[{0}] mChestType={1} mArea={2}>", mNo, mChestType, mArea);
	}
}
// Chest Client 데이터 - Icon 정보등
public class ChestIconSpec
{
	public int mNo;
	public String mNameKey;
	public ChestType mChestType;
	public int mArea;
	public AssetKey mIconAssetKey;
	// ToString()
	public override string ToString()
	{
		return String.Format("<ShopChestSpec[{0}] mName={1} mArea={2}>", mNo, mNameKey, mArea);
	}
}
public class ChestOpenData
{
	public ChestType mChestType;
	public int mArea;
	public int mTotalItemCount;
	public int mGainGold;
	public bool mIsGemActive;
	public int mGainGem;
	public List<CommanderItemData> mGainCommanderItemDataList;
	public List<int> mNewCommanderItemIdList;
	public ChestOpenData()
	{
		mTotalItemCount = 0;
		mIsGemActive = false;
		mGainGold = 0;
		mGainGem = 0;
		mGainCommanderItemDataList = new List<CommanderItemData>();
		mNewCommanderItemIdList = new List<int>();
	}
}