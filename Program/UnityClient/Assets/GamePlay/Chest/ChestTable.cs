﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChestTable
{
	public static readonly String cCSVTextAssetPath = "RandomChestTableCSV";

	public static readonly String vNoColumn = "No";
	public static readonly String vAreaColumn = "Area";
	public static readonly String vTypeColumn = "Type";
	public static readonly String vGoldMinColumn = "GoldMin";
	public static readonly String vGoldMaxColumn = "GoldMax";
	public static readonly String vTotalCardColumn = "TotalCard";
	public static readonly String vRank2CardColumn = "Rank2Card";
	public static readonly String vRank3CardColumn = "Rank3Card";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static ChestTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new ChestTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public ChestTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mChestTypeDictionary = new Dictionary<String, ChestType>();
		foreach (ChestType iForm in Enum.GetValues(typeof(ChestType)))
			mChestTypeDictionary.Add(iForm.ToString(), iForm);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mDicChestAreaType = new Dictionary<int, Dictionary<ChestType, ChestSpec>>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			ChestSpec info = new ChestSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mNo = lCSVRow.GetIntValue(vNoColumn);
			info.mArea = lCSVRow.GetIntValue(vAreaColumn);
			String lTypeString = lCSVRow.GetStringValue(vTypeColumn);
			if (!mChestTypeDictionary.TryGetValue(lTypeString, out info.mChestType))
				Debug.LogError(String.Format("Shop Chest Table [{0}] invalid Type '{1}'", info.mNo, lTypeString));
			lCSVRow.TryIntValue(vGoldMinColumn, out info.mGoldMin, 0);
			lCSVRow.TryIntValue(vGoldMaxColumn, out info.mGoldMax, 0);
			lCSVRow.TryIntValue(vTotalCardColumn, out info.mTotalCardCount, 0);
			lCSVRow.TryIntValue(vRank2CardColumn, out info.mRank2CardCount, 0);
			lCSVRow.TryIntValue(vRank3CardColumn, out info.mRank3CardCount, 0);

			if (!mDicChestAreaType.ContainsKey(info.mArea))
			{
				mDicChestAreaType.Add(info.mArea, new Dictionary<ChestType, ChestSpec>());
			}
			if (!mDicChestAreaType[info.mArea].ContainsKey(info.mChestType))
			{
				mDicChestAreaType[info.mArea].Add(info.mChestType, info);
			}
			else
			{
				Debug.LogError("Same Dictionary data Area - " + info.mArea + "/ Type - " + info.mChestType);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Chest Spec을 가져옵니다.
	public ChestSpec GetChestSpec(int pArea, ChestType pChestType)
	{
		if (!mDicChestAreaType.ContainsKey(pArea))
		{
			Debug.LogError("Not Exist Area - " + pArea);
			return null;
		}
		if (!mDicChestAreaType[pArea].ContainsKey(pChestType))
		{
			Debug.LogError("Not Exist Chest Type " + pChestType.ToString() + " in Area " + pArea);
			return null;
		}
		return mDicChestAreaType[pArea][pChestType];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderLevelTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static ChestTable sInstance = null;

	private Dictionary<String, ChestType> mChestTypeDictionary;

	private CSVObject mCSVObject;
	private Dictionary<int, Dictionary<ChestType, ChestSpec>> mDicChestAreaType;
}
