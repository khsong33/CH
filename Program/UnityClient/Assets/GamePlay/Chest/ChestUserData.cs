﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
public enum NormalChestState
{
	Empty = 0,
	Lock = 1,
	Active = 2
}
// 일반 상자 서버 데이터
public class NormalChestData
{
	public int mSlotIdx;
	public int mArea;
	public ChestType mChestType;
	public NormalChestState mNormalChestState;
	public DateTime mOpenTime;
	public NormalChestData()
	{
		mSlotIdx = 0;
		mArea = 0;
		mChestType = ChestType.Empty;
		mNormalChestState = NormalChestState.Empty;
		mOpenTime = DateTime.UtcNow;
	}
}
// 유저 상자 전체 정보
public class ChestUserData
{
	public DateTime mFreeChest0Time;
	public DateTime mFreeChest1Time;
	public int mMedalCount;
	public int mMedalMaxCount;
	public DateTime mMedalTime;
	public List<NormalChestData> mNormalChestList;
	public ChestUserData()
	{
		mFreeChest0Time = DateTime.UtcNow;
		mFreeChest1Time = DateTime.UtcNow;
		mMedalCount = 0;
		mMedalTime = DateTime.UtcNow;
		mNormalChestList = new List<NormalChestData>();
	}
	// 상자를 활성화 시킬 수 있는 상태인지 확인
	public bool IsAvailableActiveChest()
	{
		bool lIsAvailbleOpenChest = true;
		for (int iSlot = 0; iSlot < mNormalChestList.Count; iSlot++)
		{
			if (mNormalChestList[iSlot].mNormalChestState == NormalChestState.Active)
				lIsAvailbleOpenChest = false;
		}
		return lIsAvailbleOpenChest;
	}
	// 상자를 활성화 시간 값(초)
	public int GetRemainSecChestType(ChestType pChestType)
	{
		if (pChestType == ChestType.Wooden)
			return 15;
		else if (pChestType == ChestType.Silver)
			return 10800;
		else if (pChestType == ChestType.Golden)
			return 28800;
		else if (pChestType == ChestType.Magic)
			return 43200;

		return 0;
	}
}