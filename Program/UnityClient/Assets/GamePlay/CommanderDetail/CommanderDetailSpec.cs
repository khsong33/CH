﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;
public class CommanderDetailSpec
{
	public int mIdx;
	public int mCommanderNo;
	public List<TroopDetailSpec> mTroop1DetailDataList;
	public List<TroopDetailSpec> mTroop2DetailDataList;

	internal const int cMaxDetailCount = 12;

	public CommanderDetailSpec()
	{
		mTroop1DetailDataList = new List<TroopDetailSpec>();
		mTroop2DetailDataList = new List<TroopDetailSpec>();
	}
	// ToString()
	public override string ToString()
	{
		return String.Format("<CommanderDetailSpec[{0}] mCommanderNo={1}>", mIdx, mCommanderNo);
	}
}

public enum CommanderDetailType
{
	Empty,
	StringType,
	NumberType
}
public class TroopDetailSpec
{
	public CommanderDetailType mType;
	public String mStateNameKey;
	public String mStringData;
	public int mOriginValue;
	public float[] mUpRationValues;
	public TroopDetailSpec()
	{
		mUpRationValues = new float[TroopLevelSpec.cTroopMaxLevel + 1]; // 0포함
	}
	public int GetLevelValue(int pLevel)
	{
		return Mathf.FloorToInt(mOriginValue * mUpRationValues[pLevel]);
	}
}