﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CommanderDetailTable
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public readonly String cCSVTextAssetPath = "CommanderDetailInfoTableCSV";

	public readonly String vNoColumnName = "No";
	public readonly String vCommanderIdColumnName = "CommanderIndex";
	public readonly String vTroop1StatePrefixColumnName = "Troop1State";
	public readonly String vTroop2StatePrefixColumnName = "Troop2State";

	public readonly bool vIsPreLoadAll = false;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static CommanderDetailTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new CommanderDetailTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CommanderDetailTable()
	{
		Load(vIsPreLoadAll);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(bool pIsPreLoadAll)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			cCSVTextAssetPath);
		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.
		//CSVReader.DebugOutputGrid(mCSVObject.DataSet);

		// 테이블 사전을 구축합니다.
		mCommanderNoToRowDictionary = new Dictionary<int, int>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);

			int lCommanderNo;
			if (!lCSVRow.TryIntValue(vCommanderIdColumnName, out lCommanderNo, 0))
				continue; // 빈 이름이라면 이 줄을 무시합니다.

			// 이름에 대한 줄 인덱스 사전을 구축합니다.
			if (mCommanderNoToRowDictionary.ContainsKey(lCommanderNo))
			{
				Debug.LogError(String.Format("duplicate Commander No '{0}'", lCommanderNo));
				continue;
			}
			mCommanderNoToRowDictionary.Add(lCommanderNo, iRow);
		}

		// 스펙 테이블을 초기화합니다.
		mCommanderDetailSpecs = new CommanderDetailSpec[mCSVObject.RowNum];

		// 미리 로딩해야 한다면 합니다.
		if (pIsPreLoadAll)
			LoadAllCommanderDetailSpec();
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보를 모두 읽어옵니다.
	public void LoadAllCommanderDetailSpec()
	{
		Debug.LogWarning("LoadAllTroopLevelSpec() is called - " + this);

		if (mCSVObject == null)
			return;

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			if (mCommanderDetailSpecs[iRow] == null) 
				mCommanderDetailSpecs[iRow] = _LoadCommanderDetailSpec(mCSVObject.GetRow(iRow), iRow);
		}

		// 테이블을 생성했으므로 CSV 오브젝트는 삭제합니다.
		mCSVObject.UnloadCSV();
		mCSVObject = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 스펙을 찾습니다.
	public CommanderDetailSpec FindCommanderDetailSpec(int pCommanderNo)
	{
		//Debug.Log("mNameToRowDictionary=" + mNameToRowDictionary + " gameObject=" + gameObject);

		int lRowIdx;
		if (mCommanderNoToRowDictionary.TryGetValue(pCommanderNo, out lRowIdx))
		{
			if (mCommanderDetailSpecs[lRowIdx] == null)
				mCommanderDetailSpecs[lRowIdx] = _LoadCommanderDetailSpec(mCSVObject.GetRow(lRowIdx), lRowIdx);
			return mCommanderDetailSpecs[lRowIdx];
		}
		else
		{
			Debug.LogError("can't find pCommanderNo:" + pCommanderNo + " - " + this);
			return null;
		}
	}


	//-------------------------------------------------------------------------------------------------------
	// 로컬라이징 텍스트를 갱신합니다.
	public void UpdateLocalizedText()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 스펙 개수를 얻습니다.
	public int GetSpecCount()
	{
		return mCommanderDetailSpecs.Length;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("TroopLevelTable[{0}]", pRowInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 상세보기 스펙을 로딩합니다.
	private CommanderDetailSpec _LoadCommanderDetailSpec(CSVRow pCSVRow, int pRowIdx)
	{
		CommanderDetailSpec lCommanderDetailSpec = new CommanderDetailSpec();

		// 줄 인덱스
		lCommanderDetailSpec.mIdx = pRowIdx;

		lCommanderDetailSpec.mCommanderNo = pCSVRow.GetIntValue(vCommanderIdColumnName);

		for (int iState = 0; iState < CommanderDetailSpec.cMaxDetailCount; iState++)
		{
			TroopDetailSpec lTroop1DetailTypeSpec = _LoadDetailTroopSpec(pCSVRow, vTroop1StatePrefixColumnName, iState);
			if(lTroop1DetailTypeSpec != null)
				lCommanderDetailSpec.mTroop1DetailDataList.Add(lTroop1DetailTypeSpec);

			TroopDetailSpec lTroop2DetailTypeSpec = _LoadDetailTroopSpec(pCSVRow, vTroop2StatePrefixColumnName, iState);
			if (lTroop2DetailTypeSpec != null)
				lCommanderDetailSpec.mTroop2DetailDataList.Add(lTroop2DetailTypeSpec);
		}

		return lCommanderDetailSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 부대 상세 보기 스펙을 로딩합니다.
	private TroopDetailSpec _LoadDetailTroopSpec(CSVRow pCSVRow, String pColumnPrefix, int pState)
	{
		int lStateIndex = pState + 1;
		String lTroopStateColumnName = String.Format("{0}{1}", pColumnPrefix, lStateIndex);

		TroopDetailSpec lCommanderDetailTypeSpec = new TroopDetailSpec();
		lCommanderDetailTypeSpec.mStateNameKey = lTroopStateColumnName;

		String[] lStateValues = pCSVRow.GetStringValue(lTroopStateColumnName).Split(',');
		if (String.IsNullOrEmpty(lStateValues[0]))
		{
			lCommanderDetailTypeSpec.mType = CommanderDetailType.Empty;
			return null;
		}
		else if (lStateValues.Length == 1)
		{
			lCommanderDetailTypeSpec.mType = CommanderDetailType.StringType;
			lCommanderDetailTypeSpec.mStringData = pCSVRow.GetStringValue(lTroopStateColumnName);
		}
		else if (lStateValues.Length == 2)
		{
			lCommanderDetailTypeSpec.mOriginValue = Convert.ToInt32(lStateValues[0]);
			float pUpRatioValue = Convert.ToSingle(lStateValues[1]) / 100.0f;
			lCommanderDetailTypeSpec.mUpRationValues[0] = 1.0f;
			for (int iLevel = 1; iLevel <= TroopLevelSpec.cTroopMaxLevel; iLevel++)
			{
				lCommanderDetailTypeSpec.mUpRationValues[iLevel] = lCommanderDetailTypeSpec.mUpRationValues[iLevel - 1] * pUpRatioValue;
			}
		}
		return lCommanderDetailTypeSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static CommanderDetailTable sInstance = null;

	private CSVObject mCSVObject;

	private Dictionary<int, int> mCommanderNoToRowDictionary;

	private CommanderDetailSpec[] mCommanderDetailSpecs;
}
