using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using SocketIO;

public class ConnectionProtocolNodeJS : MonoBehaviour, IConnectionProtocol
{
	public enum eNetworkState
	{
		Local = 0,
		QA
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SocketIOComponent vSocket;
	public eNetworkState vNetworkState = eNetworkState.Local;
	public String[] vURL = {"ws://127.0.0.1:6265/socket.io/?EIO=4&transport=websocket", 
							"ws://182.162.250.72:6265/socket.io/?EIO=4&transport=websocket"};

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnConnectNetworkDelegate aOnConnectNetworkDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnCloseNetworkDelegate aOnCloseNetworkDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 속성
	public OnErrorNetworkDelegate aOnErrorNetworkDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//vSocket.On("test_packet", _OnTestPacket);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//------------------------------------------------------------------------------------------------------
	// URL 등록
	protected virtual void InitURL()
	{
	}
	//------------------------------------------------------------------------------------------------------
	// 이벤트 등록
	protected virtual void RegistSocketEvent()
	{
		vSocket.On("open", _OnConnectNetwork);
		vSocket.On("close", _OnCloseNetwork);
		vSocket.On("error", _OnSocketError);
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void InitNetwork()
	{
		if (vSocket == null)
			vSocket = gameObject.GetComponent<SocketIOComponent>();

		InitURL();
		vSocket.Init();

		RegistSocketEvent();
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void ConnectNetwork()
	{
		vSocket.Connect();
	}
	//-------------------------------------------------------------------------------------------------------
	// IConnectionProtocol 메서드
	public void CloseNetwork()
	{
		vSocket.Close();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 네트워크에 연결되었을 때 호출됩니다.
	private void _OnConnectNetwork(SocketIOEvent pEvent)
	{
		// IConnectionProtocol 콜백을 호출합니다.
		if (aOnConnectNetworkDelegate != null)
			aOnConnectNetworkDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 네트워크 연결이 끊겼을 때 호출됩니다.
	private void _OnCloseNetwork(SocketIOEvent pEvent)
	{
		Debug.Log("Socket Network is close - " + pEvent.name + "/" + pEvent.data);

		// IConnectionProtocol 콜백을 호출합니다.
		if (aOnCloseNetworkDelegate != null)
			aOnCloseNetworkDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 네트워크 통신이 실패했을 때 발생하는 이벤트입니다.
	private void _OnSocketError(SocketIOEvent pEvent)
	{
		Debug.Log("Network Error - " + pEvent.name + "/" + pEvent.data);
		//vSocket.OnDestroy();
		if (aOnErrorNetworkDelegate != null)
			aOnErrorNetworkDelegate();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mErrorCount;
}
