using UnityEngine;
using System.Collections;
using System;

public delegate void OnConnectNetworkDelegate();
public delegate void OnCloseNetworkDelegate();
public delegate void OnErrorNetworkDelegate();

public interface IConnectionProtocol
{
	// 네트워크 초기화를 진행합니다.
	void InitNetwork();

	// 네트워크에 연결합니다.
	void ConnectNetwork();

	// 네트워크에 연결되었을 때 호출됩니다.
	OnConnectNetworkDelegate aOnConnectNetworkDelegate { get; set; }

	// 네트워크 연결을 끊습니다.
	void CloseNetwork();

	// 네트워크가 끊겼을 때 호출됩니다.
	OnCloseNetworkDelegate aOnCloseNetworkDelegate { get; set; }

	// 네트워크 에러일 경우 호출됩니다.
	OnErrorNetworkDelegate aOnErrorNetworkDelegate { get; set; }
}
