﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CustomBattleCommanderItemData
{
	public String mUserName;
	public int mItemIdx;
	public int mItemNo;
	public int mExp;
	public int mRank;
	public int mLevel;
	public int[] mEnchantLevel;
	public int mCount;

	public const int cMaxEnchatnLevel = 6;

	public CustomBattleCommanderItemData()
	{
		mEnchantLevel = new int[cMaxEnchatnLevel];
	}
}

public class CustomBattleCommanderDeckData
{
	public String mUserName;
	public int mDeckNo;
	public DeckNation mDeckNation;
	public List<int> mItemList;
	public CustomBattleCommanderDeckData()
	{
		mItemList = new List<int>();
	}
}