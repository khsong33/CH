﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EffectPool : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	private String[] mEffectPath = new String[4] { "Effects/Rifle Bullet", "Effects/Vehicle HighAngle", "Effects/Tank Direct", "Effects/MachineGun Bullet"};

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public static EffectPool aInstance
	{
		get 
		{
			if (mInstance == null)
				mInstance = new EffectPool();
			return mInstance; 
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		mEffectList = new List<EffectUpdater>();
		mEffectPool = new Dictionary<int, Queue<int>>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이펙트를 활성화 및 생성합니다.
	public EffectUpdater CreateEffector(int pEffectId, Vector3 pStartPosition)
	{
		// 기존의 정보가 없으면 새로 생성합니다.
		int lEffectPoolIndex = _FindEffect(pEffectId);
		if (lEffectPoolIndex < 0)
		{
			// TODO :: Effect Id로 Effect prefab path를 찾는 로직 추가
			String lEffectPath = mEffectPath[pEffectId];
			GameObject lObject = null;
			lObject = ResourceUtil.Load<GameObject>(lEffectPath);
			GameObject lEffectObject = (GameObject)Instantiate(lObject);
			lEffectObject.transform.parent = transform;

			EffectUpdater lEffectSet = lEffectObject.GetComponent<EffectUpdater>();
			lEffectSet.vEffect.position = new Vector3(pStartPosition.x, pStartPosition.y, pStartPosition.z);
			lEffectSet.vEffectPoolIndex = mEffectList.Count;
			lEffectSet.aEndBombEffect += _EndBombEffect;
			mEffectList.Add(lEffectSet);

			return lEffectSet;
		}
		else
		{
			mEffectList[lEffectPoolIndex].vEffect.position = new Vector3(pStartPosition.x, pStartPosition.y, pStartPosition.z);
			return mEffectList[lEffectPoolIndex];
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트를 비활성화시킵니다.
	public void DestoryEffect(EffectUpdater pEffect)
	{
		if (!mEffectPool.ContainsKey(pEffect.vId))
		{
			mEffectPool.Add(pEffect.vId, new Queue<int>());
		}
		pEffect.Reset();
		pEffect.gameObject.SetActive(false);

		mEffectPool[pEffect.vId].Enqueue(pEffect.vEffectPoolIndex);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 풀 안에 생성된 ActorManager가 있는지 확인합니다.
	private int _FindEffect(int pEffectId)
	{
		if (mEffectPool.ContainsKey(pEffectId))
		{
			int lResultEffectIndex = mEffectPool[pEffectId].Dequeue();
			// 더 이상 풀에 id가 없으면 삭제
			if (mEffectPool[pEffectId].Count <= 0)
				mEffectPool.Remove(pEffectId);

			return lResultEffectIndex;
		}
		return -1;
	}
	//-------------------------------------------------------------------------------------------------------
	// 이펙트 루틴이 끝난 이펙트를 삭제합니다.
	private void _EndBombEffect(EffectUpdater pEffect)
	{
		DestoryEffect(pEffect);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private List<EffectUpdater> mEffectList;
	private Dictionary<int, Queue<int>> mEffectPool;
	private static EffectPool mInstance = null;
}
