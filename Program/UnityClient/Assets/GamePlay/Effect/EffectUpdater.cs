﻿using UnityEngine;
using System.Collections;

public class EffectUpdater : MonoBehaviour
{
	public delegate void EndBombEffect(EffectUpdater pEffect);
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public EndBombEffect aEndBombEffect { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vId;
	//public Transform vTargetTransform;
	public Vector3 vTargetPosition;
	public Transform vTestTargetTransform;
	public Transform vEffect;
	public Transform vOrigin;
	public float vParabolaValue = 0.75f;
	public bool vIsDirectFire = false;
	public bool vIsTrailRender = true;
	public TrailRenderer vTailRender;
	public GameObject vShotEffect;
	public GameObject vFiredEffect;
	public float vBombEffectLifeTime = 3.0f;

	public int vEffectPoolIndex = -1;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		Reset();
		//Invoke("Fire_Test", 1.0f);
	}
	void Fire_Test()
	{
		GoFire(transform, vTestTargetTransform.position, 3.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 미사일 발사
	public void GoFire(Transform pOrigin, Vector3 pTargetPosition, float pArriveTime)
	{
		SetEffect(pOrigin, pTargetPosition, pArriveTime);
		GoFire();
	}
	//-------------------------------------------------------------------------------------------------------
	// 미사일 발사
	public void GoFire()
	{
		if (vShotEffect != null)
		{
			vShotEffect.transform.position = new Vector3(vOrigin.position.x, vOrigin.position.y, vOrigin.position.z);
			vShotEffect.SetActive(true);
		}

		if (vTailRender != null)
		{
			vTailRender.enabled = vIsTrailRender;
		}

		StartCoroutine(_GoFire());	
	}
	//-------------------------------------------------------------------------------------------------------
	// 미사일 이펙트가 날아가는 위치를 셋팅합니다.
	public void SetEffect(Transform pOriginTrasnform, Transform pTargetTransform, float pArriveTime)
	{
		SetEffect(pOriginTrasnform, pTargetTransform.position, pArriveTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// 미사일 이펙트가 날아가는 위치를 셋팅합니다.
	public void SetEffect(Transform pOriginTrasnform, Vector3 pTargetPosition, float pArriveTime)
	{
		vOrigin = pOriginTrasnform;
		vTargetPosition = pTargetPosition;
		mArriveTime = pArriveTime;
		mOriginDistance = Vector3.Distance(vOrigin.position, vTargetPosition);
		mOriginPosition = vOrigin.position;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 이펙트를 모두 초기화합니다.
	public void Reset()
	{
		if (vOrigin != null)
		{
			vEffect.position = mOriginPosition = vOrigin.position;
		}
		mCurrentTime = 0.0f;
		mArriveTime = 0.0f;
		mOriginDistance = 0.0f;
		if (vFiredEffect != null)
			vFiredEffect.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 곡사포 처리를 합니다.
	private float _CalcParabola(float pDistance, float lFactor)
	{
		float lHeightValue = 0.0f;
		float lConstValue = vParabolaValue;
		lHeightValue = (-4.0f * lConstValue) * (lFactor - 0.5f) * (lFactor - 0.5f) - (-4.0f * lConstValue) / 4.0f;
		return lHeightValue * pDistance;
	}
	//-------------------------------------------------------------------------------------------------------
	//[코루틴] 미사일 발사 업데이트 코루틴
	private IEnumerator _GoFire()
	{
		vEffect.LookAt(vTargetPosition, Vector3.up); // 타겟으로 회전시킵니다.

		float lFactor = 0.0f;
		while (lFactor < 1.0f)
		{
			if(mArriveTime <= 0)
				yield break;
			lFactor = mCurrentTime / mArriveTime;
			if (lFactor >= 1.0f)
				lFactor = 1.0f;
			if (vIsDirectFire)
			{
				float lXValue = Mathf.Lerp(mOriginPosition.x, vTargetPosition.x, lFactor);
				float lYValue = Mathf.Lerp(mOriginPosition.y, vTargetPosition.y, lFactor) + 2.0f;
				float lZValue = Mathf.Lerp(mOriginPosition.z, vTargetPosition.z, lFactor);
				vEffect.position = new Vector3(lXValue, lYValue, lZValue);
			}
			else
			{
				float lXValue = Mathf.Lerp(mOriginPosition.x, vTargetPosition.x, lFactor);
				float lYValue = _CalcParabola(mOriginDistance, lFactor) + mOriginPosition.y;
				float lZValue = Mathf.Lerp(mOriginPosition.z, vTargetPosition.z, lFactor);
				vEffect.position = new Vector3(lXValue, lYValue, lZValue);
				// Debug.Log("vEffect.position - " + vEffect.position);
			}

			if (lFactor >= 1.0f)
			{
				if (vFiredEffect != null)
				{
					vFiredEffect.transform.position = new Vector3(vTargetPosition.x, vTargetPosition.y, vTargetPosition.z);
					vFiredEffect.SetActive(true);
				}
			}
			mCurrentTime += Time.deltaTime;
			yield return null;
		}
		StartCoroutine(_EndBomb());
	}
	//-------------------------------------------------------------------------------------------------------
	// 탄이 도착했을 때 발생
	private IEnumerator _EndBomb()
	{
		yield return new WaitForSeconds(vBombEffectLifeTime);

		if (aEndBombEffect != null)
			aEndBombEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private float mCurrentTime;
	private Vector3 mOriginPosition;
	private float mOriginDistance;
	private float mArriveTime;

	//public float mDebugFactor;
}
