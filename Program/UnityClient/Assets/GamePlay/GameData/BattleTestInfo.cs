﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleTestInfo
{
	public bool aIsTesting { get; set; }
	public AssetKey aMapAssetKey { get; set; }
	public AssetKey aGridLayoutAssetKey { get; set; }
	public int aStageNo { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public BattleTestInfo()
	{
		aIsTesting = false;
		aStageNo = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
