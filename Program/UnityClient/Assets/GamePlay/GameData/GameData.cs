﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public enum GamePhase
{
	None,
	Title,
	Lobby,
	Battle,
	Replay,
}
public enum LoginState
{
	UserInfo = 0,
	CommanderItem,
	Deck,
	Shop,
	ChestList,
	Count
}
public class GameData
{
	public readonly String[] cGamePhaseSceneNames = { "", "Title", "Lobby", "Battle Play", "Battle Play" };

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static GameData get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 플레이어 정보
	public GameUserInfo aPlayerInfo { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 로그인 상태
	public bool[] aLoginState { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 이전의 게임 단계
	public GamePhase aPrevGamePhase
	{
		get { return mPrevGamePhase; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 게임 단계
	public GamePhase aCurrentGamePhase
	{
		get { return mCurrentGamePhase; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 서버 주소
	public String aBattleServerURL
	{
		get { return mBattleServerURL; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 서버로부터 받은 대전 정보
	public JSONObject aBattleMatchInfoJson
	{
		get { return mBattleMatchInfoJson; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 검증 데이터
	public BattleVerifyData aBattleVerifyData
	{
		get { return mBattleVerifyData; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 테스트 정보
	public BattleTestInfo aBattleTestInfo
	{
		get { return mBattleTestInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보
	public MyArmyCommanderInfoPanel aCommanderInfoPanel
	{
		get { return mCommanderInfoPanel; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 정보 활성화
	public bool aIsActiveCommanderPanel
	{
		get { return mIsActiveCommanderInfoPanel; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비 컨트롤
	public CombatLobbyControl aLobbyControl { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 자동 로그인 여부
	public bool aAutoLogin
	{
		get
		{
			bool lIsAutoLogin = Convert.ToBoolean(PlayerPrefs.GetString("AutoLogin", "FALSE"));
			return lIsAutoLogin;
		}
		set { PlayerPrefs.SetString("AutoLogin", value.ToString()); }
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 성공 여부
	public bool aIsLoginSuccess
	{
		get
		{
			bool lIsSuccessedLogin = true;
			for (int iState = 0; iState < (int)LoginState.Count; iState++)
			{
				if (!aLoginState[iState])
					lIsSuccessedLogin = false;
			}
			return lIsSuccessedLogin;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤을 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new GameData();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public GameData()
	{
		aLobbyControl = null;

		mPrevGamePhase = GamePhase.None;
		mCurrentGamePhase = mPrevGamePhase;

		mBattleServerURL = String.Empty;

		mBattleTestInfo = new BattleTestInfo();

		aLoginState = new bool[(int)LoginState.Count];
		for (int iState = 0; iState < (int)LoginState.Count; iState++)
		{
			aLoginState[iState] = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타이틀로 갑니다.
	public void EnterTitlePhase()
	{
		_LoadGamePhaseScene(GamePhase.Title, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비로 갑니다.
	public void EnterLobbyPhase()
	{
		_LoadGamePhaseScene(GamePhase.Lobby, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 온라인 전투로 갑니다.
	public void EnterBattlePhase(String pBattleServerURL, JSONObject pBattleMatchInfoJson, bool pIsForceReload)
	{
		mBattleServerURL = pBattleServerURL;
		mBattleMatchInfoJson = pBattleMatchInfoJson;
		mBattleVerifyData = null;

		_LoadGamePhaseScene(GamePhase.Battle, pIsForceReload);
	}
	//-------------------------------------------------------------------------------------------------------
	// 온라인 전투 테스트로 갑니다.
	public void EnterBattleTestPhase(int pTestStageNo, bool pIsForceReload)
	{
		mBattleServerURL = String.Empty;
		mBattleMatchInfoJson = null;
		mBattleVerifyData = null;

		GameData.get.aBattleTestInfo.aIsTesting = true;
		GameData.get.aBattleTestInfo.aStageNo = pTestStageNo;

		_LoadGamePhaseScene(GamePhase.Battle, pIsForceReload);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리플레이로 들어갑니다.
	public void EnterReplayPhase(BattleVerifyData pBattleVerifyData, bool pIsForceReload)
	{
		mBattleServerURL = String.Empty;
		mBattleMatchInfoJson = null;
		mBattleVerifyData = pBattleVerifyData;

		_LoadGamePhaseScene(GamePhase.Replay, pIsForceReload);
	}
	//-------------------------------------------------------------------------------------------------------
	// 승패 여부를 처리합니다.
	public void UpdateBattleEnd(BattleProtocol.BattleEndCode pBattleEndCode)
	{
		if (aPlayerInfo == null)
			return;

		if (pBattleEndCode == BattleProtocol.BattleEndCode.PlayWin)
		{
			aPlayerInfo.mWinStreak += 1;
			aPlayerInfo.mWin += 1;
			aPlayerInfo.mLoseStreak = 0;
		}
		else if (pBattleEndCode == BattleProtocol.BattleEndCode.PlayLose)
		{
			aPlayerInfo.mWinStreak = 0;
			aPlayerInfo.mLose += 1;
			aPlayerInfo.mLoseStreak++;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 테스트 맵 정보를 지정합니다.
	public void SetMapTestInfo(String pMapPath, String pGridLayoutPath)
	{
		GameData.get.aBattleTestInfo.aMapAssetKey = new AssetKey(-1, pMapPath);
		GameData.get.aBattleTestInfo.aGridLayoutAssetKey = new AssetKey(-1, pGridLayoutPath);
	}
	//-------------------------------------------------------------------------------------------------------
	// 테스트 맵 정보를 지정합니다.
	public int ConvertGoldToCash(int pGold)
	{
		int lResultCash = Mathf.FloorToInt(pGold * 0.045f);
		if (lResultCash <= 1)
			lResultCash = 1;
		return lResultCash;
	}
	//-------------------------------------------------------------------------------------------------------
	// 남은 시간 표시
	public String GetRemainTimeStr(DateTime pTargetTime)
	{
		TimeSpan lRemainTime = TimeUtil.GetRemainTime(pTargetTime);
		String lStrResult = string.Empty;
		if (lRemainTime.TotalHours > 24)
		{
			lStrResult = string.Format(LocalizedTable.get.GetLocalizedText("RemainDays{0}"), lRemainTime.Days);
		}
		else if (lRemainTime.TotalMinutes > 60)
		{
			lStrResult = string.Format(LocalizedTable.get.GetLocalizedText("RemainHours{0}{1}"), lRemainTime.Hours, lRemainTime.Minutes);
		}
		else if (lRemainTime.TotalSeconds > 60)
		{
			lStrResult = string.Format(LocalizedTable.get.GetLocalizedText("RemainMin{0}{1}"), lRemainTime.Minutes, lRemainTime.Seconds);
		}
		else
		{
			lStrResult = string.Format(LocalizedTable.get.GetLocalizedText("RemainSec{0}"), lRemainTime.Seconds);
		}
		return lStrResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 판넬 등록
	public void SetCommanderInfoPanel(MyArmyCommanderInfoPanel pCommanderInfoPanel)
	{
		mCommanderInfoPanel = pCommanderInfoPanel;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보 판넬
	public void SetActiveCommanderInfoPanel(bool pIsActive, CommanderItem pCommanderItem, CommanderItemData pCommanderItemData)
	{
		if (mCommanderInfoPanel == null)
			return;
		mIsActiveCommanderInfoPanel = pIsActive;
		mCommanderInfoPanel.vRoot.SetActive(pIsActive);
		if(pIsActive)
			mCommanderInfoPanel.SetCommander(pCommanderItem, pCommanderItemData);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 게임 단계에 해당하는 씬을 로딩합니다.
	private void _LoadGamePhaseScene(GamePhase pGamePhase, bool pIsForceReload)
	{
		if (BattlePlay.main != null)
			BattlePlay.main.DestroyBattle();

		mPrevGamePhase = mCurrentGamePhase;
		mCurrentGamePhase = pGamePhase;

		if ((Application.loadedLevelName.CompareTo(cGamePhaseSceneNames[(int)mCurrentGamePhase]) != 0) ||
			pIsForceReload)
			Application.LoadLevel(cGamePhaseSceneNames[(int)mCurrentGamePhase]);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static GameData sInstance = null;

	private GamePhase mPrevGamePhase;
	private GamePhase mCurrentGamePhase;

	private String mBattleServerURL;
	private JSONObject mBattleMatchInfoJson;
	private BattleVerifyData mBattleVerifyData;

	private BattleTestInfo mBattleTestInfo;

	private MyArmyCommanderInfoPanel mCommanderInfoPanel;
	private bool mIsActiveCommanderInfoPanel;
	//	private bool mIsAutoLogin;
}
