﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class GameUserInfo
{
	public const int cMaxDeckCount = 10;
	public uint mUniqueId;
	public String mUserName;
	public int mExp;
	public int mLevel;
	public int mCash;
	public int mGold;
	public int mWin;
	public int mLose;
	public int mWinStreak;
	public int mLoseStreak;
	public int mRating;
	public int mArea;
	public DeckNation mUseNation; 
	public int mUseDeckId;
	public String mUserKey;
	public Dictionary<DeckNation, NationData> mNationData;
	public List<CommanderItemData> mCommanderItemData;
	public Dictionary<int, DeckData> mCommanderDeckData;
	public List<DeckData> mCommanderDeckDataList;
	public bool mIsReplayData;
	public List<ReplayData> mReplayDataList;
	public int mClearTutorialIdx;
	// 상점 데이터
	public List<ShopChestData> mShopChestDataList;
	public List<ShopGoodsData> mShopGemDataList;
	public List<ShopGoodsData> mShopGoldDataList;
	public ShopCommanderItemInfoData mShopCommanderItemInfoData;
	// 상자 데이터
	public ChestUserData mChestUserData;

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public GameUserInfo()
	{
		mNationData = new Dictionary<DeckNation, NationData>();
		for (int iNation = 1; iNation < (int)DeckNation.Count; iNation++)
		{
			mNationData.Add((DeckNation)iNation, new NationData());
		}
		mCommanderItemData = new List<CommanderItemData>();
		mIsReplayData = false;
		mReplayDataList = new List<ReplayData>();

		mFindCommanderItemListIdxDic = new Dictionary<int, int>();
		mFindCommanderItemListNoDic = new Dictionary<int, int>();
		mCommanderDeckData = new Dictionary<int,DeckData>();
		mCommanderDeckDataList = new List<DeckData>();

		//mNationExps = new int[(int)DeckNation.Count];
		//mNationLevels = new int[(int)DeckNation.Count];

		mShopChestDataList = new List<ShopChestData>();
		mShopGemDataList = new List<ShopGoodsData>();
		mShopGoldDataList = new List<ShopGoodsData>();
		mShopCommanderItemInfoData = new ShopCommanderItemInfoData();

		mChestUserData = new ChestUserData();
		// 임시 Area 처리
		mArea = 1;
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화합니다.
	public void Reset()
	{
		mNationData.Clear();
		mCommanderItemData.Clear();
		mIsReplayData = false;
		mReplayDataList.Clear();

		mFindCommanderItemListIdxDic.Clear();
		mFindCommanderItemListNoDic.Clear();
		mCommanderDeckData.Clear();
		mCommanderDeckDataList.Clear();

		//for (int iNation = 0; iNation < (int)DeckNation.Count; iNation++)
		//{
		//	mNationExps[iNation] = 0;
		//	mNationLevels[iNation] = 1;
		//}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 번호의 유저인지 여부
	public bool CheckMine(int pUniqueId)
	{
		return (pUniqueId == mUniqueId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 셋팅
	public void SetCommanderItemList(List<CommanderItemData> pCommanderItemData)
	{
		mCommanderItemData = pCommanderItemData;
		for (int iCommanderItem = 0; iCommanderItem < mCommanderItemData.Count; iCommanderItem++)
		{
			mFindCommanderItemListIdxDic.Add(mCommanderItemData[iCommanderItem].mCommanderItemIdx, iCommanderItem);
			mFindCommanderItemListNoDic.Add(mCommanderItemData[iCommanderItem].mCommanderSpecNo, iCommanderItem);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 추가
	public void AddCommanderItem(CommanderItemData pCommanderItemData)
	{
		mCommanderItemData.Add(pCommanderItemData);
		mFindCommanderItemListIdxDic.Add(pCommanderItemData.mCommanderItemIdx, mCommanderItemData.Count - 1);
		mFindCommanderItemListNoDic.Add(pCommanderItemData.mCommanderSpecNo, mCommanderItemData.Count - 1);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 셋팅
	public void SetDeck(List<DeckData> pDeckData)
	{
		for (int iDeck = 0; iDeck < pDeckData.Count; iDeck++)
		{
			mCommanderDeckData.Add(pDeckData[iDeck].mDeckIdx, pDeckData[iDeck]);
		}
		mCommanderDeckDataList = pDeckData;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 받아오기
	public DeckData GetDeck(int pDeckIdx)
	{
		if (!mCommanderDeckData.ContainsKey(pDeckIdx))
		{
			return null;
		}
		return mCommanderDeckData[pDeckIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 업데이트
	public void UpdateDeck(DeckData pDeckData)
	{
		if (!mCommanderDeckData.ContainsKey(pDeckData.mDeckIdx))
		{
			Debug.LogError("Not Exist Deck idx - " + pDeckData.mDeckIdx);
			return;
		}
		DeckData.Copy(pDeckData, mCommanderDeckData[pDeckData.mDeckIdx]); 
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 추가
	public void AddDeck(DeckData pDeckData)
	{
		if (mCommanderDeckData.ContainsKey(pDeckData.mDeckIdx))
		{
			Debug.LogError("Already exist deck id - " + pDeckData.mDeckIdx);
			return;
		}
		mCommanderDeckData.Add(pDeckData.mDeckIdx, pDeckData);
		mCommanderDeckDataList.Add(pDeckData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 삭제
	public void RemoveDeck(int pDeckIdx)
	{
		if (!mCommanderDeckData.ContainsKey(pDeckIdx))
		{
			Debug.LogError("Not Exist Deck Id - " + pDeckIdx);
			return;
		}
		mCommanderDeckDataList.Remove(mCommanderDeckData[pDeckIdx]);
		mCommanderDeckData.Remove(pDeckIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 모든 덱을 얻어옵니다.
	public List<DeckData> GetAllDeckList()
	{
		return mCommanderDeckDataList;
	}
	//-------------------------------------------------------------------------------------------------------
	// GameUserInfo에 저장된 지휘관 아이템의 랭크업
	public void UpdateCommanderItemExp(int pItemIdx, int pRank, int pExp)
	{
		if (!mFindCommanderItemListIdxDic.ContainsKey(pItemIdx))
			Debug.LogError("Not Hold Item. That idx - " + pItemIdx);

		int lItemListIdx = mFindCommanderItemListIdxDic[pItemIdx];
		mCommanderItemData[lItemListIdx].mCommanderExp = pExp;
		mCommanderItemData[lItemListIdx].mCommanderExpRank = pRank;
	}
	//-------------------------------------------------------------------------------------------------------
	// GameUserInfo에 저장된 지휘관 아이템의 레벨업
	public CommanderItemData UpdateCommanderItemLevel(int pItemIdx, int pLevel, int pCount)
	{
		if (!mFindCommanderItemListIdxDic.ContainsKey(pItemIdx))
			Debug.LogError("Not Hold Item Idx - " + pItemIdx);
		
		int lItemListIdx = mFindCommanderItemListIdxDic[pItemIdx];
		mCommanderItemData[lItemListIdx].mCommanderLevel = pLevel;
		mCommanderItemData[lItemListIdx].mCommanderItemCount = pCount;

		return mCommanderItemData[lItemListIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// GameUserInfo에 저장된 지휘관 아이템의 갯수
	public CommanderItemData UpdateCommanderItemCount(int pItemIdx, int pItemNo, int pCount)
	{
		if (!mFindCommanderItemListIdxDic.ContainsKey(pItemIdx))
		{
			CommanderItemData lNewCommanderItemData = new CommanderItemData();
			lNewCommanderItemData.Init(pItemIdx, pItemNo);
			AddCommanderItem(lNewCommanderItemData);
		}

		int lItemListIdx = mFindCommanderItemListIdxDic[pItemIdx];
		mCommanderItemData[lItemListIdx].mCommanderItemCount = pCount;

		return mCommanderItemData[lItemListIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// Commander Item을 찾습니다.
	public CommanderItemData FindItemFromItemIdx(int pItemIdx)
	{
		if (mFindCommanderItemListIdxDic.ContainsKey(pItemIdx))
		{
			return mCommanderItemData[mFindCommanderItemListIdxDic[pItemIdx]];
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// Commander Item을 찾습니다. ItemNo를 이용
	public CommanderItemData FindItemFromItemNo(int pItemNo)
	{
		if (mFindCommanderItemListNoDic.ContainsKey(pItemNo))
		{
			return mCommanderItemData[mFindCommanderItemListNoDic[pItemNo]];
		}
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// Shop Commander Item 정보를 갱신합니다.
	public ShopCommanderItemData UpdateShopCommanderItem(int pShopIdx, int pBuyCount, int pRemainCount, int pPrice)
	{
		mShopCommanderItemInfoData.mCommanderList[pShopIdx].mCommanderItemBuyCount = pBuyCount;
		mShopCommanderItemInfoData.mCommanderList[pShopIdx].mCommanderItemRemainCount = pRemainCount;
		mShopCommanderItemInfoData.mCommanderList[pShopIdx].mPrice = pPrice;

		return mShopCommanderItemInfoData.mCommanderList[pShopIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자를 열경우 데이터를 갱신합니다.
	public ChestOpenData UpdateChestOpenData(JSONObject pChestOpen)
	{
		ChestOpenData lChestOpenSpec = new ChestOpenData();

		lChestOpenSpec.mChestType = (ChestType)pChestOpen.GetField("ChestType").n;
		lChestOpenSpec.mArea = (int)pChestOpen.GetField("Area").n;

		int lResultGold = (int)pChestOpen.GetField("Gold").n;
		int lResultCash = (int)pChestOpen.GetField("Cash").n;
		lChestOpenSpec.mGainGold = lResultGold - mGold;
		lChestOpenSpec.mTotalItemCount++;
		lChestOpenSpec.mGainGem = lResultCash - mCash;
		if (lChestOpenSpec.mGainGem > 0)
		{
			lChestOpenSpec.mIsGemActive = true;
			lChestOpenSpec.mTotalItemCount++;
		}

		mGold = lResultGold;
		mCash = lResultCash;

		JSONObject lItemsJson = pChestOpen.GetField("Items");
		for (int iItem = 0; iItem < lItemsJson.list.Count; iItem++)
		{
			int lItemId = (int)lItemsJson.list[iItem].GetField("ItemId").n;
			int lItemNo = (int)lItemsJson.list[iItem].GetField("ItemNo").n;
			int lItemCount = (int)lItemsJson.list[iItem].GetField("ItemCount").n;

			// 몇 개를 더 얻었는지 확인하는 데이터를 만듭니다.
			CommanderItemData lGainCommanderItemData = new CommanderItemData();
			CommanderItemData lHasCommanderItemData = FindItemFromItemIdx(lItemId);
			if (lHasCommanderItemData == null)
			{
				lGainCommanderItemData.mCommanderItemIdx = lItemId;
				lGainCommanderItemData.mCommanderItemCount = lItemCount;
				lGainCommanderItemData.mCommanderSpecNo = lItemNo;
				lChestOpenSpec.mNewCommanderItemIdList.Add(lItemId);
			}
			else
			{
				CommanderItemData.Copy(lHasCommanderItemData, lGainCommanderItemData);
				lGainCommanderItemData.mCommanderItemCount = lItemCount - lHasCommanderItemData.mCommanderItemCount;
			}
			lChestOpenSpec.mGainCommanderItemDataList.Add(lGainCommanderItemData);

			// 정식 데이터에 업데이트합니다.
			UpdateCommanderItemCount(lItemId, lItemNo, lItemCount);
			lChestOpenSpec.mTotalItemCount++;
		}
		return lChestOpenSpec;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<int, int> mFindCommanderItemListIdxDic;
	private Dictionary<int, int> mFindCommanderItemListNoDic;
}
