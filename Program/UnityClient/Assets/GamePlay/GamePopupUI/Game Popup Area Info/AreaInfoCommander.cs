﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AreaInfoCommander : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIcon;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UISprite vRankSprite;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIcon == null)
			Debug.LogError("vCommanderIcon is empty - " + this);
		if (vRankSprite == null)
			Debug.LogError("vRankSprite is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		GameData.get.SetActiveCommanderInfoPanel(true, mCommanderItem, mCommanderItemData);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 데이터를 입력합니다.
	public void SetCommander(CommanderItem pCommanderItem, CommanderItemData pCommanderItemData)
	{
		mCommanderItem = pCommanderItem;
		mCommanderItemData = pCommanderItemData;

		vCommanderIcon.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();
		vRankSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
}
