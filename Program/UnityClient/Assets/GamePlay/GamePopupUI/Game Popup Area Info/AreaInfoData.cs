﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class AreaInfoData : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<Transform> vAreaCommanderList;
	public AreaInfoCommander vAreaCommanderPrefab;
	public GameObject vRecentAreaObject;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vRecentAreaObject == null)
			Debug.LogError("vRecentAreaObject is empty - " + this);
		if (vAreaCommanderPrefab == null)
			Debug.LogError("vAreaCommanderPrefab is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 셋팅합니다.
	public void SetAreaCommander(int pArea, DeckNation pNation)
	{
		List<CommanderAreaSpec> lCommanderAreaSpecList = CommanderAreaTable.get.GetCommanderAreaSpecList(pArea, pNation);
		if (lCommanderAreaSpecList == null)
			return;

		for (int iCommander = 0; iCommander < lCommanderAreaSpecList.Count; iCommander++)
		{
			if (vAreaCommanderList.Count <= iCommander)
				break;

			GameObject lAreaCommanderObject = (GameObject)Instantiate<GameObject>(vAreaCommanderPrefab.gameObject);
			lAreaCommanderObject.transform.parent = vAreaCommanderList[iCommander];
			lAreaCommanderObject.transform.localPosition = Vector3.zero;
			lAreaCommanderObject.transform.localScale = Vector3.one;

			AreaInfoCommander lAreaInfoCommander = lAreaCommanderObject.GetComponent<AreaInfoCommander>();
			lAreaInfoCommander.SetCommander(lCommanderAreaSpecList[iCommander].mCommanderItem, lCommanderAreaSpecList[iCommander].mCommanderItemData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 지역을 표시합니다.
	public void SetActiveRecentArea(bool pIsActive)
	{
		vRecentAreaObject.SetActive(pIsActive);
	}

}
