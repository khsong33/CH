﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGroupCommanderListButton : InfiniteItemBehavior
{
	public CommanderItemData aCommanderItemData { get { return mCommanderItemData; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vButtonIndex;
	public UILabel vCommanderName;
	public UILabel vSpawnMpLabel;
	public UITexture vCommanderIconTexture;
	public UISlider vExpProgressBar;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemLevelLabel;
	public UISprite vRankColorSprite;
	public UISlider vCountProgressBar;
	public UILabel vCommanderItemCountLabel;

	public GameObject vSelectPopup;
	public UIButton vCommanderInfoButton;
	public UIButton vCommanderLevelUpButton;
	public UILabel vCommanderLevelUpCostLabel;

	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	public GamePopupBattleGroup vPopup;

	public BattleGroupDragComponent vDragComponent;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vPortraitIconTexture is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);
		if (vBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vBattleUITroopCountinfo is not enough count - " + this);
		if (vItemLevelLabel == null)
			Debug.LogError("vItemLevelLabel is empty -  " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vCommanderItemCountLabel == null)
			Debug.LogError("vCommanderItemCountLabel is empty - " + this);
		if (vSelectPopup == null)
			Debug.LogError("vSelectPopup is empty - " + this);
		if (vCommanderInfoButton == null)
			Debug.LogError("vCommanderInfoButton is empty - " + this);
		if (vCommanderLevelUpButton == null)
			Debug.LogError("vCommanderLevelUpButton is empty - " + this);
		if (vCommanderLevelUpCostLabel == null)
			Debug.LogError("vCommanderLevelUpCostLabel is empty - " + this);

		vDragComponent = GetComponent<BattleGroupDragComponent>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		vPopup = (GamePopupBattleGroup)GamePopupUIManager.aInstance.aCurrentPopup;
		if (vPopup == null)
			Debug.LogError("Not Exist Popup - " + this);

		SetActiveSelectPopup(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp += _CommanderLevelUp;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp -= _CommanderLevelUp;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		vPopup.OnClickCommander(gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 드래그 콜백
	void OnDrag(Vector2 pDelta)
	{
		vPopup.OnDragCommanderList(pDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 리스트 버튼에 들어갈 정보를 입력합니다.
	public override void SetItemData(object pData)
	{
		transform.localScale = Vector3.one;
		SetCommanderInfo(pData as CommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 정보를 입력합니다.
	public void SetCommanderInfo(CommanderItemData pCommanderItemData)
	{
		if (pCommanderItemData == null)
		{
			vCommanderName.text = String.Empty;
			vSpawnMpLabel.text = String.Empty;
			vCommanderIconTexture.mainTexture = null;
			vExpProgressBar.gameObject.SetActive(false);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(false);
			}
			vItemLevelLabel.text = String.Empty;
			vRankColorSprite.gameObject.SetActive(false);
			vCountProgressBar.gameObject.SetActive(false);
			vCommanderItemCountLabel.text = String.Empty;
		}
		else
		{
			vDragComponent.SetComponentType(BattleGroupDragComponent.BattleGroupButtonType.CommanderList);
			vDragComponent.SetCommanderItemData(pCommanderItemData);
			if (mCommanderItemData == null)
				mCommanderItemData = new CommanderItemData();

			CommanderItemData.Copy(pCommanderItemData, mCommanderItemData);
			vExpProgressBar.gameObject.SetActive(true);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(true);
			}

			mCommanderItem = new CommanderItem(pCommanderItemData.mCommanderItemIdx);
			mCommanderItem.Init(CommanderItemType.UserCommander, pCommanderItemData);
			vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

			vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
			vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();

			int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				if (iKind == lOpenIndex)
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
					vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
				}
				else
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
				}
			}

			// 아이템 레벨 
			CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
			if (lCommanderLevelSpec != null)
			{
				vItemLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();
				vRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
				vCommanderItemCountLabel.text = String.Format("{0}/{1}", pCommanderItemData.mCommanderItemCount,
					(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
				if (lCommanderLevelSpec.mNeedCard == 0)
					vCountProgressBar.value = 1.0f;
				else
					vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			CommanderItemCountState lCurrentItemState = CommanderItemCountState.Normal;
			//			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderLevel == lMaxLevel)
				lCurrentItemState = CommanderItemCountState.MaxCount;
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
				lCurrentItemState = CommanderItemCountState.AvailableLevelup;
			else
				lCurrentItemState = CommanderItemCountState.Normal;

			for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
			{
				if ((int)lCurrentItemState == iState)
				{
					vStateObject[iState].SetActive(true);
				}
				else
				{
					vStateObject[iState].SetActive(false);
				}
			}
			for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
			{
				vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)lCurrentItemState];
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 오브젝트의 숨김을 처리합니다.
	public void SetActiveSelectPopup(bool pIsActive)
	{
		vSelectPopup.SetActive(pIsActive);
		if (pIsActive)
		{
			CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
			bool lIsMaxlevel = CommanderLevelTable.get.IsMaxLevel(mCommanderItem);
			if (lCommanderLevelSpec.mNeedCard <= mCommanderItemData.mCommanderItemCount && !lIsMaxlevel)
			{
				vCommanderLevelUpButton.gameObject.SetActive(true);
				vCommanderLevelUpCostLabel.text = lCommanderLevelSpec.mCost.ToString();
				vCommanderInfoButton.gameObject.SetActive(false);
			}
			else
			{
				vCommanderLevelUpButton.gameObject.SetActive(false);
				vCommanderInfoButton.gameObject.SetActive(true);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 정보 보기 버튼 클릭
	public void OnClickInfo()
	{
		SetActiveSelectPopup(false);
		GameData.get.SetActiveCommanderInfoPanel(true, mCommanderItem, mCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨업 버튼 클릭
	public void OnClickLevelUp()
	{
		SetActiveSelectPopup(false);
		GameData.get.SetActiveCommanderInfoPanel(true, mCommanderItem, mCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 사용버튼 클릭
	public void OnClickUse()
	{
		SetActiveSelectPopup(false);
		vPopup.OnSetUpCommander(mCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 재설정시 사용
	public void UpdateCommanderInfo(CommanderItemData pCommanderItemData)
	{
		vPopup.UpdateListPanel();
//		SetCommanderInfo(pCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 레벨업시 갱신
	private void _CommanderLevelUp(CommanderItemData pCommanderItemData)
	{
		if (pCommanderItemData.mCommanderItemIdx == mCommanderItemData.mCommanderItemIdx)
			UpdateCommanderInfo(pCommanderItemData);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItemData mCommanderItemData;
	private CommanderItem mCommanderItem;
}
