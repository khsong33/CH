﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGroupCommanderPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public InfiniteListPopulator vCommanderListData;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vCommanderListData == null)
			Debug.LogError("vCommanderListData is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// CommanderItemList를 셋팅합니다.
	public void SetCommanderList(List<CommanderItemData> pCommanderItemDataList, int pDeckIdx, bool pIsChange = false)
	{
		mCurrentDeckIdx = pDeckIdx;
		StartCoroutine(_SetCommanderList(pCommanderItemDataList, pIsChange));
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderListButton Drag 처리
	public void OnDragCommanderList(Vector2 pDelta)
	{
		for (int iItem = 0; iItem < vCommanderListData.aItemPoolCount; iItem++)
		{
			BattleGroupCommanderListButton lButton = (BattleGroupCommanderListButton)vCommanderListData.GetItem(iItem).GetComponent<BattleGroupCommanderListButton>();
			lButton.SetActiveSelectPopup(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 덱 리스트를 출력합니다.
	private IEnumerator _SetCommanderList(List<CommanderItemData> pCommanderItemDataList, bool pIsChange)
	{
		yield return null;

		DeckData lDeckData = GameData.get.aPlayerInfo.GetDeck(mCurrentDeckIdx);
		ArrayList lCommanderButtonList = new ArrayList();
		for (int iButton = 0; iButton < pCommanderItemDataList.Count; iButton++)
		{
			bool lIsEquip = false;
			for (int iDeckItem = 0; iDeckItem < lDeckData.mDeckItems.Length; iDeckItem++)
			{
				if (lDeckData.mDeckItems[iDeckItem].mCommanderItemIdx == pCommanderItemDataList[iButton].mCommanderItemIdx)
				{
					lIsEquip = true;
				}
			}
			if (!lIsEquip)
				lCommanderButtonList.Add(pCommanderItemDataList[iButton]);
		}

		yield return null;

		if (!pIsChange)
			vCommanderListData.InitTableView(lCommanderButtonList, null, 0);
		else
			vCommanderListData.ChangeAllDataList(lCommanderButtonList);
	}

	public void SetActiveDrag(bool pIsActive)
	{
		vCommanderListData.draggablePanel.enabled = pIsActive;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mCurrentDeckIdx;
}
