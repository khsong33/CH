﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGroupDeckPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattleGroupDeckCommanderButton vCommanderButtonPrefab;
	public List<GameObject> vCommanderButtonRoot;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vCommanderButtonPrefab == null)
			Debug.LogError("vCommanderButtonPrefab is empty - " + this);

		mCommanderButtonList = new List<BattleGroupDeckCommanderButton>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 버튼 생성
	public void SetDeck(DeckData pDeckData)
	{
		mDeckData = pDeckData;
		StartCoroutine(_SetDeck());	
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 교체
	public void UpdateDeckCommanderItemData(CommanderItemData pCommanderItemData, int pSlotIdx)
	{
		mCommanderButtonList[pSlotIdx].SetDeckCommanderInfo(pCommanderItemData, pSlotIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 선택 효과를 시작합니다.(-1 : 전체 효과 적용)
	public void StartSelectEffect(int pExceptionSlotIdx)
	{
		for (int iButton = 0; iButton < mCommanderButtonList.Count; iButton++)
		{
			if (pExceptionSlotIdx != iButton)
				mCommanderButtonList[iButton].SetActiveSelectEffect(true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 선택 효과를 정지합니다.
	public void EndSelectEffect()
	{
		for (int iButton = 0; iButton < mCommanderButtonList.Count; iButton++)
		{
			mCommanderButtonList[iButton].SetActiveSelectEffect(false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴]지휘관 아이템 순차 출력
	private IEnumerator _SetDeck()
	{
		for (int iButton = 0; iButton < vCommanderButtonRoot.Count; iButton++)
		{
			yield return null;

			if (iButton >= mCommanderButtonList.Count)
			{
				GameObject lButtonObject = Instantiate<GameObject>(vCommanderButtonPrefab.gameObject);
				lButtonObject.transform.parent = vCommanderButtonRoot[iButton].transform;
				lButtonObject.transform.localPosition = Vector3.zero;
				lButtonObject.transform.localScale = Vector3.one;

				BattleGroupDeckCommanderButton lDeckCommanderData = lButtonObject.GetComponent<BattleGroupDeckCommanderButton>();
				lDeckCommanderData.SetDeckCommanderInfo(mDeckData.mDeckItems[iButton], iButton);
				mCommanderButtonList.Add(lDeckCommanderData);
			}
			else
			{
				mCommanderButtonList[iButton].SetDeckCommanderInfo(mDeckData.mDeckItems[iButton], iButton);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private DeckData mDeckData;
	private List<BattleGroupDeckCommanderButton> mCommanderButtonList;
}
