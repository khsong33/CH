﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGroupDragComponent : MonoBehaviour
{
	public enum BattleGroupButtonType
	{
		DeckButton,
		CommanderList,
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UIWidget vWidget;
	public float vDragableValue = 5.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void Start()
	{
		mTransform = transform;
	}
	void Update()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 드래그 콜백
	void OnDrag(Vector2 pDelta)
	{
		GamePopupBattleGroup lPopup = (GamePopupBattleGroup)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;

		if (mIsFocusDrag && (Mathf.Abs(pDelta.y) >= vDragableValue || lPopup.aIsFocusActive))
		{
			if (!lPopup.aIsFocusActive)
			{
				lPopup.vFocusCommander.transform.position = UICamera.lastHit.point;
			}

			if (mUIRoot == null)
				mUIRoot = NGUITools.FindInParents<UIRoot>(this.gameObject);

			float lUIPixelScale = (float)mUIRoot.activeHeight / Screen.height;
			pDelta.x *= lUIPixelScale;
			pDelta.y *= lUIPixelScale;

			lPopup.SetActiveFocus(true);
			SetActiveAlpha(false);
			lPopup.vFocusCommander.transform.localPosition = new Vector3(lPopup.vFocusCommander.transform.localPosition.x + pDelta.x,
																		 lPopup.vFocusCommander.transform.localPosition.y + pDelta.y,
																		 lPopup.vFocusCommander.transform.localPosition.z);
			if (UICamera.lastHit.collider.gameObject != mTargetObject)
			{
				if(mTargetObject != null)
					mTargetObject.SendMessage("OutTargetObject");
				mTargetObject = UICamera.lastHit.collider.gameObject;
				mTargetObject.SendMessage("InCollideTarget", mButtonType);
			}
			mIsDrag = true;
		}
		else 
		{
			mIsFocusDrag = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 터치 콜백
	void OnPress(bool pIsPressed)
	{
		GamePopupBattleGroup lPopup = (GamePopupBattleGroup)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;
		if (!pIsPressed && mIsFocusDrag && mIsDrag)
		{
			SetActiveAlpha(true);
			CommanderItemData lCurrentMyCommanderData = new CommanderItemData();
 			CommanderItemData.Copy(mCommanderItemData, lCurrentMyCommanderData);
			mTargetObject.SendMessage("ProcessFromObject", this.gameObject);
			mTargetObject.SendMessage("ColliderTarget", lCurrentMyCommanderData);
			lPopup.SetActiveFocus(false);
		}
		else
		{
			lPopup.vFocusCommander.SetCommanderInfo(mCommanderItemData);
		}
		// 위의 기능과 상관없이 버튼을 땠을 경우 아래의 버튼 처리를 진행
		if (!pIsPressed)
		{
			mIsFocusDrag = true;
			mIsDrag = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 알파값을 수정합니다.
	public void SetActiveAlpha(bool pIsAlpha)
	{
		vWidget.alpha = pIsAlpha ? 1.0f : 0.3f;
	}
	public void SetComponentType(BattleGroupButtonType pType)
	{
		mButtonType = pType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 셋팅합니다.
	public void SetCommanderItemData(CommanderItemData pCommanderItemData)
	{
		mCommanderItemData = pCommanderItemData;
	}
	//-------------------------------------------------------------------------------------------------------
	// 충돌이 일어난 오브젝트에 아이템을 전송합니다.
	public void ColliderTarget(CommanderItemData pCommanderItemData)
	{
		mTransform.localScale = Vector3.one;
		gameObject.SendMessage("UpdateCommanderInfo", pCommanderItemData);
		mIsFocusDrag = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 충돌이 진행중인 오브젝트의 반응
	public void InCollideTarget(BattleGroupButtonType pButtonType)
	{
		if (!(pButtonType == BattleGroupButtonType.CommanderList && mButtonType == BattleGroupButtonType.CommanderList))
		{
			mTransform.localScale = new Vector3(1.2f, 1.2f, 1.2f);
		}
	}
	public void OutTargetObject()
	{
		mTransform.localScale = Vector3.one;
	}
	public void ProcessFromObject(GameObject pFromObject)
	{
		CommanderItemData lCopyCommanderItemData = new CommanderItemData();
		CommanderItemData.Copy(mCommanderItemData, lCopyCommanderItemData);
		pFromObject.SendMessage("UpdateCommanderInfo", lCopyCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIRoot mUIRoot;
	private CommanderItemData mCommanderItemData;
	private bool mIsFocusDrag = true;
	private GameObject mTargetObject;
	private bool mIsDrag = false;
	private Transform mTransform;
	private bool mIsColliding = false;
	private BattleGroupButtonType mButtonType;
}
