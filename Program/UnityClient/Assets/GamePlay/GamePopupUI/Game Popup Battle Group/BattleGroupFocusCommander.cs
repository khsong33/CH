﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class BattleGroupFocusCommander : MonoBehaviour
{
	public CommanderItemData aCommanderItemData { get { return mCommanderItemData; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vCommanderName;
	public UILabel vSpawnMpLabel;
	public UITexture vCommanderIconTexture;
	public UISlider vExpProgressBar;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemLevelLabel;
	public UISprite vRankColorSprite;
	public UISlider vCountProgressBar;
	public UILabel vCommanderItemCountLabel;

	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vPortraitIconTexture is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);
		if (vBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vBattleUITroopCountinfo is not enough count - " + this);
		if (vItemLevelLabel == null)
			Debug.LogError("vItemLevelLabel is empty -  " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vCommanderItemCountLabel == null)
			Debug.LogError("vCommanderItemCountLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 정보를 입력합니다.
	public void SetCommanderInfo(CommanderItemData pCommanderItemData)
	{
		if (pCommanderItemData == null)
		{
			vCommanderName.text = String.Empty;
			vSpawnMpLabel.text = String.Empty;
			vCommanderIconTexture.mainTexture = null;
			vExpProgressBar.gameObject.SetActive(false);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(false);
			}
			vItemLevelLabel.text = String.Empty;
			vRankColorSprite.gameObject.SetActive(false);
			vCountProgressBar.gameObject.SetActive(false);
			vCommanderItemCountLabel.text = String.Empty;
		}
		else
		{
			if (mCommanderItemData == null)
				mCommanderItemData = new CommanderItemData();

			CommanderItemData.Copy(pCommanderItemData, mCommanderItemData);
			vExpProgressBar.gameObject.SetActive(true);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(true);
			}

			mCommanderItem = new CommanderItem(pCommanderItemData.mCommanderItemIdx);
			mCommanderItem.Init(CommanderItemType.UserCommander, pCommanderItemData);
			vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

			vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
			vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();

			int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				if (iKind == lOpenIndex)
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
					vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
				}
				else
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
				}
			}

			// 아이템 레벨 
			CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
			if (lCommanderLevelSpec != null)
			{
				vItemLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();
				vRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
				vCommanderItemCountLabel.text = String.Format("{0}/{1}", pCommanderItemData.mCommanderItemCount,
					(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
				if (lCommanderLevelSpec.mNeedCard == 0)
					vCountProgressBar.value = 1.0f;
				else
					vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			CommanderItemCountState lCurrentItemState = CommanderItemCountState.Normal;
			//			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderLevel == lMaxLevel)
				lCurrentItemState = CommanderItemCountState.MaxCount;
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
				lCurrentItemState = CommanderItemCountState.AvailableLevelup;
			else
				lCurrentItemState = CommanderItemCountState.Normal;

			for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
			{
				if ((int)lCurrentItemState == iState)
				{
					vStateObject[iState].SetActive(true);
				}
				else
				{
					vStateObject[iState].SetActive(false);
				}
			}
			for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
			{
				vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)lCurrentItemState];
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItemData mCommanderItemData;
	private CommanderItem mCommanderItem;
}
