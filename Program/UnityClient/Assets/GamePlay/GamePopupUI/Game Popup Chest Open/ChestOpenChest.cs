﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChestOpenChest : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vChestTexture;
	public UILabel vRemainCountLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public int aCurrentRemainItemCount { get { return mCurrentRemainItemCount; } }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vChestTexture == null)
			Debug.LogError("vChestTexture is empty - " + this);
		if(vRemainCountLabel == null)
			Debug.LogError("vRemainCountLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Chest 상태를 저장합니다.
	public void SetChestOpenSpec(ChestOpenData pChestOpenSpec)
	{
		mChestOpenSpec = pChestOpenSpec;
		mCurrentRemainItemCount = mChestOpenSpec.mTotalItemCount;

		ChestIconSpec pChestSpec = ShopChestTable.get.GetIconShopSpec(mChestOpenSpec.mArea, mChestOpenSpec.mChestType);

		vChestTexture.mainTexture = TextureResourceManager.get.LoadTexture(pChestSpec.mIconAssetKey);
		vRemainCountLabel.text = mCurrentRemainItemCount.ToString();
	}
	//-------------------------------------------------------------------------------------------------------
	// 남은 카드 카운트를 진행합니다.
	public void UpdateChestOpenCount()
	{
		mCurrentRemainItemCount--;
		vRemainCountLabel.text = mCurrentRemainItemCount.ToString();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ChestOpenData mChestOpenSpec;
	private int mCurrentRemainItemCount;
}
