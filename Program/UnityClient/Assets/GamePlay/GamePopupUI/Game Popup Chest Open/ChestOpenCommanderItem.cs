﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChestOpenCommanderItem : ChestOpenItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIconTexture;
	public UISprite vCommanderRankSprite;
	public UILabel vCommanderNameLabel;
	public UILabel vCommanderRankLabel;
	public UILabel vGainCountLabel;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vCommanderLevelLabel;
	public UILabel vItemCountLabel;
	public UISlider vCountProgressBar;
	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vCommanderRankSprite == null)
			Debug.LogError("vCommanderRankSprite is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if (vCommanderRankLabel == null)
			Debug.LogError("vCommanderRankLabel is empty - " + this);
		if (vGainCountLabel == null)
			Debug.LogError("vGainCountLabel is empty - " + this);
		if (vCommanderLevelLabel == null)
			Debug.LogError("vCommanderLevelLabel is empty - " + this);
		if (vItemCountLabel == null)
			Debug.LogError("vItemCountLabel is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// CommanderItemData 정보를 셋팅합니다.
	public void SetCommanderItemData(ChestOpenData pChestOpenSpec, int pCurrentCommanderItemIdx)
	{
		mGainCommanderItemData = pChestOpenSpec.mGainCommanderItemDataList[pCurrentCommanderItemIdx];
		if (GameData.get.aPlayerInfo == null)
		{
			mCommanderItemData = mGainCommanderItemData;
		}
		else
		{
			mCommanderItemData = GameData.get.aPlayerInfo.FindItemFromItemIdx(mGainCommanderItemData.mCommanderItemIdx);
		}

		if (pChestOpenSpec.mNewCommanderItemIdList.Contains(mCommanderItemData.mCommanderItemIdx))
		{
			// 새로운 아이템일 경우 처리
			vGainCountLabel.text = LocalizedTable.get.GetLocalizedText("UnlockKey");
		}
		else
		{
			vGainCountLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("GetCommanderKey{0}"), mGainCommanderItemData.mCommanderItemCount);
		}

		mCommanderItem = new CommanderItem(mCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);
		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		String lRankKey = String.Format("Rank{0}Key", mCommanderItem.aRank);
		vCommanderRankLabel.text = LocalizedTable.get.GetLocalizedText(lRankKey);
		vCommanderRankLabel.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
		vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}

		// 아이템 레벨 
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
		if (lCommanderLevelSpec != null)
		{
			vCommanderLevelLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("LevelKey{0}"), mCommanderItem.mCommanderLevel);
			vCommanderRankSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
			vItemCountLabel.text = String.Format("{0}/{1}", mCommanderItemData.mCommanderItemCount, 
				(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
			if (lCommanderLevelSpec.mNeedCard == 0)
			{
				vCountProgressBar.value = 1.0f;
			}
			else
			{
				vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			// int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderLevel == lMaxLevel)
			{
				mCurrentItemState = CommanderItemCountState.MaxCount;
			}
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
			{
				mCurrentItemState = CommanderItemCountState.AvailableLevelup;
			}
			else
			{
				mCurrentItemState = CommanderItemCountState.Normal;
			}
		}
		for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
		{
			if((int)mCurrentItemState == iState)
				vStateObject[iState].SetActive(true);
			else
				vStateObject[iState].SetActive(false);
		}
		for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
		{
			vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)mCurrentItemState];
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mGainCommanderItemData;
	private CommanderItemData mCommanderItemData;
	private CommanderItemCountState mCurrentItemState;
}
