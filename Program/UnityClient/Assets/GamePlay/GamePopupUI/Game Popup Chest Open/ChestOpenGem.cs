﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChestOpenGem : ChestOpenItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vGemTexture;
	public UILabel vGainGemLabel;
	public UILabel vTotalGemLabel;
	public GameObject vMaxGemMessageObject;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vGemTexture == null)
			Debug.LogError("vGemTexture is empty - " + this);
		if (vGainGemLabel == null)
			Debug.LogError("vGainGemLabel is empty - " + this);
		if (vTotalGemLabel == null)
			Debug.LogError("vTotalGemLabel is empty - " + this);
		if (vMaxGemMessageObject == null)
			Debug.LogError("vMaxGemMessageObject is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Gold 정보를 셋팅합니다.
	public void SetGemInfo(int pGainGem)
	{
		vGainGemLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("GetGemKey{0}"), pGainGem);
		if (GameData.get.aPlayerInfo != null)
			vTotalGemLabel.text = GameData.get.aPlayerInfo.mCash.ToString();
		else
			vTotalGemLabel.text = pGainGem.ToString();

		vMaxGemMessageObject.SetActive(false);
	}
}
