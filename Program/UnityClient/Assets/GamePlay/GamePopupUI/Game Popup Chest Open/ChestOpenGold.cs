﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ChestOpenGold : ChestOpenItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vGoldTexture;
	public UILabel vGainGoldLabel;
	public UILabel vTotalGoldLabel;
	public GameObject vMaxGoldMessageObject;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vGoldTexture == null)
			Debug.LogError("vGoldTexture is empty - " + this);
		if (vGainGoldLabel == null)
			Debug.LogError("vGainGoldLabel is empty - " + this);
		if (vTotalGoldLabel == null)
			Debug.LogError("vTotalGoldLabel is empty - " + this);
		if (vMaxGoldMessageObject == null)
			Debug.LogError("vMaxGoldMessageObject is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Gold 정보를 셋팅합니다.
	public void SetGoldInfo(int pGainGold)
	{
		vGainGoldLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("GetGoldKey{0}"), pGainGold);
		if (GameData.get.aPlayerInfo != null)
			vTotalGoldLabel.text = GameData.get.aPlayerInfo.mGold.ToString();
		else
			vTotalGoldLabel.text = pGainGold.ToString();

		vMaxGoldMessageObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
