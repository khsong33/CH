﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DeckButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vIconTexture;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UISprite vMpSprite;
	public UILabel vMpLabel;
	public GameObject vSetAvailbleObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public bool aIsEmpty { get { return vIconTexture.mainTexture == null; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }
	public CommanderItemData aCommanderItemData { get { return mCommanderItemData; } }
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		mButton = GetComponent<UIButton>();
		if (mButton == null)
			Debug.Log("can't find UIButton - " + this);

		EmptyItemData();
		//SetAvailableSetItem(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		mPopup.OnClickCommanderDeckItem(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 아이템 데이터를 셋팅합니다.
	public void SetItemData(int pSlotIdx, CommanderItemData pCommanderItemData)
	{
		if(mCommanderItem == null)
			mCommanderItem = new CommanderItem(pSlotIdx);


		mCommanderItemData = pCommanderItemData;
		mCommanderItem.Init(CommanderItemType.UserCommander, pCommanderItemData);

		vIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();

		vMpSprite.gameObject.SetActive(true);
		vMpLabel.text = mCommanderItem.aSpawnMp.ToString();

		mButton.defaultColor = BattleConfig.get.mRankColors[mCommanderItem.mCommanderExpRank];
		mButton.hover = mButton.defaultColor;

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		//BattleUITroopCountInfo lCurrentTroopKindCountInfo = null;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				//lCurrentTroopKindCountInfo = vBattleUITroopCountInfo[iKind];
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}
		SetAvailableSetItem(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템을 비웁니다.
	public void EmptyItemData()
	{
		vIconTexture.mainTexture = null;
		vMpLabel.text = String.Empty;
		
		mButton.defaultColor = Color.white;
		mButton.hover = Color.white;

		vMpSprite.gameObject.SetActive(false);

		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
		}
		SetAvailableSetItem(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 셋팅 가능한 위치 표시
	public void SetAvailableSetItem(bool pActive)
	{
		vSetAvailbleObject.SetActive(pActive);
	}
	//-------------------------------------------------------------------------------------------------------
	// Popup Control을 셋팅합니다.
	public void SetPopup(GamePopupCommandItem pPopup)
	{
		mPopup = pPopup;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIButton mButton;

	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private GamePopupCommandItem mPopup;
}
