﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class DeckListButton : InfiniteItemBehavior
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vAssignedObject;
	public UITexture vIconTexture;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vMpLabel;
	public UILabel vNameLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public bool aIsAssigned { get { return mIsAssigned; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		vAssignedObject.SetActive(false);
		mIsAssigned = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 아이템 데이터를 셋팅합니다.
	public override void SetItemData(object pData)
	{
		mCommanderItemData = pData as CommanderItemData;
		mCommanderItem = new CommanderItem(itemDataIndex);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);

		vIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();
		vMpLabel.text = mCommanderItem.aSpawnMp.ToString();

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		//BattleUITroopCountInfo lCurrentTroopKindCountInfo = null;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				//lCurrentTroopKindCountInfo = vBattleUITroopCountInfo[iKind];
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}
		vNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);
		int lUseDeckId = GameData.get.aPlayerInfo.mUseDeckId;
		SetAssigned(lUseDeckId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 사용덱에 따라 버튼의 상태를 설정합니다.
	public void SetAssigned(int pUseDeckId)
	{
		DeckData lDeckData = GameData.get.aPlayerInfo.mCommanderDeckData[pUseDeckId];
		if (lDeckData != null && lDeckData.FindSlotIdFromItemIdx(mCommanderItem.mCommanderItemIdx) >= 0)
		{
			SetAssigned(true);
		}
		else
		{
			SetAssigned(false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 배치 여부를 확인합니다.
	public void SetAssigned(bool pActive)
	{
		vAssignedObject.SetActive(pActive);
		mIsAssigned = pActive;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItemData mCommanderItemData;
	private CommanderItem mCommanderItem;
	private bool mIsAssigned;
}
