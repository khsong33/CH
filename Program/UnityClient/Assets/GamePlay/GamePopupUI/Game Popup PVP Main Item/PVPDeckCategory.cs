﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PVPDeckCategory : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GamePopupPVPMain vPopupMain;
	public List<PVPDeckInfo> vPvpDeckInfoButton;
	public List<UIButton> vCategoryButton;
	public List<GameObject> vCategoryPanel;
	public GameObject vDeckLeftButton;
	public GameObject vDeckRightButton;
	public UILabel vDeckPageLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 초기화
	public void Init(List<DeckData> pDeckDataList)
	{
		mCurrentDeckPage = -1; // 초기화
		mDeckDataList = pDeckDataList;

		if ((pDeckDataList.Count % vPvpDeckInfoButton.Count == 0)
			|| (pDeckDataList.Count <= vPvpDeckInfoButton.Count))
			mMaxDeckPage = pDeckDataList.Count / vPvpDeckInfoButton.Count;
		else
			mMaxDeckPage = (pDeckDataList.Count / vPvpDeckInfoButton.Count) + 1;
		
		_SetDeckPage(0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 페이지 전환
	public void OnNextPage()
	{
		if (mCurrentDeckPage + 1 < mMaxDeckPage)
		{
			_SetDeckPage(mCurrentDeckPage + 1);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이전 페이지 전환
	public void OnPrevPage()
	{
		if (mCurrentDeckPage - 1 >= 0)
		{
			_SetDeckPage(mCurrentDeckPage - 1);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 덱 정보를 얻어옵니다.
	private void _SetDeckPage(int pPage)
	{
		if (mCurrentDeckPage == pPage)
			return;

		mCurrentDeckPage = pPage;
		int lStartDeckId = mCurrentDeckPage * vPvpDeckInfoButton.Count;
		for (int iDeck = 0; iDeck < vPvpDeckInfoButton.Count; iDeck++)
		{
			int lCurrentDeckIndex = lStartDeckId + iDeck;
			if (lCurrentDeckIndex >= mDeckDataList.Count)
			{
				vPvpDeckInfoButton[iDeck].gameObject.SetActive(false);
			}
			else
			{
				DeckData lDeckData = mDeckDataList[lCurrentDeckIndex];
				vPvpDeckInfoButton[iDeck].gameObject.SetActive(true);
				vPvpDeckInfoButton[iDeck].SetDeckInfo(lDeckData);
			}
		}

		if (mCurrentDeckPage == 0)
			vDeckLeftButton.SetActive(false);
		else
			vDeckLeftButton.SetActive(true);

		if (mCurrentDeckPage == (mMaxDeckPage - 1) || mMaxDeckPage == 0)
			vDeckRightButton.SetActive(false);
		else
			vDeckRightButton.SetActive(true);

		if (mDeckDataList.Count < vPvpDeckInfoButton.Count)
			vDeckPageLabel.text = String.Format("{0} / {1}", mCurrentDeckPage + 1, mMaxDeckPage + 1);
		else
			vDeckPageLabel.text = String.Format("{0} / {1}", mCurrentDeckPage + 1, mMaxDeckPage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mCurrentDeckPage; // 페이지는 0부터 시작 
	private int mMaxDeckPage;
	private List<DeckData> mDeckDataList;
}
