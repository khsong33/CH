﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PVPDeckInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vDeckBgIcon;
	public UITexture vDeckMarkIcon;
	public UILabel vDeckNameLabel;
	public GameObject vLockObject;
	public UILabel vLockCommanderLabel;
	public GamePopupPVPMain vPopup;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vDeckBgIcon == null)
			Debug.LogError("vDeckBgIcon is empty - " + this);
		if(vDeckMarkIcon == null)
			Debug.LogError("vDeckMarkIcon is empty - " + this);
		if(vDeckNameLabel == null)
			Debug.LogError("vDeckNameLabel is empty - " + this);
		if (vLockObject == null)
			Debug.LogError("vLockObject is empty - " + this);
		if(vLockCommanderLabel == null)
			Debug.LogError("(vLockCommanderLabel is empty - " + this);
		if(vPopup == null)
			Debug.LogError("vPopup is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭
	void OnClick()
	{
		if(!mIsLock)
			vPopup.OpenRegistePanel(mDeckData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 덱 정보를 셋팅합니다.
	public void SetDeckInfo(DeckData pDeckData)
	{
		mDeckData = pDeckData;

		vDeckNameLabel.text = mDeckData.mDeckName;

		NationSpec lNationSpec = NationTable.get.GetNationSpec(pDeckData.mDeckNation);
		vDeckBgIcon.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mBackgroundIcon);
		vDeckMarkIcon.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mMarkIcon);

		int lDeckCommanderCount = mDeckData.GetCommanderCount();
		if (lDeckCommanderCount != BattleUserInfo.cMaxCommanderItemCount)
		{
			vLockObject.SetActive(true);
			vLockCommanderLabel.text = String.Format("({0} / {1})", lDeckCommanderCount, BattleUserInfo.cMaxCommanderItemCount);
			mIsLock = true;
		}
		else
		{
			vLockObject.SetActive(false);
			mIsLock = false;
		}
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private DeckData mDeckData;
	private bool mIsLock;
}
