﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PVPRegistePanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vWaitObject;
	public UILabel vNationLevelLabel;
	
	public UISlider vNationExpGauge;
	public UILabel vDeckNameLabel;

	public UITexture vNationBgIcon;
	public UITexture vNationMarkIcon;

	public GamePopupPVPMain vPopup;

	public UIButton vStartButton;
	public UIButton vCancelButton;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vDeckNameLabel == null)
			Debug.LogError("vDeckNameLabel is empty - " + this);
		if(vWaitObject == null)
			Debug.LogError("vWaitObject is empty - " + this);
		if(vNationExpGauge == null)
			Debug.LogError("vNationExpGauge is empty - " + this);
		if(vNationLevelLabel == null)
			Debug.LogError("vNationLevelLabel is empty - " + this);
		if (vNationBgIcon == null)
			Debug.LogError("vNationBgIcon is empty - " + this);
		if(vNationMarkIcon == null)
			Debug.LogError("vNationMarkIcon is empty - " + this);
		if(vPopup == null)
			Debug.LogError("vPopup is empty - " + this);
		if(vStartButton == null)
			Debug.LogError("vStartButton is empty - " + this);
		if(vCancelButton == null)
			Debug.LogError("vCancelButton is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitRegiste += _OnWaitRegiste;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste += _OnWaitUnRegiste;
		}
		vWaitObject.SetActive(false);

		vStartButton.gameObject.SetActive(true);
		vCancelButton.gameObject.SetActive(false);

		//UIEffectControl.get.SetActiveEffect(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitRegiste -= _OnWaitRegiste;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste -= _OnWaitUnRegiste;
		}
		//UIEffectControl.get.SetActiveEffect(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 덱 정보를 셋팅합니다.
	public void SetDeckInfo(DeckData pDeckData)
	{
		mDeckData = pDeckData;

		int lExp = GameData.get.aPlayerInfo.mNationData[pDeckData.mDeckNation].mNationExp;
		int lLevel = GameData.get.aPlayerInfo.mNationData[pDeckData.mDeckNation].mNationLevel;
		NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(lLevel);
		if (lNationLevelSpec == null)
		{
			Debug.LogError("Not Exist NationLevel Data");
			return;
		}

		// 데이터를 셋팅합니다.
		vDeckNameLabel.text = pDeckData.mDeckName;

		float lExpRatio = (float)(lExp - lNationLevelSpec.mNeedExp) / (lNationLevelSpec.mMaxExp - lNationLevelSpec.mNeedExp);
		vNationExpGauge.value = lExpRatio;

		vNationLevelLabel.text = lLevel.ToString();

		NationSpec lNationSpec = NationTable.get.GetNationSpec(pDeckData.mDeckNation);
		
		vNationBgIcon.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mBackgroundIcon);
		vNationMarkIcon.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mMarkIcon);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 버튼을 누릅니다.
	public void OnRegiste()
	{
		GameData.get.aLobbyControl.GoWaitRegiste(mDeckData.mDeckIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 뒤로가기 버튼을 누릅니다.
	public void OnBack()
	{
		if (GameData.get.aLobbyControl.aIsWaitGame)
			return;
		else
			gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [서버콜백] PVP 대기
	private void _OnWaitRegiste()
	{
		vWaitObject.SetActive(true);
		vStartButton.gameObject.SetActive(false);
		vCancelButton.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// [서버콜백] PVP 대기 취소
	private void _OnWaitUnRegiste()
	{
		vWaitObject.SetActive(false);
		vStartButton.gameObject.SetActive(true);
		vCancelButton.gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private DeckData mDeckData;
}
