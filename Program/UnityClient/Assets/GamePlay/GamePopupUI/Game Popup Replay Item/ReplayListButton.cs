﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ReplayListButton : InfiniteItemBehavior
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UISprite vSelectSprite;
	public UILabel vTimeLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public String aLogKey { get { return mReplayData.mLogKey; } }
	public bool aIsAvailable { get { return mReplayData != null; } }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		if (vSelectSprite == null)
			Debug.LogError("vSelectSprite is empty - " + this.name);
		if (vTimeLabel == null)
			Debug.LogError("vTimeLabel is empty - " + this.name);

		OnSelectButton(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 아이템 데이터를 셋팅합니다.
	public override void SetItemData(object pData)
	{
		mReplayData = pData as ReplayData;
		//Debug.Log("Set ReplayData - " + lReplayData.mIndex);
		vTimeLabel.text = String.Format("Replay From {0}", mReplayData.mLogTime.ToString());
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터를 선택합니다.
	public void OnSelectButton(bool pIsActive)
	{
		vSelectSprite.gameObject.SetActive(pIsActive);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ReplayData mReplayData;
}
