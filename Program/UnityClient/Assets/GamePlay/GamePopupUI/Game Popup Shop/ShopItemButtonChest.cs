﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopItemButtonChest : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vAreaNameLabel;
	public UILabel vChestNameLabel;
	public UITexture vChestIconTexture;
	public UILabel vPriceLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vAreaNameLabel == null)
			Debug.LogError("vTypeNameLabel is empty - " + this);
		if (vChestNameLabel == null)
			Debug.LogError("vChestNameLabel is empty - " + this);
		if (vChestIconTexture == null)
			Debug.LogError("vChestIconTexture is empty - " + this);
		if (vPriceLabel == null)
			Debug.LogError("vPriceLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.aCurrentPopup;
		lPopup.SetActiveShopPopup(ShopType.Chest, true, mButtonIndex);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Chest Data를 Setting 합니다.
	public void SetChestData(ShopChestData pShopChestData, int pButtonIndex)
	{
		mShopChestData = pShopChestData;
		mShopChestSpec = ShopChestTable.get.GetIconShopSpec(mShopChestData.mArea, mShopChestData.mType);

		vAreaNameLabel.text = String.Format("{0} {1}", LocalizedTable.get.GetLocalizedText("Area"), GameData.get.aPlayerInfo.mArea);
		vChestNameLabel.text = LocalizedTable.get.GetLocalizedText(mShopChestSpec.mNameKey);

		vChestIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(mShopChestSpec.mIconAssetKey);

		vPriceLabel.text = mShopChestData.mPrice.ToString();
		if (mShopChestData.mPrice > GameData.get.aPlayerInfo.mCash)
			vPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vPriceLabel.color = new Color32(255, 255, 255, 255);

		mButtonIndex = pButtonIndex;
	}
	//-------------------------------------------------------------------------------------------------------
	// Chest Data를 재설정합니다.
	public void RefreshButton()
	{
		if (mShopChestData.mPrice > GameData.get.aPlayerInfo.mCash)
			vPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vPriceLabel.color = new Color32(255, 255, 255, 255);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mButtonIndex;
	private ShopChestData mShopChestData;
	private ChestIconSpec mShopChestSpec;
}
