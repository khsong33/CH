﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopItemButtonCommander : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIconTexture;
	public Shader vEnableIconTextureShader;
	public Shader vDisableIconTextureShader;
	public UISprite vCommanderRankSprite;
	public UILabel vCommanderNameLabel;
	public UILabel vCommanderRankLabel;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemCountLabel;
	public UISlider vCountProgressBar;
	public UILabel vItemPriceLabel;
	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vCommanderRankSprite == null)
			Debug.LogError("vCommanderRankSprite is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if (vCommanderRankLabel == null)
			Debug.LogError("vCommanderRankLabel is empty - " + this);
		if (vItemCountLabel == null)
			Debug.LogError("vItemCountLabel is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vItemPriceLabel == null)
			Debug.LogError("vItemPriceLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.aCurrentPopup;
		lPopup.SetActiveShopPopup(ShopType.Commander, true, mButtonIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 버튼을 셋팅합니다.
	public void SetCommanderItem(ShopCommanderItemData pShopCommanderItemData, int pButtonIndex)
	{
		mShopCommanderItemData = pShopCommanderItemData;
		mCommanderItemData = GameData.get.aPlayerInfo.FindItemFromItemNo(mShopCommanderItemData.mCommanderItemNo);
		// 새로운 아이템이라면
		if (mCommanderItemData == null)
		{
			mCommanderItemData = new CommanderItemData();
			mCommanderItemData.mCommanderItemIdx = 99;
			mCommanderItemData.mCommanderSpecNo = mShopCommanderItemData.mCommanderItemNo;
			mCommanderItemData.mCommanderItemCount = 0;
			mCommanderItemData.mCommanderLevel = 1;
			mCommanderItemData.mCommanderExp = 0;
			mCommanderItemData.mCommanderExpRank = 1;
			mCommanderItemData.mSpawnCount = 1;
		}

		mCommanderItem = new CommanderItem(mCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);
		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		String lRankKey = String.Format("Rank{0}Key", mCommanderItem.aRank);
		vCommanderRankLabel.text = LocalizedTable.get.GetLocalizedText(lRankKey);
		vCommanderRankLabel.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
		vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();
		if (pShopCommanderItemData.mCommanderItemRemainCount > 0)
		{
			vCommanderIconTexture.shader = vEnableIconTextureShader;
		}
		else
		{
			vCommanderIconTexture.shader = vDisableIconTextureShader;
		}
		vItemPriceLabel.text = pShopCommanderItemData.mPrice.ToString();
		if (pShopCommanderItemData.mPrice > GameData.get.aPlayerInfo.mGold)
			vItemPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vItemPriceLabel.color = new Color32(255, 255, 255, 255);

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}

		// 아이템 레벨 
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
		if (lCommanderLevelSpec != null)
		{
			vCommanderRankSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
			vItemCountLabel.text = String.Format("{0}/{1}", mCommanderItemData.mCommanderItemCount, 
				(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
			if (lCommanderLevelSpec.mNeedCard == 0)
			{
				vCountProgressBar.value = 1.0f;
			}
			else
			{
				vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderItemCount >= lMaxItemCount)
			{
				mCurrentItemState = CommanderItemCountState.MaxCount;
			}
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
			{
				mCurrentItemState = CommanderItemCountState.AvailableLevelup;
			}
			else
			{
				mCurrentItemState = CommanderItemCountState.Normal;
			}
		}
		for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
		{
			if ((int)mCurrentItemState == iState)
				vStateObject[iState].SetActive(true);
			else
				vStateObject[iState].SetActive(false);
		}
		for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
		{
			vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)mCurrentItemState];
		}

		mButtonIndex = pButtonIndex;
	}
	//-------------------------------------------------------------------------------------------------------
	// 표시 정보 재설정
	public void RefreshButton()
	{
		if (mShopCommanderItemData.mPrice > GameData.get.aPlayerInfo.mGold)
			vItemPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vItemPriceLabel.color = new Color32(255, 255, 255, 255);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private CommanderItemCountState mCurrentItemState;
	private ShopCommanderItemData mShopCommanderItemData;
	private int mButtonIndex;
}
