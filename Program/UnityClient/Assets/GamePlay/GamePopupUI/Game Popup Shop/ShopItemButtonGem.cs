﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopItemButtonGem : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vItemIconTexture;
	public UILabel vItemCountLabel;
	public UILabel vItemNameLabel;
	public UILabel vItemPriceLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vItemIconTexture == null)
			Debug.LogError("vItemIconTexture is empty - " + this);
		if (vItemCountLabel == null)
			Debug.LogError("vItemCountLabel is empty - " + this);
		if (vItemNameLabel == null)
			Debug.LogError("vItemNameLabel is empty - " + this);
		if (vItemPriceLabel == null)
			Debug.LogError("vItemPriceLabel is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭
	void OnClick()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ShopBuyGem(mShopGoodsData.mGoodsNo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Gem Item 정보 입력
	public void SetGemData(ShopGoodsData pShopGoodsData, int pButtonIndex)
	{
		mShopGoodsData = pShopGoodsData;
		mShopGoodsSpec = ShopGoodsTable.get.GetShopSpec(mShopGoodsData.mGoodsNo);

		vItemNameLabel.text = LocalizedTable.get.GetLocalizedText(mShopGoodsSpec.mNameKey);
		vItemCountLabel.text = mShopGoodsData.mCount.ToString();
		vItemIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(mShopGoodsSpec.mIconAssetKey);
		vItemPriceLabel.text = String.Format("${0:0.00}", mShopGoodsData.mPriceCash);

		//mButtonIndex = pButtonIndex;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ShopGoodsData mShopGoodsData;
	private ShopGoodsSpec mShopGoodsSpec;
	//private int mButtonIndex;
}
