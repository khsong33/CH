﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopItemButtonGold : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vItemIconTexture;
	public UILabel vItemCountLabel;
	public UILabel vItemNameLabel;
	public UILabel vItemPriceLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vItemIconTexture == null)
			Debug.LogError("vItemIconTexture is empty - " + this);
		if (vItemCountLabel == null)
			Debug.LogError("vItemCountLabel is empty - " + this);
		if (vItemNameLabel == null)
			Debug.LogError("vItemNameLabel is empty - " + this);
		if (vItemPriceLabel == null)
			Debug.LogError("vItemPriceLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.aCurrentPopup;
		lPopup.SetActiveShopPopup(ShopType.Gold, true, mButtonIndex);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Gold Item 정보 입력
	public void SetGoldData(ShopGoodsData pShopGoodsData, int pButtonIndex)
	{
		mShopGoodsData = pShopGoodsData;
		mShopGoodsSpec = ShopGoodsTable.get.GetShopSpec(mShopGoodsData.mGoodsNo);

		vItemNameLabel.text = LocalizedTable.get.GetLocalizedText(mShopGoodsSpec.mNameKey);
		vItemCountLabel.text = mShopGoodsData.mCount.ToString();
		vItemIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(mShopGoodsSpec.mIconAssetKey);
		vItemPriceLabel.text = String.Format("{0}", mShopGoodsData.mPriceGem);
		if (mShopGoodsData.mPriceGem > GameData.get.aPlayerInfo.mCash)
			vItemPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vItemPriceLabel.color = new Color32(255, 255, 255, 255);
		mButtonIndex = pButtonIndex;
	}
	//-------------------------------------------------------------------------------------------------------
	// Gold Item 정보 재설정
	public void RefreshButton()
	{
		if (mShopGoodsData.mPriceGem > GameData.get.aPlayerInfo.mCash)
			vItemPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vItemPriceLabel.color = new Color32(255, 255, 255, 255);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ShopGoodsData mShopGoodsData;
	private ShopGoodsSpec mShopGoodsSpec;
	private int mButtonIndex;
}
