﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopItemList : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public ShopType vShopType;
	public GameObject vButtonItemPrefab;
	public List<Transform> vButtonPositionList;
	public UILabel vTimeLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vButtonItemPrefab == null)
			Debug.LogError("vButtonItemPrefab is empty - " + this);
		mShopButtons = new Dictionary<int, GameObject>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander += _OnShopBuyCommander;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander -= _OnShopBuyCommander;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백 
	void Update()
	{
		if (vTimeLabel != null)
		{
			if (vShopType == ShopType.Commander)
				vTimeLabel.text = GameData.get.GetRemainTimeStr(GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mResetTime);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 상점을 셋팅합니다.
	public void SetShopList()
	{
		for (int iButton = 0; iButton < vButtonPositionList.Count; iButton++)
		{
			switch (vShopType)
			{
				case ShopType.Commander:
					if (GameData.get.aPlayerInfo != null)
					{
						if (GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList[iButton].mCommanderItemNo >= 0)
						{
							GameObject lButtonObject = _CreateButtonObject(iButton);
							ShopItemButtonCommander lCommanderButton = lButtonObject.GetComponent<ShopItemButtonCommander>();
							lCommanderButton.SetCommanderItem(GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList[iButton], iButton);
						}
					}
					break;
				case ShopType.Chest:
					if (GameData.get.aPlayerInfo != null 
						&& GameData.get.aPlayerInfo.mShopChestDataList.Count > 0)
					{
						GameObject lButtonObject = _CreateButtonObject(iButton);
						ShopItemButtonChest lChestButton = lButtonObject.GetComponent<ShopItemButtonChest>();
						lChestButton.SetChestData(GameData.get.aPlayerInfo.mShopChestDataList[iButton], iButton);
					}
					break;
				case ShopType.Gem:
					if (GameData.get.aPlayerInfo != null)
					{
						GameObject lButtonObject = _CreateButtonObject(iButton);
						ShopItemButtonGem lGemButton = lButtonObject.GetComponent<ShopItemButtonGem>();
						lGemButton.SetGemData(GameData.get.aPlayerInfo.mShopGemDataList[iButton], iButton);
					}
					break;
				case ShopType.Gold:
					if (GameData.get.aPlayerInfo != null)
					{
						GameObject lButtonObject = _CreateButtonObject(iButton);
						ShopItemButtonGold lGoldButton = lButtonObject.GetComponent<ShopItemButtonGold>();
						lGoldButton.SetGoldData(GameData.get.aPlayerInfo.mShopGoldDataList[iButton], iButton);
					}
					break;
				default:
					break;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 상점 버튼을 재설정합니다.
	public void RefreshShopList()
	{
		for (int iButton = 0; iButton < mShopButtons.Count; iButton++)
		{
			switch (vShopType)
			{
				case ShopType.Commander:
					ShopItemButtonCommander lShopCommanderButton = mShopButtons[iButton].GetComponent<ShopItemButtonCommander>();
					lShopCommanderButton.RefreshButton();
					break;
				case ShopType.Chest:
					ShopItemButtonChest lShopChestButton = mShopButtons[iButton].GetComponent<ShopItemButtonChest>();
					lShopChestButton.RefreshButton();
					break;
				case ShopType.Gem:
					break;
				case ShopType.Gold:
					ShopItemButtonGold lShopGoldButton = mShopButtons[iButton].GetComponent<ShopItemButtonGold>();
					lShopGoldButton.RefreshButton();
					break;
				default:
					break;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 오브젝트 생성
	private GameObject _CreateButtonObject(int pButtonIdx)
	{
		GameObject lButtonObject = null;
		if (mShopButtons.ContainsKey(pButtonIdx))
		{
			return mShopButtons[pButtonIdx];
		}
		else
		{
			lButtonObject = Instantiate<GameObject>(vButtonItemPrefab);
			lButtonObject.transform.parent = vButtonPositionList[pButtonIdx];
			lButtonObject.transform.localPosition = Vector3.zero;
			lButtonObject.transform.localScale = Vector3.one;

			mShopButtons.Add(pButtonIdx, lButtonObject);
		}

		return lButtonObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// Shop Commander Item 구매
	private void _OnShopBuyCommander(int pUpdateShopIdx)
	{
		ShopItemButtonCommander lShopItemButtonCommander = mShopButtons[pUpdateShopIdx].GetComponent<ShopItemButtonCommander>();
		lShopItemButtonCommander.SetCommanderItem(GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList[pUpdateShopIdx], pUpdateShopIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	public Dictionary<int, GameObject> mShopButtons;
}
