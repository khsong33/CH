﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopPopupChest : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public ShopType vShopType = ShopType.Chest;
	public UITexture vChestTexture;
	public UILabel vChestAreaLabel;
	public UILabel vChestNameLabel;
	public UILabel vChestGoldLabel;
	public UILabel vChestTotalItemLabel;

	public GameObject vRank2Object;
	public UILabel vRank2CountLabel;
	public GameObject vRank3Object;
	public UILabel vRank3CountLabel;

	public UILabel vPriceLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vChestTexture == null)
			Debug.LogError("vChestTexture is empty - " + this);
		if(vChestAreaLabel == null)
			Debug.LogError("vChestAreaLabel is empty - " + this);
		if(vChestNameLabel == null)
			Debug.LogError("vChestNameLabel is empty - " + this);
		if(vChestGoldLabel == null)
			Debug.LogError("vChestGoldLabel is empty - " + this);
		if(vChestTotalItemLabel == null)
			Debug.LogError("vChestTotalItemLabel is empty - " + this);
		if(vRank2Object == null)
			Debug.LogError("vRank2Object is empty - " + this);
		if (vRank2CountLabel == null)
			Debug.LogError("vRank2CountLabel is empty - " + this);
		if (vRank3Object == null)
			Debug.LogError("vRank3Object is empty - " + this);
		if (vRank3CountLabel == null)
			Debug.LogError("vRank3CountLabel is empty - " + this);
		if(vPriceLabel == null)
			Debug.LogError("vPriceLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 상점을 셋팅합니다.
	public void SetPopupChest(ShopChestData pShopChestData)
	{
		mShopChestData = pShopChestData;
		mShopChestSpec = ShopChestTable.get.GetIconShopSpec(mShopChestData.mArea, mShopChestData.mType);
		mChestSpec = ChestTable.get.GetChestSpec(mShopChestData.mArea, mShopChestData.mType);

		vChestTexture.mainTexture = TextureResourceManager.get.LoadTexture(mShopChestSpec.mIconAssetKey);
		vChestAreaLabel.text = String.Format("{0} {1}", LocalizedTable.get.GetLocalizedText("Area"), GameData.get.aPlayerInfo.mArea);
		vChestNameLabel.text = LocalizedTable.get.GetLocalizedText(mShopChestSpec.mNameKey);
		vChestGoldLabel.text = String.Format("{0}-{1}", mChestSpec.mGoldMin, mChestSpec.mGoldMax);
		vChestTotalItemLabel.text = String.Format("X{0}", mChestSpec.mTotalCardCount);
		if (mChestSpec.mRank2CardCount <= 0)
		{
			vRank2Object.SetActive(false);
		}
		else
		{
			vRank2Object.SetActive(true);
			vRank2CountLabel.text = String.Format("X{0}", mChestSpec.mRank2CardCount);
		}
		if (mChestSpec.mRank3CardCount <= 0)
		{
			vRank3Object.SetActive(false);
		}
		else
		{
			vRank3Object.SetActive(true);
			vRank3CountLabel.text = String.Format("X{0}", mChestSpec.mRank3CardCount);
		}
		vPriceLabel.text = mShopChestData.mPrice.ToString();
		if (mShopChestData.mPrice > GameData.get.aPlayerInfo.mCash)
			vPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vPriceLabel.color = new Color32(255, 255, 255, 255);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구매 버튼 클릭
	public void GoBuyButton()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ShopBuyChest((int)mShopChestData.mType);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ShopChestData mShopChestData;
	private ChestIconSpec mShopChestSpec;
	private ChestSpec mChestSpec;
}
