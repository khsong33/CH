﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopPopupCommander : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIconTexture;
	public UILabel vCommanderConfirmLabel;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemCountLabel;
	public UISlider vCountProgressBar;
	public UILabel vItemPriceLabel;
	public UILabel vRemainCountLabel;
	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIconTexture == null)
			Debug.LogError("vCommanderIconTexture is empty - " + this);
		if (vCommanderConfirmLabel == null)
			Debug.LogError("vCommanderConfirmLabel is empty - " + this);
		if (vItemCountLabel == null)
			Debug.LogError("vItemCountLabel is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vItemPriceLabel == null)
			Debug.LogError("vItemPriceLabel is empty - " + this);
		if (vRemainCountLabel == null)
			Debug.LogError("vRemainCountLabel is empty - " + this);
		
		mUseGem = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// shop 지휘관 아이템을 셋팅합니다.
	public void SetPopupCommanderItem(ShopCommanderItemData pShopCommanderItemData)
	{
		mShopCommanderItemData = pShopCommanderItemData;
		mCommanderItemData = GameData.get.aPlayerInfo.FindItemFromItemNo(mShopCommanderItemData.mCommanderItemNo);
		// 새로운 아이템이라면
		if (mCommanderItemData == null)
		{
			mCommanderItemData = new CommanderItemData();
			mCommanderItemData.mCommanderItemIdx = 99;
			mCommanderItemData.mCommanderSpecNo = mShopCommanderItemData.mCommanderItemNo;
			mCommanderItemData.mCommanderItemCount = 0;
			mCommanderItemData.mCommanderLevel = 1;
			mCommanderItemData.mCommanderExp = 0;
			mCommanderItemData.mCommanderExpRank = 1;
			mCommanderItemData.mSpawnCount = 1;
		}

		mCommanderItem = new CommanderItem(mCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);
		String lCommanderName = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);
		vCommanderConfirmLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("CommanderBuyConfirm{0}"), lCommanderName);

		vRemainCountLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("CommanderRemainNum{0}"), mShopCommanderItemData.mCommanderItemRemainCount);

		vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();

		vItemPriceLabel.text = mShopCommanderItemData.mPrice.ToString();
		if (mShopCommanderItemData.mPrice > GameData.get.aPlayerInfo.mGold)
			vItemPriceLabel.color = new Color32(255, 0, 0, 255);
		else
			vItemPriceLabel.color = new Color32(255, 255, 255, 255);

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}

		// 아이템 레벨 
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
		if (lCommanderLevelSpec != null)
		{
			vItemCountLabel.text = String.Format("{0}/{1}", mCommanderItemData.mCommanderItemCount, 
				(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
			if (lCommanderLevelSpec.mNeedCard == 0)
			{
				vCountProgressBar.value = 1.0f;
			}
			else
			{
				vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderItemCount >= lMaxItemCount)
			{
				mCurrentItemState = CommanderItemCountState.MaxCount;
			}
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
			{
				mCurrentItemState = CommanderItemCountState.AvailableLevelup;
			}
			else
			{
				mCurrentItemState = CommanderItemCountState.Normal;
			}
		}
		for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
		{
			if ((int)mCurrentItemState == iState)
				vStateObject[iState].SetActive(true);
			else
				vStateObject[iState].SetActive(false);
		}
		for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
		{
			vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)mCurrentItemState];
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구매 버튼 클릭
	public void GoBuyButton()
	{
		if (GameData.get.aPlayerInfo.mGold < mShopCommanderItemData.mPrice)
		{
			GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.aCurrentPopup;
			lPopup.GoBackShopPopup();

			int lLackGold = mShopCommanderItemData.mPrice - GameData.get.aPlayerInfo.mGold;
			mUseGem = GameData.get.ConvertGoldToCash(lLackGold);
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_ENOUGH_GOLD"), MessageBoxManager.MessageBoxType.OK_Cancel_Price, _GoGemBuyButton);
			MessageBoxManager.aInstance.SetGemLabel(mUseGem);
		}
		else
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.Sendpacket_ShopBuyCommander(mShopCommanderItemData.mCommanderShopIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 디테일 정보 클릭
	public void GoDetailCommander()
	{
		GameData.get.SetActiveCommanderInfoPanel(true, mCommanderItem, mCommanderItemData);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 잼 구매 버튼 클릭
	private void _GoGemBuyButton()
	{
		if (mUseGem > GameData.get.aPlayerInfo.mCash)
		{
			MessageBoxManager.aInstance.SetSafeMessageBox(true);
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_ENOUGH_CASH"), MessageBoxManager.MessageBoxType.OK_Cancel, _GoGemShop);
		}
		else
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.Sendpacket_ShopBuyCommander(mShopCommanderItemData.mCommanderShopIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 잼 샵 이동
	private void _GoGemShop()
	{
		MessageBoxManager.aInstance.SetSafeMessageBox(false);
		GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.aCurrentPopup;
		lPopup.OnClickTapButton(ShopType.Gem);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private ShopCommanderItemData mShopCommanderItemData;
	private CommanderItemCountState mCurrentItemState;
	private int mUseGem;
}
