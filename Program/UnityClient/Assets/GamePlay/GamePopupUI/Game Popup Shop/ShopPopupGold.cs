﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopPopupGold : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vIconTexture;
	public UILabel vCountLabel;
	public UILabel vPriceLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vIconTexture == null)
			Debug.LogError("vIconTexture is empty - " + this);
		if (vCountLabel == null)
			Debug.LogError("vCountLabel is empty - " + this);
		if (vCountLabel == null)
			Debug.LogError("vCountLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Shop Gold 데이터를 셋팅합니다.
	public void SetPopupGold(ShopGoodsData pShopGoodsData)
	{
		mShopGoodsData = pShopGoodsData;
		mShopGoodsSpec = ShopGoodsTable.get.GetShopSpec(mShopGoodsData.mGoodsNo);
		vIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(mShopGoodsSpec.mIconAssetKey);
		vCountLabel.text = mShopGoodsData.mCount.ToString();
		vPriceLabel.text = mShopGoodsData.mPriceGem.ToString();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구매 버튼 클릭
	public void GoBuyButton()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ShopBuyGold(mShopGoodsData.mGoodsNo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ShopGoodsData mShopGoodsData;
	private ShopGoodsSpec mShopGoodsSpec;
}
