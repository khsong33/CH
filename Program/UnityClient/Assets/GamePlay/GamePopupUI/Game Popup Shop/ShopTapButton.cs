﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ShopTapButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GamePopupShop vPopup;
	public GameObject vSelectedObject;
	public ShopType vShopType;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vPopup == null)
			Debug.LogError("vPopup is empty - " + this);
		if (vSelectedObject == null)
			Debug.LogError("vSelectedObject is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 처리
	void OnClick()
	{
		vPopup.OnClickTapButton(vShopType);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------


	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
