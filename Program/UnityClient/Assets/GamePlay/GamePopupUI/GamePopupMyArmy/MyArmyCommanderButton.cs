﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyCommanderButton : MonoBehaviour
{
	public enum ButtonType
	{
		CommanderItem,
		BattleCommander,
	}
	public enum IconType
	{
		Portrait,
		BattleIcon
	}
	public CommanderItemData aCommanderItemData { get { return mCommanderItemData; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }
	public bool aIsShow { get { return mIsShow; } }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vButtonIndex;
	public UILabel vCommanderName;
	public UILabel vSpawnMpLabel;
	public UITexture vCommanderIconTexture;
	public UILabel vCommanderRankLabel;
	public UISlider vExpProgressBar;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemLevelLabel;
	public UISprite vRankColorSprite;
	public UISlider vCountProgressBar;
	public UILabel vCommanderItemCountLabel;

	public GameObject vSelectPopup;
	public UIButton vCommanderInfoButton;
	public UIButton vCommanderLevelUpButton;
	public UILabel vCommanderLevelUpCostLabel;

	public GameObject vShowObject;

	public IconType vIconType = IconType.Portrait;
	public ButtonType vButtonType = ButtonType.CommanderItem;

	public GameObject vEquipObject;

	public GamePopupMyArmy vPopup;

	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vPortraitIconTexture is empty - " + this);
		if (vCommanderRankLabel == null)
			Debug.LogError("vCommanderRankLabel is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);
		if (vBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vBattleUITroopCountinfo is not enough count - " + this);
		if (vItemLevelLabel == null)
			Debug.LogError("vItemLevelLabel is empty -  " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vCommanderItemCountLabel == null)
			Debug.LogError("vCommanderItemCountLabel is empty - " + this);
		if (vSelectPopup == null)
			Debug.LogError("vSelectPopup is empty - " + this);
		if (vCommanderInfoButton == null)
			Debug.LogError("vCommanderInfoButton is empty - " + this);
		if (vCommanderLevelUpButton == null)
			Debug.LogError("vCommanderLevelUpButton is empty - " + this);
		if (vCommanderLevelUpCostLabel == null)
			Debug.LogError("vCommanderLevelUpCostLabel is empty - " + this);
		if (vShowObject == null)
			Debug.LogError("vShowObject is empty - " + this);

		mIsDrag = false;
		mIsDragable = true;
		SetShow(true);

		vPopup = (GamePopupMyArmy)GamePopupUIManager.aInstance.aCurrentPopup;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		vPopup.OnClickCommander(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 드래그 콜백
	void OnDrag(Vector2 pDelta)
	{
		if (!mIsDragable)
			return;

		GamePopupMyArmy lPopup = (GamePopupMyArmy)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;

		if (mUIRoot == null)
			mUIRoot = NGUITools.FindInParents<UIRoot>(this.gameObject);

		float lUIPixelScale = (float)mUIRoot.activeHeight / Screen.height;
		pDelta.x *= lUIPixelScale;
		pDelta.y *= lUIPixelScale;

		lPopup.vFocusCommander.gameObject.SetActive(true);

		for (int iGroup = 0; iGroup < lPopup.vFocusColliderGroup.Count; iGroup++)
		{
			lPopup.vFocusColliderGroup[iGroup].SetActive(true);
		}

		lPopup.vFocusCommander.transform.localPosition = new Vector3(
			lPopup.vFocusCommander.transform.localPosition.x + pDelta.x,
			lPopup.vFocusCommander.transform.localPosition.y + pDelta.y,
			lPopup.vFocusCommander.transform.localPosition.z);

		if (!mIsDrag)
		{
			if (vButtonType == ButtonType.BattleCommander)
			{
				lPopup.vFocusCommander.SetFocusType(IconType.BattleIcon);
				lPopup.vFocusCommander.SetFocusPosition(MyArmyFocusCommander.FocusPosition.SetDeck);
			}
			else
			{
				lPopup.vFocusCommander.SetFocusType(IconType.Portrait);
				lPopup.vFocusCommander.SetFocusPosition(MyArmyFocusCommander.FocusPosition.Default);
			}
			if (lPopup.aOnPutupCommanderItem != null)
			{
				lPopup.aOnPutupCommanderItem(lPopup.vFocusCommander);
			}
		}
		else
		{
			if (UICamera.lastHit.collider.tag.Equals("Group Collider"))
			{
				lPopup.vFocusCommander.SetFocusType(IconType.BattleIcon);
				lPopup.vFocusCommander.SetFocusPosition(MyArmyFocusCommander.FocusPosition.SetDeck);
			}
			else
			{
				lPopup.vFocusCommander.SetFocusType(IconType.Portrait);
				lPopup.vFocusCommander.SetFocusPosition(MyArmyFocusCommander.FocusPosition.Default);
			}
		}

		mIsDrag = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 터치 콜백
	void OnPress(bool pIsPressed)
	{
		GamePopupMyArmy lPopup = (GamePopupMyArmy)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;
		if (!pIsPressed)
		{
			lPopup.vFocusCommander.gameObject.SetActive(false);
			for (int iGroup = 0; iGroup < lPopup.vFocusColliderGroup.Count; iGroup++)
			{
				lPopup.vFocusColliderGroup[iGroup].SetActive(false);
			}

			if (lPopup.aOnDropCommanderItem != null && mIsDrag)
				lPopup.aOnDropCommanderItem(lPopup.vFocusCommander);

			mIsDrag = false;
		}
		else
		{
			lPopup.vFocusCommander.transform.position = UICamera.lastHit.point;
			lPopup.vFocusCommander.SetButtonInfo(this);
			lPopup.vFocusCommander.SetCommanderInfo(mCommanderItemData);
			lPopup.vFocusCommander.SetFocusType(vIconType);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 정보를 입력합니다.
	public void SetCommanderInfo(CommanderItemData pCommanderItemData)
	{
		vPopup.ClearCommanderButton();
		if (pCommanderItemData == null)
		{
			vCommanderName.text = String.Empty;
			vSpawnMpLabel.text = String.Empty;
			vCommanderIconTexture.mainTexture = null;
			vCommanderRankLabel.text = String.Empty;
			vExpProgressBar.gameObject.SetActive(false);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(false);
			}
			vItemLevelLabel.text = String.Empty;
			vRankColorSprite.gameObject.SetActive(false);
			vCountProgressBar.gameObject.SetActive(false);
			vCommanderItemCountLabel.text = String.Empty;
		}
		else
		{
			if(mCommanderItemData == null)
				mCommanderItemData = new CommanderItemData();

			CommanderItemData.Copy(pCommanderItemData, mCommanderItemData);
			vExpProgressBar.gameObject.SetActive(true);
			for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
			{
				vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(true);
			}

			mCommanderItem = new CommanderItem(pCommanderItemData.mCommanderItemIdx);
			mCommanderItem.Init(CommanderItemType.UserCommander, pCommanderItemData);
			vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

			vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
			vCommanderRankLabel.text = mCommanderItem.mCommanderExpRank.ToString();
			if (vIconType == IconType.Portrait)
				vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();
			else if (vIconType == IconType.BattleIcon)
				vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();

			int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
			for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
			{
				if (iKind == lOpenIndex)
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
					vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
				}
				else
				{
					vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
				}
			}

			// 아이템 레벨 
			CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItem.mCommanderLevel);
			if (lCommanderLevelSpec != null)
			{
				vItemLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();
				vRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
				vCommanderItemCountLabel.text = String.Format("{0}/{1}", pCommanderItemData.mCommanderItemCount, 
					(lCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString()));
				if (lCommanderLevelSpec.mNeedCard == 0)
					vCountProgressBar.value = 1.0f;
				else
					vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;
			}

			CommanderItemCountState lCurrentItemState = CommanderItemCountState.Normal;
//			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderLevel == lMaxLevel)
				lCurrentItemState = CommanderItemCountState.MaxCount;
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
				lCurrentItemState = CommanderItemCountState.AvailableLevelup;
			else
				lCurrentItemState = CommanderItemCountState.Normal;

			for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
			{
				if ((int)lCurrentItemState == iState)
				{
					vStateObject[iState].SetActive(true);
				}
				else
				{
					vStateObject[iState].SetActive(false);
				}
			}
			for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
			{
				vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)lCurrentItemState];
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Commander panel 한정, 장착 상태를 처리합니다.
	public void SetEquip(bool pIsEquip)
	{
		if (vEquipObject != null)
		{
			SetDragable(!pIsEquip);
			vEquipObject.SetActive(pIsEquip);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// drag 가능 여부 처리
	public void SetDragable(bool pIsDragable)
	{
		mIsDragable = pIsDragable;
	}
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트의 숨김을 처리합니다.
	public void SetShow(bool pIsShow)
	{
		mIsShow = pIsShow;
		vShowObject.SetActive(pIsShow);
	}
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트의 숨김을 처리합니다.
	public void SetActiveSelectPopup(bool pIsActive)
	{
		vSelectPopup.SetActive(pIsActive);
		if (pIsActive)
		{
			CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
			bool lIsMaxlevel = CommanderLevelTable.get.IsMaxLevel(mCommanderItem);
			if (lCommanderLevelSpec.mNeedCard <= mCommanderItemData.mCommanderItemCount && !lIsMaxlevel)
			{
				vCommanderLevelUpButton.gameObject.SetActive(true);
				vCommanderLevelUpCostLabel.text = lCommanderLevelSpec.mCost.ToString();
				vCommanderInfoButton.gameObject.SetActive(false);
			}
			else 
			{
				vCommanderLevelUpButton.gameObject.SetActive(false);
				vCommanderInfoButton.gameObject.SetActive(true);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 정보 보기 버튼 클릭
	public void OnClickInfo()
	{
		GamePopupMyArmy lPopup = (GamePopupMyArmy)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;

		lPopup.OnOpenCommanderInfo(mCommanderItem, mCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨업 버튼 클릭
	public void OnClickLevelUp()
	{
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_CommanderItemLevelUp(mCommanderItemData);
		GamePopupMyArmy lPopup = (GamePopupMyArmy)GamePopupUIManager.aInstance.aCurrentPopup;
		if (lPopup == null)
			return;

		lPopup.OnOpenCommanderInfo(mCommanderItem, mCommanderItemData);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItemData mCommanderItemData;
	private CommanderItem mCommanderItem;
	private UIRoot mUIRoot;
	private bool mIsDragable;
	private bool mIsDrag;
	private bool mIsShow;
}
