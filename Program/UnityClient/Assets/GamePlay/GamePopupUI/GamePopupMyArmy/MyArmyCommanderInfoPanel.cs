﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyCommanderInfoPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vRoot;
	public MyArmyInfoCommander vCommanderInfo;
	public MyArmyInfoSkill vSkillInfo;
	public MyArmyInfoRankEffect vRankEffectInfo;
	public MyArmyInfoTroop vTroopInfo;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vRoot == null)
			Debug.LogError("vRoot is empty - " + this);
		if (vCommanderInfo == null)
			Debug.LogError("vCommanderInfo is empty - " + this);
		if (vSkillInfo == null)
			Debug.LogError("vSkillInfo is empty - " + this);
		if (vRankEffectInfo == null)
			Debug.LogError("vRankEffectInfo is empty - " + this);
		if (vTroopInfo == null)
			Debug.LogError("vTroopInfo is empty - " + this);

		GameData.get.SetCommanderInfoPanel(this);
		vRoot.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if(GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp += _OnCommanderLevelUp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp -= _OnCommanderLevelUp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴시 콜백
	void OnDestory()
	{
		GameData.get.SetCommanderInfoPanel(null);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 셋팅합니다.
	public void SetCommander(CommanderItem pCommanderItem, CommanderItemData pCommanderItemData)
	{
		mCommanderItemData = pCommanderItemData;
		mCommanderItem = pCommanderItem;
		vCommanderInfo.SetCommanderInfo(pCommanderItem, pCommanderItemData);
		vSkillInfo.SetSkillInfo(mCommanderItem);
		vRankEffectInfo.SetRankEffect(mCommanderItem);
		vTroopInfo.SetTroopInfo(mCommanderItem, mCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 레벨업합니다.
	public void GoLevelUpCommander()
	{
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
		if (GameData.get.aPlayerInfo.mGold < lCommanderLevelSpec.mCost)
		{
			GameData.get.SetActiveCommanderInfoPanel(false, null, null);
			int lLackGold = lCommanderLevelSpec.mCost - GameData.get.aPlayerInfo.mGold;
			mUseGem = GameData.get.ConvertGoldToCash(lLackGold);
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_ENOUGH_GOLD"), MessageBoxManager.MessageBoxType.OK_Cancel_Price, _GoGemBuyButton);
			MessageBoxManager.aInstance.SetGemLabel(mUseGem);
		}
		else
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_CommanderItemLevelUp(mCommanderItemData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 창을 닫습니다.
	public void ClosePanel()
	{
		GameData.get.SetActiveCommanderInfoPanel(false, null, null);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 레벨업 서버 콜백
	private void _OnCommanderLevelUp(CommanderItemData pCommanderItemData)
	{
		vCommanderInfo.RefreshCommanderItem(pCommanderItemData);
		vTroopInfo.SetTroopInfo(mCommanderItem, pCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 골드 부족시 잼 사용
	private void _GoGemBuyButton()
	{
		if (mUseGem > GameData.get.aPlayerInfo.mCash)
		{
			MessageBoxManager.aInstance.SetSafeMessageBox(true);
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_ENOUGH_CASH"), MessageBoxManager.MessageBoxType.OK_Cancel, _GoGemShop);
		}
		else
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_CommanderItemLevelUp(mCommanderItemData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 잼 부족시 상점 이동
	private void _GoGemShop()
	{
		MessageBoxManager.aInstance.SetSafeMessageBox(false);
		GamePopupShop lPopup = (GamePopupShop)GamePopupUIManager.aInstance.OpenPopup("Popup Shop Main");
		lPopup.OnClickTapButton(ShopType.Gem);
	}	
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private int mUseGem;
}
