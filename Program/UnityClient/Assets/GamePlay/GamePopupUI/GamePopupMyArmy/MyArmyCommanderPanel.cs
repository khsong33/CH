﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyCommanderPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vItemCountPerPage = 8;
	public Transform[] vOffsetTransform;
	public MyArmyCommanderButton vCommanderItemPrefab;
	public UIButton vRightButton;
	public UIButton vLeftButton;
	public UILabel vNationLabel;
	public GamePopupMyArmy vPopup;

	public UIButton vSelectNationButton;

	public UILabel vCommanderCountLabel;
	public DeckNation vDeckNation = DeckNation.None;
	
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderItemPrefab == null)
			Debug.LogError("vCommanderItemPrefab is empty - " + this);
		if (vRightButton == null)
			Debug.LogError("vRightButton is empty - " + this);
		if (vLeftButton == null)
			Debug.LogError("vLeftButton is empty - " + this);
		if (vNationLabel == null)
			Debug.LogError("vNationLabel is empty - " + this);
		if (vPopup == null)
			Debug.LogError("vPopup is empty - " + this);
		if (vSelectNationButton == null)
			Debug.LogError("vSelectNationButton is empty - " + this);
		if (vCommanderCountLabel == null)
			Debug.LogError("vCommanderCountLabel is empty - " + this);

		mCommanderItemList = new Dictionary<DeckNation, List<CommanderItemData>>();
		mCommanderListPageNation = new Dictionary<int, DeckNation>();
		mCommanderListStartPage = new Dictionary<DeckNation, int>();
		mCommanderListEndPage = new Dictionary<DeckNation, int>();
		mCommanderItemObjectList = new List<GameObject>();
		mCommanderItemButtonList = new List<MyArmyCommanderButton>();
		mTotalPage = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		vPopup.aOnPutupCommanderItem += _OnPutupCommanderItem;
		vPopup.aOnDropCommanderItem += _OnDropCommanderItem;
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp += _OnCommanderItemLevelUp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		vPopup.aOnPutupCommanderItem -= _OnPutupCommanderItem;
		vPopup.aOnDropCommanderItem -= _OnDropCommanderItem;
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp -= _OnCommanderItemLevelUp;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 아이템을 구성합니다.
	public void SetCommanderList(List<CommanderItemData> pCommanderItemList)
	{
		mCommanderItemList.Clear();
		mCommanderListPageNation.Clear();
		mCommanderListStartPage.Clear();
		mCommanderListEndPage.Clear();

		for (int iDeckNation = (int)DeckNation.US; iDeckNation < (int)DeckNation.Count; iDeckNation++)
		{
			mCommanderItemList.Add((DeckNation)iDeckNation, new List<CommanderItemData>());
		}
		// 각 지휘관의 Nation정보에 따라 다른 리스트를 작성합니다.
		for (int iCommander = 0; iCommander < pCommanderItemList.Count; iCommander++)
		{
			CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(pCommanderItemList[iCommander].mCommanderSpecNo);
			if (lCommanderSpec == null)
				continue;

			CommanderItemData lSettingCommanderItemData = new CommanderItemData();
			CommanderItemData.Copy(pCommanderItemList[iCommander], lSettingCommanderItemData);
			mCommanderItemList[lCommanderSpec.mDeckNation].Add(lSettingCommanderItemData);
		}
		// 각 Nation별 Page를 저장합니다.
		for (int iDeckNation = (int)DeckNation.None; iDeckNation < (int)DeckNation.Count; iDeckNation++)
		{
			if (iDeckNation == (int)DeckNation.None)
			{
				continue;
			}
			if (!mCommanderItemList.ContainsKey((DeckNation)iDeckNation))
			{
				mCommanderListStartPage.Add((DeckNation)iDeckNation, mTotalPage);
				mCommanderListEndPage.Add((DeckNation)iDeckNation, mTotalPage + 1);
				mCommanderListPageNation.Add(mTotalPage, (DeckNation)iDeckNation);
				mTotalPage += 1;
				continue;
			}
			
			// 아이템 정렬을 진행합니다.
			mCommanderItemList[(DeckNation)iDeckNation].Sort((lData, rData) =>
			{
				CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(lData.mCommanderSpecNo);
				CommanderSpec rCommanderSpec = CommanderTable.get.FindCommanderSpec(rData.mCommanderSpecNo);
				
				if (lCommanderSpec.mSpawnMp > rCommanderSpec.mSpawnMp)
					return 1;
				else if (lCommanderSpec.mSpawnMp < rCommanderSpec.mSpawnMp)
					return -1;
				else if (lCommanderSpec.mNo > rCommanderSpec.mNo)
					return 1;
				else if (lCommanderSpec.mNo < rCommanderSpec.mNo)
					return -1;

				return 0;
			});
	
			int lCommanderItemCount = mCommanderItemList[(DeckNation)iDeckNation].Count;
			int lResultPage = 0;
			if (lCommanderItemCount <= vItemCountPerPage)
				lResultPage = 1;
			else if (lCommanderItemCount % vItemCountPerPage == 0)
				lResultPage = lCommanderItemCount / vItemCountPerPage;
			else
				lResultPage = lCommanderItemCount / vItemCountPerPage + 1;

			for (int iPage = mTotalPage; iPage < mTotalPage + lResultPage; iPage++)
			{
				mCommanderListPageNation.Add(iPage, (DeckNation)iDeckNation);
			}
			mCommanderListStartPage.Add((DeckNation)iDeckNation, mTotalPage);
			mCommanderListEndPage.Add((DeckNation)iDeckNation, mTotalPage + lResultPage);
			mTotalPage += lResultPage;
		}
		mCommanderListStartPage.Add(DeckNation.None, 0);
		mCommanderListEndPage.Add(DeckNation.None, mTotalPage);
		SetNationFilter(DeckNation.None);
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음페이지로 넘어갑니다.
	public void OnNextPage()
	{
		if (mCurrentPage + 1 < mMaxPage)
		{
			StartCoroutine(_SetPage(mCurrentPage + 1));
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 이전페이지로 넘어갑니다.
	public void OnPrevPage()
	{
		if (mCurrentPage - 1 >= mMinPage)
		{
			StartCoroutine(_SetPage(mCurrentPage - 1));
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 Nation 페이지로 이동합니다.
	public void GoNationPage(DeckNation pNation)
	{
		StartCoroutine(_SetPage(mCommanderListStartPage[pNation]));
	}
	//-------------------------------------------------------------------------------------------------------
	// 국가별 필터를 적용합니다.
	public void SetNationFilter(DeckNation pNation)
	{
		vDeckNation = pNation;

		if (vDeckNation == DeckNation.None)
			vSelectNationButton.isEnabled = true;
		else
			vSelectNationButton.isEnabled = false;

		mMinPage = mCommanderListStartPage[vDeckNation];
		mMaxPage = mCommanderListEndPage[vDeckNation];

		StartCoroutine(_SetPage(mCommanderListStartPage[vDeckNation]));
	}
	//-------------------------------------------------------------------------------------------------------
	// 국가별 지휘관 갯수
	public int GetNationCommanderCount(DeckNation pNation)
	{
		if (mCommanderItemList.ContainsKey(pNation))
		{
			return mCommanderItemList[pNation].Count;
		}
		return 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 북마크 카테고리를 엽니다.
	public void OpenNationCategory()
	{
		vPopup.OpenNationCategory();
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 페이지를 구성합니다.
	private IEnumerator _SetPage(int pPage)
	{
		if (!mCommanderListPageNation.ContainsKey(pPage))
			yield break;

		mCurrentPage = pPage;

		if (mCurrentPage == mMinPage)
			vLeftButton.gameObject.SetActive(false);
		else
			vLeftButton.gameObject.SetActive(true);

		if (mCurrentPage == (mMaxPage - 1) || mTotalPage == 1)
			vRightButton.gameObject.SetActive(false);
		else
			vRightButton.gameObject.SetActive(true);

		DeckNation lDeckNation = mCommanderListPageNation[pPage];

		vCommanderCountLabel.text = String.Format("{0} / {1}", mCommanderItemList[lDeckNation].Count, BattleConfig.get.mTotalCommanderCount);

		NationSpec lNationSpec = NationTable.get.GetNationSpec(lDeckNation);
		vNationLabel.text = LocalizedTable.get.GetLocalizedText(lNationSpec.mNameKey);

		List<CommanderItemData> lCommanderItemList = mCommanderItemList[lDeckNation];
		int lStartIndex = (pPage - mCommanderListStartPage[lDeckNation]) * vItemCountPerPage;
		int lOffsetIndex = 0;

		mCommanderItemButtonList.Clear();
		for (int iCommander = lStartIndex; iCommander < lStartIndex + vItemCountPerPage; iCommander++)
		{
			yield return null;
			if (iCommander >= lCommanderItemList.Count)
			{
				if (mCommanderItemObjectList.Count > lOffsetIndex)
				{
					mCommanderItemObjectList[lOffsetIndex].SetActive(false);
					lOffsetIndex++;
					continue;
				}
				else
				{
					break;
				}
			}
			if (mCommanderItemObjectList.Count <= lOffsetIndex)
			{
				GameObject lCommanderItemObject = (GameObject)Instantiate(vCommanderItemPrefab.gameObject);
				lCommanderItemObject.transform.parent = vOffsetTransform[lOffsetIndex];
				lCommanderItemObject.transform.localScale = Vector3.one;
				lCommanderItemObject.transform.localPosition = Vector3.zero;
				mCommanderItemObjectList.Add(lCommanderItemObject);
			}
			else
			{
				mCommanderItemObjectList[lOffsetIndex].SetActive(true);
			}
			
			MyArmyCommanderButton lCommanderButton = mCommanderItemObjectList[lOffsetIndex].GetComponent<MyArmyCommanderButton>();
			lCommanderButton.SetCommanderInfo(lCommanderItemList[iCommander]);
			mCommanderItemButtonList.Add(lCommanderButton);
			
			if (vPopup.vPopupState == GamePopupMyArmy.PopupState.DeckList
				|| vPopup.aSelectDeckData == null)
			{
				lCommanderButton.SetEquip(false);
				lCommanderButton.SetDragable(false);
			}
			else
			{
				bool lIsEquip = false;
				DeckData lDeckData = GameData.get.aPlayerInfo.GetDeck(vPopup.aSelectDeckData.mDeckIdx);
				for (int iDeckItem = 0; iDeckItem < vPopup.aSelectDeckData.mDeckItems.Length; iDeckItem++)
				{
					if (lDeckData != null
						&& lDeckData.mDeckItems[iDeckItem] != null
						&& lCommanderButton.aCommanderItemData.mCommanderItemIdx == lDeckData.mDeckItems[iDeckItem].mCommanderItemIdx)
					{
						lIsEquip = true;
					}
				}
				lCommanderButton.SetEquip(lIsEquip);
			}
			lOffsetIndex++;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 버튼 드랍 콜백
	private void _OnDropCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton)
	{
		if (vPopup.aIsFullDeckItem)
			return;

		for(int iCommander = 0; iCommander < mCommanderItemObjectList.Count; iCommander++)
		{
			MyArmyCommanderButton lCommanderButton = mCommanderItemObjectList[iCommander].GetComponent<MyArmyCommanderButton>();
			if (lCommanderButton.aCommanderItemData.mCommanderItemIdx == pMyArmyCommanderButton.aCommanderItemData.mCommanderItemIdx
				&& vPopup.vFocusCommander.aCurrentFocusPosition == MyArmyFocusCommander.FocusPosition.SetDeck)
			{
				lCommanderButton.SetEquip(true);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 버튼 들기 콜백
	private void _OnPutupCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton)
	{
		for (int iCommander = 0; iCommander < mCommanderItemObjectList.Count; iCommander++)
		{
			MyArmyCommanderButton lCommanderButton = mCommanderItemObjectList[iCommander].GetComponent<MyArmyCommanderButton>();
			if (lCommanderButton.aCommanderItemData.mCommanderItemIdx == pMyArmyCommanderButton.aCommanderItemData.mCommanderItemIdx)
			{
				lCommanderButton.SetEquip(false);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 레벨업
	private void _OnCommanderItemLevelUp(CommanderItemData pCommanderItemData)
	{
		for (int iButton = 0; iButton < mCommanderItemButtonList.Count; iButton++)
		{
			if (pCommanderItemData.mCommanderItemIdx == mCommanderItemButtonList[iButton].aCommanderItemData.mCommanderItemIdx)
			{
				mCommanderItemButtonList[iButton].SetCommanderInfo(pCommanderItemData);
			}
		}
		// 데이터 재설정
		CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(pCommanderItemData.mCommanderSpecNo);
		List<CommanderItemData> lCommanderItemDataList = mCommanderItemList[lCommanderSpec.mDeckNation];
		for (int iCommanderItem = 0; iCommanderItem < lCommanderItemDataList.Count; iCommanderItem++)
		{
			if (pCommanderItemData.mCommanderItemIdx == lCommanderItemDataList[iCommanderItem].mCommanderItemIdx)
			{
				CommanderItemData.Copy(pCommanderItemData, lCommanderItemDataList[iCommanderItem]);
				break;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<DeckNation, List<CommanderItemData>> mCommanderItemList;
	private Dictionary<int, DeckNation> mCommanderListPageNation;
	private Dictionary<DeckNation, int> mCommanderListStartPage;
	private Dictionary<DeckNation, int> mCommanderListEndPage;
	private int mTotalPage;
	private int mCurrentPage;
	private List<GameObject> mCommanderItemObjectList;
	private List<MyArmyCommanderButton> mCommanderItemButtonList;
	private int mMinPage;
	private int mMaxPage;
}
