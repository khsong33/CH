﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyDeckListButton : InfiniteItemBehavior
{
	public enum MyArmyDeckListButtonType
	{
		SetDeck = 0,
		NewDeck
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vSetDeckObject;
	public GameObject vNewDeckObject;

	public UITexture vDeckIconTexture;
	public UITexture vDeckMarkIconTexture;
	public UILabel vDeckNameLabel;

	public GameObject vLockObject;
	public UILabel vCommanderCountLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public DeckData aDeckData { get { return mDeckData; } }
	public MyArmyDeckListButtonType aButtonType { get { return mButtonType; } }
	
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vSetDeckObject == null)
			Debug.LogError("vSetDeckObject is empty - " + this);
		if(vNewDeckObject == null)
			Debug.LogError("vNewDeckObject is empty - " + this);

		if (vDeckIconTexture == null)
			Debug.LogError("vDeckIconTexture is empty - " + this);
		if (vDeckMarkIconTexture == null)
			Debug.LogError("vDeckMarkIconTexture is empty - " + this);
		if (vDeckNameLabel == null)
			Debug.LogError("vDeckNameLabel is empty - " + this);

		if (vLockObject == null)
			Debug.LogError("vLockObject is empty - " + this);
		if (vCommanderCountLabel == null)
			Debug.LogError("vCommanderCountLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 아이템 셋팅
	public override void SetItemData(object pData)
	{
		mDeckData = pData as DeckData;
		if (mDeckData.mDeckIdx < 0)
		{
			vSetDeckObject.SetActive(false);
			vNewDeckObject.SetActive(true);
			mButtonType = MyArmyDeckListButtonType.NewDeck;
			vDeckIconTexture.gameObject.SetActive(false);
			vDeckMarkIconTexture.gameObject.SetActive(false);
			vLockObject.SetActive(false);
			vCommanderCountLabel.gameObject.SetActive(false);
		}
		else
		{
			vSetDeckObject.SetActive(true);
			vNewDeckObject.SetActive(false);
			mButtonType = MyArmyDeckListButtonType.SetDeck;

			vDeckIconTexture.gameObject.SetActive(true);
			vDeckMarkIconTexture.gameObject.SetActive(true);

			NationSpec lNationSpec = NationTable.get.GetNationSpec(mDeckData.mDeckNation);
			vDeckIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mBackgroundIcon);
			vDeckMarkIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mMarkIcon);
			vDeckNameLabel.text = mDeckData.mDeckName;

			int lCommanderCount = mDeckData.GetCommanderCount();
			if (lCommanderCount != BattleUserInfo.cMaxCommanderItemCount)
			{
				vLockObject.SetActive(true);
				vCommanderCountLabel.text = String.Format("({0} / {1})", lCommanderCount, BattleUserInfo.cMaxCommanderItemCount);
			}
			else
			{
				vLockObject.SetActive(false);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private DeckData mDeckData;
	private MyArmyDeckListButtonType mButtonType;
}
