﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyDeckPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public InfiniteListPopulator vDeckListData;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// DeckList를 셋팅합니다.
	public void SetDeckList(List<DeckData> pDeckList)
	{
		StartCoroutine(_SetDeckList(pDeckList));
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼을 가져옵니다.
	public MyArmyDeckListButton GetDeckButton(int pDataIndex)
	{
		return (MyArmyDeckListButton)vDeckListData.GetItem(pDataIndex).GetComponent<MyArmyDeckListButton>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 덱 리스트를 출력합니다.
	private IEnumerator _SetDeckList(List<DeckData> pDeckList)
	{
		yield return null;

		ArrayList lDeckButtonList = new ArrayList();
		for (int iDeck = 0; iDeck < pDeckList.Count; iDeck++)
		{
			lDeckButtonList.Add(pDeckList[iDeck]);
		}
		DeckData lNewDeckData = new DeckData();
		lNewDeckData.mDeckIdx = -1;
		lDeckButtonList.Add(lNewDeckData);

		yield return null;

		vDeckListData.InitTableView(lDeckButtonList, null, 0);
	}


	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
