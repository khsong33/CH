﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyFocusCommander : MonoBehaviour
{
	public enum FocusPosition
	{
		Default,
		SetDeck
	}
	public FocusPosition aCurrentFocusPosition { get { return mCurrentFocusPosition; } }
	public CommanderItemData aCommanderItemData { get { return mCommanderItemData; } }
	public CommanderItem aCommanderItem { get { return mCommanderItem; } }

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vBattleIconObject;
	public GameObject vPortraitIconObject;

	public UILabel vCommanderName;
	public UILabel vSpawnMpLabel;
	public UITexture vCommanderIconTexture;
	public UILabel vCommanderRankLabel;
	public UISlider vExpProgressBar;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vItemLevelLabel;
	public UISprite vRankColorSprite;
	public UISlider vCountProgressBar;
	public UILabel vCommanderItemCountLabel;

	public UILabel vPortraitCommanderName;
	public UILabel vPortraitSpawnMpLabel;
	public UITexture vPortraitCommanderIconTexture;
	public UILabel vPortraitCommanderRankLabel;
	public UISlider vPortraitExpProgressBar;
	public BattleUITroopCountInfo[] vPortraitBattleUITroopCountInfo;
	public UILabel vPortraitItemLevelLabel;
	public UISprite vPortraitRankColorSprite;
	public UISlider vPortraitCountProgressBar;
	public UILabel vPortraitCommanderItemCountLabel;

	public int vButtonIndex = 0;
	public MyArmyCommanderButton.IconType vIconType = MyArmyCommanderButton.IconType.Portrait;
	public MyArmyCommanderButton.ButtonType vButtonType = MyArmyCommanderButton.ButtonType.CommanderItem;

	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];
	public GameObject[] vPortraitStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vBattleIconObject == null)
			Debug.LogError("vBattleIconObject is empty - " + this);
		if (vPortraitIconObject == null)
			Debug.LogError("vPortraitIconObject is empty - " + this);

		if (vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vPortraitIconTexture is empty - " + this);
		if(vCommanderRankLabel == null)
			Debug.LogError("vCommanderRankLabel is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);
		if (vBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vBattleUITroopCountinfo is not enough count - " + this);
		if (vItemLevelLabel == null)
			Debug.LogError("vItemLevelLabel is empty -  " + this);
		if (vRankColorSprite == null)
			Debug.LogError("vRankColorSprite is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vCommanderItemCountLabel == null)
			Debug.LogError("vCommanderItemCountLabel is empty - " + this);

		if (vPortraitCommanderName == null)
			Debug.LogError("vPortraitCommanderName is empty - " + this);
		if (vPortraitSpawnMpLabel == null)
			Debug.LogError("vPortraitSpawnMpLabel is empty - " + this);
		if (vPortraitCommanderIconTexture == null)
			Debug.LogError("vPortraitCommanderIconTexture is empty - " + this);
		if (vPortraitCommanderRankLabel == null)
			Debug.LogError("vPortraitCommanderRankLabel is empty - " + this);
		if (vPortraitExpProgressBar == null)
			Debug.LogError("vPortraitExpProgressBar is empty - " + this);
		if (vPortraitBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vPortraitBattleUITroopCountInfo is not enough count - " + this);
		if (vPortraitItemLevelLabel == null)
			Debug.LogError("vPortraitItemLevelLabel is empty - " + this);
		if (vPortraitRankColorSprite == null)
			Debug.LogError("vPortraitRankColorSprite is empty - " + this);
		if (vPortraitCountProgressBar == null)
			Debug.LogError("vPortraitCountProgressBar is empty - " + this);
		if (vPortraitCommanderItemCountLabel == null)
			Debug.LogError("vPortraitCommanderItemCountLabel is empty - " + this);
		
		vBattleIconObject.SetActive(false);
		vPortraitIconObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 정보를 입력합니다.
	public void SetCommanderInfo(CommanderItemData pCommanderItemData)
	{
		if (mCommanderItem != null)
			mCommanderItem = null;

		if (mCommanderItemData == null)
			mCommanderItemData = new CommanderItemData();

		CommanderItemData.Copy(pCommanderItemData, mCommanderItemData); 
		mCommanderItem = new CommanderItem(pCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);

		_SetBattleIcon();
		_SetPoraitIcon();
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼에서 필요한 정보를 얻습니다.
	public void SetButtonInfo(MyArmyCommanderButton pCommanderButton)
	{
		vButtonIndex = pCommanderButton.vButtonIndex;
		vButtonType = pCommanderButton.vButtonType;
		vIconType = pCommanderButton.vIconType;
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커스의 타입을 설정합니다.
	public void SetFocusType(MyArmyCommanderButton.IconType pType)
	{
		if (pType == MyArmyCommanderButton.IconType.BattleIcon)
		{
			vBattleIconObject.SetActive(true);
			vPortraitIconObject.SetActive(false);
		}
		else if (pType == MyArmyCommanderButton.IconType.Portrait)
		{
			vBattleIconObject.SetActive(false);
			vPortraitIconObject.SetActive(true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커스 아이콘의 위치를 저장합니다.
	public void SetFocusPosition(FocusPosition pPosition)
	{
		mCurrentFocusPosition = pPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 전투 아이콘을 셋팅합니다.
	private void _SetBattleIcon()
	{
		vExpProgressBar.gameObject.SetActive(true);
		for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
		{
			vBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(true);
		}

		vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
		//vCommanderRankLabel.text = mCommanderItem.mCommanderRank.ToString();
		vCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}

		// 아이템 레벨 
		mCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
		if(mCommanderLevelSpec != null)
		{
			vItemLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();
			vRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];

			vCommanderItemCountLabel.text = String.Format("{0}/{1}", mCommanderItemData.mCommanderItemCount, 
				mCommanderLevelSpec.mNeedCard == 0 ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : mCommanderLevelSpec.mNeedCard.ToString());
			if (mCommanderLevelSpec.mNeedCard == 0)
				vCountProgressBar.value = 1.0f;
			else
				vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / mCommanderLevelSpec.mNeedCard;
		}
		_SetCommanderItemCount();
	}
	//-------------------------------------------------------------------------------------------------------
	// 자세히 보기 아이콘을 셋팅합니다.
	private void _SetPoraitIcon()
	{
		vPortraitExpProgressBar.gameObject.SetActive(true);
		for (int iCountInfo = 0; iCountInfo < vBattleUITroopCountInfo.Length; iCountInfo++)
		{
			vPortraitBattleUITroopCountInfo[iCountInfo].gameObject.SetActive(true);
		}

		vPortraitCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		vPortraitSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
		vPortraitCommanderRankLabel.text = mCommanderItem.mCommanderExpRank.ToString();
		vPortraitCommanderIconTexture.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vPortraitBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vPortraitBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vPortraitBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}

		// 아이템 레벨 
		mCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
		if (mCommanderLevelSpec != null)
		{
			vPortraitItemLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();
			vPortraitRankColorSprite.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
			vPortraitCommanderItemCountLabel.text = String.Format("{0}/{1}", mCommanderItemData.mCommanderItemCount, mCommanderLevelSpec.mNeedCard);
			if (mCommanderLevelSpec.mNeedCard == 0)
				vPortraitCountProgressBar.value = 1.0f;
			else
				vPortraitCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / mCommanderLevelSpec.mNeedCard;
		}

		_SetCommanderItemCount();
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템의 갯수 상태를 처리합니다.
	private void _SetCommanderItemCount()
	{
		CommanderItemCountState lCurrentItemState = CommanderItemCountState.Normal;
		//int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
		int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
		if (mCommanderItemData.mCommanderLevel == lMaxLevel)
			lCurrentItemState = CommanderItemCountState.MaxCount;
		else if (mCommanderItemData.mCommanderItemCount >= mCommanderLevelSpec.mNeedCard)
			lCurrentItemState = CommanderItemCountState.AvailableLevelup;
		else
			lCurrentItemState = CommanderItemCountState.Normal;

		for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
		{
			if ((int)lCurrentItemState == iState)
			{
				vStateObject[iState].SetActive(true);
				vPortraitStateObject[iState].SetActive(true);
			}
			else
			{
				vStateObject[iState].SetActive(false);
				vPortraitStateObject[iState].SetActive(false);
			}
		}
		for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
		{
			vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)lCurrentItemState];
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private FocusPosition mCurrentFocusPosition;
	private CommanderLevelSpec mCommanderLevelSpec;
}
