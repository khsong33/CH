﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyInfoCommander : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vCommanderName;
	public UILabel vCommanderDesc;
	public UILabel vSpawnMpLabel;
	public UITexture vCommanderIconTexture;
	public UILabel vCommanderRankLabel;
	public UILabel vCommanderTypeLabel;
	public BattleUITroopCountInfo[] vBattleUITroopCountInfo;
	public UILabel vCommanderLevelLabel;
	public UILabel vCommanderItemCountLabel;
	public UISlider vCountProgressBar;
	public UIButton vLevelUpButton;
	public UILabel vLevelUpCostLabel;
	public GameObject vLevelUpExpObject;
	public UILabel vLevelUpExpLabel;

	public List<UISprite> vColorSprite; // 색에 영향을 받는 모든 오브젝트
	public GameObject[] vStateObject = new GameObject[(int)CommanderItemCountState.Count];

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vCommanderDesc == null)
			Debug.LogError("vCommanderDesc is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vCommanderIconTexture == null)
			Debug.LogError("vPortraitIconTexture is empty - " + this);
		if (vCommanderRankLabel == null)
			Debug.LogError("vCommanderRankLabel is empty - " + this);
		if (vCommanderTypeLabel == null)
			Debug.LogError("vCommanderTypeLabel is empty - " + this);
		if (vBattleUITroopCountInfo.Length != CommanderTroopSet.cMaxTroopKindCount)
			Debug.LogError("vBattleUITroopCountinfo is not enough count - " + this);
		if (vCommanderLevelLabel == null)
			Debug.LogError("vCommanderLevelLabel is empty - " + this);
		if (vCommanderItemCountLabel == null)
			Debug.LogError("vCommanderItemCountLabel is empty - " + this);
		if (vCountProgressBar == null)
			Debug.LogError("vCountProgressBar is empty - " + this);
		if (vLevelUpButton == null)
			Debug.LogError("vLevelUpButton is empty - " + this);
		if (vLevelUpCostLabel == null)
			Debug.LogError("vLevelUpPriceLabel is empty - " + this);
		if (vLevelUpExpObject == null)
			Debug.LogError("vLevelUpExpObject is empty - " + this);
		if (vLevelUpExpLabel == null)
			Debug.LogError("vLevelUpExpLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Commander 정보를 셋팅합니다.
	public void SetCommanderInfo(CommanderItem pCommanderItem, CommanderItemData pCommanderItemData)
	{
		mCommanderItem = pCommanderItem;

		vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);
		vCommanderDesc.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mDescKey);
		vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
		vCommanderIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(mCommanderItem.mCommanderSpec.mPortraitIconAssetKey);
		vCommanderRankLabel.text = LocalizedTable.get.GetLocalizedText(String.Format("Rank{0}Key", mCommanderItem.aRank));
		vCommanderRankLabel.color = BattleConfig.get.mRankColors[mCommanderItem.aRank];
		vCommanderTypeLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mEffectiveKey);
		vCommanderLevelLabel.text = mCommanderItem.mCommanderLevel.ToString();

		RefreshCommanderItem(pCommanderItemData);
	
		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(true);
				vBattleUITroopCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vBattleUITroopCountInfo[iKind].gameObject.SetActive(false);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 정보를 갱신합니다.
	public void RefreshCommanderItem(CommanderItemData pCommanderItemData)
	{
		mCommanderItemData = pCommanderItemData;
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.mCommanderSpec.mRank, mCommanderItemData.mCommanderLevel);
		bool lIsMaxLevel = CommanderLevelTable.get.IsMaxLevel(mCommanderItem);
		bool lIsActiveLevelupButton = (pCommanderItemData.mCommanderItemIdx >= 0);
		if (lCommanderLevelSpec != null)
		{
			vCommanderLevelLabel.text = pCommanderItemData.mCommanderLevel.ToString();
			vCommanderItemCountLabel.text = String.Format("{0}/{1}", pCommanderItemData.mCommanderItemCount, 
				lIsMaxLevel ? CommanderLevelSpec.cMaxLevelMarginCount.ToString() : lCommanderLevelSpec.mNeedCard.ToString());

			if (lIsMaxLevel)
				vCountProgressBar.value = 1.0f;
			else
				vCountProgressBar.value = (float)mCommanderItemData.mCommanderItemCount / lCommanderLevelSpec.mNeedCard;

			if (lIsMaxLevel || !lIsActiveLevelupButton)
			{
				vLevelUpButton.gameObject.SetActive(false);
				vLevelUpExpObject.SetActive(false);
			}
			else if (lCommanderLevelSpec.mNeedCard <= pCommanderItemData.mCommanderItemCount)
			{
				vLevelUpButton.gameObject.SetActive(true);
				vLevelUpButton.isEnabled = true;
				vLevelUpCostLabel.text = lCommanderLevelSpec.mCost.ToString();
				vLevelUpExpObject.SetActive(true);
				vLevelUpExpLabel.text = lCommanderLevelSpec.mExp.ToString();
			}
			else
			{
				vLevelUpButton.gameObject.SetActive(true);
				vLevelUpButton.isEnabled = false;
				vLevelUpCostLabel.text = lCommanderLevelSpec.mCost.ToString();
				vLevelUpExpObject.SetActive(false);
				vLevelUpExpLabel.text = String.Empty;
			}

//			int lMaxItemCount = CommanderLevelTable.get.GetMaxHaveItemCount(mCommanderItem.mCommanderLevel, mCommanderItem.aRank);
			int lMaxLevel = CommanderLevelTable.get.GetMaxLevel(mCommanderItem.aRank);
			if (mCommanderItemData.mCommanderLevel == lMaxLevel)
				mCurrentItemState = CommanderItemCountState.MaxCount;
			else if (mCommanderItemData.mCommanderItemCount >= lCommanderLevelSpec.mNeedCard)
				mCurrentItemState = CommanderItemCountState.AvailableLevelup;
			else
				mCurrentItemState = CommanderItemCountState.Normal;

			for (int iState = 0; iState < (int)CommanderItemCountState.Count; iState++)
			{
				if ((int)mCurrentItemState == iState)
					vStateObject[iState].SetActive(true);
				else
					vStateObject[iState].SetActive(false);
			}
			for (int iColorSprite = 0; iColorSprite < vColorSprite.Count; iColorSprite++)
			{
				vColorSprite[iColorSprite].color = CommanderItemData.CommanderItemStateColor[(int)mCurrentItemState];
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 레벨업 버튼 활성화를 선택합니다.
	public void SetActiveLevelUpButton(bool pIsActive)
	{
		vLevelUpButton.gameObject.SetActive(pIsActive);
		vLevelUpExpObject.SetActive(pIsActive);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
	private CommanderItemCountState mCurrentItemState;
}
