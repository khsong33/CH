﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyInfoSkill : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vSkillIcon;
	public UILabel vSkillName;
	public UILabel vSkillDesc;
	public UILabel vSkillControlLabel;
	public UILabel vSkillTypeLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vSkillIcon == null)
			Debug.LogError("vSkillIcon is empty - " + this);
		if(vSkillName == null)
			Debug.LogError("vSkillName is empty - " + this);
		if(vSkillDesc == null)
			Debug.LogError("vSkillDesc is empty - " + this);
		if (vSkillControlLabel == null)
			Debug.LogError("vSkillControlLabel is empty - " + this);
		if (vSkillTypeLabel == null)
			Debug.LogError("vSkillTypeLabel is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Skill 정보를 셋팅합니다.
	public void SetSkillInfo(CommanderItem pCommanderItem)
	{
		mSkillSpec = pCommanderItem.mCommanderSpec.mSkillSpecs[0];
		vSkillIcon.mainTexture = TextureResourceManager.get.LoadTexture(mSkillSpec.mIconAssetKey);
		vSkillName.text = LocalizedTable.get.GetLocalizedText(mSkillSpec.mNameKey);
		vSkillDesc.text = LocalizedTable.get.GetLocalizedText(mSkillSpec.mDescKey);
		vSkillControlLabel.text = LocalizedTable.get.GetLocalizedText(mSkillSpec.mControlKey);
		vSkillTypeLabel.text = LocalizedTable.get.GetLocalizedText(mSkillSpec.mTypeKey);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private SkillSpec mSkillSpec;
}
