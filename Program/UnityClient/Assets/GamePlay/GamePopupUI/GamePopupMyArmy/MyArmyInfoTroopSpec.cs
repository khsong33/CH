﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyInfoTroopSpec : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vTroopName;
	public UILabel vWeaponName;
	public UILabel vTroopDesc;
	public UITexture vFieldIcon;
	public UITexture vPortraitIcon;
	public UILabel[] vVeterancyDesc;
	public MyArmyInfoTroopState[] vTroopStates;
	public GameObject vToolTipPopupObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vTroopName == null)
			Debug.LogError("vTroopName is empty - " + this);
		if (vWeaponName == null)
			Debug.LogError("vWeaponName is empty - " + this);
		if (vTroopDesc == null)
			Debug.LogError("vTroopDesc is empty - " + this);
		if(vFieldIcon == null)
			Debug.LogError("vFieldIcon is empty - " + this);
		if(vPortraitIcon == null)
			Debug.LogError("vPortraitIcon is empty - " + this);
		if (vVeterancyDesc.Length != BattleConfig.get.mMaxVeterancyRank)
			Debug.LogError("Not same Veterancy rank");
		if (vTroopStates.Length != CommanderDetailSpec.cMaxDetailCount)
			Debug.LogError("Not Enough Slot Count - " + vTroopStates.Length);
		if (vToolTipPopupObject == null)
			Debug.LogError("vToolTipPopupObject is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		vToolTipPopupObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Troop 정보를 셋팅합니다.
	public void SetTroopInfo(CommanderItem pCommanderItem, CommanderItemData pCommanderItemData, int pTroopKind)
	{
		mCommanderItem = pCommanderItem;
		mCommanderItemData = pCommanderItemData;
		TroopSpec lTroopSpec = mCommanderItem.mTroopSet.mTroopKindCountInfos[pTroopKind].mTroopSpec;
		int lTroopCount = mCommanderItem.mTroopSet.mTroopKindCountInfos[pTroopKind].mTroopCount;

		vTroopName.text = String.Format("{0} x {1}", LocalizedTable.get.GetLocalizedText(lTroopSpec.mNameKey), lTroopCount);
		vTroopDesc.text = LocalizedTable.get.GetLocalizedText(lTroopSpec.mEffectiveText);
		if (lTroopSpec.mWeaponSpecs[TroopSpec.cSecondaryWeaponIdx] != null)
		{
			String lPrimaryWeaponName = LocalizedTable.get.GetLocalizedText(lTroopSpec.mWeaponSpecs[TroopSpec.cPrimaryWeaponIdx].mNameKey);
			String lSecondaryWeaponName = LocalizedTable.get.GetLocalizedText(lTroopSpec.mWeaponSpecs[TroopSpec.cSecondaryWeaponIdx].mNameKey);
			vWeaponName.text = String.Format("{0} + {1}", lPrimaryWeaponName, lSecondaryWeaponName);
		}
		else
		{
			String lPrimaryWeaponName = LocalizedTable.get.GetLocalizedText(lTroopSpec.mWeaponSpecs[TroopSpec.cPrimaryWeaponIdx].mNameKey);
			vWeaponName.text = lPrimaryWeaponName; 
		}
		
		vFieldIcon.mainTexture = TextureResourceManager.get.LoadTexture(lTroopSpec.mFieldIconAssetKey);
		vPortraitIcon.mainTexture = TextureResourceManager.get.LoadTexture(lTroopSpec.mPortraitIconAssetKey);
		// 베테랑 처리
		for (int iVeteran = 0; iVeteran < BattleConfig.get.mMaxVeterancyRank; iVeteran++)
		{
			vVeterancyDesc[iVeteran].text = LocalizedTable.get.GetLocalizedText(lTroopSpec.mVeterancySpecs[iVeteran + 1].mDescKey);
		}
		
		// 주요 능력치
		CommanderDetailSpec lCommanderDetailSpec = CommanderDetailTable.get.FindCommanderDetailSpec(mCommanderItem.mCommanderSpec.mNo);
		CommanderLevelSpec lCommanderLevelSpec = CommanderLevelTable.get.GetCommanderLevelSpec(mCommanderItem.aRank, mCommanderItem.mCommanderLevel);
		bool lIsLevelup = (lCommanderLevelSpec.mNeedCard <= mCommanderItemData.mCommanderItemCount) && !CommanderLevelTable.get.IsMaxLevel(mCommanderItem);
		for (int iState = 0; iState < CommanderDetailSpec.cMaxDetailCount; iState++)
		{
			if (pTroopKind == 0 && lCommanderDetailSpec.mTroop1DetailDataList.Count > iState)
				vTroopStates[iState].SetTroopDetailInfo(pCommanderItem.mCommanderLevel, lCommanderDetailSpec.mTroop1DetailDataList[iState], lIsLevelup);
			else if (pTroopKind == 1 && lCommanderDetailSpec.mTroop2DetailDataList.Count > iState)
				vTroopStates[iState].SetTroopDetailInfo(pCommanderItem.mCommanderLevel, lCommanderDetailSpec.mTroop2DetailDataList[iState], lIsLevelup);
			else
				vTroopStates[iState].SetTroopDetailInfo(pCommanderItem.mCommanderLevel, null, false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 툴팁 정보를 봅니다.
	public void OnPressToolTip()
	{
		vToolTipPopupObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 베테랑 툴팁 정보를 끕니다..
	public void OnReleaseToolTip()
	{
		vToolTipPopupObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;
}
