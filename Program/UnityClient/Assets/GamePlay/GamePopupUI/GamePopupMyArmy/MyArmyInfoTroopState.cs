﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyInfoTroopState : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vStateNameLabel;
	public UILabel vCurrentValueLabel;
	public UILabel vLevelupValueLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vStateNameLabel == null)
			Debug.LogError("vStateNameLabel is empty - " + this);
		if (vCurrentValueLabel == null)
			Debug.LogError("vCurrentValueLabel is empty - " + this);
		if (vLevelupValueLabel == null)
			Debug.LogError("vLevelupValueLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Troop 정보를 셋팅합니다.
	public void SetTroopDetailInfo(int pLevel, TroopDetailSpec pTroopDetailSpec, bool pIsLevelup)
	{
		if (pTroopDetailSpec == null)
		{
			vStateNameLabel.text = String.Empty;
			vCurrentValueLabel.text = String.Empty;
			vLevelupValueLabel.text = String.Empty;
		}
		else
		{
			vStateNameLabel.text = LocalizedTable.get.GetLocalizedText(pTroopDetailSpec.mStateNameKey);
			if (pTroopDetailSpec.mType == CommanderDetailType.StringType)
			{
				vCurrentValueLabel.text = LocalizedTable.get.GetLocalizedText(pTroopDetailSpec.mStringData);
				vLevelupValueLabel.text = String.Empty;
			}
			else
			{
				vCurrentValueLabel.text = pTroopDetailSpec.GetLevelValue(pLevel).ToString();
				if (!pIsLevelup)
					vLevelupValueLabel.text = String.Empty;
				else
				{
					int lLevelupValue = pTroopDetailSpec.GetLevelValue(pLevel + 1) - pTroopDetailSpec.GetLevelValue(pLevel);
					String lLevelupValueString = String.Empty;
					if (lLevelupValue > 0)
						lLevelupValueString = String.Format("+{0}", lLevelupValue);
					else if(lLevelupValue < 0)
						lLevelupValueString = String.Format("{0}", lLevelupValue);
					// LevelupValue가 0일 경우 표시하지 않습니다.
					vLevelupValueLabel.text = lLevelupValueString;
				}
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
