﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyNation : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vNationIcon;
	public UITexture vNationMarkIcon;
	public UILabel vCountCommanderLabel;
	public UILabel vNationLevelLabel;
	public UISlider vNationExp;
	public UILabel vNationNameLabel;

	public UIButton vSelectButton;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public NationSpec aNationSpec { get { return mNationSpec; } }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vNationIcon == null)
			Debug.LogError("vNationIcon is empty - " + this);
		if (vNationMarkIcon == null)
			Debug.LogError("vNationMarkIcon is empty - " + this);
		if (vCountCommanderLabel == null)
			Debug.LogError("vCountCommanderLabel is empty - " + this);
		if (vNationLevelLabel == null)
			Debug.LogError("vNationLevelLabel is empty - " + this);
		if (vNationExp == null)
			Debug.LogError("vNationExp is empty - " + this);
		if (vNationNameLabel == null)
			Debug.LogError("vNationNameLabel is empty - " + this);

		if (vSelectButton == null)
			Debug.LogError("vSelectButton is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 국가 정보를 셋팅합니다.
	public void SetNationInfo(NationSpec pNationSpec, int pCommanderCount)
	{
		vNationIcon.mainTexture = TextureResourceManager.get.LoadTexture(pNationSpec.mBackgroundIcon);
		vNationMarkIcon.mainTexture = TextureResourceManager.get.LoadTexture(pNationSpec.mMarkIcon);
		vCountCommanderLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("CountNationCommander{0}"), pCommanderCount);
		int lNationLevel = 1;
		if(GameData.get.aPlayerInfo != null)
			lNationLevel = GameData.get.aPlayerInfo.mNationData[pNationSpec.mNation].mNationLevel;
		vNationLevelLabel.text = lNationLevel.ToString();

		NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(lNationLevel);
		if (lNationLevelSpec == null)
		{
			Debug.LogError("Not Exist NationLevel Data");
			return;
		}
		int lExp = 0;
		if(GameData.get.aPlayerInfo != null)
			lExp = GameData.get.aPlayerInfo.mNationData[pNationSpec.mNation].mNationExp;

		float lExpRatio = (float)(lExp - lNationLevelSpec.mNeedExp) / (lNationLevelSpec.mMaxExp - lNationLevelSpec.mNeedExp);

		vNationExp.value = lExpRatio;

		vNationNameLabel.text = LocalizedTable.get.GetLocalizedText(pNationSpec.mNameKey);

		mNationSpec = pNationSpec;

		if (pCommanderCount < BattleUserInfo.cMaxCommanderItemCount)
			vSelectButton.isEnabled = false;
		else
			vSelectButton.isEnabled = true;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private NationSpec mNationSpec;
}
