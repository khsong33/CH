﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmyNationSelectPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GamePopupMyArmy vPopup;
	public MyArmyNation[] vNationButtons;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vPopup == null)
			Debug.LogError("vPopup is empty - " +this);
		if (vNationButtons.Length != (int)DeckNation.Count)
			Debug.LogError("vNationButtons is Not same count nation - " + this + "/Count - " + vNationButtons.Length);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Deck Nation Item 클릭시 처리합니다.
	public void OnClickedNation(MyArmyNation pMyArmyNation)
	{
		vPopup.OnSelectNewDeckNation(pMyArmyNation.aNationSpec.mNation);
		//Debug.Log("Clicked - " + LocalizedTable.get.GetLocalizedText(pMyArmyNation.aNationSpec.mNameKey));
	}
	//-------------------------------------------------------------------------------------------------------
	// Nation 정보를 셋팅합니다.
	public void SetNationInfos()
	{
		for (int iNationButton = (int)DeckNation.US; iNationButton < (int)DeckNation.Count; iNationButton++)
		{
			int lNationCommanderCount = vPopup.vCommanderPanel.GetNationCommanderCount((DeckNation)iNationButton);
			vNationButtons[iNationButton].SetNationInfo(NationTable.get.GetNationSpec((DeckNation)iNationButton), lNationCommanderCount);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 판넬을 닫습니다.
	public void ClosePanel()
	{
		vPopup.CloseSelectNation();
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
