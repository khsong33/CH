﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MyArmySetDeckPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject[] vCommanderItemObject;
	public MyArmyCommanderButton vCommanderItemPrefab;

	public UITexture vDeckIconTexture;
	public UITexture vDeckMarkIconTexture;
	public UILabel vDeckNameLabel;
	public UIInput vDeckNameInput;

	public GamePopupMyArmy vPopup;

	public UIButton vApplyButton;
	public UIButton vCancelButton;

	public UILabel vAverageMpLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vDeckIconTexture == null)
			Debug.LogError("vDeckIconTexture is empty - " + this);
		if(vDeckMarkIconTexture == null)
			Debug.LogError("vDeckMarkIconTexture is empty - " + this);
		if(vDeckNameLabel == null)
			Debug.LogError("vDeckNameLabel is empty - " + this);
		if (vDeckNameInput == null)
			Debug.LogError("vDeckNameInput is empty - " + this);
		if (vCommanderItemPrefab == null)
			Debug.LogError("vCommanderItemPrefab is empty - " + this);

		if(vPopup == null)
			Debug.LogError("vPopup is empty - " + this);

		if (vApplyButton == null)
			Debug.LogError("vApplyButton is empty - " + this);
		if (vCancelButton == null)
			Debug.LogError("vCancelButton is empty - " + this);

		if (vAverageMpLabel == null)
			Debug.LogError("vAverageMpLabel is empty - " + this);

		mCommanderItemButtonList = new List<MyArmyCommanderButton>();
		vAverageMpLabel.text = String.Empty; // 코루틴으로 인해 초기값이 늦게 보이는 문제는 일단 Empty로 채워놓음
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		vPopup.aOnDropCommanderItem += _OnDropCommanderItem;
		vPopup.aOnPutupCommanderItem += _OnPutupCommanderItem;

		if (vPopup.vLobbyControl != null)
		{
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemDeckUpdate += _OnCommanderItemDeckUpdate;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnAddCommanderItemDeck += _OnAddCommanderItemDeck;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnRemoveCommanderItemDeck += _OnRemoveCommanderItemDeck;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp += _OnCommanderItemLevelUp;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		vPopup.aOnDropCommanderItem -= _OnDropCommanderItem;
		vPopup.aOnPutupCommanderItem -= _OnPutupCommanderItem;

		if (vPopup.vLobbyControl != null)
		{
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemDeckUpdate -= _OnCommanderItemDeckUpdate;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnAddCommanderItemDeck -= _OnAddCommanderItemDeck;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnRemoveCommanderItemDeck -= _OnRemoveCommanderItemDeck;
			vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp -= _OnCommanderItemLevelUp;
		}

		for (int iButton = 0; iButton < mCommanderItemButtonList.Count; iButton++)
		{
			Destroy(mCommanderItemButtonList[iButton].gameObject);
		}
		mCommanderItemButtonList.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Deck 정보 셋팅
	public DeckData SetDeckInfo(DeckData pDeckData, bool pIsNewDeck)
	{
		NationSpec lNationSpec = NationTable.get.GetNationSpec(pDeckData.mDeckNation);

		vDeckIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mBackgroundIcon);
		vDeckMarkIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lNationSpec.mMarkIcon);

		vDeckNameInput.value = pDeckData.mDeckName;

		vDeckNameLabel.text = pDeckData.mDeckName;

		mSettingDeckData = new DeckData();
		DeckData.Copy(pDeckData, mSettingDeckData);

		StartCoroutine(_SetCommanderItem(pDeckData.mDeckItems));

		if (mDeckData == null)
			mDeckData = new DeckData();
		DeckData.Copy(pDeckData, mDeckData);

		mIsNewDeck = pIsNewDeck;

		return mSettingDeckData;
	}
	//-------------------------------------------------------------------------------------------------------
	// Deck 정보 셋팅 취소
	public void CancelDeck()
	{
		vPopup.OnCancelDeckSetting();
	}
	//-------------------------------------------------------------------------------------------------------
	// Deck 이름 셋팅중
	public bool IsDeckNameSetting()
	{
		return vDeckNameInput.isSelected;
	}
	//-------------------------------------------------------------------------------------------------------
	// Deck 정보 셋팅
	public void ApplyDeck()
	{
		mSettingDeckData.mDeckName = vDeckNameInput.value;
		for (int iCommander = 0; iCommander < BattleUserInfo.cMaxCommanderItemCount; iCommander++)
		{
			if (iCommander >= mCommanderItemButtonList.Count)
				mSettingDeckData.mDeckItems[iCommander] = null;
			else
				mSettingDeckData.mDeckItems[iCommander] = mCommanderItemButtonList[iCommander].aCommanderItemData;
		}
		// 덱이 바뀌었는지 확인
		bool lIsChangedDeck = false;
		for (int iCommander = 0; iCommander < BattleUserInfo.cMaxCommanderItemCount; iCommander++)
		{
			bool lIsInputDeck = mDeckData.mDeckItems[iCommander] == null && mSettingDeckData.mDeckItems[iCommander] != null;
			bool lIsOutputDeck = mDeckData.mDeckItems[iCommander] != null && mSettingDeckData.mDeckItems[iCommander] == null;
			if (lIsInputDeck || lIsOutputDeck)
			{
				lIsChangedDeck = true;
			}
			if(mDeckData.mDeckItems[iCommander] != null && mSettingDeckData.mDeckItems[iCommander] != null)
			{
				if(mDeckData.mDeckItems[iCommander].mCommanderItemIdx != mSettingDeckData.mDeckItems[iCommander].mCommanderItemIdx
				|| !mDeckData.mDeckName.Equals(mSettingDeckData.mDeckName))
				{
					lIsChangedDeck = true;
				}
			}
			
			if(lIsChangedDeck)
				break;
		}
		// 서버 전송 여부 결정
		bool lIsSendPacket = (vPopup.vLobbyControl != null && lIsChangedDeck);
		if (lIsSendPacket)
		{
			List<DeckData> lSendUpdateDeckList = new List<DeckData>();
			lSendUpdateDeckList.Add(mSettingDeckData);
			if (mIsNewDeck)
			{
				vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_NewCommanderItemDeck(lSendUpdateDeckList);
			}
			else
			{
				vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_CommanderItemDeck(lSendUpdateDeckList);
			}
			vApplyButton.gameObject.SetActive(false);
			vCancelButton.gameObject.SetActive(false);
		}
		else
		{
			DeckData.Copy(mSettingDeckData, mDeckData);
			vPopup.OnCompleteDeckSetting();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 정보창을 엽니다.
	public void OnDeckInfo()
	{
		vPopup.OpenDeckInfoPanel();
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 삭제 확인창을 엽니다.
	public void OnRemoveDeck()
	{
		vPopup.CloseDeckInfoPanel();
		MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("RemoveDeckConfirmMessage"), MessageBoxManager.MessageBoxType.OK_Cancel, _OnDeckRemove);
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 삭제를 진행합니다.
	private void _OnDeckRemove()
	{
		vPopup.vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_RemoveCommanderItemDeck(mSettingDeckData.mDeckIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CommanderItemData를 셋팅합니다.
	private IEnumerator _SetCommanderItem(CommanderItemData[] pCommanderItem)
	{
		int lButtonIndex = 0;
		mSettingDeckData.ResetData();
		for (int iCommander = 0; iCommander < pCommanderItem.Length; iCommander++)
		{
			if (pCommanderItem[iCommander] == null) 
			{
				continue;
			}
			GameObject lButtonObject = (GameObject)Instantiate(vCommanderItemPrefab.gameObject);
			lButtonObject.transform.parent = vCommanderItemObject[lButtonIndex].transform;
			lButtonObject.transform.localPosition = Vector3.zero;
			lButtonObject.transform.localScale = Vector3.one;

			MyArmyCommanderButton lButton = lButtonObject.GetComponent<MyArmyCommanderButton>();
			lButton.vButtonIndex = lButtonIndex;
			lButton.SetCommanderInfo(pCommanderItem[iCommander]);
			mCommanderItemButtonList.Add(lButton);

			mSettingDeckData.SetDeckItem(pCommanderItem[iCommander], lButtonIndex);

			lButtonIndex++;
			yield return null;
		}
		_RefreshApplyButton();
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 버튼 드랍 콜백
	private void _OnDropCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton)
	{
		if (vPopup.aIsFullDeckItem)
			return;

		if (vPopup.vFocusCommander.aCurrentFocusPosition == MyArmyFocusCommander.FocusPosition.Default
			&& pMyArmyCommanderButton.vButtonType == MyArmyCommanderButton.ButtonType.BattleCommander)
		{
			Destroy(mTempDestroyCommanderButton.gameObject);			
		}
		else if (vPopup.vFocusCommander.aCurrentFocusPosition == MyArmyFocusCommander.FocusPosition.SetDeck)
		{
			if (pMyArmyCommanderButton.vButtonType == MyArmyCommanderButton.ButtonType.BattleCommander)
			{
				Destroy(mTempDestroyCommanderButton.gameObject);
			}
			int lStartButtonIndex = 0;
			int lNewCommanderSpawnMp = pMyArmyCommanderButton.aCommanderItem.aSpawnMp;
			for (int iStartPos = 0; iStartPos < mCommanderItemButtonList.Count; iStartPos++)
			{
				int lCommanderSpawnMp = mCommanderItemButtonList[iStartPos].aCommanderItem.aSpawnMp;
				if(lCommanderSpawnMp > lNewCommanderSpawnMp)
				{
					lStartButtonIndex = iStartPos;
					break;
				}
				if (iStartPos == mCommanderItemButtonList.Count - 1)
					lStartButtonIndex = mCommanderItemButtonList.Count;
			}

			for (int iButton = mCommanderItemButtonList.Count - 1; iButton >=  lStartButtonIndex; iButton--)
			{
				mCommanderItemButtonList[iButton].transform.parent = vCommanderItemObject[iButton + 1].transform;
				mCommanderItemButtonList[iButton].vButtonIndex = iButton + 1;
				mCommanderItemButtonList[iButton].transform.localPosition = Vector3.zero;
				mCommanderItemButtonList[iButton].transform.localScale = Vector3.one;
				if (mSettingDeckData.mDeckItems[iButton + 1] == null)
					mSettingDeckData.mDeckItems[iButton + 1] = new CommanderItemData();
				CommanderItemData.Copy(mSettingDeckData.mDeckItems[iButton], mSettingDeckData.mDeckItems[iButton + 1]);
			}
			GameObject lButtonObject = (GameObject)Instantiate(vCommanderItemPrefab.gameObject);
			lButtonObject.transform.parent = vCommanderItemObject[lStartButtonIndex].transform;
			lButtonObject.transform.localPosition = Vector3.zero;
			lButtonObject.transform.localScale = Vector3.one;

			MyArmyCommanderButton lButton = lButtonObject.GetComponent<MyArmyCommanderButton>();
			lButton.vButtonIndex = lStartButtonIndex;
			lButton.SetCommanderInfo(pMyArmyCommanderButton.aCommanderItemData);
			mCommanderItemButtonList.Insert(lStartButtonIndex, lButton);

			if (mSettingDeckData.mDeckItems[lStartButtonIndex] == null)
				mSettingDeckData.mDeckItems[lStartButtonIndex] = new CommanderItemData();
			CommanderItemData.Copy(pMyArmyCommanderButton.aCommanderItemData, mSettingDeckData.mDeckItems[lStartButtonIndex]);

			_RefreshApplyButton();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// CommanderItem 버튼 들기 콜백
	private void _OnPutupCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton)
	{
		if (pMyArmyCommanderButton.vButtonType == MyArmyCommanderButton.ButtonType.BattleCommander)
		{
			mCommanderItemButtonList[pMyArmyCommanderButton.vButtonIndex].SetShow(false);
			mTempDestroyCommanderButton = mCommanderItemButtonList[pMyArmyCommanderButton.vButtonIndex];
			int lStartButtonIndex = pMyArmyCommanderButton.vButtonIndex + 1;
			for (int iButton = lStartButtonIndex; iButton < mCommanderItemButtonList.Count; iButton++)
			{
				mCommanderItemButtonList[iButton].transform.parent = vCommanderItemObject[iButton - 1].transform;
				mCommanderItemButtonList[iButton].vButtonIndex = iButton - 1;
				mCommanderItemButtonList[iButton].transform.localPosition = Vector3.zero;
				mCommanderItemButtonList[iButton].transform.localScale = Vector3.one;
				CommanderItemData.Copy(mSettingDeckData.mDeckItems[iButton], mSettingDeckData.mDeckItems[iButton-1]);
			}
			CommanderItemData.Copy(new CommanderItemData(), mSettingDeckData.mDeckItems[mCommanderItemButtonList.Count - 1]);

			mCommanderItemButtonList.RemoveAt(pMyArmyCommanderButton.vButtonIndex);
			//pMyArmyCommanderButton.SetShow(false);

			_RefreshApplyButton();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 확인버튼 처리
	private void _RefreshApplyButton()
	{
		_RefreshAverageMP();
		if (mCommanderItemButtonList.Count == BattleUserInfo.cMaxCommanderItemCount)
		{
			//vApplyButton.isEnabled = true;
			vPopup.aIsFullDeckItem = true;
		}
		else
		{
			//vApplyButton.isEnabled = false;
			vPopup.aIsFullDeckItem = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 평균 MP를 계산합니다.
	private void _RefreshAverageMP()
	{
		int lTotalMP = 0;
		int lTotalCount = 0;
		for (int iItem = 0; iItem < mSettingDeckData.mDeckItems.Length; iItem++)
		{
			if (mSettingDeckData.mDeckItems[iItem].mCommanderSpecNo <= 0)
				continue;
			
			CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(mSettingDeckData.mDeckItems[iItem].mCommanderSpecNo);
			lTotalMP += lCommanderSpec.mSpawnMp;
			lTotalCount++;
		}
		float lAverageMp = (float)lTotalMP / lTotalCount;
		vAverageMpLabel.text = String.Format("{0:0.0}", lAverageMp);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 업데이트합니다.
	private void _OnCommanderItemDeckUpdate()
	{
		//DeckData.Copy(mSettingDeckData, mDeckData);
		GameData.get.aPlayerInfo.UpdateDeck(mSettingDeckData);

		vApplyButton.gameObject.SetActive(true);
		vCancelButton.gameObject.SetActive(true);

		vPopup.OnCompleteDeckSetting();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱을 추가합니다.
	private void _OnAddCommanderItemDeck(int pNewDeckId)
	{
		mSettingDeckData.mDeckIdx = pNewDeckId;
		GameData.get.aPlayerInfo.AddDeck(mSettingDeckData);

		vApplyButton.gameObject.SetActive(true);
		vCancelButton.gameObject.SetActive(true);
		vPopup.OnCompleteDeckSetting();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱을 삭제합니다.
	private void _OnRemoveCommanderItemDeck(int pRemoveDeckId)
	{
		GameData.get.aPlayerInfo.RemoveDeck(pRemoveDeckId);
		vPopup.OnCompleteDeckSetting();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 레벨업합니다.
	private void _OnCommanderItemLevelUp(CommanderItemData pCommanderItemData)
	{
		for (int iButton = 0; iButton < mCommanderItemButtonList.Count; iButton++)
		{
			if(pCommanderItemData.mCommanderItemIdx == mCommanderItemButtonList[iButton].aCommanderItemData.mCommanderItemIdx)
				mCommanderItemButtonList[iButton].SetCommanderInfo(pCommanderItemData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private List<MyArmyCommanderButton> mCommanderItemButtonList;
	private DeckData mDeckData;
	private DeckData mSettingDeckData;
	private bool mIsNewDeck;
	private MyArmyCommanderButton mTempDestroyCommanderButton;
}
