﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//-------------------------------------------------------------------------------------------------------
// Scene에 상관없이 갖고 있어야할 데이터를 저장하는 클래스
//-------------------------------------------------------------------------------------------------------
public class GamePopupUIManager
{
	public delegate void OnClosedPopup();
    //-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------
	// 싱글톤
    public static GamePopupUIManager aInstance
    {
        get
        {
            if (sInstance == null)
            {
                sInstance = new GamePopupUIManager();
            }
            return sInstance;
        }
    }
	//-------------------------------------------------------------------------------------------------------
	// 현재 열려있는 팝업
	public GamePopupUIItem aCurrentPopup
	{
		get { return mCurrentPopup; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 팝업 닫힘 콜백
	public OnClosedPopup aOnClosedPopup { get; set; }

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // 생성자
    public GamePopupUIManager()
    {
        mStackPopupUI = new Stack<GameObject>();
    }
    //-------------------------------------------------------------------------------------------------------
    // Root로 사용될 Object를 등록합니다.
    public void RegisteRoot(GameObject pGameObject)
    {
        mUIRootObject = pGameObject;
    }
    //-------------------------------------------------------------------------------------------------------
    // Popup을 생성합니다.
    public GamePopupUIItem OpenPopup(String pPopupName)
    {
        if (mCurrentPopup != null)
        {
			//mCurrentPopup.OnDisablePopup();
            mCurrentPopup.gameObject.SetActive(false);
            mStackPopupUI.Push(mCurrentPopup.gameObject);
        }
        GameObject lResourceObject = ResourceUtil.Load<GameObject>("GamePopupUI/" + pPopupName);
        GameObject lPopupObject = (GameObject)GameObject.Instantiate(lResourceObject);
        lPopupObject.transform.parent = mUIRootObject.transform;
        lPopupObject.transform.localScale = Vector3.one;
        lPopupObject.transform.localPosition = Vector3.zero;
        mCurrentPopup = lPopupObject.GetComponent<GamePopupUIItem>();
		//mCurrentPopup.OnEnablePopup();
        return mCurrentPopup;
    }
    //-------------------------------------------------------------------------------------------------------
    // 뒤로가기를 실행합니다.
    public void BackPopup()
    {
		if (mCurrentPopup == null)
			return;

		mCurrentPopup.BackPopup();
    }
	//-------------------------------------------------------------------------------------------------------
	// 뒤로가기를 실행 후 처리입니다.
	public GamePopupUIItem OnBackPopup()
	{
		ClosePopup();
		if (mStackPopupUI.Count > 0)
		{
			GameObject lPopupUIObject = mStackPopupUI.Pop();
			if (lPopupUIObject != null)
			{
				lPopupUIObject.SetActive(true);
				mCurrentPopup = lPopupUIObject.GetComponent<GamePopupUIItem>();
				//mCurrentPopup.OnEnablePopup();
			}
		}
		return mCurrentPopup;
	}
	//-------------------------------------------------------------------------------------------------------
	// 남아있는 팝업이 있는지 확인합니다.
	public bool IsActivePopup()
	{
		if (mStackPopupUI.Count > 0)
			return true;
		return false;
	}
    //-------------------------------------------------------------------------------------------------------
    // Popup을 닫습니다.
    public void ClosePopup()
    {
        if (mCurrentPopup != null)
        {
            GameObject.Destroy(mCurrentPopup.gameObject);
            mCurrentPopup = null;
			if (aOnClosedPopup != null)
				aOnClosedPopup();
        }
        else
        {
            Debug.LogError("Popup Closed Failed :: Not Exist Popup UI");
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // 스택에 들어있는 모든 내용을 삭제합니다.
    public void ClearPopup()
    {
        GameObject lPopupObject = mStackPopupUI.Pop();
        while (lPopupObject != null)
        {
            GameObject.Destroy(lPopupObject);
            lPopupObject = null;

            lPopupObject = mStackPopupUI.Pop();
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private static GamePopupUIManager sInstance = null;
    private Stack<GameObject> mStackPopupUI;
    private GameObject mUIRootObject;
    private GamePopupUIItem mCurrentPopup;
}