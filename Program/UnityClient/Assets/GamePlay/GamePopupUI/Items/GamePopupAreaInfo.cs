﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupAreaInfo : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vScrollPanel;
	public List<int> vAreaOffsets;
	public List<AreaInfoData> vAreaInfoList;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vScrollPanel == null)
			Debug.LogError("vScrollPanel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// AreaSpecInfo를 입력합니다.
	public void SetAreaInfo()
	{
		int lCurrentArea = GameData.get.aPlayerInfo.mArea;
		int lOffset = vAreaOffsets[lCurrentArea];
		DeckNation lNation = GameData.get.aPlayerInfo.mUseNation;
		vScrollPanel.transform.localPosition = new Vector3(lOffset, vScrollPanel.transform.localPosition.y, vScrollPanel.transform.localPosition.z);
		UIPanel lScrollPanel = vScrollPanel.GetComponent<UIPanel>();
		lScrollPanel.clipOffset = new Vector2(-lOffset, 0);

		// Area0 은 튜토리얼
		for (int iArea = 0; iArea < vAreaInfoList.Count; iArea++)
		{
			vAreaInfoList[iArea].SetAreaCommander(iArea, lNation);
			if (iArea == lCurrentArea)
				vAreaInfoList[iArea].SetActiveRecentArea(true);
			else
				vAreaInfoList[iArea].SetActiveRecentArea(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------

}
