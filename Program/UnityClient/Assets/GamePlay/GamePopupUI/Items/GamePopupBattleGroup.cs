﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupBattleGroup : GamePopupUIItem
{
	public enum BattleGroupState
	{
		Normal,
		SetCommander,
		ChangePosition,
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattleGroupDeckPanel vDeckPanel;
	public BattleGroupCommanderPanel vCommanderPanel;
	public BattleGroupFocusCommander vFocusCommander;
	public GameObject vCommanderPanelShowObject;
	public BattleGroupState vBattleGroupState;
	public List<BattleGroupDeckButton> vDeckButtonList;

	public UILabel vAverageMPLabel;
	public UILabel vCommanderCountLabel;


	public bool aIsFocusActive { set; get; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vDeckPanel == null)
			Debug.LogError("vDeckPanel is empty - " + this);
		if (vCommanderPanel == null)
			Debug.LogError("vCommanderPanel is empty - " + this);
		if (vFocusCommander == null)
			Debug.LogError("vFocusCommander is empty - " + this);
		if (vCommanderPanelShowObject == null)
			Debug.LogError("vCommanderPanelShowObject is empty - " + this);
		if (vAverageMPLabel == null)
			Debug.LogError("vAverageMPLabel is empty - " + this);
		if (vCommanderCountLabel == null)
			Debug.LogError("vCommanderCountLabel is empty - " + this);

		mDeckCommanderSlotIdxList = new Dictionary<int, List<int>>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		if (GameData.get.aPlayerInfo == null)
			return;

		vCommanderPanelShowObject.SetActive(true);

		mCurrentDeckIdx = GameData.get.aPlayerInfo.mUseDeckId;

		_SetTempDeckData();
		SelectDeckIdx(GameData.get.aPlayerInfo.mUseDeckId);
		_RefreshAverageMp();

		vCommanderCountLabel.text = String.Format("{0} / {1}",
			GameData.get.aPlayerInfo.mCommanderItemData.Count,
			CommanderAreaTable.get.GetNationMaxCommanderCount(GameData.get.aPlayerInfo.mUseNation));
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		_UpdateDeck();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 확인합니다.
	public void OnClickCommander(GameObject pButtonObject)
	{
		if (pButtonObject == mSelectButton)
		{
			ClearCommanderButton();
		}
		else
		{
			if (mSelectButton != null)
				mSelectButton.SendMessage("SetActiveSelectPopup", false);
			mSelectButton = pButtonObject;
			mSelectButton.SendMessage("SetActiveSelectPopup", true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 보유 지휘관 리스트 드래그 처리를 진행합니다.
	public void OnDragCommanderList(Vector2 pDelta)
	{
		vCommanderPanel.OnDragCommanderList(pDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 선택 버튼을 해제합니다.
	public void ClearCommanderButton()
	{
		if (mSelectButton != null)
			mSelectButton.SendMessage("SetActiveSelectPopup", false);
		mSelectButton = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 배치를 진행합니다.
	public void OnSetUpCommander(CommanderItemData pCommanderItemData)
	{
		mSelectCommanderItemData = pCommanderItemData;
		vFocusCommander.gameObject.SetActive(true);
		vFocusCommander.SetCommanderInfo(mSelectCommanderItemData);
		vCommanderPanelShowObject.SetActive(false);
		vDeckPanel.StartSelectEffect(-1); // 전체 효과 적용
		vBattleGroupState = BattleGroupState.SetCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 배치 선택을 해제합니다
	public void OnUnselectSetupCommander()
	{
		mSelectCommanderItemData = null;
		vFocusCommander.gameObject.SetActive(false);
		vCommanderPanelShowObject.SetActive(true);
		vBattleGroupState = BattleGroupState.Normal;
		vDeckPanel.EndSelectEffect();
		ClearCommanderButton();
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 아이템 위치 변경 준비 선택
	public void ReadyChangePosition(int pSlotIdx, CommanderItemData pCommanderItemData)
	{
		if (mSelectCommanderItemData == null)
			mSelectCommanderItemData = new CommanderItemData();

		CommanderItemData.Copy(pCommanderItemData, mSelectCommanderItemData);
		mChangePositionSlotIdx = pSlotIdx;
		vBattleGroupState = BattleGroupState.ChangePosition;
		vDeckPanel.StartSelectEffect(pSlotIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 배치를 결정합니다.
	public void SetDeckCommanderItem(int pSlotIdx)
	{
		mCurrentDeckData.mDeckItems[pSlotIdx] = mSelectCommanderItemData;
		vDeckPanel.UpdateDeckCommanderItemData(mSelectCommanderItemData, pSlotIdx);
		vCommanderPanel.SetCommanderList(GameData.get.aPlayerInfo.mCommanderItemData, mCurrentDeckIdx);
		OnUnselectSetupCommander();
		_RefreshAverageMp();
	}
	//-------------------------------------------------------------------------------------------------------
	// 변경될 위치를 클릭합니다.
	public void SetChangeDeckCommanderItem(int pSlotIdx)
	{
		mCurrentDeckData.mDeckItems[mChangePositionSlotIdx] = mCurrentDeckData.mDeckItems[pSlotIdx];
		vDeckPanel.UpdateDeckCommanderItemData(mCurrentDeckData.mDeckItems[mChangePositionSlotIdx], mChangePositionSlotIdx);
		
		mCurrentDeckData.mDeckItems[pSlotIdx] = mSelectCommanderItemData;
		vDeckPanel.UpdateDeckCommanderItemData(mSelectCommanderItemData, pSlotIdx);

		OnUnselectSetupCommander();
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 아이템의 실제 데이터를 배치합니다.(드래그앤드랍 기능 사용)
	public void UpdateDeckCommanderItem(int pSlotIdx, CommanderItemData pCommanderItemData)
	{
		mCurrentDeckData.mDeckItems[pSlotIdx] = pCommanderItemData;
		_RefreshAverageMp();
	}
	//-------------------------------------------------------------------------------------------------------
	// 보유한 아이템의 List Panel을 셋팅합니다.
	public void UpdateListPanel()
	{
		vCommanderPanel.SetCommanderList(GameData.get.aPlayerInfo.mCommanderItemData, mCurrentDeckIdx, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 선택
	public void SelectDeckIdx(int pDeckIdx)
	{
		mCurrentDeckIdx = pDeckIdx;
		mCurrentDeckData = GameData.get.aPlayerInfo.GetDeck(mCurrentDeckIdx);
		vDeckPanel.SetDeck(mCurrentDeckData);
		vCommanderPanel.SetCommanderList(GameData.get.aPlayerInfo.mCommanderItemData, mCurrentDeckIdx);
		vBattleGroupState = BattleGroupState.Normal;

		for (int iDeck = 0; iDeck < vDeckButtonList.Count; iDeck++)
		{
			vDeckButtonList[iDeck].SetActiveSelect(iDeck == mCurrentDeckIdx);
		}
		_RefreshAverageMp();
	}
	//-------------------------------------------------------------------------------------------------------
	// 포커스 커맨더 활성화
	public void SetActiveFocus(bool pIsActive)
	{
		vFocusCommander.gameObject.SetActive(pIsActive);
		aIsFocusActive = pIsActive;
		vCommanderPanel.SetActiveDrag(!pIsActive);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 임시로 덱 아이템을 저장합니다.
	private void _SetTempDeckData()
	{
		mDeckCommanderSlotIdxList.Clear();
		List<DeckData> lDeckList = GameData.get.aPlayerInfo.GetAllDeckList();
		for (int iDeck = 0; iDeck < lDeckList.Count; iDeck++)
		{
			DeckData lDeckData = lDeckList[iDeck];
			for (int iCommander = 0; iCommander < lDeckData.mDeckItems.Length; iCommander++)
			{
				if (!mDeckCommanderSlotIdxList.ContainsKey(lDeckData.mDeckIdx))
					mDeckCommanderSlotIdxList.Add(lDeckData.mDeckIdx, new List<int>());

				mDeckCommanderSlotIdxList[lDeckData.mDeckIdx].Add(lDeckData.mDeckItems[iCommander].mCommanderItemIdx);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱을 업데이트합니다.
	private void _UpdateDeck()
	{
		List<DeckData> lUpdateDeckDataList = new List<DeckData>();
		
		Dictionary<int, List<int>>.Enumerator iter = mDeckCommanderSlotIdxList.GetEnumerator();
		while(iter.MoveNext())
		{
			int lDeckId = iter.Current.Key;
			List<int> lOriginDeckItemId = iter.Current.Value;
			DeckData lChangeDeckData = GameData.get.aPlayerInfo.GetDeck(lDeckId);
			bool lIsChangeDeck = false;
			for (int iItem = 0; iItem < lOriginDeckItemId.Count; iItem++)
			{
				if (lChangeDeckData.mDeckItems[iItem].mCommanderItemIdx != lOriginDeckItemId[iItem])
				{
					lIsChangeDeck = true;
					break;
				}
			}
			if (lIsChangeDeck)
				lUpdateDeckDataList.Add(lChangeDeckData);
		}
		bool lIsDeckChange = false;
		if (mCurrentDeckIdx != GameData.get.aPlayerInfo.mUseDeckId)
		{
			GameData.get.aPlayerInfo.mUseDeckId = mCurrentDeckIdx;
			lIsDeckChange = true;
		}

		if (lUpdateDeckDataList.Count > 0 || lIsDeckChange)
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_UpdateDeck(mCurrentDeckIdx, lUpdateDeckDataList);
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱을 업데이트합니다.
	private void _RefreshAverageMp()
	{
		DeckData lCurrentDeckData = GameData.get.aPlayerInfo.GetDeck(mCurrentDeckIdx);
		float lTotalMp = 0.0f;
		for (int iItem = 0; iItem < lCurrentDeckData.mDeckItems.Length; iItem++)
		{
			CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(lCurrentDeckData.mDeckItems[iItem].mCommanderSpecNo);
			lTotalMp += lCommanderSpec.mSpawnMp;
		}
		float lAverageMp = (float)lTotalMp / lCurrentDeckData.mDeckItems.Length;
		vAverageMPLabel.text = String.Format("{0:0.0}", lAverageMp);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mCurrentDeckIdx;
	private GameObject mSelectButton;
	private CommanderItemData mSelectCommanderItemData;
	private int mChangePositionSlotIdx;
	private DeckData mCurrentDeckData;
	private Dictionary<int, List<int>> mDeckCommanderSlotIdxList;
}
