﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupBattleResult : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<BattleResultUser> vBattleResultUserList;
	public UILabel vGainRatingLabel;
	public GameObject vChestObject;
	public UITexture vChestIcon;
	public UILabel vChestNameLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vGainRatingLabel == null)
			Debug.LogError("vGainRatingLabel is empty - " + this);
		if (vChestObject == null)
			Debug.LogError("vChestObject is empty - " + this);
		if (vChestIcon == null)
			Debug.LogError("vChestIcon is empty - " + this);
		if (vChestNameLabel == null)
			Debug.LogError("vChestLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 창에서 back 버튼은 먹히지 않습니다.
	public override void BackPopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터 셋팅
	public void SetBattleResultData(BattleGrid pBattleGrid, BattleResultData pBattleResultData)
	{
		mBattleResultData = pBattleResultData;
		mBattleGrid = pBattleGrid;

		// Rating 표시
		if (mBattleResultData.mGainRating > 0)
			vGainRatingLabel.text = String.Format("+{0}", mBattleResultData.mGainRating);
		else
			vGainRatingLabel.text = String.Format("{0}", mBattleResultData.mGainRating);

		// 상자 표시
		if (mBattleResultData.mChestIconSpec == null)
		{
			vChestObject.SetActive(false);
			vChestIcon.mainTexture = null;
			vChestNameLabel.text = String.Empty;
		}
		else
		{
			vChestObject.SetActive(true);
			vChestIcon.mainTexture = TextureResourceManager.get.LoadTexture(mBattleResultData.mChestIconSpec.mIconAssetKey);
			vChestNameLabel.text = LocalizedTable.get.GetLocalizedText(mBattleResultData.mChestIconSpec.mNameKey);
		}
		// 유저 정보 표시
		_SetUserInfos();
	}
	//-------------------------------------------------------------------------------------------------------
	// 확인 버튼 클릭
	public void OnClickConfirm()
	{
		GameData.get.EnterLobbyPhase();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 유저 결과 정보 표시
	private void _SetUserInfos()
	{
		int lMyTeamIdx = 0;
		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; iUser++)
		{
			if (mBattleGrid.aUsers[iUser] == null)
				continue;

			bool lIsMine = GameData.get.aPlayerInfo.CheckMine((int)mBattleGrid.aUsers[iUser].aUserInfo.mClientGuid);
			if (lIsMine)
				lMyTeamIdx = mBattleGrid.aUsers[iUser].aUserInfo.mTeamIdx;
		}

		int lTeamUserCount = 0;
		int lTeamLiveCommanderCount = 0;
		int lEnemyUserCount = 0;
		int lEnemyLiveCommanderCount = 0;

		for (int iUser = 0; iUser < BattleMatchInfo.cMaxUserCount; iUser++)
		{
			if (mBattleGrid.aUsers[iUser] == null)
				continue;
			// 아군인 경우
			bool lIsMyTeam = (lMyTeamIdx == mBattleGrid.aUsers[iUser].aUserInfo.mTeamIdx);
			if (lIsMyTeam)
			{
				lTeamLiveCommanderCount += mBattleGrid.aUsers[iUser].aLiveCommanderCount;
				lTeamUserCount++;
			}
			else
			{
				lEnemyLiveCommanderCount += mBattleGrid.aUsers[iUser].aLiveCommanderCount;
				lEnemyUserCount++;
			}
		}
		int lMedalCount = Mathf.CeilToInt((float)((BattleUserInfo.cMaxCommanderCount * lEnemyUserCount) - lEnemyLiveCommanderCount) / lEnemyUserCount);
		int lEnemyMedalCount = Mathf.CeilToInt((float)((BattleUserInfo.cMaxCommanderCount * lTeamUserCount) - lTeamLiveCommanderCount) / lTeamUserCount);


		for (int iUser = 0; iUser < vBattleResultUserList.Count; iUser++)
		{
			bool lIsMyTeam = (lMyTeamIdx == mBattleGrid.aUsers[iUser].aUserInfo.mTeamIdx);
			bool lIsVictory;
			int lShowMedalCount = 0;
			if (lIsMyTeam)
			{
				lIsVictory = (mBattleResultData.mBattleEndCode == BattleProtocol.BattleEndCode.PlayWin) ? true : false;
				lShowMedalCount = lMedalCount;
			}
			else
			{
				lIsVictory = (mBattleResultData.mBattleEndCode == BattleProtocol.BattleEndCode.PlayWin) ? false : true;
				lShowMedalCount = lEnemyMedalCount;
			}

			vBattleResultUserList[iUser].SetUser(mBattleGrid.aUsers[iUser].aUserInfo, lIsVictory, lShowMedalCount);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleResultData mBattleResultData;
	private BattleMatchInfo mMatchInfo;
	private BattleGrid mBattleGrid;
}
