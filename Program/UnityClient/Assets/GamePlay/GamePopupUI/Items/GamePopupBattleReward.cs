﻿//#define BATTLE_REWARD_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class GamePopupBattleReward : GamePopupUIItem
{
	public enum RewardStep
	{
		None,
		CommanderExp,
		CommanderExpComplete,
		NationExp,
		NationExpComplete,
		RewardCommanderView,
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vVictoryObject;
	public GameObject vDefeatObject;
	public GameObject vDrawObject;

	public RewardCommanderExpPanel vCommanderExpPanel;
	public RewardNationExpPanel vNationExpPanel;
	public RewardCommanderPanel vAddCommanderPanel;

	public RewardStep vRewardStep = RewardStep.None;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vVictoryObject == null)
			Debug.LogError("vVictoryObject is empty - " + this);
		if (vDefeatObject == null)
			Debug.LogError("vDefeatObject is empty - " + this);
		if (vDrawObject == null)
			Debug.LogError("vDrawObject is empty - " + this);

		if (vCommanderExpPanel == null)
			Debug.LogError("vCommanderExpPanel is empty - " + this);
		if(vNationExpPanel == null)
			Debug.LogError("vNationExpPanel is empty - " + this);
		if (vAddCommanderPanel == null)
			Debug.LogError("vAddCommanderPanel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public void SetRewardInfo(BattleMatchInfo pBattleMatchInfo, BattleEndInfo pBattleEndInfo, BattleRewardInfo pBattleRewardInfo)
	{
		mBattleMatchInfo = pBattleMatchInfo;
		mBattleEndInfo = pBattleEndInfo;
		mBattleRewardInfo = pBattleRewardInfo;

		if (mBattleRewardInfo.mCommanderItemExpList.Count <= 0)
		{
			_SetStep(RewardStep.CommanderExpComplete);
		}
		GoNextStep();

		// 개발용 코드 start //////////////////////////////////////////////////////////////////////////
		//if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		//{
		//	if (!TutorialTable.get.IsTutorial(mBattleMatchInfo.aStageSpec.mNo))
		//	{
		//		JSONObject pRewardJson = BattlePlay.main.aClientUser.CalcBattleEndPvPReward(pBattleEndInfo.mBattleEndCode);
		//		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_Dev_BattleReward(pRewardJson);
		//	}
		//	else
		//	{
		//		JSONObject pRewardJson = BattlePlay.main.aClientUser.CalcBattleEndTutorialReward(pBattleEndInfo.mBattleEndCode);
		//		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_Dev_BattleReward(pRewardJson);
		//	}
		//}
		//else if (pExpectedRewardInfo != null)
		//{
		//	_OnBattleReward(pExpectedRewardInfo);
		//}
		//else
		//{
		//	//// TEST DATA
		//	GameData.get.aPlayerInfo = new GameUserInfo();
		//	GameData.get.aPlayerInfo.SetCommanderItemList(CustomBattleCommanderItemTable.get.GetAllTestCommanderItemData(String.Empty));
		//	GameData.get.aPlayerInfo.mNationExps[1] = 900;
		//	GameData.get.aPlayerInfo.mNationLevels[1] = 1;
		//	GameData.get.aPlayerInfo.mNationExps[2] = 900;
		//	GameData.get.aPlayerInfo.mNationLevels[2] = 1;
		//	GameData.get.aPlayerInfo.mNationExps[3] = 900;
		//	GameData.get.aPlayerInfo.mNationLevels[3] = 1;

		//	List<DeckData> lDeckDataList = CustomBattleCommanderItemTable.get.GetDeckDataList(String.Empty);
		//	BattleRewardInfo lRewardTestInfo = new BattleRewardInfo();
		//	// 변하지 않은 경험치 테스트
		//	//lRewardTestInfo.mNation = DeckNation.USSR;
		//	//lRewardTestInfo.mNationExp = 990;
		//	//lRewardTestInfo.mNationLevel = 1;

		//	// 변하는 경험치 테스트
		//	lRewardTestInfo.mNation = DeckNation.USSR;
		//	lRewardTestInfo.mNationExp = 1500;
		//	lRewardTestInfo.mNationLevel = 2;

		//	// 신규 지휘관 추가 테스트
		//	CommanderItemData lAddCommanderItemData = new CommanderItemData();
		//	lAddCommanderItemData.mCommanderItemIdx = 140;
		//	lAddCommanderItemData.mCommanderSpecNo = 1005;
		//	lAddCommanderItemData.mCommanderRank = 1;
		//	lAddCommanderItemData.mCommanderExp = 0;
		//	lRewardTestInfo.mAddCommanderItemDataList.Add(lAddCommanderItemData);

		//	for (int i = 0; i < 10; i++)
		//	{
		//		BattleRewardCommanderItemExp lTestCommanderExp = new BattleRewardCommanderItemExp();
		//		CommanderSpec lCommanderSpec = CommanderTable.get.FindCommanderSpec(lDeckDataList[0].mDeckItems[i].mCommanderSpecNo);
		//		lTestCommanderExp.mItemIdx = lDeckDataList[0].mDeckItems[i].mCommanderItemIdx;
		//		lTestCommanderExp.mRank = 2;
		//		lTestCommanderExp.mExp = lCommanderSpec.mRankNeedExps[2] + 8000;
		//		lRewardTestInfo.mCommanderItemExpList.Add(lTestCommanderExp);
		//	}
		//	_OnBattleReward(lRewardTestInfo);
		//}
		// 개발용 코드 end //////////////////////////////////////////////////////////////////////////
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		if (mBattleEndInfo == null)
			return;

		vVictoryObject.SetActive(mBattleEndInfo.mIsVictory);
		vDefeatObject.SetActive(mBattleEndInfo.mIsDefeat);
		vDrawObject.SetActive(mBattleEndInfo.mIsDraw);

		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnEndTutorial += _OnEndTutorial;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnDevSetBattleReward += _OnDevSetBattleReward;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnEndTutorial -= _OnEndTutorial;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnDevSetBattleReward -= _OnDevSetBattleReward;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void BackPopup()
	{
		// 뒤로가기를 처리하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 보상창을 진행합니다.
	public void GoNextStep()
	{
		switch (vRewardStep)
		{
			case RewardStep.None:
				vCommanderExpPanel.SetBattleReward(mBattleRewardInfo);
				break;
			case RewardStep.CommanderExp:
				vCommanderExpPanel.OnNextStep();
				break;
			case RewardStep.CommanderExpComplete:
				vCommanderExpPanel.gameObject.SetActive(false);
				vNationExpPanel.gameObject.SetActive(true);
				vNationExpPanel.SetBattleReward(mBattleRewardInfo);
				break;
			case RewardStep.NationExp:
				vNationExpPanel.OnFlush();
				break;
			case RewardStep.NationExpComplete:
				vNationExpPanel.gameObject.SetActive(false);
				if (mBattleRewardInfo.mAddCommanderItemDataList.Count > 0)
				{
					vNationExpPanel.gameObject.SetActive(false);
					vAddCommanderPanel.gameObject.SetActive(true);
					vAddCommanderPanel.SetCommanderInfo(mBattleRewardInfo.mAddCommanderItemDataList[0]); // 현재는 무조건 하나만 지급
				}
				else
				{
					_UpdateUserData(mBattleRewardInfo);
					GameData.get.EnterLobbyPhase();
				}
				break;
			case RewardStep.RewardCommanderView:
			default:
				_UpdateUserData(mBattleRewardInfo);
				GameData.get.EnterLobbyPhase();
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 보상창 상태를 설정합니다.
	public void SetNextStep()
	{
		vRewardStep = (RewardStep)((int)vRewardStep + 1);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 종료시 처리
	private void _OnEndTutorial(int pUpdateTutorialIdx)
	{
		GameData.get.aPlayerInfo.mClearTutorialIdx = pUpdateTutorialIdx;
	}
	//-------------------------------------------------------------------------------------------------------
	// 개발용 코드 - 서버에 보상 정보를 입력 완료합니다.
	private void _OnDevSetBattleReward()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_BattleReward(mBattleMatchInfo.aStageSpec.mNo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 보상창 상태를 설정합니다.
	private void _SetStep(RewardStep pRewardStep)
	{
		vRewardStep = pRewardStep;
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제 데이터 업데이트
	private void _UpdateUserData(BattleRewardInfo pRewardInfo)
	{
		if (pRewardInfo == null)
			return;

		if (pRewardInfo.mNation == DeckNation.None)
			return;

		//GameData.get.aPlayerInfo.mNationExps[(int)pRewardInfo.mNation] = pRewardInfo.mNationExp;
		//GameData.get.aPlayerInfo.mNationLevels[(int)pRewardInfo.mNation] = pRewardInfo.mNationLevel;

		GameData.get.aPlayerInfo.mNationData[pRewardInfo.mNation].mNationExp = pRewardInfo.mNationExp;
		GameData.get.aPlayerInfo.mNationData[pRewardInfo.mNation].mNationLevel= pRewardInfo.mNationLevel;

		for (int iCommanderItem = 0; iCommanderItem < pRewardInfo.mCommanderItemExpList.Count; iCommanderItem++)
		{
			GameData.get.aPlayerInfo.UpdateCommanderItemExp(
				pRewardInfo.mCommanderItemExpList[iCommanderItem].mItemIdx,
				pRewardInfo.mCommanderItemExpList[iCommanderItem].mRank,
				pRewardInfo.mCommanderItemExpList[iCommanderItem].mExp);
		}
		for (int iAddCommander = 0; iAddCommander < pRewardInfo.mAddCommanderItemDataList.Count; iAddCommander++)
		{
			GameData.get.aPlayerInfo.AddCommanderItem(pRewardInfo.mAddCommanderItemDataList[iAddCommander]);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleMatchInfo mBattleMatchInfo;
	private BattleEndInfo mBattleEndInfo;
	private BattleRewardInfo mBattleRewardInfo;
	private BattleRewardInfo mExpectedRewardInfo;
}
