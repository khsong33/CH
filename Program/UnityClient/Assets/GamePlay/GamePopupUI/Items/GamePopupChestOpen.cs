﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupChestOpen : GamePopupUIItem
{
	public enum ChestOpenState
	{
		None = -1,
		Gold = 0,
		Gem,
		CommanderItem,
		End
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public ChestOpenChest vChest;
	public List<ChestOpenItem> vChestOpenItems;

	public GameObject vChestItemRoot;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vChest == null)
			Debug.LogError("vChest is empty - " + this);
		if (vChestItemRoot == null)
			Debug.LogError("vChestItemRoot is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		mCurrentState = ChestOpenState.None;
		mActiveAnimation = false;
		OnNextState();
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// back 버튼
	public override void BackPopup()
	{
		base.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// ChestOpenSpec을 셋팅합니다.
	public void SetChestOpenSpec(ChestOpenData pChestOpenSpec)
	{
		mChestOpenSpec = pChestOpenSpec;
		vChest.SetChestOpenSpec(mChestOpenSpec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 상태를 처리합니다.
	public void OnNextState()
	{
		if (mActiveAnimation)
			return;

		mActiveAnimation = true;
		if (mCurrentOpenItem != null)
		{
			mCurrentOpenItem.aOnEndAnimation -= _OnEndItemAnimation;
			mCurrentOpenItem.gameObject.SetActive(false);
		}

		switch (mCurrentState)
		{
			case ChestOpenState.None:
				mCurrentState = ChestOpenState.Gold;
				break;
			case ChestOpenState.Gold:
				if (mChestOpenSpec.mIsGemActive)
					mCurrentState = ChestOpenState.Gem;
				else
					mCurrentState = ChestOpenState.CommanderItem;
				break;
			case ChestOpenState.Gem:
				mCurrentState = ChestOpenState.CommanderItem;
				break;
			case ChestOpenState.CommanderItem:
				if (vChest.aCurrentRemainItemCount == 0)
					mCurrentState = ChestOpenState.End;
				break;
		}
		vChest.UpdateChestOpenCount();
		_UpdateCurrentState();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 상태 처리를 진행합니다.
	private void _UpdateCurrentState()
	{
		if (mCurrentState != ChestOpenState.End)
		{
			GameObject pChestOpenItemObject = (GameObject)Instantiate<GameObject>(vChestOpenItems[(int)mCurrentState].gameObject);
			pChestOpenItemObject.transform.parent = vChestItemRoot.transform;
			pChestOpenItemObject.transform.localPosition = Vector3.zero;
			pChestOpenItemObject.transform.localScale = Vector3.one;
			mCurrentOpenItem = pChestOpenItemObject.GetComponent<ChestOpenItem>();
			mCurrentOpenItem.aOnEndAnimation += _OnEndItemAnimation;
		}

		switch (mCurrentState)
		{
			case ChestOpenState.Gold:
				ChestOpenGold lChestOpenGold = mCurrentOpenItem as ChestOpenGold;
				lChestOpenGold.SetGoldInfo(mChestOpenSpec.mGainGold);
				break;
			case ChestOpenState.Gem:
				ChestOpenGem lChestOpenGem = mCurrentOpenItem as ChestOpenGem;
				lChestOpenGem.SetGemInfo(mChestOpenSpec.mGainGem);
				break;
			case ChestOpenState.CommanderItem:
				ChestOpenCommanderItem lChestOpenCommanderItem = mCurrentOpenItem as ChestOpenCommanderItem;
				lChestOpenCommanderItem.SetCommanderItemData(mChestOpenSpec, mCurrentCommanderItemIndex);
				mCurrentCommanderItemIndex++;
				break;
			case ChestOpenState.End:
				BackPopup();
				break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템 애니메이션 종료 콜백
	private void _OnEndItemAnimation()
	{
		mActiveAnimation = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ChestOpenData mChestOpenSpec;
	private ChestOpenState mCurrentState;
	private ChestOpenItem mCurrentOpenItem;
	private bool mActiveAnimation;
	private int mCurrentCommanderItemIndex;
}
