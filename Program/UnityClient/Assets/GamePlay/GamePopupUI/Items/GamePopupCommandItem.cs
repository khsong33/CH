﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupCommandItem : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public DeckButton[] vDeckButtons;
	public InfiniteListPopulator vCommanderItemListData;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		vLobbyControl = GameData.get.aLobbyControl;
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemDeckUpdate += _OnCommanderItemDeckUpdate;

		vCommanderItemListData.InfiniteItemIsClickedEvent += _OnClickCommanderItem;
		for (int iDeckButton = 0; iDeckButton < vDeckButtons.Length; iDeckButton++)
		{
			vDeckButtons[iDeckButton].SetPopup(this);
		}
		mSearchDeckSlotIdxToItemIdx = new Dictionary<int, int>();
		mCommanderSetSlot = new bool[vDeckButtons.Length];
		mCurrentSetSlotIdx = -1;
		//mAvailableSlot = new Stack<int>();

		mChangedCommanderItem = new Dictionary<int, CommanderItemData>();
		mChangedDeck = new Dictionary<int, DeckData>();

		InitCommanderItem();
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		vCommanderItemListData.InfiniteItemIsClickedEvent -= _OnClickCommanderItem;
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemDeckUpdate -= _OnCommanderItemDeckUpdate;
	}

	//-------------------------------------------------------------------------------------------------------
	// 팝업 초기화
	public void InitCommanderItem()
	{
		GameUserInfo lPlayerInfo = GameData.get.aPlayerInfo; 
		mCurrentDeckId = lPlayerInfo.mUseDeckId;

		mCommanderItemSettingData = new List<CommanderItemData>();
		for (int iCommanderItem = 0; iCommanderItem < GameData.get.aPlayerInfo.mCommanderItemData.Count; iCommanderItem++)
		{
			CommanderItemData lCommanderItemData = new CommanderItemData();
			CommanderItemData.Copy(GameData.get.aPlayerInfo.mCommanderItemData[iCommanderItem], lCommanderItemData);
			mCommanderItemSettingData.Add(lCommanderItemData);
		}
		mCommanderDeckSettingData = new DeckData();
		DeckData.Copy(GameData.get.aPlayerInfo.mCommanderDeckData[mCurrentDeckId], mCommanderDeckSettingData);

		_ResetCommanderItem();
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 카드 클릭
	public void OnClickCommanderDeckItem(DeckButton pDeckButton)
	{
		Debug.Log("Clicked - " + pDeckButton.name);
		if (pDeckButton.aIsEmpty)
			return;

		pDeckButton.EmptyItemData();

		//mAvailableSlot.Push(mCurrentSetSlotIdx);
		//mAvailableSlot.Push(pDeckButton.aCommanderItem.mSlotIdx);

		int lSlotIdx = pDeckButton.aCommanderItem.mSlotIdx;
		int lItemIdx = mSearchDeckSlotIdxToItemIdx[pDeckButton.aCommanderItem.mSlotIdx];

		//mCommanderItemSettingData[lItemIdx].mSlotNo[mCurrentDeckId] = -1;
		// mCommanderDeckSettingData.mDeckItems[lSlotIdx] = null;
		mCommanderDeckSettingData.SetDeckItem(null, lSlotIdx);

		mSearchDeckSlotIdxToItemIdx.Remove(pDeckButton.aCommanderItem.mSlotIdx);
		for (int iItem = 0; iItem < vCommanderItemListData.aItemPoolCount; iItem++)
		{
			Transform lItemTransform = vCommanderItemListData.GetItem(iItem);
			DeckListButton lItemDataListButton = lItemTransform.GetComponent<DeckListButton>();
			if (lItemDataListButton.aCommanderItem.mSlotIdx == lItemIdx)
			{
				lItemDataListButton.SetAssigned(false);
				break;
			}
		}
		_UpdateSetSlotIdx();

		if (!mChangedCommanderItem.ContainsKey(pDeckButton.aCommanderItemData.mCommanderItemIdx))
		{
			mChangedCommanderItem.Add(pDeckButton.aCommanderItemData.mCommanderItemIdx, pDeckButton.aCommanderItemData);
		}
		if (!mChangedDeck.ContainsKey(mCurrentDeckId))
		{
			mChangedDeck.Add(mCurrentDeckId, mCommanderDeckSettingData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 적용 클릭
	public void OnApply()
	{
		//Dictionary<int, CommanderItemData>.Enumerator iter = mChangedCommanderItem.GetEnumerator();
		//List<CommanderItemData> lChangedItemList = new List<CommanderItemData>();
		//while (iter.MoveNext())
		//{
		//	//CommanderItemData lCommanderItemData = iter.Current.Value;

		//	lChangedItemList.Add(iter.Current.Value);
		//}

		Dictionary<int, DeckData>.Enumerator iter = mChangedDeck.GetEnumerator();
		List<DeckData> lChangedDeckList = new List<DeckData>();
		while (iter.MoveNext())
		{
			lChangedDeckList.Add(iter.Current.Value);
		}
		vLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.SendPacket_CommanderItemDeck(lChangedDeckList);
	}
	//-------------------------------------------------------------------------------------------------------
	// 원래대로 클릭
	public void OnRestore()
	{
		for (int iCommanderItem = 0; iCommanderItem < GameData.get.aPlayerInfo.mCommanderItemData.Count; iCommanderItem++)
		{
			CommanderItemData.Copy(GameData.get.aPlayerInfo.mCommanderItemData[iCommanderItem], mCommanderItemSettingData[iCommanderItem]);
		}
		DeckData.Copy(GameData.get.aPlayerInfo.mCommanderDeckData[mCurrentDeckId], mCommanderDeckSettingData);
		_ResetCommanderItem();
	}
	//-------------------------------------------------------------------------------------------------------
	// 확인 클릭
	public void OnConfirm()
	{
		GamePopupUIManager.aInstance.ClosePopup();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// UI를 초기화합니다.
	private void _ResetCommanderItem()
	{
		//ArrayList lData = new ArrayList();

		//Dictionary<int, int> lItemIdForListItemPoolIdx = new Dictionary<int, int>();
		//lItemIdForListItemPoolIdx.Clear();

		//mSearchDeckSlotIdxToItemIdx.Clear();
		//for (int iCommanderItem = 0; iCommanderItem < mCommanderItemSettingData.Count; iCommanderItem++)
		//{
		//	lData.Add(mCommanderItemSettingData[iCommanderItem]);
		//	lItemIdForListItemPoolIdx.Add(mCommanderItemSettingData[iCommanderItem].mCommanderItemIdx, iCommanderItem);
		//}
		//for (int iDeckSlot = 0; iDeckSlot < mCommanderDeckSettingData.mDeckItems.Length; iDeckSlot++)
		//{
		//	if (mCommanderDeckSettingData.mDeckItems[iDeckSlot] != null)
		//	{
		//		// mCommanderDeckSettingData.SetDeckItem(mCommanderDeckSettingData.mDeckItems[iDeckSlot], mCurrentSetSlotIdx);
		//		vDeckButtons[iDeckSlot].SetItemData(iDeckSlot, mCommanderDeckSettingData.mDeckItems[iDeckSlot]);
		//		mSearchDeckSlotIdxToItemIdx.Add(iDeckSlot, lItemIdForListItemPoolIdx[mCommanderDeckSettingData.mDeckItems[iDeckSlot].mCommanderItemIdx]);
		//		mCommanderSetSlot[iDeckSlot] = true;
		//	}
		//}
		//vCommanderItemListData.InitTableView(lData, null, 0);
		//// 채우기 가능한 슬롯을 확인합니다.
		//for (int iCommanderSlot = vDeckButtons.Length - 1; iCommanderSlot >= 0; iCommanderSlot--)
		//{
		//	if (!mCommanderSetSlot[iCommanderSlot])
		//	{
		//		vDeckButtons[iCommanderSlot].EmptyItemData();
		//	}
		//}
		//_UpdateSetSlotIdx();
		StartCoroutine(_CoResetCommanderItem());
	}
	private IEnumerator _CoResetCommanderItem()
	{
		ArrayList lData = new ArrayList();

		Dictionary<int, int> lItemIdForListItemPoolIdx = new Dictionary<int, int>();
		lItemIdForListItemPoolIdx.Clear();

		mSearchDeckSlotIdxToItemIdx.Clear();
		for (int iCommanderItem = 0; iCommanderItem < mCommanderItemSettingData.Count; iCommanderItem++)
		{
			lData.Add(mCommanderItemSettingData[iCommanderItem]);
			lItemIdForListItemPoolIdx.Add(mCommanderItemSettingData[iCommanderItem].mCommanderItemIdx, iCommanderItem);
		}
		for (int iDeckSlot = 0; iDeckSlot < mCommanderDeckSettingData.mDeckItems.Length; iDeckSlot++)
		{
			if (mCommanderDeckSettingData.mDeckItems[iDeckSlot] != null)
			{
				// mCommanderDeckSettingData.SetDeckItem(mCommanderDeckSettingData.mDeckItems[iDeckSlot], mCurrentSetSlotIdx);
				yield return null;
				vDeckButtons[iDeckSlot].SetItemData(iDeckSlot, mCommanderDeckSettingData.mDeckItems[iDeckSlot]);
				mSearchDeckSlotIdxToItemIdx.Add(iDeckSlot, lItemIdForListItemPoolIdx[mCommanderDeckSettingData.mDeckItems[iDeckSlot].mCommanderItemIdx]);
				mCommanderSetSlot[iDeckSlot] = true;
			}
		}
		vCommanderItemListData.InitTableView(lData, null, 0);
		// 채우기 가능한 슬롯을 확인합니다.
		for (int iCommanderSlot = vDeckButtons.Length - 1; iCommanderSlot >= 0; iCommanderSlot--)
		{
			if (!mCommanderSetSlot[iCommanderSlot])
			{
				vDeckButtons[iCommanderSlot].EmptyItemData();
			}
		}
		_UpdateSetSlotIdx();
	}
	//-------------------------------------------------------------------------------------------------------
	// 리스트에서 선택한 아이템
	private void _OnClickCommanderItem(int pSelectDataIndex, int pSelectItemNumber)
	{
		Transform lCommanderButtonTrasnform = vCommanderItemListData.GetItem(pSelectItemNumber);
		DeckListButton lDeckListButton = lCommanderButtonTrasnform.GetComponent<DeckListButton>();
		if (lDeckListButton.aIsAssigned)
			return;

		lDeckListButton.SetAssigned(true);
		CommanderItemData lSelectCommanderItemData = (CommanderItemData)vCommanderItemListData.GetData(pSelectDataIndex);
		//lSelectCommanderItemData.mSlotNo[mCurrentDeckId] = mCurrentSetSlotIdx;
		vDeckButtons[mCurrentSetSlotIdx].SetItemData(mCurrentSetSlotIdx, lSelectCommanderItemData);
		mSearchDeckSlotIdxToItemIdx.Add(mCurrentSetSlotIdx, pSelectDataIndex);

		mCommanderDeckSettingData.SetDeckItem(lSelectCommanderItemData, mCurrentSetSlotIdx);

		_UpdateSetSlotIdx();

		if (!mChangedCommanderItem.ContainsKey(lSelectCommanderItemData.mCommanderItemIdx))
		{
			mChangedCommanderItem.Add(lSelectCommanderItemData.mCommanderItemIdx, lSelectCommanderItemData);
		}
		if (!mChangedDeck.ContainsKey(mCurrentDeckId))
		{
			mChangedDeck.Add(mCurrentDeckId, mCommanderDeckSettingData);
		}

	}
	//-------------------------------------------------------------------------------------------------------
	// 다음에 채울 슬롯을 결정합니다.
	private void _UpdateSetSlotIdx()
	{
		if (mCurrentSetSlotIdx > 0)
		{
			vDeckButtons[mCurrentSetSlotIdx].SetAvailableSetItem(false);
		}
		for (int iButton = 0; iButton < vDeckButtons.Length; iButton++)
		{
			if (vDeckButtons[iButton].aIsEmpty)
			{
				mCurrentSetSlotIdx = iButton;
				vDeckButtons[mCurrentSetSlotIdx].SetAvailableSetItem(true);
				break;
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 업데이트합니다.
	private void _OnCommanderItemDeckUpdate()
	{
		for (int iCommanderItem = 0; iCommanderItem < GameData.get.aPlayerInfo.mCommanderItemData.Count; iCommanderItem++)
		{
			CommanderItemData.Copy(mCommanderItemSettingData[iCommanderItem], GameData.get.aPlayerInfo.mCommanderItemData[iCommanderItem]);
		}
		DeckData.Copy(mCommanderDeckSettingData, GameData.get.aPlayerInfo.mCommanderDeckData[mCurrentDeckId]);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private List<CommanderItemData> mCommanderItemSettingData;
	private DeckData mCommanderDeckSettingData;

	private bool[] mCommanderSetSlot;
	//private Stack<int> mAvailableSlot;
	private int mCurrentSetSlotIdx;
	private int mCurrentDeckId;
	private Dictionary<int, int> mSearchDeckSlotIdxToItemIdx;

	private Dictionary<int, CommanderItemData> mChangedCommanderItem;
	private Dictionary<int, DeckData> mChangedDeck;
}
