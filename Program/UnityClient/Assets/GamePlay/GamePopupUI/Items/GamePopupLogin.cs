﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupLogin : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public UIInput vUserNameInput;
	public UIToggle vIsAutoLoginToggle;
	public UIButton vLoginButton;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (String.IsNullOrEmpty(vUserNameInput.value))
		{
			vLoginButton.isEnabled = false;
		}
		else
		{
			vLoginButton.isEnabled = true;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		vUserNameInput.value = PlayerPrefs.GetString("LoginName");
		if (String.IsNullOrEmpty(vUserNameInput.value))
		{
			vUserNameInput.value = String.Empty; // default name
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비 컨트롤 등록
	public void SetLobbyControl(CombatLobbyControl pLobbyControl)
	{
		vLobbyControl = pLobbyControl;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 요청
	public void OnLogin()
	{
		if (String.IsNullOrEmpty(vUserNameInput.value))
		{
			MessageBoxManager.aInstance.OpenMessageBox("Null Login Name.\nPlease fill this place.", MessageBoxManager.MessageBoxType.OK);
			return;
		}
		PlayerPrefs.SetString("LoginName", vUserNameInput.value);
		if (String.IsNullOrEmpty(vUserNameInput.value))
		{
			// message box 출력
			Debug.LogError("Empty Loing User Name. please insert user name");
			return;
		}
		vLobbyControl.GoLogin(vUserNameInput.value);
		GameData.get.aAutoLogin = vIsAutoLoginToggle.value;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 창에서 back 버튼은 먹히지 않습니다.
	public override void BackPopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
