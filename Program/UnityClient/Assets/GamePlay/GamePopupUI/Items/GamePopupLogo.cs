﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupLogo : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<UITexture> vLogoTextureList;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		mCurrentLogoIndex = 0;
		vLogoTextureList[mCurrentLogoIndex].gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 창에서 back 버튼은 먹히지 않습니다.
	public override void BackPopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로고 한장이 끝날 때 마다 호출됩니다.
	public void OnEndLogoImage()
	{
		vLogoTextureList[mCurrentLogoIndex].gameObject.SetActive(false);
		mCurrentLogoIndex++;
		if (mCurrentLogoIndex >= vLogoTextureList.Count)
			_OnEndLogo();
		else
			vLogoTextureList[mCurrentLogoIndex].gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 모든 로고가 끝날 경우 호출됩니다.
	private void _OnEndLogo()
	{
		GamePopupUIManager.aInstance.ClosePopup();
		GameData.get.aLobbyControl.OnEndLogo();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mCurrentLogoIndex;
}
