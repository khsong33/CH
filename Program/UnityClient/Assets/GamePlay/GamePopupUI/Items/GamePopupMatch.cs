﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupMatch : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vAreaImage;
	public UILabel vAreaLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vAreaImage == null)
			Debug.LogError("vAreaIamge is empty - " + this);
		if (vAreaLabel == null)
			Debug.LogError("vAreaLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste += _OnWaitUnRegiste;
		_SetMatchInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste -= _OnWaitUnRegiste;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 창에서 back 버튼은 먹히지 않습니다.
	public override void BackPopup()
	{
		base.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 매칭을 취소합니다.
	public void CancelMatching()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_WaitUnRegiste();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 큐 취소 콜백
	private void _OnWaitUnRegiste()
	{
		BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 큐에 표시될 유저정보를 표시합니다.
	private void _SetMatchInfo()
	{
		mAreaSpec = AreaTable.get.GetAreaSpec(GameData.get.aPlayerInfo.mArea);
		vAreaImage.mainTexture = TextureResourceManager.get.LoadTexture(mAreaSpec.mIconAssetKey);
		vAreaLabel.text = LocalizedTable.get.GetLocalizedText(mAreaSpec.mNameKey);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private AreaSpec mAreaSpec;
}
