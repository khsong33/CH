﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupMyArmy : GamePopupUIItem
{
	public enum PopupState
	{
		DeckList,
		SetDeck
	}

	public delegate void OnDropCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton);
	public delegate void OnPutupCommanderItem(MyArmyFocusCommander pMyArmyCommanderButton);
	public OnDropCommanderItem aOnDropCommanderItem { get; set; }
	public OnPutupCommanderItem aOnPutupCommanderItem { get; set; }

	public DeckData aSelectDeckData { get { return mSettingDeckData; } }
	public bool aIsFullDeckItem { set; get; }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public MyArmyCommanderPanel vCommanderPanel;
	public MyArmyDeckPanel vDeckPanel;
	public MyArmySetDeckPanel vSetDeckPanel;
	public MyArmyNationSelectPanel vNationSelectPanel;
	public MyArmyCommanderInfoPanel vInfoPanel;
	public MyArmyNationCategoryPanel vNationCategoryPanel;
	public MyArmyDeckInfoPanel vDeckInfoPanel;
	public MyArmyFocusCommander vFocusCommander;
	public List<GameObject> vFocusColliderGroup;

	public PopupState vPopupState = PopupState.DeckList;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vCommanderPanel == null)
			Debug.LogError("vCommanderPanel is empty - " + this);
		if (vDeckPanel == null)
			Debug.LogError("vDeckPanel is empty - " + this);
		if(vSetDeckPanel == null)
			Debug.LogError("vSetDeckPanel is empty - " + this);
		if (vNationSelectPanel == null)
			Debug.LogError("vNationSelectPanel is empty - " + this);
		if (vInfoPanel == null)
			Debug.LogError("vInfoPanel is empty - " + this);
		if (vNationCategoryPanel == null)
			Debug.LogError("vNationCategoryPanel is empty - " + this);
		if (vDeckInfoPanel == null)
			Debug.LogError("vDeckInfoPanel is empty - " + this);
		if(vFocusCommander == null)
			Debug.LogError("vFocusCommander is empty - " + this);
		if(vFocusColliderGroup.Count <= 0)
			Debug.LogError("vFocusColliderGroup is empty - " + this);

		vFocusCommander.gameObject.SetActive(false);
		for (int iCollider = 0; iCollider < vFocusColliderGroup.Count; iCollider++)
		{
			vFocusColliderGroup[iCollider].SetActive(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		vLobbyControl = GameData.get.aLobbyControl;

		vCommanderPanel.gameObject.SetActive(true);
		vDeckPanel.gameObject.SetActive(true);
		vSetDeckPanel.gameObject.SetActive(false);
		vNationSelectPanel.gameObject.SetActive(false);
		vInfoPanel.gameObject.SetActive(false);
		vDeckInfoPanel.gameObject.SetActive(false);

		StartCoroutine(_OnOpenPopup());

		vDeckPanel.vDeckListData.InfiniteItemIsClickedEvent += _OnSetDeckClicked;
	}
	private IEnumerator _OnOpenPopup()
	{
		yield return null;
		if (vLobbyControl == null)
		{
			vCommanderPanel.SetCommanderList(CustomBattleCommanderItemTable.get.GetAllTestCommanderItemData(String.Empty));
			vDeckPanel.SetDeckList(CustomBattleCommanderItemTable.get.GetDeckDataList(String.Empty));
			vNationSelectPanel.SetNationInfos();
		}
		else
		{
			// 정식 데이터 [10/23/2015 khsong33]
			vCommanderPanel.SetCommanderList(GameData.get.aPlayerInfo.mCommanderItemData);
			vDeckPanel.SetDeckList(GameData.get.aPlayerInfo.mCommanderDeckDataList);
			vNationSelectPanel.SetNationInfos();
			// 정식 데이터 끝 [10/23/2015 khsong33]
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		vDeckPanel.vDeckListData.InfiniteItemIsClickedEvent -= _OnSetDeckClicked;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 창에서 back 버튼은 먹히지 않습니다.
	public override void BackPopup()
	{
		// Deck 이름 변경시에는 뒤로가기가 막힙니다.
		if (vSetDeckPanel.IsDeckNameSetting())
		{
			return;
		}
		if (vNationCategoryPanel.aIsOpen)
		{
			vNationCategoryPanel.ClosePanel();
			return;
		}
		if (vDeckInfoPanel.aIsOpen)
		{
			vDeckInfoPanel.ClosePanel();
			return;
		}

		base.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 설정을 완료합니다.
	public void OnCompleteDeckSetting()
	{
		vSetDeckPanel.gameObject.SetActive(false);
		vDeckPanel.gameObject.SetActive(true);

		if (vLobbyControl == null)
		{
			vDeckPanel.SetDeckList(CustomBattleCommanderItemTable.get.GetDeckDataList(String.Empty));
		}
		else
		{
			vDeckPanel.SetDeckList(GameData.get.aPlayerInfo.mCommanderDeckDataList);
		}
		vCommanderPanel.SetNationFilter(DeckNation.None);
		vPopupState = PopupState.DeckList;
		mSettingDeckData = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 설정을 취소합니다.
	public void OnCancelDeckSetting()
	{
		vSetDeckPanel.gameObject.SetActive(false);
		vDeckPanel.gameObject.SetActive(true);

		vCommanderPanel.SetNationFilter(DeckNation.None);
		vPopupState = PopupState.DeckList;
		mSettingDeckData = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 새로운 덱 국가 설정을 완료합니다.
	public void OnSelectNewDeckNation(DeckNation pDeckNation)
	{
		vNationSelectPanel.gameObject.SetActive(false);
		vCommanderPanel.gameObject.SetActive(true);
		vSetDeckPanel.gameObject.SetActive(true);

		NationSpec lNationSpec = NationTable.get.GetNationSpec(pDeckNation);

		DeckData lNewDeckData = new DeckData();
		lNewDeckData.mDeckNation = pDeckNation;
		lNewDeckData.mDeckName = LocalizedTable.get.GetLocalizedText(lNationSpec.mDefaultDeckNameKey);

		_GoSetDeck(lNewDeckData, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 확인합니다.
	public void OnClickCommander(MyArmyCommanderButton pButton)
	{
		if (pButton == mSelectButton)
		{
			ClearCommanderButton();
		}
		else
		{
			if (mSelectButton != null)
				mSelectButton.SetActiveSelectPopup(false);
			mSelectButton = pButton;
			mSelectButton.SetActiveSelectPopup(true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 선택 버튼을 해제합니다.
	public void ClearCommanderButton()
	{
		if (mSelectButton != null)
			mSelectButton.SetActiveSelectPopup(false);
		mSelectButton = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 확인합니다.
	public void OnOpenCommanderInfo(CommanderItem pCommanderItem, CommanderItemData pCommanderItemData)
	{
		//vInfoPanel.gameObject.SetActive(true);
		//vInfoPanel.SetCommander(pCommanderItem, pCommanderItemData);
		GameData.get.SetActiveCommanderInfoPanel(true, pCommanderItem, pCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 정보를 닫습니다.
	public void OnClickCommanderInfo()
	{
		vInfoPanel.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// Nation category 북마크를 엽니다.
	public void OpenNationCategory()
	{
		if(vPopupState == PopupState.DeckList)
			vNationCategoryPanel.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// Nation category 북마크를 닫습니다.
	public void CloseNationCategory()
	{
		vNationCategoryPanel.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// Nation Select panel을 닫습니다.
	public void CloseSelectNation()
	{
		vNationSelectPanel.gameObject.SetActive(false);
		vCommanderPanel.gameObject.SetActive(true);
		vDeckPanel.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// DeckInfoPanel을 엽니다.
	public void OpenDeckInfoPanel()
	{
		vDeckInfoPanel.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// DeckInfoPanel을 닫습니다.
	public void CloseDeckInfoPanel()
	{
		vDeckInfoPanel.gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// Deck Setting 버튼을 클릭합니다.
	private void _OnSetDeckClicked(int itemDataIndex, int itemNumber)
	{
		MyArmyDeckListButton lSelectDeckButton = vDeckPanel.GetDeckButton(itemDataIndex);
		if (lSelectDeckButton.aButtonType == MyArmyDeckListButton.MyArmyDeckListButtonType.NewDeck)
		{
			_GoNewDeck();
		}
		else
		{
			DeckData lDeckData = lSelectDeckButton.aDeckData;
			_GoSetDeck(lDeckData, false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Deck Setting 으로 들어갑니다.
	private void _GoSetDeck(DeckData pDeckData, bool pIsNewDeck)
	{
		vCommanderPanel.gameObject.SetActive(true);
		vDeckPanel.gameObject.SetActive(false);
		vSetDeckPanel.gameObject.SetActive(true);
		vNationSelectPanel.gameObject.SetActive(false);

		mSettingDeckData = vSetDeckPanel.SetDeckInfo(pDeckData, pIsNewDeck);
		vCommanderPanel.SetNationFilter(pDeckData.mDeckNation);

		vPopupState = PopupState.SetDeck;
	}
	//-------------------------------------------------------------------------------------------------------
	// New deck Setting 으로 들어갑니다.
	private void _GoNewDeck()
	{
		vCommanderPanel.gameObject.SetActive(false);
		vDeckPanel.gameObject.SetActive(false);
		vSetDeckPanel.gameObject.SetActive(false);
		vNationSelectPanel.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private DeckData mSettingDeckData;
	private MyArmyCommanderButton mSelectButton;
}
