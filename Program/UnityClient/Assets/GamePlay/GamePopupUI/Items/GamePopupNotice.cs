﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityStandardAssets.ImageEffects;

public class GamePopupNotice : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public TextAsset vNoticeText;
	public UILabel vNoticeLabel;
	public GameObject vScrollViewColliderObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vNoticeText == null)
			Debug.LogError("vNoticeText is empty - " + this);
		if (vNoticeLabel == null)
			Debug.LogError("vNoticeLabel is empty - " + this);
		if (vScrollViewColliderObject == null)
			Debug.LogError("vScrollViewColliderObject is empty - " + this);

	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// UI 활성화
	public override void OnOpenPopup()
	{
		vNoticeLabel.text = vNoticeText.text;
		BoxCollider lCollider = (BoxCollider)vScrollViewColliderObject.GetComponent<BoxCollider>();
		lCollider.size = new Vector3(lCollider.size.x, vNoticeLabel.height, lCollider.size.z);
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 비활성화
	public override void OnClosePopup()
	{

	}
	//-------------------------------------------------------------------------------------------------------
	// UI 닫기
	public override void ClosePopup()
	{
		base.ClosePopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 뒤로 가기
	public override void BackPopup()
	{
		base.BackPopup();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	// private int mSleepTimeoutSave;
}
