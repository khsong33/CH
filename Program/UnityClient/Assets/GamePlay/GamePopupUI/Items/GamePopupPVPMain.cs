﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupPVPMain : GamePopupUIItem
{
	public enum PvpMainDeckCategory
	{
		Normal = 0,
		Custom
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<UIButton> vCategoryButton;
	public PVPDeckCategory vCategoryPanel;
	public PVPRegistePanel vRegistePanel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vCategoryPanel == null)
			Debug.LogError("vCategoryPanel is empty - " + this);
		if(vRegistePanel == null)
			Debug.LogError("vRegistePanel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		SelectCategoryCustomDeck();
		vRegistePanel.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 카테고리 선택
	public void SelectCategoryNormalDeck()
	{
		mCategory = PvpMainDeckCategory.Normal;
		_RefreshCategory();
		vCategoryPanel.Init(GameData.get.aPlayerInfo.mCommanderDeckDataList); // Defalut로 변경해야됨..
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 카테고리 선택
	public void SelectCategoryCustomDeck()
	{
		mCategory = PvpMainDeckCategory.Custom;
		_RefreshCategory();
		vCategoryPanel.Init(GameData.get.aPlayerInfo.mCommanderDeckDataList);
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 등록 판넬 열기
	public void OpenRegistePanel(DeckData pDeckData)
	{
		vRegistePanel.gameObject.SetActive(true);
		vRegistePanel.SetDeckInfo(pDeckData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 뒤로가기 버튼 클릭시 처리
	public override void BackPopup()
	{
		if (vRegistePanel.gameObject.activeInHierarchy)
		{
			vRegistePanel.OnBack();
		}
		else
		{
			base.BackPopup();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 카테고리 버튼 상태를 재설정합니다
	private void _RefreshCategory()
	{
		for (int iCategory = 0; iCategory < vCategoryButton.Count; iCategory++)
		{
			vCategoryButton[iCategory].isEnabled = (iCategory != (int)mCategory);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private PvpMainDeckCategory mCategory;
}
