﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupPVPReady : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public List<RoomUserPanel> vRoomUserPanelList;
	public UILabel vCountLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		vCountLabel.gameObject.SetActive(true);
		StartCoroutine(_StartGame(3));
	}
	private IEnumerator _StartGame(int pStartCount)
	{
		int lCurrentCount = 0;
		while (pStartCount > lCurrentCount)
		{
			yield return new WaitForSeconds(1.0f);
			lCurrentCount++;
			vCountLabel.text = String.Format("{0}", pStartCount - lCurrentCount);
		}
		vCountLabel.gameObject.SetActive(false);
		OnReady();
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비 컨트롤 등록
	public void SetLobbyControl(CombatLobbyControl pLobbyControl)
	{
		vLobbyControl = pLobbyControl;
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 대기 등록
	public void OnReady()
	{
		Debug.Log("On Ready");
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 방 나가기
	public void OnRoomExit()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 유저 정보 셋팅
	public void SetUserInfos(List<GameUserInfo> pUserInfoList)
	{
		Debug.Log("pUserInfoList.Count - " + pUserInfoList.Count);
		if (pUserInfoList.Count != vRoomUserPanelList.Count)
			Debug.LogError("Not enough room user info socket. please check user socket ui");

		gameObject.SetActive(true);
		for (int iUser = 0; iUser < pUserInfoList.Count; iUser++)
		{
			vRoomUserPanelList[iUser].SetUserInfo(pUserInfoList[iUser]);
		}
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	//private bool mIsGameReady = false; // 플레이어가 게임 시작 준비가 되었는지
}
