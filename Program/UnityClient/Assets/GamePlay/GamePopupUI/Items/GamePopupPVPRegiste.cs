﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupPVPRegiste : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vWaiting;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitRegiste += _OnWaitRegiste;
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste += _OnWaitUnRegiste;
		vWaiting.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitRegiste -= _OnWaitRegiste;
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste -= _OnWaitUnRegiste;
	}
	//-------------------------------------------------------------------------------------------------------
	// PVP 큐 등록
	public void GoPVPRegiste()
	{
		if (GameData.get.aLobbyControl != null)
			GameData.get.aLobbyControl.GoWaitRegiste(0);
		else
			Debug.LogError("Lobby Control is Empty - " + gameObject.name);

		GamePopupUIManager.aInstance.ClosePopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 뒤로가기 버튼 클릭시 처리
	public override void BackPopup()
	{
		if (GameData.get.aLobbyControl.aIsWaitGame)
		{
			GoPVPRegiste();
		}
		base.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [서버콜백] PVP 대기
	private void _OnWaitRegiste()
	{
		vWaiting.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// [서버콜백] PVP 대기 취소
	private void _OnWaitUnRegiste()
	{
		vWaiting.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
