﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class GamePopupReplayList : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public InfiniteListPopulator vScrollList;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		vScrollList.InfiniteItemIsClickedEvent += _OnClick;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		vLobbyControl = GameData.get.aLobbyControl;
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnReplayList += _OnReplayList;
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnReplayData += _OnReplayData;

		if (!GameData.get.aPlayerInfo.mIsReplayData)
			vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ReplayList();
		else
			SetReplayList(GameData.get.aPlayerInfo.mReplayDataList);
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnReplayList -= _OnReplayList;
		vLobbyControl.vNetworkLobby.aILobbyProtocol.aOnReplayData -= _OnReplayData;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리플레이 데이터 설정
	public void SetReplayList(List<ReplayData> pReplayList)
	{
		ArrayList lData = new ArrayList();
		for (int iReplay = 0; iReplay < pReplayList.Count; iReplay++)
		{
			lData.Add(pReplayList[iReplay]);
		}
		vScrollList.InitTableView(lData, null, 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 확인 버튼을 클릭합니다.
	public void OnConfirmReplayData()
	{
		if(mSelectReplayData != null && !String.IsNullOrEmpty(mSelectReplayData.mLogKey))
			vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ReplayData(mSelectReplayData.mLogKey);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 클릭 처리 - 리플레이 선택
	private void _OnClick(int pSelectDataIndex, int pSelectItemNumber)
	{
		mSelectReplayData = (ReplayData)vScrollList.GetData(pSelectDataIndex);
		// Debug.Log("Select LogKey [" + pData.mLogKey + "]");
		for (int iButton = 0; iButton < vScrollList.aItemPoolCount; iButton++)
		{
			Transform lItemTransform = vScrollList.GetItem(iButton);
			ReplayListButton lListButton = lItemTransform.GetComponent<ReplayListButton>();
			if (!lListButton.aIsAvailable)
				continue;

			if (lListButton.aLogKey.Equals(mSelectReplayData.mLogKey))
				lListButton.OnSelectButton(true);
			else
				lListButton.OnSelectButton(false);
		}
		//vLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ReplayData(pData.mLogKey);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리플레이 서버 콜백
	private void _OnReplayList(List<ReplayData> pReplayList)
	{
		GameData.get.aPlayerInfo.mReplayDataList.Clear();
		GameData.get.aPlayerInfo.mReplayDataList = pReplayList;
		GameData.get.aPlayerInfo.mIsReplayData = true;
		SetReplayList(pReplayList);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리플레이 데이터 콜백
	private void _OnReplayData(BattleVerifyData pVerifyData)
	{
		GameData.get.EnterReplayPhase(pVerifyData, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//------------------------------------------------------------------------------------;-------------------
	private ReplayData mSelectReplayData;
}

public class ReplayData
{
	public int mIndex;
	public String mLogKey;
	public DateTime mLogTime;
}