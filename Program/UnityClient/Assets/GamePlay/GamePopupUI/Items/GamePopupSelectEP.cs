﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupSelectEP : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public CombatLobbyControl vLobbyControl;
	public InfiniteListPopulator vEpisodeScrollList;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		vEpisodeScrollList.InfiniteItemIsClickedEvent += _OnClick;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비 컨트롤 등록
	public void SetLobbyControl(CombatLobbyControl pLobbyControl)
	{
		SetFullBackground();
		vLobbyControl = pLobbyControl;
	}
	//-------------------------------------------------------------------------------------------------------
	// 에피소드 데이터 설정
	public void SetEpisodeData(List<EpisodeData> pEpisodeList)
	{
		ArrayList lData = new ArrayList();
		for (int iEpisode = 0; iEpisode < pEpisodeList.Count; iEpisode++)
		{
			lData.Add(pEpisodeList[iEpisode]);
		}
		vEpisodeScrollList.InitTableView(lData, null, 0);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 클릭 처리 - 에피소드 선택
	private void _OnClick(int pSelectDataIndex, int pSelectItemNumber)
	{
		EpisodeData pData = (EpisodeData)vEpisodeScrollList.GetData(pSelectDataIndex);
		Debug.Log("Select Data Name[" + pData.mEpName + "]");
		GamePopupUIManager.aInstance.ClosePopup();
		//vLobbyControl.ShowMyInfo();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}

public class EpisodeData
{
	public int mIndex;
	public String mEpName;
}