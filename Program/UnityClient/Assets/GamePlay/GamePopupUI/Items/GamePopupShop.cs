﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;
public enum ShopType
{
	Commander = 0,
	Chest,
	Gem,
	Gold,
}
public class GamePopupShop : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<ShopTapButton> vTapButtonList;
	public List<ShopItemList> vShopItemGroupList;
	public GameObject vShopPopupRoot;
	public List<GameObject> vShopPopupPanelList;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		for (int iType = 0; iType < vTapButtonList.Count; iType++)
		{
			if ((int)vTapButtonList[iType].vShopType != iType)
				Debug.LogError("Not Same Shop Type to TapButton - " + (ShopType)iType);
			if ((int)vShopItemGroupList[iType].vShopType != iType)
				Debug.LogError("Not Same Shop Type to Item Group List - " + (ShopType)iType);
		}
		if (vShopPopupRoot == null)
			Debug.LogError("vShopPopupRoot is empty - " + this);

		mCurrentShopType = ShopType.Commander;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		for (int iGroup = 0; iGroup < vShopItemGroupList.Count; iGroup++)
		{
			vShopItemGroupList[iGroup].SetShopList();
		}
		OnClickTapButton(mCurrentShopType);
		SetActiveShopPopup(mCurrentShopType, mIsShopPopupOpen, mSelectButtonIndex);

		vShopPopupRoot.SetActive(false);
		for (int iPopup = 0; iPopup < vShopPopupPanelList.Count; iPopup++)
		{
			vShopPopupPanelList[iPopup].SetActive(false);
		}

		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnGameServerError += _OnGameServerError;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenChest += _OnOpenChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGem += _OnShopBuyGem;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGold += _OnShopBuyGold;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander += _OnShopBuyCommander;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnGameServerError -= _OnGameServerError;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenChest -= _OnOpenChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGem -= _OnShopBuyGem;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGold -= _OnShopBuyGold;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander -= _OnShopBuyCommander;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Tap 버튼을 클릭합니다.
	public void OnClickTapButton(ShopType pShopType)
	{
		for(int iType = 0; iType < vTapButtonList.Count; iType++)
		{
			vShopItemGroupList[iType].gameObject.SetActive(iType == (int)pShopType);
			vTapButtonList[iType].vSelectedObject.SetActive(iType == (int)pShopType);
		}
		mCurrentShopType = pShopType;
	}
	//-------------------------------------------------------------------------------------------------------
	// Main Shop 뒤로 가기 클릭
	public void GoBackMainShop()
	{
		GamePopupUIManager.aInstance.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// ShopPopup 뒤로 가기 클릭
	public void GoBackShopPopup()
	{
		vShopPopupRoot.SetActive(false);
		for (int iPopup = 0; iPopup < vShopPopupPanelList.Count; iPopup++)
		{
			vShopPopupPanelList[iPopup].SetActive(false);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Shop Popup 창을 활성/비활성화 합니다.
	public void SetActiveShopPopup(ShopType pShopType, bool pIsActive, int pSelectButtonIndex)
	{
		mIsShopPopupOpen = pIsActive;
		mSelectButtonIndex = pSelectButtonIndex;

		vShopPopupRoot.SetActive(pIsActive);
		vShopPopupPanelList[(int)pShopType].SetActive(pIsActive);
		if (mIsShopPopupOpen)
		{
			switch (pShopType)
			{
				case ShopType.Commander:
					if (GameData.get.aPlayerInfo != null)
					{
						ShopPopupCommander lShopPopup = vShopPopupPanelList[(int)pShopType].GetComponent<ShopPopupCommander>();
						lShopPopup.SetPopupCommanderItem(GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList[mSelectButtonIndex]);
					}
					break;
				case ShopType.Chest:
					if (GameData.get.aPlayerInfo != null)
					{
						ShopPopupChest lShopPopup = vShopPopupPanelList[(int)pShopType].GetComponent<ShopPopupChest>();
						lShopPopup.SetPopupChest(GameData.get.aPlayerInfo.mShopChestDataList[mSelectButtonIndex]);
					}
					break;
				case ShopType.Gem:
					break;
				case ShopType.Gold:
					if (GameData.get.aPlayerInfo != null)
					{
						ShopPopupGold lShopPopup = vShopPopupPanelList[(int)pShopType].GetComponent<ShopPopupGold>();
						lShopPopup.SetPopupGold(GameData.get.aPlayerInfo.mShopGoldDataList[mSelectButtonIndex]);
					}
					break;
				default:
					break;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 상자 열기
	private void _OnOpenChest(ChestOpenData pChestOpenSpec)
	{
		GoBackShopPopup();
		_RefreshShopList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 잼 구매
	private void _OnShopBuyGem(int pGainGem)
	{
		_RefreshShopList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 골드 구매
	private void _OnShopBuyGold(int pGainGold)
	{
		GoBackShopPopup();
		_RefreshShopList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 구매
	private void _OnShopBuyCommander(int pUpdateShopIdx)
	{
		GoBackShopPopup();
		_RefreshShopList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 에러 처리
	private void _OnGameServerError(NetworkErrorCode pNetworkErrorCode)
	{
		GoBackShopPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// ShopList 재설정
	private void _RefreshShopList()
	{
		for (int iGroup = 0; iGroup < vShopItemGroupList.Count; iGroup++)
		{
			vShopItemGroupList[iGroup].RefreshShopList();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private ShopType mCurrentShopType;
	private bool mIsShopPopupOpen;
	private int mSelectButtonIndex;
}
