﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class GamePopupTutorial : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vGuideImage;
	public GameObject vGuideNextButton;
	public GameObject vGuidePrevButton;
	
	public List<Transform> vButtonRoots;
	public TutorialItemButton vButtonPrefab;

	public GameObject vStartButton;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		if (vGuideImage == null)
			Debug.LogError("vGuideImage is empty - " + this);
		if (vGuideNextButton == null)
			Debug.LogError("vGuideNextButton is empty - " + this);
		if (vGuidePrevButton == null)
			Debug.LogError("vGuidePrevButton is empty - " + this);
		if (vButtonPrefab == null)
			Debug.LogError("vButtonPrefab is empty - " + this);
		if (vStartButton == null)
			Debug.LogError("vStartButton is empty - " + this);
		if (vButtonRoots.Count != TutorialTable.get.aTutorialCount)
			Debug.LogError("Not Enough Tutorial button - " + this);

		mMaxGuidePage = 0;
		mCurrentGuideIdx = 0;
		mGuideAssetKeyList = new List<AssetKey>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnStartTutorial2 += _OnStartTutorial2;
		}
		StartCoroutine(_SetTutorialButton());
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 버튼을 설정합니다.
	private IEnumerator _SetTutorialButton()
	{
		yield return null;
		if (GameData.get.aPlayerInfo != null)
			mCurrentOpenIndex = GameData.get.aPlayerInfo.mClearTutorialIdx + 1;
		else // 테스트
			mCurrentOpenIndex = 5;
		for (int iTutorial = 0; iTutorial < TutorialTable.get.aTutorialCount; iTutorial++)
		{
			yield return null;

			GameObject lTutorialButtonObject = (GameObject)Instantiate(vButtonPrefab.gameObject);
			lTutorialButtonObject.transform.parent = vButtonRoots[iTutorial];
			lTutorialButtonObject.transform.localPosition = Vector3.zero;
			lTutorialButtonObject.transform.localScale = Vector3.one;

			if (mCurrentOpenIndex >= iTutorial)
			{
				TutorialItemButton lButton = lTutorialButtonObject.GetComponent<TutorialItemButton>();
				TutorialSpec lTutorialSpec = TutorialTable.get.GetTutorialSpecByClearIdx(iTutorial);
				lButton.SetCommander(iTutorial, lTutorialSpec.mCommanderItemData);

				mMaxGuidePage += lTutorialSpec.mGuideAssetKeyList.Count;
				for(int iGuide = 0; iGuide < lTutorialSpec.mGuideAssetKeyList.Count; iGuide++)
				{
					mGuideAssetKeyList.Add(lTutorialSpec.mGuideAssetKeyList[iGuide]);
				}
				if (mCurrentOpenIndex == iTutorial)
				{
					mCurrentGuideIdx = mGuideAssetKeyList.Count - lTutorialSpec.mGuideAssetKeyList.Count;
					mTutorialSpec = lTutorialSpec;
					lButton.SetState(TutorialItemButton.TutorialButtonState.Active);
				}
				else
				{
					lButton.SetState(TutorialItemButton.TutorialButtonState.Clear);
				}
			}
		}
		_SetGuidePage();
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnStartTutorial2 -= _OnStartTutorial2;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// back 버튼
	public override void BackPopup()
	{
		base.BackPopup();
	}
	//-------------------------------------------------------------------------------------------------------
	// Guide Right button 클릭
	public void OnNextGuidePage()
	{
		if(mCurrentGuideIdx + 1 >= mMaxGuidePage)
			return;
		mCurrentGuideIdx++;
		_SetGuidePage();
	}
	//-------------------------------------------------------------------------------------------------------
	// Guide Left button 클릭
	public void OnPrevGuidePage()
	{
		if (mCurrentGuideIdx - 1 < 0)
			return;
		mCurrentGuideIdx--;
		_SetGuidePage();
	}
	//-------------------------------------------------------------------------------------------------------
	// Guide Left button 클릭
	public void OnStartButton()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_StartTutorial(mTutorialSpec.mStageNo);
		else
			GameData.get.EnterBattleTestPhase(mTutorialSpec.mStageNo, true);

		vStartButton.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 현재 가이드 페이지를 출력합니다.
	private void _SetGuidePage()
	{
		vGuideImage.mainTexture = TextureResourceManager.get.LoadTexture(mGuideAssetKeyList[mCurrentGuideIdx]);
		
		if (mCurrentGuideIdx == 0)
			vGuidePrevButton.SetActive(false);
		else
			vGuidePrevButton.SetActive(true);
		
		if (mCurrentGuideIdx == mMaxGuidePage - 1)
			vGuideNextButton.SetActive(false);
		else
			vGuideNextButton.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 시작
	private void _OnStartTutorial2(String pBattleServ, JSONObject pBattleMatchInfoJSON)
	{
		GameData.get.EnterBattlePhase(pBattleServ, pBattleMatchInfoJSON, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mCurrentOpenIndex;
	private int mMaxGuidePage;
	private int mCurrentGuideIdx;
	private List<AssetKey> mGuideAssetKeyList;
	private TutorialSpec mTutorialSpec;
}
