﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupUITest : GamePopupUIItem
{
    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
    public UILabel vLabel;
    public InfiniteListPopulator vScrollList;

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
	void Awake()
	{
		SetTestScroll();
	}
    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // GamePopupUIItem 메서드
    public override void OnOpenPopup()
    {
    }
    //-------------------------------------------------------------------------------------------------------
    // GamePopupUIItem 메서드
    public override void OnClosePopup()
    {
    }
	//-------------------------------------------------------------------------------------------------------
	// 스크롤 테스트
	public void SetTestScroll()
	{
		if (vScrollList != null && !mIsCreateList)
		{
			ArrayList lData = new ArrayList();
			for (int i = 0; i < 15; i++)
			{
				ListTestData data = new ListTestData();
				data.mIndex = i;
				data.mName = String.Format("Test Name - {0}", (i));
				lData.Add(data);
			}
			vScrollList.InitTableView(lData, null, 5);

			mIsCreateList = true;
		}
	}
    //-------------------------------------------------------------------------------------------------------
    // 다른 UI 오픈
    public void OpenOtherUI()
    {
        GamePopupUITest lPopup = (GamePopupUITest)GamePopupUIManager.aInstance.OpenPopup("Popup Test2");
		lPopup.SetTestScroll();
    }
    //-------------------------------------------------------------------------------------------------------
    // TEST Label 값 입력
    public void SetLabel(String pLabelText)
    {
        vLabel.text = pLabelText;
    }

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private bool mIsCreateList = false;
}


public class ListTestData
{
    public int mIndex;
    public String mName;
}