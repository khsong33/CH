﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GamePopupUserInfo : GamePopupUIItem
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vUserNameLabel;
	public UILabel vUserLevelLabel;
	public UILabel vWinLabel;
	public UILabel vLoseLabel;
	public UILabel vWinStreakLabel;
	public UILabel vRatingLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnOpenPopup()
	{
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnUserInfo += _OnUserInfo;
	}
	//-------------------------------------------------------------------------------------------------------
	// GamePopupUIItem 메서드
	public override void OnClosePopup()
	{
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnUserInfo -= _OnUserInfo;
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보를 입력합니다.
	public void SetUserInfo(GameUserInfo pPlayerInfo)
	{
		vUserNameLabel.text = pPlayerInfo.mUserName;
		vUserLevelLabel.text = String.Format("Lv {0}", pPlayerInfo.mLevel);
		vWinLabel.text = pPlayerInfo.mWin.ToString();
		vLoseLabel.text = pPlayerInfo.mLose.ToString();
		vWinStreakLabel.text = pPlayerInfo.mWinStreak.ToString();
		vRatingLabel.text = "0";
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
