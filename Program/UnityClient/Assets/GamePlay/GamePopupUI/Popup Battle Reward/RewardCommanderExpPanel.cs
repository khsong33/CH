﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RewardCommanderExpPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<Transform> vExpCommanderRoots;
	public GameObject vExpCommanderPrefab;

	public GamePopupBattleReward vPopup;
	public int vUpdateCommanderExpSpeed = 100; // 초당 얼마나 증가
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vExpCommanderRoots.Count < BattleUserInfo.cMaxCommanderCount)
			Debug.LogError("Not Enough vExpCommanderRoots - " + this);
		if (vExpCommanderPrefab == null)
			Debug.LogError("vExpCommanderPrefab is empty - " + this);
		if (vPopup == null)
			Debug.LogError("vPopup is empty - " + this);

		mRewardExpCommanderList = new List<RewardExpCommander>();
		mEndProgressExpCount = 0;
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 전투 보상 셋팅
	public void SetBattleReward(BattleRewardInfo pRewardInfo)
	{
		mRewardInfo = pRewardInfo;
		for (int iReward = 0; iReward < mRewardInfo.mCommanderItemExpList.Count; iReward++)
		{
			BattleRewardCommanderItemExp lRewardCommanderExp = mRewardInfo.mCommanderItemExpList[iReward];

			GameObject lExpCommanderObject = (GameObject)Instantiate(vExpCommanderPrefab);
			lExpCommanderObject.transform.parent = vExpCommanderRoots[iReward];
			lExpCommanderObject.transform.localPosition = Vector3.zero;
			lExpCommanderObject.transform.localScale = Vector3.one;

			RewardExpCommander lExpCommander = lExpCommanderObject.GetComponent<RewardExpCommander>();
			lExpCommander.SetRewardCommanderExp(lRewardCommanderExp);
			lExpCommander.SetUpdateSpeed(vUpdateCommanderExpSpeed);

			lExpCommander.aOnEndProgressExp += _OnEndProgressExp;

			mRewardExpCommanderList.Add(lExpCommander);
		}
		vPopup.SetNextStep();
	}
	//-------------------------------------------------------------------------------------------------------
	// 다음 단계로 넘어갑니다.
	public void OnNextStep()
	{
		if (mRewardExpCommanderList.Count <= 0)
			vPopup.SetNextStep();

		for (int iCommander = 0; iCommander < mRewardExpCommanderList.Count; iCommander++)
		{
			mRewardExpCommanderList[iCommander].OnFlush();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 경험치 증가 효과가 끝날경우 호출됩니다.
	private void _OnEndProgressExp()
	{
		mEndProgressExpCount++;
		if (mEndProgressExpCount == mRewardExpCommanderList.Count)
		{
			vPopup.SetNextStep();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleRewardInfo mRewardInfo;
	private List<RewardExpCommander> mRewardExpCommanderList;
	private int mEndProgressExpCount;
}
