﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RewardCommanderPanel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIcon;
	public BattleUITroopCountInfo[] vTroopKindCountInfo;
	public UILabel vCommanderNameLabel;
	public UILabel vRankLabel;
	public UILabel vSpawnMpLabel;
	public UISlider vExpProgressBar;

	public GamePopupBattleReward vPopup;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIcon == null)
			Debug.LogError("vCommanderIcon is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if (vRankLabel == null)
			Debug.LogError("vRankLabel is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);

		if (vPopup == null)
			Debug.LogError("vPopup is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템을 셋팅합니다.
	public void SetCommanderInfo(CommanderItemData pCommanderItemData)
	{
		mCommanderItemData = pCommanderItemData;
		mCommanderItem = new CommanderItem(mCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);

		vCommanderIcon.mainTexture = mCommanderItem.mCommanderSpec.GetPortraitIconTexture();
		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);
		vRankLabel.text = mCommanderItem.mCommanderExpRank.ToString();
		vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(true);
				vTroopKindCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
			}
		}
		// 경험치 처리
		vExpProgressBar.value = 0.0f;
		vPopup.SetNextStep();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItemData mCommanderItemData;
	private CommanderItem mCommanderItem;
}
