﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RewardExpCommander : MonoBehaviour
{
	public delegate void OnEndProgressExp();

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIcon;
	public BattleUITroopCountInfo[] vTroopKindCountInfo;
	public UILabel vCommanderNameLabel;
	public UILabel vRankLabel;
	public UILabel vSpawnMpLabel;
	public UILabel vGainExpLabel;
	public UISlider vExpProgressBar;

	public GameObject vRankUpObject;

	public int vUpdateExpSpeed = 100;
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public OnEndProgressExp aOnEndProgressExp { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIcon == null)
			Debug.LogError("vCommanderIcon is empty - " + this);
		if (vCommanderNameLabel == null)
			Debug.LogError("vCommanderNameLabel is empty - " + this);
		if (vRankLabel == null)
			Debug.LogError("vRankLabel is empty - " + this);
		if (vSpawnMpLabel == null)
			Debug.LogError("vSpawnMpLabel is empty - " + this);
		if (vGainExpLabel == null)
			Debug.LogError("vGainExpLabel is empty - " + this);
		if (vRankUpObject == null)
			Debug.LogError("vRankUpObject is empty - " + this);
		if (vExpProgressBar == null)
			Debug.LogError("vExpProgressBar is empty - " + this);

		vRankUpObject.SetActive(false);
		mIsFlush = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 보상 정보를 처리합니다.
	public void SetRewardCommanderExp(BattleRewardCommanderItemExp pRewardCommanderExp)
	{
		mRewardCommanderExp = pRewardCommanderExp;

		mCommanderItemData = GameData.get.aPlayerInfo.FindItemFromItemIdx(mRewardCommanderExp.mItemIdx);
		mCommanderItem = new CommanderItem(mCommanderItemData.mCommanderItemIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, mCommanderItemData);

		vCommanderIcon.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();
		vCommanderNameLabel.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);
		vRankLabel.text = mCommanderItem.mCommanderExpRank.ToString();
		vSpawnMpLabel.text = mCommanderItem.aSpawnMp.ToString();
		int lGainExp = mRewardCommanderExp.mExp - mCommanderItem.mCommanderExp;
		vGainExpLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("CommanderGainExp{0}"), lGainExp);

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(true);
				vTroopKindCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
			}
		}
		// 경험치 처리
		float lExpRatio = 0.0f;
		if (mCommanderItem.mCommanderExpRank == CommanderSpec.cRankCount)
		{
			lExpRatio = 1.0f;
		}
		else
		{
			int lNextRankNeedExp = mCommanderItem.mCommanderSpec.mRankNeedExps[mCommanderItem.mCommanderExpRank + 1] - mCommanderItem.mCommanderSpec.mRankNeedExps[mCommanderItem.mCommanderExpRank];
			int lCurrentExp = mCommanderItem.mCommanderExp - mCommanderItem.mCommanderSpec.mRankNeedExps[mCommanderItem.mCommanderExpRank];
			lExpRatio = (float)lCurrentExp / lNextRankNeedExp;
		}
		vExpProgressBar.value = lExpRatio;

		StartCoroutine(_PlayExp());
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 진행 속도
	public void SetUpdateSpeed(int pSpeed)
	{
		vUpdateExpSpeed = pSpeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 바로 적용
	public void OnFlush()
	{
		mIsFlush = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 경험치 증가 효과를 시작합니다.
	private IEnumerator _PlayExp()
	{
		yield return null;

		int lCurrentTotalExp = mCommanderItem.mCommanderExp;
		int lCurrentRank = mCommanderItem.mCommanderExpRank;
		while ((lCurrentTotalExp != mRewardCommanderExp.mExp) && !mIsFlush)
		{
			yield return null;

			lCurrentTotalExp += (int)(vUpdateExpSpeed * Time.deltaTime);
			if (lCurrentTotalExp >= mRewardCommanderExp.mExp)
				lCurrentTotalExp = mRewardCommanderExp.mExp;

			int lNextRankNeedExp = mCommanderItem.mCommanderSpec.mRankNeedExps[lCurrentRank + 1] - mCommanderItem.mCommanderSpec.mRankNeedExps[lCurrentRank];
			int lCurrentExp = lCurrentTotalExp - mCommanderItem.mCommanderSpec.mRankNeedExps[lCurrentRank];
			float lExpRatio = 0.0f;
			if (lCurrentExp >= lNextRankNeedExp)
			{
				lCurrentRank++;
				vRankLabel.text = lCurrentRank.ToString();
				lExpRatio = 1.0f;
				vRankUpObject.SetActive(true);
			}
			else
			{
				lExpRatio = (float)lCurrentExp / lNextRankNeedExp;
			}
			vExpProgressBar.value = lExpRatio;
		}
		_FlushExpCommander();
	}
	//-------------------------------------------------------------------------------------------------------
	// 순간 적용
	private void _FlushExpCommander()
	{
		int lNextRankNeedExp = mCommanderItem.mCommanderSpec.mRankNeedExps[mRewardCommanderExp.mRank + 1] - mCommanderItem.mCommanderSpec.mRankNeedExps[mRewardCommanderExp.mRank];
		int lCurrentExp = mRewardCommanderExp.mExp - mCommanderItem.mCommanderSpec.mRankNeedExps[mRewardCommanderExp.mRank];
		float lExpRatio = (float)lCurrentExp / lNextRankNeedExp;

		if (mCommanderItem.mCommanderExpRank != mRewardCommanderExp.mRank)
			vRankUpObject.SetActive(true);

		vRankLabel.text = mRewardCommanderExp.mRank.ToString();
		vExpProgressBar.value = lExpRatio;

		if (aOnEndProgressExp != null)
			aOnEndProgressExp();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
	private CommanderItemData mCommanderItemData;

	private BattleRewardCommanderItemExp mRewardCommanderExp;
	private bool mIsFlush;
}
