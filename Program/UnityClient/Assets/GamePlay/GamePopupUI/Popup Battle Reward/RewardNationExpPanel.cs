﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class RewardNationExpPanel : MonoBehaviour
{

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vNationIcon;
	public UITexture vNationMarkIcon;
	public UILabel vNationLevel;
	public UISlider vNationExpProgressBar;
	public GameObject vLevelUpObject;

	public GamePopupBattleReward vPopup;

	public int vUpdateExpSpeed = 100;
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vNationIcon == null)
			Debug.LogError("vNationIcon is empty - " + this);
		if (vNationMarkIcon == null)
			Debug.LogError("vNationMarkIcon is empty - " + this);
		if (vNationLevel == null)
			Debug.LogError("vNationLevel is empty - " + this);
		if (vNationExpProgressBar == null)
			Debug.LogError("vNationExpProgressBar is empty - " + this);
		if (vLevelUpObject == null)
			Debug.LogError("vLevelUpObject is empty - " + this);

		if (vPopup == null)
			Debug.LogError("vPopup is empty - " + this);

		vLevelUpObject.SetActive(false);

		mIsFlush = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 정보를 입력합니다.
	public void SetBattleReward(BattleRewardInfo pBattleRewardInfo)
	{
		mBattleRewardInfo = pBattleRewardInfo;

		int lNationExp = GameData.get.aPlayerInfo.mNationData[mBattleRewardInfo.mNation].mNationExp;
		int lNationLevel = GameData.get.aPlayerInfo.mNationData[mBattleRewardInfo.mNation].mNationLevel;

		NationSpec lNationSpec = NationTable.get.GetNationSpec(mBattleRewardInfo.mNation);
		NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(lNationLevel);

		vNationIcon.mainTexture = lNationSpec.GetNationBackgroundIcon();
		vNationMarkIcon.mainTexture = lNationSpec.GetNationMarkIcon();

		vNationLevel.text = lNationLevel.ToString();

		//int lLevel = GameData.get.aPlayerInfo.mNationLevels[(int)mBattleRewardInfo.mNation];

		float lExpRatio = (float)(lNationExp - lNationLevelSpec.mNeedExp) / (lNationLevelSpec.mMaxExp - lNationLevelSpec.mNeedExp);
		vNationExpProgressBar.value = lExpRatio;

		StartCoroutine(_PlayExp());

		vPopup.SetNextStep();
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 적용 속도
	public void SetExpUpdateSpeed(int pSpeed)
	{
		vUpdateExpSpeed = pSpeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 바로 적용
	public void OnFlush()
	{
		mIsFlush = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 경험치 증가 효과를 시작합니다.
	private IEnumerator _PlayExp()
	{
		int lCurrentTotalExp = GameData.get.aPlayerInfo.mNationData[mBattleRewardInfo.mNation].mNationExp;
		int lCurrentLevel = GameData.get.aPlayerInfo.mNationData[mBattleRewardInfo.mNation].mNationLevel;

		while ((lCurrentTotalExp != mBattleRewardInfo.mNationExp) && !mIsFlush)
		{
			yield return null;

			NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(lCurrentLevel);
	
			lCurrentTotalExp += (int)(vUpdateExpSpeed * Time.deltaTime);
			if (lCurrentTotalExp >= mBattleRewardInfo.mNationExp)
				lCurrentTotalExp = mBattleRewardInfo.mNationExp;

			int lNextLevelNeedExp = lNationLevelSpec.mMaxExp - lNationLevelSpec.mNeedExp;
			int lCurrentExp = lCurrentTotalExp - lNationLevelSpec.mNeedExp;
			float lExpRatio = 0.0f;
			if (lCurrentExp >= lNextLevelNeedExp)
			{
				lCurrentLevel++;
				vNationLevel.text = lCurrentLevel.ToString();
				lExpRatio = 1.0f;
				vLevelUpObject.SetActive(true);
			}
			else
			{
				lExpRatio = (float)lCurrentExp / lNextLevelNeedExp;
			}
			vNationExpProgressBar.value = lExpRatio;
		}
		_FlushExpNation();
	}
	//-------------------------------------------------------------------------------------------------------
	// 순간 적용
	private void _FlushExpNation()
	{
		int lCurrentTotalExp = mBattleRewardInfo.mNationExp;
		int lCurrentLevel = mBattleRewardInfo.mNationLevel;
		NationLevelSpec lNationLevelSpec = NationLevelTable.get.GetNationLevelSpec(lCurrentLevel);

		int lNextRankNeedExp = lNationLevelSpec.mMaxExp - lNationLevelSpec.mNeedExp;
		int lCurrentExp = lCurrentTotalExp - lNationLevelSpec.mNeedExp;

		float lExpRatio = (float)lCurrentExp / lNextRankNeedExp;

		if (GameData.get.aPlayerInfo.mNationData[mBattleRewardInfo.mNation].mNationLevel != mBattleRewardInfo.mNationLevel)
			vLevelUpObject.SetActive(true);

		vNationLevel.text = mBattleRewardInfo.mNationLevel.ToString();
		vNationExpProgressBar.value = lExpRatio;

		vPopup.SetNextStep();
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleRewardInfo mBattleRewardInfo;
	private bool mIsFlush;
}
