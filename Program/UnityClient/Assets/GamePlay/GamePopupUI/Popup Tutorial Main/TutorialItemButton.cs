﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TutorialItemButton : MonoBehaviour
{
	public enum TutorialButtonState
	{
		None,
		Active,
		Clear
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vCommanderIcon;
	public UILabel vCommanderName;
	public GameObject vStateActiveObject;
	public GameObject vClearObject;

	public BattleUITroopCountInfo[] vTroopKindCountInfo;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vCommanderIcon == null)
			Debug.LogError("vCommanderIcon is empty - " + this);
		if (vCommanderName == null)
			Debug.LogError("vCommanderName is empty - " + this);
		if (vStateActiveObject == null)
			Debug.LogError("vStateActiveObject is empty - " + this);
		if (vClearObject == null)
			Debug.LogError("vClearObject is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
//		mButtonState = TutorialButtonState.None;
		//SetState(mButtonState);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Commander Item Id를 전송받아 Commander를 셋팅합니다.
	public void SetCommander(int lTutorialIdx, CommanderItemData pCommanderItemData)
	{
		mCommanderItem = new CommanderItem(lTutorialIdx);
		mCommanderItem.Init(CommanderItemType.UserCommander, pCommanderItemData);

		vCommanderIcon.mainTexture = mCommanderItem.mCommanderSpec.GetIconTexture();
		vCommanderName.text = LocalizedTable.get.GetLocalizedText(mCommanderItem.mCommanderSpec.mNameKey);

		int lOpenIndex = mCommanderItem.mTroopSet.mTroopKindCount - 1;
		//BattleUITroopCountInfo lCurrentTroopKindCountInfo = null;
		for (int iKind = 0; iKind < CommanderTroopSet.cMaxTroopKindCount; iKind++)
		{
			if (iKind == lOpenIndex)
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(true);
				//lCurrentTroopKindCountInfo = vTroopKindCountInfo[iKind];
				vTroopKindCountInfo[iKind].SetTroopCountInfo(mCommanderItem.mTroopSet);
			}
			else
			{
				vTroopKindCountInfo[iKind].gameObject.SetActive(false);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 상태에 따른 버튼을 출력합니다.
	public void SetState(TutorialButtonState pState)
	{
//		mButtonState = pState;
		switch (pState)
		{
			case TutorialButtonState.None:
				vStateActiveObject.SetActive(false);
				vClearObject.SetActive(false);
				break;
			case TutorialButtonState.Active:
				vStateActiveObject.SetActive(true);
				vClearObject.SetActive(false);
				break;
			case TutorialButtonState.Clear:
				vStateActiveObject.SetActive(false);
				vClearObject.SetActive(true);
				break;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CommanderItem mCommanderItem;
//	private TutorialButtonState mButtonState;
}
