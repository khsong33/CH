﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EpisodeListItem : InfiniteItemBehavior
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public override void SetItemData(object pData)
	{
		mData = pData as EpisodeData;
		vLabel.text = String.Format("No.{0}", mData.mIndex);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private EpisodeData mData;
}
