﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CHScrollBar : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vForeGround;
	public UISprite vGuideBar;
	public bool vIsVertical = true;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
//		mOffsetPosition = 0.0f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 스크롤에 사용될 정보를 셋팅합니다.
	public void SetMaxItemCount(int pMaxItemCount, float pCellValue, float pPageSize, int pStartIndex)
	{
//		mOffsetPosition = pStartIndex * pCellValue;
		mMaxScrollSize = pMaxItemCount * pCellValue;
		float lScrollBarScaleFactor = mMaxScrollSize / pPageSize;
		if (lScrollBarScaleFactor <= 1.0f)
		{
			mIsOverSize = true;
			gameObject.SetActive(false);
		}
		else
		{
			mIsOverSize = false;
			gameObject.SetActive(true);
		}

		if(vIsVertical)
			vForeGround.localScale = new Vector3(1.0f, 1.0f/lScrollBarScaleFactor, 1.0f);
		else
			vForeGround.localScale = new Vector3(1.0f / lScrollBarScaleFactor, 1.0f, 1.0f);

		mMaxScrollMoveFactor = (float)(mMaxScrollSize - pPageSize) / mMaxScrollSize;
	}
	//-------------------------------------------------------------------------------------------------------
	// 위치를 갱신합니다.
	public void UpdatePosition(float pPositionValue)
	{
		if (mIsOverSize)
			return;

		//float lMoveFactor = (float)(pPositionValue + (mOffsetPosition / 2)) / mMaxScrollSize;
		float lMoveFactor = 0.0f;
		lMoveFactor = (float)(pPositionValue) / mMaxScrollSize;

		if (lMoveFactor < 0.0f)
			lMoveFactor = 0.0f;
		else if (lMoveFactor >= mMaxScrollMoveFactor)
			lMoveFactor = mMaxScrollMoveFactor;

		if (vIsVertical)
		{
			vForeGround.localPosition = new Vector3(vGuideBar.transform.localPosition.x,
													vGuideBar.transform.localPosition.y + ((float)(lMoveFactor * vGuideBar.height) * -1.0f),
													vGuideBar.transform.localPosition.z);
		}
		else
		{
			vForeGround.localPosition = new Vector3(vGuideBar.transform.localPosition.x + ((float)(lMoveFactor * vGuideBar.width)),
													vGuideBar.transform.localPosition.y,
													vGuideBar.transform.localPosition.z);
		}

	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private float mMaxScrollSize;
	private float mMaxScrollMoveFactor;
//	private float mOffsetPosition;
	private bool mIsOverSize;
}
