﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class AreaButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vAreaIconTexture;
	public UILabel vAreaLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vAreaIconTexture == null)
			Debug.LogError("vAreaIconTexture is empty - " + this);
		if (vAreaLabel == null)
			Debug.LogError("vAreaLabel is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		SetArea();
	}

	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 지역 정보를 출력합니다.
	public void SetArea()
	{
		if(GameData.get.aPlayerInfo == null)
			return;

		int lArea = GameData.get.aPlayerInfo.mArea;
		AreaSpec lAreaSpec = AreaTable.get.GetAreaSpec(lArea);
		if (lAreaSpec == null)
			return;

		vAreaIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lAreaSpec.mIconAssetKey);
		vAreaLabel.text = LocalizedTable.get.GetLocalizedText(lAreaSpec.mNameKey);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
