﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Very simple example of how to use a TextList with a UIInput for chat.
/// </summary>

[RequireComponent(typeof(UIInput))]
[AddComponentMenu("NGUI/Examples/Chat Input")]
public class ChattingInput : MonoBehaviour
{
    public UITextList textList;
    public bool fillWithDummyData = false;
    public CombatLobbyControl vLobbyControl;

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
        if (vLobbyControl == null)
            Debug.LogError("Lobby Control is empty");

        mInput = GetComponent<UIInput>();
        mInput.label.maxLineCount = 1;

        vLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.aOnChat += _OnChat;
    }

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // 확인
    public void OnSubmit()
    {
        if (textList != null)
        {
            // It's a good idea to strip out all symbols as we don't want user input to alter colors, add new lines, etc
            string text = NGUIText.StripSymbols(mInput.value);
            // 서버로 전송
            vLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.SendPacket_Chat(text);
            //_OnChatMessage("UserName", text); // 임시 처리
        }
    }
 
    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 채팅 메시지 콜백
    private void _OnChat(String pUserName, String pMessage)
    {
        if (!string.IsNullOrEmpty(pMessage))
        {
            String lCompleteMsg = String.Format("[{0}]:{1}", pUserName, pMessage);
            textList.Add(lCompleteMsg);
            mInput.value = "";
            mInput.isSelected = true;
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private UIInput mInput;
}
