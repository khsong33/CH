﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class FreeChestButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vIngObject;
	public UILabel vRemainTimeLabel;
	public GameObject vCompleteObject;
	public GameObject vCompleteIcon1Object;
	public GameObject vCompleteIcon2Object;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vIngObject == null)
			Debug.LogError("vIngObject is empty - " + this);
		if (vRemainTimeLabel == null)
			Debug.LogError("vRemainTimeLabel is empty - " + this);
		if (vCompleteObject == null)
			Debug.LogError("vCompleteObject is empty - " + this);
		if (vCompleteIcon1Object == null)
			Debug.LogError("vCompleteIcon1Object is empty - " + this);
		if (vCompleteIcon2Object == null)
			Debug.LogError("vCompleteIcon2Object is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenFreeChest += _OnOpenFreeChest;
		}
		SetFreeChest();
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenFreeChest -= _OnOpenFreeChest;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (!mIsActiveFreeChest)
		{
			vRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mFreeChestOpenTime);
			if (mFreeChestOpenTime <= TimeUtil.aSyncTime)
			{
				SetFreeChest();
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		if (mIsActiveFreeChest)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenFreeChest();
		}
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 무료 상자의 데이터를 설정합니다.
	public void SetFreeChest()
	{
		if (GameData.get.aPlayerInfo == null)
			return;

		mFreeChestOpenTime = GameData.get.aPlayerInfo.mChestUserData.mFreeChest0Time;
		mNextFreeChestOpenTime = GameData.get.aPlayerInfo.mChestUserData.mFreeChest1Time;
		// 상자 열기 가능
		if (mFreeChestOpenTime <= TimeUtil.aSyncTime)
		{
			vIngObject.SetActive(false);
			vCompleteObject.SetActive(true);
			vCompleteIcon1Object.SetActive(true);
			// 두번째 상자 열기 가능
			if (mNextFreeChestOpenTime <= TimeUtil.aSyncTime)
			{
				vCompleteIcon2Object.SetActive(true);
			}
			else
				vCompleteIcon2Object.SetActive(false);

			mIsActiveFreeChest = true;
		}
		else
		{
			vIngObject.SetActive(true);
			vRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mFreeChestOpenTime);
			vCompleteObject.SetActive(false);
			mIsActiveFreeChest = false;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 무료 상자 열기
	private void _OnOpenFreeChest(ChestOpenData pChestOpenData)
	{
		SetFreeChest();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsActiveFreeChest;
	private DateTime mFreeChestOpenTime;
	private DateTime mNextFreeChestOpenTime;
}
