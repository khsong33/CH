﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class MedalChestButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vBeforeObject;
	public UILabel vBeforeRemainTimeLabel;
	
	public GameObject vIngObject;
	public UILabel vMedalCountLabel;
	public UISlider vMedalProgressBar;
	
	public GameObject vCompleteObject;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vBeforeObject == null)
			Debug.LogError("vBeforeObject is empty - " + this);
		if (vBeforeRemainTimeLabel == null)
			Debug.LogError("vBeforeRemainTimeLabel is empty - " + this);

		if (vIngObject == null)
			Debug.LogError("vIngObject is empty - " + this);
		if (vMedalCountLabel == null)
			Debug.LogError("vMedalCountLabel is empty - " + this);
		if (vMedalProgressBar == null)
			Debug.LogError("vMedalProgressBar is empty - " + this);

		if (vCompleteObject == null)
			Debug.LogError("vCompleteObject is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenMedalChest += _OnOpenMedalChest;
		}
		SetMedalChest();
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenMedalChest -= _OnOpenMedalChest;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (!mIsOpenActiveMedalChest)
		{
			vBeforeRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mActiveTime);
			if (mActiveTime < TimeUtil.aSyncTime)
			{
				SetMedalChest();
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		if (mIsOpenActiveMedalChest)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenMedalChest();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 메달 상자의 데이터를 설정합니다.
	public void SetMedalChest()
	{
		if (GameData.get.aPlayerInfo == null)
			return;

		mActiveTime = GameData.get.aPlayerInfo.mChestUserData.mMedalTime;
		int lMaxCount = GameData.get.aPlayerInfo.mChestUserData.mMedalMaxCount;
		int lCurrentCount = GameData.get.aPlayerInfo.mChestUserData.mMedalCount;
		if (mActiveTime < TimeUtil.aSyncTime)
		{
			if (lCurrentCount >= lMaxCount)
			{
				vBeforeObject.SetActive(false);
				vIngObject.SetActive(false);
				vCompleteObject.SetActive(true);
				mIsOpenActiveMedalChest = true;
			}
			else
			{
				vBeforeObject.SetActive(false);
				vCompleteObject.SetActive(false);
				vIngObject.SetActive(true);
				vMedalCountLabel.text = String.Format("{0}/{1}", lCurrentCount, lMaxCount);
				vMedalProgressBar.value = (float)lCurrentCount / lMaxCount;
				mIsOpenActiveMedalChest = false;
			}
		}
		else
		{
			vIngObject.SetActive(false);
			vCompleteObject.SetActive(false);
			vBeforeObject.SetActive(true);
			vBeforeRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mActiveTime);
			mIsOpenActiveMedalChest = false;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 메달 상자 열기
	private void _OnOpenMedalChest(ChestOpenData pChestOpenData)
	{
		SetMedalChest();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsOpenActiveMedalChest;
	private DateTime mActiveTime;
}
