﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class NormalChestButton : MonoBehaviour
{
	public enum ButtonState
	{
		Empty, 
		AvailableLock, 
		DisableLock,
		ActiveTime,
		EnabledOpen
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vIconTexture;
	public GameObject vBeforeObject;
	public UILabel vBeforeAreaLabel;
	public UILabel vBeforeOpenTimeLabel;
	public GameObject vBeforeTapAvailableObject;
	public GameObject vBeforeTapDisableObject;
	public GameObject vIngObject;
	public UILabel vIngRemainTimeLabel;
	public UILabel vIngGemLabel;
	public GameObject vCompleteObject;
	public NormalChestUI vNormalChestRoot;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vIconTexture == null)
			Debug.LogError("vIconTexture is empty - " + this);
		if (vBeforeObject == null)
			Debug.LogError("vBeforeObject is empty - " + this);
		if (vBeforeOpenTimeLabel == null)
			Debug.LogError("vBeforeOpenTimeLabel is empty - " + this);
		if (vBeforeAreaLabel == null)
			Debug.LogError("vBeforeAreaLabel is empty - " + this);
		if (vBeforeTapAvailableObject == null)
			Debug.LogError("vBeforeTapAvailableObject is empty - " + this);
		if (vBeforeTapDisableObject == null)
			Debug.LogError("vBeforeTapDisableObject is empty - " + this);

		if (vIngObject == null)
			Debug.LogError("vIngObject is empty - " + this);
		if (vIngRemainTimeLabel == null)
			Debug.LogError("vIngRemainTimeLabel is empty - " + this);
		if (vIngGemLabel == null)
			Debug.LogError("vIngGemLabel is empty - " + this);

		if (vCompleteObject == null)
			Debug.LogError("vCompleteObject is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (mNormalChestButtonState == ButtonState.ActiveTime)
		{
			float lRemainSec = (float)(mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds;
			vIngGemLabel.text = Mathf.FloorToInt(lRemainSec / 600.0f).ToString();
			vIngRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mNormalChestData.mOpenTime);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 클릭 콜백
	void OnClick()
	{
		if (mNormalChestButtonState == ButtonState.Empty)
			return;

		if (mNormalChestButtonState == ButtonState.EnabledOpen)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenNormalChest(mNormalChestData.mSlotIdx);
		}
		else
		{
			vNormalChestRoot.OnOpenNormalChestPopup(mNormalChestData);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// NormalChestRoot를 셋팅합니다.
	public void SetNormalChestRoot(NormalChestUI pRoot)
	{
		vNormalChestRoot = pRoot;
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 상자 상태를 표시합니다.
	public void SetNormalChestData(int pSlotIdx)
	{
		mNormalChestData = GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[pSlotIdx];
		if (mNormalChestData == null)
			return;

		if (mNormalChestData.mChestType == ChestType.Empty)
		{
			vIconTexture.gameObject.SetActive(false);
			vBeforeObject.SetActive(false);
			vIngObject.SetActive(false);
			vCompleteObject.SetActive(false);
			mNormalChestButtonState = ButtonState.Empty;
		}
		else if (mNormalChestData.mNormalChestState == NormalChestState.Lock)
		{
			vIconTexture.gameObject.SetActive(true);
			ChestIconSpec lChestIconSpec = ShopChestTable.get.GetIconShopSpec(GameData.get.aPlayerInfo.mArea, mNormalChestData.mChestType);
			vIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lChestIconSpec.mIconAssetKey);
			vBeforeObject.SetActive(true);
			vBeforeAreaLabel.text = String.Format(LocalizedTable.get.GetLocalizedText("AreaName{0}"), mNormalChestData.mArea);
			String lOpenTimeKey = String.Format("RemainChest{0}", (int)mNormalChestData.mChestType);
			vBeforeOpenTimeLabel.text = LocalizedTable.get.GetLocalizedText(lOpenTimeKey);
			if (GameData.get.aPlayerInfo.mChestUserData.IsAvailableActiveChest())
			{
				vBeforeTapAvailableObject.SetActive(true);
				vBeforeTapDisableObject.SetActive(false);
				mNormalChestButtonState = ButtonState.AvailableLock;
			}
			else
			{
				vBeforeTapAvailableObject.SetActive(false);
				vBeforeTapDisableObject.SetActive(true);
				mNormalChestButtonState = ButtonState.DisableLock;
			}
			vIngObject.SetActive(false);
			vCompleteObject.SetActive(false);
		}
		else if (mNormalChestData.mNormalChestState == NormalChestState.Active)
		{
			vIconTexture.gameObject.SetActive(true);
			ChestIconSpec lChestIconSpec = ShopChestTable.get.GetIconShopSpec(GameData.get.aPlayerInfo.mArea, mNormalChestData.mChestType);
			vIconTexture.mainTexture = TextureResourceManager.get.LoadTexture(lChestIconSpec.mIconAssetKey);
			// 열 수 있는 시간이 되었다면
			if ((mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds < 0)
			{
				vIngObject.SetActive(false);
				vCompleteObject.SetActive(true);
				mNormalChestButtonState = ButtonState.EnabledOpen;
			}
			else
			{
				vIngObject.SetActive(true);
				vCompleteObject.SetActive(false);
				float lRemainSec = (float)(mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds;
				vIngGemLabel.text = Mathf.FloorToInt(lRemainSec / 600.0f).ToString();
				vIngRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mNormalChestData.mOpenTime);
				mNormalChestButtonState = ButtonState.ActiveTime;
			}
			vBeforeObject.SetActive(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private NormalChestData mNormalChestData;
	private ButtonState mNormalChestButtonState;
}
