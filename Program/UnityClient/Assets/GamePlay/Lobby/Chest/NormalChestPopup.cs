﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NormalChestPopup : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vChestTexture;
	public UILabel vChestAreaLabel;
	public UILabel vChestNameLabel;
	public UILabel vChestGoldLabel;
	public UILabel vChestTotalItemLabel;

	public NormalChestUI vRoot;

	public GameObject vRank2Object;
	public UILabel vRank2CountLabel;
	public GameObject vRank3Object;
	public UILabel vRank3CountLabel;

	public GameObject vUnlockObject;
	public UILabel vUnlockTimeLabel;

	public GameObject vPurchargeObject;
	public UILabel vPurchargePriceLabel;

	public GameObject vUnlockIngObject;
	public UILabel vUnlockRemainTimeLabel;

	public GameObject vOtherUnlockObject;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vChestTexture == null)
			Debug.LogError("vChestTexture is empty - " + this);
		if (vChestAreaLabel == null)
			Debug.LogError("vChestAreaLabel is empty - " + this);
		if (vChestNameLabel == null)
			Debug.LogError("vChestNameLabel is empty - " + this);
		if (vChestGoldLabel == null)
			Debug.LogError("vChestGoldLabel is empty - " + this);
		if (vChestTotalItemLabel == null)
			Debug.LogError("vChestTotalItemLabel is empty - " + this);
		if (vRank2Object == null)
			Debug.LogError("vRank2Object is empty - " + this);
		if (vRank2CountLabel == null)
			Debug.LogError("vRank2CountLabel is empty - " + this);
		if (vRank3Object == null)
			Debug.LogError("vRank3Object is empty - " + this);

		if (vRoot == null)
			Debug.LogError("vRoot is empty - " + this);

		if (vUnlockObject == null)
			Debug.LogError("vUnlockObject is empty - " + this);
		if (vUnlockTimeLabel == null)
			Debug.LogError("vUnlockTimeLabel is empty - " + this);
		
		if (vPurchargeObject == null)
			Debug.LogError("vPurchargeObject is empty - " + this);
		if (vPurchargePriceLabel == null)
			Debug.LogError("vPurchargePriceLabel is empty - " + this);
		
		if (vUnlockIngObject == null)
			Debug.LogError("vUnlockIngObject is empty - " + this);
		if (vUnlockRemainTimeLabel == null)
			Debug.LogError("vUnlockRemainTimeLabel is empty - " + this);
		
		if (vOtherUnlockObject == null)
			Debug.LogError("vOtherUnlockObject is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 업데이트 콜백
	void Update()
	{
		if (mNormalChestData.mNormalChestState == NormalChestState.Active)
		{
			if ((mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds < 0)
			{
				vRoot.ClosePopup();
			}
			else
			{
				vUnlockRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mNormalChestData.mOpenTime);
				float lRemainSec = (float)(mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds;
				vPurchargePriceLabel.text = Mathf.FloorToInt(lRemainSec / 600.0f).ToString();
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 상점을 셋팅합니다.
	public void SetPopupChest(NormalChestData pNormalChestData)
	{
		mNormalChestData = pNormalChestData;
		mChestIconSpec = ShopChestTable.get.GetIconShopSpec(mNormalChestData.mArea, mNormalChestData.mChestType);
		mChestSpec = ChestTable.get.GetChestSpec(mNormalChestData.mArea, mNormalChestData.mChestType);

		vChestTexture.mainTexture = TextureResourceManager.get.LoadTexture(mChestIconSpec.mIconAssetKey);
		vChestAreaLabel.text = String.Format("{0} {1}", LocalizedTable.get.GetLocalizedText("Area"), mNormalChestData.mArea);
		vChestNameLabel.text = LocalizedTable.get.GetLocalizedText(mChestIconSpec.mNameKey);
		vChestGoldLabel.text = String.Format("{0}-{1}", mChestSpec.mGoldMin, mChestSpec.mGoldMax);
		vChestTotalItemLabel.text = String.Format("X{0}", mChestSpec.mTotalCardCount);
		if (mChestSpec.mRank2CardCount <= 0)
		{
			vRank2Object.SetActive(false);
		}
		else
		{
			vRank2Object.SetActive(true);
			vRank2CountLabel.text = String.Format("X{0}", mChestSpec.mRank2CardCount);
		}
		if (mChestSpec.mRank3CardCount <= 0)
		{
			vRank3Object.SetActive(false);
		}
		else
		{
			vRank3Object.SetActive(true);
			vRank3CountLabel.text = String.Format("X{0}", mChestSpec.mRank3CardCount);
		}

		_SetButtonState();
	}
	//-------------------------------------------------------------------------------------------------------
	// 언락 버튼 클릭
	public void GoUnlockButton()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ActiveNormalChest(mNormalChestData.mSlotIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구매 버튼 클릭
	public void GoPurchargeButton()
	{
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenNormalChest(mNormalChestData.mSlotIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 버튼 상태를 설정합니다.
	private void _SetButtonState()
	{
		if (mNormalChestData.mNormalChestState == NormalChestState.Lock)
		{
			// 상자 활성화가 가능한 경우
			if (GameData.get.aPlayerInfo.mChestUserData.IsAvailableActiveChest())
			{
				vUnlockObject.SetActive(true);
				String lOpenTimeKey = String.Format("RemainChest{0}", (int)mNormalChestData.mChestType);
				vUnlockTimeLabel.text = LocalizedTable.get.GetLocalizedText(lOpenTimeKey);
				vUnlockIngObject.SetActive(false);
				vPurchargeObject.SetActive(false);
				vOtherUnlockObject.SetActive(false);
			}
			else
			{
				vUnlockObject.SetActive(false);
				
				vPurchargeObject.SetActive(true);
				int lRemainSec = GameData.get.aPlayerInfo.mChestUserData.GetRemainSecChestType(mNormalChestData.mChestType);
				vPurchargePriceLabel.text = Mathf.FloorToInt(lRemainSec / 600.0f).ToString();
				vUnlockIngObject.SetActive(false);
				vOtherUnlockObject.SetActive(true);
			}
		}
		else if (mNormalChestData.mNormalChestState == NormalChestState.Active)
		{
			vUnlockObject.SetActive(false);
			
			vUnlockIngObject.SetActive(true);
			vUnlockRemainTimeLabel.text = GameData.get.GetRemainTimeStr(mNormalChestData.mOpenTime);
			vPurchargeObject.SetActive(true);
			float lRemainSec = (float)(mNormalChestData.mOpenTime - TimeUtil.aSyncTime).TotalSeconds;
			vPurchargePriceLabel.text = Mathf.FloorToInt(lRemainSec / 600.0f).ToString();

			vOtherUnlockObject.SetActive(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private NormalChestData mNormalChestData;
	private ChestIconSpec mChestIconSpec;
	private ChestSpec mChestSpec;
}
