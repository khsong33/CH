﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public class NormalChestUI : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public List<NormalChestButton> vNormalChestList;
	public NormalChestPopup vNormalChestPopup;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vNormalChestPopup == null)
			Debug.LogError("vNormalChestPopup is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		for (int iSlot = 0; iSlot < vNormalChestList.Count; iSlot++)
		{
			vNormalChestList[iSlot].SetNormalChestRoot(this);
			vNormalChestList[iSlot].SetNormalChestData(iSlot);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenNormalChest += _OnOpenNormalChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnActiveNormalChest += _OnActiveNormalChest;
		}
		vNormalChestPopup.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		if (GameData.get.aLobbyControl != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenNormalChest -= _OnOpenNormalChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnActiveNormalChest -= _OnActiveNormalChest;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 버튼 클릭시 팝업창을 사용합니다.
	public void OnOpenNormalChestPopup(NormalChestData pNormalChestData)
	{
		vNormalChestPopup.gameObject.SetActive(true);
		vNormalChestPopup.SetPopupChest(pNormalChestData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 버튼 클릭시 팝업창을 사용합니다.
	public void ClosePopup()
	{
		vNormalChestPopup.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 일반 상자를 열었을 경우 갱신
	private void _OnOpenNormalChest(int pSlotIdx, ChestOpenData pChestOpenData)
	{
		ClosePopup();
		for (int iSlot = 0; iSlot < vNormalChestList.Count; iSlot++)
		{
			vNormalChestList[iSlot].SetNormalChestData(iSlot);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 상자를 활성화 경우 갱신
	private void _OnActiveNormalChest(int pActiveSlotIdx)
	{
		ClosePopup();
		for (int iSlot = 0; iSlot < vNormalChestList.Count; iSlot++)
		{
			vNormalChestList[iSlot].SetNormalChestData(iSlot);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
