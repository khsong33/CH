﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CombatGoodsInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vGoldLabel;
	public UILabel vGemLabel;


	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if(vGoldLabel == null)
			Debug.LogError("vGoldLabel is empty - " + this);
		if (vGemLabel == null)
			Debug.LogError("vGemLabel is empty - " + this);

		SetGoodsInfo();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보를 입력합니다.
	public void SetGoodsInfo()
	{
		if (GameData.get.aPlayerInfo != null)
		{
			vGoldLabel.text = GameData.get.aPlayerInfo.mGold.ToString();
			vGemLabel.text = GameData.get.aPlayerInfo.mCash.ToString();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
