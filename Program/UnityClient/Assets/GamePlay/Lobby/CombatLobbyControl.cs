using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CombatLobbyControl : MonoBehaviour
{
	public enum SceneLevel
	{
		Title = 0,
		Lobby = 1,
		Battle = 2,
	}
	public delegate void OnWaitPvPGameState(bool pActive);
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
    public LobbyNetwork vNetworkLobby;

    //-------------------------------------------------------------------------------------------------------
	// 속성
    //-------------------------------------------------------------------------------------------------------
	public OnWaitPvPGameState aOnWaitPvPGameState { get; set; }

    public bool aIsLogin { get; set; }
    public bool aIsWaitGame 
    {
		set 
		{ 
			mIsWaitingGame = value;
			if (aOnWaitPvPGameState != null)
				aOnWaitPvPGameState(mIsWaitingGame);
		}
        get { return mIsWaitingGame; } 
    }
	public bool aIsConnectedServer{	get { return mIsConnectServer; }}
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
    // 시작 전 콜백
    void Awake()
    {
		if (GameData.get.aLobbyControl == null)
		{
			GameData.get.aLobbyControl = this;

			if (vNetworkLobby == null)
				Debug.LogError("vNetworkLobby is Empty");

			vNetworkLobby.aILobbyProtocol.InitNetwork();

			vNetworkLobby.aILobbyProtocol.aOnConnectLobbyServer += _OnConnectLobbyServer;
			vNetworkLobby.aILobbyProtocol.aOnRecvLogin += _OnLogin;
			vNetworkLobby.aILobbyProtocol.aOnRecvLoginCommanderItem += _OnLoginCommanderItem;
			vNetworkLobby.aILobbyProtocol.aOnRecvLoginDeck += _OnLoginDeck;
			vNetworkLobby.aILobbyProtocol.aOnShopList += _OnShopList;
			vNetworkLobby.aILobbyProtocol.aOnChestList += _OnChestList;
			vNetworkLobby.aILobbyProtocol.aOnWaitRegiste += _OnWaitRegiste;
			vNetworkLobby.aILobbyProtocol.aOnWaitUnRegiste += _OnWaitUnRegiste;

			DontDestroyOnLoad(this.gameObject);
		}
		else
		{
			DestroyObject(this.gameObject);
		}
    }
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//PlayerPrefs.DeleteKey("AutoLogin");
		GamePopupUIManager.aInstance.OpenPopup("Popup Logo");

		vNetworkLobby.aILobbyProtocol.ConnectNetwork();
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로비 컨트롤 초기화
	public void ResetLobbyControl()
	{
		aIsWaitGame = false;
	}
	//-------------------------------------------------------------------------------------------------------
    // 로그인 클릭시
    public void GoLogin(String pUserName)
    {
        vNetworkLobby.aILobbyProtocol.SendPacket_Login(pUserName);
    }
    //-------------------------------------------------------------------------------------------------------
    // 대기 큐 찾기 클릭시
    public void GoWaitRegiste(int pDeckId)
    {
		if (!aIsWaitGame)
        {
			vNetworkLobby.aILobbyProtocol.SendPacket_WaitRegiste(pDeckId);
        }
		//else
		//{
		//	vNetworkLobby.aILobbyProtocol.SendPacket_WaitUnRegiste();
		//}
    }
	//-------------------------------------------------------------------------------------------------------
	// 로고 종료시 처리
	public void OnEndLogo()
	{
		StartCoroutine(_GoLoginPage());
	}
	private IEnumerator _GoLoginPage()
	{
		while (!aIsConnectedServer)
		{
			yield return null;
		}

		//Debug.Log("Try Login!!");

		if (!GameData.get.aAutoLogin)
		{
			GamePopupLogin lPopup = (GamePopupLogin)GamePopupUIManager.aInstance.OpenPopup("Popup Login");
			if (lPopup != null)
			{
				lPopup.SetLobbyControl(GameData.get.aLobbyControl);
			}
		}
		else
		{
			vNetworkLobby.aILobbyProtocol.SendPacket_Login(PlayerPrefs.GetString("LoginName"));
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
	//-------------------------------------------------------------------------------------------------------
	// 서버 접속 콜백
	private void _OnConnectLobbyServer()
	{
		//Debug.Log("Connected Lobby Server!!!!");
		mIsConnectServer = true;
	}
	//-------------------------------------------------------------------------------------------------------
    // 로그인 콜백
    private void _OnLogin(GameUserInfo pPlayerInfo)
    {
        // 플레이어 정보를 이곳에서 저장
		GameData.get.aPlayerInfo = pPlayerInfo;

		if (GamePopupUIManager.aInstance.aCurrentPopup != null)
			GamePopupUIManager.aInstance.ClosePopup();

		if(GameData.get.aIsLoginSuccess)
			GameData.get.EnterLobbyPhase();

		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ReqShopList();
		GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ReqChestList();
    }
	//-------------------------------------------------------------------------------------------------------
	// 로그인 지휘관 아이템 콜백
	private void _OnLoginCommanderItem(List<CommanderItemData> pCommanderItemDataList)
	{
		GameData.get.aPlayerInfo.SetCommanderItemList(pCommanderItemDataList);
		if (GameData.get.aIsLoginSuccess)
			GameData.get.EnterLobbyPhase();
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 지휘관 덱 콜백
	private void _OnLoginDeck(List<DeckData> pDeckList)
	{
		GameData.get.aPlayerInfo.SetDeck(pDeckList);
		if (GameData.get.aIsLoginSuccess)
			GameData.get.EnterLobbyPhase();
	}
	//-------------------------------------------------------------------------------------------------------
	// 상점 정보
	private void _OnShopList()
	{
		if (GameData.get.aIsLoginSuccess)
			GameData.get.EnterLobbyPhase();
	}
	//-------------------------------------------------------------------------------------------------------
	// 보상상자 리스트
	private void _OnChestList()
	{
		if (GameData.get.aIsLoginSuccess)
			GameData.get.EnterLobbyPhase();
	}
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보 획득
	private void _OnUserInfo(GameUserInfo pPlayerInfo)
	{
		GameData.get.aPlayerInfo = pPlayerInfo;
	}
    //-------------------------------------------------------------------------------------------------------
    // PVP 대기 큐 등록
    private void _OnWaitRegiste()
	{
		aIsWaitGame = true;
		GamePopupUIManager.aInstance.OpenPopup("Popup Matching");
	}
    //-------------------------------------------------------------------------------------------------------
	// PVP 대기 큐 등록 해제
	private void _OnWaitUnRegiste()
	{
		aIsWaitGame = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 대기방 접속 승인
	private void _OnRoomJoinAccept(int pTotalUserCount, int pJoinUserCount)
    {
        if(pTotalUserCount == pJoinUserCount)
			aIsWaitGame = false;
    }
    //-------------------------------------------------------------------------------------------------------
    // 대기방 접속 취소
    private void _OnRoomJoinCancel(int pType)
    {
        if (pType == 0) // 본인 취소시
			aIsWaitGame = false;
        else // 타인이 취소시
			aIsWaitGame = true;
    }
	//-------------------------------------------------------------------------------------------------------
	// 전투 결과 처리
	//private void _OnBattleReward(BattleRewardInfo pRewardInfo)
	//{
	//	GameData.get.aPlayerInfo.mNationExps[(int)pRewardInfo.mNation] = pRewardInfo.mNationExp;
	//	GameData.get.aPlayerInfo.mNationLevels[(int)pRewardInfo.mNation] = pRewardInfo.mNationLevel; 

	//	for (int iCommanderItem = 0; iCommanderItem < pRewardInfo.mCommanderItemExps.Count; iCommanderItem++)
	//	{
	//		GameData.get.aPlayerInfo.UpdateCommanderItemExp(
	//			pRewardInfo.mCommanderItemExps[iCommanderItem].mItemIdx, 
	//			pRewardInfo.mCommanderItemExps[iCommanderItem].mRank,
	//			pRewardInfo.mCommanderItemExps[iCommanderItem].mExp);
	//	}
	//}

    //-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
    private bool mIsWaitingGame = false;
	private bool mIsConnectServer = false;
}