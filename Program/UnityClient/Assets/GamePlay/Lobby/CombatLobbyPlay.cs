﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityStandardAssets.ImageEffects;
using CombatJson;

public class CombatLobbyPlay : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vLobbyContents;
	public CombatGoodsInfo vGoodsContents;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vLobbyContents == null)
			Debug.LogError("vLobbyContents is empty - " + this);
		if (vGoodsContents == null)
			Debug.LogError("vGoodsContents is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mSleepTimeoutSave = Screen.sleepTimeout;
		Screen.sleepTimeout = SleepTimeout.NeverSleep;
		mIsNetworkChecker = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		GamePopupUIManager.aInstance.aOnClosedPopup += _OnClosedPopup; 
		vLobbyContents.SetActive(true);

		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aIConnectionProtocol.aOnErrorNetworkDelegate += _OnErrorNetwork;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnGameServerError += _OnGameServerError;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnStartBattleRoom += _OnStartBattleRoom;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp += _OnCommanderItemLevelUp;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenChest += _OnOpenChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGold += _OnShopBuyGold;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGem += _OnShopBuyGem;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander += _OnShopBuyCommander;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenNormalChest += _OnOpenNormalChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenFreeChest += _OnOpenFreeChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenMedalChest += _OnOpenMedalChest;

			GameData.get.aLobbyControl.ResetLobbyControl();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		GamePopupUIManager.aInstance.aOnClosedPopup -= _OnClosedPopup;
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aIConnectionProtocol.aOnErrorNetworkDelegate -= _OnErrorNetwork;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnGameServerError -= _OnGameServerError;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnStartBattleRoom -= _OnStartBattleRoom;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnCommanderItemLevelUp -= _OnCommanderItemLevelUp;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenChest -= _OnOpenChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGold -= _OnShopBuyGold;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyGem -= _OnShopBuyGem;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnShopBuyCommander -= _OnShopBuyCommander;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenNormalChest -= _OnOpenNormalChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenFreeChest -= _OnOpenFreeChest;
			GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.aOnOpenMedalChest -= _OnOpenMedalChest;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		// UI 입력을 처리합니다.
		if (Input.GetKeyDown(KeyCode.Backspace) ||
			Input.GetKeyDown(KeyCode.Escape))
		{
			if (GameData.get.aIsActiveCommanderPanel)
			{
				GameData.get.SetActiveCommanderInfoPanel(false, null, null);
			}
			else if (GamePopupUIManager.aInstance.aCurrentPopup != null)
			{
				GamePopupUIManager.aInstance.BackPopup();
			}
			else
			{
				Debug.LogWarning("Application.Quit();");
				Application.Quit();
			}
		}
		if (!mIsNetworkChecker)
		{
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_CONNECTION_SERVER"), MessageBoxManager.MessageBoxType.OK, _OnNetworkErrorConfirm);
			mIsNetworkChecker = true;
			GameData.get.aLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.vSocket.OnDestroy();
		}
		TimeUtil.Update();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestroy()
	{
		Screen.sleepTimeout = mSleepTimeoutSave;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// PVP 큐 등록
	public void GoPVP()
	{
		//GamePopupUIManager.aInstance.OpenPopup("Popup PVP Main");
		//vLobbyContents.SetActive(false);
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_WaitRegiste(GameData.get.aPlayerInfo.mUseDeckId);
		GameData.get.aLobbyControl.GoWaitRegiste(GameData.get.aPlayerInfo.mUseDeckId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 셋팅
	public void GoSetCommander()
	{
		//GamePopupUIManager.aInstance.OpenPopup("Popup My Army");
		GamePopupUIManager.aInstance.OpenPopup("Popup Battle Group");
		vLobbyContents.SetActive(false);
		//GamePopupCommandItem lPopup = (GamePopupCommandItem)GamePopupUIManager.aInstance.OpenPopup("Popup Command Item");
		//lPopup.SetFullBackground();
	}
	//-------------------------------------------------------------------------------------------------------
	// 리플레이 리스트
	public void GoReplayList()
	{
//		GamePopupReplayList lPopup = (GamePopupReplayList)GamePopupUIManager.aInstance.OpenPopup("Popup Replay List");
		GamePopupUIManager.aInstance.OpenPopup("Popup Replay List");
		// lPopup.SetFullBackground();
	}
	//-------------------------------------------------------------------------------------------------------
	// 옵션
	public void GoOption()
	{
		GamePopupUIManager.aInstance.OpenPopup("Popup Option");
	}
	//-------------------------------------------------------------------------------------------------------
	// 상점
	public void GoShop()
	{
		GamePopupUIManager.aInstance.OpenPopup("Popup Shop Main");
		vLobbyContents.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 훈련 열기
	public void GoCampaign()
	{
		GamePopupUIManager.aInstance.OpenPopup("Popup Tutorial Main");
		vLobbyContents.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 공지 열기
	public void GoNotice()
	{
		GamePopupUIManager.aInstance.OpenPopup("Popup Notice");
	}
	//-------------------------------------------------------------------------------------------------------
	// 지역 정보 열기
	public void GoAreaInfo()
	{
		GamePopupAreaInfo lPopup = (GamePopupAreaInfo)GamePopupUIManager.aInstance.OpenPopup("Popup AreaInfo");
		lPopup.SetAreaInfo();
	}

	//-------------------------------------------------------------------------------------------------------
	// 테스트
	public void GoTestOpen()
	{
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_ActiveNormalChest(0);
		//GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenNormalChest(0);
        //GameData.get.aLobbyControl.vNetworkLobby.aILobbyProtocol.SendPacket_OpenFreeChest();
        //GamePopupUIManager.aInstance.OpenPopup("Popup Battle Group");
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 팝업이 닫힐 때 콜백
	private void _OnClosedPopup()
	{
		if(!GamePopupUIManager.aInstance.IsActivePopup())
			vLobbyContents.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비에서 방 입장
	private void _OnStartBattleRoom(String pBattleServ, JSONObject pBattleMatchInfoJson)
	{
		GameData.get.EnterBattlePhase(pBattleServ, pBattleMatchInfoJson, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 레벨업
	private void _OnCommanderItemLevelUp(CommanderItemData pCommanderItemData)
	{
		vGoodsContents.SetGoodsInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 열기
	private void _OnOpenChest(ChestOpenData pChestOpenData)
	{
		vGoodsContents.SetGoodsInfo();
		GamePopupChestOpen lPopup = (GamePopupChestOpen)GamePopupUIManager.aInstance.OpenPopup("Popup Chest Opening");
		lPopup.SetChestOpenSpec(pChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 골드 구매
	private void _OnShopBuyGold(int pGainGold)
	{
		vGoodsContents.SetGoodsInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 캐쉬(잼) 구매
	private void _OnShopBuyGem(int pGainGem)
	{
		vGoodsContents.SetGoodsInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 구매
	private void _OnShopBuyCommander(int pUpdateShopIdx)
	{
		vGoodsContents.SetGoodsInfo();
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 보상 상자 열기
	private void _OnOpenNormalChest(int pNormalSlotIdx, ChestOpenData pChestOpenData)
	{
		vGoodsContents.SetGoodsInfo();
		GamePopupChestOpen lPopup = (GamePopupChestOpen)GamePopupUIManager.aInstance.OpenPopup("Popup Chest Opening");
		lPopup.SetChestOpenSpec(pChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 무료 상자 열기
	private void _OnOpenFreeChest(ChestOpenData pChestOpenData)
	{
		vGoodsContents.SetGoodsInfo();
		GamePopupChestOpen lPopup = (GamePopupChestOpen)GamePopupUIManager.aInstance.OpenPopup("Popup Chest Opening");
		lPopup.SetChestOpenSpec(pChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메달 상자 열기
	private void _OnOpenMedalChest(ChestOpenData pChestOpenData)
	{
		vGoodsContents.SetGoodsInfo();
		GamePopupChestOpen lPopup = (GamePopupChestOpen)GamePopupUIManager.aInstance.OpenPopup("Popup Chest Opening");
		lPopup.SetChestOpenSpec(pChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 네트워크 에러
	private void _OnErrorNetwork()
	{
		mErrorCount++;
		if (mErrorCount > 3)
		{
			mIsNetworkChecker = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 관련 서버 에러
	private void _OnGameServerError(NetworkErrorCode lNetworkErrorCode)
	{
		MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText(lNetworkErrorCode.ToString()), MessageBoxManager.MessageBoxType.OK);
	}
	//-------------------------------------------------------------------------------------------------------
	// 네트워크 에러 확인 버튼 클릭
	private void _OnNetworkErrorConfirm()
	{
		GameData.get.EnterTitlePhase();
		// 로그인을 초기화합니다.
		for (int iState = 0; iState < (int)LoginState.Count; iState++)
		{
			GameData.get.aLoginState[iState] = false;
		}

		GameData.get.aLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.InitNetwork();
		GameData.get.aLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.ConnectNetwork();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mSleepTimeoutSave;
	private int mErrorCount;
	private bool mIsNetworkChecker;
}
