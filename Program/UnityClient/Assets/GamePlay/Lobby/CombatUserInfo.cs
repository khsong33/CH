﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CombatUserInfo : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vUserNameLabel;
	public UILabel vUserLevelLabel;
	public UILabel vWinLabel;
	public UILabel vLoseLabel;
	public UILabel vWinStreakLabel;
	public UILabel vRatingLabel;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		SetUserInfo(GameData.get.aPlayerInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 유저 정보를 입력합니다.
	public void SetUserInfo(GameUserInfo pPlayerInfo)
	{
		vUserNameLabel.text = (pPlayerInfo != null) ? pPlayerInfo.mUserName : vUserNameLabel.text;
		vUserLevelLabel.text = String.Format("Lv {0}", (pPlayerInfo != null) ? pPlayerInfo.mLevel : 0);
		vWinLabel.text = (pPlayerInfo != null) ? pPlayerInfo.mWin.ToString() : "0";
		vLoseLabel.text = (pPlayerInfo != null) ? pPlayerInfo.mLose.ToString() : "0";
		vWinStreakLabel.text = (pPlayerInfo != null) ? pPlayerInfo.mWinStreak.ToString() : "0";
		vRatingLabel.text = (pPlayerInfo != null) ? pPlayerInfo.mRating.ToString() : "0"; ;
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
