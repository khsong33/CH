using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public enum NetworkErrorCode
{
	NOT_ENOUGH_GOLD = 1,
	NOT_ENOUGH_CASH,
	FAILED_ACCESS_CHEST
}
public class LobbyNetwork : MonoBehaviour
{
	public delegate void OnConnectLobbyServer();
	public delegate void OnRecvLogin(GameUserInfo pPlayerInfo);
	public delegate void OnRecvLoginCommanderItem(List<CommanderItemData> pCommanderItemDataList);
	public delegate void OnRecvLoginDeck(List<DeckData> pDeckList);
	public delegate void OnUserInfo(GameUserInfo pPlayerInfo);
	public delegate void OnWaitRegiste();
	public delegate void OnWaitUnRegiste();
	public delegate void OnMatchRoomInfo(List<GameUserInfo> pUserInfoList);
    public delegate void OnRoomJoinAcceptRequest();
    public delegate void OnRoomJoinAccept(int pTotalUserCount, int pJoinUserCount);
    public delegate void OnRoomJoinCancel(int pType); // Type (0) : 본인이 취소, (1) : 타인이 취소
	public delegate void OnRoomReady(int pReadyUniqueId);
	public delegate void OnRoomReadyCancel(int pReadyUniqueId);
	public delegate void OnRoomStart(String pBattleURL);
	public delegate void OnCommanderItemDeckUpdate();
	public delegate void OnAddCommanderItemDeck(int pNewDeckId);
	public delegate void OnRemoveCommanderItemDeck(int pRemoveDeckId);
	public delegate void OnReplayList(List<ReplayData> pReplayList);
	public delegate void OnReplayData(BattleVerifyData pVerifyData);
	public delegate void OnBattleReward(BattleRewardInfo pRewardInfo); // lagacy
	public delegate void OnBattleReward2(BattleResultData pBattleResultData);
	public delegate void OnStartTutorial(int pStartStageNo);
	public delegate void OnStartTutorial2(String pBattleServ, JSONObject pBattleMatchInfoJSON);
	public delegate void OnEndTutorial(int pUpdateTutorialIdx);
	public delegate void OnCommanderItemLevelUp(CommanderItemData pCommanderItemData);
	public delegate void OnStartBattleRoom(String pBattleServ, JSONObject pBattleMatchInfoJSON);
	public delegate void OnDevSetBattleReward();
	public delegate void OnShopList();
	public delegate void OnOpenChest(ChestOpenData pChestOpenSpec);
	public delegate void OnShopBuyGold(int pGainGold);
	public delegate void OnShopBuyGem(int pGainGem);
	public delegate void OnShopBuyCommander(int pUpdateShopIndex);
	public delegate void OnChestList();
	public delegate void OnActiveNormalChest(int pActiveSlotIdx);
	public delegate void OnOpenNormalChest(int pOpenSlotIdx, ChestOpenData pChestOpenData);
	public delegate void OnOpenFreeChest(ChestOpenData pChestOpenData);
	public delegate void OnOpenMedalChest(ChestOpenData pChestOpenData);
	public delegate void OnGameServerError(NetworkErrorCode pErrorCode);
	public delegate void OnChat(String pUserName, String pMessage);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public LobbyProtocolNodeJS vLobbyProtocolNodeJS;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 접속 프로토콜
	public IConnectionProtocol aIConnectionProtocol
	{
		get { return vLobbyProtocolNodeJS; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 로비 프로토콜
	public LobbyProtocolNodeJS aILobbyProtocol // 원래는 ILobbyProtocol aILobbyProtocol
	{
		get { return vLobbyProtocolNodeJS; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vLobbyProtocolNodeJS == null)
			Debug.LogError("vLobbyProtocolNodeJS is empty - " + this);

	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
