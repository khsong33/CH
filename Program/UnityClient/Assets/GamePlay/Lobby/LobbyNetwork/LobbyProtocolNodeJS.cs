using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using SocketIO;
using CombatJson;
using System.Globalization;

public class LobbyProtocolNodeJS : ConnectionProtocolNodeJS, ILobbyProtocol
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public LobbyNetwork.OnConnectLobbyServer aOnConnectLobbyServer { get; set; }
	public LobbyNetwork.OnRecvLogin aOnRecvLogin { get; set; }
	public LobbyNetwork.OnRecvLoginCommanderItem aOnRecvLoginCommanderItem { get; set; }
	public LobbyNetwork.OnRecvLoginDeck aOnRecvLoginDeck { get; set; }
	public LobbyNetwork.OnWaitRegiste aOnWaitRegiste { get; set; }
	public LobbyNetwork.OnWaitUnRegiste aOnWaitUnRegiste { get; set; }
	public LobbyNetwork.OnCommanderItemDeckUpdate aOnCommanderItemDeckUpdate { get; set; }
	public LobbyNetwork.OnAddCommanderItemDeck aOnAddCommanderItemDeck { get; set; }
	public LobbyNetwork.OnRemoveCommanderItemDeck aOnRemoveCommanderItemDeck { get; set; }
	public LobbyNetwork.OnReplayList aOnReplayList { get; set; }
	public LobbyNetwork.OnReplayData aOnReplayData { get; set; }
	public LobbyNetwork.OnBattleReward aOnBattleReward { get; set; }
	public LobbyNetwork.OnBattleReward2 aOnBattleReward2 { get; set; }
	public LobbyNetwork.OnStartTutorial2 aOnStartTutorial2 { get; set; }
	public LobbyNetwork.OnEndTutorial aOnEndTutorial { get; set; }
	public LobbyNetwork.OnCommanderItemLevelUp aOnCommanderItemLevelUp { get; set; }
	public LobbyNetwork.OnStartBattleRoom aOnStartBattleRoom { get; set; }
	public LobbyNetwork.OnDevSetBattleReward aOnDevSetBattleReward { get; set; }
	public LobbyNetwork.OnShopList aOnShopList { get; set; }
	public LobbyNetwork.OnOpenChest aOnOpenChest { get; set; }
	public LobbyNetwork.OnShopBuyGold aOnShopBuyGold { get; set; }
	public LobbyNetwork.OnShopBuyGem aOnShopBuyGem { get; set; }
	public LobbyNetwork.OnShopBuyCommander aOnShopBuyCommander { get; set; }
	public LobbyNetwork.OnChestList aOnChestList { get; set; }
	public LobbyNetwork.OnActiveNormalChest aOnActiveNormalChest { get; set; }
	public LobbyNetwork.OnOpenNormalChest aOnOpenNormalChest { get; set; }
	public LobbyNetwork.OnOpenFreeChest aOnOpenFreeChest { get; set; }
	public LobbyNetwork.OnOpenMedalChest aOnOpenMedalChest { get; set; }
	public LobbyNetwork.OnGameServerError aOnGameServerError { get; set; }
	public LobbyNetwork.OnChat aOnChat { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Start()
	{
		//InitNetwork();
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	protected override void InitURL()
	{
		vSocket.url = vURL[(int)vNetworkState];
	}
	protected override void RegistSocketEvent()
	{
		base.RegistSocketEvent();

		vSocket.On("sc_login", _OnLoginUser);
		vSocket.On("sc_login_commanderitem", _OnLoginCommanderItem);
		vSocket.On("sc_login_deck", _OnLoginDeck);
		vSocket.On("sc_wait_registe", _OnWaitRegiste);
		vSocket.On("sc_wait_unregiste", _OnWaitUnRegiste);
		vSocket.On("sc_update_commanditemdeck", _OnCommanderItemDeck);
		vSocket.On("sc_remove_commanditemdeck", _OnRemoveCommanderItemDeck);
		vSocket.On("sc_add_commanditemdeck", _OnAddCommanderItemDeck);
		vSocket.On("sc_replaylist", _OnReplayList);
		vSocket.On("sc_replaydata", _OnReplayData);
		vSocket.On("sc_battle_reward", _OnBattleReward2);
		vSocket.On("sc_tutorial_start2", _OnStartTutorial2);
		vSocket.On("sc_tutorial_end", _OnEndTutorial);
		vSocket.On("sc_start_room", _OnStartRoom);
		vSocket.On("sc_commander_levelup", _OnCommanderLevelUp);
		vSocket.On("sc_dev_battle_reward", _OnDevSetBattleReward);
		vSocket.On("sc_shop_list", _OnShopList);
		vSocket.On("sc_open_chest", _OnOpenChest);
		vSocket.On("sc_shop_chest_buy", _OnOpenChest);
		vSocket.On("sc_shop_gold_buy", _OnShopBuyGold);
		vSocket.On("sc_shop_buy_cash", _OnShopBuyGem);
		vSocket.On("sc_shop_commander_buy", _OnShopBuyCommander);
		vSocket.On("sc_chest_list", _OnChestList);
		vSocket.On("sc_active_normal_chest", _OnActiveChest);
		vSocket.On("sc_open_normal_chest", _OnOpenNormalChest);
		vSocket.On("sc_open_free_chest", _OnOpenFreeChest);
		vSocket.On("sc_open_medal_chest", _OnOpenMedalChest);
		vSocket.On("sc_server_error", _OnError);
		vSocket.On("sc_chat", _OnChat);

		aOnConnectNetworkDelegate += OnConnectNetwork;
		aOnCloseNetworkDelegate += OnCloseNetwork;
	}

	//-------------------------------------------------------------------------------------------------------
	// 프로토콜
	//-------------------------------------------------------------------------------------------------------
	// OnConnectNetworkDelegate
	public void OnConnectNetwork()
	{
		//mIsConnected = true;
		//SendPacket_Login(mLoginUserName);
		if (aOnConnectLobbyServer != null)
			aOnConnectLobbyServer();
	}
	//-------------------------------------------------------------------------------------------------------
	// OnCloseNetworkDelegate
	public void OnCloseNetwork()
	{
		//mIsConnected = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 요청
	public void SendPacket_Login(String pUserName)
	{
		JSONObject lLoginJson = new JSONObject();
		lLoginJson.AddField("Name", pUserName);
		lLoginJson.AddField("UserKey", PlayerPrefs.GetString("UserKey"));

		vSocket.Emit("cs_login", lLoginJson);
		StartCoroutine(_RetryLogin(lLoginJson));
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 재요청
	private IEnumerator _RetryLogin(JSONObject pLoginJson)
	{
		yield return new WaitForSeconds(5.0f);

		if (!GameData.get.aIsLoginSuccess)
		{
			SendPacket_Logout();
			yield return null;

			vSocket.OnDestroy();
			yield return null;

			ConnectNetwork();
			vSocket.Emit("cs_retry_login", pLoginJson);
		}		
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 요청 정보 받기
	private void _OnLoginUser(SocketIOEvent pEvent)
	{
		if (!GameData.get.aLoginState[(int)LoginState.UserInfo])
		{
			Debug.Log("Received Login User info");
			GameUserInfo lPlayerInfo = new GameUserInfo();
			lPlayerInfo.mUniqueId = (uint)pEvent.data.GetField("UniqueId").n;
			lPlayerInfo.mUserName = pEvent.data.GetField("UserName").str;
			if(pEvent.data.GetField("Cash") != null)
				lPlayerInfo.mCash = (int)pEvent.data.GetField("Cash").n;
			if (pEvent.data.GetField("Gold") != null)
				lPlayerInfo.mGold = (int)pEvent.data.GetField("Gold").n;

			lPlayerInfo.mWin = (int)pEvent.data.GetField("Win").n;
			lPlayerInfo.mLose = (int)pEvent.data.GetField("Lose").n;
			lPlayerInfo.mWinStreak = (int)pEvent.data.GetField("WinStreak").n;
			if (pEvent.data.GetField("LoseStreak") != null)
				lPlayerInfo.mLoseStreak = (int)pEvent.data.GetField("LoseStreak").n;
			// 현재 선택된 국가의 점수와 지역
			lPlayerInfo.mRating = (int)pEvent.data.GetField("Rating").n;
			if (pEvent.data.GetField("Area") != null)
				lPlayerInfo.mArea = (int)pEvent.data.GetField("Area").n;
			lPlayerInfo.mUseDeckId = (int)pEvent.data.GetField("UseDeckId").n;
			lPlayerInfo.mUseNation = (DeckNation)pEvent.data.GetField("UseDeckNation").n;
			lPlayerInfo.mUserKey = pEvent.data.GetField("UserKey").str;
			lPlayerInfo.mClearTutorialIdx = (int)pEvent.data.GetField("ClearTutorial").n;

			if (pEvent.data.GetField("LoginTime") != null)
				TimeUtil.aSyncTime = DateTime.Parse(pEvent.data.GetField("LoginTime").str).ToUniversalTime();

			if (pEvent.data.GetField("NationInfo") != null)
			{
				JSONObject lNationData = pEvent.data.GetField("NationInfo");
				for (int iNation = 1; iNation < (int)DeckNation.Count; iNation++)
				{
					JSONObject lNationInfo = lNationData.GetField(iNation.ToString());
					DeckNation lNation = (DeckNation)lNationInfo.GetField("Nation").n;
					if (!lPlayerInfo.mNationData.ContainsKey(lNation))
					{
						lPlayerInfo.mNationData.Add(lNation, new NationData());
					}
					lPlayerInfo.mNationData[lNation].mNation = lNation;
					lPlayerInfo.mNationData[lNation].mNationLevel = (int)lNationInfo.GetField("NationLevel").n;
					lPlayerInfo.mNationData[lNation].mNationExp = (int)lNationInfo.GetField("NationExp").n;
					// 각국가별 점수와 지역
					lPlayerInfo.mNationData[lNation].mArea = (int)lNationInfo.GetField("Area").n;
					lPlayerInfo.mNationData[lNation].mRating = (int)lNationInfo.GetField("Rating").n;
				}
			}
			// 재접속을 위한 유저키를 저장합니다.
			PlayerPrefs.SetString("UserKey", lPlayerInfo.mUserKey);

			GameData.get.aLoginState[(int)LoginState.UserInfo] = true;

			if (aOnRecvLogin != null)
				aOnRecvLogin(lPlayerInfo);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그인 지휘관 아이템 정보 받기
	private void _OnLoginCommanderItem(SocketIOEvent pEvent)
	{
		if (!GameData.get.aLoginState[(int)LoginState.CommanderItem])
		{
			Debug.Log("Received Login Commander Item List");
			List<CommanderItemData> lCommanderItemDataList = new List<CommanderItemData>();
			int lItemCount = (int)pEvent.data.GetField("Count").n;
			JSONObject pItemList = new JSONObject();
			pItemList = pEvent.data.GetField("Items");
			for (int iItem = 0; iItem < lItemCount; iItem++)
			{
				CommanderItemData lCommanderItemData = new CommanderItemData();
				lCommanderItemData.mCommanderItemIdx = (int)pItemList.list[iItem].GetField("ItemId").n;
				lCommanderItemData.mCommanderSpecNo = (int)pItemList.list[iItem].GetField("ItemNo").n;
				lCommanderItemData.mCommanderExpRank = (int)pItemList.list[iItem].GetField("ItemRank").n;
				lCommanderItemData.mCommanderLevel = (int)pItemList.list[iItem].GetField("ItemLevel").n;
				lCommanderItemData.mCommanderItemCount = (int)pItemList.list[iItem].GetField("ItemCount").n;
				lCommanderItemData.mCommanderExp = (int)pItemList.list[iItem].GetField("ItemExp").n;
				//JSONObject lDeckJson = pItemList.list[iItem].GetField("Decks");
				//int lDeckCount = lDeckJson.list.Count;
				//lCommanderItemData.mSlotNo = new int[lDeckCount];
				//for (int iDeck = 0; iDeck < lDeckCount; iDeck++)
				//{
				//	lCommanderItemData.mSlotNo[iDeck] = (int)lDeckJson.list[iDeck].n;		
				//}
				lCommanderItemDataList.Add(lCommanderItemData);
			}

			GameData.get.aLoginState[(int)LoginState.CommanderItem] = true;

			if (aOnRecvLoginCommanderItem != null)
				aOnRecvLoginCommanderItem(lCommanderItemDataList);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 정보 처리
	private void _OnLoginDeck(SocketIOEvent pEvent)
	{
		if (!GameData.get.aLoginState[(int)LoginState.Deck])
		{
			Debug.Log("Received Login Deck");
			List<DeckData> lDeckList = new List<DeckData>();
			int lItemCount = (int)pEvent.data.GetField("Count").n;
			JSONObject lDeckListJson = pEvent.data.GetField("Decks");
			for (int iDeck = 0; iDeck < lItemCount; iDeck++)
			{
				DeckData lDeckData = new DeckData();
				lDeckData.mDeckIdx = (int)lDeckListJson.list[iDeck].GetField("DeckId").n;
				lDeckData.mDeckNation = (DeckNation)lDeckListJson.list[iDeck].GetField("DeckNation").n;
				lDeckData.mDeckName = lDeckListJson.list[iDeck].GetField("DeckName").str;
				for (int iItem = 0; iItem < BattleUserInfo.cMaxCommanderItemCount; iItem++)
				{
					JSONObject lDeckItem = lDeckListJson.list[iDeck].GetField("ItemIds");
					int lItemId = (int)lDeckItem.list[iItem].n;
					if (lItemId < 0)
						lDeckData.SetDeckItem(null, iItem);
					else
						lDeckData.SetDeckItem(GameData.get.aPlayerInfo.FindItemFromItemIdx(lItemId), iItem);
				}
				lDeckList.Add(lDeckData);
			}

			GameData.get.aLoginState[(int)LoginState.Deck] = true;

			if (aOnRecvLoginDeck != null)
				aOnRecvLoginDeck(lDeckList);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 로그 아웃
	public void SendPacket_Logout()
	{
		for (int iState = 0; iState < (int)LoginState.Count; iState++)
		{
			GameData.get.aLoginState[iState] = false;
		}
		vSocket.Emit("cs_logout");
	}
	//-------------------------------------------------------------------------------------------------------
	// 대기 큐 등록
	public void SendPacket_WaitRegiste(int pUseDeckId)
	{
		JSONObject lDeckJsonObject = new JSONObject();
		lDeckJsonObject.AddField("DeckId", pUseDeckId);
		vSocket.Emit("cs_wait_registe", lDeckJsonObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// PVP 대기 큐 등록 완료
	private void _OnWaitRegiste(SocketIOEvent pEvent)
	{
		if (aOnWaitRegiste != null)
			aOnWaitRegiste();
	}
	//-------------------------------------------------------------------------------------------------------
	// 대기 큐 등록 해제
	public void SendPacket_WaitUnRegiste()
	{
		vSocket.Emit("cs_wait_unregiste");
	}
	//-------------------------------------------------------------------------------------------------------
	// PVP 대기 큐 등록 해제 완료
	public void _OnWaitUnRegiste(SocketIOEvent pEvent)
	{
		if (aOnWaitUnRegiste != null)
			aOnWaitUnRegiste();
	}

	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 업데이트 요청
	public void SendPacket_CommanderItemDeck(List<DeckData> pUpdateDeckDataList)
	{
		JSONObject lUpdateDataJSON = new JSONObject();
		for (int iDeck = 0; iDeck < pUpdateDeckDataList.Count; iDeck++)
		{
			JSONObject lDeckDataJSON = new JSONObject();
			lDeckDataJSON.AddField("DeckId", pUpdateDeckDataList[iDeck].mDeckIdx);
			lDeckDataJSON.AddField("DeckNation", (int)pUpdateDeckDataList[iDeck].mDeckNation);
			lDeckDataJSON.AddField("DeckName", pUpdateDeckDataList[iDeck].mDeckName);
			
			JSONObject lItemDataJSON = new JSONObject();
			for (int iItem = 0; iItem < pUpdateDeckDataList[iDeck].mDeckItems.Length; iItem++)
			{
				if (pUpdateDeckDataList[iDeck].mDeckItems[iItem] == null)
					lItemDataJSON.Add(-1);
				else
					lItemDataJSON.Add(pUpdateDeckDataList[iDeck].mDeckItems[iItem].mCommanderItemIdx);
			}
			lDeckDataJSON.AddField("ItemIds", lItemDataJSON);
			
			lUpdateDataJSON.Add(lDeckDataJSON);
		}
		vSocket.Emit("cs_update_commanditemdeck", lUpdateDataJSON);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 업데이트
	private void _OnCommanderItemDeck(SocketIOEvent pEvent)
	{
		if (aOnCommanderItemDeckUpdate != null)
			aOnCommanderItemDeckUpdate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 생성 요청
	public void SendPacket_NewCommanderItemDeck(List<DeckData> pNewDeckDataList)
	{
		JSONObject lNewDataJSON = new JSONObject();
		for (int iDeck = 0; iDeck < pNewDeckDataList.Count; iDeck++)
		{
			JSONObject lDeckDataJSON = new JSONObject();
			lDeckDataJSON.AddField("DeckId", pNewDeckDataList[iDeck].mDeckIdx);
			lDeckDataJSON.AddField("DeckNation", (int)pNewDeckDataList[iDeck].mDeckNation);
			lDeckDataJSON.AddField("DeckName", pNewDeckDataList[iDeck].mDeckName);

			JSONObject lItemDataJSON = new JSONObject();
			for (int iItem = 0; iItem < pNewDeckDataList[iDeck].mDeckItems.Length; iItem++)
			{
				if (pNewDeckDataList[iDeck].mDeckItems[iItem] == null)
					lItemDataJSON.Add(-1);
				else
					lItemDataJSON.Add(pNewDeckDataList[iDeck].mDeckItems[iItem].mCommanderItemIdx);
			}
			lDeckDataJSON.AddField("ItemIds", lItemDataJSON);

			lNewDataJSON.Add(lDeckDataJSON);
		}
		vSocket.Emit("cs_add_commanditemdeck", lNewDataJSON);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 생성
	private void _OnAddCommanderItemDeck(SocketIOEvent pEvent)
	{
		int lNewDeckId = (int)pEvent.data.GetField("NewDeckId").n;
		if (aOnAddCommanderItemDeck != null)
			aOnAddCommanderItemDeck(lNewDeckId);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 삭제 요청
	public void SendPacket_RemoveCommanderItemDeck(int pRemoveDeckIdx)
	{
		JSONObject lRemoveDeckJSON = new JSONObject();
		lRemoveDeckJSON.AddField("DeckId", pRemoveDeckIdx);

		vSocket.Emit("cs_remove_commanditemdeck", lRemoveDeckJSON);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 덱 삭제
	private void _OnRemoveCommanderItemDeck(SocketIOEvent pEvent)
	{
		int lRemoveDeckId = (int)pEvent.data.GetField("DeckId").n;
		if (aOnRemoveCommanderItemDeck != null)
			aOnRemoveCommanderItemDeck(lRemoveDeckId);
	}
	//-------------------------------------------------------------------------------------------------------
	// Replay List Logkey 받아오기
	public void SendPacket_ReplayList()
	{
		vSocket.Emit("cs_replaylist");
	}
	//-------------------------------------------------------------------------------------------------------
	// Replay List Logkey 받아오기
	private void _OnReplayList(SocketIOEvent pEvent)
	{
		List<ReplayData> lReplayList = new List<ReplayData>();

		JSONObject lKeysObject = pEvent.data.GetField("Keys");
		for (int iKey = 0; iKey < lKeysObject.list.Count; iKey++)
		{
			ReplayData lReplayData = new ReplayData();
			lReplayData.mIndex = iKey;
			lReplayData.mLogKey = lKeysObject.list[iKey].GetField("LogKey").str;
			lReplayData.mLogTime = DateTime.Parse(lKeysObject.list[iKey].GetField("LogTime").str);

			lReplayList.Add(lReplayData);
		}

		if (aOnReplayList != null)
			aOnReplayList(lReplayList);
	}
	//-------------------------------------------------------------------------------------------------------
	// Replay Data 받아오기
	public void SendPacket_ReplayData(String pLogKey)
	{
		JSONObject lLogKeyJSON = new JSONObject();
		lLogKeyJSON.AddField("LogKey", pLogKey);
		vSocket.Emit("cs_replaydata", lLogKeyJSON);
	}
	//-------------------------------------------------------------------------------------------------------
	// Replay Data 받아오기
	private void _OnReplayData(SocketIOEvent pEvent)
	{
		BattleVerifyData lBattleVerifyData = new BattleVerifyData(null);

		// 대전 정보를 받아옵니다.
		JSONObject lMatchInfoJson = pEvent.data.GetField("MatchInfo");
		lBattleVerifyData.aMatchInfo.InitFromJson(lMatchInfoJson);

		// 입력 데이터를 받아옵니다.
		JSONObject lReplayDatas = pEvent.data.GetField("Datas");
		int lReplayCount = lReplayDatas.list.Count;
		for (int iData = 0; iData < lReplayCount; iData++)
			lBattleVerifyData.AddFrameInput(lReplayDatas.list[iData].GetField("Data").str);

		// 프레임 입력을 입력 순서에 따라 정렬합니다.
//		lBattleVerifyData.aFrameInputList.Sort();
		SortUtil.SortList<BattleFrameInput>(lBattleVerifyData.aFrameInputList);

		// 콜백을 호출합니다.
		if (aOnReplayData != null)
			aOnReplayData(lBattleVerifyData);
	}
	//-------------------------------------------------------------------------------------------------------
	// PVP(Battle) 종료
	public void SendPacket_BattleEnd()
	{
		vSocket.Emit("cs_battle_end");
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 보상 수령(임시) 테스트용
	public void SendPacket_BattleReward_Test(JSONObject pRewardData, int pStageNo)
	{
		pRewardData.AddField("StageId", pStageNo);
		vSocket.Emit("cs_battle_reward", pRewardData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 보상 수령
	public void SendPacket_BattleReward(int pStageNo)
	{
		JSONObject lRewardData = new JSONObject();
		lRewardData.AddField("StageId", pStageNo);
		vSocket.Emit("cs_battle_reward", lRewardData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 보상 수령
	private void _OnBattleReward(SocketIOEvent pEvent)
	{
		BattleRewardInfo lBattleRewardInfo = new BattleRewardInfo();
		lBattleRewardInfo.InitFromJson(pEvent.data);

		if (aOnBattleReward != null)
			aOnBattleReward(lBattleRewardInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 보상 및 결과
	private void _OnBattleReward2(SocketIOEvent pEvent)
	{
		BattleResultData lBattleResultData = new BattleResultData();
		int lTotalRating = (int)pEvent.data.GetField("Rating").n;
		lBattleResultData.mGainRating = lTotalRating - GameData.get.aPlayerInfo.mRating;
		lBattleResultData.mGainMedal = (int)pEvent.data.GetField("GainMedal").n;
		lBattleResultData.mBattleEndCode = (BattleProtocol.BattleEndCode)pEvent.data.GetField("BattleEndCode").n;
	
		GameData.get.aPlayerInfo.mWin = (int)pEvent.data.GetField("Win").n;
		GameData.get.aPlayerInfo.mWinStreak = (int)pEvent.data.GetField("WinStreak").n;
		GameData.get.aPlayerInfo.mLose = (int)pEvent.data.GetField("Lose").n;
		GameData.get.aPlayerInfo.mLoseStreak = (int)pEvent.data.GetField("LoseStreak").n;
		GameData.get.aPlayerInfo.mArea = (int)pEvent.data.GetField("Area").n;
		GameData.get.aPlayerInfo.mRating = lTotalRating;
		GameData.get.aPlayerInfo.mChestUserData.mMedalCount = (int)pEvent.data.GetField("TotalMedal").n;
		int lNormalChestSlotIdx = (int)pEvent.data.GetField("ChestSlot").n;
		ChestType lNormalChestType = (ChestType)pEvent.data.GetField("ChestType").n;
		if(lNormalChestSlotIdx >= 0)
		{
			GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lNormalChestSlotIdx].mChestType = lNormalChestType;
			GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lNormalChestSlotIdx].mNormalChestState = NormalChestState.Lock;
			GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lNormalChestSlotIdx].mArea = GameData.get.aPlayerInfo.mArea;

			lBattleResultData.mChestIconSpec = ShopChestTable.get.GetIconShopSpec(GameData.get.aPlayerInfo.mArea, lNormalChestType);
		}

		if (aOnBattleReward2 != null)
			aOnBattleReward2(lBattleResultData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 시작
	public void SendPacket_StartTutorial(int pStageNo)
	{
		JSONObject lTutorialInfo = new JSONObject();
		lTutorialInfo.AddField("StageId", pStageNo);
		vSocket.Emit("cs_tutorial_start", lTutorialInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 리펙토링 튜토리얼 시작
	private void _OnStartTutorial2(SocketIOEvent pEvent)
	{
		JSONObject lMatchInfo = pEvent.data.GetField("MatchInfo");
		String lBattleServ = pEvent.data.GetField("BattleServ").str;

		if (aOnStartTutorial2 != null)
			aOnStartTutorial2(lBattleServ, lMatchInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 완료
	public void SendPacket_EndTutorial()
	{
		vSocket.Emit("cs_tutorial_end");
	}
	//-------------------------------------------------------------------------------------------------------
	// 튜토리얼 완료
	private void _OnEndTutorial(SocketIOEvent pEvent)
	{
		int lTutorialCurrentIdx = (int)pEvent.data.GetField("ClearIdx").n;
		GameData.get.aPlayerInfo.mClearTutorialIdx = lTutorialCurrentIdx;
		if (aOnEndTutorial != null)
			aOnEndTutorial(lTutorialCurrentIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 레벨업
	public void SendPacket_CommanderItemLevelUp(CommanderItemData pCommanderItemData)
	{
		JSONObject lItemLevelUpData = new JSONObject();
		lItemLevelUpData.AddField("ItemId", pCommanderItemData.mCommanderItemIdx);
		lItemLevelUpData.AddField("ItemNo", pCommanderItemData.mCommanderSpecNo);
		vSocket.Emit("cs_commander_levelup", lItemLevelUpData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 아이템 레벨업
	private void _OnCommanderLevelUp(SocketIOEvent pEvent)
	{
		GameData.get.aPlayerInfo.mGold = (int)pEvent.data.GetField("Gold").n;
		GameData.get.aPlayerInfo.mCash = (int)pEvent.data.GetField("Cash").n;

		int lItemIdx = (int)pEvent.data.GetField("ItemId").n;
		//int lItemSpecNo = (int)pEvent.data.GetField("ItemNo").n; // 서버측 데이터 변경시 사용
		int lItemLevel = (int)pEvent.data.GetField("ItemLevel").n;
		int lItemCount = (int)pEvent.data.GetField("ItemCount").n;
		CommanderItemData pCommanderItemData = GameData.get.aPlayerInfo.UpdateCommanderItemLevel(lItemIdx, lItemLevel, lItemCount);

		if (aOnCommanderItemLevelUp != null)
			aOnCommanderItemLevelUp(pCommanderItemData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 열기(개발용)
	public void SendPacket_OpenChest(int pChestType, int pArea)
	{
		JSONObject lChestData = new JSONObject();
		lChestData.AddField("ChestType", pChestType);
		lChestData.AddField("Area", pArea);

		vSocket.Emit("cs_open_chest", lChestData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 구매
	public void SendPacket_ShopBuyChest(int pChestType)
	{
		JSONObject lChestData = new JSONObject();
		lChestData.AddField("ChestType", pChestType);

		vSocket.Emit("cs_shop_chest_buy", lChestData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 열기
	private void _OnOpenChest(SocketIOEvent pEvent)
	{
		ChestOpenData lChestOpenSpec = GameData.get.aPlayerInfo.UpdateChestOpenData(pEvent.data);

		if (aOnOpenChest != null)
			aOnOpenChest(lChestOpenSpec);
	}
	//-------------------------------------------------------------------------------------------------------
	// 골드 구매
	public void SendPacket_ShopBuyGold(int pGoodsNo)
	{
		JSONObject lGoodsData = new JSONObject();
		lGoodsData.AddField("GoodsNo", pGoodsNo);

		vSocket.Emit("cs_shop_gold_buy", lGoodsData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 골드 구매
	private void _OnShopBuyGold(SocketIOEvent pEvent)
	{
		int lResultGold = (int)pEvent.data.GetField("Gold").n;
		int lGainGold = lResultGold - GameData.get.aPlayerInfo.mGold;
		GameData.get.aPlayerInfo.mGold = lResultGold;
		GameData.get.aPlayerInfo.mCash = (int)pEvent.data.GetField("Cash").n;

		if (aOnShopBuyGold != null)
			aOnShopBuyGold(lGainGold);
	}
	//-------------------------------------------------------------------------------------------------------
	// 캐쉬(잼) 구매
	public void SendPacket_ShopBuyGem(int pGoodsNo)
	{
		JSONObject lGoodsData = new JSONObject();
		lGoodsData.AddField("GoodsNo", pGoodsNo);

		vSocket.Emit("cs_shop_buy_gem", lGoodsData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 캐쉬(잼) 구매
	private void _OnShopBuyGem(SocketIOEvent pEvent)
	{
		int lResultGem = (int)pEvent.data.GetField("Cash").n;
		int lGainGem = lResultGem - GameData.get.aPlayerInfo.mCash;
		GameData.get.aPlayerInfo.mCash = lResultGem;

		if (aOnShopBuyGem != null)
			aOnShopBuyGem(lGainGem);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 구매
	public void Sendpacket_ShopBuyCommander(int pShopIdx)
	{
		JSONObject lShopCommanderData = new JSONObject();
		lShopCommanderData.AddField("ShopIndex", pShopIdx);

		vSocket.Emit("cs_shop_commander_buy", lShopCommanderData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 구매
	private void _OnShopBuyCommander(SocketIOEvent pEvent)
	{
		int lItemIdx = (int)pEvent.data.GetField("ItemId").n;
		int lItemNo = (int)pEvent.data.GetField("ItemNo").n;
		int lItemCount = (int)pEvent.data.GetField("ItemCount").n;
		GameData.get.aPlayerInfo.UpdateCommanderItemCount(lItemIdx, lItemNo, lItemCount);

		int lUpdateShopIndex = (int)pEvent.data.GetField("ShopIndex").n;
		int lShopBuyCount = (int)pEvent.data.GetField("ShopIndex").n;
		int lRemainCount = (int)pEvent.data.GetField("RemainCount").n;
		int lPrice = (int)pEvent.data.GetField("Price").n;
		GameData.get.aPlayerInfo.UpdateShopCommanderItem(lUpdateShopIndex, lShopBuyCount, lRemainCount, lPrice);
		
		GameData.get.aPlayerInfo.mGold = (int)pEvent.data.GetField("Gold").n;
		GameData.get.aPlayerInfo.mCash = (int)pEvent.data.GetField("Cash").n;

		if (aOnShopBuyCommander != null)
			aOnShopBuyCommander(lUpdateShopIndex);
	}
	//-------------------------------------------------------------------------------------------------------
	// 샵 물품 리스트 요청
	public void SendPacket_ReqShopList()
	{
		// GameData.get.aLoginState[(int)LoginState.Shop] = true;
		JSONObject lAreaData = new JSONObject();
		lAreaData.AddField("Area", GameData.get.aPlayerInfo.mArea);

		vSocket.Emit("cs_shop_list", lAreaData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 샵 물품 리스트
	private void _OnShopList(SocketIOEvent pEvent)
	{
		Debug.Log("Received Shop List"); 

		// Chest 입력
		GameData.get.aPlayerInfo.mShopChestDataList.Clear();
		JSONObject lShopChestListJson = pEvent.data.GetField("Chest");
		for (int iChest = 0; iChest < lShopChestListJson.list.Count; iChest++)
		{
			ShopChestData lShopChestData = new ShopChestData();
			lShopChestData.SetShopDataJson(lShopChestListJson.list[iChest]);
			GameData.get.aPlayerInfo.mShopChestDataList.Add(lShopChestData);
		}
		// Goods 입력
		GameData.get.aPlayerInfo.mShopGemDataList.Clear();
		GameData.get.aPlayerInfo.mShopGoldDataList.Clear();
		JSONObject lShopGoodsListJson = pEvent.data.GetField("Goods");
		for (int iGoods = 0; iGoods < lShopGoodsListJson.list.Count; iGoods++)
		{
			ShopGoodsData lShopGoodsData = new ShopGoodsData();
			lShopGoodsData.SetShopDataJson(lShopGoodsListJson.list[iGoods]);
			if (lShopGoodsData.mType == GoodsType.Gem)
				GameData.get.aPlayerInfo.mShopGemDataList.Add(lShopGoodsData);
			else //if(lShopGoodsData.mType == GoodsType.Gold)
				GameData.get.aPlayerInfo.mShopGoldDataList.Add(lShopGoodsData);
		}
		// Shop CommanderItemData 입력
		GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList.Clear();
		
		JSONObject lShopCommanderInfoJson = pEvent.data.GetField("Commander");
		GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mResetTime = DateTime.Parse(lShopCommanderInfoJson.GetField("ResetTime").str).ToUniversalTime();
		
		JSONObject lShopCommanderItemListJson = lShopCommanderInfoJson.GetField("CommanderList");
		for (int iCommander = 0; iCommander < lShopCommanderItemListJson.list.Count; iCommander++)
		{
			ShopCommanderItemData lShopCommanderItemData = new ShopCommanderItemData();
			lShopCommanderItemData.mCommanderShopIdx = (int)lShopCommanderItemListJson[iCommander].GetField("Index").n;
			lShopCommanderItemData.mCommanderItemNo = (int)lShopCommanderItemListJson[iCommander].GetField("CommanderNo").n;
			lShopCommanderItemData.mCommanderItemBuyCount = (int)lShopCommanderItemListJson[iCommander].GetField("Count").n;
			lShopCommanderItemData.mCommanderItemRemainCount = (int)lShopCommanderItemListJson[iCommander].GetField("RemainCount").n;
			lShopCommanderItemData.mPrice = (int)lShopCommanderItemListJson[iCommander].GetField("Price").n;

			GameData.get.aPlayerInfo.mShopCommanderItemInfoData.mCommanderList.Add(lShopCommanderItemData);
		}
		
		GameData.get.aLoginState[(int)LoginState.Shop] = true;
		
		if (aOnShopList != null)
			aOnShopList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 정보 획득
	public void SendPacket_ReqChestList()
	{
		//GameData.get.aLoginState[(int)LoginState.ChestList] = true;
		vSocket.Emit("cs_chest_list"); 
	}
	//-------------------------------------------------------------------------------------------------------
	// 상자 정보 획득
	private void _OnChestList(SocketIOEvent pEvent)
	{
		Debug.Log("Received Chest List");

		//DateTime.Parse(pEvent.data.GetField("LoginTime").str).ToUniversalTime();
		ChestUserData lChestUserData = GameData.get.aPlayerInfo.mChestUserData;
		lChestUserData.mFreeChest0Time = DateTime.Parse(pEvent.data.GetField("FCTime0").str).ToUniversalTime();
		lChestUserData.mFreeChest1Time = DateTime.Parse(pEvent.data.GetField("FCTime1").str).ToUniversalTime();
		lChestUserData.mMedalCount = (int)pEvent.data.GetField("MCCount").n;
		lChestUserData.mMedalMaxCount = (int)pEvent.data.GetField("MCMaxCount").n;
		lChestUserData.mMedalTime = DateTime.Parse(pEvent.data.GetField("MCTime").str).ToUniversalTime();
		JSONObject lNormalChestInfo = pEvent.data.GetField("NCInfo");
		for (int iChest = 0; iChest < lNormalChestInfo.list.Count; iChest++)
		{
			NormalChestData lNormalChestData = new NormalChestData();
			lNormalChestData.mSlotIdx = (int)lNormalChestInfo[iChest].GetField("SlotIdx").n;
			lNormalChestData.mArea = (int)lNormalChestInfo[iChest].GetField("Area").n;
			lNormalChestData.mChestType = (ChestType)lNormalChestInfo[iChest].GetField("Type").n;
			lNormalChestData.mNormalChestState = (NormalChestState)lNormalChestInfo[iChest].GetField("State").n;
			lNormalChestData.mOpenTime = DateTime.Parse(lNormalChestInfo[iChest].GetField("Time").str).ToUniversalTime();
			
			lChestUserData.mNormalChestList.Add(lNormalChestData);
		}
		GameData.get.aLoginState[(int)LoginState.ChestList] = true;
		if (aOnChestList != null)
			aOnChestList();
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 보상 상자 활성화 상태로 변경
	public void SendPacket_ActiveNormalChest(int pSlotIdx)
	{
		JSONObject lChestData = new JSONObject();
		lChestData.AddField("SlotIdx", pSlotIdx);
		vSocket.Emit("cs_active_normal_chest", lChestData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 보상 상자 활성화 상태로 변경
	private void _OnActiveChest(SocketIOEvent pEvent)
	{
		int lActiveSlotIdx = (int)pEvent.data.GetField("SlotIdx").n;
		DateTime lOpenTime = DateTime.Parse(pEvent.data.GetField("ChestTime").str).ToUniversalTime();
		GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lActiveSlotIdx].mOpenTime = lOpenTime;
		GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lActiveSlotIdx].mNormalChestState = NormalChestState.Active;

		if (aOnActiveNormalChest != null)
			aOnActiveNormalChest(lActiveSlotIdx);
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 보상 상자 열기
	public void SendPacket_OpenNormalChest(int pSlotIdx)
	{
		JSONObject lChestData = new JSONObject();
		lChestData.AddField("SlotIdx", pSlotIdx);
		vSocket.Emit("cs_open_normal_chest", lChestData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 보상 상자 열기
	private void _OnOpenNormalChest(SocketIOEvent pEvent)
	{
		JSONObject lChestOpenDataJSON = pEvent.data.GetField("Chest");
		ChestOpenData lChestOpenData = GameData.get.aPlayerInfo.UpdateChestOpenData(lChestOpenDataJSON);
		int lNormalChestSlotIdx = (int)pEvent.data.GetField("SlotIdx").n;
		GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lNormalChestSlotIdx].mChestType = ChestType.Empty;
		GameData.get.aPlayerInfo.mChestUserData.mNormalChestList[lNormalChestSlotIdx].mNormalChestState = NormalChestState.Empty;
		
		if (aOnOpenNormalChest != null)
			aOnOpenNormalChest(lNormalChestSlotIdx, lChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 무료 상자 열기
	public void SendPacket_OpenFreeChest()
	{
		vSocket.Emit("cs_open_free_chest");
	}
	//-------------------------------------------------------------------------------------------------------
	// 무료 상자 열기
	private void _OnOpenFreeChest(SocketIOEvent pEvent)
	{
		GameData.get.aPlayerInfo.mChestUserData.mFreeChest0Time = DateTime.Parse(pEvent.data.GetField("Time0").str).ToUniversalTime();
		GameData.get.aPlayerInfo.mChestUserData.mFreeChest1Time = DateTime.Parse(pEvent.data.GetField("Time1").str).ToUniversalTime();
		JSONObject lChestOpenDataJSON = pEvent.data.GetField("OpenChest");
		ChestOpenData lChestOpenData = GameData.get.aPlayerInfo.UpdateChestOpenData(lChestOpenDataJSON);

		if (aOnOpenFreeChest != null)
			aOnOpenFreeChest(lChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메달 상자 열기
	public void SendPacket_OpenMedalChest()
	{
		vSocket.Emit("cs_open_medal_chest");
	}
	//-------------------------------------------------------------------------------------------------------
	// 메달 상자 열기
	private void _OnOpenMedalChest(SocketIOEvent pEvent)
	{
		GameData.get.aPlayerInfo.mChestUserData.mMedalTime = DateTime.Parse(pEvent.data.GetField("MedalTime").str).ToUniversalTime();
		GameData.get.aPlayerInfo.mChestUserData.mMedalCount = 0;
		JSONObject lChestOpenDataJSON = pEvent.data.GetField("OpenChest");
		ChestOpenData lChestOpenData = GameData.get.aPlayerInfo.UpdateChestOpenData(lChestOpenDataJSON);

		if (aOnOpenMedalChest != null)
			aOnOpenMedalChest(lChestOpenData);
	}
	//-------------------------------------------------------------------------------------------------------
	// 덱 업데이트
	public void SendPacket_UpdateDeck(int pSelectDeckIdx, List<DeckData> pUpdateDeckDataList)
	{
		JSONObject lUpdateDeckJSON = new JSONObject();
		JSONObject lUpdateDeckListJSON = new JSONObject(JSONObject.Type.ARRAY);
		lUpdateDeckJSON.AddField("UseDeckId", pSelectDeckIdx);
		for (int iDeck = 0; iDeck < pUpdateDeckDataList.Count; iDeck++)
		{
			JSONObject lUpdateDeckData = new JSONObject();
			lUpdateDeckData.AddField("DeckId", pUpdateDeckDataList[iDeck].mDeckIdx);
			lUpdateDeckData.AddField("Nation", (int)pUpdateDeckDataList[iDeck].mDeckNation);
			JSONObject lItemIdList = new JSONObject(JSONObject.Type.ARRAY);
			for (int iItem = 0; iItem < pUpdateDeckDataList[iDeck].mDeckItems.Length; iItem++)
			{
				lItemIdList.Add(pUpdateDeckDataList[iDeck].mDeckItems[iItem].mCommanderItemIdx);
			}
			lUpdateDeckData.AddField("ItemIds", lItemIdList);
			lUpdateDeckListJSON.Add(lUpdateDeckData);
		}
		lUpdateDeckJSON.AddField("DeckInfo", lUpdateDeckListJSON);
		vSocket.Emit("cs_update_deck", lUpdateDeckJSON);
	}
	//-------------------------------------------------------------------------------------------------------
	// 매치 정보 획득, 방 시작
	private void _OnStartRoom(SocketIOEvent pEvent)
	{
		JSONObject lMatchInfo = pEvent.data.GetField("MatchInfo");
		String lBattleServ = pEvent.data.GetField("BattleServ").str;

		if (aOnStartBattleRoom != null) 
			aOnStartBattleRoom(lBattleServ, lMatchInfo);
	}
	//-------------------------------------------------------------------------------------------------------
	// [개발용 코드]보상정보를 서버에 입력완료합니다.
	private void _OnDevSetBattleReward(SocketIOEvent pEvent)
	{
		if (aOnDevSetBattleReward != null)
			aOnDevSetBattleReward();
	}
	//-------------------------------------------------------------------------------------------------------
	// 서버 에러
	private void _OnError(SocketIOEvent pEvent)
	{
		NetworkErrorCode lNetworkErrorCode = (NetworkErrorCode)pEvent.data.GetField("ErrorCode").n;
		if (aOnGameServerError != null)
			aOnGameServerError(lNetworkErrorCode);
	}
	//-------------------------------------------------------------------------------------------------------
    // 채팅 전송
    public void SendPacket_Chat(String pMessage)
    {
        JSONObject lChatJsonObject = new JSONObject();
        lChatJsonObject.AddField("Msg", pMessage);
        
        vSocket.Emit("cs_chat", lChatJsonObject);
    }
    //-------------------------------------------------------------------------------------------------------
    // 채팅 받기
    private void _OnChat(SocketIOEvent pEvent)
    {
        String lUserName = pEvent.data.GetField("Name").str;
        String lMessage = pEvent.data.GetField("Msg").str;
        if (aOnChat != null)
            aOnChat(lUserName, lMessage);
    }
	//-------------------------------------------------------------------------------------------------------
	// 현재 검증서버에서 보상이 나오지 않아 전투 종료시 해당 패킷을 우선 보내 놓습니다.
	// 실제 서비스에서는 해당 패킷을 호출하지 말아야 합니다.
	public void SendPacket_Dev_BattleReward(JSONObject pBattleReward)
	{
		vSocket.Emit("cs_dev_battle_reward", pBattleReward);
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	//private String mLoginUserName = String.Empty;
	//private bool mIsConnected = false;
}
