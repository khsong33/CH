﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
//-------------------------------------------------------------------------------------------------------
// Scene에 상관없이 갖고 있어야할 데이터를 저장하는 클래스
//-------------------------------------------------------------------------------------------------------
public class MessageBoxManager
{
	public delegate void OnClickOKDelegate();
	public delegate void OnClickCancelDelegate();

	public enum MessageBoxType
	{
		OK,
		OK_Cancel,
		OK_Cancel_Price,
	}
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public OnClickOKDelegate aOnClickedOk { get; set; }
	public OnClickCancelDelegate aOnClickedCancel { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 인스턴스
	//-------------------------------------------------------------------------------------------------------
	public static MessageBoxManager aInstance
	{
		get
		{
			if (sInstance == null)
			{
				sInstance = new MessageBoxManager();
			}
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 루트 오브젝트 등록
	public void RegisteRoot(MessageBoxRoot pMessageBoxRoot)
	{
		mRoot = pMessageBoxRoot;
		mRoot.SetActiveMessageBox(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메시지 박스 열기
	public void OpenMessageBox(String pMessage, MessageBoxType pType)
	{
		mRoot.SetActiveMessageBox(true);
		mRoot.SetMessage(pMessage);
		mRoot.OpenButtonType(pType);
	}
	public void OpenMessageBox(String pMessage, MessageBoxType pType, OnClickOKDelegate pOkDelegate)
	{
		OpenMessageBox(pMessage, pType);
		aOnClickedOk = pOkDelegate;
	}
	public void OpenMessageBox(String pMessage, MessageBoxType pType, OnClickOKDelegate pOkDelegate, OnClickCancelDelegate pCancelDelegate)
	{
		OpenMessageBox(pMessage, pType);
		aOnClickedOk = pOkDelegate;
		aOnClickedCancel = pCancelDelegate;
	}
	//-------------------------------------------------------------------------------------------------------
	// 가격 셋팅
	public void SetGemLabel(int pPrice)
	{
		mRoot.SetGemLabel(pPrice);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메시지 박스 닫기
	public void CloseMessageBox()
	{
		if (!mIsSafe)
		{
			mRoot.SetActiveMessageBox(false);
			aOnClickedOk = null;
			aOnClickedCancel = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메시지 박스 닫기 보호
	public void SetSafeMessageBox(bool pIsActive)
	{
		mIsSafe = pIsActive;
	}
	//-------------------------------------------------------------------------------------------------------
	// OK 버튼 클릭
	public void OnClickedOk()
	{
		Debug.Log("On Click OK Button");

		if (aOnClickedOk != null)
			aOnClickedOk();
	
		CloseMessageBox();
	}
	//-------------------------------------------------------------------------------------------------------
	// Cancel 버튼 클릭
	public void OnClickedCancel()
	{
		Debug.Log("On Clicked Cancel Button");
		if (aOnClickedCancel != null)
			aOnClickedCancel();

		CloseMessageBox();
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static MessageBoxManager sInstance = null;
	private MessageBoxRoot mRoot = null;
	private bool mIsSafe;
}