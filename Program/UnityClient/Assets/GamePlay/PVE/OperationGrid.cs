﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class OperationGrid : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public int vRowCount = 20;
	public int vColCount = 20;
	public float vGridInterval = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		//Debug.Log("Awake");
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//StartCoroutine(_CoCreate());
		CreateGrid();
	}

	private IEnumerator _CoCreate()
	{
		yield return null;
		yield return null;
		CreateGrid();
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 그리드를 생성합니다.
	public void CreateGrid()
	{
		float lIntervalValue = vGridInterval * 0.5f;
		float lRowStartPoint = vRowCount * 0.5f;
		float lColStartPoint = vColCount * 0.5f;

		Vector3 lGridStartPoint = new Vector3((-1.0f * vGridInterval * lRowStartPoint) + lIntervalValue, 
											  0.0f,
											  (vGridInterval * lColStartPoint)- lIntervalValue);

		for (int iRow = 0; iRow < vRowCount; iRow++)
		{
			for (int iCol = 0; iCol < vColCount; iCol++)
			{
				GameObject lObj = ResourceUtil.Load<GameObject>("Grid Cell");
				GameObject lGridObject = (GameObject)Instantiate(lObj);
				lGridObject.transform.parent = transform;
				lGridObject.transform.localPosition = new Vector3(lGridStartPoint.x + (vGridInterval * iRow),
														   0.1f,
														   lGridStartPoint.z - (vGridInterval * iCol));

				lGridObject.name = String.Format("Grid Cell {0}:{1}", iRow, iCol);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
