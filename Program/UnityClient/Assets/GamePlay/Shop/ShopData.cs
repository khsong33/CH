﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using CombatJson;

public enum ChestType
{
	Empty = 0,
	Free,
	Crown,
	Wooden,
	Silver,
	Golden,
	Magic,
}
public enum GoodsType
{
	Gem = 1,
	Gold = 2
}
// Chest Server 데이터 - 실제 가격 등
public class ShopChestData
{
	public ChestType mType;
	public int mArea;
	public int mPrice;
	public ShopChestData()
	{
		mType = ChestType.Empty;
		mArea = 0;
		mPrice = 0;
	}
	public void SetShopDataJson(JSONObject pShopChestJson)
	{
		mType = (ChestType)pShopChestJson.GetField("Type").n;
		mArea = (int)pShopChestJson.GetField("Area").n;
		mPrice = (int)pShopChestJson.GetField("Price").n;
	}
}

// Goods Server 데이터
public class ShopGoodsData
{
	public int mGoodsNo;
	public GoodsType mType;
	public int mCount;
	public int mPriceGem;
	public float mPriceCash;
	public ShopGoodsData()
	{
		mCount = 0;
		mPriceGem = 0;
		mPriceCash = 0.0f;
	}
	public void SetShopDataJson(JSONObject pShopGoodsJson)
	{
		mGoodsNo = (int)pShopGoodsJson.GetField("GoodsId").n;
		mType = (GoodsType)pShopGoodsJson.GetField("Type").n;
		mCount = (int)pShopGoodsJson.GetField("Count").n;
		mPriceGem = (int)pShopGoodsJson.GetField("PriceGem").n;
		mPriceCash = (float)pShopGoodsJson.GetField("PriceCash").f;
	}
}
// Goods Client 데이터
public class ShopGoodsSpec
{
	public int mGoodsNo;
	public String mNameKey;
	public GoodsType mGoodsType;
	public AssetKey mIconAssetKey;
	// ToString()
	public override string ToString()
	{
		return String.Format("<ShopGoodsSpec[{0}] mName={1} mGoodsKey={2}>", mGoodsNo, mNameKey, mGoodsType);
	}
}
// Commander Shop 데이터
public class ShopCommanderItemInfoData
{
	public DateTime mResetTime;
	public List<ShopCommanderItemData> mCommanderList;
	public ShopCommanderItemInfoData()
	{
		mCommanderList = new List<ShopCommanderItemData>();
	}
}
// Commander Shop 안에 있는 Commander Item 데이터
public class ShopCommanderItemData
{
	public int mCommanderShopIdx;
	public int mCommanderItemNo;
	public int mCommanderItemBuyCount;
	public int mCommanderItemRemainCount;
	public int mPrice;
	// ToString()
	public override string ToString()
	{
		return String.Format("<ShopCommanderItemData[{0}] mCommanderShopIdx={1} mCommanderItemNo={2}, mCommanderBuyCount={2}>", 
			mCommanderShopIdx, mCommanderItemNo, mCommanderItemBuyCount);
	}
}