﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
// 상점 상자 테이블
public class ShopChestTable
{
	public static readonly String cCSVTextAssetPath = "ItemChestInfoTableCSV";

	public static readonly String vNoColumn = "No";
	public static readonly String vNameKeyColumn = "NameKey";
	public static readonly String vTypeColumn = "Type";
	public static readonly String vAreaColumn = "Area";
	public static readonly String vIconBundleColumn = "IconBundle";
	public static readonly String vIconPathColumn = "IconPath";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static ShopChestTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new ShopChestTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public ShopChestTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mChestTypeDictionary = new Dictionary<String, ChestType>();
		foreach (ChestType iForm in Enum.GetValues(typeof(ChestType)))
			mChestTypeDictionary.Add(iForm.ToString(), iForm);
	
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mDicChestAreaType = new Dictionary<int, Dictionary<ChestType, ChestIconSpec>>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			ChestIconSpec info = new ChestIconSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mNo = lCSVRow.GetIntValue(vNoColumn);
			info.mArea = lCSVRow.GetIntValue(vAreaColumn);
			info.mNameKey = lCSVRow.GetStringValue(vNameKeyColumn);
			String lTypeString = lCSVRow.GetStringValue(vTypeColumn);
			if (!mChestTypeDictionary.TryGetValue(lTypeString, out info.mChestType))
				Debug.LogError(String.Format("Shop Chest Table [{0}] invalid Type '{1}'", info.mNo, lTypeString));

			info.mIconAssetKey = AssetManager.get.CreateAssetKey(
				lCSVRow.GetStringValue(vIconBundleColumn),
				lCSVRow.GetStringValue(vIconPathColumn));

			if (!mDicChestAreaType.ContainsKey(info.mArea))
			{
				mDicChestAreaType.Add(info.mArea, new Dictionary<ChestType, ChestIconSpec>());
			}
			if (!mDicChestAreaType[info.mArea].ContainsKey(info.mChestType))
			{
				mDicChestAreaType[info.mArea].Add(info.mChestType, info);
			}
			else
			{
				Debug.LogError("Same Dictionary data Area - " + info.mArea + "/ Type - " + info.mChestType);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Client Shop Spec을 가져옵니다.
	public ChestIconSpec GetIconShopSpec(int pArea, ChestType pChestType)
	{
		if (!mDicChestAreaType.ContainsKey(pArea))
		{
			Debug.LogError("Not Exist Area - " + pArea);
			return null;
		}
		if (!mDicChestAreaType[pArea].ContainsKey(pChestType))
		{
			Debug.LogError("Not Exist Chest Type " + pChestType.ToString() + " in Area " + pArea);
			return null;
		}
		return mDicChestAreaType[pArea][pChestType];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderLevelTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static ShopChestTable sInstance = null;

	private Dictionary<String, ChestType> mChestTypeDictionary;

	private CSVObject mCSVObject;
	private Dictionary<int, Dictionary<ChestType, ChestIconSpec>> mDicChestAreaType;
}
// Shop Goods Table
public class ShopGoodsTable
{
	public static readonly String cCSVTextAssetPath = "ItemGoodsInfoTableCSV";

	public static readonly String vNoColumn = "No";
	public static readonly String vNameKeyColumn = "NameKey";
	public static readonly String vTypeColumn = "Type";
	public static readonly String vIconBundleColumn = "IconBundle";
	public static readonly String vIconPathColumn = "IconPath";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static ShopGoodsTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new ShopGoodsTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public ShopGoodsTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mGoodsTypeDictionary = new Dictionary<String, GoodsType>();
		foreach (GoodsType iForm in Enum.GetValues(typeof(GoodsType)))
			mGoodsTypeDictionary.Add(iForm.ToString(), iForm);

		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mDicGoodsSpecs = new Dictionary<int,ShopGoodsSpec>();

		int lCSVRowCount = mCSVObject.RowNum;

		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			ShopGoodsSpec info = new ShopGoodsSpec();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			lCSVRow.SetParsingLogPrefix(_OnParsingLogPrefix, iRow.ToString());
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mGoodsNo = lCSVRow.GetIntValue(vNoColumn);
			info.mNameKey = lCSVRow.GetStringValue(vNameKeyColumn);
			String lTypeString = lCSVRow.GetStringValue(vTypeColumn);
			if (!mGoodsTypeDictionary.TryGetValue(lTypeString, out info.mGoodsType))
				Debug.LogError(String.Format("Shop Goods Table [{0}] invalid Type '{1}'", info.mGoodsNo, lTypeString));

			info.mIconAssetKey = AssetManager.get.CreateAssetKey(
				lCSVRow.GetStringValue(vIconBundleColumn),
				lCSVRow.GetStringValue(vIconPathColumn));

			mDicGoodsSpecs.Add(info.mGoodsNo, info);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Client Shop Spec을 가져옵니다.
	public ShopGoodsSpec GetShopSpec(int pGoodsNo)
	{
		if (!mDicGoodsSpecs.ContainsKey(pGoodsNo))
		{
			Debug.LogError("Not Exist Goods No - " + pGoodsNo);
			return null;
		}
		return mDicGoodsSpecs[pGoodsNo];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// CSVRow.OnParsingLogPrefixDelegate
	private String _OnParsingLogPrefix(String pRowInfo)
	{
		return String.Format("CommanderLevelTable[{0}]", pRowInfo);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static ShopGoodsTable sInstance = null;

	private Dictionary<String, GoodsType> mGoodsTypeDictionary;

	private CSVObject mCSVObject;
	private Dictionary<int, ShopGoodsSpec> mDicGoodsSpecs;
}

