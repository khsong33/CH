using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapHeight : MonoBehaviour
{
	public enum SamplingMethod
	{
		HeightMap,
		Collider,
		Test,
	}

	private const float cTestMaxHeight = 100.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SamplingMethod vSamplingMethod = SamplingMethod.HeightMap;
	public Texture2D vHeightMapTexture;
	public String vHeightMapColliderLayer = "Terrain";
	public Rect vSamplingArea;
	public float vTileScale = 0.25f;
	public float vMaxHeight = 256.0f;

	public bool vIsShowDebugInfo = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if ((vSamplingMethod == SamplingMethod.HeightMap) &&
			(vHeightMapTexture == null))
			Debug.LogError("vHeightMapTexture is empty - " + this);

		switch (vSamplingMethod)
		{
		case SamplingMethod.HeightMap:
			{
				LoadHeightMapRgba(vHeightMapTexture, vMaxHeight);
				mIsMeshCollider = false;
			}
			break;
		case SamplingMethod.Collider:
			{
				mMeshColliderRayOrigin = new Vector3(0.0f, vMaxHeight, 0.0f);
				mMeshColliderRayDirection = new Vector3(0.0f, -1.0f, 0.0f);
				mMeshColliderRayMaxDistance = vMaxHeight;
				mMeshColliderRayLayerMask = (1 << LayerMask.NameToLayer(vHeightMapColliderLayer));
				mIsMeshCollider = true;
			}
			break;
		case SamplingMethod.Test:
			{
				LoadTestHeightMap(vMaxHeight);
				mIsMeshCollider = false;
			}
			break;
		}
		vHeightMapTexture = null; // 해제
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
#if UNITY_EDITOR
	void Update()
	{
		if (vIsShowDebugInfo)
			_ShowDebugInfo();

		mMeshColliderRayOrigin.y = vMaxHeight;
		mMeshColliderRayMaxDistance = vMaxHeight;
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// RGBA 포맷의 높이 맵을 로딩합니다.
	public void LoadHeightMapRgba(Texture2D pRgbaTexture, float pMaxHeight)
	{
		int lTextureWidth = pRgbaTexture.width;
		int lTextureHeight = pRgbaTexture.height;

		int lRowSamplingOffset = (int)vSamplingArea.yMin;
		int lColSamplingOffset = (int)vSamplingArea.xMin;
		mRowCount = (int)vSamplingArea.height;
		mColCount = (int)vSamplingArea.width;
		if ((lRowSamplingOffset + mRowCount) > lTextureHeight)
			Debug.LogError("invalid vSamplingArea:" + vSamplingArea);
		if ((lColSamplingOffset + mColCount) > lTextureWidth)
			Debug.LogError("invalid vSamplingArea:" + vSamplingArea);

		Color32[] lHeightMapColor32s = pRgbaTexture.GetPixels32();
		mHeights = new float[mRowCount + 1, mColCount + 1]; // 끝 줄을 예외 처리 안 하려고 여분의 한 줄을 추가

		mMaxHeightOnLoad = vMaxHeight; // 디버깅 목적으로 저장
		float lHeightColorScale = pMaxHeight / 255.0f;

		for (int iRow = 0; iRow < mRowCount; ++iRow)
			for (int iCol = 0; iCol < mColCount; ++iCol)
				mHeights[iRow, iCol] = lHeightMapColor32s[(lRowSamplingOffset + iRow) * lTextureWidth + (lColSamplingOffset + iCol)].r * lHeightColorScale;

		for (int iRow = 0; iRow <= mRowCount; ++iRow) // 오른쪽 여분의 한 줄을 그 왼쪽 줄과 같은 높이로 초기화
			mHeights[iRow, mColCount] = mHeights[iRow, mColCount - 1];
		for (int iCol = 0; iCol < mColCount; ++iCol) // 위쪽 여분의 한 줄을 그 아래 줄과 같은 높이로 초기화
			mHeights[mRowCount, iCol] = mHeights[mRowCount - 1, iCol];
	}
	//-------------------------------------------------------------------------------------------------------
	// 테스트 높이 맵을 로딩합니다.
	public void LoadTestHeightMap(float pMaxHeight)
	{
		vTileScale *= vSamplingArea.width;
		vMaxHeight = cTestMaxHeight;

		mRowCount = 2;
		mColCount = 2;
		mHeights = new float[mRowCount + 1, mColCount + 1]; // 끝 줄을 예외 처리 안 하려고 여분의 한 줄을 추가
		mMaxHeightOnLoad = vMaxHeight; // 디버깅 목적으로 저장

		mHeights[0, 0] = 0.0f;
		mHeights[0, 1] = 0.0f;
		mHeights[1, 0] = vMaxHeight;
		mHeights[1, 1] = 0.0f;

		for (int iRow = 0; iRow <= mRowCount; ++iRow) // 오른쪽 여분의 한 줄을 그 왼쪽 줄과 같은 높이로 초기화
			mHeights[iRow, mColCount] = mHeights[iRow, mColCount - 1];
		for (int iCol = 0; iCol < mColCount; ++iCol) // 위쪽 여분의 한 줄을 그 아래 줄과 같은 높이로 초기화
			mHeights[mRowCount, iCol] = mHeights[mRowCount - 1, iCol];
	}
	//-------------------------------------------------------------------------------------------------------
	// 높이를 얻습니다.
	public float SampleHeight(Vector3 pPosition)
	{
		// 메쉬 충돌체 방식일 경우의 처리를 합니다.
		if (mIsMeshCollider)
		{
			mMeshColliderRayOrigin.x = pPosition.x;
			mMeshColliderRayOrigin.z = pPosition.z;
			Ray lPositionRay = new Ray(mMeshColliderRayOrigin, mMeshColliderRayDirection);
			RaycastHit lRaycastHit;
			if (Physics.Raycast(lPositionRay, out lRaycastHit, mMeshColliderRayMaxDistance, mMeshColliderRayLayerMask))
				return lRaycastHit.point.y;
			else
				return 0.0f;
		}

		// 높이맵 방식일 경우의 처리를 합니다.
		int lRowIndex = (int)(pPosition.z / vTileScale);
		float lRowOffset;
		if (lRowIndex < 0)
		{
			lRowIndex = 0;
			lRowOffset = 0.0f;
		}
		else if (lRowIndex < mRowCount)
		{
			lRowOffset = pPosition.z - lRowIndex * vTileScale;
		}
		else
		{
			lRowIndex = mRowCount - 1;
			lRowOffset = vTileScale;
		}

		int lColIndex = (int)(pPosition.x / vTileScale);
		float lColOffset;
		if (lColIndex < 0)
		{
			lColIndex = 0;
			lColOffset = 0.0f;
		}
		else if (lColIndex < mColCount)
		{
			lColOffset = pPosition.x - lColIndex * vTileScale;
		}
		else
		{
			lColIndex = mColCount - 1;
			lColOffset = vTileScale;
		}

		float lLeftBottomHeight = mHeights[lRowIndex, lColIndex];
		float lRightBottomHeight = mHeights[lRowIndex, lColIndex + 1];
		float lLeftTopHeight = mHeights[lRowIndex + 1, lColIndex];
		float lRightTopHeight = mHeights[lRowIndex + 1, lColIndex + 1];

		float lPositionHeight;
		if ((lRowOffset + lColOffset) < vTileScale)
		{
			float lRowVector = lLeftTopHeight - lLeftBottomHeight;
			float lColVector = lRightBottomHeight - lLeftBottomHeight;
			lPositionHeight
				= lLeftBottomHeight
				+ lRowVector * lRowOffset / vTileScale
				+ lColVector * lColOffset / vTileScale;
		}
		else
		{
			float lRowVector = lRightBottomHeight - lRightTopHeight;
			float lColVector = lLeftTopHeight - lRightTopHeight;
			lPositionHeight
				= lRightBottomHeight
				+ lRowVector * (1.0f - lRowOffset / vTileScale)
				+ lColVector * (1.0f - lColOffset / vTileScale);
		}

		return lPositionHeight;
		//return ((pPosition.x - ((int)(pPosition.x / 20.0f) * 20.0f)) / 0.1f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 높이를 얻습니다.
	public Vector3 SampleHeightPosition(Vector3 pPosition)
	{
		return new Vector3(
			pPosition.x,
			SampleHeight(pPosition),
			pPosition.z);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 디버깅 정보를 보여줍니다.
	private void _ShowDebugInfo()
	{
		float lDebugMaxHeight = vMaxHeight / mMaxHeightOnLoad;

		for (int iRow = 0; iRow < mRowCount; ++iRow)
			for (int iCol = 0; iCol < mColCount; ++iCol)
			{
				Vector3 lLeftBottomPosition = new Vector3(
					iCol * vTileScale,
					mHeights[iRow, iCol] * lDebugMaxHeight,
					iRow * vTileScale);
				Vector3 lRightBottomPosition = new Vector3(
					(iCol + 1) * vTileScale,
					mHeights[iRow, iCol + 1] * lDebugMaxHeight,
					iRow * vTileScale);
				Vector3 lLeftTopPosition = new Vector3(
					iCol * vTileScale,
					mHeights[iRow + 1, iCol] * lDebugMaxHeight,
					(iRow + 1) * vTileScale);

				float lLeftBottomColorFactor = mHeights[iRow, iCol] / mMaxHeightOnLoad;
				Color lLeftBottomColor = new Color(lLeftBottomColorFactor, lLeftBottomColorFactor, lLeftBottomColorFactor);

				Debug.DrawLine(lLeftBottomPosition, lRightBottomPosition, lLeftBottomColor, 0.0f, true);
				Debug.DrawLine(lLeftBottomPosition, lLeftTopPosition, lLeftBottomColor, 0.0f, true);
				Debug.DrawLine(lLeftTopPosition, lRightBottomPosition, lLeftBottomColor, 0.0f, true);
			}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsMeshCollider;
	private int mRowCount;
	private int mColCount;
	private float[,] mHeights;
	private float mMaxHeightOnLoad;

	private Vector3 mMeshColliderRayOrigin;
	private Vector3 mMeshColliderRayDirection;
	private float mMeshColliderRayMaxDistance;
	private int mMeshColliderRayLayerMask;
}
