﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapLodControl : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vPlaneMap;
	public GameObject vTerrainMap;
	public GameObject vTerrainTextureMap;
	public List<GameObject> vPlaneMapHideList;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 근접 맵
	public void SetTerrainMap()
	{
		if (vPlaneMap != null)
			vPlaneMap.SetActive(false);

		if (vTerrainMap != null)
			vTerrainMap.SetActive(true);

		if(vTerrainTextureMap != null)
			vTerrainTextureMap.SetActive(true);

		for (int iObject = 0; iObject < vPlaneMapHideList.Count; iObject++)
			if (vPlaneMapHideList[iObject] != null)
				vPlaneMapHideList[iObject].SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전체 지도
	public void SetPlaneMap()
	{
		for (int iObject = 0; iObject < vPlaneMapHideList.Count; iObject++)
			if (vPlaneMapHideList[iObject] != null)
				vPlaneMapHideList[iObject].SetActive(false);

		if (vTerrainMap != null)
			vTerrainMap.SetActive(false);

		if (vTerrainTextureMap != null)
			vTerrainTextureMap.SetActive(false);

		if (vPlaneMap != null)
			vPlaneMap.SetActive(true);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
