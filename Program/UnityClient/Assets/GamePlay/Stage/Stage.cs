using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Stage : MonoBehaviour, IStage
{
	public enum ActorPoolType
	{
		Alpha,
		Real,
	}

	public readonly bool cIsEnableBattleHeadquarterPooling = true;
	public readonly bool cIsEnableBattleCheckPointPooling = true;
	public readonly bool cIsEnableBattleEventPointPooling = true;
	public readonly bool cIsEnableBattleBarricadePooling = true;
	public readonly bool cIsEnableBattleCommanderPooling = true;
	public readonly bool cIsEnableBattleTroopPooling = true;
	public readonly bool cIsEnableBattleObserverDummyPooling = true;
	public readonly bool cIsEnableBattleTargetingDummyPooling = true;
	public readonly bool cIsEnableBattleFireEffectPooling = true;
	public readonly bool cIsEnableBattleTroopSkillEffectPooling = true;
	public readonly bool cIsEnableBattleGridSkillEffectPooling = true;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public StageTouchControl vStageTouchControl;

	public StageHeadquarter vStageHeadquarterPrefab;
	public StageCheckPoint vStageCheckPointPrefab;
	public StageEventPoint vStageEventPointPrefab;
	public StageBarricade vStageBarricadePrefab;
	public StageCommander vStageCommanderPrefab;
	public StageTroop vStageTroopPrefab;
	public StageObserverDummy vStageObserverDummyPrefab;
	public StageTargetingDummy vStageTargetingDummyPrefab;
	public StageFireEffect vStageFireEffectPrefab;
	public StageTroopSkillEffect vStageTroopSkillEffectPrefab;
	public StageGridSkillEffect vStageGridSkillEffectPrefab;

	public Transform vGridCellRoot;
	public Transform vHeadquarterRoot;
	public Transform vCheckPointRoot;
	public Transform vEventPointRoot;
	public Transform vBarricadeRoot;
	public Transform vCommanderRoot;
	public Transform vTroopRoot;
	public Transform vObserverDummyRoot;
	public Transform vTargetingDummyRoot;
	public Transform vFireEffectRoot;
	public Transform vTroopSkillEffectRoot;
	public Transform vGridSkillEffectRoot;

	public GameObject vMapTemplate;

	public AssetInfo vActorPoolAssetInfo;

	public EffectPool vEffectPool;

	public Transform vUIWidgetRoot;

	public Transform vStageOverlayCameraRoot;

	public bool vIsShowTileDebugInfo = true;
	public Color[] vGroundTypeDebugColors = new Color[GroundSpec.cTypeCount];
	//{
	//	ColorUtil.Color3ub(128, 255, 128), // Grass
	//	ColorUtil.Color3ub(255, 106, 128), // Land
	//	ColorUtil.Color3ub(255, 255, 255), // Empty,
	//	ColorUtil.Color3ub(255, 128, 128), // Block,
	//	};
	public Color vGroundTroopBlockDebugColor = ColorUtil.Color3ub(255, 128, 128);
	public Color vEnteringTroopLineColor = ColorUtil.Color3ub(255, 255, 128);

	public float vLodDistance = 175.0f;

	public bool vIsShowCellDebugInfo = false;
	public Color vCellAreaDebugColor = ColorUtil.Color3ub(255, 255, 128);

	public bool vIsHideMap = false;
	public bool vIsHideActor = false;
	public bool vIsDebugShowActor = false;
	public bool vIsMinimalGraphics = false;

	public int vObserverTeamIdx = -1;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 풀
	public IActorPool aActorPool
	{
		get { return mActorPool; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 맵 LOD 컨트롤
	public MapLodControl aMapLodControl
	{
		get { return mMapLodControl; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 맵 높이
	public MapHeight aMapHeight
	{
		get { return mMapHeight; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vStageTouchControl == null)
			Debug.LogError("vStageTouchControl is empty - " + this);

		if (vStageHeadquarterPrefab == null)
			Debug.LogError("vStageHeadquarterPrefab is empty - " + this);
		if (vStageCheckPointPrefab == null)
			Debug.LogError("vStageCheckPointPrefab is empty - " + this);
		if (vStageEventPointPrefab == null)
			Debug.LogError("vStageEventPointPrefab is empty - " + this);
		if (vStageBarricadePrefab == null)
			Debug.LogError("vStageBarricadePrefab is empty - " + this);
		if (vStageCommanderPrefab == null)
			Debug.LogError("vStageCommanderPrefab is empty - " + this);
		if (vStageTroopPrefab == null)
			Debug.LogError("vStageTroopPrefab is empty - " + this);
		if (vStageObserverDummyPrefab == null)
			Debug.LogError("vStageObserverDummyPrefab is empty - " + this);
		if (vStageTargetingDummyPrefab == null)
			Debug.LogError("vStageTargetingDummyPrefab is empty - " + this);
		if (vStageFireEffectPrefab == null)
			Debug.LogError("vStageFireEffectPrefab is empty - " + this);
		if (vStageTroopSkillEffectPrefab == null)
			Debug.LogError("vStageTroopSkillEffectPrefab is empty - " + this);
		if (vStageGridSkillEffectPrefab == null)
			Debug.LogError("vStageGridSkillEffectPrefab is empty - " + this);

		if (vEffectPool == null)
			Debug.LogError("vEffectPool is empty - " + this);

		if (vUIWidgetRoot == null)
			Debug.LogError("vUIWidgetRoot is empty - " + this);

		if (vStageOverlayCameraRoot == null)
			Debug.LogError("vStageOverlayCameraRoot is empty - " + this);

		if (vGroundTypeDebugColors.Length != GroundSpec.cTypeCount)
			Debug.LogError("vGroundTypeDebugColors.Length mismatch - " + this);

		mTransform = transform;

		_LoadActorPool();

		mStageHeadquarterPoolingQueue = new Queue<StageHeadquarter>();
		mStageCheckPointPoolingQueue = new Queue<StageCheckPoint>();
		mStageEventPointPoolingQueue = new Queue<StageEventPoint>();
		mStageBarricadePoolingQueue = new Queue<StageBarricade>();
		mStageCommanderPoolingQueue = new Queue<StageCommander>();
		mStageTroopPoolingQueue = new Queue<StageTroop>();
		mStageObserverDummyPoolingQueue = new Queue<StageObserverDummy>();
		mStageTargetingDummyPoolingQueue = new Queue<StageTargetingDummy>();
		mStageFireEffectPoolingQueue = new Queue<StageFireEffect>();
		mStageTroopSkillEffectPoolingQueue = new Queue<StageTroopSkillEffect>();
		mStageGridSkillEffectPoolingQueue = new Queue<StageGridSkillEffect>();

		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
// 	void Start()
// 	{
// 		//Debug.Log("Start() - " + this);
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
#if UNITY_EDITOR
	void Update()
	{
		if (BattlePlay.main.aBattleGrid != null)
		{
			if (vIsShowCellDebugInfo)
				_ShowCellDebugInfo(BattlePlay.main.aBattleGrid);
			if (vIsShowTileDebugInfo)
				_ShowTileDebugInfo(BattlePlay.main.aBattleGrid);
		}
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 맵을 로딩합니다.
	public void LoadMap(AssetKey pMapAssetKey)
	{
		if (vIsHideMap)
			return;

		if (vMapTemplate != null)
			GameObject.Destroy(vMapTemplate); // 정식으로 맵을 로딩하므로 템플릿 맵은 숨깁니다.

		GameObject lMapRootObject = AssetManager.get.InstantiateObject<GameObject>(pMapAssetKey); // 맵을 로딩해서
		lMapRootObject.transform.parent = mTransform; // 스테이지 아래에 둡니다.

		mMapLodControl = lMapRootObject.GetComponent<MapLodControl>();
		_OnZoomChanged(); // 맵을 로딩할 때 거리를 감안해서 LOD가 적용되어야 함

//lMapRootObject.AddComponent<MapHeight>();
		mMapHeight = lMapRootObject.GetComponent<MapHeight>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지 중심을 지정합니다.
	public void SetViewCenter(Coord2 pViewCenter)
	{
		Vector3 lOverlayCameraposition = vStageOverlayCameraRoot.position;
		lOverlayCameraposition.x = pViewCenter.x * Coord2.cToWorld;
		lOverlayCameraposition.z = pViewCenter.z * Coord2.cToWorld;
		vStageOverlayCameraRoot.position = lOverlayCameraposition;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageHeadquarter CreateHeadquarter(BattleHeadquarter pBattleHeadquarter)
	{
		StageHeadquarter lStageHeadquarter;
		if (mStageHeadquarterPoolingQueue.Count <= 0)
		{
			lStageHeadquarter = ObjectUtil.InstantiateComponentObject<StageHeadquarter>(vStageHeadquarterPrefab);
			lStageHeadquarter.aTransform.parent = vHeadquarterRoot;
		}
		else
		{
			lStageHeadquarter = mStageHeadquarterPoolingQueue.Dequeue();
			lStageHeadquarter.gameObject.SetActive(true);
		}

		lStageHeadquarter.OnCreateHeadquarter(this, pBattleHeadquarter);

		return lStageHeadquarter;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyHeadquarter(StageHeadquarter pStageHeadquarter)
	{
		if (cIsEnableBattleHeadquarterPooling)
		{
			pStageHeadquarter.gameObject.SetActive(false);
			mStageHeadquarterPoolingQueue.Enqueue(pStageHeadquarter);
		}
		else
			GameObject.DestroyObject(pStageHeadquarter.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageCheckPoint CreateCheckPoint(BattleCheckPoint pBattleCheckPoint)
	{
		StageCheckPoint lStageCheckPoint;
		if (mStageCheckPointPoolingQueue.Count <= 0)
		{
			lStageCheckPoint = ObjectUtil.InstantiateComponentObject<StageCheckPoint>(vStageCheckPointPrefab);
			lStageCheckPoint.aTransform.parent = vCheckPointRoot;
		}
		else
		{
			lStageCheckPoint = mStageCheckPointPoolingQueue.Dequeue();
			lStageCheckPoint.gameObject.SetActive(true);
		}

		lStageCheckPoint.OnCreateCheckPoint(this, pBattleCheckPoint);

		return lStageCheckPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCheckPoint(StageCheckPoint pStageCheckPoint)
	{
		if (cIsEnableBattleCheckPointPooling)
		{
			pStageCheckPoint.gameObject.SetActive(false);
			mStageCheckPointPoolingQueue.Enqueue(pStageCheckPoint);
		}
		else
			GameObject.DestroyObject(pStageCheckPoint.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageEventPoint CreateEventPoint(BattleEventPoint pBattleEventPoint)
	{
		StageEventPoint lStageEventPoint;
		if (mStageEventPointPoolingQueue.Count <= 0)
		{
			lStageEventPoint = ObjectUtil.InstantiateComponentObject<StageEventPoint>(vStageEventPointPrefab);
			lStageEventPoint.aTransform.parent = vEventPointRoot;
		}
		else
		{
			lStageEventPoint = mStageEventPointPoolingQueue.Dequeue();
			lStageEventPoint.gameObject.SetActive(true);
		}

		lStageEventPoint.OnCreateEventPoint(this, pBattleEventPoint);

		return lStageEventPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyEventPoint(StageEventPoint pStageEventPoint)
	{
		if (cIsEnableBattleEventPointPooling)
		{
			pStageEventPoint.gameObject.SetActive(false);
			mStageEventPointPoolingQueue.Enqueue(pStageEventPoint);
		}
		else
			GameObject.DestroyObject(pStageEventPoint.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageBarricade CreateBarricade(BattleBarricade pBattleBarricade)
	{
		StageBarricade lStageBarricade;
		if (mStageBarricadePoolingQueue.Count <= 0)
		{
			lStageBarricade = ObjectUtil.InstantiateComponentObject<StageBarricade>(vStageBarricadePrefab);
			lStageBarricade.aTransform.parent = vBarricadeRoot;
		}
		else
		{
			lStageBarricade = mStageBarricadePoolingQueue.Dequeue();
			lStageBarricade.gameObject.SetActive(true);
		}

		lStageBarricade.OnCreateBarricade(this, pBattleBarricade);

		return lStageBarricade;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyBarricade(StageBarricade pStageBarricade)
	{
		if (cIsEnableBattleBarricadePooling)
		{
			pStageBarricade.gameObject.SetActive(false);
			mStageBarricadePoolingQueue.Enqueue(pStageBarricade);
		}
		else
			GameObject.DestroyObject(pStageBarricade.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageCommander CreateCommander(BattleCommander pBattleCommander)
	{
		StageCommander lStageCommander;
		if (mStageCommanderPoolingQueue.Count <= 0)
		{
			lStageCommander = ObjectUtil.InstantiateComponentObject<StageCommander>(vStageCommanderPrefab);
			lStageCommander.aTransform.parent = vCommanderRoot;
		}
		else
		{
			lStageCommander = mStageCommanderPoolingQueue.Dequeue();
			lStageCommander.gameObject.SetActive(true);
		}

		lStageCommander.OnCreateCommander(this, pBattleCommander);

		return lStageCommander;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyCommander(StageCommander pStageCommander)
	{
		if (cIsEnableBattleCommanderPooling)
		{
			pStageCommander.gameObject.SetActive(false);
			mStageCommanderPoolingQueue.Enqueue(pStageCommander);
		}
		else
			GameObject.DestroyObject(pStageCommander.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageTroop CreateTroop(BattleTroop pBattleTroop)
	{
		StageTroop lStageTroop;
		if (mStageTroopPoolingQueue.Count <= 0)
		{
			lStageTroop = ObjectUtil.InstantiateComponentObject<StageTroop>(vStageTroopPrefab);
			lStageTroop.aTransform.parent = vTroopRoot;
		}
		else
		{
			lStageTroop = mStageTroopPoolingQueue.Dequeue();
			lStageTroop.gameObject.SetActive(true);
		}

		lStageTroop.OnCreateTroop(this, pBattleTroop);

		return lStageTroop;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroop(StageTroop pStageTroop)
	{
		if (cIsEnableBattleTroopPooling)
		{
			pStageTroop.gameObject.SetActive(false);
			mStageTroopPoolingQueue.Enqueue(pStageTroop);
		}
		else
			GameObject.DestroyObject(pStageTroop.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageObserverDummy CreateObserverDummy(BattleObserverDummy pBattleObserverDummy)
	{
		StageObserverDummy lStageObserverDummy;
		if (mStageObserverDummyPoolingQueue.Count <= 0)
		{
			lStageObserverDummy = ObjectUtil.InstantiateComponentObject<StageObserverDummy>(vStageObserverDummyPrefab);
			lStageObserverDummy.aTransform.parent = vObserverDummyRoot;
		}
		else
		{
			lStageObserverDummy = mStageObserverDummyPoolingQueue.Dequeue();
			lStageObserverDummy.gameObject.SetActive(true);
		}

		lStageObserverDummy.OnCreateObserverDummy(this, pBattleObserverDummy);

		return lStageObserverDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyObserverDummy(StageObserverDummy pStageObserverDummy)
	{
		if (cIsEnableBattleObserverDummyPooling)
		{
			pStageObserverDummy.gameObject.SetActive(false);
			mStageObserverDummyPoolingQueue.Enqueue(pStageObserverDummy);
		}
		else
			GameObject.DestroyObject(pStageObserverDummy.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageTargetingDummy CreateTargetingDummy(BattleTargetingDummy pBattleTargetingDummy)
	{
		StageTargetingDummy lStageTargetingDummy;
		if (mStageTargetingDummyPoolingQueue.Count <= 0)
		{
			lStageTargetingDummy = ObjectUtil.InstantiateComponentObject<StageTargetingDummy>(vStageTargetingDummyPrefab);
			lStageTargetingDummy.aTransform.parent = vTargetingDummyRoot;
		}
		else
		{
			lStageTargetingDummy = mStageTargetingDummyPoolingQueue.Dequeue();
			lStageTargetingDummy.gameObject.SetActive(true);
		}

		lStageTargetingDummy.OnCreateTargetingDummy(this, pBattleTargetingDummy);

		return lStageTargetingDummy;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTargetingDummy(StageTargetingDummy pStageTargetingDummy)
	{
		if (cIsEnableBattleTargetingDummyPooling)
		{
			pStageTargetingDummy.gameObject.SetActive(false);
			mStageTargetingDummyPoolingQueue.Enqueue(pStageTargetingDummy);
		}
		else
			GameObject.DestroyObject(pStageTargetingDummy.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageFireEffect CreateFireEffect(BattleFireEffect pBattleFireEffect)
	{
		StageFireEffect lStageFireEffect;
		if (mStageFireEffectPoolingQueue.Count <= 0)
		{
			lStageFireEffect = ObjectUtil.InstantiateComponentObject<StageFireEffect>(vStageFireEffectPrefab);
			lStageFireEffect.aTransform.parent = vFireEffectRoot;
		}
		else
		{
			lStageFireEffect = mStageFireEffectPoolingQueue.Dequeue();
			lStageFireEffect.gameObject.SetActive(true);
		}

		lStageFireEffect.OnCreateFireEffect(this, pBattleFireEffect);

		return lStageFireEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyFireEffect(StageFireEffect pStageFireEffect)
	{
		if (cIsEnableBattleFireEffectPooling)
		{
			pStageFireEffect.gameObject.SetActive(false);
			mStageFireEffectPoolingQueue.Enqueue(pStageFireEffect);
		}
		else
			GameObject.DestroyObject(pStageFireEffect.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageTroopSkillEffect CreateTroopSkillEffect(BattleTroopSkillEffect pBattleTroopSkillEffect)
	{
		StageTroopSkillEffect lStageTroopSkillEffect;
		if (mStageTroopSkillEffectPoolingQueue.Count <= 0)
		{
			lStageTroopSkillEffect = ObjectUtil.InstantiateComponentObject<StageTroopSkillEffect>(vStageTroopSkillEffectPrefab);
			lStageTroopSkillEffect.aTransform.parent = vTroopSkillEffectRoot;
		}
		else
		{
			lStageTroopSkillEffect = mStageTroopSkillEffectPoolingQueue.Dequeue();
			lStageTroopSkillEffect.gameObject.SetActive(true);
		}

		lStageTroopSkillEffect.OnCreateTroopSkillEffect(this, pBattleTroopSkillEffect);

		return lStageTroopSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyTroopSkillEffect(StageTroopSkillEffect pStageTroopSkillEffect)
	{
		if (cIsEnableBattleTroopSkillEffectPooling)
		{
			pStageTroopSkillEffect.gameObject.SetActive(false);
			mStageTroopSkillEffectPoolingQueue.Enqueue(pStageTroopSkillEffect);
		}
		else
			GameObject.DestroyObject(pStageTroopSkillEffect.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStage 메서드
	public IStageGridSkillEffect CreateGridSkillEffect(BattleGridSkillEffect pBattleGridSkillEffect, int pDirectionY)
	{
		StageGridSkillEffect lStageGridSkillEffect;
		if (mStageGridSkillEffectPoolingQueue.Count <= 0)
		{
			lStageGridSkillEffect = ObjectUtil.InstantiateComponentObject<StageGridSkillEffect>(vStageGridSkillEffectPrefab);
			lStageGridSkillEffect.aTransform.parent = vGridSkillEffectRoot;
		}
		else
		{
			lStageGridSkillEffect = mStageGridSkillEffectPoolingQueue.Dequeue();
			lStageGridSkillEffect.gameObject.SetActive(true);
		}

		lStageGridSkillEffect.OnCreateGridSkillEffect(this, pBattleGridSkillEffect, pDirectionY);

		return lStageGridSkillEffect;
	}
	//-------------------------------------------------------------------------------------------------------
	// 풀링 객체를 파괴합니다.
	public void DestroyGridSkillEffect(StageGridSkillEffect pStageGridSkillEffect)
	{
		if (cIsEnableBattleGridSkillEffectPooling)
		{
			pStageGridSkillEffect.gameObject.SetActive(false);
			mStageGridSkillEffectPoolingQueue.Enqueue(pStageGridSkillEffect);
		}
		else
			GameObject.DestroyObject(pStageGridSkillEffect.gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 위젯 깊이를 정렬합니다.
	public void UpdateUIWidgetDepth()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 터레인 좌표를 얻습니다.
	public Vector3 GetTerrainPosition(Vector3 pPosition)
	{
		if (mMapHeight != null)
			return mMapHeight.SampleHeightPosition(pPosition);
		else
			return pPosition;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 액터 풀을 로딩합니다.
	private void _LoadActorPool()
	{
		AssetKey lActorPoolAssetKey = AssetManager.get.CreateAssetKey(vActorPoolAssetInfo);
		if (!AssetKey.IsValid(lActorPoolAssetKey))
			Debug.LogError("invalid asset key '" + lActorPoolAssetKey + "' - " + this);
		GameObject lActorPoolObject = AssetManager.get.InstantiateObject<GameObject>(lActorPoolAssetKey);
		lActorPoolObject.transform.parent = mTransform;
		mActorPool = _FindActorManager(lActorPoolObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 액터 풀 오브젝트에서 액터 풀 인터페이스를 찾습니다.
	private IActorPool _FindActorManager(GameObject pActorPoolObject)
	{
		RealActorPool lRealActorPool = pActorPoolObject.GetComponent<RealActorPool>();
		if (lRealActorPool != null)
			return lRealActorPool;

		AlphaActorPool lAlphaActorPool = pActorPoolObject.GetComponent<AlphaActorPool>();
		if (lAlphaActorPool != null)
			return lAlphaActorPool;

		Debug.LogError("can't find IActorPool in " + pActorPoolObject + " - " + this);
		return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// QuaterViewCameraSet.OnZoomChangedDelegate
	private void _OnZoomChanged()
	{
		if (mMapLodControl == null)
			return;

		float lViewDist = BattlePlay.main.vControlPanel.vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance;
		//Debug.Log("lView Dist - " + lViewDist + "/Actor - " + vActorManagerIndex);

		if (lViewDist <= vLodDistance)
			mMapLodControl.SetTerrainMap();
		else
			mMapLodControl.SetPlaneMap();
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일의 디버깅 정보를 보여줍니다.
	private void _ShowTileDebugInfo(BattleGrid pBattleGrid)
	{
		Vector3 lTileWorldScale = new Vector3(
			pBattleGrid.aTileScaleX * Coord2.cToWorld,
			1.0f,
			pBattleGrid.aTileScaleZ * Coord2.cToWorld);
		Vector3 lHalfTileWorldScale = lTileWorldScale * 0.5f;
		Vector3 lTileAreaMargin = lTileWorldScale * 0.025f;
		Vector3 lTileAreaOffset = lHalfTileWorldScale - lTileAreaMargin;
		Vector3 lTileAreaScale = lTileAreaOffset * 2.0f;

		for (int iRow = 0; iRow < pBattleGrid.aTileRowCount; ++iRow)
		{
			for (int iCol = 0; iCol < pBattleGrid.aTileColCount; ++iCol)
			{
				BattleGridTile lTile = pBattleGrid.aTiles[iRow, iCol];

				Vector3 lTilePosition = Coord2.ToWorld(lTile.aCenterCoord);
				Rect lTileArea = new Rect(
					lTilePosition.x - lTileAreaOffset.x,
					lTilePosition.z - lTileAreaOffset.z,
					lTileAreaScale.x,
					lTileAreaScale.z);

				DebugUtil.DrawPlaneY(lTileArea, lTilePosition.y, vGroundTypeDebugColors[(int)lTile.aGroundSpec.mType], 0.0f, false);
				if (lTile.aOccupyingTroopList.Count > 0)
				{
					Debug.DrawLine(
						new Vector3(lTilePosition.x - lTileAreaOffset.x, lTilePosition.y, lTilePosition.z - lTileAreaOffset.z),
						new Vector3(lTilePosition.x + lTileAreaOffset.x, lTilePosition.y, lTilePosition.z + lTileAreaOffset.z),
						vGroundTroopBlockDebugColor);
					Debug.DrawLine(
						new Vector3(lTilePosition.x - lTileAreaOffset.x, lTilePosition.y, lTilePosition.z + lTileAreaOffset.z),
						new Vector3(lTilePosition.x + lTileAreaOffset.x, lTilePosition.y, lTilePosition.z - lTileAreaOffset.z),
						vGroundTroopBlockDebugColor);
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 셀의 디버깅 정보를 보여줍니다.
	private void _ShowCellDebugInfo(BattleGrid pBattleGrid)
	{
		Vector3 lCellWorldScale = new Vector3(
			pBattleGrid.aCellScaleX * Coord2.cToWorld,
			1.0f,
			pBattleGrid.aCellScaleZ * Coord2.cToWorld);
		Vector3 lHalfCellWorldScale = lCellWorldScale * 0.5f;
		Vector3 lCellAreaMargin = lCellWorldScale * 0.025f;
		Vector3 lCellAreaOffset = lHalfCellWorldScale - lCellAreaMargin;
		Vector3 lCellAreaScale = lCellAreaOffset * 2.0f;

		for (int iRow = 0; iRow < pBattleGrid.aCellRowCount; ++iRow)
		{
			for (int iCol = 0; iCol < pBattleGrid.aCellColCount; ++iCol)
			{
				Vector3 lCellPosition = Coord2.ToWorld(pBattleGrid.aCells[iRow, iCol].aCenterCoord);
				Rect lCellArea = new Rect(
					lCellPosition.x - lCellAreaOffset.x,
					lCellPosition.z - lCellAreaOffset.z,
					lCellAreaScale.x,
					lCellAreaScale.z);
				DebugUtil.DrawPlaneY(lCellArea, lCellPosition.y, vCellAreaDebugColor, 0.0f, false);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;

	private Queue<StageHeadquarter> mStageHeadquarterPoolingQueue;
	private Queue<StageCheckPoint> mStageCheckPointPoolingQueue;
	private Queue<StageEventPoint> mStageEventPointPoolingQueue;
	private Queue<StageBarricade> mStageBarricadePoolingQueue;
	private Queue<StageCommander> mStageCommanderPoolingQueue;
	private Queue<StageTroop> mStageTroopPoolingQueue;
	private Queue<StageObserverDummy> mStageObserverDummyPoolingQueue;
	private Queue<StageTargetingDummy> mStageTargetingDummyPoolingQueue;
	private Queue<StageFireEffect> mStageFireEffectPoolingQueue;
	private Queue<StageTroopSkillEffect> mStageTroopSkillEffectPoolingQueue;
	private Queue<StageGridSkillEffect> mStageGridSkillEffectPoolingQueue;

	private IActorPool mActorPool;

	private MapLodControl mMapLodControl;
	private MapHeight mMapHeight;
}
