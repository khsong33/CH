using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageFireEffect : StageEffect, IStageFireEffect
{
	public const float cShowEffectRangeDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public tk2dSprite vGroundEffectRangeSprite;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 전투 효과
	public BattleFireEffect aBattleFireEffect
	{
		get { return mBattleFireEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public override void DestroyEffect()
	{
		if (!vGroundEffectRangeSprite.gameObject.activeInHierarchy) // 지면 효과 범위를 보여주는 도중이 아니라면
			base.DestroyEffect(); // 파괴합니다.
		else
			mIsToBeDestroyed = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnNewEffect()
	{
		if (vGroundEffectRangeSprite == null)
			Debug.LogError("vGroundEffectRangeSprite is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateFireEffect(Stage pStage, BattleFireEffect pBattleFireEffect)
	{
		OnCreateEffect(pStage);

		mBattleFireEffect = pBattleFireEffect;

		aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(pBattleFireEffect.aHitCoord));

		vGroundEffectRangeSprite.gameObject.SetActive(false);
		mIsToBeDestroyed = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		aStage.DestroyFireEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate(Coord2 pActivationCoord)
	{
		//Debug.Log("Activate() - " + this);

		if ((mBattleFireEffect.aHitResult == TroopHitResult.GroundHit) &&
			(mBattleFireEffect.aFireWeaponData.aMaxAoeRadius > 0))
		{
			StartCoroutine(_ShowGroundEffectRange());
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 지면 효과 범위를 보입니다.
	private IEnumerator _ShowGroundEffectRange()
	{
		float lGroundEffecrRange = mBattleFireEffect.aFireWeaponData.aMaxAoeRadius * Coord2.cToWorld;
		//Debug.Log("lGroundEffecrRange=" + lGroundEffecrRange);
		vGroundEffectRangeSprite.scale = new Vector3(lGroundEffecrRange, lGroundEffecrRange, lGroundEffecrRange);
		vGroundEffectRangeSprite.gameObject.SetActive(true);

		float lShowEffectRangeTimer = cShowEffectRangeDelay;
		do
		{
			float lFactorX = 1.0f - lShowEffectRangeTimer / cShowEffectRangeDelay;
			float lSpriteAlpha = -lFactorX * lFactorX + 1.0f;
			ColorLib.SetSpriteAlpha(vGroundEffectRangeSprite, lSpriteAlpha);

			lShowEffectRangeTimer -= Time.deltaTime;
			yield return null;

		} while (lShowEffectRangeTimer >= 0.0f);

		if (mIsToBeDestroyed) // 파괴가 예정되어 있다면
			base.DestroyEffect(); // 효과 범위가 사라졌으니 파괴합니다.
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleFireEffect mBattleFireEffect;

	private bool mIsToBeDestroyed;
}
