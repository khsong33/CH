using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageGridSkillEffect : StageEffect, IStageGridSkillEffect
{
	public const float cShowEffectRangeDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vActionEffectRoot;
	public Transform[] vEffectTypeObjectRoots;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과
	public BattleGridSkillEffect aBattleGridSkillEffect
	{
		get { return mBattleGridSkillEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
// 	public override void DestroyEffect()
// 	{
// 		base.DestroyEffect();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnNewEffect()
	{
		vActionEffectRoot.gameObject.SetActive(false);
		for (int iObject = 0; iObject < vEffectTypeObjectRoots.Length; ++iObject)
			if (vEffectTypeObjectRoots[iObject] != null)
				vEffectTypeObjectRoots[iObject].gameObject.SetActive(false);
		mActiveEffectTypeObjectIdx = -1;
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateGridSkillEffect(Stage pStage, BattleGridSkillEffect pBattleGridSkillEffect, int pDirectionY)
	{
		OnCreateEffect(pStage);

		mBattleGridSkillEffect = pBattleGridSkillEffect;
		mDirectionY = pDirectionY;

		aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(pBattleGridSkillEffect.aEffectGridCoord));

		if ((vActionEffectRoot != null) &&
			(pBattleGridSkillEffect.aActivationDelay > 0))
			vActionEffectRoot.gameObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		if (mActiveEffectTypeObjectIdx >= 0)
		{
			vEffectTypeObjectRoots[mActiveEffectTypeObjectIdx].gameObject.SetActive(false);
			mActiveEffectTypeObjectIdx = -1;
		}

		aStage.DestroyGridSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate()
	{
		//Debug.Log("Activate() - " + this);

		if (vActionEffectRoot != null)
			vActionEffectRoot.gameObject.SetActive(false);

		if (mBattleGridSkillEffect.aGroundEffectSpec != null)
		{
			for (int iRoot = 0; iRoot < vEffectTypeObjectRoots.Length; ++iRoot)
			{
				if ((vEffectTypeObjectRoots[iRoot] != null) &&
					(vEffectTypeObjectRoots[iRoot].gameObject.name == mBattleGridSkillEffect.aGroundEffectSpec.mEffectType))
				{
					mActiveEffectTypeObjectIdx = iRoot;
					vEffectTypeObjectRoots[mActiveEffectTypeObjectIdx].gameObject.SetActive(true);
					vEffectTypeObjectRoots[mActiveEffectTypeObjectIdx].rotation = MathLib.GetRotation(mDirectionY);
					break;
				}
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleGridSkillEffect mBattleGridSkillEffect;
	private int mDirectionY;

	private int mActiveEffectTypeObjectIdx;
}
