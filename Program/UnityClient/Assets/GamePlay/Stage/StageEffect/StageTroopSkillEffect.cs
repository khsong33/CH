using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroopSkillEffect : StageEffect, IStageTroopSkillEffect
{
	public const float cShowEffectRangeDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스킬 효과
	public BattleTroopSkillEffect aBattleTroopSkillEffect
	{
		get { return mBattleTroopSkillEffect; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		BattleTroop lEffectTroop = mBattleTroopSkillEffect.aEffectTroop;
		if (lEffectTroop != null)
			aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(lEffectTroop.aCoord));
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
// 	public override void DestroyEffect()
// 	{
// 		base.DestroyEffect();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
// 	protected override void OnNewEffect()
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	public void OnCreateTroopSkillEffect(Stage pStage, BattleTroopSkillEffect pBattleTroopSkillEffect)
	{
		OnCreateEffect(pStage);

		mBattleTroopSkillEffect = pBattleTroopSkillEffect;

		aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(pBattleTroopSkillEffect.aEffectTroop.aCoord));
	}
	//-------------------------------------------------------------------------------------------------------
	// StageEffect 메서드
	protected override void OnDestroyEffect()
	{
		aStage.DestroyTroopSkillEffect(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEffect 메서드
	public void Activate()
	{
		//Debug.Log("Activate() - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroopSkillEffect mBattleTroopSkillEffect;
}
