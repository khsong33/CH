using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageCheckPoint : StageObject, IStageCheckPoint
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public uint vGuid = 0;

	public Transform vCaptureRangeTransform;
	public float vCaptureRangeScale = 2.5f;

	public Transform vFlagDownTransform;
	public Transform vFlagUpTransform;
	public Transform vFlagTransform;
	public tk2dSprite vFlagSprite;
	public Transform vFlagMarkTransform;
	public tk2dSprite vFlagMarkSprite;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleCheckPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleCheckPoint aBattleCheckPoint
	{
		get { return mBattleCheckPoint; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();

		if (vCaptureRangeTransform == null)
			Debug.Log("vCaptureRangeTransform is empty - " + this);

		if (vFlagDownTransform == null)
			Debug.Log("vFlagDownTransform is empty - " + this);
		if (vFlagUpTransform == null)
			Debug.Log("vFlagUpTransform is empty - " + this);
		if (vFlagSprite == null)
			Debug.Log("vFlagSprite is empty - " + this);
		if (vFlagMarkSprite == null)
			Debug.Log("vFlagMarkSparite is empty - " + this);

		mCaptureSpriteId = vFlagMarkSprite.GetSpriteIdByName("capture_icon");
		mRecruitSpriteId = vFlagMarkSprite.GetSpriteIdByName("recovery_icon");
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateCheckPoint(Stage pStage, BattleCheckPoint pBattleCheckPoint)
	{
		vGuid = pBattleCheckPoint.aGuid;

		OnCreateObject(
			pStage,
			pBattleCheckPoint.aCoord,
			0);//pBattleCheckPoint.aDirectionY);

		mBattleCheckPoint = pBattleCheckPoint;
//		mDebugBattleCheckPointIdx = mBattleCheckPoint.aCheckPointIdx;
		float lCaptureRangeScale = BattleConfig.get.mCheckPointCaptureRange * (Coord2.cToWorld / vCaptureRangeScale);
		vCaptureRangeTransform.localScale = new Vector3(lCaptureRangeScale, 1.0f, lCaptureRangeScale);

		ResetTeamChange(pBattleCheckPoint.aTeamIdx);
		if (pBattleCheckPoint.aTeamIdx > 0)
			UpdateTeam(pBattleCheckPoint.aTeamIdx);

		vFlagMarkTransform.gameObject.SetActive(false);
		mCurrentFlagMarkSpriteId = -1;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyCheckPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void ResetTeamChange(int pTeamIdx)
	{
		//Debug.Log("ResetTeamChange() pTeamIdx=" + pTeamIdx);

		vFlagSprite.gameObject.SetActive(pTeamIdx > 0);

		if (pTeamIdx > 0)
			vFlagSprite.color = BattlePlay.main.vTeamColors[pTeamIdx];
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void TryTeamChange(int pTeamIdx, float pChangeFactor)
	{
		//Debug.Log("TryTeamChange() pTeamIdx=" + pTeamIdx + " pChangeFactor=" + pChangeFactor);

		_SetFlagFactor(pChangeFactor);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void UpdateTeam(int pTeamIdx)
	{
		//Debug.Log("UpdateTeam() pTeamIdx=" + pTeamIdx);

		vFlagSprite.gameObject.SetActive(pTeamIdx > 0);

		if (pTeamIdx > 0)
		{
			vFlagSprite.color = BattlePlay.main.vTeamColors[pTeamIdx];
			_SetFlagFactor(1.0f);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void SetSelected(bool pIsSelected, int pAdvanceDirectionId)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCheckPoint 메서드
	public void UpdateCommanderCaptureState(BattleCommander pUpdateCommander)
	{
		int lUpdateFlagSpriteId;
		if (pUpdateCommander != null)
		{
			if (pUpdateCommander.CheckRecruitable() &&
				(mBattleCheckPoint.IsRecruit(pUpdateCommander.aTeamIdx)))
				lUpdateFlagSpriteId = mRecruitSpriteId;
			else if (mBattleCheckPoint.IsConquer(pUpdateCommander.aTeamIdx))
				lUpdateFlagSpriteId = mCaptureSpriteId;
			else
				lUpdateFlagSpriteId = -1;
		}
		else
			lUpdateFlagSpriteId = -1;

		if (mCurrentFlagMarkSpriteId == lUpdateFlagSpriteId)
			return; // 변화 없음

		mCurrentFlagMarkSpriteId = lUpdateFlagSpriteId;

		if (vFlagMarkTransform.gameObject.activeInHierarchy != (mCurrentFlagMarkSpriteId >= 0))
			vFlagMarkTransform.gameObject.SetActive(mCurrentFlagMarkSpriteId >= 0);
		if (mCurrentFlagMarkSpriteId >= 0)
			vFlagMarkSprite.spriteId = mCurrentFlagMarkSpriteId;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 팀 변경 상태를 지정합니다.
	private void _SetFlagFactor(float pChangeFactor)
	{
		vFlagTransform.position = Vector3.Lerp(vFlagDownTransform.position, vFlagUpTransform.position, pChangeFactor);
		vFlagTransform.localRotation = Quaternion.Lerp(vFlagDownTransform.localRotation, vFlagUpTransform.localRotation, pChangeFactor);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCheckPoint mBattleCheckPoint;
//	private int mDebugBattleCheckPointIdx;

	private int mCaptureSpriteId;
	private int mRecruitSpriteId;
	private int mCurrentFlagMarkSpriteId;
}
