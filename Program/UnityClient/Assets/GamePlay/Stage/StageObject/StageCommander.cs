using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageCommander : StageObject, IStageCommander
{
	// 알람 표시의 우선순위별로 정렬합니다.
	public enum CommanderAlarm
	{
		Battle = 0,
		Conquer,
		Moveing,

		Count
	}
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleCommander; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleCommander aBattleCommander
	{
		get { return mBattleCommander; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	protected override void OnNewObject()
// 	{
// 		base.OnNewObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateCommander(Stage pStage, BattleCommander pBattleCommander)
	{
		OnCreateObject(
			pStage,
			pBattleCommander.aCoord,
			pBattleCommander.aDirectionY);

		mBattleCommander = pBattleCommander;

		// 미니맵에 추가합니다.
		mIsShowMiniMap = _CheckShowMiniMap();
		if (mIsShowMiniMap)
			BattlePlay.main.vControlPanel.vMiniMap.CreateCommander(mBattleCommander);
	}

	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		// 미니맵에서 뺍니다.
		if (mIsShowMiniMap)
			BattlePlay.main.vControlPanel.vMiniMap.DestroyCommander(mBattleCommander);

		aStage.DestroyCommander(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void SetSelected(bool pIsSelected)
	{
		// 하위 부대들의 선택 상태를 보입니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lSpawnTroop = mBattleCommander.aTroops[iTroop];
			if (lSpawnTroop != null)
				lSpawnTroop.aStageTroop.SetSelected(pIsSelected);
		}

		// 현재 상태의 컨트롤 커서를 보입니다.
// 		if (pIsSelected)
// 		{
// 			BattleTroop lExplicitTargetTroop = mBattleCommander.aExplicitTargetTroop;
// 			if (lExplicitTargetTroop != null)
// 				aStage.vTargetTroopCursor.ShowTargetTroop(mBattleCommander, lExplicitTargetTroop); // 타겟 부대 커서를 보입니다.
// 			else
// 				aStage.vTargetCoordCursor.ShowMoveCoord(mBattleCommander, mBattleCommander.aControlCoord, true, false); // 타겟 좌표 커서를 보입니다.
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		aTransform.position = Coord2.ToWorld(pCoord);
		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void OnMoveTo(Coord2 pCoord, bool pIsArrived)
	{
		aTransform.position = Coord2.ToWorld(mBattleCommander.aCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void OnRotateToDirectionY(int pDirectionY)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void OnUpdateTeam(int pTeamIdx)
	{
		// 미니맵에서 갱신합니다.
		if (mIsShowMiniMap)
			BattlePlay.main.vControlPanel.vMiniMap.DestroyCommander(mBattleCommander);
		mIsShowMiniMap = _CheckShowMiniMap();
		if (mIsShowMiniMap)
			BattlePlay.main.vControlPanel.vMiniMap.CreateCommander(mBattleCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void ShowState(StageCommanderState pCommanderState)
	{
		BattlePlay.main.vControlPanel.ShowCommanderState(mBattleCommander, pCommanderState);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageCommander 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; iTroop++)
		{
			BattleTroop lTroop = mBattleCommander.aTroops[iTroop];
			if (lTroop == null)
				continue;

			lTroop.aStageTroop.ShowRetreat(pIsRetreat);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 미니맵 표시 여부를 얻습니다.
	private bool _CheckShowMiniMap()
	{
		if ((mBattleCommander.aTeamUser != null) &&
			mBattleCommander.aCommanderItem.mCommanderItemType != CommanderItemType.DefenderCommander)
			return true;
		else
			return false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleCommander mBattleCommander;
	private Vector3 mLastLeaderPosition;
	private bool mIsShowMiniMap;
}
