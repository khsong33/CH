using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageEventPoint : StageObject, IStageEventPoint
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public uint vGuid = 0;

	public GameObject vMedKit;
	public GameObject vMunition;
	public GameObject vRepairKit;

	public bool vIsDebugShowActor = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleEventPoint; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleEventPoint aBattleEventPoint
	{
		get { return mBattleEventPoint; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();

		if (vMedKit == null)
			Debug.LogError("vMedKit is empty - " + this);
		if (vMunition == null)
			Debug.LogError("vMunition is empty - " + this);
		if (vRepairKit == null)
			Debug.LogError("vRepairKit is empty - " + this);

		vMedKit.gameObject.SetActive(false);
		vMunition.gameObject.SetActive(false);
		vRepairKit.gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateEventPoint(Stage pStage, BattleEventPoint pBattleEventPoint)
	{
		vGuid = pBattleEventPoint.aGuid;

		vIsDebugShowActor = pStage.vIsDebugShowActor;

		OnCreateObject(
			pStage,
			pBattleEventPoint.aCoord,
			pBattleEventPoint.aDirectionY);

		mBattleEventPoint = pBattleEventPoint;

		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;

		if (mEventObject != null)
		{
			DestroyObject(mEventObject);
			mEventObject = null;
		}

		aStage.DestroyEventPoint(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
		int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
		if (pDetectingTeamIdx != lObserverTeamIdx)
			return; // 이 클라이언트에 대해서만 처리

		bool lIsVisible = mBattleEventPoint.aIsDetecteds[lObserverTeamIdx] || vIsDebugShowActor;
		if (mEventObject != null)
			mEventObject.SetActive(lIsVisible);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void UpdateCoord(Coord2 pCoord)
	{
		aTransform.position = Coord2.ToWorld(pCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void OnTriggerReady()
	{
		if (mBattleEventPoint.aGroundEventSpec.mStageEventObjectAssetKey != null)
		{
			Transform lObjectTransform = AssetManager.get.InstantiateChildObject(mBattleEventPoint.aGroundEventSpec.mStageEventObjectAssetKey, aTransform);
			lObjectTransform.localPosition = Vector3.zero;
			mEventObject = lObjectTransform.gameObject;
		}

		_OnZoomChanged(); // 보이기 여부를 갱신
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageEventPoint 메서드
	public void OnTriggerActivated()
	{
		if (mEventObject != null)
		{
			mEventObject.SetActive(false);
			mEventObject = null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 줌이 변경될때 호출됩니다.
	private void _OnZoomChanged()
	{
		float lViewDist = BattlePlay.main.vControlPanel.vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance;
		//Debug.Log("lView Dist - " + lViewDist + "/Actor - " + vActorManagerIndex);
		bool lIsVisible = (lViewDist <= aStage.vLodDistance);

		int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
		bool lIsActive;
		if (!vIsDebugShowActor)
			lIsActive = mBattleEventPoint.aIsDetecteds[lObserverTeamIdx]; // 색적이 안 된 부대의 경우에는 계속 안보이도록
		else
			lIsActive = true;
		lIsActive &= lIsVisible;

		if (mEventObject != null)
			mEventObject.SetActive(lIsActive);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleEventPoint mBattleEventPoint;
	private GameObject mEventObject;
}
