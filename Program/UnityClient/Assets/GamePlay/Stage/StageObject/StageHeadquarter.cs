using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageHeadquarter : StageObject, IStageHeadquarter
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleHeadquarter; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleHeadquarter aBattleHeadquarter
	{
		get { return mBattleHeadquarter; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	protected override void OnNewObject()
// 	{
// 		base.OnNewObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateHeadquarter(Stage pStage, BattleHeadquarter pBattleHeadquarter)
	{
		OnCreateObject(
			pStage,
			pBattleHeadquarter.aCoord,
			pBattleHeadquarter.aDirectionY);

		mBattleHeadquarter = pBattleHeadquarter;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyHeadquarter(this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleHeadquarter mBattleHeadquarter;
}
