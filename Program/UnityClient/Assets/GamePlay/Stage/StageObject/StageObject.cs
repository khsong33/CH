using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class StageObject : MonoBehaviour
{
	public enum DebugObjectUpdate
	{
		Disable,
		OnStart,
		OnUpdate,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UIWidgetSorter vUIWidgetSorter;

	public int vTouchRadius = 300;

	public Transform vAssetTransform;

	public DebugObjectUpdate vDebugObjectUpdate = DebugObjectUpdate.Disable;
	public GameObject vDebugObjectPrefab;

	public bool vIsShowDebugInfo = true;
	public Color vDebugColor = ColorUtil.Color3ub(255, 255, 255);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 위치
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public abstract BattleObject aBattleObject { get; }
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public IStage aIStage
	{
		get { return mStage; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public String aStateText
	{
		get { return String.Empty; }
		set {}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public String aHpText
	{
		get { return String.Empty; }
		set { }
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public int aSqrTouchRadius
	{
		get { return mSqrTouchRadius; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public Vector3 aWorldPosition
	{
		get { return mTransform.position; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 스테이지
	public Stage aStage
	{
		get { return mStage; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		mTransform = transform;

		mSqrTouchRadius = vTouchRadius * vTouchRadius;

		OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
#if UNITY_EDITOR
	void Update()
	{
		if (vIsShowDebugInfo)
			_ShowDebugInfo();
		if (vDebugObjectPrefab != null)
			_UpdateDebugObject();

		OnEditorUpdate();
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public virtual void DestroyObject()
	{
		OnDestroyObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 처음 할당될 때 호출됩니다.
	protected virtual void OnNewObject()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	protected virtual void OnCreateObject(
		Stage pStage,
		Coord2 pCoord,
		int pDirectionY)
	{
		mStage = pStage;

		mTransform.position = pStage.GetTerrainPosition(Coord2.ToWorld(pCoord));
		mTransform.rotation = MathLib.GetRotation(pDirectionY);

		if (vAssetTransform != null)
		{
			Vector3 lAssetPosition = mTransform.position;
// 			if (Terrain.activeTerrain != null)
// 				lAssetPosition.y = Terrain.activeTerrain.SampleHeight(lAssetPosition);
			vAssetTransform.position = lAssetPosition;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴될 때 호출됩니다(풀링 고려).
	protected abstract void OnDestroyObject();
	//-------------------------------------------------------------------------------------------------------
	// 에디터 상태에서 업데이트 될 때 호출됩니다.
	protected virtual void OnEditorUpdate()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public virtual bool CheckTouch(BattleGrid.Touch pGridTouch)
	{
		return false; // 기본으로는 이벤트에 반응하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public virtual void DebugLog(String pLog)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 디버깅 정보를 보여줍니다.
	protected virtual void _ShowDebugInfo()
	{
// 		Vector3 lHalfWorldScale = new Vector3(vTouchRadius * Coord2.cToWorld, 1.0f, vTouchRadius * Coord2.cToWorld);
// 		Vector3 lWorldScale = lHalfWorldScale * 2.0f;
// 		Rect lArea = new Rect(
// 			mTransform.position.x - lHalfWorldScale.x,
// 			mTransform.position.z - lHalfWorldScale.z,
// 			lWorldScale.x,
// 			lWorldScale.z);
// 		DebugUtil.DrawPlaneY(lArea, mTransform.position.y, vDebugColor, 0.0f, false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 오브젝트의 상태를 갱신합니다.
	protected virtual void _UpdateDebugObject()
	{
		if (vDebugObjectUpdate != DebugObjectUpdate.Disable)
		{
			if (mDebugObjectTransform == null)
			{
				GameObject lDebugObject = (GameObject)GameObject.Instantiate(vDebugObjectPrefab);
				mDebugObjectTransform = lDebugObject.transform;
				mDebugObjectTransform.parent = mTransform;
				mDebugObjectTransform.position = Coord2.ToWorld(aBattleObject.aCoord); 
				mDebugObjectTransform.rotation = MathLib.GetRotation(aBattleObject.aDirectionY);
			}
			else
			{
				if (!mDebugObjectTransform.gameObject.activeInHierarchy)
					mDebugObjectTransform.gameObject.SetActive(true);
				if (vDebugObjectUpdate == DebugObjectUpdate.OnUpdate)
				{
					mDebugObjectTransform.position = Coord2.ToWorld(aBattleObject.aCoord);
					mDebugObjectTransform.rotation = MathLib.GetRotation(aBattleObject.aDirectionY);
				}
			}
		}
		else
		{
			if (mDebugObjectTransform != null)
			{
				if (mDebugObjectTransform.gameObject.activeInHierarchy)
					mDebugObjectTransform.gameObject.SetActive(false);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Transform mDebugObjectTransform;

	private Stage mStage;

	private int mSqrTouchRadius;
}
