using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageObserverDummy : StageObject, IStageObserverDummy
{
	private const float cSightRnageScaleChangeSpeed = 15.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vSightRangeTransform;
	public float vSightRangeScale = 2.5f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleObserverDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleObserverDummy aBattleObserverDummy
	{
		get { return mBattleObserverDummy; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		if (vSightRangeTransform == null)
			Debug.LogError("vSightRangeTransform is empty - " + this);

		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateObserverDummy(Stage pStage, BattleObserverDummy pBattleObserverDummy)
	{
		OnCreateObject(
			pStage,
			pBattleObserverDummy.aCoord,
			pBattleObserverDummy.aDirectionY);

		mBattleObserverDummy = pBattleObserverDummy;

		mObserverTeamIdx = aStage.vObserverTeamIdx;
		_ResetSightRange();

		UpdateSightRange(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		aStage.DestroyObserverDummy(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// StageObject 메서드
	protected override void OnEditorUpdate()
	{
		// 색적 팀을 갱신합니다.
		if (mObserverTeamIdx != aStage.vObserverTeamIdx)
		{	
			mObserverTeamIdx = aStage.vObserverTeamIdx;
			vSightRangeTransform.gameObject.SetActive(mBattleObserverDummy.aTeamIdx == mObserverTeamIdx);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObserverDummy 메서드
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObserverDummy 메서드
	public void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		aTransform.position = Coord2.ToWorld(pCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObserverDummy 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void UpdateSightRange(bool pIsInstantSight)
	{
		//mSightRangeLocalScale = 0.0f; // 시야 범위 0에서 시작
		//mSightRangeChangeFactor = 1.0f; // 변화 완료한 상태에서 시작

		mSightRangeLocalScaleTo = mBattleObserverDummy.aDetectionRange * (Coord2.cToWorld / vSightRangeScale);

		if (gameObject.activeInHierarchy &&
			!pIsInstantSight)
		{
			mSightRangeLocalScaleFrom = mSightRangeLocalScale;

			if (mSightRangeChangeFactor >= 1.0f)
				StartCoroutine(_UpdateSightRangeScale());
			else
				mSightRangeChangeFactor = 0.0f; // 변화 초기화
		}
		else
		{
			mSightRangeLocalScale = mSightRangeLocalScaleTo;
			mSightRangeChangeFactor = 1.0f;

			vSightRangeTransform.localScale = new Vector3(mSightRangeLocalScale, 1.0f, mSightRangeLocalScale);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 시야 범위 변화를 갱신합니다.
	private IEnumerator _UpdateSightRangeScale()
	{
		mSightRangeChangeFactor = 0.0f;
		do
		{
			float lChangeDistance = Mathf.Abs(mSightRangeLocalScaleTo - mSightRangeLocalScaleFrom);
			if (lChangeDistance > Mathf.Epsilon)
			{
				mSightRangeChangeFactor += cSightRnageScaleChangeSpeed * Time.deltaTime / lChangeDistance;
				if (mSightRangeChangeFactor > 1.0f)
					mSightRangeChangeFactor = 1.0f;
			}
			else
				mSightRangeChangeFactor = 1.0f;
			//Debug.Log("mSightRangeChangeFactor=" + mSightRangeChangeFactor);

			mSightRangeLocalScale = Mathf.Lerp(mSightRangeLocalScaleFrom, mSightRangeLocalScaleTo, mSightRangeChangeFactor);
			vSightRangeTransform.localScale = new Vector3(mSightRangeLocalScale, 1.0f, mSightRangeLocalScale);

			yield return null;
		} while (mSightRangeChangeFactor < 1.0f);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 시야 범위를 초기화합니다.
	private void _ResetSightRange()
	{
		vSightRangeTransform.gameObject.SetActive(mBattleObserverDummy.aTeamIdx == mObserverTeamIdx);
		mSightRangeLocalScale = 0.0f; // 시야 범위 0에서 시작
		mSightRangeChangeFactor = 1.0f; // 변화 완료한 상태에서 시작
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleObserverDummy mBattleObserverDummy;

	private int mObserverTeamIdx;
	private float mSightRangeChangeFactor;
	private float mSightRangeLocalScaleFrom;
	private float mSightRangeLocalScaleTo;
	private float mSightRangeLocalScale;
}
