using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTargetingDummy : StageObject, IStageTargetingDummy
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vDummyObjectTransform;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleTargetingDummy; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTargetingDummy aBattleTargetingDummy
	{
		get { return mBattleTargetingDummy; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		base.OnNewObject();
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateTargetingDummy(Stage pStage, BattleTargetingDummy pBattleTargetingDummy)
	{
		OnCreateObject(
			pStage,
			pBattleTargetingDummy.aCoord,
			pBattleTargetingDummy.aDirectionY);

		mBattleTargetingDummy = pBattleTargetingDummy;

		if (!aStage.vIsHideActor)
		{
			vDummyObjectTransform = AssetManager.get.InstantiateChildObject(mBattleTargetingDummy.aTroopSpec.mStageTargetingDummyAssetKey, aTransform);
			vDummyObjectTransform.position = aTransform.position;
			vDummyObjectTransform.rotation = aTransform.rotation;
		}
		else
			vDummyObjectTransform = null;

		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;

		_OnZoomChanged();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;

		if (vDummyObjectTransform != null)
		{
			DestroyObject(vDummyObjectTransform.gameObject);
			vDummyObjectTransform = null;
		}

		aStage.DestroyTargetingDummy(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void SetDetected(int pDetectingTeamIdx, bool pIsToDetected)
	{
		int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
		if (pDetectingTeamIdx != lObserverTeamIdx)
			return; // 이 클라이언트에 대해서만 처리

		_OnZoomChanged();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void UpdateCoord(Coord2 pCoord, bool pIsArrived)
	{
		aTransform.position = Coord2.ToWorld(pCoord);

		if (vDummyObjectTransform != null)
			vDummyObjectTransform.position = aTransform.position;
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTargetingDummy 메서드
	public void UpdateDirectionY(int pDirectionY)
	{
		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);

		if (vDummyObjectTransform != null)
			vDummyObjectTransform.rotation = aTransform.rotation;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
    //-------------------------------------------------------------------------------------------------------
	// 줌이 변경될때 호출됩니다.
	private void _OnZoomChanged()
	{
		float lViewDist = BattlePlay.main.vControlPanel.vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance;
		bool lIsVisible
			= (lViewDist <= aStage.vLodDistance)
			&& !mBattleTargetingDummy.aIsDetecteds[BattlePlay.main.vStage.vObserverTeamIdx]; // 부대 보이기 조건의 반대(안 보이는 부대에게만 표시되는 것이므로)
		if (vDummyObjectTransform != null)
			vDummyObjectTransform.gameObject.SetActive(lIsVisible);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTargetingDummy mBattleTargetingDummy;
	private bool mIsActive;
}
