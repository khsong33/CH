using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroop : StageObject, IStageTroop
{
	private const float cMovePathOffsetScale = 0.5f;
	private static readonly Vector3[] cMovePathOffsets = {
		Vector3.zero,
		new Vector3(-cMovePathOffsetScale, 0.0f, cMovePathOffsetScale),		new Vector3(0.0f, 0.0f, cMovePathOffsetScale),	new Vector3(cMovePathOffsetScale, 0.0f, cMovePathOffsetScale),
		new Vector3(-cMovePathOffsetScale, 0.0f, 0.0f),																		new Vector3(cMovePathOffsetScale, 0.0f, 0.0f),
		new Vector3(-cMovePathOffsetScale, 0.0f, -cMovePathOffsetScale),	new Vector3(0.0f, 0.0f, -cMovePathOffsetScale),	new Vector3(cMovePathOffsetScale, 0.0f, -cMovePathOffsetScale)};

	private const float cSightRnageScaleChangeSpeed = 15.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public uint vGuid = 0;

	public StageTroopMark vMarkPrefab;
	public Transform vMarkRoot;

	public GameObject vSelectionObject;
	public Transform vSelectionRingTransform;
	public float vSelectionRingScale = 2.5f;

	public Transform vSightRangeTransform;
	public float vSightRangeScale = 2.5f;

	public Transform vAttackRangeTransform;
	public float vAttackRangeScale = 2.3f;

	public bool vIsLogControl = false;
	public bool vIsNoDamage = false;
	public bool vIsDebugShowActor = false;
	public bool vIsShowMonitor = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 속성
	public new Vector3 aWorldPosition
	{
		get { return ((mActorManager != null) ? mActorManager.aTransform.position : aTransform.position); }
	}
	//-------------------------------------------------------------------------------------------------------
	// StageObject 속성
	public override BattleObject aBattleObject
	{
		get { return mBattleTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 전투 객체
	public BattleTroop aBattleTroop
	{
		get { return mBattleTroop; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택 가능 여부
	public bool aIsSelectable
	{
		get { return mIsSelectable; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택 여부
	public bool aIsSelected
	{
		get { return mIsSelected; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("StageTroop@{0}", mBattleTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
// 	public override void DestroyObject()
// 	{
// 		base.DestroyObject();
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnNewObject()
	{
		if (vSelectionObject == null)
			Debug.LogError("vSelectionObject is empty - " + this);
		if (vSelectionRingTransform == null)
			Debug.LogError("vSelectionRingTransform is empty - " + this);
		if (vSightRangeTransform == null)
			Debug.LogError("vSightRangeTransform is empty - " + this);
		if (vAttackRangeTransform == null)
			Debug.LogError("vAttackRangeTransform is empty - " + this);

		base.OnNewObject();

		mSelectionRingLocalPosition = vSelectionRingTransform.localPosition;
		mSelectionRingBaseScale = vSelectionRingTransform.localScale;
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성할 때 호출됩니다(풀링 고려).
	public virtual void OnCreateTroop(Stage pStage, BattleTroop pBattleTroop)
	{
		vGuid = pBattleTroop.aGuid;

		vIsLogControl = pBattleTroop.aIsLogControl;
		vIsNoDamage = pBattleTroop.aIsNoDamage;
		vIsDebugShowActor = pStage.vIsDebugShowActor;

		OnCreateObject(
			pStage,
			pBattleTroop.aCoord,
			pBattleTroop.aDirectionY);

		mBattleTroop = pBattleTroop;

		mActorManager = null;
		mCollider = null;

		if (!aStage.vIsHideActor &&
			!mBattleTroop.aIsObserver)
		{
			mActorManager = aStage.aActorPool.CreateActorManager(this);
			if (mActorManager != null)
			{
				vSelectionRingTransform.parent = mActorManager.aTransform; // 선택링을 리더에 붙임
				vSelectionRingTransform.localPosition = mSelectionRingLocalPosition;

				mCollider = mActorManager.aCollider;
			}
		}

		_UpdateSelectable();

		if (mStageTroopMark == null)
			_CreateMark();
		mStageTroopMark.vTroopMark.SetDisplayTroop(mBattleTroop, this);
		mStageTroopMark.gameObject.SetActive(vIsDebugShowActor && !mBattleTroop.aIsNpc);

		if (mActorManager != null)
		{
			float lSelectionRingScale = mActorManager.aVisualRadius / vSelectionRingScale;
			vSelectionRingTransform.localScale = new Vector3(
				mSelectionRingBaseScale.x * lSelectionRingScale,
				mSelectionRingBaseScale.y,
				mSelectionRingBaseScale.z * lSelectionRingScale);
		}

		mObserverTeamIdx = aStage.vObserverTeamIdx;
		_ResetSightRange();

		OnUpdateSightRange();
		OnUpdateAttackRange();

		if (mActorManager != null)
			SetMarkActorTransform(mActorManager.aTransform);
		else
			SetMarkActorTransform(aTransform);

		mIsSelected = false;
		vSelectionObject.SetActive(mIsSelected);
		vSelectionRingTransform.gameObject.SetActive(mIsSelected);

		mCommanderCollider.enabled = (mBattleTroop.aCommander.aUser != null);// && !mIsSelected; // 유저 부대 중에 선택 안된 부대라면 지휘관을 통해서 선택 가능

		if (BattlePlay.main.vControlPanel.aFocusingCommander == mBattleTroop.aCommander) // 현재 선택된 지휘관의 부대라면
			SetSelected(true); // 생성하면서 선택 상태를 만듭니다.

		if (mActorManager != null)
			mActorManager.OnSpawn();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	protected override void OnDestroyObject()
	{
		_ShowMonitor(false);

		mStageTroopMark.gameObject.SetActive(false);

		if (mActorManager != null)
		{
			vSelectionRingTransform.parent = aTransform; // 선택링을 가져옴

			mActorManager.Destroy();
			mActorManager = null;
		}

		aStage.DestroyTroop(this);
	}
	//-------------------------------------------------------------------------------------------------------
	// StageObject 메서드
	protected override void OnEditorUpdate()
	{
		// 속성 변화를 반영합니다.
		if (mBattleTroop.aIsLogControl != vIsLogControl)
		{
			mBattleTroop.aIsLogControl = vIsLogControl;
			if (vIsLogControl)
				Debug.Log(mBattleTroop.aActiveControlName);
		}
		if (mBattleTroop.aIsNoDamage != vIsNoDamage)
		{
			mBattleTroop.aIsNoDamage = vIsNoDamage;
			vIsNoDamage = mBattleTroop.aIsNoDamage; // 내부에서의 상태 재반영(외부 지정이 안 될 수도 있어서)
			if (vIsNoDamage)
				Debug.Log(mBattleTroop.aActiveControlName);
		}

		// 색적 팀을 갱신합니다.
		if (mObserverTeamIdx != aStage.vObserverTeamIdx)
		{	
			mObserverTeamIdx = aStage.vObserverTeamIdx;
			vSightRangeTransform.gameObject.SetActive(mBattleTroop.aTeamIdx == mObserverTeamIdx);
		}

		// 모니터 표시 여부를 갱신합니다.
		if ((mMonitor != null) != vIsShowMonitor)
			_ShowMonitor(vIsShowMonitor);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
// 	public override bool CheckTouch(BattleGrid.Touch pGridTouch)
// 	{
// 		return false;
// 	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public override void DebugLog(String pLog)
	{
		if (vIsLogControl)
			mStageTroopMark.DebugLog(pLog);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageObject 메서드
	public void SetSelected(bool pIsSelected)
	{
		if (mIsSelected == pIsSelected)
			return;

		mIsSelected = pIsSelected;
		vSelectionObject.SetActive(mIsSelected);
		vSelectionRingTransform.gameObject.SetActive(mIsSelected);
		mStageTroopMark.SetSelected(mIsSelected);

		mCommanderCollider.enabled = (mBattleTroop.aCommander.aUser != null);// && !mIsSelected; // 유저 부대 중에 선택 안된 부대라면 지휘관을 통해서 선택 가능
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void SetTargetable(bool pIsTargetable)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetCoordAndDirectionY(Coord2 pCoord, int pDirectionY)
	{
		aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(pCoord));
		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);

		if (mActorManager != null)
			mActorManager.OnSetCoordAndDirectionY(pCoord, pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFrameUpdate(int pDeltaTime)
	{
		if (mActorManager != null)
			mActorManager.OnFrameUpdate(pDeltaTime);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateTeam(int pTeamIdx)
	{
		//Debug.Log("UpdateTeam() pTeamIdx=" + pTeamIdx);

		mStageTroopMark.vTroopMark.SetDisplayTroop(mBattleTroop, this);
		mStageTroopMark.vTroopMark.UpdateHp(mBattleTroop.aStat.aCurMilliHp, mBattleTroop.aStat.aMaxMilliHp);

		_ResetSightRange(); // 팀이 바뀌면 시야 범위도 다시 초기화
		OnUpdateSightRange();

		_UpdateSelectable(); // 팀이 바뀌면 선택 가능 여부도 바뀔 수 있음

		OnUpdateDetection(mBattleTroop.aTeamIdx, true);

		vDebugColor = BattlePlay.main.GetUserColor(mBattleTroop.aCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateDetection(int pDetectingTeamIdx, bool pIsToDetected)
	{
		int lObserverTeamIdx = BattlePlay.main.vStage.vObserverTeamIdx;
		if (pDetectingTeamIdx != lObserverTeamIdx)
			return; // 이 클라이언트에 대해서만 처리

		bool lIsVisible = mBattleTroop.aIsDetecteds[lObserverTeamIdx] || vIsDebugShowActor;
		if (mActorManager != null)
			mActorManager.aIsVisible = lIsVisible;
		mStageTroopMark.gameObject.SetActive(lIsVisible && !mBattleTroop.aIsNpc);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateHp(int pCurHp, int pMaxHp, TroopHpChangeType pHpChangeType)
	{
		mStageTroopMark.vTroopMark.UpdateHp(mBattleTroop.aStat.aCurMilliHp, mBattleTroop.aStat.aMaxMilliHp);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateExp()
	{
		mStageTroopMark.vTroopMark.UpdateExp();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateVeteranRank(int pRank)
	{
		mStageTroopMark.vTroopMark.ShowVeteranRank(pRank);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateSightRange()
	{
		//mSightRangeLocalScale = 0.0f; // 시야 범위 0에서 시작
		//mSightRangeChangeFactor = 1.0f; // 변화 완료한 상태에서 시작

		mSightRangeLocalScaleTo = mBattleTroop.aStat.aSightRange * (Coord2.cToWorld / vSightRangeScale);

		if (gameObject.activeInHierarchy &&
			!mBattleTroop.aIsObserver)
		{
			mSightRangeLocalScaleFrom = mSightRangeLocalScale;

			if (mSightRangeChangeFactor >= 1.0f)
				StartCoroutine(_UpdateSightRangeScale());
			else
				mSightRangeChangeFactor = 0.0f; // 변화 초기화
		}
		else
		{
			mSightRangeLocalScale = mSightRangeLocalScaleTo;
			mSightRangeChangeFactor = 1.0f;

			vSightRangeTransform.localScale = new Vector3(mSightRangeLocalScale, 1.0f, mSightRangeLocalScale);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateAttackRange()
	{
		float lAttackRangeScale = mBattleTroop.aStat.aMaxAttackRange * (Coord2.cToWorld / vAttackRangeScale);
		vAttackRangeTransform.localScale = new Vector3(lAttackRangeScale, 1.0f, lAttackRangeScale);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateHiding(bool pIsHiding)
	{
		if (mActorManager != null)
		{
			bool lIsTransparent = pIsHiding;
			if (pIsHiding)
			{
				for (int iTeam = 0; iTeam < mBattleTroop.aBattleGrid.aTeamCount; ++iTeam)
					if ((iTeam != mBattleTroop.aTeamIdx) &&
						mBattleTroop.aIsDetecteds[iTeam])
					{
						lIsTransparent = false; // 한 팀에게라도 발각되면 투명 상태 해제
						break;
					}
			}

			mActorManager.aIsHiding = lIsTransparent;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateMoraleState(TroopMoraleState pTroopMoraleState)
	{
		mStageTroopMark.vTroopMark.UpdateMoraleState(pTroopMoraleState);

		if (mActorManager != null)
			mActorManager.OnUpdateMoraleState(pTroopMoraleState);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateReturnState(bool pIsFallBehind)
	{
		mStageTroopMark.vTroopMark.UpdateReturnState(pIsFallBehind);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateLeaderState(bool pIsLeader)
	{
		mStageTroopMark.vTroopMark.ShowCommanderInfo(pIsLeader);
		OnUpdateRecruitState();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUpdateRecruitState()
	{
		if (mStageTroopMark.vTroopMark.aIsShowCommanderInfo)
		{
			if (mBattleTroop.aCommander.CheckRecruitable())
				mStageTroopMark.vTroopMark.ShowRecruitInfo(mBattleTroop.aCommander.aRecruitMp);
			else
				mStageTroopMark.vTroopMark.HideRecruitInfo();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstCoord(Coord2 pDstCoord, int pArrivalDelay, BattleTroopMoveUpdater.StopType pStopType)
	{
		if (mActorManager != null)
			mActorManager.OnSetDstCoord(pDstCoord, pArrivalDelay, pStopType);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyDirectionY(int pDstBodyDirectionY, int pArrivalDelay)
	{
		if (mActorManager != null)
			mActorManager.OnSetDstBodyDirectionY(pDstBodyDirectionY, pArrivalDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstBodyWeaponDirectionY(int pDstBodyWeaponDirectionY, int pArrivalDelay)
	{
		if (mActorManager != null)
			mActorManager.OnSetDstBodyWeaponDirectionY(pDstBodyWeaponDirectionY, pArrivalDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnSetDstTurretDirectionY(int pDstTurretDirectionY, int pArrivalDelay)
	{
		if (mActorManager != null)
			mActorManager.OnSetDstTurretDirectionY(pDstTurretDirectionY, pArrivalDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnHoldMove(bool pIsHoldMove)
	{
		if (mActorManager != null)
			mActorManager.OnHoldMove(pIsHoldMove);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopMove(Coord2 pStopCoord)
	{
		if (mActorManager != null)
			mActorManager.OnStopMove(pStopCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnMoveTo(Coord2 pCoord, bool pIsArrived)
	{
		//if (vIsLogControl) Debug.Log("UpdateCoord() pCoord=" + pCoord + " pIsArrived=" + pIsArrived);

		aTransform.position = aStage.GetTerrainPosition(Coord2.ToWorld(pCoord));

		if (mActorManager != null)
			mActorManager.OnMoveTo(pCoord, pIsArrived);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateBodyToDirectionY(int pDirectionY)
	{
		//if (vIsLogControl) Debug.Log("RotateBodyToDirectionY() pDirectionY=" + pDirectionY);

		aTransform.rotation = Quaternion.Euler(0.0f, pDirectionY, 0.0f);

		if (mActorManager != null)
			mActorManager.OnRotateBodyToDirectionY(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateBodyWeaponToDirectionY(int pDirectionY)
	{
		//if (vIsLogControl) Debug.Log("RotateBodyWeaponToDirectionY() pDirectionY=" + pDirectionY);

		if (mActorManager != null)
			mActorManager.OnRotateBodyWeaponToDirectionY(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnRotateTurretToDirectionY(int pDirectionY)
	{
		//if (vIsLogControl) Debug.Log("RotateTurretToDirectionY() pDirectionY=" + pDirectionY);

		if (mActorManager != null)
			mActorManager.OnRotateTurretToDirectionY(pDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnInstallWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pInstallDelay)
	{
		if (mActorManager != null)
			mActorManager.OnInstallWeapon(pWeaponSpec, pTargetCoord, pTargetTroop, pInstallDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnUnInstallWeapon(WeaponSpec pWeaponSpec, int pUnInstallDelay)
	{
		if (mActorManager != null)
			mActorManager.OnUnInstallWeapon(pWeaponSpec, pUnInstallDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnTrackWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pTrackingDelay)
	{
		if (mActorManager != null)
			mActorManager.OnTrackWeapon(pWeaponSpec, pTargetCoord, pTargetTroop, pTrackingDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnWindupWeapon(WeaponSpec pWeaponSpec, Coord2 pTargetCoord, BattleTroop pTargetTroop, int pWindupDelay)
	{
		if (mActorManager != null)
			mActorManager.OnWindupWeapon(pWeaponSpec, pTargetCoord, pTargetTroop, pWindupDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IActorManager 메서드
	public void OnAimWeapon(int pAimDelay)
	{
		if (mActorManager != null)
			mActorManager.OnAimWeapon(pAimDelay);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnFireWeapon(BattleFireEffect pFireEffect, int pCooldownDelay, bool pIsResumeAim)
	{
		if (mActorManager != null)
			mActorManager.OnFireWeapon(pFireEffect, pCooldownDelay, pIsResumeAim);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnEndCooldown()
	{
		if (mActorManager != null)
			mActorManager.OnEndCooldown();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnGoToInteraction(TroopInteractionType pTroopInteractionType, BattleTroop pTargetTroop)
	{
		if (mActorManager != null)
			mActorManager.OnGoToInteraction(pTroopInteractionType, pTargetTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnComeBackInteraction()
	{
		if (mActorManager != null)
			mActorManager.OnComeBackInteraction();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStartSkill(BattleCommanderSkill pCommanderSkill)
	{
		if (mActorManager != null)
			mActorManager.OnStartSkill(pCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopSkill(BattleCommanderSkill pCommanderSkill)
	{
		if (mActorManager != null)
			mActorManager.OnStopSkill(pCommanderSkill);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStartAction(TroopActionType pActionType)
	{
		if (mActorManager != null)
			mActorManager.OnStartAction(pActionType);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void OnStopAction()
	{
		if (mActorManager != null)
			mActorManager.OnStopAction();
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
		mStageTroopMark.ShowHit(pTroopHitResult);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowRetreat(bool pIsRetreat)
	{
		mStageTroopMark.vTroopMark.ShowRetreat(pIsRetreat);
	}
	//-------------------------------------------------------------------------------------------------------
	// IStageTroop 메서드
	public void ShowTroopScript(string pMessageKey, float pDuration)
	{
		mStageTroopMark.vTroopMark.ShowTroopScript(pMessageKey, pDuration);
	}
	//-------------------------------------------------------------------------------------------------------
	// 부대 마크를 붙일 변환을 지정합니다.
	public void SetMarkActorTransform(Transform pMarkActorTransform)
	{
		if (mStageTroopMark == null)
			return;

		NGUIUtil.SetAnchorTarget(mStageTroopMark.vAnchorWidget, pMarkActorTransform);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 마크를 생성합니다.
	private void _CreateMark()
	{
		if (vMarkPrefab == null)
			Debug.LogError("vMarkPrefab is empty - " + this);

		mStageTroopMark = ObjectUtil.InstantiateComponentObject<StageTroopMark>(vMarkPrefab);
		NGUIUtil.SetParent(mStageTroopMark.transform, aStage.vUIWidgetRoot);
		NGUIUtil.SetAnchorTarget(mStageTroopMark.vAnchorWidget, vMarkRoot);

		mCommanderCollider = mStageTroopMark.vTroopMark.vCommanderInfoRoot.GetComponent<Collider>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 모니터 보이기를 지정합니다.
	private void _ShowMonitor(bool pIsShowMonitor)
	{
		if (pIsShowMonitor)
		{
			if (mMonitor != null)
				return;

			mMonitor = gameObject.AddComponent<StageTroopMonitor>();
			mMonitor.vStageTroop = this;
		}
		else
		{
			if (mMonitor == null)
				return;

			Destroy(mMonitor);
			mMonitor = null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버깅 정보를 보여줍니다.
	protected override void _ShowDebugInfo()
	{
		//base._ShowDebugInfo(); // 재정의
		
		// 유저 컬러 사각형을 그립니다.
		float lUnitScale = mBattleTroop.aTroopSpec.mUnitRadius * Coord2.cToWorld;
		Vector3 lHalfWorldScale = new Vector3(lUnitScale, 1.0f, lUnitScale);
		Vector3 lWorldScale = lHalfWorldScale * 2.0f;
		Rect lArea = new Rect(
			aTransform.position.x - lHalfWorldScale.x,
			aTransform.position.z - lHalfWorldScale.z,
			lWorldScale.x,
			lWorldScale.z);
		DebugUtil.DrawPlaneY(lArea, aTransform.position.y, vDebugColor, 0.0f, false);

		// 이동 경로를 보입니다.
		BattleTroopMoveUpdater lMoveUpdater = mBattleTroop.aMoveUpdater;
		if (lMoveUpdater.aIsMoving)
		{
			Vector3 lMovePathOffset = cMovePathOffsets[mBattleTroop.aTroopSlotIdx % cMovePathOffsets.Length];

			if (lMoveUpdater.aPathTileCount <= 2)
			{
				Debug.DrawLine(
					Coord2.ToWorld(lMoveUpdater.aMoveStartCoord) + lMovePathOffset,
					Coord2.ToWorld(lMoveUpdater.aMoveEndCoord) + lMovePathOffset,
					vDebugColor, 0.0f, false);
			}
			else
			{
				int lPathMoveCount = lMoveUpdater.aPathTileCount - 1;
				int lPathMoveIndex = 0;
				Coord2 lLineStartCoord = lMoveUpdater.aMoveStartCoord;
				while (++lPathMoveIndex <= lPathMoveCount)
				{
					Coord2 lLineEndCoord = lMoveUpdater.GetPathMoveCoord(lPathMoveIndex, lLineStartCoord);
					Debug.DrawLine(
						Coord2.ToWorld(lLineStartCoord) + lMovePathOffset,
						Coord2.ToWorld(lLineEndCoord) + lMovePathOffset,
						vDebugColor, 0.0f, false);

					lLineStartCoord = lLineEndCoord;
				}
			}
		}

		// 진입하고 있는 타일을 보입니다.
		if (mBattleTroop.aMoveUpdater.aEnteringTile != null)
		{
			Debug.DrawLine(
				Coord2.ToWorld(mBattleTroop.aMoveUpdater.aEnteringTile.aCenterCoord),
				Coord2.ToWorld(mBattleTroop.aCoord),
				aStage.vEnteringTroopLineColor);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 시야 범위를 초기화합니다.
	private void _ResetSightRange()
	{
		vSightRangeTransform.gameObject.SetActive(mBattleTroop.aTeamIdx == mObserverTeamIdx);
		mSightRangeLocalScale = 0.0f; // 시야 범위 0에서 시작
		mSightRangeChangeFactor = 1.0f; // 변화 완료한 상태에서 시작
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택 가능 여부를 갱신합니다.
	private void _UpdateSelectable()
	{
		int lClientUserTeamIdx = BattlePlay.main.aClientUser.aTeamIdx;
		if (mBattleTroop.aCommander.aUser != null) // 유저 부대의 경우
		{
			mIsSelectable = (mBattleTroop.aTeamIdx != lClientUserTeamIdx); // 다른 팀의 유저 부대만 선택(타게팅) 가능(자신의 팀은 선택 불가)
		}
		else // 방어군이나 이벤트 부대의 경우
		{
			if (mBattleTroop.aTeamIdx != lClientUserTeamIdx)
			{
				BattleCommander lCommander = mBattleTroop.aCommander;
				if (lCommander.aLinkCheckPoint != null)
				{
					int lTroopFormationIdx = lCommander.GetTroopFormationIdx(mBattleTroop.aTroopSlotIdx);
					mIsSelectable = (mBattleTroop.aCommander.aTroopSet.mFormationPositionOffsets[lTroopFormationIdx] != Coord2.zero); // 방어 부대는 오프셋이 (0,0)이면 거점 안에 있는 것이므로 선택(타게팅) 불가
				}
				else
				{
					mIsSelectable = true; // 그 외의 경우는 선택(타게팅) 가능
				}
			}
			else
			{
				mIsSelectable = false; // 같은 팀의 방어 부대는 선택 불가
			}
		}

		// 선택 가능한 여부에 따라 충돌체를 활성화시킵니다.
		if (mCollider != null)
			mCollider.enabled = mIsSelectable;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 시야 범위 변화를 갱신합니다.
	private IEnumerator _UpdateSightRangeScale()
	{
		mSightRangeChangeFactor = 0.0f;
		do
		{
			float lChangeDistance = Mathf.Abs(mSightRangeLocalScaleTo - mSightRangeLocalScaleFrom);
			if (lChangeDistance > Mathf.Epsilon)
			{
				mSightRangeChangeFactor += cSightRnageScaleChangeSpeed * Time.deltaTime / lChangeDistance;
				if (mSightRangeChangeFactor > 1.0f)
					mSightRangeChangeFactor = 1.0f;
			}
			else
				mSightRangeChangeFactor = 1.0f;
			//Debug.Log("mSightRangeChangeFactor=" + mSightRangeChangeFactor);

			mSightRangeLocalScale = Mathf.Lerp(mSightRangeLocalScaleFrom, mSightRangeLocalScaleTo, mSightRangeChangeFactor);
			vSightRangeTransform.localScale = new Vector3(mSightRangeLocalScale, 1.0f, mSightRangeLocalScale);

			yield return null;
		} while (mSightRangeChangeFactor < 1.0f);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BattleTroop mBattleTroop;
	private StageTroopMonitor mMonitor;

	private StageTroopMark mStageTroopMark;
	private Collider mCommanderCollider;

	private bool mIsSelectable;
	private bool mIsSelected;
	private Vector3 mSelectionRingLocalPosition;
	private Vector3 mSelectionRingBaseScale;

	private int mObserverTeamIdx;
	private float mSightRangeChangeFactor;
	private float mSightRangeLocalScaleFrom;
	private float mSightRangeLocalScaleTo;
	private float mSightRangeLocalScale;

	private IActorManager mActorManager;
	private Collider mCollider;
}
