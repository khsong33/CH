using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroopMonitor : MonoBehaviour
{
	public enum DebugRender
	{
		DetectionRange,
		HidingDetectionRange,
		MinAttackRange,
		NearAttackRange,
		MiddleAttackRange,
		FarAttackRange,
		UnitSize,

		Count
	};
	public enum DebugText
	{
		Morale = 0,

		Count
	}

	internal const float cLineWidth = 0.25f;
	internal const int cCircleVertexCount = 100;
	internal const float cCircleVertexAngle = Mathf.PI * 2.0f / cCircleVertexCount;
	internal const float cCirclePositionOffsetY = 0.25f;

	internal readonly Color cDetectionRangeColor = Color.blue;//new Color(0.0f, 0.0f, 1.0f);
	internal readonly Color cHidingDetectionRangeColor = Color.blue;//new Color(0.5f, 0.5f, 1.0f);
	internal readonly Color cMinAttackRangeColor = Color.red;//new Color(1.0f, 0.75f, 0.75f);
	internal readonly Color cNearAttackRangeColor = Color.red;//new Color(1.0f, 0.5f, 0.5f);
	internal readonly Color cMiddleAttackRangeColor = Color.red;//new Color(1.0f, 0.25f, 0.25f);
	internal readonly Color cFarAttackRangeColor = Color.red;//new Color(1.0f, 0.0f, 0.0f);
	internal readonly Color cUnitSizeColor = Color.yellow;//new Color(1.0f, 1.0f, 1.0f);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public StageTroop vStageTroop;

	public bool vIsShowDetectionRange = true;
	public bool vIsShowHidingDetectionRange = true;
	public bool vIsShowMinAttackRange = true;
	public bool vIsShowNearAttackRange = true;
	public bool vIsShowMiddleAttackRange = true;
	public bool vIsShowFarAttackRange = true;
	public bool vIsShowUnitSize = true;
	public bool vIsShowMorale = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		mTransform = transform;
		mLineRenderers = new LineRenderer[(int)DebugRender.Count];
		mTextMeshs = new TextMesh[(int)DebugText.Count];

		mTextCount = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		for (int iDebug = 0; iDebug < (int)DebugRender.Count; iDebug++)
			_RemoveLineRenderer((DebugRender)iDebug);

		for (int iDebug = 0; iDebug < (int)DebugText.Count; iDebug++)
			_RemoveText((DebugText)iDebug);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		if (vIsShowDetectionRange)
		{
			int lDetectionRange = vStageTroop.aBattleTroop.aStat.aSightRange;
			if (lDetectionRange > 0)
			{
				float lCheckDistance = (float)lDetectionRange * Coord2.cToWorld;
				_DrawCircle(DebugRender.DetectionRange, mTransform.position, lCheckDistance, cDetectionRangeColor);
			}
			else
				_RemoveLineRenderer(DebugRender.DetectionRange);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.DetectionRange);
		}

		if (vIsShowHidingDetectionRange)
		{
			int lHidingDetectionRange = vStageTroop.aBattleTroop.aStat.aHidingDetectionRange;
			if (lHidingDetectionRange > 0)
			{
				float lCheckDistance = (float)lHidingDetectionRange * Coord2.cToWorld;
				_DrawCircle(DebugRender.HidingDetectionRange, mTransform.position, lCheckDistance, cHidingDetectionRangeColor);
			}
			else
				_RemoveLineRenderer(DebugRender.HidingDetectionRange);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.HidingDetectionRange);
		}

		BattleTroopStat.WeaponData lPrimaryWeaponData = vStageTroop.aBattleTroop.aStat.aWeaponDatas[TroopSpec.cPrimaryWeaponIdx];

		if (vIsShowMinAttackRange)
		{
			int lMinAttackRange = lPrimaryWeaponData.aWeaponStatTable.aMinRange;
			if (lMinAttackRange > 0)
			{
				float lCheckDistance = (float)lMinAttackRange * Coord2.cToWorld;
				_DrawCircle(DebugRender.MinAttackRange, mTransform.position, lCheckDistance, cMinAttackRangeColor);
			}
			else
				_RemoveLineRenderer(DebugRender.MinAttackRange);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.MinAttackRange);
		}

		if (vIsShowNearAttackRange)
		{
			float lCheckDistance = (float)lPrimaryWeaponData.aWeaponRangeStatTables[(int)WeaponAttackRange.Near].aRange * Coord2.cToWorld;
			_DrawCircle(DebugRender.NearAttackRange, mTransform.position, lCheckDistance, cNearAttackRangeColor);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.NearAttackRange);
		}

		if (vIsShowMiddleAttackRange)
		{
			float lCheckDistance = (float)lPrimaryWeaponData.aWeaponRangeStatTables[(int)WeaponAttackRange.Middle].aRange * Coord2.cToWorld;
			_DrawCircle(DebugRender.MiddleAttackRange, mTransform.position, lCheckDistance, cMiddleAttackRangeColor);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.MiddleAttackRange);
		}

		if (vIsShowFarAttackRange)
		{
			float lCheckDistance = (float)lPrimaryWeaponData.aWeaponRangeStatTables[(int)WeaponAttackRange.Far].aRange * Coord2.cToWorld;
			_DrawCircle(DebugRender.FarAttackRange, mTransform.position, lCheckDistance, cFarAttackRangeColor);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.FarAttackRange);
		}

		if (vIsShowUnitSize)
		{
			float lCheckDistance = (float)vStageTroop.aBattleTroop.aTroopSpec.mUnitRadius * Coord2.cToWorld;
			_DrawCircle(DebugRender.UnitSize, mTransform.position, lCheckDistance, cUnitSizeColor);
		}
		else
		{
			_RemoveLineRenderer(DebugRender.UnitSize);
		}

		if (vIsShowMorale)
		{
			String lMoraleText = String.Format("Morale : {0:F}", vStageTroop.aBattleTroop.aStat.aCurMilliMorale * 0.001f);
			_DrawText(DebugText.Morale, mTransform.position, lMoraleText);
		}
		else
		{
			_RemoveText(DebugText.Morale);
		}
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ToString() 메서드
	public override string ToString()
	{
		return String.Format("StageTroopMonitor({0})", vStageTroop);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 텍스트 로그를 추가합니다
	private void _DrawText(DebugText pState, Vector3 pCenterPosition, String pText)
	{
		TextMesh lTextMesh = mTextMeshs[(int)pState];
		if (lTextMesh == null)
		{
			GameObject lDebugTextObject = new GameObject();
			lDebugTextObject.transform.parent = transform;
			lDebugTextObject.name = pState.ToString();
			BillboardObject lBillboardObject = lDebugTextObject.AddComponent<BillboardObject>();
			lBillboardObject.vCamera = BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aMainCamera;

			lTextMesh = lDebugTextObject.AddComponent<TextMesh>();
			Font lArialFont = Resources.GetBuiltinResource(typeof(Font), "Arial.ttf") as Font;
			lTextMesh.font = lArialFont;
			lTextMesh.anchor = TextAnchor.MiddleCenter;
			lTextMesh.lineSpacing = mTextCount;
			lTextMesh.GetComponent<Renderer>().material = lArialFont.material;
			mTextCount++;

			mTextMeshs[(int)pState] = lTextMesh;
		}
		lTextMesh.text = pText;
	}
	//-------------------------------------------------------------------------------------------------------
	// 텍스트 로그를 삭제합니다.
	private void _RemoveText(DebugText pState)
	{
		if (mTextMeshs[(int)pState] == null)
			return;

		Destroy(mTextMeshs[(int)pState].gameObject);
		mTextMeshs[(int)pState] = null;

		mTextCount--;
		if (mTextCount < 0)
		{
			Debug.LogError("Overflow Text Count - " + mTextCount);
			mTextCount = 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 원을 그립니다.
	private void _DrawCircle(DebugRender pState, Vector3 pCenterPosition, float pCheckDistance, Color pCircleColor)
	{
		LineRenderer lLineRenderer = mLineRenderers[(int)pState];
		if (lLineRenderer == null)
		{
			GameObject lDebugRenderObject = new GameObject();
			lDebugRenderObject.transform.parent = transform;
			lDebugRenderObject.name = pState.ToString();

			lLineRenderer = lDebugRenderObject.AddComponent<LineRenderer>();

			lLineRenderer.material = new Material(Shader.Find("Particles/Additive"));
			lLineRenderer.SetColors(pCircleColor, pCircleColor);
			lLineRenderer.SetWidth(cLineWidth, cLineWidth);
			lLineRenderer.SetVertexCount(cCircleVertexCount + 1);

			mLineRenderers[(int)pState] = lLineRenderer;
		}

		for (int iPosition = 0; iPosition <= cCircleVertexCount; ++iPosition)
		{
			float lTheta = cCircleVertexAngle * iPosition;
			float lX = pCheckDistance * Mathf.Cos(lTheta);
			float lY = pCheckDistance * Mathf.Sin(lTheta);
			Vector3 pos = new Vector3(lX + pCenterPosition.x, cCirclePositionOffsetY, lY + pCenterPosition.z);
			lLineRenderer.SetPosition(iPosition, pos);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 라인 렌더러를 파괴합니다.
	private void _RemoveLineRenderer(DebugRender pState)
	{
		if (mLineRenderers[(int)pState] == null)
			return;

		Destroy(mLineRenderers[(int)pState].gameObject);
		mLineRenderers[(int)pState] = null;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private StageTroop mStageTroop;
	private LineRenderer[] mLineRenderers;
	private TextMesh[] mTextMeshs;
	private int mTextCount;
}
