using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroopShadow : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Vector3 vShadowScale = new Vector3(1.5f, 0.8f, 1.0f);
	public Vector3 vShadowOffset = new Vector3(0.25f, 0.1f, 0.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mTransform = transform;
		mParent = mTransform.parent;
//		mRotation = mTransform.rotation;
		mRotation = Quaternion.Euler(90.0f, 0.0f, 0.0f);

//vShadowScale.Scale(new Vector3(0.8f, 0.8f, 0.8f));

		mTransform.localScale = new Vector3(
			mTransform.localScale.x * vShadowScale.x,
			mTransform.localScale.y * vShadowScale.y,
			mTransform.localScale.z * vShadowScale.z);

//gameObject.layer = LayerMask.NameToLayer("Default");
	}
	//-------------------------------------------------------------------------------------------------------
	// 늦은 갱신 콜백
	void LateUpdate()
	{
		Vector3 lPositionOffset = new Vector3(
			vShadowOffset.x * mTransform.localScale.x,
			vShadowOffset.y,
			vShadowOffset.z * mTransform.localScale.z);
		mTransform.position = mParent.position + lPositionOffset;

		mTransform.rotation = mRotation;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Transform mParent;
	private Quaternion mRotation;
}
