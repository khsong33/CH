using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageCommanderClick : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattleUITroopMark vTroopMark;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vTroopMark == null)
			Debug.LogError("vTroopMark is empty - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnClick()
	{
		//Debug.Log("OnClick() - " + vTroopMark.aStageTroop.aBattleTroop);

		BattleCommander lControlCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
		if (lControlCommander == null)
			return;

		BattleTroop lTargetTroop = vTroopMark.aStageTroop.aBattleTroop;
		if (lTargetTroop.aTeamIdx == lControlCommander.aTeamIdx) // 같은 팀의 부대라면
		{
			// 유저 부대의 경우 그 부대의 지휘관을 포커싱합니다.
			if (lTargetTroop.aCommander.aUser != null)
				BattlePlay.main.vControlPanel.FocusCommander(
					lTargetTroop.aCommander,
					true,
					BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
					BattlePlay.main.vControlPanel.vDefaultFocusingDelay);
		}
		else // 다른 팀의 부대라면
		{
			// 그 부대를 공격합니다.
			BattlePlay.main.vStage.vStageTouchControl.TouchTargetTroop(lControlCommander, lTargetTroop);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
