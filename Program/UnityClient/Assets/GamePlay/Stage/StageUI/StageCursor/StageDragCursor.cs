﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageDragCursor : MonoBehaviour
{
	public delegate bool OnCheckValidPositionDelegate(Vector3 pPosition);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vValidPositionMark;
	public GameObject vInvalidPositionMark;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// Transform
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 유효한 좌표인지 여부
	public bool aIsValidPosition
	{
		get { return mIsValidPosition; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mTransform = transform;

		if (vValidPositionMark == null)
			Debug.LogError("vValidPositionMark is empty - " + this);
		if (vInvalidPositionMark == null)
			Debug.LogError("vInvalidPositionMark is empty - " + this);

		mIsValidPosition = true;
		vValidPositionMark.SetActive(mIsValidPosition);
		vInvalidPositionMark.SetActive(!mIsValidPosition);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 커서를 표시합니다.
	public void ShowCursor(OnCheckValidPositionDelegate pOnCheckValidPositionDelegate)
	{
		if (!gameObject.activeInHierarchy)
			gameObject.SetActive(true);
		else
			mIsHoltHiding = true; // 혹시 숨기는 중이었다면 중단

		_UpdateValidPosition(pOnCheckValidPositionDelegate);
	}
	//-------------------------------------------------------------------------------------------------------
	// 커서를 숨깁니다.
	public void HideCursor(float pHideDelay)
	{
		if(gameObject.activeInHierarchy)
			StartCoroutine(_HideCursor(pHideDelay));
	}
	//-------------------------------------------------------------------------------------------------------
	// 커서를 이동합니다.
	public void MovePosition(Vector3 pPos, OnCheckValidPositionDelegate pOnCheckValidPositionDelegate)
	{
		aTransform.position = pPos;

		_UpdateValidPosition(pOnCheckValidPositionDelegate);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 유효 좌표 여부를 지정합니다.
	private void _UpdateValidPosition(OnCheckValidPositionDelegate pOnCheckValidPositionDelegate)
	{
		bool lIsValidPosition = (pOnCheckValidPositionDelegate != null) ? pOnCheckValidPositionDelegate(mTransform.position) : true;
		if (mIsValidPosition == lIsValidPosition)
			return;

		mIsValidPosition = lIsValidPosition;
		vValidPositionMark.SetActive(lIsValidPosition);
		vInvalidPositionMark.SetActive(!lIsValidPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// 일정 시간 후에 숨깁니다.
	private IEnumerator _HideCursor(float pHideDelay)
	{
		mIsHoltHiding = false;

		float lHideTimer = pHideDelay;
		while (lHideTimer > 0.0f)
		{
			if (mIsHoltHiding)
				yield break;

			lHideTimer -= Time.deltaTime;
			yield return null;
		}
		gameObject.SetActive(false); // 비활성화 시킵니다.
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private bool mIsValidPosition;
	private bool mIsHoltHiding;
}
