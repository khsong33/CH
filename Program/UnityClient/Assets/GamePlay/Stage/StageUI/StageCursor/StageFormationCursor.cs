using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageFormationCursor : MonoBehaviour
{
	internal const float cHideDelay = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject[] vFormationObjects;
	public GameObject vInvalidFormationObject;
	public Transform[] vUpDownAnimationRoots;
	public float vAnimationHeight = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vFormationObjects.Length != CommanderTroopSet.cMaxTroopCount)
			Debug.LogError("vFormationObjects.Length mismatch - " + this);
		if (vInvalidFormationObject == null)
			Debug.LogError("vInvalidFormationObject is empty - " + this);
		if (vUpDownAnimationRoots.Length != CommanderTroopSet.cMaxTroopCount)
			Debug.LogError("vUpDownAnimationRoots.Length mismatch - " + this);

		mTransform = transform;
		mStage = BattlePlay.main.vStage;
		mFormationObjectTransforms = new Transform[CommanderTroopSet.cMaxTroopCount];
		mFormationObjectUpdateCodes = new int[CommanderTroopSet.cMaxTroopCount];
		mFormationUpdateCode = 0;
		mUpDownAnimationOffsets = new float[CommanderTroopSet.cMaxTroopCount];

		for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
		{
			mFormationObjectTransforms[iFormation] = vFormationObjects[iFormation].transform;
			mFormationObjectUpdateCodes[iFormation] = mFormationUpdateCode;

			if (vUpDownAnimationRoots[iFormation] != null)
				mUpDownAnimationOffsets[iFormation] = vUpDownAnimationRoots[iFormation].position.y - mTransform.position.y;
		}
		mInvalidObjectTransform = vInvalidFormationObject.transform;

		mBattleCommanderLink = new GuidLink<BattleCommander>();
		mLeaderObjectIdx = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		mIsAnimating = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 포메이션을 보입니다.
	public void ShowCommanderItemFormation(CommanderItem pCommanderItem, Coord2 pFormationCoord, int pFormationDirectionY)
	{
		mHideTimer = 0.0f;

		Vector3 lCursorPosition = Coord2.ToWorld(pFormationCoord);
// 		if (Terrain.activeTerrain != null)
// 			lCursorPosition.y = Terrain.activeTerrain.SampleHeight(lCursorPosition);
		mTransform.position = lCursorPosition;
		mTransform.rotation = MathLib.GetRotation(pFormationDirectionY);

		if (pCommanderItem != null)
			_SetCommanderItemFormationObjectPositions(pCommanderItem, pFormationCoord);
		else
			_SetNullFormationObjectPositions(pFormationCoord);

		if (!mIsAnimating)
			StartCoroutine(_AnimateCursor());
	}
	//-------------------------------------------------------------------------------------------------------
	// 포메이션을 보입니다.
	public void ShowCommanderFormation(BattleCommander pBattleCommander, Coord2 pFormationCoord, int pFormationDirectionY)
	{
		mHideTimer = 0.0f;

		Vector3 lCursorPosition = Coord2.ToWorld(pFormationCoord);
// 		if (Terrain.activeTerrain != null)
// 			lCursorPosition.y = Terrain.activeTerrain.SampleHeight(lCursorPosition);
		mTransform.position = lCursorPosition;
		mTransform.rotation = MathLib.GetRotation(pFormationDirectionY);

		if (pBattleCommander.aSkill.aIsMovable)
			_SetCommanderFormationObjectPositions(pBattleCommander, pFormationCoord);
		else
			_SetInvalidFormationObjectPosition(pFormationCoord);

		if (!mIsAnimating)
			StartCoroutine(_AnimateCursor());
	}
	//-------------------------------------------------------------------------------------------------------
	// 포메이션을 숨깁니다.
	public void HideFormation()
	{
		mHideTimer = cHideDelay;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 지휘관 상황을 고려하지 않은 포메이션 오브젝트의 위치를 지정합니다.
	private void _SetCommanderItemFormationObjectPositions(CommanderItem pCommanderItem, Coord2 pFormationCoord)
	{
		++mFormationUpdateCode;

		// 리더용 오브젝트의 위치를 갱신합니다.
		int lLeaderTroopFormationIdx = 0;
		if (mLeaderObjectIdx != lLeaderTroopFormationIdx)
		{
			GameObject lTempObject = vFormationObjects[mLeaderObjectIdx];
			vFormationObjects[mLeaderObjectIdx] = vFormationObjects[lLeaderTroopFormationIdx];
			vFormationObjects[lLeaderTroopFormationIdx] = lTempObject;

			Transform lTempTransform = mFormationObjectTransforms[mLeaderObjectIdx];
			mFormationObjectTransforms[mLeaderObjectIdx] = mFormationObjectTransforms[lLeaderTroopFormationIdx];
			mFormationObjectTransforms[lLeaderTroopFormationIdx] = lTempTransform;

			//Debug.Log(String.Format("mLeaderObjectIdx={0}<-{1}", lLeaderTroopFormationIdx, mLeaderObjectIdx));
			mLeaderObjectIdx = lLeaderTroopFormationIdx;
		}

		// 포메이션 오브젝트들의 위치를 구합니다.
		int lFirstGroupCount = 0;
		int lSecondGroupCount = 0;
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			if (pCommanderItem.mTroopSet.mTroopInfos[iTroop] == null)
				continue;

			int lFormationIdx;
			if (pCommanderItem.mTroopSet.mFormationGroupIds[iTroop] == 0)
				lFormationIdx = pCommanderItem.mTroopSet.mFirstGroupIdxs[lFirstGroupCount++]; // 첫 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당
			else
				lFormationIdx = pCommanderItem.mTroopSet.mSecondGroupIdxs[lSecondGroupCount++]; // 두 번째 그룹의 활동 부대에게 포메이션 인덱스를 할당

			_SetTerrainLocalPosition(
				mFormationObjectTransforms[lFormationIdx],
				Coord2.ToWorld(pCommanderItem.mTroopSet.mFormationPositionOffsets[lFormationIdx]));
			mFormationObjectUpdateCodes[lFormationIdx] = mFormationUpdateCode;
		}

		// 포메이션 오브젝트들의 보이기 여부를 지정합니다.
		int lValidFormationObjectCount = 0;
		for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
		{
			bool lIsValidFormation = (mFormationObjectUpdateCodes[iFormation] == mFormationUpdateCode);
			if (vFormationObjects[iFormation].activeInHierarchy != lIsValidFormation)
				vFormationObjects[iFormation].SetActive(lIsValidFormation);
			if (lIsValidFormation)
				++lValidFormationObjectCount;
		}

		// 아무 포메이션도 보이지 않는다면 이동 불가 포메이션 커서를 보입니다.
		if (lValidFormationObjectCount <= 0)
			mInvalidObjectTransform.rotation = Quaternion.identity;
		if (vInvalidFormationObject.activeInHierarchy != (lValidFormationObjectCount <= 0))
			vInvalidFormationObject.SetActive(lValidFormationObjectCount <= 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 불가 포메이션 위치를 지정합니다.
	private void _SetNullFormationObjectPositions(Coord2 pFormationCoord)
	{
		++mFormationUpdateCode;

		// 포메이션 오브젝트들의 보이기 여부를 지정합니다.
		for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
			if (vFormationObjects[iFormation].activeInHierarchy)
				vFormationObjects[iFormation].SetActive(false);

		// 아무 포메이션도 보이지 않는다면 이동 불가 포메이션 커서를 보입니다.
		mInvalidObjectTransform.rotation = Quaternion.identity;
		if (!vInvalidFormationObject.activeInHierarchy)
			vInvalidFormationObject.SetActive(true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관의 상태를 고려한 포메이션 오브젝트의 위치를 지정합니다.
	private void _SetCommanderFormationObjectPositions(BattleCommander pBattleCommander, Coord2 pFormationCoord)
	{
		mBattleCommanderLink.Set(pBattleCommander);

		++mFormationUpdateCode;

		// 리더용 오브젝트의 위치를 갱신합니다.
		int lLeaderTroopFormationIdx = pBattleCommander.GetTroopFormationIdx(pBattleCommander.aLeaderTroop.aTroopSlotIdx);
		if (mLeaderObjectIdx != lLeaderTroopFormationIdx)
		{
			GameObject lTempObject = vFormationObjects[mLeaderObjectIdx];
			vFormationObjects[mLeaderObjectIdx] = vFormationObjects[lLeaderTroopFormationIdx];
			vFormationObjects[lLeaderTroopFormationIdx] = lTempObject;

			Transform lTempTransform = mFormationObjectTransforms[mLeaderObjectIdx];
			mFormationObjectTransforms[mLeaderObjectIdx] = mFormationObjectTransforms[lLeaderTroopFormationIdx];
			mFormationObjectTransforms[lLeaderTroopFormationIdx] = lTempTransform;

			//Debug.Log(String.Format("mLeaderObjectIdx={0}<-{1}", lLeaderTroopFormationIdx, mLeaderObjectIdx));
			mLeaderObjectIdx = lLeaderTroopFormationIdx;
		}

		// 포메이션 오브젝트들의 위치를 구합니다.
		for (int iTroop = 0; iTroop < CommanderTroopSet.cMaxTroopCount; ++iTroop)
		{
			BattleTroop lTroop = pBattleCommander.aTroops[iTroop];
			if (lTroop == null)
				continue;
			if ((lTroop.aStat.aMoraleState == TroopMoraleState.PinDown) || // 핀다운 상태거나
				lTroop.aIsFallBehind) // 낙오 상태라면
				continue; // 포메이션에서 제외

			int lFormationIdx = pBattleCommander.GetTroopFormationIdx(iTroop);

			_SetTerrainLocalPosition(
				mFormationObjectTransforms[lFormationIdx],
				Coord2.ToWorld(pBattleCommander.aTroopSet.mFormationPositionOffsets[lFormationIdx]));
			mFormationObjectUpdateCodes[lFormationIdx] = mFormationUpdateCode;
		}

		// 포메이션 오브젝트들의 보이기 여부를 지정합니다.
		int lValidFormationObjectCount = 0;
		for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
		{
			bool lIsValidFormation = (mFormationObjectUpdateCodes[iFormation] == mFormationUpdateCode);
			if (vFormationObjects[iFormation].activeInHierarchy != lIsValidFormation)
				vFormationObjects[iFormation].SetActive(lIsValidFormation);
			if (lIsValidFormation)
				++lValidFormationObjectCount;
		}

		// 아무 포메이션도 보이지 않는다면 이동 불가 포메이션 커서를 보입니다.
		if (lValidFormationObjectCount <= 0)
			mInvalidObjectTransform.rotation = Quaternion.identity;
		if (vInvalidFormationObject.activeInHierarchy != (lValidFormationObjectCount <= 0))
			vInvalidFormationObject.SetActive(lValidFormationObjectCount <= 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 불가 포메이션 오브젝트의 위치를 지정합니다.
	private void _SetInvalidFormationObjectPosition(Coord2 pFormationCoord)
	{
		// 포메이션 오브젝트들의 보이기 여부를 지정합니다.
		for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
			if (vFormationObjects[iFormation].activeInHierarchy)
				vFormationObjects[iFormation].SetActive(false);

		// 아무 포메이션도 보이지 않는다면 이동 불가 포메이션 커서를 보입니다.
		if (!vInvalidFormationObject.activeInHierarchy)
			vInvalidFormationObject.SetActive(true);

		mInvalidObjectTransform.rotation = Quaternion.identity;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 애니메이션을 보입니다.
	private IEnumerator _AnimateCursor()
	{
		mIsAnimating = true;

		float lAnimationFactor = 0.0f;

		for (; ; )
		{
			BattleCommander lBattleCommander = mBattleCommanderLink.Get();
			if (lBattleCommander != null)
			{
				if (!lBattleCommander.aIsAlive)
					break; // 부대 사라짐

				if (lBattleCommander != BattlePlay.main.vControlPanel.aFocusingCommander)
					break; // 포커싱 지휘관이 아님
			}

			lAnimationFactor += Time.deltaTime;
			lAnimationFactor -= (int)lAnimationFactor;
			float lAnimationOffsetY = Mathf.Sin(lAnimationFactor * Mathf.PI * 2.0f) * vAnimationHeight;

			for (int iFormation = 0; iFormation < CommanderTroopSet.cMaxTroopCount; ++iFormation)
			{
				if (vUpDownAnimationRoots[iFormation] != null)
				{
					Vector3 lAnimationLocalPosition = vUpDownAnimationRoots[iFormation].localPosition;
					lAnimationLocalPosition.y = mUpDownAnimationOffsets[iFormation] + lAnimationOffsetY;
					_SetTerrainLocalPosition(vUpDownAnimationRoots[iFormation], lAnimationLocalPosition);
				}
			}

			yield return null;

			if (mHideTimer > 0.0f)
			{
				mHideTimer -= Time.deltaTime; // 타이머 감소
				if (mHideTimer <= 0.0f)
					break;
			}
		}

		gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬 터레인 좌표를 지정합니다.
	private void _SetTerrainLocalPosition(Transform pSettingTransform, Vector3 pLocalPosition)
	{
		pSettingTransform.localPosition = pLocalPosition;
		pSettingTransform.position = mStage.GetTerrainPosition(pSettingTransform.position);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Stage mStage;
	private Transform[] mFormationObjectTransforms;
	private Transform mInvalidObjectTransform;
	private int[] mFormationObjectUpdateCodes;
	private int mFormationUpdateCode;
	private float[] mUpDownAnimationOffsets;

	private GuidLink<BattleCommander> mBattleCommanderLink;
	private bool mIsAnimating;
	private float mHideTimer;
	private int mLeaderObjectIdx;
}
