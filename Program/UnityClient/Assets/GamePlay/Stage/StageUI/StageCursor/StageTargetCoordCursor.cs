using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTargetCoordCursor : MonoBehaviour
{
	internal const float cFlashDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vFlashAnimationRoot;
	public Transform vUpDownAnimationRoot;

	public float vArrivalRadius = 5.0f;
	public float vAnimationHeight = 1.0f;
	public float vMinShowDelay = 2.0f;
	public float vMoveCursorShowDelay = 1.0f;
	public float vSpawnCursorShowDelay = 1.0f;

	public GameObject vMoveCursorObject;
	public GameObject vSpawnCursorObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vMoveCursorObject == null)
			Debug.Log("vMoveCursorObject is empty - " + this);
		if (vSpawnCursorObject == null)
			Debug.Log("vSpawnCursorObject is empty - " + this);

		mTransform = transform;
		mStage = BattlePlay.main.vStage;

		if (vFlashAnimationRoot != null)
			mFlashBaseScale = vFlashAnimationRoot.localScale;

		if (vUpDownAnimationRoot != null)
			mAnimationOffset = vUpDownAnimationRoot.position - mTransform.position;

		mSqrArrivalRadius = vArrivalRadius * vArrivalRadius;

		mBattleCommanderLink = new GuidLink<BattleCommander>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
// 	void OnEnable()
// 	{
// 	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이동 좌표를 보입니다.
	public void ShowMoveCoord(BattleCommander pBattleCommander, Coord2 pMoveCoord, bool pIsSkipWhenArrival, bool pIsFlash)
	{
		if (pIsSkipWhenArrival &&
			(pBattleCommander != null))
		{
			Vector3 lPositionVector = pBattleCommander.aStageCommander.aWorldPosition - Coord2.ToWorld(pMoveCoord);
			if ((lPositionVector.x * lPositionVector.x + lPositionVector.z * lPositionVector.z) <= mSqrArrivalRadius)
			{
				gameObject.SetActive(false);
				return; // 이미 도착함
			}
		}

		vMoveCursorObject.SetActive(true);
		vSpawnCursorObject.SetActive(false);

		Vector3 lCursorPosition = Coord2.ToWorld(pMoveCoord);
// 		if (Terrain.activeTerrain != null)
// 			lCursorPosition.y = Terrain.activeTerrain.SampleHeight(lCursorPosition);
		mTransform.position = mStage.GetTerrainPosition(lCursorPosition);

		mBattleCommanderLink.Set(pBattleCommander);
		mShowTime = 0.0f;

		StartCoroutine(_ShowCursor(vMoveCursorShowDelay, pIsFlash));
	}
	//-------------------------------------------------------------------------------------------------------
	// 스폰 좌표를 보입니다.
	public void ShowSpawnCoord(BattleCommander pBattleCommander, Coord2 pMoveCoord)
	{
		vMoveCursorObject.SetActive(false);
		vSpawnCursorObject.SetActive(true);

		Vector3 lCursorPosition = Coord2.ToWorld(pMoveCoord);
// 		if (Terrain.activeTerrain != null)
// 			lCursorPosition.y = Terrain.activeTerrain.SampleHeight(lCursorPosition);
		mTransform.position = mStage.GetTerrainPosition(lCursorPosition);

		mBattleCommanderLink.Set(pBattleCommander);
		mShowTime = 0.0f;

		StartCoroutine(_ShowCursor(vSpawnCursorShowDelay, false));
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 좌표를 보입니다.
	public void ShowSkillCoord(BattleCommander pBattleCommander, Coord2 pSkillCoord, int pSkillDirectionY)
	{
		// 임시 구현
		ShowSpawnCoord(pBattleCommander, pSkillCoord);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 커서를 보입니다.
	private IEnumerator _ShowCursor(float pShowDelay, bool pIsFlash)
	{
		if (vFlashAnimationRoot != null)
		{
			if (pIsFlash)
			{
				float lFlashTimer = 0.0f;
				while (lFlashTimer < cFlashDelay)
				{
					float lLerpFactor = lFlashTimer / cFlashDelay;

					Vector3 lFlashScale = mFlashBaseScale * (2.0f - lLerpFactor);
					vFlashAnimationRoot.localScale = lFlashScale;

					lFlashTimer += Time.deltaTime;
					yield return null;
				}
			}
			vFlashAnimationRoot.localScale = mFlashBaseScale;
		}

		float lAnimationFactor = 0.0f;

		for (;;)
		{
			BattleCommander lBattleCommander = mBattleCommanderLink.Get();
			if (lBattleCommander != null)
			{
				if (!lBattleCommander.aIsAlive)
					break; // 부대 사라짐

				if (lBattleCommander != BattlePlay.main.vControlPanel.aFocusingCommander)
					break; // 포커싱 지휘관이 아님

				Vector3 lPositionVector = lBattleCommander.aStageCommander.aWorldPosition - mTransform.position;
				if ((mShowTime > vMinShowDelay) &&
					((lPositionVector.x * lPositionVector.x + lPositionVector.z * lPositionVector.z) <= mSqrArrivalRadius))
					break; // 부대 도착함
			}

			if (vUpDownAnimationRoot != null)
			{
				lAnimationFactor += Time.deltaTime;
				lAnimationFactor -= (int)lAnimationFactor;

				Vector3 lAnimationLocalPosition = mAnimationOffset;
				lAnimationLocalPosition.y += Mathf.Sin(lAnimationFactor * Mathf.PI * 2.0f) * vAnimationHeight;
				vUpDownAnimationRoot.localPosition = lAnimationLocalPosition;
			}

			mShowTime += Time.deltaTime; // 시간 증가
			yield return null; // 다음 프레임으로

			if ((pShowDelay > 0.0f) &&
				(mShowTime > pShowDelay))
				break;
		}

		gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Stage mStage;
	private Vector3 mFlashBaseScale;
	private Vector3 mAnimationOffset;
	private float mSqrArrivalRadius;

	private GuidLink<BattleCommander> mBattleCommanderLink;
	private float mShowTime;
}
