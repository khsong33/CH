using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTargetTroopCursor : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vAnimationTransform;

	public float vAnimationHeight = 1.0f;

	public GameObject vMoveCursorObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vMoveCursorObject == null)
			Debug.Log("vMoveCursorObject is empty - " + this);

		mTransform = transform;

		if (vAnimationTransform != null)
			mAnimationOffset = vAnimationTransform.position - mTransform.position;

		mBattleCommanderLink = new GuidLink<BattleCommander>();
		mTargetTroopLink = new GuidLink<BattleTroop>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		mIsShowCursor = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 잠시 보입니다.
	public void FlashTargetTroop(BattleCommander pBattleCommander, BattleTroop pTargetTroop)
	{
		vMoveCursorObject.SetActive(true);

		mTargetTroopLink.Set(pTargetTroop);

		mBattleCommanderLink.Set(pBattleCommander);

		if (!mIsShowCursor)
			BattlePlay.main.vStage.StartCoroutine(_ShowCursor(0.0f));
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 커서를 보입니다.
	private IEnumerator _ShowCursor(float pShowDelay)
	{
		mIsShowCursor = true;

		float lAnimationFactor = 0.0f;

		for (;;)
		{
			BattleCommander lBattleCommander = mBattleCommanderLink.Get();
			if (lBattleCommander != null)
			{
				if (!lBattleCommander.aIsAlive)
					break; // 지휘관 부대 전멸

				if (lBattleCommander != BattlePlay.main.vControlPanel.aFocusingCommander)
					break; // 포커싱 지휘관이 아님
			}

			BattleTroop lTargetTroop = mTargetTroopLink.Get();
			if (lTargetTroop == null)
				break; // 타겟 부대 사라짐

			Vector3 lCursorPosition = lTargetTroop.aStageTroop.aWorldPosition;
// 			if (Terrain.activeTerrain != null)
// 				lCursorPosition.y = Terrain.activeTerrain.SampleHeight(lCursorPosition);
			mTransform.position = lCursorPosition;

			if (vAnimationTransform != null)
			{
				lAnimationFactor += Time.deltaTime;
				lAnimationFactor -= (int)lAnimationFactor;

				Vector3 lAnimationLocalPosition = mAnimationOffset;
				lAnimationLocalPosition.y += Mathf.Sin(lAnimationFactor * Mathf.PI * 2.0f) * vAnimationHeight;
				vAnimationTransform.localPosition = lAnimationLocalPosition;
			}

			yield return null; // 다음 프레임으로
		}

		mIsShowCursor = false;
		gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mAnimationOffset;

	private GuidLink<BattleCommander> mBattleCommanderLink;
	private GuidLink<BattleTroop> mTargetTroopLink;
	private bool mIsShowCursor;
}
