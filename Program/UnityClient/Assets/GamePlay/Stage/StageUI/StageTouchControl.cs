//#define GRAPHICS_MOVE_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTouchControl : MonoBehaviour
{
	public delegate void OnSpawnCommanderDelegate(BattleUser pUser, CommanderItem pCommanderItem, Coord2 pSpawnCoord);
	public delegate void OnSpawnCommanderAndGrabCheckPointDelegate(BattleUser pUser, CommanderItem pCommanderItem, BattleCheckPoint pCheckPoint);
	public delegate void OnSpawnCommanderAndMoveToEventPointDelegate(BattleUser pUser, CommanderItem pCommanderItem, BattleEventPoint pEventPoint);
	public delegate void OnSpawnCommanderAndAttackTroopDelegate(BattleUser pUser, CommanderItem pCommanderItem, BattleTroop pTargetTroop);
	public delegate void OnMoveTroopsDelegate(BattleCommander pCommander, Coord2 pMoveCoord, int pMoveDirectionY);
	public delegate void OnMoveTroopsToEventPointDelegate(BattleCommander pCommander, BattleEventPoint pEventPoint, int pMoveDirectionY);
	public delegate void OnGrabCheckPointDelegate(BattleCommander pCommander, BattleCheckPoint pCheckPoint);
	public delegate void OnRetreatCommanderDelegate(BattleCommander pCommander);
	public delegate void OnAttackTroopDelegate(BattleCommander pCommander, BattleTroop pTargetTroop);
	public delegate void OnCommanderSkillStartDelegate(BattleCommander pCommander, Coord2 pSkillCoord, int pSkillDirectionY);
	public delegate void OnCommanderSkillStopDelegate(BattleCommander pCommander);
	public delegate void OnFocusCommander(BattleCommander pCommander);

	public enum DragMode
	{
		SpawnDrag,
		MoveDrag,
		SkillDrag,
	}
	public enum DragResult
	{
		None,
		SpawnCommander,
		MoveCommander,
		UseSkill,
	}

	internal float cDragCursorHideDelay = 1.0f;
	internal float cFormationStartRadius = 10.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Stage vStage;	
	public TouchPanel vTouchPanel;
	public QuaterViewCameraSet vQuaterViewCameraSet;

	public StageDragCursor vStageDragCursor;

	public StageTargetCoordCursor vStageTargetCoordCursor;
	public StageTargetTroopCursor vStageTargetTroopCursor;
	public StageFormationCursor vStageFormationCursor;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 지휘 입력 처리 여부
	public bool aIsCommandable
	{
		get { return mIsCommandable; }
		set { mIsCommandable = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 입력 이벤트 콜백
	public OnSpawnCommanderDelegate aOnSpawnCommanderDelegate { get; set; }
	public OnSpawnCommanderAndGrabCheckPointDelegate aOnSpawnCommanderAndGrabCheckPointDelegate { get; set; }
	public OnSpawnCommanderAndMoveToEventPointDelegate aOnSpawnCommanderAndMoveToEventPointDelegate { get; set; }
	public OnSpawnCommanderAndAttackTroopDelegate aOnSpawnCommanderAndAttackTroopDelegate { get; set; }
	public OnMoveTroopsDelegate aOnMoveTroopsDelegate { get; set; }
	public OnMoveTroopsToEventPointDelegate aOnMoveTroopsToEventPointDelegate { get; set; }
	public OnGrabCheckPointDelegate aOnGrabCheckPointDelegate { get; set; }
	public OnRetreatCommanderDelegate aOnRetreatCommanderDelegate { get; set; }
	public OnAttackTroopDelegate aOnAttackTroopDelegate { get; set; }
	public OnCommanderSkillStartDelegate aOnCommanderSkillStartDelegate { get; set; }
	public OnCommanderSkillStopDelegate aOnCommanderSkillStopDelegate { get; set; }
	public OnFocusCommander aOnFocusCommander { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		if (vStage == null)
			Debug.LogError("vStage is empty - " + this);
		if (vTouchPanel == null)
			Debug.LogError("vTouchPanel is empty - " + this);
		if (vQuaterViewCameraSet == null)
			Debug.LogError("vQuaterViewCameraSet is empty - " + this);

		if (vStageDragCursor == null)
			Debug.LogError("vStageDragCursor is empty - " + this);

		if (vStageTargetCoordCursor == null)
			Debug.LogError("vStageTargetCoordCursor is empty - " + this);
		if (vStageTargetTroopCursor == null)
			Debug.LogError("vStageTargetTroopCursor is empty - " + this);
		if (vStageFormationCursor == null)
			Debug.LogError("vStageFormationCursor is empty - " + this);

		vStageDragCursor.gameObject.SetActive(false);

		mCommanderCursorObjects = new GameObject[3];
		mCommanderCursorObjects[0] = vStageTargetCoordCursor.gameObject;
		mCommanderCursorObjects[1] = vStageTargetTroopCursor.gameObject;
		mCommanderCursorObjects[2] = vStageFormationCursor.gameObject;
		for (int iCursor = 0; iCursor < mCommanderCursorObjects.Length; ++iCursor)
			mCommanderCursorObjects[iCursor].SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		vTouchPanel.aOnHoldDelegate += _OnHold;
		vTouchPanel.aOnDragStartDelegate += _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate += _OnDragMove;
		vTouchPanel.aOnDragEndDelegate += _OnDragEnd;
		vTouchPanel.aOnClickDelegate += _OnClickTouchPanel;
		vTouchPanel.aOnZoomDelegate += _OnZoom;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		vTouchPanel.aOnHoldDelegate -= _OnHold;
		vTouchPanel.aOnDragStartDelegate -= _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate -= _OnDragMove;
		vTouchPanel.aOnDragEndDelegate -= _OnDragEnd;
		vTouchPanel.aOnClickDelegate -= _OnClickTouchPanel;
		vTouchPanel.aOnZoomDelegate -= _OnZoom;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 보입니다.
	public void ShowDragCursor(DragMode pDragMode, StageDragCursor.OnCheckValidPositionDelegate pOnCheckValidPositionDelegate)
	{
		vStageDragCursor.ShowCursor(pOnCheckValidPositionDelegate);
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 움직입니다.
	public void MoveDragCursor(Vector2 pDragPoint, StageDragCursor.OnCheckValidPositionDelegate pOnCheckValidPositionDelegate)
	{
		mDragPoint = pDragPoint;
		vStageDragCursor.MovePosition(vTouchPanel.GetTouchPosition(pDragPoint), pOnCheckValidPositionDelegate);
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 숨깁니다.
	public void HideDragCursor()
	{
		vStageDragCursor.HideCursor(0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 숨기고 그 자리에 지휘관을 스폰합니다.
	public void HideDragCursorAndSpawn(BattleUser pUser, CommanderItem pCommanderItem)
	{
		if (vStageDragCursor.gameObject.activeInHierarchy)
		{
			vStageDragCursor.HideCursor(cDragCursorHideDelay);
			if (!vStageDragCursor.aIsValidPosition)
				return; // 유효한 좌표가 아니라서 이후 진행 안 함
		}

		// 먼저 스폰 후에 상호 작용할 오브젝트 클릭을 시도합니다.
		Ray lTouchRay = vQuaterViewCameraSet.aMainCamera.ScreenPointToRay(mDragPoint);
		RaycastHit lRaycastHit;
		if (Physics.Raycast(lTouchRay, out lRaycastHit))
		{
			bool lIsColliderProcessed = false;
			StageObject lStageObject = lRaycastHit.collider.GetComponent<StageObject>();
			if (lStageObject != null)
			{
				// 거점 클릭을 처리합니다.
				BattleCheckPoint lCheckPoint = lStageObject.aBattleObject as BattleCheckPoint;
				if (lCheckPoint != null)
				{
					if ((lCheckPoint.aLinkHeadquarter == null) ||
						(lCheckPoint.aTeamIdx != pUser.aTeamIdx)) // 아군 본부가 아니라면
					{
						BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommanderAndGrabCheckPoint(pUser, pCommanderItem, lCheckPoint); // 스폰 후 거점으로 이동
						if (aOnSpawnCommanderAndGrabCheckPointDelegate != null)
							aOnSpawnCommanderAndGrabCheckPointDelegate(pUser, pCommanderItem, lCheckPoint);

						_ActivateCommanderCursorObject(vStageTargetCoordCursor.gameObject);
						vStageTargetCoordCursor.ShowMoveCoord(null, lCheckPoint.aCoord, false, true); // 이동 커서 보임

						return; // 처리 완료
					}
					lIsColliderProcessed = true;
				}

				// 이벤트 지점 클릭을 처리합니다.
				BattleEventPoint lEventPoint = lStageObject.aBattleObject as BattleEventPoint;
				if (lEventPoint != null)
				{
					BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommander(pUser, pCommanderItem, lEventPoint.aCoord); // 스폰 후 이벤트 지점으로 이동
					if (aOnSpawnCommanderAndMoveToEventPointDelegate != null)
						aOnSpawnCommanderAndMoveToEventPointDelegate(pUser, pCommanderItem, lEventPoint);

					FlashCommanderItemFormationCursor(
						pCommanderItem,
						lEventPoint.aCoord,
						BattleCommander.GetFormationMoveDirectionY(lEventPoint.aCoord, pUser.aHeadquarter.aSpawnCoord));

					lIsColliderProcessed = true;
					return; // 처리 완료
				}
			}

			StageTroopTag lStageTroopTag = lRaycastHit.collider.GetComponent<StageTroopTag>();
			if (lStageTroopTag != null)
			{
				BattleTroop lTargetTroop = (lStageTroopTag.vStageTroop != null) ? lStageTroopTag.vStageTroop.aBattleTroop : null;
				if (lTargetTroop != null)
				{
					if (lTargetTroop.aTeamIdx != pUser.aTeamIdx) // 다른 팀의 부대라면
					{
						BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommanderAndAttackTroop(pUser, pCommanderItem, lTargetTroop); // 스폰 후 그 부대를 공격
						if (aOnSpawnCommanderAndAttackTroopDelegate != null)
							aOnSpawnCommanderAndAttackTroopDelegate(pUser, pCommanderItem, lTargetTroop);

						_ActivateCommanderCursorObject(vStageTargetTroopCursor.gameObject);
						vStageTargetTroopCursor.FlashTargetTroop(null, lTargetTroop);

						lIsColliderProcessed = true;
						return; // 처리 완료
					}				
				}
			}

			// [버그 상황] 알 수 없는 충돌체에 대해서는 로그를 남깁니다.
			if (!lIsColliderProcessed)
				Debug.LogError("suspected collider:" + lRaycastHit.collider + " - " + this);
		}

		// 오브젝트 클릭이 없다면 그냥 스폰합니다.
		Coord2 lSpawnCoord = Coord2.ToCoord(vStageDragCursor.aTransform.position);
		BattleGridTile lSpawnGridTile = BattlePlay.main.aBattleGrid.GetTile(lSpawnCoord);
		if ((lSpawnGridTile != null) &&
			!lSpawnGridTile.aIsBlocked)
		{
			BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommander(pUser, pCommanderItem, lSpawnCoord);
			if (aOnSpawnCommanderDelegate != null)
				aOnSpawnCommanderDelegate(pUser, pCommanderItem, lSpawnCoord);

			FlashCommanderItemFormationCursor(
				pCommanderItem,
				lSpawnCoord,
				BattleCommander.GetFormationMoveDirectionY(lSpawnCoord, pUser.aHeadquarter.aSpawnCoord));
		}
		else
		{
			FlashCommanderItemFormationCursor(
				null,
				lSpawnCoord,
				BattleCommander.GetFormationMoveDirectionY(lSpawnCoord, pUser.aHeadquarter.aSpawnCoord));
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 숨기고 그 자리로 지휘관을 이동시킵니다.
	public void HideDragCursorAndMove(BattleCommander pCommander)
	{
		if (vStageDragCursor.gameObject.activeInHierarchy)
		{
			vStageDragCursor.HideCursor(cDragCursorHideDelay);
			if (!vStageDragCursor.aIsValidPosition)
				return; // 유효한 좌표가 아니라서 이후 진행 안 함
		}

		if (pCommander != BattlePlay.main.vControlPanel.aFocusingCommander)
			BattlePlay.main.vControlPanel.FocusCommander(pCommander, true); // 지휘관을 포커싱

		Vector3 lMovePosition = vTouchPanel.GetTouchPosition(mDragPoint);
		_OnClick(lMovePosition, mDragPoint, false, pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 커서를 숨기고 그 자리에 스킬을 사용합니다.
	public void HideDragCursorAndUseSkill(BattleCommanderSkill pCommanderSkill)
	{
		if (vStageDragCursor.gameObject.activeInHierarchy)
		{
			vStageDragCursor.HideCursor(cDragCursorHideDelay);
			if (!vStageDragCursor.aIsValidPosition)
				return; // 유효한 좌표가 아니라서 이후 진행 안 함
		}

		if (!pCommanderSkill.aIsStarted)
		{
			Coord2 lSkillCoord = Coord2.ToCoord(vStageDragCursor.aTransform.position);
			int lSkillDirectionY = BattleCommander.GetFormationMoveDirectionY(lSkillCoord, pCommanderSkill.aCommander.aCoord);

			BattlePlay.main.vBattleNetwork.SetFrameInput_CommanderSkillStart(pCommanderSkill.aCommander, lSkillCoord, lSkillDirectionY);
			if (aOnCommanderSkillStartDelegate != null)
				aOnCommanderSkillStartDelegate(pCommanderSkill.aCommander, lSkillCoord, lSkillDirectionY);

			FlashCommanderSkillCursor(pCommanderSkill.aCommander, lSkillCoord, lSkillDirectionY);
		}
		else
		{
			BattlePlay.main.vBattleNetwork.SetFrameInput_CommanderSkillStop(pCommanderSkill.aCommander);
			if (aOnCommanderSkillStopDelegate != null)
				aOnCommanderSkillStopDelegate(pCommanderSkill.aCommander);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 터치합니다.
	public void TouchTargetTroop(BattleCommander pCommander, BattleTroop pTargetTroop)
	{
		//Debug.Log("TouchTargetTroop() pTargetTroop=" + pTargetTroop);

		if (!pCommander.aIsAlive)
			return;

		_ActivateCommanderCursorObject(vStageTargetTroopCursor.gameObject);
		vStageTargetTroopCursor.FlashTargetTroop(pCommander, pTargetTroop);

		BattlePlay.main.vBattleNetwork.SetFrameInput_AttackTroop(pCommander, pTargetTroop);
		if (aOnAttackTroopDelegate != null)
			aOnAttackTroopDelegate(pCommander, pTargetTroop);
	}
	//-------------------------------------------------------------------------------------------------------
	// 타겟 부대를 터치합니다.
	public void TouchTargetCommander(BattleCommander pCommander, BattleCommander pTargetCommander)
	{
		if (pCommander == null)
			return;
		if(!pCommander.aIsAlive)
			return;
		
		if (pTargetCommander == null)
			return;
		if (!pTargetCommander.aIsAlive)
			return;

		BattleTroop lTargetTroop = pTargetCommander.aLeaderTroop;
		if (lTargetTroop.aTeamIdx == pTargetCommander.aTeamIdx) // 같은 팀의 부대라면
		{
			// 유저 부대의 경우 그 부대의 지휘관을 포커싱합니다.
			if (lTargetTroop.aCommander.aUser != null)
				BattlePlay.main.vControlPanel.FocusCommander(
					lTargetTroop.aCommander,
					true,
					BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
					BattlePlay.main.vControlPanel.vDefaultFocusingDelay);
		}
		else // 다른 팀의 부대라면
		{
			// 그 부대를 공격합니다.
			BattlePlay.main.vStage.vStageTouchControl.TouchTargetTroop(pTargetCommander, lTargetTroop);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 거점을 터치합니다.
	public void TouchCheckPoint(BattleCommander pCommander, BattleCheckPoint pCheckPoint)
	{
		//Debug.Log("TouchCheckPoint() pCheckPoint=" + pCheckPoint);
		if (pCommander == null)
			return;
		if (!pCommander.aIsAlive)
			return;

		if (pCheckPoint.aLinkHeadquarter != null &&
			pCheckPoint.aTeamIdx == pCommander.aTeamIdx) // 아군 본부라면
		{
			// 퇴각합니다.
			BattlePlay.main.vControlPanel.FocusCommander(
				pCommander,
				true,
				BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
				BattlePlay.main.vControlPanel.vDefaultFocusingDelay); // 퇴각하는 부대로 카메라 이동
			BattlePlay.main.vBattleNetwork.SetFrameInput_RetreatCommander(pCommander);
			if (aOnRetreatCommanderDelegate != null)
				aOnRetreatCommanderDelegate(pCommander);
		}
		else // 일반 거점이라면
		{
			// 거점 이동을 합니다.
			BattlePlay.main.vBattleNetwork.SetFrameInput_GrabCheckPoint(pCommander, pCheckPoint);
			if (aOnGrabCheckPointDelegate != null)
				aOnGrabCheckPointDelegate(pCommander, pCheckPoint);
		}
		_ActivateCommanderCursorObject(vStageTargetCoordCursor.gameObject);
		vStageTargetCoordCursor.ShowMoveCoord(pCommander, pCheckPoint.aCoord, false, true); // 이동 커서 보임
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 지점을 터치합니다.
	public void TouchEventPoint(BattleCommander pCommander, BattleEventPoint pEventPoint)
	{
		//Debug.Log("TouchEventPoint() pEventPoint=" + pEventPoint);
		if (pCommander == null)
			return;
		if (!pCommander.aIsAlive)
			return;

		int lMoveDirectionY = BattleCommander.GetFormationMoveDirectionY(pEventPoint.aCoord, pCommander.aCoord);
		BattlePlay.main.vBattleNetwork.SetFrameInput_MoveTroops(pCommander, pEventPoint.aCoord, lMoveDirectionY);
		if (aOnMoveTroopsToEventPointDelegate != null)
			aOnMoveTroopsToEventPointDelegate(pCommander, pEventPoint, lMoveDirectionY);

		_ActivateCommanderCursorObject(vStageTargetCoordCursor.gameObject);
		vStageTargetCoordCursor.ShowMoveCoord(pCommander, pEventPoint.aCoord, false, true);
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 지점을 터치합니다.
	public void TouchMoveCoord(BattleCommander pCommander, Coord2 pMoveCoord)
	{
		if (pCommander == null)
			return;
		if (!pCommander.aIsAlive)
			return;

		TouchMoveCoord(pCommander, pMoveCoord, BattleCommander.GetFormationMoveDirectionY(pMoveCoord, pCommander.aCoord));
	}
	//-------------------------------------------------------------------------------------------------------
	// 이동 지점을 터치합니다.
	public void TouchMoveCoord(BattleCommander pCommander, Coord2 pMoveCoord, int pMoveDirectionY)
	{
		//Debug.Log("TouchMoveCoord() pMoveCoord=" + pMoveCoord + " pMoveDirectionY=" + pMoveDirectionY);
		if (pCommander == null)
			return;
		if (!pCommander.aIsAlive)
			return;

		BattlePlay.main.vBattleNetwork.SetFrameInput_MoveTroops(pCommander, pMoveCoord, pMoveDirectionY);
		if (aOnMoveTroopsDelegate != null)
			aOnMoveTroopsDelegate(pCommander, pMoveCoord, pMoveDirectionY);

		FlashCommanderFormationCursor(pCommander, pMoveCoord, pMoveDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 진형 커서를 잠시보입니다.
	public void FlashCommanderItemFormationCursor(CommanderItem pCommanderItem, Coord2 pFormationCoord, int pFormationDirectionY)
	{
		_ActivateCommanderCursorObject(vStageFormationCursor.gameObject);
		vStageFormationCursor.ShowCommanderItemFormation(pCommanderItem, pFormationCoord, pFormationDirectionY);
		vStageFormationCursor.HideFormation();
	}
	//-------------------------------------------------------------------------------------------------------
	// 진형 커서를 잠시보입니다.
	public void FlashCommanderFormationCursor(BattleCommander pCommander, Coord2 pFormationCoord, int pFormationDirectionY)
	{
		_ActivateCommanderCursorObject(vStageFormationCursor.gameObject);
		vStageFormationCursor.ShowCommanderFormation(pCommander, pFormationCoord, pFormationDirectionY);
		vStageFormationCursor.HideFormation();
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬 커서를 잠시보입니다.
	public void FlashCommanderSkillCursor(BattleCommander pCommander, Coord2 pSkillCoord, int pSkillDirectionY)
	{
		_ActivateCommanderCursorObject(vStageTargetCoordCursor.gameObject);
		vStageTargetCoordCursor.ShowSkillCoord(pCommander, pSkillCoord, pSkillDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 진형 커서를 보입니다.
	public void ShowFormationCursor(BattleCommander pCommander, Coord2 pFormationCoord, int pFormationDirectionY)
	{
		_ActivateCommanderCursorObject(vStageFormationCursor.gameObject);
		vStageFormationCursor.ShowCommanderFormation(pCommander, pFormationCoord, pFormationDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 진형 커서를 숨깁니다.
	public void HideFormationCursor()
	{
		vStageFormationCursor.HideFormation();
	}
	//-------------------------------------------------------------------------------------------------------
	// 지휘관을 스폰합니다.
	public void SpawnCommander(BattleUser pUser, CommanderItem pCommanderItem, Coord2 pSpawnCoord)
	{
		BattlePlay.main.vBattleNetwork.SetFrameInput_SpawnCommander(pUser, pCommanderItem, pSpawnCoord);
		if (aOnSpawnCommanderDelegate != null)
			aOnSpawnCommanderDelegate(pUser, pCommanderItem, pSpawnCoord);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관을 포커싱합니다.
	public void FocusCommander(BattleCommander pCommander, bool pIsSelection, AnimationCurve pFocusingCurve, float pFocusingDelay)
	{
		BattlePlay.main.vControlPanel.FocusCommander(
			pCommander,
			pIsSelection,
			pFocusingCurve,
			pFocusingDelay);
		if (aOnFocusCommander != null)
			aOnFocusCommander(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 지휘관을 철수시킵니다.
	public void RetreatCommander(BattleCommander pCommander)
	{
		BattlePlay.main.vBattleNetwork.SetFrameInput_RetreatCommander(pCommander);
		if (aOnRetreatCommanderDelegate != null)
			aOnRetreatCommanderDelegate(pCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 시작합니다.
	public void StartSkill(BattleCommander pCommander, Coord2 pSkillTargetCoord, int pSkillDirectionY)
	{
		BattlePlay.main.vBattleNetwork.SetFrameInput_CommanderSkillStart(pCommander, pSkillTargetCoord, pSkillDirectionY);
		if (aOnCommanderSkillStartDelegate != null)
			aOnCommanderSkillStartDelegate(pCommander, pSkillTargetCoord, pSkillDirectionY);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스킬을 멈춥니다.
	public void StopSkill(BattleCommander pCommander)
	{
		BattlePlay.main.vBattleNetwork.SetFrameInput_CommanderSkillStop(pCommander);
		if (aOnCommanderSkillStopDelegate != null)
			aOnCommanderSkillStopDelegate(pCommander);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnHoldDelegate
	private void _OnHold(Plane pHoldPlane, Vector2 pHoldPoint, bool pIsHolding)
	{
		//Debug.Log("_OnHold() pIsHolding=" + pIsHolding);

		if (!mIsCommandable)
			return; // 지휘 입력을 받을 때에만 홀드 입력을 처리

		mIsHolding = pIsHolding;

		if (mIsHolding)
		{
			BattleCommander lFormationCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
			if (lFormationCommander != null)
			{
				mHoldPosition = vQuaterViewCameraSet.GetTouchPosition(pHoldPlane, pHoldPoint);

				mFormationCoord = Coord2.ToCoord(mHoldPosition);
				mFormationDirectionY = BattleCommander.GetFormationMoveDirectionY(mFormationCoord, lFormationCommander.aCoord);
				ShowFormationCursor(lFormationCommander, mFormationCoord, mFormationDirectionY);
			}
		}
		else
		{
			HideFormationCursor();

			BattleCommander lFormationCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
			if (lFormationCommander != null)
				TouchMoveCoord(lFormationCommander, mFormationCoord, mFormationDirectionY);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragStartDelegate
	private void _OnDragStart(Plane pDragPlane, Vector2 pDragStartPoint)
	{
		//Debug.Log("_OnDragStart()");

		if (!mIsHolding)
		{
			if (_CheckDraggable())
			{
				BattlePlay.main.vControlPanel.aIsDraggingStage = true;
				vQuaterViewCameraSet.DragStart(pDragPlane, pDragStartPoint);
			}
		}
		else
		{
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragMoveDelegate
	private void _OnDragMove(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount)
	{
		//Debug.Log("_OnDragMove()");

		if (!mIsHolding)
		{
			if (_CheckDraggable())
			{
				BattlePlay.main.vControlPanel.ResetFocusingApproach();
				vQuaterViewCameraSet.DragMove(pDragPlane, pDragLastPoint, pDragMovePoint, pDragDelay, pTouchCount);
			}
		}
		else
		{
			BattleCommander lFormationCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
			if (lFormationCommander != null)
			{
				Vector3 lDragMovePosition = vQuaterViewCameraSet.GetTouchPosition(pDragPlane, pDragMovePoint);
				Vector3 lDragVector = lDragMovePosition - mHoldPosition;

				mFormationCoord = Coord2.ToCoord(mHoldPosition);
				mFormationDirectionY = (int)MathLib.GetDirectionY(lDragVector);

				ShowFormationCursor(lFormationCommander, mFormationCoord, mFormationDirectionY);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragEndDelegate
	private void _OnDragEnd(Plane pDragPlane, Vector2 pDragEndPoint)
	{
		//Debug.Log("_OnDragEnd() mIsHolding=" + mIsHolding);

		if (!mIsHolding)
		{
			if (_CheckDraggable())
			{
				BattlePlay.main.vControlPanel.aIsDraggingStage = false;
				vQuaterViewCameraSet.DragEnd(pDragPlane, pDragEndPoint);
			}
		}
// 		else
// 		{
// 			BattleCommander lFormationCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
// 			if (lFormationCommander != null)
// 				TouchMoveCoord(lFormationCommander, mFormationCoord, mFormationDirectionY);
// 		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClickTouchPanel(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded)
	{
		//Debug.Log("_OnClickTouchPanel()");

		Vector3 lClickPosition;
// 		if (Terrain.activeTerrain != null)
// 		{
// 			Ray lTouchRay = vQuaterViewCameraSet.aMainCamera.ScreenPointToRay(pClickPoint);
// 			RaycastHit lRaycastHit;
// 			if (Physics.Raycast(lTouchRay, out lRaycastHit))
// 			{
// 				//Debug.Log("lRaycastHit.collider.gameObject=" + lRaycastHit.collider.gameObject);
// 				lClickPosition = lRaycastHit.point;
// 			}
// 			else
// 				return;
// 		}
// 		else
		{
			lClickPosition = vQuaterViewCameraSet.GetTouchPosition(pClickPlane, pClickPoint);
		}

		_OnClick(lClickPosition, pClickPoint, pIsHolded, BattlePlay.main.vControlPanel.aFocusingCommander);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClick(Vector3 pClickPosition, Vector2 pClickPoint, bool pIsHolded, BattleCommander pClickCommander)
	{
		if (!mIsCommandable)
			return; // 지휘 입력을 받을 때에만 클릭 입력을 처리

		if (pClickCommander != null)
		{
			if (!pClickCommander.aIsAlive)
				return; // 클릭 불가

			Vector3 lClickWorldPosition = pClickPosition;

			// 먼저 오브젝트 클릭을 시도합니다.
		#if !GRAPHICS_MOVE_TEST
			Ray lTouchRay = vQuaterViewCameraSet.aMainCamera.ScreenPointToRay(pClickPoint);
			RaycastHit lRaycastHit;
			if (Physics.Raycast(lTouchRay, out lRaycastHit))
			{
				StageObject lStageObject = lRaycastHit.collider.GetComponent<StageObject>();
				if (lStageObject != null)
				{
					// 거점 클릭을 처리합니다.
					BattleCheckPoint lCheckPoint = lStageObject.aBattleObject as BattleCheckPoint;
					if (lCheckPoint != null)
					{
						TouchCheckPoint(pClickCommander, lCheckPoint);
						return; // 처리 완료
					}

					// 이벤트 지점 클릭을 처리합니다.
					BattleEventPoint lEventPoint = lStageObject.aBattleObject as BattleEventPoint;
					if (lEventPoint != null)
					{
						TouchEventPoint(pClickCommander, lEventPoint);
						return; // 처리 완료
					}
				}

				StageTroopTag lStageTroopTag = lRaycastHit.collider.GetComponent<StageTroopTag>();
				if (lStageTroopTag != null)
				{
					BattleTroop lTargetTroop = (lStageTroopTag.vStageTroop != null) ? lStageTroopTag.vStageTroop.aBattleTroop : null;
					if (lTargetTroop != null)
					{
						if (lTargetTroop.aTeamIdx == pClickCommander.aTeamIdx) // 같은 팀의 부대라면
						{
							// 유저 부대의 경우 그 부대의 지휘관을 포커싱합니다.
							if (lTargetTroop.aCommander.aUser != null)
								BattlePlay.main.vControlPanel.FocusCommander(
									lTargetTroop.aCommander,
									true,
									BattlePlay.main.vControlPanel.vDefaultFocusingCurve,
									BattlePlay.main.vControlPanel.vDefaultFocusingDelay);
						}
						else // 다른 팀의 부대라면
						{
							// 그 부대를 공격합니다.
							TouchTargetTroop(pClickCommander, lTargetTroop);
						}
						return; // 처리 완료
					}
				}

				// 알 수 없는 충돌체에 대해서는 지면 충돌체로 간주하고 클릭 지점을 갱신합니다.
				lClickWorldPosition = lRaycastHit.point;
				//Debug.Log("click collider:" + lRaycastHit.collider + " - " + this);
			}
		#endif

			// 오브젝트 클릭이 없다면 이동시킵니다.
			Coord2 lClickCoord = Coord2.ToCoord(lClickWorldPosition);
			BattleGridTile lClickGridTile = BattlePlay.main.aBattleGrid.GetTile(lClickCoord);
			if ((lClickGridTile != null) &&
				!lClickGridTile.aIsBlocked)
			{
				if (!pIsHolded)
					TouchMoveCoord(pClickCommander, lClickCoord);
// 				else
// 					TouchMoveCoord(pClickCommander, lClickCoord, mFormationDirectionY);
			}
			else
			{
				FlashCommanderItemFormationCursor(null, lClickCoord, mFormationDirectionY);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnZoomDelegate
	private void _OnZoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta)
	{
		if (BattlePlay.main.vControlPanel.aZoomApproachTimer > 0.0f)
			return; // 점근 중이면 무시

		vQuaterViewCameraSet.Zoom(pZoomPlane, pZoomCenterPoint, pZoomDelta);

		if (pZoomDelta < 0.0f)
		{
			if (vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance >= vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewSlideDistance)
				BattlePlay.main.vControlPanel.SlideZoom(1.0f); // 임계치에서 줌 아웃을 하면 최대 줌 아웃으로 점근
		}
		else
		{
			if (vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance >= vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewSlideDistance)
				BattlePlay.main.vControlPanel.SlideZoom(BattlePlay.main.vControlPanel.aDefaultZoomSliderValue); // 임계치 밖에서 줌 인을 하면 기본 거리로 점근
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 가능 여부
	private bool _CheckDraggable()
	{
		if (vTouchPanel.vIsEnableDrag &&
			(vQuaterViewCameraSet.vPerspectiveZoomInfo.vViewDistance <= vQuaterViewCameraSet.vPerspectiveZoomInfo.vFarViewSlideDistance))
			return true;
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 커서 오브젝트만 활성화시킵니다.
	private void _ActivateCommanderCursorObject(GameObject pActivationCursorObject)
	{
		for (int iCursor = 0; iCursor < mCommanderCursorObjects.Length; ++iCursor)
			mCommanderCursorObjects[iCursor].SetActive(mCommanderCursorObjects[iCursor] == pActivationCursorObject);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Vector2 mDragPoint;

	private bool mIsCommandable = true;
	private bool mIsHolding;
	private Vector3 mHoldPosition;

	private Coord2 mFormationCoord;
	private int mFormationDirectionY;

	private GameObject[] mCommanderCursorObjects;
}
