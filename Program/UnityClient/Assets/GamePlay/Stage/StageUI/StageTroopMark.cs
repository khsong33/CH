using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroopMark : MonoBehaviour
{
	public const float cShowHitSpriteDelay = 0.5f;
	public static readonly String[] cHitSpriteNames = { "Decorators_Recrew", "Decorators_Decorator_Cover_Negative_28x28.PNG", "Decorators_Decorator_Cover_Heavy_28x28.PNG", "Icons_minimap_mm_victory_point", "" };

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UIWidget vAnchorWidget;

	public BattleUITroopMark vTroopMark;
	public GameObject vSelectionObject;
	public UISprite vHitSprite;
	public StackLabelDebug vStackLabelDebug;

	public Transform vHeadMarkRoot;

	public Vector3 vLocalScaleClose = new Vector3(1.5f, 1.5f, 1.5f);
	public Vector3 vLocalScaleFar = Vector3.one;

	public Vector3 vHeadMarkRootLocalPositionClose = new Vector3(0.0f, 70.0f, 0.0f);
	public Vector3 vHeadMarkRootLocalPositionFar = Vector3.zero;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Start() - " + this);

		if (vAnchorWidget == null)
			Debug.LogError("vAnchorWidget is empty - " + this);

		if (vTroopMark == null)
			Debug.LogError("vTroopMark is empty - " + this);
		if (vSelectionObject == null)
			Debug.LogError("vSelectionObject is empty - " + this);
		if (vHitSprite == null)
			Debug.LogError("vHitSprite is empty - " + this);

		if (vHeadMarkRoot == null)
			Debug.LogError("vHeadMarkRoot is empty - " + this);

		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		mIsShowHitSprite = false;
		mLastTroopHitResult = TroopHitResult.None;
		mNewTroopHitResult = TroopHitResult.None;

		vSelectionObject.SetActive(false);
		vHitSprite.gameObject.SetActive(false);

		_OnZoomChanged(); // 줌 상태 반영
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate += _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		BattlePlay.main.vControlPanel.vQuaterViewCameraSet.aOnZoomChangedDelegate -= _OnZoomChanged;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnDrag(Vector2 pDelta)
	{
		BattlePlay.main.vControlPanel.vStageTouchControl.vTouchPanel.OnDrag(pDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnPress(bool pIsDown)
	{
		BattlePlay.main.vControlPanel.vStageTouchControl.vTouchPanel.OnPress(pIsDown);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnClick()
	{
		//Debug.Log("OnClick() - " + vTroopMark.aStageTroop.aBattleTroop);

		BattleCommander lControlCommander = BattlePlay.main.vControlPanel.aFocusingCommander;
		if (lControlCommander == null)
			return;

		BattlePlay.main.vStage.vStageTouchControl.TouchTargetCommander(vTroopMark.aBattleTroop.aCommander, lControlCommander);

		//BattleTroop lTargetTroop = vTroopMark.aStageTroop.aBattleTroop;
		//if (lTargetTroop.aTeamIdx == lControlCommander.aTeamIdx) // 같은 팀의 부대라면
		//{
		//	// 유저 부대의 경우 그 부대의 지휘관을 포커싱합니다.
		//	if (lTargetTroop.aCommander.aUser != null)
		//		BattlePlay.main.vControlPanel.FocusCommander(lTargetTroop.aCommander, true, true);
		//}
		//else // 다른 팀의 부대라면
		//{
		//	// 그 부대를 공격합니다.
		//	BattlePlay.main.vStage.vStageTouchControl.TouchTargetTroop(lControlCommander, lTargetTroop);
		//}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 타격 스프라이트를 보입니다.
	public void ShowHit(TroopHitResult pTroopHitResult)
	{
		mNewTroopHitResult = pTroopHitResult;
		mShowHitSpriteTimer = cShowHitSpriteDelay;
		if (!mIsShowHitSprite &&
			gameObject.activeInHierarchy)
			StartCoroutine(_ShowHitSprite());
	}
	//-------------------------------------------------------------------------------------------------------
	// 개발 로그를 남깁니다.
	public void DebugLog(String pLog)
	{
		if (vStackLabelDebug != null)
			vStackLabelDebug.AddString(pLog, Color.white);
	}
	//-------------------------------------------------------------------------------------------------------
	// 선택 상태를 변경합니다.
	public void SetSelected(bool pIsSelected)
	{
		vSelectionObject.SetActive(pIsSelected);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// QuaterViewCameraSet.OnZoomChangedDelegate
	private void _OnZoomChanged()
	{
		QuaterViewCameraSet lQuaterViewCameraSet = BattlePlay.main.vControlPanel.vQuaterViewCameraSet;

		vHeadMarkRoot.localPosition = Vector3.Lerp(vHeadMarkRootLocalPositionClose, vHeadMarkRootLocalPositionFar, lQuaterViewCameraSet.aZoomLerpFactor);
		mTransform.localScale = Vector3.Lerp(vLocalScaleClose, vLocalScaleFar, lQuaterViewCameraSet.aZoomLerpFactor);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 타격 스프라이트를 보입니다.
	private IEnumerator _ShowHitSprite()
	{
		mIsShowHitSprite = true;
		vHitSprite.gameObject.SetActive(true);

		do
		{
			if (mLastTroopHitResult != mNewTroopHitResult)
			{
				mLastTroopHitResult = mNewTroopHitResult;
				vHitSprite.spriteName = cHitSpriteNames[(int)mNewTroopHitResult];
			}

			float lFactorX = 1.0f - mShowHitSpriteTimer / cShowHitSpriteDelay;
			float lSpriteAlpha = -lFactorX * lFactorX + 1.0f;
			vHitSprite.alpha = lSpriteAlpha;

			if (!mIsShowHitSprite)
				break;

			mShowHitSpriteTimer -= Time.deltaTime;
			yield return null;

		} while (mShowHitSpriteTimer >= 0.0f);

		mIsShowHitSprite = false;
		vHitSprite.gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;

	private bool mIsShowHitSprite;
	private TroopHitResult mLastTroopHitResult;
	private TroopHitResult mNewTroopHitResult;
	private float mShowHitSpriteTimer;
}
