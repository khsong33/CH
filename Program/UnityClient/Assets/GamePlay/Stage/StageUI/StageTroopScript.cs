﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StageTroopScript : MonoBehaviour
{
	public delegate void OnFinishedScript();
	public OnFinishedScript aOnFinishedScript { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BattleUITroopMark vTroopMark;
	public UILabel vMessageLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if(vTroopMark == null)
			Debug.LogError("vTroopMark is empty - " + this);
		if (vMessageLabel == null)
			Debug.LogError("vMessageLabel is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 메시지를 출력합니다.
	public void ShowScript(String pMessageKey, float pDuration)
	{
		vMessageLabel.text = LocalizedTable.get.GetLocalizedText(pMessageKey);
		if (isActiveAndEnabled)
			StartCoroutine(_PlayScript(pDuration));
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 스크립트를 진행합니다.
	private IEnumerator _PlayScript(float pDuration)
	{
		yield return new WaitForSeconds(pDuration);

		vTroopMark.HideTroopScript();

		if (aOnFinishedScript != null)
			aOnFinishedScript();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
