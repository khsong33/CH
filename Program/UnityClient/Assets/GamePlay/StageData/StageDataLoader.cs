﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class StageDataLoader : MonoBehaviour
{
	public class StageData
	{
		public String mFullPath;
		public String mFolderName;
		public String mPrefabName;
		public Vector3 mWorldPosition;
	}

	public delegate void OnLoadObjectDelegate(GameObject pGameObject);
	public delegate void OnPostLoadObjectDelegate();

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public String vPathPrefix = "MapInfos/";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트 로딩 콜백
	public OnLoadObjectDelegate aOnLoadObjectDelegate
	{
		get { return mOnLoadObjectDelegate; }
		set { mOnLoadObjectDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트 로딩 후처리 콜백
	public OnPostLoadObjectDelegate aOnPostLoadObjectDelegate
	{
		get { return mOnPostLoadObjectDelegate; }
		set { mOnPostLoadObjectDelegate = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 맵을 로딩합니다.
	public void Load(String pAssetName)
	{
		if(mStageDatas == null)
			mStageDatas = new List<StageData>();
		mStageDatas.Clear();

		TextAsset lStageDataInfo = ResourceUtil.Load<TextAsset>(vPathPrefix + pAssetName);
		StringReader lDataReader = new StringReader(lStageDataInfo.text);
		String lStrMapData = String.Empty;

		while ((lStrMapData = lDataReader.ReadLine()) != null)
		{
			//Debug.Log("--> " + lStrMapData);
			String[] lData = lStrMapData.Split(',');
			StageData lStageData = new StageData();
			lStageData.mFullPath = lData[0];
			lStageData.mFolderName = lData[1];
			lStageData.mPrefabName = lData[2];

			lStageData.mWorldPosition = new Vector3(float.Parse(lData[3]), float.Parse(lData[4]), float.Parse(lData[5]));

			mStageDatas.Add(lStageData);

			// Debug.Log("lData[0] - " + lData[0] + "/lData[1] - " + lData[1] + "/lData[2] - " + lData[2]);
			GameObject lStageDataObject = ResourceUtil.InstantiateObject<GameObject>(lData[2]);
			lStageDataObject.transform.position = lStageData.mWorldPosition;
			lStageDataObject.transform.parent = this.transform;

			if (mOnLoadObjectDelegate != null)
				mOnLoadObjectDelegate(lStageDataObject);
		}

		if (mOnPostLoadObjectDelegate != null)
			mOnPostLoadObjectDelegate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private List<StageData> mStageDatas;
	private OnLoadObjectDelegate mOnLoadObjectDelegate;
	private OnPostLoadObjectDelegate mOnPostLoadObjectDelegate; 

}
