﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CombatTitlePlay : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vMessageLabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vMessageLabel == null)
			Debug.LogError("vMessageLabel is empty - " + this);

	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if(GameData.get.aPrevGamePhase == GamePhase.Lobby)
			StartCoroutine(_GoLoginPage());

		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aIConnectionProtocol.aOnErrorNetworkDelegate += _OnErrorNetwork;
		}
		mErrorCount = 0;
		mIsNetworkChecker = true;
		mMessage = LocalizedTable.get.GetLocalizedText("CONNTING_SERVER");
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestroy()
	{
		if (GameData.get.aLobbyControl != null && GameData.get.aLobbyControl.vNetworkLobby != null)
		{
			GameData.get.aLobbyControl.vNetworkLobby.aIConnectionProtocol.aOnErrorNetworkDelegate -= _OnErrorNetwork;
		}
		mErrorCount = 0;
	}
	void Update()
	{
		vMessageLabel.text = mMessage;
		if (!mIsNetworkChecker)
		{
			MessageBoxManager.aInstance.OpenMessageBox(LocalizedTable.get.GetLocalizedText("NOT_CONNECTION_SERVER"), MessageBoxManager.MessageBoxType.OK);
			mIsNetworkChecker = true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 로고 화면 종료를 처리합니다.
	public void OnEndLogo()
	{
		StartCoroutine(_GoLoginPage());
	}
	private IEnumerator _GoLoginPage()
	{
		while (!GameData.get.aLobbyControl.aIsConnectedServer)
		{
			yield return null;
		}

		GamePopupLogin lPopup = (GamePopupLogin)GamePopupUIManager.aInstance.OpenPopup("Popup Login");
		if (lPopup != null)
		{
			lPopup.SetLobbyControl(GameData.get.aLobbyControl);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 네트워크 에러 처리
	private void _OnErrorNetwork()
	{
		mErrorCount++;
		if (mErrorCount > 3)
		{
			mMessage = LocalizedTable.get.GetLocalizedText("NOT_CONNECTION_SERVER");
			mIsNetworkChecker = false;
			GameData.get.aLobbyControl.vNetworkLobby.vLobbyProtocolNodeJS.vSocket.OnDestroy();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mErrorCount;
	private String mMessage;
	private bool mIsNetworkChecker;
}
