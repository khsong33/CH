﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityStandardAssets.ImageEffects;

public class UIEffectControl : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public BlurOptimized vBlurEffectComponent;
	public UICamera vEventMaskingCamera;
	public GameObject vBlurRootObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		UIEffectManager.get.SetEffectControl(this);

		if (vBlurEffectComponent == null)
			Debug.LogError("vBlurEffectComponent is empty - " + this);
		if (vBlurRootObject == null)
			Debug.LogError("vBlurRootObject is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// Effect를 실행합니다.
	public void SetActiveEffect(bool pIsActive)
	{
		vBlurEffectComponent.enabled = pIsActive;
		vBlurRootObject.SetActive(pIsActive);

		if (pIsActive)
			vEventMaskingCamera.eventReceiverMask = (1 << LayerMask.NameToLayer("Behind blur NGUI"));
		else
			vEventMaskingCamera.eventReceiverMask = (1 << LayerMask.NameToLayer("NGUI"));
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
