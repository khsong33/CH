﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityStandardAssets.ImageEffects;

public class UIEffectManager
{
	public static UIEffectManager get
	{
		get
		{
			if (mInstance == null)
				mInstance = new UIEffectManager();
			return mInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public UIEffectManager()
	{
		mBlurObjectStack = new Stack<GameObject>();
	}
	//-------------------------------------------------------------------------------------------------------
	// Effect Controller 셋팅
	public void SetEffectControl(UIEffectControl pEffectControl)
	{
		mEffectControl = pEffectControl;
	}
	//-------------------------------------------------------------------------------------------------------
	// Effect 활성화
	public void SetActiveEffect(bool pIsActive)
	{
		if (!pIsActive)
		{
			mBlurObjectStack.Pop();
		}
		mEffectControl.SetActiveEffect(mBlurObjectStack.Count > 0);
	}
	//-------------------------------------------------------------------------------------------------------
	// Effect를 실행 오브젝트를 설정합니다.
	public void SetObject(GameObject pGameObject)
	{
		mBlurObjectStack.Push(pGameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// Effect를 실행 오브젝트가 같은지 검사합니다.
	public bool IsEffectObject(GameObject pGameObject)
	{
		if (mBlurObjectStack.Peek() == pGameObject)
			return true;
		return false;
	}
	private static UIEffectManager mInstance;

	public Stack<GameObject> mBlurObjectStack;
	public UIEffectControl mEffectControl;
}