using UnityEngine;
using System.Collections;
using System;

public class CameraCapture : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Camera[] targetCameras;
	public RenderTexture captureImage;
	public GameObject[] inactivatingObjects;
	public GameObject[] activatingObjects;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 카메라로 보이는 장면을 캡쳐합니다.
	public IEnumerator Capture()
	{
		if (targetCameras.Length == 0)
			Debug.LogError("targetCameras is empty. - " + gameObject);
		if (captureImage == null)
			Debug.LogError("captureImage is empty. - " + gameObject);

		// 캡쳐 이미지에 렌더링하도록 카메라 타겟을 바꿉니다.
		for (int iCamera = 0; iCamera < targetCameras.Length; ++iCamera)
			targetCameras[iCamera].targetTexture = captureImage;

		// 이번 프레임에서의 렌더링이 완료되도록 한 프레임 쉽니다.
		yield return null;

		// 여기서부터는 렌더링이 완료된 다음 프레임입니다.

		// 카메라 타겟을 해제합니다.
		for (int iCamera = 0; iCamera < targetCameras.Length; ++iCamera)
			targetCameras[iCamera].targetTexture = null;

		// 비활성화 시키기로 등록된 오브젝트들을 비활성화시킵니다.
		for (int iObject = 0; iObject < inactivatingObjects.Length; ++iObject)
			inactivatingObjects[iObject].SetActive(false);

		// 활성화 시키기로 등록된 오브젝트들을 활성화시킵니다.
		for (int iObject = 0; iObject < activatingObjects.Length; ++iObject)
			activatingObjects[iObject].SetActive(true);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
