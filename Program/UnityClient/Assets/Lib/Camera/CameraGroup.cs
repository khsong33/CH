using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode()]
public class CameraGroup : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Camera mainCamera;
	public Camera[] subCameras;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 보조 카메라들의 트랜스폼 상태를 메인 카메라와 같도록 합니다.
	public void SyncSubCameraTransforms()
	{
		Transform lMainTransform = mainCamera.transform;
		for (int iCamera = 0; iCamera < subCameras.Length; ++iCamera)
		{
			Camera lLinkCamera = subCameras[iCamera];
			Transform lLinkTransform = lLinkCamera.transform;
			
			lLinkTransform.parent = lMainTransform;

			lLinkTransform.position = lMainTransform.position;
			lLinkTransform.rotation = lMainTransform.rotation;
			lLinkTransform.localScale = lMainTransform.localScale;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 보조 카메라들의 프로젝션 상태를 메인 카메라와 같도록 합니다.
	public void SyncSubCameraProjections()
	{
		for (int iCamera = 0; iCamera < subCameras.Length; ++iCamera)
		{
			Camera lLinkCamera = subCameras[iCamera];
			
			lLinkCamera.orthographic = mainCamera.orthographic;
			if (lLinkCamera.orthographic)
				lLinkCamera.orthographicSize = mainCamera.orthographicSize;
			else
				lLinkCamera.fieldOfView = mainCamera.fieldOfView;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
