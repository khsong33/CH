using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class CameraTargeting : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Camera vCamera;
	public Transform vViewTarget;
	public Vector3 vViewOffset = Vector3.zero;
	public Vector3 vViewAngles = new Vector3(45.0f, 45.0f, 0.0f);
	public float vViewDistance = 50.0f;

	public KeyCode vApplicationExitKey = KeyCode.None;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransform = transform;

		if (vCamera == null)
		{
			vCamera = GetComponent<Camera>();

			if (vCamera == null)
				Debug.LogError("vCamera is empty - " + this);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 늦은 갱신 콜백
	void LateUpdate()
	{
		Vector3 lCameraVector = new Vector3(0.0f, 0.0f, -vViewDistance);
		mTransform.rotation = Quaternion.Euler(vViewAngles);
		mTransform.position = vViewOffset + mTransform.rotation * lCameraVector;

		if (vViewTarget != null)
			mTransform.position += vViewTarget.position;

		if ((vCamera != null) &&
			vCamera.orthographic)
			vCamera.orthographicSize = vViewDistance;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
}
