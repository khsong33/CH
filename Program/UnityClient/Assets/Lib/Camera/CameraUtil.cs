using UnityEngine;
using System.Collections;
using System;

public class CameraUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 고정 종횡비로 뷰포트를 갱신합니다.
	public static Rect GetFixedAspectRatioViewport(float pAspectWidth, float pAspectHeight)
	{
		float lFixedAspectRatio = pAspectHeight / pAspectWidth;
		float lScreenAspectRatio = (float)Screen.height / Screen.width;
		if (lFixedAspectRatio < lScreenAspectRatio)
		{
			// 상하가 잘립니다.
			float lClippingSize = (1.0f - lFixedAspectRatio / lScreenAspectRatio) * 0.5f;
			Rect lViewportRect = new Rect(
				0.0f,
				lClippingSize,
				1.0f,
				1.0f - lClippingSize * 2.0f);
			//Debug.Log("lFixedAspectRatio=" + lFixedAspectRatio + " lScreenAspectRatio=" + lScreenAspectRatio + " top bottom lViewportRect=" + lViewportRect);
			return lViewportRect;
		}
		else
		{
			// 좌우가 잘립니다.
			float lClippingSize = (1.0f - lScreenAspectRatio / lFixedAspectRatio) * 0.5f;
			Rect lViewportRect = new Rect(
				lClippingSize,
				0.0f,
				1.0f - lClippingSize * 2.0f,
				1.0f);
			//Debug.Log("lFixedAspectRatio=" + lFixedAspectRatio + " lScreenAspectRatio=" + lScreenAspectRatio + " left right lViewportRect=" + lViewportRect);
			return lViewportRect;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 UI 좌표에 대한 월드 좌표를 얻습니다.
	public static Vector3 GetTouchPosition(Camera pCamera, Plane pTouchPlane, Vector2 pTouchPoint)
	{
		Ray lTouchRay = pCamera.ScreenPointToRay(pTouchPoint);
		//Debug.DrawRay(lTouchRay.origin, lTouchRay.direction * 10, Color.yellow);

		float lTouchDistance;
		if (pTouchPlane.Raycast(lTouchRay, out lTouchDistance))
		{
			Vector3 lTouchPosition = lTouchRay.GetPoint(lTouchDistance);
			//Logger.Log("lTouchPosition=" + LogUtil.LogVector(lTouchPosition));
			return lTouchPosition;
		}
		else
		{
			Logger.Log("no hit! - lTouchRay.origin=" + lTouchRay.origin);
			return Vector3.zero;
		}
	}
}
