using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapControl : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public TouchPanel vTouchPanel;
	public QuaterViewCameraSet vQuaterViewCameraSet;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (vTouchPanel == null)
			Debug.LogError("vTouchPanel is empty - " + this);
		if (vQuaterViewCameraSet == null)
			Debug.LogError("vQuaterViewCameraSet is empty - " + this);

		vTouchPanel.aOnDragStartDelegate += _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate += _OnDragMove;
		vTouchPanel.aOnDragEndDelegate += _OnDragEnd;
		vTouchPanel.aOnClickDelegate += _OnClick;
		vTouchPanel.aOnZoomDelegate += _OnZoom;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		vTouchPanel.aOnDragStartDelegate -= _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate -= _OnDragMove;
		vTouchPanel.aOnDragEndDelegate -= _OnDragEnd;
		vTouchPanel.aOnClickDelegate -= _OnClick;
		vTouchPanel.aOnZoomDelegate -= _OnZoom;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragStartDelegate
	private void _OnDragStart(Plane pDragPlane, Vector2 pDragStartPoint)
	{
		vQuaterViewCameraSet.DragStart(pDragPlane, pDragStartPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragMoveDelegate
	private void _OnDragMove(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount)
	{
		vQuaterViewCameraSet.DragMove(pDragPlane, pDragLastPoint, pDragMovePoint, pDragDelay, pTouchCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragEndDelegate
	private void _OnDragEnd(Plane pDragPlane, Vector2 pDragEndPoint)
	{
		vQuaterViewCameraSet.DragEnd(pDragPlane, pDragEndPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClick(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded)
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnZoomDelegate
	private void _OnZoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta)
	{
		vQuaterViewCameraSet.Zoom(pZoomPlane, pZoomCenterPoint, pZoomDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
