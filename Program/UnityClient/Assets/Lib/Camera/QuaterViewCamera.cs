using UnityEngine;
using System.Collections;

[ExecuteInEditMode()]
public class QuaterViewCamera : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public GameObject vViewObject;
	public Vector3 vViewOffset = Vector3.zero;
	public Vector3 vViewAngles = new Vector3(60.0f, 0.0f, 0.0f);
	public float vViewDistance = 10.0f;

	public KeyCode vApplicationExitKey = KeyCode.None;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 카메라를 드래그합니다.
	public void DragPosition(Vector3 pDragVector)
	{
		mDragVector = pDragVector;
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 늦은 갱신 콜백
	void LateUpdate()
	{
		if (mDragVector != Vector3.zero)
		{
			vViewOffset += mDragVector;
			mDragVector = Vector3.zero;
		}
		
		Vector3 lCameraVector = new Vector3(0.0f, 0.0f, -vViewDistance);
		mTransform.rotation = Quaternion.Euler(vViewAngles);
		mTransform.position = vViewOffset + mTransform.rotation * lCameraVector;

		if (vViewObject != null)
			mTransform.position += vViewObject.transform.position;

		if ((GetComponent<Camera>() != null) &&
			GetComponent<Camera>().orthographic)
			GetComponent<Camera>().orthographicSize = vViewDistance;

		if (vApplicationExitKey != KeyCode.None)
		{
			if (Input.GetKeyUp(vApplicationExitKey))
			{
				Debug.Log("QuaterViewCamera Application.Quit();");
				Application.Quit();
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mDragVector = Vector3.zero;
}
