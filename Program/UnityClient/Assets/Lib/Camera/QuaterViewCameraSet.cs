using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[ExecuteInEditMode()]
public class QuaterViewCameraSet : MonoBehaviour
{
	public enum Projection
	{
		Orthographic,
		Perspective,
	}
	public enum FieldOfViewFit
	{
		Width,
		Height,
	}

	[Serializable]
	public class OrthographicZoomInfo
	{
		public float vNearClipPlane = 1.0f;
		public float vFarClipPlane = 1000.0f;
		public float vViewSize = 8.0f;
		public float vCameraDistance = 11.0f;
		public float vCloseViewSize = 2.0f;
		public Rect vCloseViewArea = new Rect(-10.0f, -10.0f, 20.0f, 20.0f);
		public float vFarViewSize = 11.0f;
		public Rect vFarViewArea = new Rect(0.0f, 0.0f, 0.0f, 0.0f);
		public Vector3 vFarViewCneteringOffset = new Vector3(0.0f, 0.0f, 0.0f);
		public float vFarViewSlideDistance = 180.0f;
		public float vZoomSpeed = 10.0f;
		public float vInstantZoomViewOffset = -3.0f;
	}
	[Serializable]
	public class PerspectiveZoomInfo
	{
		public float vNearClipPlane = 1.0f;
		public float vFarClipPlane = 1000.0f;
		public FieldOfViewFit vFieldOfViewFit = FieldOfViewFit.Width;
		public float vFieldOfViewFitAspectRatio = 16.0f / 9.0f; // 16:9 기준으로 종횡비 입력
		public float vFieldOfView = 30.0f;
		public float vCloseFieldOfView = 30.0f;
		internal float mCloseFieldOfViewValue;
		public float vFarFieldOfView = 30.0f;
		internal float mFarFieldOfViewValue;
		public float vViewDistance = 100.0f;
		public float vCloseViewDistance = 10.0f;
		public Rect vCloseViewArea = new Rect(-10.0f, 0.0f, 20.0f, 0.0f);
		public float vFarViewDistance = 100.0f;
		public Rect vFarViewArea = new Rect(-7.0f, 0.0f, 14.0f, 0.0f);
		public float vCloseViewPitchOffset = 0.0f;
		public float vFarViewPitchOffset = 0.0f;
		public Vector3 vFarViewCneteringOffset = new Vector3(0.0f, 0.0f, -25.0f);
		public float vFarViewSlideDistance = 161.5f;
		public float vZoomSpeed = -50.0f;
		public float vInstantZoomViewOffset = -30.0f;
	}

	public delegate void OnZoomChangedDelegate();

	public static QuaterViewCameraSet main = null;
	private static QuaterViewCameraSet main_destroyed = null;

	public const float cInstantZoomDelay = 0.1f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Camera[] vSharingCameras;

	public Rect vRect = new Rect(0.0f, 0.0f, 1.0f, 1.0f);
	public bool vIsFixedAspectRatio = false;
	public Vector2 vFixedAspectRatio = new Vector2(16.0f, 9.0f);

	public Vector3 vViewPlaneCenter = Vector3.zero;
	public bool vLinkViewArea = true;
	public GameObject vLinkObject;
	public Vector3 vLinkOffset = Vector3.zero;
	public Vector3 vViewAngles = new Vector3(45.0f, 0.0f, 0.0f);
	public Projection vProjection = Projection.Perspective;
	public OrthographicZoomInfo vOrthographicZoomInfo;
	public PerspectiveZoomInfo vPerspectiveZoomInfo;
	public bool vIsInstantZoom = false;
	public bool vIsFocusingZoom = true;
	public bool vIsFarViewCentering = true;
	public float vDragMoveScale = 1.0f;

	public bool vIsUpdateVisibleObjects = true;
	public bool vIsDestroyInvisibleObjects = true;
	public float vVisibleObjectActivateDistance = 150.0f;
	public float vVisibleObjectUpdateDistance = 50.0f;

	public float vShakeMaxSize = 0.5f;
	public float vShakeDelay = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 메인 카메라
	public Camera aMainCamera
	{
		get { return mCameras[0]; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 뷰포트
	public Rect aRect
	{
		get { return vRect; }
		set { vRect = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 프로젝션
	public Projection aProjection
	{
		get { return vProjection; }
		set
		{
			if (vProjection != value)
				_SetProjection(value, true);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 뷰 중심점
	public Vector3 aViewCenter
	{
		get { return vViewPlaneCenter; }
		set { vViewPlaneCenter = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 뷰 방향
	public float aViewDirection
	{
		get { return vViewAngles.y; }
		set { vViewAngles.y = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라에 연결된 오브젝트
	public GameObject aLinkObject
	{
		get { return vLinkObject; }
		set { vLinkObject = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라에 연결된 오브젝트에 대한 오프셋 거리
	public Vector3 aLinkOffset
	{
		get { return vLinkOffset; }
		set { vLinkOffset = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라 중심
	public Vector3 aCameraCenter
	{
		get { return mCameraCenter; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 여부
	public bool aIsZoomed
	{
		get { return mIsZoomed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 보간 팩터 [close,far]에 대해서 [0,1]
	public float aZoomLerpFactor
	{
		get { return mZoomLerpFactor; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌이 변경 콜백
	public OnZoomChangedDelegate aOnZoomChangedDelegate
	{
		get { return mOnZoomChangedDelegate; }
		set { mOnZoomChangedDelegate = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		// 전역 참조를 지정합니다.
		if ((main == null) ||
			(main == main_destroyed))
			main = this;

		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		// 자식 카메라들을 찾아 배열에 등록합니다.
		int lCameraCount = 0;
		if (GetComponent<Camera>() != null)
			++lCameraCount; // 자식 말고 이 오브젝트에 붙은 카메라도 셉니다.
		Camera[] lChildCameras = GetComponentsInChildren<Camera>();
		lCameraCount += lChildCameras.Length;
		//Debug.Log("lCameraCount=" + lCameraCount);

		int lSharingCameraCount = 0;
		for (int iCamera = 0; iCamera < vSharingCameras.Length; ++iCamera)
			if (vSharingCameras[iCamera] != null)
				++lSharingCameraCount;
		lCameraCount += lSharingCameraCount;

		mCameras = new Camera[lCameraCount];
// 		mCameraOffsets = new Vector3[lCameraCount];
		mSharingCameraTransforms = new Transform[lSharingCameraCount];
		mCameraRenderTextures = new RenderTexture[lCameraCount];

		lCameraCount = 0;
		lSharingCameraCount = 0;
		if (GetComponent<Camera>() != null)
			mCameras[lCameraCount++] = GetComponent<Camera>(); // 이 오브젝트에 붙은 카메라
		for (int iCamera = 0; iCamera < lChildCameras.Length; ++iCamera)
		{
			mCameras[lCameraCount] = lChildCameras[iCamera]; // 자식 카메라
// 			mCameraOffsets[lCameraCount] = lChildCameras[iCamera].transform.localPosition;
// 			Debug.Log("mCameraOffsets[lCameraCount]=" + mCameraOffsets[lCameraCount] + " - " + lChildCameras[iCamera]);

			++lCameraCount;
		}
		for (int iCamera = 0; iCamera < vSharingCameras.Length; ++iCamera)
			if (vSharingCameras[iCamera] != null)
			{
				mCameras[lCameraCount] = vSharingCameras[iCamera]; // 공유 카메라
// 				mCameraOffsets[lCameraCount] = vSharingCameras[iCamera].transform.localPosition;
// 				Debug.Log("mCameraOffsets[lCameraCount]=" + mCameraOffsets[lCameraCount] + " - " + vSharingCameras[iCamera]);

				mSharingCameraTransforms[lSharingCameraCount] = vSharingCameras[iCamera].transform; // 나중에 동기화를 위해 변환을 저장해 놓습니다.

				++lCameraCount;
				++lSharingCameraCount;
			}
		//Debug.Log("mCameras=" + mCameras);

// 		// 자식 카메라들의 로컬 위치를 이 게임 오브젝트와 같게 합니다.
// 		for (int iCamera = 0; iCamera < lChildCameras.Length; ++iCamera)
// 		{
// 			Transform lChildTransform = lChildCameras[iCamera].transform;
// 			lChildTransform.localPosition = Vector3.zero;
// 			lChildTransform.localRotation = Quaternion.identity;
// 			lChildTransform.localScale = Vector3.one;
// 		}

		// 뷰포트를 초기화합니다.
		_SetViewport(vRect);
		mLastUpdateViewport = vRect;

		// 카메라 모드를 초기화합니다.
		vPerspectiveZoomInfo.mCloseFieldOfViewValue = vPerspectiveZoomInfo.vCloseFieldOfView;
		vPerspectiveZoomInfo.mFarFieldOfViewValue = vPerspectiveZoomInfo.vFarFieldOfView;

		_SetProjection(vProjection, false);
		mLastUpdateProjection = vProjection;

		if (Application.isPlaying
			&& (mCameras.Length == 0))
			enabled = false; // 아무 카메라도 없습니다.

		mLastZoomFactor = 0.0f;
		_UpdateZoomState();
		mCurrentZoomPosition = mCurrentViewArea.center;

		// 가시 오브젝트 리스트를 초기화합니다.
		mVisibleObjectList = new LinkedList<GameObject>();
		mVisibleObjectUpdatePosition = mTransform.position;

		// 흔들기를 초기화합니다.
		mShakeTimer = 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 늦은 갱신 콜백
	void LateUpdate()
	{
		// 고정 종횡비를 적용합니다.
		if (vIsFixedAspectRatio)
			vRect = CameraUtil.GetFixedAspectRatioViewport(vFixedAspectRatio.x, vFixedAspectRatio.y);

		// 뷰포트를 갱신합니다.
		if (mLastUpdateViewport != vRect)
		{
			_SetViewport(vRect);
			mLastUpdateViewport = vRect;
		}

		// 인스펙터에서 모드가 바뀌었을 때의 처리를 합니다.
		if (mLastUpdateProjection != vProjection)
		{
			_SetProjection(vProjection, true);
			mLastUpdateProjection = vProjection;
		}

		// 줌 상태를 갱신합니다.
		_UpdateZoomState();

		// 뷰 중심점이 뷰 평면 안에 있도록 합니다.
		vViewPlaneCenter = _GetClippedViewPlanePosition(
			mCurrentViewArea,
			vViewPlaneCenter);

		// 흔들기를 갱신합니다.
		if (mShakeTimer > 0.0f)
		{
			mShakeTimer -= Time.deltaTime;
			if (mShakeTimer < 0.0f)
				mShakeTimer = 0.0f;
		}

		// 카메라를 움직입니다.
		switch (vProjection)
		{
		case Projection.Orthographic:
			_TransformOrthograpicCameras();
			break;
		case Projection.Perspective:
			_TransformPerspectiveCameras();
			break;
		}

		// 공유 카메라들의 위치를 이 게임 오브젝트와 같게 합니다.
		for (int iCamera = 0; iCamera < mSharingCameraTransforms.Length; ++iCamera)
		{
			mSharingCameraTransforms[iCamera].position = mTransform.position;
			mSharingCameraTransforms[iCamera].rotation = mTransform.rotation;
		}

		// 에디터 모드에서의 디버깅 정보를 그립니다.
		if (Application.isEditor)
		{
			// 현재 줌 상태에서의 뷰 영역을 그립니다.
			Debug.DrawLine(
				new Vector3(mCurrentViewArea.xMin, vViewPlaneCenter.y, mCurrentViewArea.yMin),
				new Vector3(mCurrentViewArea.xMax, vViewPlaneCenter.y, mCurrentViewArea.yMin),
				Color.blue, 0.0f, false);
			Debug.DrawLine(
				new Vector3(mCurrentViewArea.xMax, vViewPlaneCenter.y, mCurrentViewArea.yMin),
				new Vector3(mCurrentViewArea.xMax, vViewPlaneCenter.y, mCurrentViewArea.yMax),
				Color.blue, 0.0f, false);
			Debug.DrawLine(
				new Vector3(mCurrentViewArea.xMax, vViewPlaneCenter.y, mCurrentViewArea.yMax),
				new Vector3(mCurrentViewArea.xMin, vViewPlaneCenter.y, mCurrentViewArea.yMax),
				Color.blue, 0.0f, false);
			Debug.DrawLine(
				new Vector3(mCurrentViewArea.xMin, vViewPlaneCenter.y, mCurrentViewArea.yMax),
				new Vector3(mCurrentViewArea.xMin, vViewPlaneCenter.y, mCurrentViewArea.yMin),
				Color.blue, 0.0f, false);

			// 뷰 영역의 중심에서 뷰 중심까지의 선분을 그립니다.
			Vector3 lCurrentViewAreaCenter = mCurrentViewArea.center;
			Debug.DrawLine(new Vector3(lCurrentViewAreaCenter.x, vViewPlaneCenter.y, lCurrentViewAreaCenter.y), vViewPlaneCenter, Color.green, 0.0f, false);
			//Debug.Log(String.Format("mCurrentViewArea.center{0} - vViewPlaneCenter{1}", mCurrentViewArea.center, vViewPlaneCenter));
			switch (vProjection)
			{
			case Projection.Orthographic:
				Debug.DrawLine(vViewPlaneCenter, vViewPlaneCenter + Vector3.up * vOrthographicZoomInfo.vCameraDistance, Color.green, 0.0f, false);
				break;
			case Projection.Perspective:
				Debug.DrawLine(vViewPlaneCenter, vViewPlaneCenter + Vector3.up * vPerspectiveZoomInfo.vViewDistance, Color.green, 0.0f, false);
				break;
			}

			// 뷰 영역의 중심에서 줌 위치까지의 선분을 그립니다.
			Debug.DrawLine(mCurrentViewArea.center, mCurrentZoomPosition, Color.red, 0.0f, false);
		}

		// 가시 오브젝트의 보이기 여부를 갱신합니다.
		if (vIsUpdateVisibleObjects)
			_UpdateVisibleObjectVisibility();
	}
	//-------------------------------------------------------------------------------------------------------
	// 파괴 콜백
	void OnDestroy()
	{
		// 전역 참조를 해제합니다.
		if (main == this)
			main_destroyed = this;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 화면 좌표에 해당하는 직선을 얻습니다.
	public Ray ScreenPointToRay(Vector3 pPosition)
	{
		//Debug.Log("ScreenPointToRay() mCameras=" + mCameras + " - " + this);
		return mCameras[0].ScreenPointToRay(pPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그를 시작합니다.
	public void DragStart(Plane pDragPlane, Vector2 pDragStartPoint)
	{
		if (vIsInstantZoom)
		{
			mInstantZoomPlane = pDragPlane;
			mInstantZoomPoint = pDragStartPoint;

			if (vIsInstantZoom)
			{
				switch (vProjection)
				{
				case Projection.Orthographic:
					StartCoroutine(_InstantOrthographicZoom());
					break;
				case Projection.Perspective:
					StartCoroutine(_InstantPerspectiveZoom());
					break;
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그 이동을 합니다.
	public void DragMove(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount)
	{
		if (mCameras.Length == 0)
			return;

		Vector3 lDragLastPosition = GetTouchPosition(pDragPlane, pDragLastPoint);
		Vector3 lDragMovePosition = GetTouchPosition(pDragPlane, pDragMovePoint);
		Vector3 lDragVector = lDragMovePosition - lDragLastPosition;
		lDragVector *= vDragMoveScale;

		vViewPlaneCenter.x += lDragVector.x;
		vViewPlaneCenter.z += lDragVector.z;

		mCurrentZoomPosition.x += lDragVector.x;
		mCurrentZoomPosition.z += lDragVector.z;

		if (vIsInstantZoom)
		{
			mInstantZoomPlane = pDragPlane;
			mInstantZoomPoint = (pDragLastPoint + pDragMovePoint) * 0.5f;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 드래그를 끝냅니다.
	public void DragEnd(Plane pDragPlane, Vector2 pDragEndPoint)
	{
		if (vIsInstantZoom)
		{
			mInstantZoomPlane = pDragPlane;
			mInstantZoomPoint = pDragEndPoint;

			mRestoreInstantZoom = true;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌을 합니다.
	public void Zoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta)
	{
		if (mCameras.Length == 0)
			return;

		Vector3 lOldZoomCenterPosition;
		switch (vProjection)
		{
		case Projection.Orthographic:
			{
				// 최대 줌 상태를 검사합니다.
				//Logger.Log("vOrthographicZoomInfo: pZoomDelta({3}) vViewSize({0}) vCloseViewSize({1}) vFarViewSize({2})", vOrthographicZoomInfo.vViewSize, vOrthographicZoomInfo.vCloseViewSize, vOrthographicZoomInfo.vFarViewSize, pZoomDelta);
				if ((pZoomDelta * vOrthographicZoomInfo.vZoomSpeed) < 0.0f)
				{
					if (vOrthographicZoomInfo.vViewSize <= vOrthographicZoomInfo.vCloseViewSize) // 줌인 입력인대 이미 최대 줌인 상태
						return;
				}
				else
				{
					if (vOrthographicZoomInfo.vViewSize >= vOrthographicZoomInfo.vFarViewSize) // 줌아웃 입력인데 이미 최대 줌아웃 상태
						return;
				}

				// 줌하기 전에 줌 센터의 평면 위치를 얻습니다.
				lOldZoomCenterPosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);
					
				// 현재 뷰 크기에 비례한 줌 값을 얻습니다.
				float lZoomScale
					= vOrthographicZoomInfo.vViewSize
					/ ((vOrthographicZoomInfo.vCloseViewSize + vOrthographicZoomInfo.vFarViewSize) * 0.5f);
				float lZoomValue = vOrthographicZoomInfo.vZoomSpeed * pZoomDelta * lZoomScale;

				// 뷰 크기를 변경해서 줌을 합니다.
				vOrthographicZoomInfo.vViewSize = Mathf.Clamp(
					vOrthographicZoomInfo.vViewSize + lZoomValue,
					vOrthographicZoomInfo.vCloseViewSize,
					vOrthographicZoomInfo.vFarViewSize);
				//Debug.Log("vOrthographicZoomInfo.vViewSize=" + vOrthographicZoomInfo.vViewSize);

				// GetTouchPosition()이 새로운 줌 상태에 맞게 계산을 하도록 카메라를 움직입니다.
				_TransformOrthograpicCameras();

				// 줌 여부를 갱신합니다.
				mIsZoomed = (vOrthographicZoomInfo.vViewSize != vOrthographicZoomInfo.vFarViewSize);
				//Debug.Log("mIsZoomed=" + mIsZoomed);
			}
			break;
		case Projection.Perspective:
			{
				// 최대 줌 상태를 검사합니다.
				//Logger.Log("vPerspectiveZoomInfo: pZoomDelta({3}) vViewDistance({0}) vCloseViewDistance({1}) vFarViewDistance({2})", vPerspectiveZoomInfo.vViewDistance, vPerspectiveZoomInfo.vCloseViewDistance, vPerspectiveZoomInfo.vFarViewDistance, pZoomDelta);
				if ((pZoomDelta * vPerspectiveZoomInfo.vZoomSpeed) < 0.0f)
				{
					if (vPerspectiveZoomInfo.vViewDistance <= vPerspectiveZoomInfo.vCloseViewDistance) // 줌인 입력인대 이미 최대 줌인 상태
						return;
				}
				else
				{
					if (vPerspectiveZoomInfo.vViewDistance >= vPerspectiveZoomInfo.vFarViewDistance) // 줌아웃 입력인데 이미 최대 줌아웃 상태
						return;
				}

				// 줌하기 전에 줌 센터의 평면 위치를 얻습니다.
				lOldZoomCenterPosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);

				// 현재 뷰 거리에 비례한 줌 값을 얻습니다.
				float lZoomScale
					= vPerspectiveZoomInfo.vViewDistance
					/ ((vPerspectiveZoomInfo.vCloseViewDistance + vPerspectiveZoomInfo.vFarViewDistance) * 0.5f);
				float lZoomValue = vPerspectiveZoomInfo.vZoomSpeed * pZoomDelta * lZoomScale;

				// 거리를 변경해서 줌을 합니다.
				vPerspectiveZoomInfo.vViewDistance = Mathf.Clamp(
					vPerspectiveZoomInfo.vViewDistance + lZoomValue,
					vPerspectiveZoomInfo.vCloseViewDistance,
					vPerspectiveZoomInfo.vFarViewDistance);

				// GetTouchPosition()이 새로운 줌 상태에 맞게 계산을 하도록 카메라를 움직입니다.
				_TransformPerspectiveCameras();

				// 줌 여부를 갱신합니다.
				mIsZoomed = (vPerspectiveZoomInfo.vViewDistance != vPerspectiveZoomInfo.vFarViewDistance);
				//Debug.Log("mIsZoomed=" + mIsZoomed);
			}
			break;
		default:
			lOldZoomCenterPosition = mCurrentZoomPosition;
			break;
		}

		// 줌을 하고 나서 줌 센터의 위치를 보정합니다.
		if (!vIsFocusingZoom)
		{
			Vector3 lNewZoomCenterPlanePosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);
			_CorrectZoomCenter(lOldZoomCenterPosition, lNewZoomCenterPlanePosition);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌을 지정합니다.
	public void SetOrthographicZoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pViewSize)
	{
		if (mCameras.Length == 0)
			return;

		// 줌하기 전에 줌 센터의 평면 위치를 얻습니다.
		Vector3 lOldZoomCenterPosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);
					
		// 뷰 크기를 변경해서 줌을 합니다.
		vOrthographicZoomInfo.vViewSize = Mathf.Clamp(
			pViewSize,
			vOrthographicZoomInfo.vCloseViewSize,
			vOrthographicZoomInfo.vFarViewSize);

		// GetTouchPosition()이 새로운 줌 상태에 맞게 계산을 하도록 카메라를 움직입니다.
		_TransformOrthograpicCameras();

		// 줌 여부를 갱신합니다.
		mIsZoomed = (vOrthographicZoomInfo.vViewSize != vOrthographicZoomInfo.vFarViewSize);

		// 줌을 하고 나서 줌 센터의 위치를 보정합니다.
		Vector3 lNewZoomCenterPlanePosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);
		_CorrectZoomCenter(lOldZoomCenterPosition, lNewZoomCenterPlanePosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌을 지정합니다.
	public void SetPerspectiveZoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pViewDistance)
	{
		if (mCameras.Length == 0)
			return;

		// 줌하기 전에 줌 센터의 평면 위치를 얻습니다.
		Vector3 lOldZoomCenterPosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);

		// 거리를 변경해서 줌을 합니다.
		vPerspectiveZoomInfo.vViewDistance = Mathf.Clamp(
			pViewDistance,
			vPerspectiveZoomInfo.vCloseViewDistance,
			vPerspectiveZoomInfo.vFarViewDistance);

		// GetTouchPosition()이 새로운 줌 상태에 맞게 계산을 하도록 카메라를 움직입니다.
		_TransformPerspectiveCameras();

		// 줌 여부를 갱신합니다.
		mIsZoomed = (vPerspectiveZoomInfo.vViewDistance != vPerspectiveZoomInfo.vFarViewDistance);
		//Debug.Log("mIsZoomed=" + mIsZoomed);

		// 줌을 하고 나서 줌 센터의 위치를 보정합니다.
		Vector3 lNewZoomCenterPlanePosition = GetTouchPosition(pZoomPlane, pZoomCenterPoint);
		_CorrectZoomCenter(lOldZoomCenterPosition, lNewZoomCenterPlanePosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 UI 좌표에 대한 월드 좌표를 얻습니다.
	public Vector3 GetTouchPosition(Plane pTouchPlane, Vector2 pTouchPoint)
	{
		return CameraUtil.GetTouchPosition(mCameras[0], pTouchPlane, pTouchPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 현재 장면을 캡쳐합니다.
	public IEnumerator Capture(RenderTexture pCaptureImage)
	{
		//if (pCaptureImage == null)
		//	Debug.LogError("pCaptureImage is nul - " + this);

		// 현재 카메라들의 렌더 텍스처를 기억해 둡니다.
		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
			mCameraRenderTextures[iCamera] = mCameras[iCamera].targetTexture;

		// 캡쳐 이미지에 렌더링하도록 카메라 타겟을 바꿉니다.
		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
			mCameras[iCamera].targetTexture = pCaptureImage;

		// 이번 프레임에서의 렌더링이 완료되도록 한 프레임 쉽니다.
		yield return null;

		// 여기서부터는 렌더링이 완료된 다음 프레임입니다.

		// 카메라 타겟을 복구합니다.
		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
			mCameras[iCamera].targetTexture = mCameraRenderTextures[iCamera];
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라 근처에 있을 때만 활성화되는 가시 오브젝트를 추가합니다.
	public void AddVisibleObject(GameObject pGameObject)
	{
		mVisibleObjectList.AddLast(pGameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 흔듭니다.
	public void Shake(float pShakeScale)
	{
		mShakeTimer = vShakeDelay * pShakeScale;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 뷰포트를 지정합니다.
	private void _SetViewport(Rect pViewport)
	{
		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
			mCameras[iCamera].rect = pViewport;

		vRect = pViewport;
	}
	//-------------------------------------------------------------------------------------------------------
	// 모드를 지정합니다.
	private void _SetProjection(Projection pProjection, bool pProjectionChange)
	{
		switch (pProjection)
		{
		case Projection.Orthographic:
			{
				for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
					mCameras[iCamera].orthographic = true;

				// 모드 변환이라면 현재 뷰와 가장 비슷한 뷰 크기를 지정합니다.
				if (pProjectionChange)
				{
					vOrthographicZoomInfo.vViewSize
						= mZoomFactor * (vOrthographicZoomInfo.vFarViewSize - vOrthographicZoomInfo.vCloseViewSize)
						+ vOrthographicZoomInfo.vCloseViewSize;
					//Debug.Log("vOrthographicZoomInfo.vViewSize=" + vOrthographicZoomInfo.vViewSize);
				}
			}
			break;
		case Projection.Perspective:
			{
				for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
					mCameras[iCamera].orthographic = false;

				// 모드 변환이라면 현재 뷰와 가장 비슷한 뷰 거리를 지정합니다.
				if (pProjectionChange)
				{
					vPerspectiveZoomInfo.vViewDistance
						= mZoomFactor * (vPerspectiveZoomInfo.vFarViewDistance - vPerspectiveZoomInfo.vCloseViewDistance)
						+ vPerspectiveZoomInfo.vCloseViewDistance;
					//Debug.Log("vPerspectiveZoomInfo.vViewDistance=" + vPerspectiveZoomInfo.vViewDistance);
				}
			}
			break;
		}

		vProjection = pProjection;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 임시 줌을 처리합니다.
	private IEnumerator _InstantOrthographicZoom()
	{
// 		float lInstantZoomViewSizeSave = vOrthographicZoomInfo.vViewSize;
// 		SetOrthographicZoom(mInstantZoomPlane, mInstantZoomPoint, vOrthographicZoomInfo.vInstantZoomViewOffset);

		yield return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 임시 줌을 처리합니다.
	private IEnumerator _InstantPerspectiveZoom()
	{
		mRestoreInstantZoom = false;

		float lInstantZoomStartViewDistance = vPerspectiveZoomInfo.vViewDistance;
		float lInstantZoomEndViewDistance = vPerspectiveZoomInfo.vViewDistance + vPerspectiveZoomInfo.vInstantZoomViewOffset;

		float lInstantZoomTimer = 0.0f;
		do
		{
			lInstantZoomTimer += Time.deltaTime;
			if (lInstantZoomTimer > cInstantZoomDelay)
				lInstantZoomTimer = cInstantZoomDelay;

			float lInstantZoomLerpFactor = lInstantZoomTimer / cInstantZoomDelay;
			float lInstantZoomViewDistance = Mathf.Lerp(lInstantZoomStartViewDistance, lInstantZoomEndViewDistance, lInstantZoomLerpFactor);
			SetPerspectiveZoom(mInstantZoomPlane, mInstantZoomPoint, lInstantZoomViewDistance);

			if (mRestoreInstantZoom)
				break;

			yield return null;
		} while (lInstantZoomTimer < cInstantZoomDelay);

		while (!mRestoreInstantZoom)
			yield return null;

		do
		{
			lInstantZoomTimer -= Time.deltaTime;
			if (lInstantZoomTimer < 0.0f)
				lInstantZoomTimer = 0.0f;

			float lInstantZoomLerpFactor = lInstantZoomTimer / cInstantZoomDelay;
			float lInstantZoomViewDistance = Mathf.Lerp(lInstantZoomStartViewDistance, lInstantZoomEndViewDistance, lInstantZoomLerpFactor);
			SetPerspectiveZoom(mInstantZoomPlane, mInstantZoomPoint, lInstantZoomViewDistance);

			yield return null;
		} while (lInstantZoomTimer > 0.0f);
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌을 하고 나서 줌 센터의 위치를 보정합니다.
	private void _CorrectZoomCenter(Vector3 pOldZoomCenterPosition, Vector3 pNewZoomCenterPlanePosition)
	{
		// 줌 센터의 평면 위치 변화만큼 뷰 센터를 반대로 옮깁니다.
		Vector3 lZoomCenterMoveVector = pNewZoomCenterPlanePosition - pOldZoomCenterPosition;
		vViewPlaneCenter.x -= lZoomCenterMoveVector.x;
		vViewPlaneCenter.z -= lZoomCenterMoveVector.z;

		// 줌 위치를 갱신합니다.
		mCurrentZoomPosition = pNewZoomCenterPlanePosition;

//		Vector3 lNewViewPlaneCenterVector = vViewPlaneCenter - lZoomCenterMoveVector - lViewAreaCenter;
//		if ((lNewViewPlaneCenterVector.x * lZoomCenterMoveVector.x) <= 0.0f)
//			vViewPlaneCenter.x -= lZoomCenterMoveVector.x;
//		if ((lNewViewPlaneCenterVector.z * lZoomCenterMoveVector.z) <= 0.0f)
//			vViewPlaneCenter.z -= lZoomCenterMoveVector.z;
//Logger.Log("lZoomCenterMoveVector{0} lNewViewPlaneCenterVector{1}", lZoomCenterMoveVector.x, lNewViewPlaneCenterVector.x);
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 거리에 따른 상태 변화를 갱신합니다.
	private void _UpdateZoomState()
	{
		switch (vProjection)
		{
		case Projection.Orthographic:
			{
				mZoomFactor
					= (vOrthographicZoomInfo.vViewSize - vOrthographicZoomInfo.vCloseViewSize)
					/ (vOrthographicZoomInfo.vFarViewSize - vOrthographicZoomInfo.vCloseViewSize);
				float lMinX = Mathf.Lerp(
					vOrthographicZoomInfo.vCloseViewArea.xMin,
					vOrthographicZoomInfo.vFarViewArea.xMin,
					mZoomFactor);
				float lMinY = Mathf.Lerp(
					vOrthographicZoomInfo.vCloseViewArea.yMin,
					vOrthographicZoomInfo.vFarViewArea.yMin,
					mZoomFactor);
				float lMaxX = Mathf.Lerp(
					vOrthographicZoomInfo.vCloseViewArea.xMax,
					vOrthographicZoomInfo.vFarViewArea.xMax,
					mZoomFactor);
				float lMaxY = Mathf.Lerp(
					vOrthographicZoomInfo.vCloseViewArea.yMax,
					vOrthographicZoomInfo.vFarViewArea.yMax,
					mZoomFactor);

				mCurrentViewArea = new Rect(lMinX, lMinY, lMaxX - lMinX, lMaxY - lMinY);

				mZoomLerpFactor
					= (vOrthographicZoomInfo.vViewSize - vOrthographicZoomInfo.vCloseViewSize)
					/ (vOrthographicZoomInfo.vFarViewSize - vOrthographicZoomInfo.vCloseViewSize);
			}
			break;
		case Projection.Perspective:
			{
				//Debug.Log("vPerspectiveZoomInfo.vCloseViewArea=" + vPerspectiveZoomInfo.vCloseViewArea);
				//Debug.Log("vPerspectiveZoomInfo.vFarViewDistance=" + vPerspectiveZoomInfo.vFarViewDistance);

				mZoomFactor
					= (vPerspectiveZoomInfo.vViewDistance - vPerspectiveZoomInfo.vCloseViewDistance)
					/ (vPerspectiveZoomInfo.vFarViewDistance - vPerspectiveZoomInfo.vCloseViewDistance);
				float lMinX = Mathf.Lerp(
					vPerspectiveZoomInfo.vCloseViewArea.xMin,
					vPerspectiveZoomInfo.vFarViewArea.xMin,
					mZoomFactor);
				float lMinY = Mathf.Lerp(
					vPerspectiveZoomInfo.vCloseViewArea.yMin,
					vPerspectiveZoomInfo.vFarViewArea.yMin,
					mZoomFactor);
				float lMaxX = Mathf.Lerp(
					vPerspectiveZoomInfo.vCloseViewArea.xMax,
					vPerspectiveZoomInfo.vFarViewArea.xMax,
					mZoomFactor);
				float lMaxY = Mathf.Lerp(
					vPerspectiveZoomInfo.vCloseViewArea.yMax,
					vPerspectiveZoomInfo.vFarViewArea.yMax,
					mZoomFactor);

				mCurrentViewArea = new Rect(lMinX, lMinY, lMaxX - lMinX, lMaxY - lMinY);
				
				mViewPitchOffset = Mathf.Lerp(
					vPerspectiveZoomInfo.vCloseViewPitchOffset,
					vPerspectiveZoomInfo.vFarViewPitchOffset,
					mZoomFactor);

				mZoomLerpFactor
					= (vPerspectiveZoomInfo.vViewDistance - vPerspectiveZoomInfo.vCloseViewDistance)
					/ (vPerspectiveZoomInfo.vFarViewDistance - vPerspectiveZoomInfo.vCloseViewDistance);

				// 광각도 조정합니다.
				switch (vPerspectiveZoomInfo.vFieldOfViewFit)
				{
				case FieldOfViewFit.Width:
					{
						float lAspectRatioWidth = mLastUpdateViewport.width * Screen.width;
						float lAspectRatioHeight = mLastUpdateViewport.height * Screen.height;
						float lFieldOfViewScale = (lAspectRatioWidth / lAspectRatioHeight) / vPerspectiveZoomInfo.vFieldOfViewFitAspectRatio;

						vPerspectiveZoomInfo.mCloseFieldOfViewValue = vPerspectiveZoomInfo.vCloseFieldOfView / lFieldOfViewScale;
						vPerspectiveZoomInfo.mFarFieldOfViewValue = vPerspectiveZoomInfo.vFarFieldOfView / lFieldOfViewScale;
					}
					break;
				case FieldOfViewFit.Height:
					{
						vPerspectiveZoomInfo.mCloseFieldOfViewValue = vPerspectiveZoomInfo.vCloseFieldOfView;
						vPerspectiveZoomInfo.mFarFieldOfViewValue = vPerspectiveZoomInfo.vFarFieldOfView;
					}
					break;
				}
				vPerspectiveZoomInfo.vFieldOfView = Mathf.Lerp(
					vPerspectiveZoomInfo.mCloseFieldOfViewValue,
					vPerspectiveZoomInfo.mFarFieldOfViewValue,
					mZoomFactor);
			}
			break;
		}

		if ((vLinkObject != null) &&
			vLinkViewArea)
		{
			Vector3 lLinkObjectPosition = vLinkObject.transform.position + vLinkOffset;
			//Debug.Log("lLinkObjectPosition=" + lLinkObjectPosition);
			//Debug.Log("mCurrentViewArea=" + mCurrentViewArea);
			mCurrentViewArea.center += new Vector2(lLinkObjectPosition.x, lLinkObjectPosition.z);
			//Debug.Log("=>mCurrentViewArea=" + mCurrentViewArea);
		}

		if (mLastZoomFactor != mZoomFactor)
		{
			mLastZoomFactor = mZoomFactor;

			// 줌 변경을 알립니다.
			if (mOnZoomChangedDelegate != null)
				mOnZoomChangedDelegate();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줌 거리에 따라 클리핑 된 뷰 평면상의 좌표를 얻습니다.
	private Vector3 _GetClippedViewPlanePosition(Rect pViewArea, Vector3 pViewPlanePosition)
	{
		Vector3 lViewAreaMin = new Vector3(pViewArea.xMin, pViewPlanePosition.y, pViewArea.yMin);
		Vector3 lViewAreaMax = new Vector3(pViewArea.xMax, pViewPlanePosition.y, pViewArea.yMax);
		Vector3 lViewAreaCenter = (lViewAreaMin + lViewAreaMax) * 0.5f;

		Vector3 lClippedViewPlanePosition = pViewPlanePosition;

		if (lViewAreaMin.x < lViewAreaMax.x)
		{
			if (lClippedViewPlanePosition.x < lViewAreaMin.x)
			{
//				Vector3 lPositionVector = lClippedViewPlanePosition - lViewAreaCenter;
//				float lVectorScale = (lViewAreaMin.x - lViewAreaCenter.x) / lPositionVector.x;
				lClippedViewPlanePosition.x = lViewAreaMin.x;
				//				lClippedViewPlanePosition.z = lViewAreaCenter.z + lPositionVector.z * lVectorScale;
				//Logger.Log("pViewPlanePosition{0} viewArea{1}->{2} lViewAreaCenter{3}", pViewPlanePosition, lViewAreaMin, lViewAreaMax, lViewAreaCenter);
				//Logger.Log("case 1: lPositionVector{0} lVectorScale({1}) lClippedViewPlanePosition{2}", lPositionVector, lVectorScale, lClippedViewPlanePosition);
			}
			else if (lClippedViewPlanePosition.x > lViewAreaMax.x)
			{
// 				Vector3 lPositionVector = lClippedViewPlanePosition - lViewAreaCenter;
// 				float lVectorScale = (lViewAreaMax.x - lViewAreaCenter.x) / lPositionVector.x;
				lClippedViewPlanePosition.x = lViewAreaMax.x;
				//				lClippedViewPlanePosition.z = lViewAreaCenter.z + lPositionVector.z * lVectorScale;
				//Logger.Log("pViewPlanePosition{0} viewArea{1}->{2} lViewAreaCenter{3}", pViewPlanePosition, lViewAreaMin, lViewAreaMax, lViewAreaCenter);
				//Logger.Log("case 2: lPositionVector{0} lVectorScale({1}) lClippedViewPlanePosition{2}", lPositionVector, lVectorScale, lClippedViewPlanePosition);
			}
		}
		else
			lClippedViewPlanePosition.x = lViewAreaCenter.x;

		if (lViewAreaMin.z < lViewAreaMax.z)
		{
			if (lClippedViewPlanePosition.z < lViewAreaMin.z)
			{
// 				Vector3 lPositionVector = lClippedViewPlanePosition - lViewAreaCenter;
// 				float lVectorScale = (lViewAreaMin.z - lViewAreaCenter.z) / lPositionVector.z;
//				lClippedViewPlanePosition.x = lViewAreaCenter.x + lPositionVector.x * lVectorScale;
				lClippedViewPlanePosition.z = lViewAreaMin.z;
				//Logger.Log("pViewPlanePosition{0} viewArea{1}->{2} lViewAreaCenter{3}", pViewPlanePosition, lViewAreaMin, lViewAreaMax, lViewAreaCenter);
				//Logger.Log("case 3: lPositionVector{0} lVectorScale({1}) lClippedViewPlanePosition{2}", lPositionVector, lVectorScale, lClippedViewPlanePosition);
			}
			else if (lClippedViewPlanePosition.z > lViewAreaMax.z)
			{
// 				Vector3 lPositionVector = lClippedViewPlanePosition - lViewAreaCenter;
// 				float lVectorScale = (lViewAreaMax.z - lViewAreaCenter.z) / lPositionVector.z;
//				lClippedViewPlanePosition.x = lViewAreaCenter.x + lPositionVector.x * lVectorScale;
				lClippedViewPlanePosition.z = lViewAreaMax.z;
				//Logger.Log("pViewPlanePosition{0} viewArea{1}->{2} lViewAreaCenter{3}", pViewPlanePosition, lViewAreaMin, lViewAreaMax, lViewAreaCenter);
				//Logger.Log("case 4: lPositionVector{0} lVectorScale({1}) lClippedViewPlanePosition{2}", lPositionVector, lVectorScale, lClippedViewPlanePosition);
			}
		}
		else
			lClippedViewPlanePosition.z = lViewAreaCenter.z;

		return lClippedViewPlanePosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// Orthograpic 상태에서의 카메라 위치를 지정합니다.
	private void _TransformOrthograpicCameras()
	{
		//Debug.Log("_TransformOrthograpicCameras()");

		mTransform.rotation = Quaternion.Euler(vViewAngles);

		Vector3 lCameraVector = new Vector3(0.0f, 0.0f, -vOrthographicZoomInfo.vCameraDistance);
		mCameraCenter = vViewPlaneCenter;
		mTransform.position = vViewPlaneCenter + mTransform.rotation * lCameraVector;

		if (!TimeUtil.aIsTimePaused)
			mTransform.position += _GetShakeVector();

		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
		{
			Camera lCamera = mCameras[iCamera];
			lCamera.nearClipPlane = vOrthographicZoomInfo.vNearClipPlane;
			lCamera.farClipPlane = vOrthographicZoomInfo.vFarClipPlane;
			lCamera.orthographicSize = vOrthographicZoomInfo.vViewSize;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Perspective 상태에서의 카메라 위치를 지정합니다.
	private void _TransformPerspectiveCameras()
	{
		//Debug.Log("_TransformPerspectiveCameras()");
		
		Vector3 lViewAngles = vViewAngles;
		lViewAngles.x += mViewPitchOffset;
		mTransform.rotation = Quaternion.Euler(lViewAngles);

		Vector3 lCameraVector = new Vector3(0.0f, 0.0f, -vPerspectiveZoomInfo.vViewDistance);
		if (vIsFarViewCentering &&
			(vPerspectiveZoomInfo.vViewDistance > vPerspectiveZoomInfo.vFarViewSlideDistance))
		{
			Vector3 lFarViewCenter = new Vector3(
				(vPerspectiveZoomInfo.vFarViewArea.xMin + vPerspectiveZoomInfo.vFarViewArea.xMax) * 0.5f,
				vViewPlaneCenter.y,
				(vPerspectiveZoomInfo.vFarViewArea.yMin + vPerspectiveZoomInfo.vFarViewArea.yMax) * 0.5f);
			lFarViewCenter += vPerspectiveZoomInfo.vFarViewCneteringOffset;
			float lFarViewCenteringFactor
				= (vPerspectiveZoomInfo.vViewDistance - vPerspectiveZoomInfo.vFarViewSlideDistance)
				/ (vPerspectiveZoomInfo.vFarViewDistance - vPerspectiveZoomInfo.vFarViewSlideDistance);
			mCameraCenter = Vector3.Lerp(vViewPlaneCenter, lFarViewCenter, lFarViewCenteringFactor);
		}
		else
			mCameraCenter = vViewPlaneCenter;
		mCameraCenter.y += vLinkOffset.y;

// 		if ((vProjection == Projection.Perspective) &&
// 			vIsFocusingZoom)
// 		{
// 			// 카메라가 뉘었을 때 뷰 영역 중심을 카메라 센터로 잡을 경우 아래 쪽이 덜 보이는 현상을 보정하는 처리를 합니다.
// 			float lFovLerpFactor = vPerspectiveZoomInfo.vFieldOfView / 180.0f;
// 			float lFovCorrectionFactor = Mathf.Lerp(0.5f, 0.0f, lFovLerpFactor);
// 			float lPitchLerpFactor = vViewAngles.x / 90.0f;
// 			float lPitchCorrectionFactor = Mathf.Lerp(lFovCorrectionFactor, 0.5f, lPitchLerpFactor);
// 			float lPerspectiveZOffset = mCurrentViewArea.height * (0.5f - lPitchCorrectionFactor);
// 			mCameraCenter.z -= lPerspectiveZOffset;
// 		}

		mTransform.position = mCameraCenter + mTransform.rotation * lCameraVector;

		if (!TimeUtil.aIsTimePaused)
			mTransform.position += _GetShakeVector();

		for (int iCamera = 0; iCamera < mCameras.Length; ++iCamera)
		{
			Camera lCamera = mCameras[iCamera];
			lCamera.nearClipPlane = vPerspectiveZoomInfo.vNearClipPlane;
			lCamera.farClipPlane = vPerspectiveZoomInfo.vFarClipPlane;
			lCamera.fieldOfView = vPerspectiveZoomInfo.vFieldOfView;
		}

		//Debug.Log("_TransformPerspectiveCameras() mTransform.position=" + mTransform.position);
	}
	//-------------------------------------------------------------------------------------------------------
	// 가시 오브젝트들의 보이기 여부를 갱신합니다.
	private void _UpdateVisibleObjectVisibility()
	{
		float lSqrVisibleObjectUpdateDistance = vVisibleObjectUpdateDistance * vVisibleObjectUpdateDistance;
		Vector3 lVisibleObjectUpdateVector = mVisibleObjectUpdatePosition - vViewPlaneCenter;
		float lSqrVisibleObjectDistanceVector
			= lVisibleObjectUpdateVector.x * lVisibleObjectUpdateVector.x
			+ lVisibleObjectUpdateVector.z * lVisibleObjectUpdateVector.z;
		if (lSqrVisibleObjectDistanceVector > lSqrVisibleObjectUpdateDistance)
		{
			float lSqrVisibleObjectActivateDistance
				= vVisibleObjectActivateDistance * vVisibleObjectActivateDistance;
			//Debug.Log("vVisibleObjectActivateDistance=" + vVisibleObjectActivateDistance);

			LinkedListNode<GameObject> lObjectNode = mVisibleObjectList.First;
			LinkedListNode<GameObject> lNextObjectNode;
			while (lObjectNode != null)
			{
				GameObject lObject = lObjectNode.Value;
				lNextObjectNode = lObjectNode.Next;

				Vector3 lDistanceVector = lObject.transform.position - vViewPlaneCenter;
				float lSqrDistanceVector
					= lDistanceVector.x * lDistanceVector.x
					+ lDistanceVector.z * lDistanceVector.z;
				bool bIsSetActive = lSqrDistanceVector <= lSqrVisibleObjectActivateDistance;
				if (bIsSetActive)
				{
					lObject.SetActive(true);
				}
				else
				{
					if (!vIsDestroyInvisibleObjects)
					{
						lObject.SetActive(false);
					}
					else
					{
						lObjectNode.List.Remove(lObjectNode);
						Destroy(lObject);
					}
				}

				//if (lSqrDistanceVector <= lSqrVisibleObjectActivateDistance)
				//	Debug.Log("Visible - lDistanceVector=" + lDistanceVector);
				//else
				//	Debug.Log("Invisible - lDistanceVector=" + lDistanceVector);

				lObjectNode = lNextObjectNode;
			}

			mVisibleObjectUpdatePosition = vViewPlaneCenter;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 흔들기 벡터를 얻습니다.
	private Vector3 _GetShakeVector()
	{
		float lShakeSize = vShakeMaxSize * Mathf.Min(mShakeTimer, vShakeDelay) / vShakeDelay;
		//Debug.Log("lShakeSize=" + lShakeSize);
		Vector3 lShakeVector
			= mTransform.rotation
			* Quaternion.Euler(0.0f, 0.0f, UnityEngine.Random.Range(0.0f, 360.0f))
			* new Vector3(0.0f, lShakeSize, 0.0f);

		return lShakeVector;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Camera[] mCameras;
// 	private Vector3[] mCameraOffsets;
	private Transform[] mSharingCameraTransforms;
	private RenderTexture[] mCameraRenderTextures;
	private Rect mLastUpdateViewport;
	private Rect mCurrentViewArea;
	private Vector3 mCurrentZoomPosition;
	private Plane mInstantZoomPlane;
	private Vector2 mInstantZoomPoint;
	private bool mRestoreInstantZoom;
	private float mZoomFactor;
	private float mLastZoomFactor;
	private float mViewPitchOffset;
	private bool mIsZoomed;
	private float mZoomLerpFactor;
	private Projection mLastUpdateProjection;
	private Vector3 mCameraCenter;
	private LinkedList<GameObject> mVisibleObjectList;
	private Vector3 mVisibleObjectUpdatePosition;

	private float mShakeTimer;

	private OnZoomChangedDelegate mOnZoomChangedDelegate;
}
