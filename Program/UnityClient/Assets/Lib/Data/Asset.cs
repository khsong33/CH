﻿using UnityEngine;
using System.Collections;
using System;

public class Asset
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 텍스트 에셋을 읽어옵니다.
	public static String LoadText(String pTextAssetBundle, String pTextAssetPath)
	{
		AssetKey lTextAssetKey = AssetManager.get.CreateAssetKey(pTextAssetBundle, pTextAssetPath);
		TextAsset lTextAsset = AssetManager.get.Load<TextAsset>(lTextAssetKey);
		return lTextAsset.text;
	}
}
