using UnityEngine;
using System.Collections;
using System;

public class ColorLib
{
	public const float cColor256Scale = 1.0f / 255.0f;

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 위젯의 알팔르 제외한 컬러를 지정합니다.
	public static void SetWidgetColor(UIWidget pWidget, Color pColor)
	{
		Color lWidgetColor = pColor;
		lWidgetColor.a = pWidget.alpha;
		pWidget.color = lWidgetColor;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 위젯의 알파를 지정합니다.
	public static void SetWidgetAlpha(UIWidget pWidget, float pAlpha)
	{
		Color lWidgetColor = pWidget.color;
		lWidgetColor.a = pAlpha;
		pWidget.color = lWidgetColor;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 스프라이트의 알파를 지정합니다.
	public static void SetSpriteAlpha(tk2dBaseSprite pSprite, float pAlpha)
	{
		Color lSpriteColor = pSprite.color;
		lSpriteColor.a = pAlpha;
		pSprite.color = lSpriteColor;
	}
}
