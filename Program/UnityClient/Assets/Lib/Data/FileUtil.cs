using UnityEngine;
using System.Collections;
using System.IO;
using System;

namespace CombatHeroes // UnityEditor.FileUtil과 구분하기 위해서
{

public class FileUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 경로의 파일을 읽어서 내용 문자열을 반환합니다.
	public static String ReadTextFile(String pFilePath)
	{
		try
		{
			using (StreamReader lStreamReader = new StreamReader(pFilePath)) // 오류 상황에서도 Dispose()되도록 using 사용할 것
			{
				String lTextContents = lStreamReader.ReadToEnd();
				lStreamReader.Close();
				return lTextContents;
			}
		}
		catch (Exception e)
		{
			Console.WriteLine("The file could not be read:");
			Console.WriteLine(e.Message);
		}

		return String.Empty;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 경로의 파일을 생성해서 내용 문자열을 저장합니다.
	public static void WriteTextFile(String pFilePath, String pTextContents)
	{
		try
		{
			using (StreamWriter lStreamWriter = new StreamWriter(pFilePath)) // 오류 상황에서도 Dispose()되도록 using 사용할 것
			{
				lStreamWriter.WriteLine(pTextContents);
				//lStreamWriter.Flush(); // Close() 할 것이라면 불필요
				lStreamWriter.Close();
			}
		}
		catch (Exception e)
		{
			Console.WriteLine("The file could not be write:");
			Console.WriteLine(e.Message);
		}
	}
}

} // namespace CombatHeroes
