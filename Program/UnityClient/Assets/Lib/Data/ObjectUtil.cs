using UnityEngine;
using System.Collections;
using System;

public class ObjectUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 콤포넌트를 가진 프리팹으로 게임 오브젝트를 생성한 다음에 콤포넌트를 찾아 반환합니다.
	public static T_Component InstantiateComponentObject<T_Component>(T_Component pPrefab) where T_Component : Component
	{
		GameObject lGameObject = (GameObject)GameObject.Instantiate(pPrefab.gameObject);
		T_Component lComponent = lGameObject.GetComponent<T_Component>();

		return lComponent;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 콤포넌트를 가진 프리팹으로 게임 오브젝트를 생성한 다음에 콤포넌트를 찾아 반환합니다.
	public static T_Component InstantiateComponentObject<T_Component>(T_Component pPrefab, Vector3 pPosition, Quaternion pRotation) where T_Component : Component
	{
		GameObject lGameObject = (GameObject)GameObject.Instantiate(pPrefab.gameObject, pPosition, pRotation);
		T_Component lComponent = lGameObject.GetComponent<T_Component>();

		return lComponent;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 콤포넌트를 가진 오브젝트를 찾거나, 없다면 프리팹으로 게임 오브젝트를 생성한 다음에 해당 콤포넌트를 찾아 반환합니다.
	public static T_Component FindOrInstantiateComponentObject<T_Component>(String pGameObjectPath, T_Component pPrefab) where T_Component : Component
	{
		T_Component lComponent = null;
	
		GameObject lGameObject = GameObject.Find(pGameObjectPath);
		if (lGameObject != null)
		{
			lComponent = lGameObject.GetComponent<T_Component>();
			if (lComponent == null)
				Debug.LogError(String.Format("can't find component {0} in the scene object \"{1}\"", typeof(T_Component).Name, pGameObjectPath));
		}
		if (lComponent == null)
		{
			lGameObject = (GameObject)GameObject.Instantiate(pPrefab.gameObject);
			lComponent = lGameObject.GetComponent<T_Component>();
		}
		
		return lComponent;
	}
	//-------------------------------------------------------------------------------------------------------
	// 자식 노드 중에서 해당 콤포넌트를 가진 오브젝트를 찾아 반환합니다.
	public static T_Component FindChild<T_Component>(Transform pTransform, bool pIsRecursive) where T_Component : Component
	{
		foreach (Transform iChild in pTransform)
		{
			// 자식 노드에서 찾습니다.
			T_Component lComponent = iChild.gameObject.GetComponent<T_Component>();
			if (lComponent != null)
				return lComponent;
			
			// 재귀적으로 자식 노드의 자식 노드에서 찾습니다.
			if (pIsRecursive)
			{
				lComponent = FindChild<T_Component>(iChild, true);
				if (lComponent != null)
					return lComponent;
			}			
		}

		return null;
	}
}
