using UnityEngine;
using System.Collections;
using System;

public class ResourceUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 해당 경로의 문자열을 얻습니다.
	public static String LoadString(String pStringPath)
	{
		TextAsset lTextAsset = Load<TextAsset>(pStringPath);
		if (lTextAsset != null)
			return lTextAsset.text;
		else
			return String.Empty;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 프리팹을 로딩한 다음에 반환합니다(리소스 로딩 과정에서 파일이 없거나 하는 등의 경우에 대한 에러 로그를 남기는 유틸 함수입니다).
	public static T_Object Load<T_Object>(String pResourcePath) where T_Object : UnityEngine.Object
	{
		UnityEngine.Object lResourceObject = Resources.Load(pResourcePath);
		if (lResourceObject == null)
		{
			Debug.LogError("can't load resource \"" + pResourcePath + "\"");
			return null;
		}
		
		T_Object lObject = lResourceObject as T_Object;
		if (lObject == null)
		{
			Debug.LogError(String.Format("can't load resource \"{0}\" as {1}", pResourcePath, typeof(T_Object).Name));
			return null;
		}

		return lObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 읽은 프리팹으로 게임 오브젝트를 생성한 다음에 반환합니다(리소스 로딩 과정에서 파일이 없거나 하는 등의 경우에 대한 에러 로그를 남기는 유틸 함수입니다).
	public static T_Object InstantiateObject<T_Object>(String pPrefabPath) where T_Object : UnityEngine.Object
	{
		UnityEngine.Object lResourceObject = Resources.Load(pPrefabPath);
		if (lResourceObject == null)
		{
			Debug.LogError("can't load resource \"" + pPrefabPath + "\"");
			return null;
		}

		T_Object lObject = UnityEngine.Object.Instantiate(lResourceObject) as T_Object;
		if (lObject == null)
		{
			Debug.LogError(String.Format("can't instantiate resource \"{0}\" as {1}", pPrefabPath, typeof(T_Object).Name));
			return null;
		}

		return lObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 리소스에서 읽은 프리팹으로 게임 오브젝트를 생성한 다음에 해당 콤포넌트를 찾아 반환합니다(리소스 로딩 과정에서 파일이 없거나 하는 등의 경우에 대한 에러 로그를 남기는 유틸 함수입니다).
	public static T_Component InstantiateComponent<T_Component>(String pPrefabPath) where T_Component : Component
	{
		UnityEngine.Object lResourceObject = Resources.Load(pPrefabPath);
		if (lResourceObject == null)
		{
			Debug.LogError("can't load resource \"" + pPrefabPath + "\"");
			return null;
		}

		GameObject lGameObject = UnityEngine.Object.Instantiate(lResourceObject) as GameObject;
		if (lGameObject == null)
		{
			Debug.LogError("can't instantiate resource object \"" + pPrefabPath + "\"");
			return null;
		}
		
		T_Component lComponent = lGameObject.GetComponent<T_Component>();
		if (lComponent == null)
		{
			Debug.LogError(String.Format("can't find component {0} in resource object \"{1}\"", typeof(T_Component).Name, pPrefabPath));
			return null;
		}
			
		return lComponent;
	}
}
