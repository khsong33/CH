using UnityEngine;
using System.Collections;
using System;

public class TimeUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 서버와 동기화 하는 시간
	public static DateTime aSyncTime
	{
		get { return sSyncTime;}
		set { 
			sSyncTime = value;
			sPreSyncTime = DateTime.Now;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간 멈춤 여부
	public static bool aIsTimePaused
	{
		get { return sIsTimePaused; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간 가속 여부
	public static bool aIsTimeAcceled
	{
		get { return sIsTimeAcceled; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 시간
	public static float aNormalTimeScale
	{
		get { return sNormalTimeScale; }
		set
		{
			sNormalTimeScale = value;
			UpdateTimeScale();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 가속 시간
	public static float aAccelTimeScale
	{
		get { return (sAccelTimeScale * (sIsDebugTimeAcceled ? sDebugAccelTimeScale : 1.0f)); }
		set
		{
			sAccelTimeScale = value;
			UpdateTimeScale();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 시간을 멈춥니다.
	static public void PauseTime(bool pIsTimePaused)
	{
		sIsTimePaused = pIsTimePaused;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 가속합니다.
	static public void AccelTime(float pAccelTimeScale)
	{
		sIsTimeAcceled = true;
		sAccelTimeScale = pAccelTimeScale;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 가속합니다.
	static public void AccelTime(bool pIsTimeAcceled)
	{
		sIsTimeAcceled = pIsTimeAcceled;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 시간을 가속합니다.
	static public void DebugAccelTime(float pDebugAccelTimeScale)
	{
		sIsDebugTimeAcceled = true;
		sDebugAccelTimeScale = pDebugAccelTimeScale;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 시간을 가속합니다.
	static public void DebugAccelTime(bool pIsDebugTimeAcceled)
	{
		sIsDebugTimeAcceled = pIsDebugTimeAcceled;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 현재 상태로 갱신합니다.
	static public void UpdateTimeScale()
	{
		Time.timeScale = sIsTimePaused
			? 0.0f
			: (sIsTimeAcceled ? sAccelTimeScale : sNormalTimeScale) * (sIsDebugTimeAcceled ? sDebugAccelTimeScale : 1.0f);
		//Debug.Log("Time.timeScale=" + Time.timeScale);
	}
	//-------------------------------------------------------------------------------------------------------
	// 남은시간을 구합니다.
	static public TimeSpan GetRemainTime(DateTime pStartTime, DateTime pEndTime)
	{
		return pEndTime - pStartTime;
	}
	//-------------------------------------------------------------------------------------------------------
	// 서버시간을 이용해 남은시간을 구합니다.
	static public TimeSpan GetRemainTime(DateTime pEndTime)
	{
		return pEndTime - aSyncTime;
	}
	//-------------------------------------------------------------------------------------------------------
	// 게임 컨트롤이 활성화 된 이후에는 매 프레임 호출합니다.
	static public void Update()
	{
		TimeSpan lRealTimeSpan = DateTime.Now - sPreSyncTime;
		aSyncTime += lRealTimeSpan;
	}
	//-------------------------------------------------------------------------------------------------------
	// string으로 된 시간을 DateTime형으로 변환합니다.(서버사이드) - lagacy
	static public DateTime ConvertStrToDateTime(String strTime)
	{
		int lYear = Convert.ToInt32(strTime.Substring(0, 4));
		int lMonth = Convert.ToInt32(strTime.Substring(4, 2));
		int lDay = Convert.ToInt32(strTime.Substring(6, 2));
		int lHour = Convert.ToInt32(strTime.Substring(8, 2));
		int lMinute = Convert.ToInt32(strTime.Substring(10, 2));
		int lSecond = Convert.ToInt32(strTime.Substring(12, 2));

	//	Debug.Log(lYear + " " + lMonth + " " + lDay + " " + lHour + " " + lMinute + " " + lSecond);
		return new DateTime(lYear, lMonth, lDay, lHour, lMinute, lSecond);
	}
	//-------------------------------------------------------------------------------------------------------
	// string으로 된 시간을 DateTime형으로 변환합니다.(클라이언트 CSV 문서용) - lagacy
	static public DateTime ConvertStrToDateTimeInClient(String strTime)
	{
		char[] lSplitToken = { '/',' ', ':', '-' };
		string[] lSplitString = strTime.Split(lSplitToken);

		int lYear = Convert.ToInt32(lSplitString[0]);
		int lMonth = Convert.ToInt32(lSplitString[1]);
		int lDay = Convert.ToInt32(lSplitString[2]);
		int lHour = Convert.ToInt32(lSplitString[3]);
		int lMinute = Convert.ToInt32(lSplitString[4]);
		int lSecond = Convert.ToInt32(lSplitString[5]);

		return new DateTime(lYear, lMonth, lDay, lHour, lMinute, lSecond);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 시간과 끝시간을 확인합니다.
	static public bool IsAvailableTime(DateTime pStartTime, DateTime pEndTime)
	{
		if (pStartTime <= aSyncTime && pEndTime >= aSyncTime)
			return true;

		return false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static DateTime sSyncTime;
	private static DateTime sPreSyncTime;
	
	private static bool sIsTimePaused = false;
	private static bool sIsTimeAcceled = false;
	private static float sAccelTimeScale = 1.0f;
	private static bool sIsDebugTimeAcceled = false;
	private static float sDebugAccelTimeScale = 1.0f;
	private static float sNormalTimeScale = 1.0f;
}
