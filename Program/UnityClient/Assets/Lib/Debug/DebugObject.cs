using UnityEngine;
using System.Collections;
using System;

public class DebugObject : MonoBehaviour
{
	public enum Action
	{
		InactivateOnPlay,
		HideOnPlay,
		Static,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Action action = Action.InactivateOnPlay;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if ((action == Action.InactivateOnPlay) &&
			Application.isPlaying)
			gameObject.SetActive(false);
		else if ((action == Action.HideOnPlay) &&
			(GetComponent<Renderer>() != null) &&
			Application.isPlaying)
			GetComponent<Renderer>().enabled = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
