using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System;

public class DebugUtil
{
	internal const int cCirclePrecision = 36;
	internal const float cCircleSnapRadian = Mathf.PI * 2.0f / cCirclePrecision;

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// Assert
	[Conditional("UNITY_EDITOR")]
	public static void Assert(bool pCondition, String pMessage)
	{
		if (!pCondition)
			UnityEngine.Debug.LogError("Assert: " + pMessage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 벡터를 정확한 소수값으로 출력합니다.
	public static String LogVector(Vector2 pVector)
	{
		return String.Format("({0:0.##},{1:0.##})", pVector.x, pVector.y);
	}
	//-------------------------------------------------------------------------------------------------------
	// 벡터를 정확한 소수값으로 출력합니다.
	public static String LogVector(Vector3 pVector)
	{
		return String.Format("({0:0.##},{1:0.##},{2:0.##})", pVector.x, pVector.y, pVector.z);
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표축을 그립니다.
	[Conditional("UNITY_EDITOR")]
	public static void DrawAxis(Vector3 pOrigin, Quaternion pRotation, float pScale, float pDuration, bool pDepthTest)
	{
		Vector3 lXAxis = pRotation * new Vector3(pScale, 0.0f, 0.0f);
		Vector3 lYAxis = pRotation * new Vector3(0.0f, pScale, 0.0f);
		Vector3 lZAxis = pRotation * new Vector3(0.0f, 0.0f, pScale);

		UnityEngine.Debug.DrawLine(pOrigin, pOrigin + lXAxis, Color.red, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(pOrigin, pOrigin + lYAxis, Color.green, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(pOrigin, pOrigin + lZAxis, Color.blue, pDuration, pDepthTest);
	}
	//-------------------------------------------------------------------------------------------------------
	// 좌표축을 그립니다.
	[Conditional("UNITY_EDITOR")]
	public static void DrawAxis(float pScale, float pDuration, bool pDepthTest)
	{
		UnityEngine.Debug.DrawLine(Vector3.zero, new Vector3(pScale, 0.0f, 0.0f), Color.red, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(Vector3.zero, new Vector3(0.0f, pScale, 0.0f), Color.green, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(Vector3.zero, new Vector3(0.0f, 0.0f, pScale), Color.blue, pDuration, pDepthTest);
	}
	//-------------------------------------------------------------------------------------------------------
	// Y축 법선 평면을 그립니다.
	[Conditional("UNITY_EDITOR")]
	public static void DrawPlaneY(Rect pArea, float pY, Color pColor, float pDuration, bool pDepthTest)
	{
		UnityEngine.Debug.DrawLine(new Vector3(pArea.xMin, pY, pArea.yMin), new Vector3(pArea.xMin, pY, pArea.yMax), pColor, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(new Vector3(pArea.xMin, pY, pArea.yMax), new Vector3(pArea.xMax, pY, pArea.yMax), pColor, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(new Vector3(pArea.xMax, pY, pArea.yMax), new Vector3(pArea.xMax, pY, pArea.yMin), pColor, pDuration, pDepthTest);
		UnityEngine.Debug.DrawLine(new Vector3(pArea.xMax, pY, pArea.yMin), new Vector3(pArea.xMin, pY, pArea.yMin), pColor, pDuration, pDepthTest);
	}
	//-------------------------------------------------------------------------------------------------------
	// Y축 법선 원을 그립니다.
	[Conditional("UNITY_EDITOR")]
	public static void DrawCircleY(Vector3 pCenter, float pRadius, Color pColor, float pDuration, bool pDepthTest)
	{
		Vector3 lLineStart;
		Vector3 lLineEnd = pCenter;
		lLineEnd.z += pRadius;

		for (int iPoint = 1; iPoint <= cCirclePrecision; ++iPoint)
		{
			lLineStart = lLineEnd;
			lLineEnd = new Vector3(
				pCenter.x + pRadius * Mathf.Sin(iPoint * cCircleSnapRadian),
				pCenter.y,
				pCenter.z + pRadius * Mathf.Cos(iPoint * cCircleSnapRadian));

			UnityEngine.Debug.DrawLine(lLineStart, lLineEnd, pColor, pDuration, pDepthTest);
		}
	}
}
