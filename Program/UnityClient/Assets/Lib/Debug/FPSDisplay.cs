using UnityEngine;
using System.Collections;
using System;

[RequireComponent(typeof(GUIText))]
public class FPSDisplay : MonoBehaviour
{
	public const float cFPSUpdateDelay = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public KeyCode applicationExitKey = KeyCode.None;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public float aFPSCurrent
	{
		get { return mFPSCurrent; }
	}
	//-------------------------------------------------------------------------------------------------------
	public float aFPSAverage
	{
		get { return mFPSAverage; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		StartCoroutine(_UpdateFPS());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] FPS를 계속 업데이트합니다.
	private IEnumerator _UpdateFPS()
	{
		for (; ; )
		{
			if (Time.deltaTime > 0.0f)
			{
				mFPSSum += Time.deltaTime;
				++mFPSCounter;

				mFPSCurrent = 1.0f / Time.deltaTime;

				mFPSUpdateTimer -= Time.deltaTime;
				if (mFPSUpdateTimer <= 0.0f)
				{
					mFPSAverage = 1.0f / (mFPSSum / mFPSCounter);
					mFPSSum = 0.0f;
					mFPSCounter = 0;
					mFPSUpdateTimer = cFPSUpdateDelay;
				}

				GetComponent<GUIText>().text = String.Format(
					"FPS {0:F} ({1:F})", mFPSAverage * Time.timeScale, mFPSCurrent * Time.timeScale);
				//Debug.Log("guiText.text=" + guiText.text);

				if (Input.GetKeyUp(applicationExitKey))
				{
					Debug.Log("FPSDisplay Application.Quit();");
					Application.Quit();
				}
			}

			yield return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private float mFPSCurrent = 1.0f;
	private float mFPSAverage = 1.0f;
	private float mFPSSum = 0.0f;
	private int mFPSCounter = 0;
	private float mFPSUpdateTimer = cFPSUpdateDelay;
}
