using UnityEngine;
using System.Collections;
using System;

public class ObjectGenerator : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vInfoLabel;
	public TouchPanel vTouchPanel;

	public GameObject vObjectPrefab;
	public Vector3 vObjectRotation = new Vector3(0.0f, 0.0f, 0.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vInfoLabel == null)
		{
			vInfoLabel = GetComponent<UILabel>();
			if (vInfoLabel == null)
				Debug.LogError("can't find vInfoLabel - " + this);
		}
		if (vTouchPanel == null)
			Debug.LogError("vTouchPanel is empty - " + this);

		if (vObjectPrefab == null)
			Debug.LogError("vObjectPrefab is empty - " + this);

		vTouchPanel.aOnClickDelegate = _OnClick;

		mObjectCount = 1;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClick(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded)
	{
		//Debug.Log("_OnClick() pClickPosition=" + pClickPosition);

		Quaternion lObjectRotation = Quaternion.Euler(vObjectRotation);

		Vector3 lClickPosition = CameraUtil.GetTouchPosition(Camera.main, pClickPlane, pClickPoint);
		GameObject.Instantiate(vObjectPrefab, lClickPosition, lObjectRotation);

		++mObjectCount;
		vInfoLabel.text = "Object Count : " + mObjectCount;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mObjectCount;
}
