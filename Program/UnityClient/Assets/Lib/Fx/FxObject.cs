using UnityEngine;
using System.Collections;
using System;

public class FxObject : MonoBehaviour
{
	public delegate void OnFxEventDelegate(FxObject pFxObject, float pEventValue);
	public delegate void OnFxUpdateDelegate(FxObject pFxObject);
	public delegate void OnFxEndDelegate(FxObject pFxObject);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Animation vAnimation;
	public float vAnimationLength = 5.0f;
	
	public GameObject vMessageObject;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnFxEventDelegate aOnFxEvent
	{
		get { return mOnFxEventDelegate; }
		set { mOnFxEventDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnFxUpdateDelegate aOnFxUpdate
	{
		get { return mOnFxUpdateDelegate; }
		set { mOnFxUpdateDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnFxEndDelegate aOnFxEnd
	{
		get { return mOnFxEndDelegate; }
		set { mOnFxEndDelegate = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);
	
		if (vAnimation == null)
		{
			vAnimation = GetComponent<Animation>();
			if (vAnimation != null)
				vAnimationLength = vAnimation.clip.length;
		}

		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mIsStarted = true;
		_StartFx();
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		//Debug.Log("OnEnable() - " + this);
	
		if (mIsStarted)
			_StartFx(); // 한 번 시작했던 효과는 활성화 될 때마다 다시 시작합니다.
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 이벤트를 발생시킵니다.
	public void SetEvent(float pEventValue)
	{
		//Debug.Log("PlayFxEvent() pEventValue=" + pEventValue + " - " + this);

		if (mOnFxEventDelegate != null)
			mOnFxEventDelegate(this, pEventValue);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 효과를 시작합니다.
	private void _StartFx()
	{
		mIsEnded = false;

		// 이벤트 호출을 위한 코루틴을 실행합니다.
		if (mOnFxUpdateDelegate != null)
			StartCoroutine(_UpdateFx());
		if ((mOnFxEndDelegate != null) &&
			(vAnimationLength > 0.0f))
			StartCoroutine(_EndFx(vAnimationLength));
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 효과를 갱신합니다.
	private IEnumerator _UpdateFx()
	{
		for (; ; )
		{
			if (mIsEnded)
				yield break;

			if (mOnFxUpdateDelegate != null)
				mOnFxUpdateDelegate(this);

			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 딜레이 후에 효과를 멈춥니다.
	private IEnumerator _EndFx(float pEndDelay)
	{
		yield return new WaitForSeconds(pEndDelay);

		mIsEnded = true;

		if (mOnFxEndDelegate != null)
			mOnFxEndDelegate(this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private bool mIsStarted = false;
	private bool mIsEnded;
	private OnFxEventDelegate mOnFxEventDelegate;
	private OnFxUpdateDelegate mOnFxUpdateDelegate;
	private OnFxEndDelegate mOnFxEndDelegate;
}
