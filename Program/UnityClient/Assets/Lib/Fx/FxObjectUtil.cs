using UnityEngine;
using System.Collections;
using System;

public class FxObjectUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 에셋 키를 가지고 효과 오브젝트를 생성합니다.
	public static FxObject CreateFxObject(AssetKey pFxAssetKey, Vector3 pPosition, Quaternion pRotation, GameObject pMessageObject, FxObject.OnFxEventDelegate pOnFxEventDelegate, FxObject.OnFxUpdateDelegate pOnFxUpdateDelegate, FxObject.OnFxEndDelegate pOnFxEndDelegate)
	{
		FxObject lFxObject = AssetManager.get.InstantiateComponent<FxObject>(pFxAssetKey);

	#if UNITY_EDITOR // [ExecuteInEditMode()] 스크립트라서 Awake()가 호출이 아직 안되어 aTransform이 초기화가 안되어 있으므로
		lFxObject.transform.position = pPosition;
		lFxObject.transform.rotation = pRotation;
	#else
		lFxObject.aTransform.position = pPosition;
		lFxObject.aTransform.rotation = pRotation;
	#endif

		lFxObject.vMessageObject = pMessageObject;

		lFxObject.aOnFxEvent = pOnFxEventDelegate;
		lFxObject.aOnFxUpdate = pOnFxUpdateDelegate;
		lFxObject.aOnFxEnd = pOnFxEndDelegate;

		lFxObject.gameObject.SetActive(true);
		
		return lFxObject;
	}
	//-------------------------------------------------------------------------------------------------------
	// 에셋 정보를 가지고 효과 오브젝트를 생성합니다.
	public static FxObject CreateFxObject(AssetInfo pFxAssetInfo, Vector3 pPosition, Quaternion pRotation, GameObject pMessageObject, FxObject.OnFxEventDelegate pOnFxEventDelegate, FxObject.OnFxUpdateDelegate pOnFxUpdateDelegate, FxObject.OnFxEndDelegate pOnFxEndDelegate)
	{
		AssetKey lFxAssetKey = AssetManager.get.CreateAssetKey(pFxAssetInfo);
		if (!AssetKey.IsValid(lFxAssetKey))
		{
			Debug.LogError("invalid asset key '" + lFxAssetKey);
			return null;
		}
		
		return CreateFxObject(lFxAssetKey, pPosition, pRotation, pMessageObject, pOnFxEventDelegate, pOnFxUpdateDelegate, pOnFxEndDelegate);
	}
}
