using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ExternalLocaleMgr
{
	public enum eLANG
	{
		KOR = 0,
		ENG,
	}
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 인스턴스
	public static ExternalLocaleMgr Instance
	{
		get
		{
			if (mInstance == null)
			{
				mInstance = new ExternalLocaleMgr();
			}
			return mInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public ExternalLocaleMgr()
	{
		TextAsset lCSVTextAsset = Resources.Load("TextAsset/ExternalLocaleCSV") as TextAsset;
		Load(lCSVTextAsset);
	}
	public void Load(TextAsset pCSVTextAsset)
	{
		mCSVObject = new CSVObject();
		mCSVObject.LoadCSV(pCSVTextAsset.text, 0);

		mLocalizedDictionary = new Dictionary<string, LocalizedData>();

        int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			LocalizedData info = new LocalizedData();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mKey = lCSVRow.GetStringValue(vKeyColName);

			info.mKor = lCSVRow.GetStringValue(vKorColName);
			info.mEng = lCSVRow.GetStringValue(vEngColName);

			if (!mLocalizedDictionary.ContainsKey(info.mKey))
			{
				mLocalizedDictionary.Add(info.mKey, info);
			}

		}
	}
	public void SetLang(eLANG pLanguage)
	{
		vLangauge = pLanguage;
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	public string GetLocalizedText(string pKey)
	{
		string lResult = string.Empty;
		// 키에 대응되는 값이 없을 경우 키값을 다시 리턴해준다.
		if(!mLocalizedDictionary.ContainsKey(pKey))
		{
			return pKey;
		}

		if (vLangauge == eLANG.KOR)
			lResult = mLocalizedDictionary[pKey].mKor;
		else if (vLangauge == eLANG.ENG)
			lResult = mLocalizedDictionary[pKey].mEng;

		return lResult;
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CSVObject mCSVObject;
	private Dictionary<string, LocalizedData> mLocalizedDictionary;

	private string vKeyColName = "Key";
	private string vKorColName = "KOR";
	private string vEngColName = "ENG";

	private eLANG vLangauge = eLANG.KOR;

	private static ExternalLocaleMgr mInstance = null;

}
