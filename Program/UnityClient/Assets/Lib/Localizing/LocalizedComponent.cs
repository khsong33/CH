using UnityEngine;
using System.Collections;
using System;
[AddComponentMenu("Game/Localized/Localized Component")]
public class LocalizedComponent : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	
	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	void Start () 
	{
		UILabel LocalizedTextLabel = gameObject.GetComponent<UILabel>();
		if (LocalizedTextLabel != null)
		{
			string lKey = LocalizedTextLabel.text;
			LocalizedTextLabel.text = LocalizedTable.get.GetLocalizedText(lKey);
		}
		else
		{
			Debug.LogError("LocalizedComponent is attached to Label. check GameObject [" + gameObject + "]");
		}
	}
}
