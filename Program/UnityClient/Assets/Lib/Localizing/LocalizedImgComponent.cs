//using UnityEngine;
//using System.Collections;
//using System.Collections.Generic;
//using System;

//[AddComponentMenu("Game/Localized/LocalizedImage Component")]
//public class LocalizedImgComponent : MonoBehaviour
//{
//	//-------------------------------------------------------------------------------------------------------
//	// 인스펙터
//	//-------------------------------------------------------------------------------------------------------
//	public String LOCAL_KOR_NAME = "Local KOR Atlas";
//	public String LOCAL_ENG_NAME = "Local ENG Atlas";
//	public String vSpriteName;
//	//-------------------------------------------------------------------------------------------------------
//	// 콜백
//	//-------------------------------------------------------------------------------------------------------
//	// 시작 콜백
//	void Start () 
//	{
//		if (GameControl.sInstance != null)
//		{
//			GameControl.sInstance.RegisteLocaleImg(this);
//		}
//		vSpriteName = gameObject.GetComponent<UISprite>().spriteName;
//		LocalRefresh();
//	}
//	//-------------------------------------------------------------------------------------------------------
//	// 삭제 콜백
//	void OnDestroy()
//	{
//		if (GameControl.sInstance != null)
//		{
//			GameControl.sInstance.UnRegisteLocaleImg(this);
//		}
//	}

//	//-------------------------------------------------------------------------------------------------------
//	// 메서드
//	//-------------------------------------------------------------------------------------------------------
//	// 갱신
//	public void LocalRefresh()
//	{
//		UISprite lLocalizedSprite = gameObject.GetComponent<UISprite>();

//		GameObject lObj;
//		if (testflag)
//		{
//			lObj = Resources.Load("Atlas/Locale Atlas/ENG/" + LOCAL_ENG_NAME) as GameObject;
//		}
//		else
//		{
//			lObj = Resources.Load("Atlas/Locale Atlas/KOR/" + LOCAL_KOR_NAME) as GameObject;
//		}
//		lLocalizedSprite.atlas = lObj.GetComponent<UIAtlas>();
//		lLocalizedSprite.spriteName = vSpriteName;

//		UIAtlas.Sprite lRealSprite = lLocalizedSprite.GetAtlasSprite();
//		lLocalizedSprite.transform.localScale = new Vector3(lRealSprite.outer.width,
//													lRealSprite.outer.height,
//													1);

//		testflag = !testflag;
//	}

//	private bool testflag = false;
//}
