using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class LocalizedData
{
	public String mKey;
	public String mKor;
	public String mEng;
}

public class LocalizedTable
{
	public enum eLANG
	{
		KOR = 0,
		ENG,
	}

	public static readonly String cCSVTextAssetPath = "LocalizedTableCSV";

	public static readonly String vKeyColName = "Key";
	public static readonly String vKorColName = "KOR";
	public static readonly String vEngColName = "ENG";
	public static readonly eLANG cDefaultLanguage = eLANG.KOR;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static LocalizedTable get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재 언더
	public eLANG aLanguage
	{
		get { return mLanguage; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new LocalizedTable();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public LocalizedTable()
	{
		Load(cCSVTextAssetPath);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 로컬 데이터를 로딩합니다.
	public void Load(String pCSVTextAssetPath)
	{
		mCSVObject = new CSVObject();

		String lCSVText = Asset.LoadText(
			Config.get.GetStringValue("TablesBundle"),
			pCSVTextAssetPath);

		mCSVObject.LoadCSV(lCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mLocalizedDictionary = new Dictionary<String, LocalizedData>();

		int lCSVRowCount = mCSVObject.RowNum;
		for (int iRow = 0; iRow < lCSVRowCount; iRow++)
		{
			LocalizedData info = new LocalizedData();
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			// GUID(고유 number)가 숫자가 아닌경우 다음 row로 이동
			info.mKey = lCSVRow.GetStringValue(vKeyColName);

			info.mKor = lCSVRow.GetStringValue(vKorColName);
			info.mEng = lCSVRow.GetStringValue(vEngColName);

			if (!mLocalizedDictionary.ContainsKey(info.mKey))
			{
				mLocalizedDictionary.Add(info.mKey, info);
			}

		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제 변경할 언어를 처리합니다.
	public String GetLocalizedText(String pKey)
	{
		if(String.IsNullOrEmpty(pKey))
			return String.Empty;
		String lResult = String.Empty;
		if (String.IsNullOrEmpty(pKey))
		{
			return String.Empty;
		}
		// 키에 대응되는 값이 없을 경우 키값을 다시 리턴해준다.
		if(!mLocalizedDictionary.ContainsKey(pKey))
		{
			return pKey;
		}

		if (mLanguage == eLANG.KOR)
			lResult = mLocalizedDictionary[pKey].mKor;
		else if (mLanguage == eLANG.ENG)
			lResult = mLocalizedDictionary[pKey].mEng;

		return lResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 언어 교체
	public void SetLanguage(eLANG pLanguage)
	{
		mLanguage = pLanguage;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static LocalizedTable sInstance = null;

	private CSVObject mCSVObject;
	private Dictionary<String, LocalizedData> mLocalizedDictionary;

	private eLANG mLanguage = cDefaultLanguage;
}
