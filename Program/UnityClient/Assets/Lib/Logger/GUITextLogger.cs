using UnityEngine;
using System.Collections;
using System;

public class GUITextLogger : MonoBehaviour, ILogger
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public bool globalLogger = true;
	public int scrollLineCount = 40;
	public GUIText guiTextPrefab;
	public Vector3 guiTextPosition = Vector3.zero;
	public Vector3 guiTextOffset = new Vector3(0.0f, 0.025f, 0.0f);
	public Vector3 guiTextScale = Vector3.one;
	public TextAnchor guiTextAnchor = TextAnchor.LowerLeft;
	public TextAlignment guiTextAlignment = TextAlignment.Left;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void Log(String pLogMessage)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.Log(pLogMessage);
			return;
		}
		
		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = pLogMessage;
		Debug.Log(mGUITexts[0].text);
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogFormat(String pLogMessage, params object[] args)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.Log(String.Format(pLogMessage, args));
			return;
		}

		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = String.Format(pLogMessage, args);
		Debug.Log(mGUITexts[0].text);
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogWarning(String pLogMessage)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.LogWarning(pLogMessage);
			return;
		}

		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = pLogMessage;
		Debug.LogWarning(mGUITexts[0].text);
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogWarningFormat(String pLogMessage, params object[] args)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.LogWarning(String.Format(pLogMessage, args));
			return;
		}

		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = String.Format(pLogMessage, args);
		Debug.LogWarning(mGUITexts[0].text);
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogError(String pLogMessage)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.LogError(pLogMessage);
			return;
		}

		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = pLogMessage;
		Debug.LogError(mGUITexts[0].text);
	}
	//-------------------------------------------------------------------------------------------------------
	// ILogger 메서드
	public void LogErrorFormat(String pLogMessage, params object[] args)
	{
		if (mGUITexts.Length <= 0)
		{
			Debug.LogError(String.Format(pLogMessage, args));
			return;
		}

		for (int iLine = (mGUITexts.Length - 1); iLine >= 1; --iLine)
			mGUITexts[iLine].text = mGUITexts[iLine - 1].text;
		mGUITexts[0].text = String.Format(pLogMessage, args);
		Debug.LogError(mGUITexts[0].text);
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (globalLogger)
			Logger.SetInstance(this);

		mGUITexts = new GUIText[scrollLineCount];

		for (int iLine = 0; iLine < mGUITexts.Length; ++iLine)
		{
			GameObject lGUITextObject = (GameObject)Instantiate(guiTextPrefab.gameObject);
			lGUITextObject.transform.parent = transform;
			
			GUIText lGUIText = lGUITextObject.GetComponent<GUIText>();
			lGUIText.transform.position = guiTextPosition + guiTextOffset * iLine;
			lGUIText.transform.localScale = guiTextScale;
			lGUIText.anchor = guiTextAnchor;
			lGUIText.alignment = guiTextAlignment;

			mGUITexts[iLine] = lGUIText;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private GUIText[] mGUITexts;
}
