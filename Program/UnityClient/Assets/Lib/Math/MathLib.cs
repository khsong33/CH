using UnityEngine;
using System.Collections;
using System;

public class MathLib
{
	public const float cPI = Mathf.PI;
	public const float cTwoPI = 2.0f * Mathf.PI;

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	public static float GetDirectionY(Quaternion pRotation)
	{
		return pRotation.eulerAngles.y;
	}
	public static float GetDirectionY(Coord2 pCoordVector)
	{
		float lDirectionY = Mathf.Atan2(pCoordVector.x, pCoordVector.z) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static float GetDirectionY(Vector2 pWorldVector2)
	{
		float lDirectionY = Mathf.Atan2(pWorldVector2.x, pWorldVector2.y) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static float GetDirectionY(Vector3 pWorldVector3)
	{
		float lDirectionY = Mathf.Atan2(pWorldVector3.x, pWorldVector3.z) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static Quaternion GetRotation(float pDirectionY)
	{
		return Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	public static Quaternion GetRotation(Coord2 pCoordVector)
	{
		return Quaternion.LookRotation(Coord2.ToWorld(pCoordVector));
	}
	public static Quaternion GetRotation(Vector2 pWorldVector2)
	{
		return Quaternion.LookRotation(new Vector3(-pWorldVector2.x, 0.0f, -pWorldVector2.y));
	}
	public static Quaternion GetRotation(Vector3 pWorldVector3)
	{
		return Quaternion.LookRotation(new Vector3(-pWorldVector3.x, 0.0f, -pWorldVector3.z));
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
