using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CountdownLabel : MonoBehaviour
{
	public delegate void OnCountdownEndDelegate();

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public bool vIsStartOnEnable = true;

	public UILabel vUILabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 타이머
	public float aTimer
	{
		get { return mTimer; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 카운트다운 종료 콜백
	public OnCountdownEndDelegate aOnCountdownEndDelegate
	{
		get { return mOnCountdownEndDelegate; }
		set { mOnCountdownEndDelegate = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vUILabel == null)
		{
			vUILabel = GetComponent<UILabel>();
			if (vUILabel == null)
				Debug.LogError("can't find vLabel - " + this);
		}

		mIsUpdating = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		//Debug.Log("OnDisable() - " + this);

		mIsUpdating = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 카운트다운을 시작합니다.
	public void StartCountdown(float pCountdownSec)
	{
		gameObject.SetActive(true);

		mTimer = pCountdownSec;
		if (!mIsUpdating)
			StartCoroutine(_UpdateTimer());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 타이머를 갱신합니다.
	private IEnumerator _UpdateTimer()
	{
		//Debug.Log("Update() - " + this);
		
		mIsUpdating = true;
		
		for (;;)
		{
			_UpdateLabel();
			yield return null;

			mTimer -= Time.deltaTime;
			if (mTimer <= 0.0f)
				break;
		}

		mTimer = 0.0f;
		_UpdateLabel();
			
		if (mOnCountdownEndDelegate != null)
			mOnCountdownEndDelegate();
			
		mIsUpdating = false;

		gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 레이블을 갱신합니다.
	private void _UpdateLabel()
	{
		int lMinutes = (int)(mTimer / 60.0f);
		int lSeconds = Mathf.CeilToInt(mTimer - lMinutes * 60.0f);
		//Debug.Log("_UpdateLabel() lMinutes=" + lMinutes + " lSeconds=" + lSeconds + " ==>" + String.Format("{0}:{1:00}", lMinutes, lSeconds));
		if (lMinutes > 0)
			vUILabel.text = String.Format("{0}:{1:00}", lMinutes, lSeconds);
		else
			vUILabel.text = lSeconds.ToString();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private float mTimer;
	private bool mIsUpdating;
	private OnCountdownEndDelegate mOnCountdownEndDelegate;
}
