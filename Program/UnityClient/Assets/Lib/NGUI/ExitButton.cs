using UnityEngine;
using System.Collections;
using System;

public class ExitButton : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public FadingObjectManager vFadingObjectManager;
	public KeyCode vButtonKeyCode1 = KeyCode.Backspace;
	public KeyCode vButtonKeyCode2 = KeyCode.Escape;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		if (Input.GetKeyUp(vButtonKeyCode1))
			StartCoroutine(FadeAndQuit());
		else if (Input.GetKeyUp(vButtonKeyCode2))
			StartCoroutine(FadeAndQuit());
	}
	//-------------------------------------------------------------------------------------------------------
	// 클릭 콜백
	void OnClick()
	{
		StartCoroutine(FadeAndQuit());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 페이딩 처리를 합니다.
	public IEnumerator FadeAndQuit()
	{
		if (vFadingObjectManager != null)
			yield return StartCoroutine(vFadingObjectManager.ScrollObjects(false));

		yield return null; // 종료 과정에서 화면 백 버퍼 스와핑이 보이는 것을 막기 위해 한 프레임 쉽니다.
		Debug.Log("ExitButton Application.Quit();");
		Application.Quit();
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
