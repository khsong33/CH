﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class FEBackground : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UITexture vBackground; 

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Awake()
	{
		if (vBackground == null)
			Debug.LogError("Not Exist Background Texture - " + gameObject.name);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작전 콜백
	void Start()
	{
		mRoot = NGUITools.FindInParents<UIRoot>(gameObject);

		float lScreenRatio = (float)mRoot.activeHeight / Screen.height;

		vBackground.width = Mathf.CeilToInt(Screen.width * lScreenRatio);
		vBackground.height = Mathf.CeilToInt(Screen.height * lScreenRatio);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIRoot mRoot;
}
