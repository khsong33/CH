using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode()]
public class FadingObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public bool vIsOpen = false;
	public Vector3 vOpenPosition;
	public Vector3 vClosedPosition;
	public float vOpenDelay = 1.0f;
	public float vCloseDelay = 0.0f;
	public float vScrollOpenDelay = 1.0f;
	public float vScrollCloseDelay = 0.25f;
	public float vCushionOpenDelay = 0.1f;
	public Vector3 vCushionOpenVector = new Vector3(0.0f, 2.0f, 0.0f);
	public float vCushionCloseDelay = 0.1f;
	public Vector3 vCushionCloseVector = new Vector3(0.0f, 1.0f, 0.0f);
	
	public GameObject[] vOpenLinkObjects;
	public GameObject[] vCloseLinkObjects;
	
	public UISprite[] vLinkSprites;
	public UITexture[] vLinkTextures;
	public Color vLinkImageOpenColor = new Color(1.0f, 1.0f, 1.0f, 0.0f);
	public Color vLinkObjectCloseColor = new Color(1.0f, 1.0f, 1.0f, 1.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public bool aIsScrolling
	{
		get { return mIsScrolling; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Awake()
	{
		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		// 초기 위치로 시작합니다.
		UpdateScrollPosition(vIsOpen);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백(에디터에서만)
#if UNITY_EDITOR
	void Update()
	{
		if (!mIsScrolling)
		{
			if (mUpdateIsOpen != vIsOpen)
			{
				mUpdateIsOpen = vIsOpen;

				// 인스펙터로 커튼 상태가 바뀌면 이에 맞춰 위치를 옮깁니다.
				UpdateScrollPosition(vIsOpen);
			}

			// 현재 커튼 위치를 인스펙터에 반영합니다.
			if (vIsOpen)
				vOpenPosition = mTransform.localPosition;
			else
				vClosedPosition = mTransform.localPosition;
		}
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 스크롤 위치를 갱신합니다.
	public void UpdateScrollPosition(bool pIsOpen)
	{
		//Debug.Log(String.Format("{0}.UpdateScrollPosition({1}) mUpdateIsOpen={2}", gameObject, pIsOpen, mUpdateIsOpen));

		if (pIsOpen)
		{
			mTransform.localPosition = vOpenPosition;
			_UpdateLinkState(1.0f);
		}
		else
		{
			mTransform.localPosition = vClosedPosition;
			_UpdateLinkState(0.0f);
		}

		vIsOpen = pIsOpen;
		mUpdateIsOpen = pIsOpen;
	}
	//-------------------------------------------------------------------------------------------------------
	// 커튼을 엽니다.
	public IEnumerator Open()
	{
		//Debug.Log(String.Format("{0}.Open() vIsOpen={1}", gameObject, vIsOpen));

		if (!vIsOpen)
		{
			mIsScrolling = true;
			yield return StartCoroutine(_ScrollObject(vOpenPosition, vOpenDelay, vScrollOpenDelay, vCushionOpenDelay, vCushionOpenVector, true));
			mIsScrolling = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 커튼을 닫습니다.
	public IEnumerator Close()
	{
		//Debug.Log(String.Format("{0}.Close() vIsOpen={1}", gameObject, vIsOpen)); 

		if (vIsOpen)
		{
			mIsScrolling = true;
			yield return StartCoroutine(_ScrollObject(vClosedPosition, vCloseDelay, vScrollCloseDelay, vCushionCloseDelay, vCushionCloseVector, false));
			mIsScrolling = false;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 커튼을 엽니다.
	IEnumerator _ScrollObject(Vector3 pScrollPosition, float pScrollStartDelay, float pScrollDelay, float pCushionDelay, Vector3 pCushionVector, bool pWantToOpen)
	{
		//Debug.Log("_ScrollObject() - " + gameObject);

		Transform lTransform = transform;
		Vector3 lStartPosition = lTransform.localPosition;
		
		// 시작 딜레이를 줍니다.
		if (pScrollStartDelay > 0.0f)
		{
			float lWaitTimer = 0.0f;
			float lWaitTime = Time.realtimeSinceStartup;
			while (lWaitTimer < pScrollStartDelay)
			{
				float lNewWaitTime = Time.realtimeSinceStartup;
				lWaitTimer += (lNewWaitTime - lWaitTime) * TimeUtil.aAccelTimeScale;
				lWaitTime = lNewWaitTime;
				
				yield return null;
			}
		}

		// 열기 스크롤이라면 스크롤 전에 쿠션을 줍니다.
		if (pWantToOpen &&
			(pCushionDelay > 0.0f))
			yield return StartCoroutine(_CushionObject(pCushionDelay, pCushionVector));

		// 사운드를 플레이합니다.
		FadingObjectSound lSound = gameObject.GetComponent<FadingObjectSound>();
		if (lSound != null)
		{
			if (pWantToOpen)
				lSound.PlayOpenSound();
			else
				lSound.PlayCloseSound();
		}

		// 스크롤 시킵니다.
		float lScrollTimer = 0.0f;
		float lRealTime = Time.realtimeSinceStartup;
		while (lScrollTimer < pScrollDelay)
		{
			float lLerpFactor = lScrollTimer / pScrollDelay;
			lTransform.localPosition = Vector3.Lerp(lStartPosition, pScrollPosition, lLerpFactor);
			_UpdateLinkState(pWantToOpen ? lLerpFactor : (1.0f - lLerpFactor));
			//Debug.Log("Scroll lTransform.localPosition=" + lTransform.localPosition);
			//Debug.Log("lScrollTimer=" + lScrollTimer);

			float lNewRealTime = Time.realtimeSinceStartup;			
			lScrollTimer += (lNewRealTime - lRealTime) * TimeUtil.aAccelTimeScale;
			lRealTime = lNewRealTime;
			
			yield return null;
		}
		lTransform.localPosition = pScrollPosition;
		_UpdateLinkState(pWantToOpen ? 1.0f : 0.0f);

		// 닫기 스크롤이라면 스크롤 다음에 쿠션을 줍니다.
		if (!pWantToOpen &&
			(pCushionDelay > 0.0f))
			yield return StartCoroutine(_CushionObject(pCushionDelay, pCushionVector));

		// 스크롤 상태를 바꿉니다.
		vIsOpen = pWantToOpen;
		mUpdateIsOpen = pWantToOpen; // 갱신 상태도 같이 바꿉니다.

		//Debug.Log("_ScrollObject() ends - " + gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 반동을 줍니다.
	IEnumerator _CushionObject(float pCushionDelay, Vector3 pCushionVector)
	{
		Transform lTransform = transform;
		Vector3 lStartPosition = lTransform.localPosition;

		float lCushionTimer = 0.0f;
		float lRealTime = Time.realtimeSinceStartup;
		while (lCushionTimer < pCushionDelay)
		{
			float lCushionFactorX = (1.0f - lCushionTimer / pCushionDelay) * 2.0f;
			float lCushionFactorXMinusOne = lCushionFactorX - 1.0f;
			float lCushionScale = 1.0f - lCushionFactorXMinusOne * lCushionFactorXMinusOne;
			lTransform.localPosition = lStartPosition + pCushionVector * lCushionScale;
			//Debug.Log("lCushionScale=" + lCushionScale);

			float lNewRealTime = Time.realtimeSinceStartup;
			lCushionTimer += (lNewRealTime - lRealTime) * TimeUtil.aAccelTimeScale;
			lRealTime = lNewRealTime;

			yield return null;
		}
		lTransform.localPosition = lStartPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// 링크된 개체들을 갱신합니다.
	private void _UpdateLinkState(float pOpenLerpFactor)
	{
		// 링크된 오브젝트의 활성화를 갱신합니다.
		bool lIsOpenObjectsActive = pOpenLerpFactor >= 1.0f;
		bool lIsCloseObjectsActive = pOpenLerpFactor <= 0.0f;

		for (int iObject = 0; iObject < vOpenLinkObjects.Length; ++iObject)
			if (vOpenLinkObjects[iObject].activeInHierarchy != lIsOpenObjectsActive)
				vOpenLinkObjects[iObject].SetActive(lIsOpenObjectsActive);
		for (int iObject = 0; iObject < vCloseLinkObjects.Length; ++iObject)
			if (vCloseLinkObjects[iObject].activeInHierarchy != lIsCloseObjectsActive)
				vCloseLinkObjects[iObject].SetActive(lIsCloseObjectsActive);

		// 링크된 이미지들의 색을 갱신합니다.
		Color lLerpColor = Color.Lerp(vLinkObjectCloseColor, vLinkImageOpenColor, pOpenLerpFactor);

		if (lLerpColor.a > 0.0f)
		{
			for (int iSprite = 0; iSprite < vLinkSprites.Length; ++iSprite)
				if (vLinkSprites[iSprite] != null)
				{
					if (!vLinkSprites[iSprite].gameObject.activeInHierarchy)
						vLinkSprites[iSprite].gameObject.SetActive(true);
					vLinkSprites[iSprite].color = lLerpColor;
				}
			for (int iTexture = 0; iTexture < vLinkTextures.Length; ++iTexture)
				if (vLinkTextures[iTexture] != null)
				{
					if (!vLinkTextures[iTexture].gameObject.activeInHierarchy)
						vLinkTextures[iTexture].gameObject.SetActive(true);
					vLinkTextures[iTexture].color = lLerpColor;
				}
		}
		else
		{
			for (int iSprite = 0; iSprite < vLinkSprites.Length; ++iSprite)
				if (vLinkSprites[iSprite] != null)
					vLinkSprites[iSprite].gameObject.SetActive(false);
			for (int iTexture = 0; iTexture < vLinkTextures.Length; ++iTexture)
				if (vLinkTextures[iTexture] != null)
					vLinkTextures[iTexture].gameObject.SetActive(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private bool mUpdateIsOpen;
//	private Vector3 mClosedPosition;
	private bool mIsScrolling = false;
}
