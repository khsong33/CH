using UnityEngine;
using System.Collections;
using System;

public class FadingObjectManager : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public FadingObject[] vFadingObjects;
	public bool vOpenAtStart = false;
	public bool vScrollAtStart = false;
	public bool vDoNotScrollAtApplicationStart = false;
	public bool vInactivateWhenOpen = true;
	public bool vInactivateWhenClosed = false;
	
	public AudioClip vOpenSound;
	public AudioClip vCloseSound;

	public AudioSource vSoundSource;

	public Collider vCollider;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public bool aIsOpen
	{
		get { return mIsOpen; }
	}
	public bool aIsScrolling
	{
		get { return mIsScrolling; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vCollider == null)
			Debug.LogError("vCollider is empty - " + this);
	
		bool lScroll = vScrollAtStart;
		if (vScrollAtStart &&
			vDoNotScrollAtApplicationStart)
		{
			//Debug.Log("First scroll ignored");
			lScroll = false;
		}

		if (lScroll)
		{
			mIsOpen = !vOpenAtStart;
			for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
				vFadingObjects[iObject].UpdateScrollPosition(!vOpenAtStart);
			StartCoroutine(ScrollObjects(vOpenAtStart));
		}
		else
		{
			mIsOpen = vOpenAtStart;
			for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
				vFadingObjects[iObject].UpdateScrollPosition(vOpenAtStart);
			_UpdateCurtainActivity(vOpenAtStart);

			// 열려 있는 상태에 따라 충돌체를 활성화합니다.
			vCollider.gameObject.SetActive(!mIsOpen);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 페이딩 오브젝트를 스크롤시킵니다.
	public IEnumerator ScrollObjects(bool pWantToOpen)
	{
		//Debug.Log("pWantToOpen=" + pWantToOpen);
		//Debug.Log("mIsOpen=" + mIsOpen);
		
		if (mIsOpen != pWantToOpen)
		{
			// 스크롤을 시작합니다.
			mIsScrolling = true;

			// 스크롤 되는 동안에는 충돌체를 활성화합니다.
			vCollider.gameObject.SetActive(true);
			
			// 닫기 시작할 것이라면 열림 플래그를 미리 끕니다.
			if (!pWantToOpen)
				mIsOpen = false;

			// 커튼 스크롤을 합니다.
			if (pWantToOpen)
			{
				for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
				{
					//Debug.Log("pWantToOpen vFadingObjects[iObject].gameObject=" + vFadingObjects[iObject].gameObject);
					vFadingObjects[iObject].gameObject.SetActive(true);
					yield return null; // Start()에서 호출된 경우를 생각해서 vFadingObjects의 Start()도 호출되도록 한 프레임을 쉽니다.
					StartCoroutine(vFadingObjects[iObject].Open());
				}
			}
			else
			{
				for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
				{
					//Debug.Log("!pWantToOpen vFadingObjects[iObject].gameObject=" + vFadingObjects[iObject].gameObject);
					vFadingObjects[iObject].gameObject.SetActive(true);
					yield return null; // Start()에서 호출된 경우를 생각해서 vFadingObjects의 Start()도 호출되도록 한 프레임을 쉽니다.
					StartCoroutine(vFadingObjects[iObject].Close());
				}
			}

			// 스크롤이 끝났습니다.
			mIsOpen = pWantToOpen;

			// 커튼 스크롤이 모두 끝나기를 기다립니다.
			//Debug.Log("vFadingObjects.Length=" + vFadingObjects.Length);
			while (vFadingObjects.Length > 0)
			{
				yield return null;

				bool lIsScrolling = false;
				for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
				{
					//Debug.Log("if (vFadingObjects[iObject].aIsScrolling) - " + vFadingObjects[iObject].aIsScrolling);
					if (vFadingObjects[iObject].aIsScrolling)
					{
						lIsScrolling = true;
						//Debug.Log("curtainObject is scrolling - " + vFadingObjects[iObject]);
						break;
					}
				}

				//Debug.Log("if (lIsScrolling) - " + lIsScrolling);
				if (!lIsScrolling)
                {
					break;
                }
			}

			// 스크롤이 끝나면 자동으로 비활성화 되는 처리를 합니다.
			_UpdateCurtainActivity(pWantToOpen);

			mIsScrolling = false;
            //Debug.Log("End Fading");
			//Debug.Log("mIsScrolling = false;");

			// 스크롤이 끝나면 스크롤 결과에 따라 충돌체를 활성화합니다.
			vCollider.gameObject.SetActive(!mIsOpen);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 커튼 활성 여부를 갱신합니다.
	private void _UpdateCurtainActivity(bool pIsOpen)
	{
		if (pIsOpen)
		{
			if (vInactivateWhenOpen)
			{
				for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
					vFadingObjects[iObject].gameObject.SetActive(false);
			}
		}
		else
		{
			if (vInactivateWhenClosed)
			{
				for (int iObject = 0; iObject < vFadingObjects.Length; ++iObject)
					vFadingObjects[iObject].gameObject.SetActive(false);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 사운드를 플레이합니다.
	public void PlaySound(bool isOpen)
	{
		if (isOpen)
			vSoundSource.PlayOneShot(vOpenSound);
		else
			vSoundSource.PlayOneShot(vCloseSound);
	}
	//-------------------------------------------------------------------------------------------------------
	// 볼륨을 조절합니다.
	public void SetVoulmn(float pVolumn)
	{
		vSoundSource.volume = pVolumn;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsOpen = false;
	private bool mIsScrolling = false;
}
