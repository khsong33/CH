using UnityEngine;
using System.Collections;
using System;

public class FlashWidget : MonoBehaviour
{
	[Serializable]
	public class FlashItem
	{
		public UIWidget vWidget;
		public Color vFlashColor = ColorUtil.Color4ub(255, 255, 255, 0);

		internal Color mWidgetBaseColor;
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public FlashItem[] vFlashItems;
	public float vFlashSpeed = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		for (int iItem = 0; iItem < vFlashItems.Length; ++iItem)
		{
			FlashItem lFlashItem = vFlashItems[iItem];
			if (lFlashItem.vWidget == null)
				continue;

			lFlashItem.mWidgetBaseColor = lFlashItem.vWidget.color;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		float lFlashFactor = (Time.timeSinceLevelLoad - (int)Time.timeSinceLevelLoad);
		float lColorLerpFactor = (Mathf.Sin(lFlashFactor * Mathf.PI * 2.0f * vFlashSpeed) + 1.0f) * 0.5f;

		for (int iItem = 0; iItem < vFlashItems.Length; ++iItem)
		{
			FlashItem lFlashItem = vFlashItems[iItem];
			if (lFlashItem.vWidget == null)
				continue;

			lFlashItem.vWidget.color = Color.Lerp(lFlashItem.mWidgetBaseColor, lFlashItem.vFlashColor, lColorLerpFactor);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Color mFlashWidgetColor;
}
