using UnityEngine;
using System.Collections;
using System;

public class FocusFlash : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public UISprite vFlashSprite;
	public float vFlashDelay = 0.25f;

	public UISprite vFocusSprite;
	public float vFocusDelay = 2.0f;
	public float vFocusSpriteEndRotation = 360.0f;
	public float vFocusSpriteEndScaleFactor = 2.0f;
	
	public Vector3 vPopUpScale = new Vector3(0.5f, 0.5f, 0.5f);
	public float vPopUpDelay = 0.3f;
	public bool vIsPopUpAtStart = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mIsStarted = true;
	
		if (vFlashSprite == null)
			Debug.LogError("vFlashSprite is empty - " + this);
		if (vFocusSprite == null)
			Debug.LogError("vFocusSprite is empty - " + this);
	
		mFlashSpriteColor = vFlashSprite.color;

		mFocusSpriteTransform = vFocusSprite.transform;
		mFocusSpriteLocalScale = mFocusSpriteTransform.localScale;
		mFocusSpriteColor = vFocusSprite.color;

		if (vIsPopUpAtStart)
			StartCoroutine(_PopUp());
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (mIsStarted)
		{
			if (vIsPopUpAtStart)
				StartCoroutine(_PopUp());
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 팝업 애니메이션을 합니다.
	public void PopUp()
	{
		if (!gameObject.activeInHierarchy)
		{
			gameObject.SetActive(true);
			if (vIsPopUpAtStart)
				return; // OnEnable()에서 팝업을 처리합니다.
		}

		StartCoroutine(_PopUp());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 띄웁니다.
	private IEnumerator _PopUp()
	{
		//Debug.Log("_PopUp()");

		if (!mIsStarted)
			yield return null; // Start()가 호출되도록 한 프레임 쉽니다.
	
		if (mIsFlashPopUp || mIsFocusPopUp)
		{
			mIsFlashPopUp = false;
			mIsFocusPopUp = false;
			yield return null; // 기존 코루틴이 자동으로 종료하도록 한 프레임 기다립니다.
		}

		StartCoroutine(_PopUpFlashSprite());
		StartCoroutine(_PopUpFocusSprite());
		
		while (mIsFlashPopUp || mIsFocusPopUp)
			yield return null;
			
		gameObject.SetActive(false); // 코루틴이 종료하면 비활성화 시킵니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 플래시 스프라이트를 띄웁니다.
	private IEnumerator _PopUpFlashSprite()
	{
		//Debug.Log("_PopUpFlashSprite() mIsFlashPopUp=" + mIsFlashPopUp);
		
		mIsFlashPopUp = true;

		float lFlashTimer = Time.deltaTime;
		while (lFlashTimer < vFlashDelay)
		{
			float lFlashLerpFactor = lFlashTimer / vFlashDelay;

			float lFlashAlpha = -lFlashLerpFactor * lFlashLerpFactor + 1.0f;
			vFlashSprite.color = new Color(
				mFlashSpriteColor.r,
				mFlashSpriteColor.g,
				mFlashSpriteColor.b,
				lFlashAlpha);
				
			if (!mIsFlashPopUp)
				yield break;

			lFlashTimer += Time.deltaTime;
			yield return null;
		}
		vFlashSprite.color = new Color(
			mFlashSpriteColor.r,
			mFlashSpriteColor.g,
			mFlashSpriteColor.b,
			0.0f);

		mIsFlashPopUp = false;

		//Debug.Log("_PopUpFlashSprite() mIsFlashPopUp=" + mIsFlashPopUp);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 포커스 스프라이트를 띄웁니다.
	private IEnumerator _PopUpFocusSprite()
	{
		//Debug.Log("_PopUpFocusSprite() mIsFlashPopUp=" + mIsFocusPopUp);

		mIsFocusPopUp = true;
		
		mFocusSpriteTransform.rotation = Quaternion.identity;
		
		Vector3 lFocusSpriteEndScale = mFocusSpriteLocalScale * vFocusSpriteEndScaleFactor;
	
		float lFocusTimer = Time.deltaTime;
		while (lFocusTimer < vFocusDelay)
		{
			float lFocusLerpFactor = lFocusTimer / vFocusDelay;

			vFocusSprite.color = new Color(
				mFocusSpriteColor.r,
				mFocusSpriteColor.g,
				mFocusSpriteColor.b,
				Mathf.Lerp(mFocusSpriteColor.a, 0.0f, lFocusLerpFactor));

			mFocusSpriteTransform.rotation = Quaternion.Euler(
				0.0f,
				0.0f,
				Mathf.Lerp(0.0f, vFocusSpriteEndRotation, lFocusLerpFactor));

			mFocusSpriteTransform.localScale = Vector3.Lerp(
				mFocusSpriteLocalScale,
				lFocusSpriteEndScale,
				lFocusLerpFactor);

			if (!mIsFocusPopUp)
				yield break;

			lFocusTimer += Time.deltaTime;
			yield return null;
		}
		vFocusSprite.color = new Color(
			mFocusSpriteColor.r,
			mFocusSpriteColor.g,
			mFocusSpriteColor.b,
			0.0f);
		mFocusSpriteTransform.rotation = Quaternion.Euler(
			0.0f,
			0.0f,
			vFocusSpriteEndRotation);
		mFocusSpriteTransform.localScale = lFocusSpriteEndScale;

		mIsFocusPopUp = false;

		//Debug.Log("_PopUpFocusSprite() mIsFlashPopUp=" + mIsFocusPopUp);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Color mFlashSpriteColor;
	private bool mIsFlashPopUp;

	private Transform mFocusSpriteTransform;
	private Vector3 mFocusSpriteLocalScale;
	private Color mFocusSpriteColor;
	private bool mIsFocusPopUp;

	private bool mIsStarted;
}
