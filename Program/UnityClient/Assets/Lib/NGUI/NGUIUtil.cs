using UnityEngine;
using System.Collections;
using System;

public class NGUIUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 스크린 좌표에 해당하는 UI 좌표를 얻습니다.
	public static Vector3 ScreenToUIPoint(UIRoot pUIRoot, Vector3 pScreenPoint)
	{
		//Debug.Log("Screen.width=" + Screen.width + " Screen.height=" + Screen.height);
	
		Vector3 lUIPoint = pScreenPoint;
		lUIPoint.x -= Screen.width * 0.5f;
		lUIPoint.y -= Screen.height * 0.5f;
		float lUIPixelScale = (float)pUIRoot.activeHeight / Screen.height;
		lUIPoint.x *= lUIPixelScale;
		lUIPoint.y *= lUIPixelScale;

		return lUIPoint;
	}
	//-------------------------------------------------------------------------------------------------------
	// 월드 좌표에 해당하는 UI 좌표를 얻습니다.
	public static Vector3 WorldToUIPoint(UIRoot pUIRoot, Camera pCamera, Vector3 pWorldPosition)
	{
		//Debug.Log("pCamera.pixelRect" + pCamera.pixelRect);
	
		return ScreenToUIPoint(
			pUIRoot,
			pCamera.WorldToScreenPoint(pWorldPosition));
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 콤포넌트를 가진 프리팹으로 위젯 오브젝트를 생성한 다음에 위젯을 찾아 반환합니다.
	public static T_Widget InstantiateWidget<T_Widget>(GameObject pParentObject, T_Widget pWidgetPrefab) where T_Widget : UIWidget
	{
		GameObject lWidgetObject = NGUITools.AddChild(pParentObject, pWidgetPrefab.gameObject);
		T_Widget lWidget = lWidgetObject.GetComponent<T_Widget>();

		return lWidget;
	}
	//-------------------------------------------------------------------------------------------------------
	// UIWidget의 부모를 바꿉니다.
	public static void SetParent(Transform lUIWidgetTransform, Transform pParent)
	{
		Vector3 lWidgetScale = lUIWidgetTransform.localScale;
		lUIWidgetTransform.parent = pParent;
		lUIWidgetTransform.localScale = lWidgetScale;
	}
	//-------------------------------------------------------------------------------------------------------
	// UIWidget의 부모를 바꿉니다.
	public static void SetAnchorTarget(UIWidget pUIWidget, Transform pTarget)
	{
		pUIWidget.leftAnchor.target = pTarget;
		pUIWidget.rightAnchor.target = pTarget;
		pUIWidget.bottomAnchor.target = pTarget;
		pUIWidget.topAnchor.target = pTarget;
		pUIWidget.ResetAnchors();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 부모 변환에 대한 로컬 좌표를 얻습니다(NGUI 용으로 회전은 고려하지 않음).
	public static Vector3 GetLocalPositionFromParent(Transform pChildTransform, Transform pParentTransform)
	{
		Transform lLocalTransform = pChildTransform.transform;
		Transform lAnchorTransform = pParentTransform.transform;
		Vector3 lLocalPosition = lLocalTransform.localPosition;
		while (lLocalTransform != lAnchorTransform)
		{
			lLocalTransform = lLocalTransform.parent;
			if (lLocalTransform == null)
			{
				Debug.LogError("can't find pParentTransform:" + pParentTransform + " from pChildTransform:" + pChildTransform);
				return lLocalPosition;
			}

			lLocalPosition += lLocalTransform.localPosition;
		}
		return lLocalPosition;
	}
}
