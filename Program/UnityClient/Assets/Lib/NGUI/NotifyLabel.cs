using UnityEngine;
using System.Collections;
using System;

public class NotifyLabel : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vLabel;

	public Transform vNorityTransform;
	public float vNotifyScale = 1.5f;
	public float vNotifyDelay = 0.2f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vLabel == null)
			Debug.LogError("vLabel is empty - " + this);

		if (vNorityTransform == null)
			Debug.LogError("vNorityTransform is empty - " + this);

		mNotifyBaseScale = vNorityTransform.localScale;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public void UpdateLabel(String pString, bool pIsNotify)
	{
		vLabel.text = pString;

		if (pIsNotify)
		{
			if (!mIsNotifying &&
				gameObject.activeInHierarchy)
				StartCoroutine(_Notify());
			else
				mNotifyingTimer = 0.0f;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 카운트 변화를 알립니다.
	private IEnumerator _Notify()
	{
		mIsNotifying = true;

		mNotifyingTimer = Time.deltaTime;
		while (mNotifyingTimer < vNotifyDelay)
		{
			float lLerpFactor = mNotifyingTimer / vNotifyDelay;
			Vector3 lNotifyScale = mNotifyBaseScale * Mathf.Lerp(vNotifyScale, 1.0f, lLerpFactor);
			vNorityTransform.localScale = lNotifyScale;

			mNotifyingTimer += Time.deltaTime;
			yield return null;
		}

		vNorityTransform.localScale = mNotifyBaseScale;

		mIsNotifying = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsNotifying;
	private float mNotifyingTimer;
	private Vector3 mNotifyBaseScale;
}
