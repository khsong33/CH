using UnityEngine;
using System.Collections;
using System;

public class PopOpaqueObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public UIPanel vPopOpaquePanel;
	
	public Vector3 vPopOpaqueScale = new Vector3(3.0f, 3.0f, 3.0f);
	public float vPopOpaqueDelay = 0.3f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vPopOpaquePanel == null)
			Debug.LogError("vPopOpaquePanel is empty - " + gameObject);
	
		mTransform = transform;
		mObjectLocalScale = mTransform.localScale;

		StartCoroutine(_PopOpaque());
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (mIsStarted)
			StartCoroutine(_PopOpaque());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 등장 애니메이션을 합니다.
	private IEnumerator _PopOpaque()
	{
		Vector3 lStartScale = new Vector3(
			mObjectLocalScale.x * vPopOpaqueScale.x,
			mObjectLocalScale.y * vPopOpaqueScale.y,
			mObjectLocalScale.z * vPopOpaqueScale.z);

		mPopOpaqueTimer = Time.deltaTime;
	
		while (mPopOpaqueTimer < vPopOpaqueDelay)
		{
			float lLerpFactor = mPopOpaqueTimer / vPopOpaqueDelay;

			mTransform.localScale = Vector3.Lerp(lStartScale, mObjectLocalScale, lLerpFactor);			
			vPopOpaquePanel.alpha = Mathf.Lerp(0.0f, 1.0f, lLerpFactor);

			mPopOpaqueTimer += RealTime.deltaTime;
			yield return null;
		}
		
		mTransform.localScale = mObjectLocalScale;
		vPopOpaquePanel.alpha = 1.0f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mObjectLocalScale;
	private float mPopOpaqueTimer;
	private bool mIsStarted;
}
