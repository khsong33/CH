using UnityEngine;
using System.Collections;
using System;

public class PopUpObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Vector3 vMinScale = new Vector3(0.05f, 0.05f, 0.05f);
	public Vector3 vPopUpScale = new Vector3(0.4f, 0.4f, 0.4f);
	public float vPopUpDelay = 0.3f;
	public bool vIsPopUpAtStart = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mIsStarted = true;
	
		mTransform = transform;
		mObjectLocalScale = mTransform.localScale;

		if (vIsPopUpAtStart)
			StartCoroutine(_PopUp());
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (mIsStarted)
		{
			if (vIsPopUpAtStart)
				StartCoroutine(_PopUp());
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 팝업 애니메이션을 합니다.
	public void PopUp()
	{
		StartCoroutine(_PopUp());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 팝업 애니메이션을 합니다.
	private IEnumerator _PopUp()
	{
		//Debug.Log("vPopUpScale=" + vPopUpScale);
	
		mPopUpTimer = 0.0f;
	
		while (mPopUpTimer < vPopUpDelay)
		{
			float lLerpFactor = mPopUpTimer / vPopUpDelay;
			float lXFactor = 2.0f * lLerpFactor - 1.0f;
			float lPopUpFactor = -lXFactor * lXFactor + 1.0f;

			mTransform.localScale = new Vector3(
				Mathf.Max(mObjectLocalScale.x * (lLerpFactor + vPopUpScale.x * lPopUpFactor), vMinScale.x),
				Mathf.Max(mObjectLocalScale.y * (lLerpFactor + vPopUpScale.y * lPopUpFactor), vMinScale.y),
				Mathf.Max(mObjectLocalScale.z * (lLerpFactor + vPopUpScale.z * lPopUpFactor), vMinScale.z));

			mPopUpTimer += RealTime.deltaTime;
			yield return null;
		}
		
		mTransform.localScale = mObjectLocalScale;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mObjectLocalScale;
	private float mPopUpTimer;
	private bool mIsStarted;
}
