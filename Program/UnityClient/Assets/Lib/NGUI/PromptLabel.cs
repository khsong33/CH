using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PromptLabel : MonoBehaviour
{
	private class PromptTextInfo
	{
		public String mText;
		public float mShowTimer;

		public LinkedListNode<PromptTextInfo> mNode;

		public PromptTextInfo()
		{
			mNode = new LinkedListNode<PromptTextInfo>(this);
		}

		public void Set(String pText, float pShowTime)
		{
			mText = pText;
			mShowTimer = pShowTime;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UILabel vUILabel;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vUILabel == null)
		{
			vUILabel = GetComponent<UILabel>();
			if (vUILabel == null)
				Debug.LogError("can't find vLabel - " + this);
		}

		mActiveTextInfoList = new LinkedList<PromptTextInfo>();
		mFactoryTextInfoList = new LinkedList<PromptTextInfo>();

		mIsUpdating = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);
	
		if (!mIsUpdating)
			gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		//Debug.Log("OnDisable() - " + this);

		mIsUpdating = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 메시지를 보입니다.
	public void PromptMessage(MonoBehaviour pPromptComponent, String pPromptMessage, float pPromptTime)
	{
		LinkedListNode<PromptTextInfo> lTextInfoNode;
		if (mFactoryTextInfoList.First != null)
		{
			lTextInfoNode = mFactoryTextInfoList.First;
			mFactoryTextInfoList.RemoveFirst();
		}
		else
		{
			lTextInfoNode = (new PromptTextInfo()).mNode;
		}

		lTextInfoNode.Value.Set(pPromptMessage, pPromptTime);
		mActiveTextInfoList.AddLast(lTextInfoNode);

		if (!mIsUpdating &&
			pPromptComponent.gameObject.activeInHierarchy)
		{
			mIsUpdating = true;
			pPromptComponent.StartCoroutine(_UpdateTimer());
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 타이머를 갱신합니다.
	private IEnumerator _UpdateTimer()
	{
		//Debug.Log("_UpdateTimer() - " + this);

		for (;;)
		{
			String lShowText = String.Empty;

			LinkedListNode<PromptTextInfo> lTextInfoNode = mActiveTextInfoList.First;
			while (lTextInfoNode != null)
			{
				PromptTextInfo lTextInfo = lTextInfoNode.Value;
				lTextInfoNode = lTextInfoNode.Next;

				lTextInfo.mShowTimer -= Time.deltaTime;
				if (lTextInfo.mShowTimer < 0)
				{
					mActiveTextInfoList.Remove(lTextInfo.mNode);
					mFactoryTextInfoList.AddFirst(lTextInfo.mNode);
				}
				else
				{
					if (lShowText != String.Empty)
						lShowText += "\n";
					lShowText += lTextInfo.mText;
				}
			}

			vUILabel.text = lShowText;
			if (lShowText == String.Empty)
				break;

			yield return null;
		}

		mIsUpdating = false;
		
		gameObject.SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private LinkedList<PromptTextInfo> mActiveTextInfoList;
	private LinkedList<PromptTextInfo> mFactoryTextInfoList;
	private bool mIsUpdating;
}
