using UnityEngine;
using System.Collections;

public class QuitButton : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnClick()
	{
		//Debug.Log("Quit Application");
// #if UNITY_EDITOR
// 		if(GameControl.aGameServer.aIsOnline)
// 			NetworkManager.instance.Disconnect();
// 		GameControl.LoadLogin();
// #endif
		Debug.Log("QuitButton Application.Quit();");
		Application.Quit();
	}
}
