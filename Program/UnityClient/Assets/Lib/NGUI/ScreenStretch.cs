using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode()]
public class ScreenStretch : MonoBehaviour
{
	public enum Side
	{
		None,
		Horizontal,
		Vertical,
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Vector3 vStandardScale = Vector3.one;
	public Vector2 vStandardScreenRatio = new Vector2(10.0f, 16.0f);
	public Side vSide = Side.None;
	public bool vIsUpdateScaleInPlaying = false;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransfrom = transform;

		UpdateScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백(에디터에서만)
#if UNITY_EDITOR
	void Update()
	{
		if (!Application.isPlaying ||
			vIsUpdateScaleInPlaying)
			UpdateScale();
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public void UpdateScale()
	{
		//Debug.Log("UpdateScale()");

		Vector3 lLocalScale = vStandardScale;
		float lBaseAspectRatio = vStandardScreenRatio.x / vStandardScreenRatio.y;
		float lCurrentAspectRatio = (float)Screen.width / Screen.height;
		float lScale = lCurrentAspectRatio / lBaseAspectRatio;

		if (vSide == Side.Horizontal)
		{
			lLocalScale.x *= lScale;
			lLocalScale.y *= lScale;
		}
		else if (vSide == Side.Vertical)
		{
			lLocalScale.x /= lScale;
			lLocalScale.y /= lScale;
		}
		mTransfrom.localScale = lLocalScale;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	Transform mTransfrom;
}
