using UnityEngine;
using System.Collections;
using System;

public class ScrollMaterialObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Renderer vScrollRenderer;
	public Texture vScrollTexture;
	public Vector2 vScrollSpeed = new Vector2(1.0f, 1.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스크롤 재질
	public Material aScrollMaterial
	{
		get { return mScrollMaterial; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vScrollRenderer == null)
			vScrollRenderer = gameObject.GetComponent<Renderer>();

		if (vScrollRenderer == null)
			Debug.LogError("vScrollRenderer is empty - " + this);

		vScrollRenderer.material = new Material(vScrollRenderer.material);
		vScrollRenderer.material.mainTexture = vScrollTexture;
		mScrollMaterial = vScrollRenderer.material;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
// 		mScrollMaterial.mainTextureOffset = new Vector2(
// 			mScrollMaterial.mainTextureOffset.x + vScrollSpeed.x * Time.deltaTime,
// 			mScrollMaterial.mainTextureOffset.y + vScrollSpeed.y * Time.deltaTime);
		mScrollMaterial.mainTextureOffset = new Vector2(
			mScrollMaterial.mainTextureOffset.x + vScrollSpeed.x * RealTime.deltaTime * TimeUtil.aNormalTimeScale,
			mScrollMaterial.mainTextureOffset.y + vScrollSpeed.y * RealTime.deltaTime * TimeUtil.aNormalTimeScale);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Material mScrollMaterial;
}
