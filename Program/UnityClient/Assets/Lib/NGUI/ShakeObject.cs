using UnityEngine;
using System.Collections;
using System;

public class ShakeObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public float vShakeDuration = 1.5f;
	public float vShakeDelay = 0.5f;
	public Vector3 vShakeScale = new Vector3(2.5f, 2.5f, 0.0f);
	public bool vIsLooping = true;
	public float vShakeFactor = 1.5f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransform = transform;
		mShakePosition = mTransform.localPosition;
		mIsShaking = false;
		vIsLooping = true;
		mShakeTimer = 0.0f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 흔듭니다.
	public void StartShake()
	{
		//Debug.Log("Shake() - " + gameObject);
	
		if (!mIsShaking)
			StartCoroutine(_Shake());
		else
			mShakeTimer = vShakeDuration; // 이미 흔들고 있다면 타이머만 초기화합니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 정지합니다.
	public void EndShake()
	{
		mIsShaking = false;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 흔듭니다.
	private IEnumerator _Shake()
	{
		mIsShaking = true;
		mShakeTimer = vShakeDuration;

		while (mIsShaking)
		{
			Vector3 lShakeVector
				= Quaternion.Euler(0.0f, 0.0f, UnityEngine.Random.Range(0.0f, 360.0f))
				* (vShakeScale * vShakeFactor);
			Vector3 lTargetPosition = mShakePosition + lShakeVector;
			Vector3 velocity = Vector3.zero;

			mTransform.localPosition = Vector3.SmoothDamp(mTransform.localPosition, lTargetPosition, ref velocity, vShakeDelay);
			if (!vIsLooping)
			{
				mShakeTimer -= Time.deltaTime;
				if (mShakeTimer <= 0)
				{
					mIsShaking = false;
				}
			}
			yield return new WaitForSeconds(vShakeDelay);
		}
		mTransform.localPosition = Vector3.one;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mShakePosition;
	private bool mIsShaking;
	private float mShakeTimer;
}
