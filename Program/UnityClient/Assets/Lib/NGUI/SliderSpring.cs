using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SliderSpring : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public UISlider vUISlider;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	public float vDefaultValue;
	public float vSpringDelay = 0.333f;

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vUISlider == null)
		{
			vUISlider = GetComponent<UISlider>();
			if (vUISlider == null)
				Debug.LogError("can't find UISlider - " + this);
		}

		vDefaultValue = vUISlider.value;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnPress(bool pIsDown)
	{
		//Debug.Log("OnPress() - " + this);

		if (!pIsDown &&
			enabled)
			StartCoroutine(_UpdateSpring());
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 스프링을 갱신합니다.
	private IEnumerator _UpdateSpring()
	{
		//Debug.Log("Update() - " + this);
		
		float lSpringTimer = Time.deltaTime;
		float lStartValue = vUISlider.value;

		while (lSpringTimer < vSpringDelay)
		{
			float lXFactor = lSpringTimer / vSpringDelay;
			float lXMinusOne = lXFactor - 1.0f;
			float lYFactor = -lXMinusOne * lXMinusOne + 1.0f;

			vUISlider.value = Mathf.Lerp(lStartValue, vDefaultValue, lYFactor);

			lSpringTimer += Time.deltaTime;
			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
