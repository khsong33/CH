using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SplashLabel : MonoBehaviour
{
	public const float cStartTimeScale = 1.0f;
	public const float cEndTimeScale = 0.2f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SplashLabelPool vSplashLabelPool;

	public UILabel vDisplayLabel;

	public UISprite[] vMarkSprites;
	public Vector3 vMarkOffset = Vector3.zero;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 변환
	public Transform aTransform
	{
		get { return mTransform; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 마크 인덱스
	public int aMarkIdx
	{
		get { return mMarkIdx; }
		set
		{
			if (mMarkIdx != value)
			{
				if (mMarkIdx >= 0)
					vMarkSprites[mMarkIdx].gameObject.SetActive(false);
				mMarkIdx = value;
				if (mMarkIdx >= 0)
					vMarkSprites[mMarkIdx].gameObject.SetActive(true);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vDisplayLabel == null)
			Debug.LogError("vDisplayLabel is empty - " + this);
		if (vMarkSprites.Length <= 0)
			Debug.LogError("vMarkSprite is empty - " + this);

		vSplashLabelPool = null;

		mTransform = transform;

		mMarkIdx = -1;
		for (int iTransform = 0; iTransform < vMarkSprites.Length; ++iTransform)
			vMarkSprites[iTransform].gameObject.SetActive(false);
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vSplashLabelPool == null)
			Debug.LogError("vSplashLabelPool is empty - " + this);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 월드에 떨어지는 보이기를 합니다.
	public IEnumerator ShowWorldDrop(String pText, Color pTextColor, int pMarkIdx, Vector3 pWorldPosition, WorldToScreen pWorldToScreen, Vector3 pScreenOffset, Vector3 pVelocity, float pDstSpeed, Vector3 pAccel, float pStartScale, float pEndScale, float pShowDelay, float pFadingDelay)
	{
		vDisplayLabel.text = pText;
		vDisplayLabel.color = pTextColor;

		// 마크 위치를 조정합니다.
		aMarkIdx = pMarkIdx; // 해당 마크만 활성화
		Vector3 lMarkStartPosition = vMarkOffset;
		Vector3 lMarkEndPosition = vMarkOffset;
		if (mMarkIdx >= 0)
		{
// 			lMarkStartPosition.x -= vDisplayLabel.relativeSize.x * pStartScale * 0.5f;
// 			lMarkEndPosition.x -= vDisplayLabel.relativeSize.x * pEndScale * 0.5f;
			lMarkStartPosition.x -= vDisplayLabel.width * pStartScale * 0.5f;
			lMarkEndPosition.x -= vDisplayLabel.width * pEndScale * 0.5f;
			//Debug.Log("vDisplayLabel.relativeSize=" + vDisplayLabel.relativeSize);

			vMarkSprites[mMarkIdx].color = pTextColor;
		}

		// 보이는 시간 동안의 변화를 처리합니다.
		float lFadingInterval = pShowDelay - pFadingDelay;		
		float lShowTimer = Time.deltaTime;
		Vector3 lDropScreenOffset = pScreenOffset;
		Vector3 lDropVelocity = pVelocity;
		Vector3 lBaseScale = mTransform.localScale;
				
		while (lShowTimer < pShowDelay)
		{
			float lTimerLerpFactor = lShowTimer / pShowDelay;
			
			float lTimeScale = Mathf.Lerp(cStartTimeScale, cEndTimeScale, lTimerLerpFactor);
			float lDeltaTime = Time.deltaTime * lTimeScale;
		
			// 위치를 움직입니다.
			mTransform.localPosition = pWorldToScreen.GetUIPosition(pWorldPosition) + lDropScreenOffset;
			lDropScreenOffset += lDropVelocity * (lDeltaTime * Mathf.Lerp(1.0f, pDstSpeed, lTimerLerpFactor));
			lDropVelocity += pAccel * lDeltaTime;

			// 색을 바꿉니다.
			if (lShowTimer > pFadingDelay)
			{
				float lFadingLerpFactor = (lShowTimer - pFadingDelay) / lFadingInterval;
				Color lFadingColor = new Color(
					pTextColor.r,
					pTextColor.g,
					pTextColor.b,
					pTextColor.a * (1.0f - lFadingLerpFactor));
				vDisplayLabel.color = lFadingColor;

				if (mMarkIdx >= 0)
					vMarkSprites[mMarkIdx].color = lFadingColor;
			}
			//Debug.Log("vDisplayLabel.color=" + vDisplayLabel.color);

			// 스케일을 바꿉니다.
			mTransform.localScale = lBaseScale * Mathf.Lerp(pStartScale, pEndScale, lTimerLerpFactor);
			
			// 타이머를 증가시킵니다.
			lShowTimer += lDeltaTime;
			yield return null;
		}

		// 비활성화 시킵니다.
		if (vSplashLabelPool != null)
			vSplashLabelPool.StoreSplashLabel(this);
		else
			Destroy(gameObject);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;

	private int mMarkIdx;
}
