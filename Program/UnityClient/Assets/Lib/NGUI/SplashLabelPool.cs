using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SplashLabelPool : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SplashLabel vSplashLabelPrefab;
	public Transform vSplashLabelRoot;

	public Vector3 vLabelScale = new Vector3(20.0f, 20.0f, 1.0f);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vSplashLabelPrefab == null)
			Debug.LogError("vSplashLabelPrefab is empty - " + this);
	
		mPoolingQueue = new Queue<SplashLabel>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 플래시 레이블을 생성합니다.
	public SplashLabel GenerateSplashLabel()
	{
		SplashLabel lSplashLabel;
		if (mPoolingQueue.Count <= 0)
		{
			GameObject lSplashLabelObject = (GameObject)Instantiate(vSplashLabelPrefab.gameObject);
			lSplashLabelObject.SetActive(true);

			lSplashLabel = lSplashLabelObject.GetComponent<SplashLabel>();
			lSplashLabel.aTransform.parent = vSplashLabelRoot; // 공용 루트를 가집니다.
			lSplashLabel.vSplashLabelPool = this;
		}
		else
		{
			lSplashLabel = mPoolingQueue.Dequeue();
		}

		lSplashLabel.aTransform.localScale = vLabelScale;
		//Debug.Log("lSplashLabel.aTransform.localPosition=" + lSplashLabel.aTransform.localPosition);

		lSplashLabel.gameObject.SetActive(true);
		
		return lSplashLabel;
	}
	//-------------------------------------------------------------------------------------------------------
	// 플래시 레이블을 팩토리에 보관합니다.
	public void StoreSplashLabel(SplashLabel pSplashLabel)
	{
		pSplashLabel.gameObject.SetActive(false);
		mPoolingQueue.Enqueue(pSplashLabel);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Queue<SplashLabel> mPoolingQueue;
}
