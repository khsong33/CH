﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class StackLabelDebug : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public UILabel vNGUILabel;
	public float vShowTime = 0.5f;
	public bool vIsTopDownWrite = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if(vNGUILabel == null)
			vNGUILabel = GetComponent<UILabel>();

		if (vNGUILabel == null)
			Debug.LogError("Not Exist vNGUILabel Component");

		vNGUILabel.text = String.Empty;

		mLabelStrings = new Queue<String>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 문자를 추가합니다.
	public void AddString(String pString, Color pColor)
	{
		Color32 pColor32 = new Color(pColor.r, pColor.g, pColor.b);
		String pLabelText = String.Format("[{0:X2}{1:X2}{2:X2}]{3}[-]", 
			pColor32.r,
			pColor32.g,
			pColor32.b,
			pString);

		mLabelStrings.Enqueue(pLabelText);
		_RefreshLabel();

		Invoke("_UpdateQueue", vShowTime);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 큐에 있는 메시지를 갱신합니다.
	private void _UpdateQueue()
	{
		mLabelStrings.Dequeue();
		_RefreshLabel();
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제 라벨 갱신
	private void _RefreshLabel()
	{
		Queue<String>.Enumerator iter = mLabelStrings.GetEnumerator();
		vNGUILabel.text = String.Empty;

		if (vIsTopDownWrite)
		{
			while (iter.MoveNext())
			{
				if (vNGUILabel.text.Length > 0)
					vNGUILabel.text += '\n';
				vNGUILabel.text += (String)iter.Current;
			}
		}
		else
		{
			_RefreshLabelBottomUpWriteCall(iter); // 재귀적으로 맨 마지막 줄부터 추가
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 실제 라벨 갱신
	private void _RefreshLabelBottomUpWriteCall(Queue<String>.Enumerator iter)
	{
		if (iter.MoveNext())
		{
			String lText = (String)iter.Current;
			_RefreshLabelBottomUpWriteCall(iter);
			if (vNGUILabel.text.Length > 0)
				vNGUILabel.text += '\n';
			vNGUILabel.text += lText;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Queue<String> mLabelStrings;
}
