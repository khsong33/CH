using UnityEngine;
using System.Collections;
using System;

public class StampSprite : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public UISprite vSprite;
	
	public Vector3 vStartScale = new Vector3(2.0f, 2.0f, 1.0f);
	public Vector3 vShrinkScale = new Vector3(0.9f, 0.9f, 1.0f);
	public float vStampDownDelay = 0.35f;
	public float vStampRestoreDelay = 0.15f;
	public float vStampStayDelay = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vSprite == null)
			Debug.LogError("vSprite is empty - " + this);
	
		mTransform = transform;
		mBaseScale = mTransform.localScale;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		Debug.Log("OnEnable() - " + this);
	
		mStampRestoreEndTime = vStampDownDelay + vStampRestoreDelay;
		mStampStayEndTime = mStampRestoreEndTime + vStampStayDelay;
		mStampTimer = 0.0f;
		
		mLastTime = Time.realtimeSinceStartup;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		float lNewTime = Time.realtimeSinceStartup;
		float lTimeDelta = lNewTime - mLastTime;
		mLastTime = lNewTime;

		mStampTimer += lTimeDelta * TimeUtil.aAccelTimeScale;

		if (mStampTimer <= vStampDownDelay)
		{
			// 시작 크기에서 축소 크기로 줄어듭니다.
			float lLerpFactor = mStampTimer / vStampDownDelay;
			mTransform.localScale = new Vector3(
				Mathf.Lerp(mBaseScale.x * vStartScale.x, mBaseScale.x * vShrinkScale.x, lLerpFactor),
				Mathf.Lerp(mBaseScale.y * vStartScale.y, mBaseScale.y * vShrinkScale.y, lLerpFactor),
				mBaseScale.z);

			vSprite.alpha = lLerpFactor;
		}
		else if (mStampTimer <= mStampRestoreEndTime)
		{
			// 축소 크기에서 원래 크기로 복구합니다.
			float lRestoreTimer = mStampTimer - vStampDownDelay;
			float lLerpFactor = lRestoreTimer / vStampRestoreDelay;
			mTransform.localScale = new Vector3(
				Mathf.Lerp(mBaseScale.x * vShrinkScale.x, mBaseScale.x, lLerpFactor),
				Mathf.Lerp(mBaseScale.y * vShrinkScale.y, mBaseScale.y, lLerpFactor),
				mBaseScale.z);
		}
		else if (mStampTimer <= mStampStayEndTime)
		{
			// 그대로 있습니다.
			mTransform.localScale = mBaseScale;
		}
		else
		{
			// 애니가 끝나서 비활성화합니다.
			gameObject.SetActive(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 보입니다.
	public void Show(Vector2 pShowPosition)
	{
		mTransform.localPosition = pShowPosition;
		gameObject.SetActive(true);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mBaseScale;
	private float mStampRestoreEndTime;
	private float mStampStayEndTime;

	private float mStampTimer;
	private float mLastTime;
}
