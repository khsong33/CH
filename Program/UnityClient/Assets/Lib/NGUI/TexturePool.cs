using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TexturePool : MonoBehaviour
{
	public class TextureInfo
	{
		internal TextureInfo(int pPoolGuid, int pTextureId, String pResourcePath)
		{
			mPoolGuid = pPoolGuid;
			mTextureId = pTextureId;
			mResourcePath = pResourcePath;
		}

		internal int mPoolGuid;
		internal int mTextureId;
		internal String mResourcePath;
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public List<String> vTextureResourcePathList;
	public List<Texture> vTextureList;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + gameObject);

		mPoolGuid = ++sPoolGuidGenerator;

		vTextureResourcePathList = new List<string>();
		vTextureList = new List<Texture>();

		mTextureIdDictionary = new Dictionary<String, int>();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 해당 경로를 가진 텍스처의 정보를 얻습니다.
	public TextureInfo GetTextureInfo(String pTextureResourcePath)
	{
		//Debug.Log("GetTextureId() pTextureResourcePath=" + pTextureResourcePath + " - " + this);

		int lTextureId;
		if (!mTextureIdDictionary.TryGetValue(pTextureResourcePath, out lTextureId))
		{
			lTextureId = _AddTexture(pTextureResourcePath);
			if (lTextureId < 0)
				return null;
		}
		return new TextureInfo(mPoolGuid, lTextureId, pTextureResourcePath);
	}
	//-------------------------------------------------------------------------------------------------------
	// 텍스처 정보를 가지고 텍스처를 얻습니다.
	public Texture LoadTexture(TextureInfo pTextureInfo)
	{
		// 텍스처 정보를 검증합니다.
		if (pTextureInfo.mPoolGuid != mPoolGuid)
		{
			//Debug.Log("if (pTextureInfo.mPoolGuid != mPoolGuid) pTextureInfo.mResourcePath=" + pTextureInfo.mResourcePath);
			if (!mTextureIdDictionary.TryGetValue(pTextureInfo.mResourcePath, out pTextureInfo.mTextureId))
			{
				pTextureInfo.mTextureId = _AddTexture(pTextureInfo.mResourcePath);
				if (pTextureInfo.mTextureId < 0)
					return null;
			}
		}

		// 텍스처를 반환합니다.
		return vTextureList[pTextureInfo.mTextureId];
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 텍스처를 풀에 추가합니다.
	private int _AddTexture(String pTextureResourcePath)
	{
		int lTextureId = vTextureResourcePathList.Count;

		Texture lTexture = Resources.Load(pTextureResourcePath) as Texture;
		if (lTexture == null)
		{
			Debug.LogError(String.Format("can't load texture '{0}'" + pTextureResourcePath));
			return -1;
		}
		vTextureList.Add(lTexture);

		vTextureResourcePathList.Add(pTextureResourcePath);
		mTextureIdDictionary.Add(pTextureResourcePath, lTextureId);
		
		return lTextureId;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static int sPoolGuidGenerator = 0;
	private int mPoolGuid;

	private Dictionary<String, int> mTextureIdDictionary;
}
