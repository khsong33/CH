using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TextureResourceManager
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static TextureResourceManager get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new TextureResourceManager();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public TextureResourceManager()
	{
		mTextureDictionary = new Dictionary<String, Texture>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 텍스쳐를 로딩합니다.
	public Texture LoadTexture(AssetKey pTextureAssetKey)
	{
		Texture lTexture;
		if (!mTextureDictionary.TryGetValue(pTextureAssetKey.aAssetPath, out lTexture))
		{
			lTexture = AssetManager.get.Load<Texture>(pTextureAssetKey);

			mTextureDictionary.Add(pTextureAssetKey.aAssetPath, lTexture);
		}

		return lTexture;
	}
	//-------------------------------------------------------------------------------------------------------
	// 모든 텍스쳐를 삭제합니다.
	public void Clear()
	{
		mTextureDictionary.Clear();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static TextureResourceManager sInstance = null;

	private Dictionary<String, Texture> mTextureDictionary;
}
