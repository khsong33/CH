using UnityEngine;
using System.Collections;
using System;

public class ThumbSprite : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Vector2 vPivotOffset = Vector2.zero;
	public float vHoverAlpha = 0.35f;
	public float vTouchAlpha = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		// 모바일 기기에서는 숨깁니다.
		if ((Application.platform == RuntimePlatform.Android) ||
			(Application.platform == RuntimePlatform.IPhonePlayer))
		{
			gameObject.SetActive(false);
			return;
		}

		mTransform = transform;
		mTexture = GetComponent<UITexture>();
		mUIRoot = NGUITools.FindInParents<UIRoot>(gameObject);

		mTexture.alpha = vHoverAlpha;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		if (Input.GetMouseButtonDown(0))
			mTexture.alpha = vTouchAlpha;
		else if (Input.GetMouseButtonUp(0))
			mTexture.alpha = vHoverAlpha;

		if (mTexture.enabled)
		{
			float lUIPixelScale = (float)mUIRoot.activeHeight / Screen.height;

			Vector3 lUIPoint = Input.mousePosition;
			lUIPoint.x -= Screen.width * 0.5f + vPivotOffset.x * lUIPixelScale;
			lUIPoint.y -= Screen.height * 0.5f - vPivotOffset.y * lUIPixelScale;
			lUIPoint.x *= lUIPixelScale;
			lUIPoint.y *= lUIPixelScale;
			//Debug.Log("lUIPoint=" + lUIPoint);

			mTransform.localPosition = new Vector3(
				lUIPoint.x,
				lUIPoint.y,
				mTransform.localPosition.z);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private UITexture mTexture;
	private UIRoot mUIRoot;
}
