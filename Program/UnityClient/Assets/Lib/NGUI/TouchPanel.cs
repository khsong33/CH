using UnityEngine;
using System.Collections;
using System;

public class TouchPanel : MonoBehaviour
{
	public enum DragMode
	{
		DragCamera,
		DragPlay,
	}
	
	private enum TouchAction
	{
		None,
		OneTouch,
		TwoTouch,
	}

	public delegate void OnDragStartDelegate(Plane pDragPlane, Vector2 pDragStartPoint);
	public delegate void OnDragMoveDelegate(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount);
	public delegate void OnDragEndDelegate(Plane pDragPlane, Vector2 pDragEndPoint);
	public delegate void OnClickDelegate(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded);
	public delegate void OnDoubleClickDelegate(Plane pClickPlane, Vector2 pClickPoint);
	public delegate void OnZoomDelegate(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta);
	public delegate void OnHoldDelegate(Plane pHoldPlane, Vector2 pHoldPoint, bool pIsHolding);

	public const float cWheelZoomSpeedScale = -0.5f;
	public const float cTouchZoomSpeedScale = -0.005f;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Camera vTouchCamera;
	public Vector3 vTouchPlaneNormal = Vector3.up;
	public Vector3 vTouchPlanePoint = Vector3.zero;

	public float vWheelZoomSpeed = -4.0f;
	public float vTouchZoomSpeed = 2.0f;
	public float vPlayDragThreshold = 2.0f;
	public float vHoldDelay = 0.3f;

	public bool vIsEnableDrag = true;
	public bool vIsEnableZoom = true;

	public QuaterViewCameraSet vQuaterViewCameraSet;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 드래그를 시작할 때 호출되는 콜백
	public OnDragStartDelegate aOnDragStartDelegate { get; set; }
	public OnDragMoveDelegate aOnDragMoveDelegate { get; set; }
	public OnDragEndDelegate aOnDragEndDelegate { get; set; }
	public OnClickDelegate aOnClickDelegate { get; set; }
	public OnDoubleClickDelegate aOnDoubleClickDelegate { get; set; }
	public OnZoomDelegate aOnZoomDelegate { get; set; }
	public OnHoldDelegate aOnHoldDelegate { get; set; }

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		if (vTouchCamera == null)
			vTouchCamera = Camera.main;
	
		mBoxCollider = GetComponent<BoxCollider>();
		if (mBoxCollider == null)
			Debug.LogError("can't find BoxCollider - " + gameObject);

		mTouchPlane = new Plane(vTouchPlaneNormal, vTouchPlanePoint);
		
		mSqrDragThreshold = vPlayDragThreshold * vPlayDragThreshold;
		mIsDragged = false;

		mHoldEventGuid = 0;
		mHoldStartGuid = -1;

		UpdateCollider();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		if (mTouchAction == TouchAction.OneTouch)
			mDragTimer += Time.deltaTime;
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnDrag(Vector2 pDelta)
	{
		//Debug.Log(String.Format("OnDrag() lastEventPosition{3} pDelta({0:0.##}, {1:0.##}) touchId({2})", pDelta.x, pDelta.y, UICamera.currentTouchID, UICamera.lastEventPosition));
		//Debug.Log(String.Format("OnDrag() lastEventPosition{3} pDelta({0:0.##}, {1:0.##}) touchId({2}) mTouchAction({4})", pDelta.x, pDelta.y, UICamera.currentTouchID, UICamera.lastEventPosition, mTouchAction));

		switch (mTouchAction)
		{
		case TouchAction.OneTouch:
			{
				Vector2 lDragVector = UICamera.lastEventPosition - mDragTouchPoint;
				if (lDragVector.sqrMagnitude >= mSqrDragThreshold)
				{
					if (!mIsDragged)
					{
						mIsDragged = true;
						//Debug.Log("mIsDragged=" + mIsDragged);

						if (aOnDragStartDelegate != null)
							aOnDragStartDelegate(
								mTouchPlane,
								UICamera.lastEventPosition);
						else if (vQuaterViewCameraSet != null)
							_OnDragStartQuaterViewCameraSet(
								mTouchPlane,
								UICamera.lastEventPosition);
					}

					if (aOnDragMoveDelegate != null)
						aOnDragMoveDelegate(
							mTouchPlane,
							UICamera.lastEventPosition,
							mDragTouchPoint,
							mDragTimer,
							Input.touchCount);
					else if (vQuaterViewCameraSet != null)
						_OnDragMoveQuaterViewCameraSet(
							mTouchPlane,
							UICamera.lastEventPosition,
							mDragTouchPoint,
							mDragTimer,
							Input.touchCount);

					mDragTouchPoint = UICamera.lastEventPosition;
				}

				mDragTimer = 0.0f; // 드래그 타이머를 다시 리셋합니다.
			}
			break;
		case TouchAction.TwoTouch:
			{
				//Debug.Log("case TouchAction.TwoTouch: vTouchZoomSpeed=" + vTouchZoomSpeed + " mFirstTouchID=" + mFirstTouchID);

				if (mFirstTouchID >= 0)
				{
					// 실제 터치라면 터치 번호에 따라 위치를 갱신합니다.
					if (UICamera.currentTouchID == mFirstTouchID)
						mFirstTouchPoint = UICamera.lastEventPosition;
					else if (UICamera.currentTouchID == mSecondTouchID)
						mSecondTouchPoint = UICamera.lastEventPosition;
				}
				else
				{
					// 마우스 터치라면 두 번째 터치의 움직임으로 간주합니다.
					if (UICamera.currentTouchID == -1) // 주로 왼쪽 버튼 입력이 들어오고 가끔씩 오른쪽 버튼 입력이 들어오는데, 오른쪽은 무시하고 왼쪽 버튼 입력일 때에만 커서 위치를 인정합니다.
						mSecondTouchPoint = UICamera.lastEventPosition;
				}

				float lNewZoomTouchDistance = (mSecondTouchPoint - mFirstTouchPoint).magnitude;
				float lZoomDelta = lNewZoomTouchDistance - mZoomTouchDistance;
				mZoomTouchDistance = lNewZoomTouchDistance;
				
				if (vIsEnableZoom &&
					(vTouchZoomSpeed != 0.0f))
				{
					if (aOnZoomDelegate != null)
						aOnZoomDelegate(
							mTouchPlane,
							UICamera.lastEventPosition,
							lZoomDelta * -vTouchZoomSpeed * cTouchZoomSpeedScale);
					else if (vQuaterViewCameraSet != null)
						_OnZoomQuaterViewCameraSet(
							mTouchPlane,
							UICamera.lastEventPosition,
							lZoomDelta * -vTouchZoomSpeed * cTouchZoomSpeedScale);
				}
			}
			break;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnPress(bool pIsDown)
	{
		//Debug.Log(String.Format("OnPress(pIsDown={0}, currentTouchID={1})", pIsDown, UICamera.currentTouchID) + " mTouchAction=" + mTouchAction);

		if (pIsDown)
		{
			++mTouchCount;
			//Debug.Log("++mTouchCount; mTouchCount=" + mTouchCount);
			
			switch (mTouchAction)
			{
			case TouchAction.None:
				{
					// 첫 터치가 생기면 드래그 상태가 됩니다.
					if (mTouchCount == 1)
						_StartOneTouch(
							UICamera.currentTouchID,
							UICamera.lastEventPosition);
				}
				break;
			case TouchAction.OneTouch:
				{
					// 더블 클릭 할 때 OnPress(true)가 두 번 호출되는 것을 무시합니다.
					if (mFirstTouchID == UICamera.currentTouchID)
					{
						--mTouchCount;
						//Debug.Log("if (mFirstTouchID == UICamera.currentTouchID)");
						break;
					}
				
					// 또 다른 터치가 생기면 드래그 상태를 해제하고 줌 상태가 됩니다.
					_ResetOneTouch(false);

					// 마우스 입력이라면 2-터치 흉내를 위해서 화면 중심을 첫 터치 위치로 기억합니다.
					if (UICamera.currentTouchID < 0)
					{
						Camera lUICamera = NGUITools.FindCameraForLayer(gameObject.layer);
						mFirstTouchPoint = new Vector2(
							lUICamera.pixelRect.width * 0.5f,
							lUICamera.pixelRect.height * 0.5f);
						//Debug.Log("mFirstTouchPoint=" + mFirstTouchPoint + " UICamera.lastEventPosition=" + UICamera.lastEventPosition);
					}
					
					mSecondTouchID = UICamera.currentTouchID;
					mSecondTouchPoint = UICamera.lastEventPosition;
					mTouchAction = TouchAction.TwoTouch;
					//Debug.Log("mTouchAction = TouchAction.TwoTouch;");
					
					mZoomTouchDistance = (mFirstTouchPoint - mSecondTouchPoint).magnitude;
				}
				break;
			case TouchAction.TwoTouch:
				{
					// 줌 상태에서는 새로운 터치를 무시합니다.
				}
				break;
			}
		}
		else
		{
			--mTouchCount;
			//Debug.Log("--mTouchCount; mTouchCount=" + mTouchCount);

			switch (mTouchAction)
			{
			case TouchAction.None:
				{
					if (mIsMultiTouched &&
						(mTouchCount == 0))
					{
						mIsMultiTouched = false;
						//Debug.Log(String.Format("mIsMultiTouched={0}", mIsMultiTouched));
					}
				}
				break;
			case TouchAction.OneTouch:
				{
					// 떼어진 터치가 드래그 터치나 줌 터치라면 원 터치 상태를 해제합니다.
					if ((mFirstTouchID == UICamera.currentTouchID) ||
						(mSecondTouchID == UICamera.currentTouchID))
					{
						bool lIsClicked = !mIsMultiTouched;
						_ResetOneTouch(lIsClicked);

						mIsMultiTouched = false;
					}
				}
				break;
			case TouchAction.TwoTouch:
				{
					// 떼어진 터치가 드래그 터치나 줌 터치라면 줌 상태를 해제합니다.
					if ((mFirstTouchID == UICamera.currentTouchID) ||
						(mSecondTouchID == UICamera.currentTouchID))
					{
						if (mFirstTouchID == UICamera.currentTouchID)
							_StartOneTouch(
								mSecondTouchID,
								mSecondTouchPoint);
						else
							_StartOneTouch(
								mFirstTouchID,
								mFirstTouchPoint);
						
						mIsMultiTouched = true; // 모든 터치가 없어질 때까지 터치 입력을 무시하는 락 플래그를 켭니다.
						//Debug.Log(String.Format("mIsMultiTouched={0}", mIsMultiTouched));
					}
				}
				break;
			}
		}
		//Debug.Log(String.Format("OnPress(pIsDown={0}) mTouchCount={1} currentTouchID={2} mTouchAction={3}", pIsDown, mTouchCount, UICamera.currentTouchID, mTouchAction));
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnClick()
	{
		//Debug.Log("OnClick()");

		if (mTouchCount > 0)
			return;
// 		if (mIsHolding)
// 			return; // 홀딩 이벤트로 처리했음

		// 클릭을 처리합니다.
		if (aOnClickDelegate != null)
			aOnClickDelegate(
				mTouchPlane,
				UICamera.lastEventPosition,
				mIsHolding);
		else if (vQuaterViewCameraSet != null)
			_OnClickQuaterViewCameraSet(
				mTouchPlane,
				UICamera.lastEventPosition,
				mIsHolding);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnDoubleClick()
	{
		//Debug.Log("OnDoubleClick()");

		// 클릭을 처리합니다.
		if (aOnDoubleClickDelegate != null)
			aOnDoubleClickDelegate(
				mTouchPlane,
				UICamera.lastEventPosition);
		else if (vQuaterViewCameraSet != null)
			_OnDoubleClickQuaterViewCameraSet(
				mTouchPlane,
				UICamera.lastEventPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// NGUI 콜백
	public void OnScroll(float pDelta)
	{
		//Debug.Log("OnScroll() pDelta=" + pDelta);

		if (vIsEnableZoom)
		{
			if (aOnZoomDelegate != null)
				aOnZoomDelegate(
					mTouchPlane,
					Input.mousePosition,
					pDelta * vWheelZoomSpeed * cWheelZoomSpeedScale);
			else if (vQuaterViewCameraSet != null)
				_OnZoomQuaterViewCameraSet(
					mTouchPlane,
					Input.mousePosition,
					pDelta * vWheelZoomSpeed * cWheelZoomSpeedScale);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 충돌체를 화면 크기에 맞춰 갱신합니다.
	public void UpdateCollider()
	{
		// 리지드 바디와 충돌하지 않습니다.
		mBoxCollider.isTrigger = true;

		// 충돌체의 중심을 지정합니다.
		mBoxCollider.center = new Vector3(0.0f, 0.5f, -0.25f);
		// 꽉찬 패널에 NGUI로 충돌체를 적용했더니 위와 같은 상수로 초기화되기에, 따라서 적용함.

		// 충돌체에 적용할 패널 크기를 구합니다.
		Camera lUICamera = NGUITools.FindCameraForLayer(gameObject.layer);
		UIRoot lUIRoot = NGUITools.FindInParents<UIRoot>(gameObject);

		float lSizeAdjustment;
		if (lUIRoot != null)
			lSizeAdjustment = lUIRoot.pixelSizeAdjustment;
		else
			lSizeAdjustment = 1.0f;

		float lRectWidth = lUICamera.pixelRect.width;
		float lRectHeight = lUICamera.pixelRect.height;

		if (lSizeAdjustment != 1f && lRectHeight > 1f)
		{
			float lScale = lUIRoot.activeHeight / lRectHeight;
			lRectWidth *= lScale;
			lRectHeight *= lScale;
		}

		// 충돌체의 크기를 지정합니다.
		Vector3 lLocalScale = transform.localScale;
		mBoxCollider.size = new Vector3(
			lLocalScale.x * lRectWidth,
			lLocalScale.x * lRectHeight,
			0.0f);
		//Debug.Log("mBoxCollider.size=" + mBoxCollider.size);
	}
	//-------------------------------------------------------------------------------------------------------
	// 터치 위치를 얻습니다.
	public Vector3 GetTouchPosition(Vector2 pUIPoint)
	{
		Vector3 lTouchPoint = _GetTouchPoint(
			mTouchPlane,
			vTouchCamera.ScreenPointToRay(pUIPoint));
			
		return lTouchPoint;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 터치 위치를 얻습니다.
	private Vector3 _GetTouchPoint(Plane pTouchPlane, Ray pTouchRay)
	{
		//Debug.DrawRay(pTouchRay.origin, pTouchRay.direction * 10, Color.yellow);
		float lTouchDistance;
		if (pTouchPlane.Raycast(pTouchRay, out lTouchDistance))
		{
			Vector3 lTouchPoint = pTouchRay.GetPoint(lTouchDistance);
			//Logger.Log("lTouchPoint=" + LogUtil.LogVector(lTouchPoint));
			return lTouchPoint;
		}
		else
		{
			//Logger.Log("--------");
			//Logger.Log("no hit! - pTouchRay.origin=" + pTouchRay.origin);
			//Logger.Log("--------");
			return Vector3.zero;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 단일 터치 상태를 시작합니다.
	private void _StartOneTouch(int pTouchID, Vector2 pUITouchPoint)
	{
		//Debug.Log("_StartOneTouch() pTouchID=" + pTouchID + " + mTouchCount=" + mTouchCount);

		mFirstTouchID = pTouchID;
		mFirstTouchPoint = pUITouchPoint;
		mDragTouchPoint = pUITouchPoint;
		mTouchAction = TouchAction.OneTouch;
		mDragTimer = 0.0f;

		if (aOnHoldDelegate != null)
		{
			//Debug.Log("StartCoroutine(_WaitHold(++mHoldEventGuid))");
			StartCoroutine(_WaitHold(++mHoldEventGuid));
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 단일 터치 상태를 해제합니다.
	private void _ResetOneTouch(bool pIsClick)
	{
		//Debug.Log("_ResetOneTouch() pIsClick=" + pIsClick);

		if (mIsDragged)
		{
			// 드래그를 멈춥니다.
			if (aOnDragEndDelegate != null)
				aOnDragEndDelegate(
					mTouchPlane,
					UICamera.lastEventPosition);
			else if (vQuaterViewCameraSet != null)
				_OnDragEndQuaterViewCameraSet(
					mTouchPlane,
					UICamera.lastEventPosition);

			mIsDragged = false;
		}
		else if (pIsClick &&
			(mTouchCount == 0))
		{
// 			// 클릭을 처리합니다.
// 			if (aOnClickDelegate != null)
// 				aOnClickDelegate(
// 					mTouchPlane,
// 					UICamera.lastEventPosition);
// 			else if (vQuaterViewCameraSet != null)
// 				_OnClickQuaterViewCameraSet(
// 					mTouchPlane,
// 					UICamera.lastEventPosition);
		}

		mTouchAction = TouchAction.None;

		if ((mHoldStartGuid == mHoldEventGuid) &&
			(aOnHoldDelegate != null))
			aOnHoldDelegate(mTouchPlane, mFirstTouchPoint, false);
		++mHoldEventGuid; // 홀드 입력 초기화
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragStartDelegate
	private void _OnDragStartQuaterViewCameraSet(Plane pDragPlane, Vector2 pDragStartPoint)
	{
		//Debug.Log("_OnDragStart()");

		if (vIsEnableDrag)
			vQuaterViewCameraSet.DragStart(pDragPlane, pDragStartPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragMoveDelegate
	private void _OnDragMoveQuaterViewCameraSet(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount)
	{
		//Debug.Log("_OnDragMove()");

		if (vIsEnableDrag)
			vQuaterViewCameraSet.DragMove(pDragPlane, pDragLastPoint, pDragMovePoint, pDragDelay, pTouchCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragEndDelegate
	private void _OnDragEndQuaterViewCameraSet(Plane pDragPlane, Vector2 pDragEndPoint)
	{
		//Debug.Log("_OnDragEnd() mIsDraggingStage=" + mIsDraggingStage);

		if (vIsEnableDrag)
			vQuaterViewCameraSet.DragEnd(pDragPlane, pDragEndPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClickQuaterViewCameraSet(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded)
	{
		//Debug.Log("_OnClick()");

// 		if (Terrain.activeTerrain != null)
// 		{
// 			Ray lTouchRay = Camera.main.ScreenPointToRay(pClickPoint);
// 			RaycastHit lRaycastHit;
// 			if (Physics.Raycast(lTouchRay, out lRaycastHit))
// 			{
// 				Debug.Log("lRaycastHit.point=" + lRaycastHit.point);
// 			}
// 		}
// 		else
		{
			Vector3 lClickPosition = CameraUtil.GetTouchPosition(Camera.main, pClickPlane, pClickPoint);
			Debug.Log("lClickPosition=" + lClickPosition);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDoubleClickDelegate
	private void _OnDoubleClickQuaterViewCameraSet(Plane pClickPlane, Vector2 pClickPoint)
	{
		//Debug.Log("_OnDoubleClick()");

		Vector3 lDoubleClickPosition = CameraUtil.GetTouchPosition(Camera.main, pClickPlane, pClickPoint);
		Debug.Log("lDoubleClickPosition=" + lDoubleClickPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnZoomDelegate
	private void _OnZoomQuaterViewCameraSet(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta)
	{
		if (vIsEnableZoom)
			vQuaterViewCameraSet.Zoom(pZoomPlane, pZoomCenterPoint, pZoomDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 홀드 이벤트를 처리합니다.
	private IEnumerator _WaitHold(int pHoldGuid)
	{
		//Debug.Log("_WaitHold() pHoldGuid=" + pHoldGuid);

		mHoldEventGuid = pHoldGuid;
		mIsHolding = false;

		yield return new WaitForSeconds(vHoldDelay);

		if ((mHoldEventGuid == pHoldGuid) &&
			!mIsDragged)
		{
			//Debug.Log("OnHold() mHoldEventGuid=" + mHoldEventGuid);
			if (aOnHoldDelegate != null)
			{
				mIsHolding = true;
				mHoldStartGuid = mHoldEventGuid;
				aOnHoldDelegate(mTouchPlane, mFirstTouchPoint, true);
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private BoxCollider mBoxCollider;
	private Plane mTouchPlane;
	private TouchAction mTouchAction = TouchAction.None;
	private bool mIsMultiTouched = false;
	private int mTouchCount = 0;
	private int mFirstTouchID = 0;
	private Vector2 mFirstTouchPoint;
	private int mSecondTouchID = 0;
	private Vector2 mSecondTouchPoint;
	private float mZoomTouchDistance;

	private float mSqrDragThreshold;
	private Vector2 mDragTouchPoint;
	private bool mIsDragged;
	private float mDragTimer;

	private int mHoldEventGuid;
	private int mHoldStartGuid;
	private bool mIsHolding;
}
