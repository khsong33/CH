using UnityEngine;
using System.Collections;
using System;

public class TweakObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Vector3 vTweakScale = new Vector3(1.2f, 1.2f, 1.2f);
	public float vTweakDelay = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransform = transform;
		mObjectLocalScale = mTransform.localScale;
		mObjectTweakScaleOffset = new Vector3(
			mObjectLocalScale.x * (vTweakScale.x - 1.0f),
			mObjectLocalScale.y * (vTweakScale.y - 1.0f),
			mObjectLocalScale.z * (vTweakScale.z - 1.0f));
		mTweakTimer = 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		mTweakTimer += Time.deltaTime;
		if (mTweakTimer >= vTweakDelay)
			mTweakTimer -= (float)((int)(mTweakTimer / vTweakDelay)); // [0, vTweakDelay) 이후에 다시 0부터 시작
		
		float lTweakFactor = mTweakTimer / vTweakDelay;
		float lLerpFactor = Mathf.Abs(lTweakFactor - 0.5f);
		mTransform.localScale = mObjectLocalScale + mObjectTweakScaleOffset * lLerpFactor;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mObjectLocalScale;
	private Vector3 mObjectTweakScaleOffset;
	private float mTweakTimer;
}
