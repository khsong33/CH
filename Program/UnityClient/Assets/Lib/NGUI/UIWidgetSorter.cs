using UnityEngine;
using System.Collections;
using System;

public class UIWidgetSorter : MonoBehaviour
{
	[SerializableAttribute]
	public class SortingItem
	{
		public UIWidget vUIWidget;
		public int vDepthOffset;
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public SortingItem[] vSortingItems;
	public float vUpdateDelayMin = 1.2f;
	public float vUpdateDelayMax = 1.3f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vSortingItems.Length != vSortingItems.Length)
			Debug.LogError("vUIWidgets length mismatch - " + this);

		mTransform = transform;
		mCameraTransfrom = Camera.main.transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		StartCoroutine(_UpdateUIDepth());
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// UI 깊이를 갱신합니다.
	public void SetUIDepth(int pUIDepth)
	{
		for (int iWidget = 0; iWidget < vSortingItems.Length; ++iWidget)
		{
			SortingItem lSortingItem = vSortingItems[iWidget];
			lSortingItem.vUIWidget.depth = pUIDepth + lSortingItem.vDepthOffset;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 일정 주기로 깊이값을 갱신합니다.
	private IEnumerator _UpdateUIDepth()
	{
		yield return null; // Start() 이후에 실행되도록 한 프레임 건너뜀

		for (;;)
		{
			float lSqrCameraDistance = (mTransform.position - mCameraTransfrom.position).sqrMagnitude;
			SetUIDepth((int)-lSqrCameraDistance);

			yield return new WaitForSeconds(UnityEngine.Random.Range(vUpdateDelayMin, vUpdateDelayMax));
//			yield return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Transform mCameraTransfrom;
}
