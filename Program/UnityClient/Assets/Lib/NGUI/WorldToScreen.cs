using UnityEngine;
using System.Collections;
using System;

public class WorldToScreen : MonoBehaviour
{
	private delegate Vector3 _GetUIPointDelegate(Vector3 pScreenPosition);

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Camera vWorldCamera;
	public UIAnchor.Side vAnchorSide = UIAnchor.Side.Center;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		if (vWorldCamera == null)
			vWorldCamera = Camera.main;

		mUIRoot = NGUITools.FindInParents<UIRoot>(gameObject);
		if (mUIRoot == null)
			Debug.LogError("can't find UIRoot - " + this);
			
		mGetUIPointDelegates = new _GetUIPointDelegate[Enum.GetValues(typeof(UIAnchor.Side)).Length];
		mGetUIPointDelegates[(int)UIAnchor.Side.BottomLeft] = _GetUIPoint_BottomLeft;
		mGetUIPointDelegates[(int)UIAnchor.Side.Left] = _GetUIPoint_Left;
		mGetUIPointDelegates[(int)UIAnchor.Side.TopLeft] = _GetUIPoint_TopLeft;
		mGetUIPointDelegates[(int)UIAnchor.Side.Top] = _GetUIPoint_Top;
		mGetUIPointDelegates[(int)UIAnchor.Side.TopRight] = _GetUIPoint_TopRight;
		mGetUIPointDelegates[(int)UIAnchor.Side.Right] = _GetUIPoint_Right;
		mGetUIPointDelegates[(int)UIAnchor.Side.BottomRight] = _GetUIPoint_BottomRight;
		mGetUIPointDelegates[(int)UIAnchor.Side.Bottom] = _GetUIPoint_Bottom;
		mGetUIPointDelegates[(int)UIAnchor.Side.Center] = _GetUIPoint_Center;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// UI 좌표를 얻습니다.
	public Vector3 GetUIPosition(Vector3 pWorldPosition)
	{
		//Debug.Log("mGetUIPointDelegates=" + mGetUIPointDelegates + " vWorldCamera=" + vWorldCamera);

		// 월드 좌표를 스크린 좌표로 변환합니다.
		Vector3 lUIPoint = mGetUIPointDelegates[(int)vAnchorSide](vWorldCamera.WorldToScreenPoint(pWorldPosition));
			
// 		// 뷰포트 크기를 반영합니다.
// 		lUIPoint.x *= vWorldCamera.rect.width;
// 		lUIPoint.y *= vWorldCamera.rect.height;

		// UI 좌표계와의 스케일 차이를 반영합니다.
		float lUIPixelScale = (float)mUIRoot.activeHeight / Screen.height;
		//Debug.Log("lUIPixelScale=" + lUIPixelScale);
		lUIPoint.x *= lUIPixelScale;
		lUIPoint.y *= lUIPixelScale;
		//Debug.Log("lUIPoint=" + lUIPoint);

		Vector3 lUIPosition = new Vector3(
			lUIPoint.x,
			lUIPoint.y,
			pWorldPosition.z);
		//Debug.Log("lUIPosition=" + lUIPosition);
		
		return lUIPosition;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_BottomLeft(Vector3 pScreenPosition)
	{
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_Left(Vector3 pScreenPosition)
	{
		pScreenPosition.y -= Screen.height * 0.5f;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_TopLeft(Vector3 pScreenPosition)
	{
		pScreenPosition.y -= Screen.height;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_Top(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width * 0.5f;
		pScreenPosition.y -= Screen.height;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_TopRight(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width;
		pScreenPosition.y -= Screen.height;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_Right(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width;
		pScreenPosition.y -= Screen.height * 0.5f;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_BottomRight(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_Bottom(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width * 0.5f;
		return pScreenPosition;
	}
	//-------------------------------------------------------------------------------------------------------
	// _GetUIPointDelegate
	private Vector3 _GetUIPoint_Center(Vector3 pScreenPosition)
	{
		pScreenPosition.x -= Screen.width * 0.5f;
		pScreenPosition.y -= Screen.height * 0.5f;
		return pScreenPosition;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UIRoot mUIRoot;
	private _GetUIPointDelegate[] mGetUIPointDelegates;
}
