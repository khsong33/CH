using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class FrameBehaviour : FrameUpdateBehaviour, IFrameObject
{
	public uint vGuid = 0;
	public int vTaskCount = 0;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// IGuidObject 속성
	public uint aGuid
	{
		get { return vGuid; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameObject 속성
	public int aUpdateFrameSliceIdx
	{
		get { return mUpdateFrameSliceIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// IFrameObject 속성
	public int aTaskCount
	{
		get { return vTaskCount; }
		set { vTaskCount = value; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 프레임 오브젝트 GUID를 생성합니다.
	public void GenerateFrameObjectGuid(FrameUpdater pFrameUpdater)
	{
		vGuid = pFrameUpdater.GenerateObjectGuid();
		mUpdateFrameSliceIdx = pFrameUpdater.GenerateFrameSliceIdx();
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 오브젝트 GUID를 초기화합니다.
	public void ResetFrameObjectGuid(FrameUpdater pFrameUpdater)
	{
		pFrameUpdater.DeleteFrameSliceIdx(mUpdateFrameSliceIdx);
		vGuid = 0;
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신을 반복합니다.
	public void RepeatUpdate(FrameUpdater pRepeatUpdater)
	{
		RepeatUpdate(this, pRepeatUpdater, FrameUpdateLayer.Layer0);
	}
	//-------------------------------------------------------------------------------------------------------
	// IGuidObject 메서드
	public void ResetGuid()
	{
		vGuid = 0;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private int mUpdateFrameSliceIdx;
}
