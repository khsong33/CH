using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class FrameUpdateBehaviour : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 프레임 객체 인터페이스
	public IFrameObject aFrameObject
	{
		get { return mFrameObject; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 반복 갱신자
	public FrameUpdater aFrameUpdater
	{
		get { return mFrameUpdater; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 갱신을 반복합니다.
	public void RepeatUpdate(IFrameObject pFrameObject, FrameUpdater pFrameUpdater, FrameUpdateLayer pFrameUpdateLayer)
	{
		//Debug.Log("RepeatUpdate() - " + this);

		mFrameObject = pFrameObject;
		mFrameUpdater = pFrameUpdater;

		if (mOnFrameUpdateTask != null)
			mOnFrameUpdateTask.Cancel();
		mOnFrameUpdateTask = pFrameUpdater.RepeatTask(pFrameObject, OnFrameUpdate, pFrameUpdateLayer);
	}
	//-------------------------------------------------------------------------------------------------------
	// 아이템을 갱신 리스트에서 뺍니다.
	public void StopUpdate()
	{
		if (mOnFrameUpdateTask == null)
			return;

		mOnFrameUpdateTask.Cancel();
		mOnFrameUpdateTask = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// [FrameTask] 갱신 콜백
	public virtual void OnFrameUpdate(FrameTask pTask, int pFrameDelta)
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private IFrameObject mFrameObject;
	private FrameUpdater mFrameUpdater;
	private FrameTask mOnFrameUpdateTask;
}
