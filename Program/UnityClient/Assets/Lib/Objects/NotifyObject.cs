using UnityEngine;
using System.Collections;
using System;

public class NotifyObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터 인자
	//-------------------------------------------------------------------------------------------------------
	public Vector3 vNotifyScale = new Vector3(0.2f, 0.2f, 0.2f);
	public float vNotifyDelay = 0.2f;
	public bool vIsNotifyAtStart = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mIsStarted = false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		mTransform = transform;
		mObjectLocalScale = mTransform.localScale;

		if (vIsNotifyAtStart)
			StartCoroutine(_Notify());
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (mIsStarted)
		{
			if (vIsNotifyAtStart)
				StartCoroutine(_Notify());
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 팝업 애니메이션을 합니다.
	public void Notify()
	{
		if (gameObject.activeInHierarchy)
			StartCoroutine(_Notify());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 팝업 애니메이션을 합니다.
	private IEnumerator _Notify()
	{
		yield return null; // Start() 이후에 진행되도록
	
		mNotifyTimer = Time.deltaTime;

		while (mNotifyTimer < vNotifyDelay)
		{
			float lLerpFactor = mNotifyTimer / vNotifyDelay;
			float lNotifyFactor = -lLerpFactor * lLerpFactor + 1.0f;

			mTransform.localScale = new Vector3(
				mObjectLocalScale.x * (1.0f + vNotifyScale.x * lNotifyFactor),
				mObjectLocalScale.y * (1.0f + vNotifyScale.y * lNotifyFactor),
				mObjectLocalScale.z * (1.0f + vNotifyScale.z * lNotifyFactor));

			mNotifyTimer += RealTime.deltaTime;
			yield return null;
		}

		mTransform.localScale = mObjectLocalScale;
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mObjectLocalScale;
	private float mNotifyTimer;
	private bool mIsStarted;
}
