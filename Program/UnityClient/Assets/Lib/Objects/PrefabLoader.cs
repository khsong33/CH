using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PrefabLoader : MonoBehaviour
{
	public String vPrefabPath;

	void Start()
	{
		UnityEngine.Object lPrefabObject = Resources.Load(vPrefabPath);
		if (lPrefabObject == null)
			Debug.LogError("can't load resource \"" + vPrefabPath + "\"");

		GameObject lObject = UnityEngine.Object.Instantiate(lPrefabObject) as GameObject;
		if (lObject == null)
			Debug.LogError(String.Format("can't instantiate resource \"{0}\" as {1}", vPrefabPath, typeof(GameObject).Name));
	}
}
