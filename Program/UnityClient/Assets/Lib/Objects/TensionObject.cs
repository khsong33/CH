using UnityEngine;
using System.Collections;
using System;

public class TensionObject : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Transform vLinkTransform;
	public Vector3 vLinkOffset = Vector3.zero;

	public float vTensionStrength = 4.0f;
	public bool vIsUpdateRotation = true;
	public bool vIsAutoUpdate = true;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		mTransform = transform;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		if (vLinkTransform == null)
			Debug.LogError("vLinkTransform is empty - " + this);
			
		mTensionPosition = mTransform.position;
		mTentionRotation = mTransform.rotation;
// 		mTensionVelocity = Vector3.zero;
//		mTensionAccel = Vector3.zero;

// 		if (vIsAutoUpdate)
// 			StartCoroutine(_UpdateTension());
	}
	//-------------------------------------------------------------------------------------------------------
	// 늦은 갱신 콜백
	void LateUpdate()
	{
		if (vIsAutoUpdate)
			UpdateTension(Time.deltaTime);
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 관성 연결을 갱신합니다.
	void UpdateTension(float pUpdateDelay)
	{
		Vector3 lLinkPosition = vLinkTransform.position + vLinkTransform.rotation * vLinkOffset;

		float lLerpFactor = vTensionStrength * pUpdateDelay;
		mTensionPosition = Vector3.Lerp(mTensionPosition, lLinkPosition, lLerpFactor);
		mTransform.position = mTensionPosition;
		
		if (vIsUpdateRotation)
		{
			mTentionRotation = Quaternion.Lerp(mTentionRotation, vLinkTransform.rotation, lLerpFactor);
			mTransform.rotation = mTentionRotation;
		}
		
// 		Vector3 lLinkVelocity = mTensionPosition - mTransform.position;
// 		Vector3 lNewTensionVelocity = Vector3.Lerp(mTensionVelocity, lLinkVelocity, lLerpFactor);
// 		mTensionAccel = lNewTensionVelocity - mTensionVelocity;
// 		//Debug.Log("mTensionAccel * 1000.0f=" + (mTensionAccel * 1000.0f));
// 		
// 		Vector3 lAccelPoint = mTensionPosition + Vector3.up * 2.0f;
// 		lAccelPoint += mTensionAccel * 1000.0f;
// 		Vector3 lUpwardVector = mTransform.position + lAccelPoint;
// 		Vector3 lForwardVector = vLinkTransform.rotation * Vector3.forward;
		
// 		mTransform.rotation = Quaternion.LookRotation(lForwardVector, lUpwardVector);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	private IEnumerator _UpdateTension()
	{
		for (; ; )
		{
			UpdateTension(Time.deltaTime);
			
			yield return null;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mTensionPosition;
	private Quaternion mTentionRotation;
// 	private Vector3 mTensionVelocity;
// 	private Vector3 mTensionAccel;
}
