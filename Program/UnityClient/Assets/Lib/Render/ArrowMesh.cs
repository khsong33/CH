using UnityEngine;
using System.Collections;
using System;

public class ArrowMesh : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public Animation vArrowAnimation;
	public float vArrowLength = 20.0f;
	public float vArrowMinLength = 5.0f;
	public float vArrowCurveAngleLimit = 30.0f;
	public float vArrowBendRotationFactor = 0.5f;
	public float vArrowBendScaleFactor = 0.5f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		if (vArrowAnimation == null)
		{
			vArrowAnimation = GetComponent<Animation>();
			if (vArrowAnimation == null)
				Debug.LogError("vArrowAnimation is empty. - " + this);
		}

		mTransform = transform;
		mBaseScale = mTransform.localScale;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		vArrowAnimation.Play(PlayMode.StopSameLayer);
		mAnimationState = vArrowAnimation[vArrowAnimation.clip.name];
		mAnimationState.speed = 0.0f;
		mAnimationState.normalizedTime = 0.5f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 화살표의 시작점에서의 방향과 시작점, 끝점을 지정합니다.
	public void SetArrow(Vector3 pStartPosition, Vector3 pEndPosition, float pStartDirectionY)
	{
		//Debug.Log("SetArrow() pStartPosition=" + pStartPosition + " pEndPosition=" + pEndPosition + " pStartDirectionY=" + pStartDirectionY);
		
		Vector3 lArrowVector = pEndPosition - pStartPosition;
		float lArrowVectorDirectionY = MathUtil.GetDirectionY(lArrowVector);

		// 화살표가 시작점과 끝점을 연결하도록 스케일을 계산합니다.
		float lArrowVectorSize = lArrowVector.magnitude;

		if (lArrowVectorSize < vArrowMinLength)
		{
			if (gameObject.activeInHierarchy)
				gameObject.SetActive(false);
			return;
		}
		else if (!gameObject.activeInHierarchy)
			gameObject.SetActive(true);
		
		float lArrowScale = Mathf.Max(lArrowVectorSize / vArrowLength, 1.0f);
		//Debug.Log("lArrowVectorSize=" + lArrowVectorSize + " lArrowScale=" + lArrowScale);

		// 스케일에 따라 위치를 얻습니다.
		Vector3 lArrowPosition;
		if (lArrowScale <= 1.0f)
		{
			// 화살표의 최소 크기를 감안해서 위치를 조정합니다.
			lArrowPosition
				= pStartPosition
				+ Quaternion.Euler(0.0f, lArrowVectorDirectionY, 0.0f) * new Vector3(0.0f, 0.0f, -vArrowLength);
		}
		else
		{
			// 화살표의 끝을 마지막점에 둡니다.
			lArrowPosition = pEndPosition;
		}

		// 화살표의 회전 방향을 계산합니다.
		float lArrowDirectionY;
		float lBendFactor;
		float lAngleVector = MathUtil.DegreeVector(pStartDirectionY, lArrowVectorDirectionY);
		if (Mathf.Abs(lAngleVector) < vArrowCurveAngleLimit)
		{
			lArrowDirectionY = lArrowVectorDirectionY;

			// 화살표의 시작 방향에서 벗어난 정도에 따라 화살표를 휩니다.
			lBendFactor = lAngleVector / vArrowCurveAngleLimit;
		}
		else
		{
			lArrowDirectionY = lArrowVectorDirectionY;

			// 화살표를 최대로 휩니다.
			if (lAngleVector > 0.0f)
				lBendFactor = 1.0f;
			else
				lBendFactor = -1.0f;
		}

		// 화살표 변환을 합니다.
		mTransform.position = lArrowPosition;
		mTransform.rotation = Quaternion.Euler(
			0.0f,
			lArrowDirectionY + vArrowCurveAngleLimit * lBendFactor * vArrowBendRotationFactor,
			0.0f);
		mTransform.localScale = new Vector3(
			mBaseScale.x * lArrowScale,
			mBaseScale.y,
			mBaseScale.z * (lArrowScale +  + Mathf.Abs(lBendFactor) * vArrowBendScaleFactor));
		
		// 애니를 조정해서 화살표가 휘게 합니다(0:왼쪽, 1:오른쪽).
		vArrowAnimation.Play(PlayMode.StopSameLayer);
		mAnimationState = vArrowAnimation[vArrowAnimation.clip.name];
		mAnimationState.speed = 0.0f;
		mAnimationState.normalizedTime = (lBendFactor + 1.0f) * 0.5f;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Transform mTransform;
	private Vector3 mBaseScale;
	private AnimationState mAnimationState;
}
