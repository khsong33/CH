using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


[CustomEditor(typeof(PrefabLightmapData))]
public class PrefabLightmapDataInspector : Editor
{
    public override void OnInspectorGUI()
    {
        var pld = (PrefabLightmapData)target;

        DrawDefaultInspector();

        if (GUILayout.Button("Browse Save Folder..."))
        {
            string path = EditorUtility.OpenFolderPanel("저장위치를 고르세요", "", "");
            if (!string.IsNullOrEmpty(path))
            {
                pld.saveFolder = path.Replace(Application.dataPath + "/", "");
                pld.saveFolder += "/";
            }
        }

        GUILayout.Label("global maps");
        GUILayout.BeginHorizontal();
        foreach (var lm in LightmapSettings.lightmaps)
        {
            EditorGUILayout.LabelField(new GUIContent(lm.lightmapFar), GUILayout.Width(60), GUILayout.Height(60));
            //EditorGUILayout.LabelField(new GUIContent(lm.lightmapNear), GUILayout.Width(60), GUILayout.Height(60));
        }
        GUILayout.EndHorizontal();
        GUILayout.Label("my maps");
        GUILayout.BeginHorizontal();
        foreach (var lm in pld.m_Lightmaps)
        {
            EditorGUILayout.LabelField(new GUIContent(lm), GUILayout.Width(60), GUILayout.Height(60));
        }
        GUILayout.EndHorizontal();
    }
}