using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[ExecuteInEditMode]
public class PrefabLightmapData : MonoBehaviour
{
    public int rendererInfoCount = 0;
	[SerializeField]
	public Texture2D[] m_Lightmaps;
	public LightmapsMode m_LightmapsMode;
    public string saveFolder = "";

    void Awake()
	{
		if (rendererInfoCount == 0)
			return;

        var lightmaps = LightmapSettings.lightmaps;
		var combinedLightmaps = new LightmapData[lightmaps.Length + m_Lightmaps.Length];

		lightmaps.CopyTo(combinedLightmaps, 0);
		for (int i = 0; i < m_Lightmaps.Length; i++)
		{
			combinedLightmaps[i + lightmaps.Length] = new LightmapData();
			combinedLightmaps[i + lightmaps.Length].lightmapFar = m_Lightmaps[i];
		}

		ApplyRendererInfo(lightmaps.Length);
		LightmapSettings.lightmapsMode = m_LightmapsMode;
		LightmapSettings.lightmaps = combinedLightmaps;
    }

	void ApplyRendererInfo(int lightmapOffsetIndex)
	{
        var renderers = transform.GetComponentsInChildren<MeshRenderer>();
        foreach (var renderer in renderers)
        {
            var lightmapInfo = renderer.GetComponent<LightmapInfo>();
			if (lightmapInfo == null)
			{
				//Debug.LogWarning("non-LightmapInfo renderer in " + renderer);
				continue;
			}
            lightmapInfo.Apply(renderer, lightmapOffsetIndex);
        }
    }

#if UNITY_EDITOR
    public void GenerateLightmapInfo()
    {
        var data = transform.GetComponent<PrefabLightmapData>();
        rendererInfoCount = 0;

        var lightmaps = new List<Texture2D>();

        GenerateLightmapInfo(gameObject, lightmaps);
        RemoveCollider(gameObject);

        data.m_Lightmaps = lightmaps.ToArray();
        data.m_LightmapsMode = LightmapSettings.lightmapsMode;
    }

    void GenerateLightmapInfo(GameObject root, List<Texture2D> lightmaps)
	{
		var renderers = root.GetComponentsInChildren<MeshRenderer>();
		foreach (MeshRenderer renderer in renderers)
		{
			if (renderer.lightmapIndex != -1)
			{
                Texture2D lightmap = LightmapSettings.lightmaps[renderer.lightmapIndex].lightmapFar;
                int lightmapIndex = lightmaps.IndexOf(lightmap);
				if (lightmapIndex == -1)
				{
                    lightmapIndex = lightmaps.Count;
					lightmaps.Add(lightmap);
				}
                rendererInfoCount++;

                if (renderer.gameObject.GetComponent<LightmapInfo>() == null)
                    renderer.gameObject.AddComponent<LightmapInfo>();
                renderer.gameObject.GetComponent<LightmapInfo>().localLightmapIndex = lightmapIndex;
                renderer.gameObject.GetComponent<LightmapInfo>().lightmapScaleOffset = renderer.lightmapScaleOffset;
            }
        }
	}
	static void RemoveCollider(GameObject root)
	{
		var colliders = root.GetComponentsInChildren<Collider>();
		foreach (Collider collider in colliders)
		{
			collider.enabled = false;
		}
	}
#endif
}
