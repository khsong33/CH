using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Sound : MonoBehaviour
{
	private class FxPlayLog
	{
		public AudioClip mAudioClip;
		public float mLastPlayTime;
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public AudioSource vBgmAudio;
	public AudioSource vFxAudio;
	public AudioSource vUiAudio;
	
	public float vFxOverlapDelay = 0.1f;
	
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static Sound main
	{
		get
		{
			if (sInstance == null)
				CreateSingletonObject();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 마스터 볼륨
	public float aMasterVolume
	{
		get { return mMasterVolume; }
		set
		{
			mMasterVolume = value;
			_UpdateBgmVolume();
			_UpdateFxVolume();
			_UpdateUiVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음 볼륨
	public float aBgmVolume
	{
		get { return mBgmVolume; }
		set
		{
			mBgmVolume = value;
			_UpdateBgmVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과음 볼륨
	public float aFxVolume
	{
		get { return mFxVolume; }
		set
		{
			mFxVolume = value;
			_UpdateFxVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 볼륨
	public float aUiVolume
	{
		get { return mUiVolume; }
		set
		{
			mUiVolume = value;
			_UpdateUiVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 마스터 음소거
	public bool aIsMuteMaster
	{
		get { return mIsMuteMaster; }
		set
		{
			mIsMuteMaster = value;
			_UpdateBgmVolume();
			_UpdateFxVolume();
			_UpdateUiVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음 음소거
	public bool aIsMuteBgm
	{
		get { return mIsMuteBgm; }
		set
		{
			mIsMuteBgm = value;
			_UpdateBgmVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과음 음소거
	public bool aIsMuteFx
	{
		get { return mIsMuteFx; }
		set
		{
			mIsMuteFx = value;
			_UpdateFxVolume();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과음 음소거
	public bool aIsMuteUi
	{
		get { return mIsMuteUi; }
		set
		{
			mIsMuteUi = value;
			_UpdateUiVolume();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + gameObject);

// 		if (vBgmAudio == null)
// 			Debug.LogError("vBgmAudio is empty - " + this);
// 		if (vFxAudio == null)
// 			Debug.LogError("vFxAudio is empty - " + this);
// 		if (vUiAudio == null)
// 			Debug.LogError("vUiAudio is empty - " + this);

		// 싱글톤 처리를 합니다.
		if (sInstance == null)
		{
			OnCreate();
		}
		else if (sInstance != this)
		{
			DestroyObject(gameObject); // 기존 객체가 있으므로 이 객체는 삭제합니다.
			return;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성 콜백
	void OnCreate()
	{
		//Debug.Log("OnCreate() - " + this);

		// 싱글톤을 등록합니다.
		sInstance = this;
		if (Application.isPlaying)
			DontDestroyOnLoad(gameObject); // 이후로 파괴하지 않습니다.

// 		vBgmAudio.loop = true;
// 		vFxAudio.loop = false;
// 		vUiAudio.loop = false;
// 
// 		vBgmAudio.priority = 128;
// 		vFxAudio.priority = 128;
// 		vUiAudio.priority = 0;
		
		mMasterVolume = 1.0f;
		mBgmVolume = 1.0f;
		mFxVolume = 1.0f;
		mMasterVolume = 1.0f;

		mIsMuteMaster = false;
		mIsMuteBgm = false;
		mIsMuteFx = false;
		mIsMuteUi = false;
		
		mFxPlayLogDictionary = new Dictionary<AudioClip, FxPlayLog>();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}
	//-------------------------------------------------------------------------------------------------------
	// 씬 로딩 콜백
	void OnLevelWasLoaded(int pLevel)
	{
		//Debug.Log("OnLevelWasLoaded() pLevel=" + pLevel + " - " + this);

		// 씬이 새로 로딩되면 기존 효과음 플레이 기록을 모두 삭제합니다.
		mFxPlayLogDictionary.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingletonObject()
	{
		if (sInstance == null)
		{
			GameObject lSingletonRoot = GameObject.Find("_Singletons");
			if (lSingletonRoot == null)
			{
				lSingletonRoot = new GameObject("_Singletons");
				DontDestroyOnLoad(lSingletonRoot);
			}
			else
				sInstance = ObjectUtil.FindChild<Sound>(lSingletonRoot.transform, false);

			if (sInstance == null) // ObjectUtil.FindChild<Sound>()로도 못찾았다면 생성합니다.
			{
				sInstance = ResourceUtil.InstantiateComponent<Sound>(Config.get.GetStringValue("SoundPath"));
				sInstance.transform.parent = lSingletonRoot.transform;
			}
			else
				sInstance.OnCreate();

// 			if (!Application.isPlaying) // Editor 실행시에는 Awake()가 아직 호출이 안되므로 명시적으로 호출합니다.
// 				sInstance.OnCreate();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음을 플레이합니다.
	public void PlayBgm(AudioClip pAudioClip)
	{
		vBgmAudio.clip = pAudioClip;
		vBgmAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음을 리소스에서 읽어서 플레이합니다.
	public void PlayBgmResource(String pAudioClipPath)
	{
		AudioClip lAudioClip = Resources.Load(pAudioClipPath) as AudioClip;
		if (lAudioClip == null)
		{
			Debug.LogError("can't load sound:" + pAudioClipPath);
			return;
		}
		vBgmAudio.clip = lAudioClip;
		vBgmAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음을 중지합니다.
	public void StopBgm()
	{
		vBgmAudio.Stop();
		vBgmAudio.clip = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 배경음이 플레이 중인지를 얻습니다.
	public bool IsPlayBgm()
	{
		return vBgmAudio.isPlaying;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 사운드를 플레이합니다. 사운드 소스에 넣지 않고 사용하는 버전. 현재 플레이 중인지 체크되지 않습니다.
	public void PlayFx(AudioClip pAudioClip)
	{
		if (_UpdateFxPlayLog(pAudioClip))
			vFxAudio.PlayOneShot(pAudioClip);
		//else
		//	Debug.Log("skipped pAudioClip=" + pAudioClip);
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 사운드를 리소스에서 읽어서 플레이합니다. 사운드 소스에 넣지 않고 사용하는 버전. 현재 플레이 중인지 체크되지 않습니다.
	public void PlayFxResource(String pAudioClipPath)
	{
		AudioClip lAudioClip = Resources.Load(pAudioClipPath) as AudioClip;
		vFxAudio.PlayOneShot(lAudioClip);
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 효과 사운드를 플레이합니다. 사운드 소스에 넣어서 사용하는 버전. 현재 플레이중인지 체크(IsPlayExclusiveFx)가 가능합니다..
	public void PlayExclusiveFx(AudioClip pAudioClip, bool pIsLoop)
	{
		vFxAudio.clip = pAudioClip;
		vFxAudio.loop = pIsLoop;
		vFxAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 효과 사운드를 리소스에서 읽어서 플레이합니다. 사운드 소스에 넣어서 사용하는 버전. 현재 플레이중인지 체크(IsPlayExclusiveFx)가 가능합니다..
	public void PlayExclusiveFxResource(String pAudioClipPath, bool pIsLoop)
	{
		AudioClip lAudioClip = Resources.Load(pAudioClipPath) as AudioClip;
		if (lAudioClip == null)
		{
			Debug.LogError("can't load sound:" + pAudioClipPath);
			return;
		}
		vFxAudio.clip = lAudioClip;
		vFxAudio.loop = pIsLoop;
		vFxAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 효과음을 중지합니다.
	public void StopExclusiveFx()
	{
		vFxAudio.loop = false;
		vFxAudio.Stop();
		vFxAudio.clip = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과음이 플레이 중인지를 얻습니다.
	public bool IsPlayExclusiveFx()
	{
		return vFxAudio.isPlaying;
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 사운드를 플레이합니다. 사운드 소스에 넣지 않고 사용하는 버전. 현재 플레이 중인지 체크되지 않습니다.
	public void PlayUi(AudioClip pAudioClip)
	{
		vUiAudio.PlayOneShot(pAudioClip);
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 사운드를 리소스에서 읽어서 플레이합니다. 사운드 소스에 넣지 않고 사용하는 버전. 현재 플레이 중인지 체크되지 않습니다.
	public void PlayUiResource(String pAudioClipPath)
	{
		AudioClip lAudioClip = Resources.Load(pAudioClipPath) as AudioClip;
		vUiAudio.PlayOneShot(lAudioClip);
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 UI 사운드를 플레이합니다. 사운드 소스에 넣어서 사용하는 버전. 현재 플레이중인지 체크(IsPlayExclusiveUi)가 가능합니다..
	public void PlayExclusiveUi(AudioClip pAudioClip, bool pIsLoop)
	{
		vUiAudio.clip = pAudioClip;
		vUiAudio.loop = pIsLoop;
		vUiAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 UI 사운드를 리소스에서 읽어서 플레이합니다. 사운드 소스에 넣어서 사용하는 버전. 현재 플레이중인지 체크(IsPlayExclusiveUi)가 가능합니다..
	public void PlayExclusiveUiResource(String pAudioClipPath, bool pIsLoop)
	{
		AudioClip lAudioClip = Resources.Load(pAudioClipPath) as AudioClip;
		if (lAudioClip == null)
		{
			Debug.LogError("can't load sound:" + pAudioClipPath);
			return;
		}
		vUiAudio.clip = lAudioClip;
		vUiAudio.loop = pIsLoop;
		vUiAudio.Play();
	}
	//-------------------------------------------------------------------------------------------------------
	// 독점적인 UI 사운드를 중지합니다.
	public void StopExclusiveUi()
	{
		vUiAudio.loop = false;
		vUiAudio.Stop();
		vUiAudio.clip = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 사운드가 플레이 중인지를 얻습니다.
	public bool IsPlayExclusiveUi()
	{
		return vUiAudio.isPlaying;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 배경음 볼륨을 갱신합니다.
	private void _UpdateBgmVolume()
	{
		vBgmAudio.volume = (!mIsMuteMaster && !mIsMuteBgm) ? (mMasterVolume * mBgmVolume) : 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과음 볼륨을 갱신합니다.
	private void _UpdateFxVolume()
	{
		vFxAudio.volume = (!mIsMuteMaster && !mIsMuteFx) ? (mMasterVolume * mFxVolume) : 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// UI 볼륨을 갱신합니다.
	private void _UpdateUiVolume()
	{
		vUiAudio.volume = (!mIsMuteMaster && !mIsMuteUi) ? (mMasterVolume * mUiVolume) : 0.0f;
	}
	//-------------------------------------------------------------------------------------------------------
	// 효과 사운드 플레이 기록을 갱신하고 플레이 여부를 반환합니다.
	private bool _UpdateFxPlayLog(AudioClip pAudioClip)
	{
		FxPlayLog lFxPlayLog;
		if (!mFxPlayLogDictionary.TryGetValue(pAudioClip, out lFxPlayLog))
		{
			lFxPlayLog = new FxPlayLog();
			lFxPlayLog.mAudioClip = pAudioClip;
			lFxPlayLog.mLastPlayTime = Time.timeSinceLevelLoad;

			mFxPlayLogDictionary.Add(pAudioClip, lFxPlayLog);
			
			return true;
		}
		else
		{
			float lTimeElapsed = Time.timeSinceLevelLoad - lFxPlayLog.mLastPlayTime;
			if (lTimeElapsed >= vFxOverlapDelay)
			{
				lFxPlayLog.mLastPlayTime = Time.timeSinceLevelLoad;
				return true;
			}
			else
			{
				return false;
			}
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Sound sInstance = null;

	private float mMasterVolume;
	private float mBgmVolume;
	private float mFxVolume;
	private float mUiVolume;

	private bool mIsMuteMaster;
	private bool mIsMuteBgm;
	private bool mIsMuteFx;
	private bool mIsMuteUi;

	private Dictionary<AudioClip, FxPlayLog> mFxPlayLogDictionary;
}
