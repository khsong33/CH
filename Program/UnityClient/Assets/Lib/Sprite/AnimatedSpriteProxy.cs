using UnityEngine;
using System.Collections;
using System;

public class AnimatedSpriteProxy : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public tk2dAnimatedSprite vAnimatedSprite;
	public tk2dSpriteAnimation vSpriteAnimation;
	public AssetInfo vSpriteAnimationAssetInfo;
	public int vClipId = 0;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		SetUpAnimatedSprite();
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 애니 스프라이트를 구성합니다.
	public void SetUpAnimatedSprite()
	{
		if (vAnimatedSprite == null)
		{
			if (vSpriteAnimation == null)
			{
				AssetKey lSpriteAnimationAssetKey = AssetManager.get.CreateAssetKey(vSpriteAnimationAssetInfo);
				if (!AssetKey.IsValid(lSpriteAnimationAssetKey))
					Debug.LogError("invalid asset key '" + lSpriteAnimationAssetKey + "' - " + this + " of " + transform.parent.gameObject);
				vSpriteAnimation = AssetManager.get.Load<tk2dSpriteAnimation>(lSpriteAnimationAssetKey);
			}

			vAnimatedSprite = tk2dAnimatedSprite.AddComponent(
				gameObject,
				vSpriteAnimation,
				vClipId);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
