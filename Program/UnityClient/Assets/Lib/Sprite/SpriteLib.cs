using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System;

public class SpriteLib : MonoBehaviour
{
	[Serializable]
	public class ActionClip
	{
		public String vName;
		public bool vIsFlipped = false;

		public tk2dSpriteAnimationClip aAnimationClip
		{
			get { return mAnimationClip; }
		}
		public float aDirectionY
		{
			get { return mDirectionY; }
		}

		internal tk2dSpriteAnimationClip mAnimationClip;
		internal float mDirectionY;
	}

	[Serializable]
	public class Action
	{
		public String vName;
		public int vActionIdx;
		public ActionClip[] vDirectionClips;
		
		internal int mDirectionCount;
		internal float mDirectionYSnap;
		internal float mDirectionYSnapHalf;

		// 방향을 가지고 애니메이션 클립의 인덱스를 얻습니다.
		public int GetDirectionIndex(float pSpriteDeltaAngle)
		{
			float lSpriteDeltaAngleAbs = pSpriteDeltaAngle - (int)(pSpriteDeltaAngle / 360.0f) * 360.0f;
			if (lSpriteDeltaAngleAbs < 0.0f)
				lSpriteDeltaAngleAbs += 360.0f;
			//Debug.Log(String.Format("pSpriteDeltaAngle={0}({1})", lSpriteDeltaAngleAbs, pSpriteDeltaAngle));

			int lDirectionIndex
				= (int)((lSpriteDeltaAngleAbs + mDirectionYSnapHalf) / mDirectionYSnap);
			if (lDirectionIndex >= mDirectionCount)
				lDirectionIndex = 0;
			return lDirectionIndex;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public tk2dSpriteAnimation vSpriteAnimation;
	public AssetInfo vSpriteAnimationAssetInfo;
	public Action[] vActions;

	public static String cDefaultShaderName = "tk2d/BlendVertexColorAddTone";
	public static String cAddToneShaderName = "tk2d/BlendVertexColorAddTone";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 액션 딕셔너리
	public Dictionary<String, Action> aActionDictionary
	{
		get { return mActionDictionary; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 기본 쉐이더
	public Shader aDefaultShader
	{
		get { return mDefaultShader; }
	}	
	//-------------------------------------------------------------------------------------------------------
	// 애드 톤 여부
	public bool aIsAddTone
	{
		get { return mIsAddTone; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//Debug.Log("Start() - " + this);

		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 종료 콜백
#if UNITY_EDITOR
	void OnApplicationQuit()
	{
		Free();
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 로딩합니다.
	public void Load()
	{
		//Debug.Log("Load() - " + this);
		//Debug.Log("mIsLoaded=" + mIsLoaded);

		if (_CheckLoaded(this))
			return;
		_SetLoaded(this);
		_Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 해제합니다.
	public void Free()
	{
		//Debug.Log("Free() - " + this);

		if (!_CheckLoaded(this))
			return;
		_ResetLoaded(this);
		_Free();
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 로딩합니다.
	private void _Load()
	{
		//Debug.Log("_Load() - " + this);

		if (vSpriteAnimation == null)
		{
			AssetKey lSpriteAnimationAssetKey = AssetManager.get.CreateAssetKey(vSpriteAnimationAssetInfo);
			if (!AssetKey.IsValid(lSpriteAnimationAssetKey))
				Debug.LogError("invalid asset key '" + lSpriteAnimationAssetKey + "' - " + this);
			vSpriteAnimation = AssetManager.get.Load<tk2dSpriteAnimation>(lSpriteAnimationAssetKey);
		}
		if (vActions.Length == 0)
			Debug.LogError("vActions is empty" + this);
			
		mActionDictionary = new Dictionary<String, Action>();
		for (int iAction = 0; iAction < vActions.Length; ++iAction)
		{
			Action lAction = vActions[iAction];

			lAction.vActionIdx = iAction;
			if (mActionDictionary.ContainsKey(lAction.vName))
				Debug.LogError(String.Format("vActions[{0}] duplicate name '{1}' - {2}", iAction, lAction.vName, gameObject));
			mActionDictionary.Add(lAction.vName, lAction);

			lAction.mDirectionCount = lAction.vDirectionClips.Length;
			if (lAction.mDirectionCount == 0)
				Debug.LogError(String.Format("vActions[{0}].vDirectionClips is empty - {1}", iAction, gameObject));

			lAction.mDirectionYSnap = 360.0f / lAction.mDirectionCount;
			//Debug.Log("mDirectionYSnap=" + mDirectionYSnap);
			lAction.mDirectionYSnapHalf = lAction.mDirectionYSnap * 0.5f;
				
			for (int iDirection = 0; iDirection < lAction.mDirectionCount; ++iDirection)
			{
				ActionClip lActionClip = lAction.vDirectionClips[iDirection];

				lActionClip.mAnimationClip = vSpriteAnimation.GetClipByName(lActionClip.vName);
				if (lActionClip.mAnimationClip == null)
					Debug.LogError(String.Format("can't find vDirectionClip[{0}] '{1}' in vActions[{2}] - {3}", iDirection, lAction.vDirectionClips[iDirection].vName, iAction, gameObject));
				//Debug.Log(String.Format("vActions[{0}].vDirectionClips[{1}].mAnimationClip={2}", iAction, iDirection, lActionClip.mAnimationClip));
				
				lActionClip.mDirectionY = lAction.mDirectionYSnap * iDirection;
				//Debug.Log("lActionClip.mDirectionY=" + lActionClip.mDirectionY);
			}
		}

		// 톤 쉐이더 검사를 합니다.
		tk2dSpriteCollectionData lSpriteCollection = vSpriteAnimation.clips[0].frames[0].spriteCollection;
		mDefaultShader = lSpriteCollection.materials[0].shader;
		//Debug.Log("mDefaultShader=" + mDefaultShader);
		//if (mDefaultShader == null)
		//{
		//    mDefaultShader = Shader.Find(SpriteLib.cDefaultShaderName);
		//    gameObject.renderer.sharedMaterial.shader = mDefaultShader;
		//}		
		mIsAddTone = (mDefaultShader.name == cAddToneShaderName);
		//Debug.Log("lSpriteCollection.materials[0].shader=" + lSpriteCollection.materials[0].shader);
		//if (mIsAddTone) Debug.Log("mIsAddTone=" + mIsAddTone + " - " + this);
// 		for (int iMaterial = 1; iMaterial < lSpriteCollection.materials.Length; ++iMaterial)
// 			if (mIsAddTone &&
// 				(lSpriteCollection.materials[iMaterial].shader.name != cAddToneShaderName))
// 				Debug.LogError(String.Format("Sprite Collection materials do not have add tone shader(\"{0}\" - {1}) - ", cAddToneShaderName, this));
	}
	//-------------------------------------------------------------------------------------------------------
	// 라이브러리를 해제합니다.
	private void _Free()
	{
		mActionDictionary.Clear();
	}
	//-------------------------------------------------------------------------------------------------------
	// 로딩된 스프라이트 라이브러리 색인 사전을 얻습니다.
	private static bool _CheckLoaded(SpriteLib pSpriteLib)
	{
		if (sSpriteLibDictionary == null)
			sSpriteLibDictionary = new Dictionary<SpriteLib, SpriteLib>();

		SpriteLib lSpriteLib;
		return sSpriteLibDictionary.TryGetValue(pSpriteLib, out lSpriteLib);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 라이브러리를 로딩합니다.
	public static void _SetLoaded(SpriteLib pSpriteLib)
	{
		sSpriteLibDictionary.Add(pSpriteLib, pSpriteLib);
	}
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 라이브러리를 삭제합니다.
	public static void _ResetLoaded(SpriteLib pSpriteLib)
	{
		sSpriteLibDictionary.Remove(pSpriteLib);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Dictionary<SpriteLib, SpriteLib> sSpriteLibDictionary = null;

	private Dictionary<String, Action> mActionDictionary;
	private Shader mDefaultShader;
	private bool mIsAddTone;
}
