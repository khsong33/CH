using UnityEngine;
using System.Collections;
using System;

[ExecuteInEditMode()]
public class SpriteObject : MonoBehaviour
{
	public enum UpdateMask
	{
		None = 0x0000,

		Action = 0x0001,
		Pivot = 0x0002,
		Camera = 0x0004,
		Direction = 0x0008,
		
		All = 0xffff,
	}
	public enum BillboardType
	{
		TowardCamera,
		YAxis,
		YAxisCameraPitchCorrection,
	}
	
	public const String cNullShader = "Hidden/InternalErrorShader";

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public SpriteLib vSpriteLib;
	public tk2dSpriteAnimator vSpriteAnimator;
	public bool vIsLoadSpriteInEditor = true;
	public int vActionIdx = 0;
	public Transform vPivotTransform;
	public SpriteObject vPivotSpriteObject;
	public Vector3 vPivotOffset = Vector3.zero;
	public Transform vCameraTransform;
	public float vViewOffset = 0.0f;
	
	public float vDirectionY = 0.0f;
	public bool vIsLocalDirection = true;
	public bool vIsUpdateCameraRotation = false;
	public BillboardType vBillboardType = BillboardType.TowardCamera;
	
	public Transform[] vUnflippableChilds;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 스프라이트 Y축 방향각
	public float aSpriteDirectionY
	{
		get { return mCurrentSpriteDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 로컬 Y축 방향각
	public float aLocalDirectionY
	{
		get { return mCurrentLocalDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 글로벌 Y축 방향각
	public float aGlobalDirectionY
	{
		get { return mCurrentGlobalDirectionY; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 색상 톤
	public Color aToneColor
	{
		get { return mToneColor; }
		set
		{
			mToneColor = value;

			if (vSpriteLib.aIsAddTone)
				vSpriteAnimator.Sprite.color = new Color(
					mToneColor.r,
					mToneColor.g,
					mToneColor.b,
					mAlpha);
			else
				vSpriteAnimator.Sprite.color = new Color(
					mToneColor.r * 2.0f,
					mToneColor.g * 2.0f,
					mToneColor.b * 2.0f,
					mAlpha);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 투명도
	public float aAlpha
	{
		get { return mAlpha; }
		set
		{
			mAlpha = value;

			Color lSpriteColor = vSpriteAnimator.Sprite.color;
			lSpriteColor.a = value;
			vSpriteAnimator.Sprite.color = lSpriteColor;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 플리핑 여부
	public bool aIsFlipped
	{
		get { return mIsFlipped; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//if (Application.isPlaying) Debug.Log("Awake() - " + this);
	
		if (vSpriteLib == null)
			Debug.LogError("vSpriteLib is empty" + this);

		vSpriteLib.Load(); // 스프라이트를 로딩합니다.

		mSpriteTransform = vSpriteAnimator.transform;

		if (Application.isPlaying || vIsLoadSpriteInEditor)
		{
			//Debug.Log("vSpriteAnimator=" + vSpriteAnimator + " vSpriteLib.vSpriteAnimation=" + vSpriteLib.vSpriteAnimation + " - " + this);
			if (vSpriteAnimator == null)
			{
				tk2dSprite lSprite = GetComponent<tk2dSprite>();
				if (lSprite == null)
				{
					tk2dSpriteAnimationClip lSpriteAnimationClip = vSpriteLib.vSpriteAnimation.clips[0];
					tk2dSpriteCollectionData lSpriteCollectionData = lSpriteAnimationClip.frames[0].spriteCollection;				
					lSprite = tk2dSprite.AddComponent<tk2dSprite>(
						gameObject,
						lSpriteCollectionData,
						0);
				}

				vSpriteAnimator = GetComponent<tk2dSpriteAnimator>();
				if (vSpriteAnimator == null)
				{
					vSpriteAnimator = tk2dSpriteAnimator.AddComponent(
						gameObject,
						vSpriteLib.vSpriteAnimation,
						0);
				}
			}

			if (vSpriteLib.vSpriteAnimation != vSpriteAnimator.Library)
				Debug.LogError("vSpriteAnimator mismatch: vSpriteLib.vSpriteAnimation=" + vSpriteLib.vSpriteAnimation + " vSpriteAnimator.Library=" + vSpriteAnimator.Library + " - " + transform.parent);
			
		}
		else
		{
			//Debug.Log("vSpriteAnimator=" + vSpriteAnimator + " vSpriteLib.vSpriteAnimation=" + vSpriteLib.vSpriteAnimation + " - " + this);
		}

		// 카메라 각도에 따른 늘리기 처리를 초기화합니다.
		if ((vBillboardType == BillboardType.YAxisCameraPitchCorrection) &&
			Application.isPlaying)
		{
			mIsYAxisCameraPitchCorrection = true;
			mCameraPitchCorrectionBaseScale = mSpriteTransform.localScale;
		}
		else
			mIsYAxisCameraPitchCorrection = false;

		// 카메라를 확보합니다.
		//Debug.Log("vCameraTransform=" + vCameraTransform);
		if (vCameraTransform == null)
		{
			//Debug.Log("Camera.mainCamera=" + Camera.mainCamera);
			vCameraTransform = Camera.main.transform;
		}
		
		mIsActivated = false;
		//Debug.Log("Awake() mIsActivated=" + mIsActivated + " - " + this);
		mIsStarted = false;
		
		mIsUpdatingCameraRotation = false;

		_Activate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		//if (Application.isPlaying) Debug.Log("Start() - " + this);

		mIsStarted = true;
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		//if (Application.isPlaying) Debug.Log("OnEnable() - " + this);

		if (mIsStarted)
			_Activate();
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		//if (Application.isPlaying) Debug.Log("OnDisable() - " + this);

		mIsUpdatingCameraRotation = false; // 갱신 코루틴이 실행중이었다면 중단되므로
		
		mIsActivated = false;
		//Debug.Log("OnDisable() mIsActivated=" + mIsActivated + " - " + this);
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백(에디터에서만)
#if UNITY_EDITOR
	void Update()
	{
		if (Application.isPlaying)
		{
			// 게임 실행 중이라면 카메라 회전 갱신 변화만 반영합니다.
			if (vIsUpdateCameraRotation &&
				!mIsUpdatingCameraRotation)
				StartCoroutine(_UpdateCameraRotation());
		}
		else
		{
			// 게임 실행 중이 아닐 때에는 에디터 인스펙터에서의 변화를 계속 반영합니다.
			if (vSpriteAnimator != null)
				UpdateChange(UpdateMask.All);
		}
	}
#endif

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 애니를 멈춤니다.
	public void PauseAnimation()
	{
		vSpriteAnimator.Pause();
	}
	//-------------------------------------------------------------------------------------------------------
	// 애니를 계속합니다.
	public void ResumeAnimation()
	{
		vSpriteAnimator.Resume();
	}
	//-------------------------------------------------------------------------------------------------------
	// 변화를 갱신합니다.
	public void UpdateChange(UpdateMask pUpdateMask)
	{
		//Debug.Log("UpdateChange() pUpdateMask=" + pUpdateMask + " - " + vSpriteAnimator);
	
		bool lIsSetActionClip = false;
		bool lIsSetNewAction = false;
		bool lIsSetSpritePosition = false;
		bool lIsSetSpriteRotation = false;
		bool lIsSetSpriteDirectionId = false;

		if ((pUpdateMask & UpdateMask.Action) == UpdateMask.Action)
		{
			if (vActionIdx >= 0)
			{
				SpriteLib.Action lNewAction = vSpriteLib.vActions[vActionIdx];
				if (mCurrentAction != lNewAction)
				{
					mCurrentAction = lNewAction;
					//Debug.Log("mCurrentAction=" + mCurrentAction + " vActionIdx=" + vActionIdx + " - " + this);

					lIsSetActionClip = true;
					lIsSetNewAction = true;
				}
			}
		}

		if ((pUpdateMask & UpdateMask.Camera) == UpdateMask.Camera)
		{
			if ((vViewOffset != 0.0f) &&
				(vPivotTransform != null))
			{
				mCurrentViewOffset = vCameraTransform.rotation * new Vector3(0.0f, 0.0f, vViewOffset);
				//Debug.Log("mCurrentViewOffset=" + mCurrentViewOffset);

				lIsSetSpritePosition = true;
				//Debug.Log("1 lIsSetSpritePosition=" + lIsSetSpritePosition);
			}

			mCurrentCameraDirectionAngles = vCameraTransform.rotation.eulerAngles;

			lIsSetSpriteRotation = true;
			lIsSetSpriteDirectionId = true;
		}

		if (((pUpdateMask & UpdateMask.Pivot) == UpdateMask.Pivot) &&
			(vPivotTransform != null))
		{
			mCurrentPivotDirectionY = vPivotTransform.rotation.eulerAngles.y;
			//Debug.Log("mCurrentPivotDirectionY=" + mCurrentPivotDirectionY);

			lIsSetSpritePosition = true;
			//Debug.Log("2 lIsSetSpritePosition=" + lIsSetSpritePosition);
			lIsSetSpriteRotation = true;
			lIsSetSpriteDirectionId = true;
		}

		if (lIsSetSpriteDirectionId ||
			(pUpdateMask & UpdateMask.Direction) == UpdateMask.Direction)
		{
			//Debug.Log("vPivotTransform=" + vPivotTransform);
			if (vIsLocalDirection &&
				(vPivotTransform != null))
				mCurrentSpriteDirectionY = mCurrentPivotDirectionY + vDirectionY;
			else
				mCurrentSpriteDirectionY = vDirectionY;
			//Debug.Log("mCurrentSpriteDirectionY=" + mCurrentSpriteDirectionY);
			
			lIsSetSpriteDirectionId = true;
		}

		if (lIsSetSpriteDirectionId)
		{
			int lNewSpriteDirectionId
				= mCurrentAction.GetDirectionIndex(mCurrentSpriteDirectionY - mCurrentCameraDirectionAngles.y);
			//Debug.Log(String.Format("pSpriteDirectionY={0} mCurrentCameraDirectionAngles={1} lNewSpriteDirectionId={2}", pSpriteDirectionY, mCurrentCameraDirectionAngles, lNewSpriteDirectionId));
			if (mCurrentSpriteDirectionId != lNewSpriteDirectionId)
			{
				mCurrentSpriteDirectionId = lNewSpriteDirectionId;

				if (vPivotTransform != null)
				{
					lIsSetSpritePosition = true; // 본체 회전에 따라 피봇도 같이 회전을 해주어야 하므로
					//Debug.Log("3 lIsSetSpritePosition=" + lIsSetSpritePosition);
				}

				lIsSetActionClip = true;
			}
		}

		if (lIsSetActionClip)
		{
			SpriteLib.ActionClip lActionClip = mCurrentAction.vDirectionClips[mCurrentSpriteDirectionId];
			_SetActionClip(lActionClip, lIsSetNewAction);

			mCurrentLocalDirectionY = lActionClip.aDirectionY;
			mCurrentGlobalDirectionY = mCurrentLocalDirectionY + mCurrentCameraDirectionAngles.y;
		}

		if (lIsSetSpritePosition) // 이 조건이면 (vPivotTransform != null)은 보장됩니다.
		{
			Vector3 lSpritePosition = vPivotTransform.position;

			if (vPivotSpriteObject != null)
			{
				mFlippingPivotOffset.x = !vPivotSpriteObject.aIsFlipped ? vPivotOffset.x : -vPivotOffset.x;
				//Debug.Log("mFlippingPivotOffset=" + mFlippingPivotOffset);

				lSpritePosition
					+= Quaternion.Euler(
						0.0f,
						vPivotSpriteObject.mCurrentGlobalDirectionY,
						0.0f)
					* mFlippingPivotOffset;
			}
			else
			{
				lSpritePosition += vPivotTransform.rotation * vPivotOffset;
			}

			if (vViewOffset != 0.0f)
				lSpritePosition += mCurrentViewOffset;

			mSpriteTransform.position = lSpritePosition;
		}

		if (lIsSetSpriteRotation)
		{
			//Debug.Log("vCameraTransform.rotation=" + vCameraTransform.rotation);
			if (vBillboardType == BillboardType.TowardCamera)
				mSpriteTransform.rotation = vCameraTransform.rotation;
			else
				mSpriteTransform.rotation = Quaternion.Euler(0.0f, mCurrentCameraDirectionAngles.y, 0.0f);

			if (mIsYAxisCameraPitchCorrection)
			{
				mSpriteTransform.localScale = new Vector3(
					mCameraPitchCorrectionBaseScale.x,
					mCameraPitchCorrectionBaseScale.y / Mathf.Cos(mCurrentCameraDirectionAngles.x * Mathf.Deg2Rad),
					mCameraPitchCorrectionBaseScale.z);
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 플리핑 하지 않는 자식 노드를 추가합니다(추가만 가능하며 이후로는 자식 노드가 삭제되면 안됩니다).
	public void AddUnflippableChild(Transform pUnflippableChild, int pMaxChild)
	{
		int lNewLength = Mathf.Max(vUnflippableChilds.Length + 1, pMaxChild);
		if (vUnflippableChilds == null)
		{
			vUnflippableChilds = new Transform[lNewLength];
			vUnflippableChilds[0] = pUnflippableChild;
		}
		else
		{
			Transform[] lOldChilds = vUnflippableChilds;
			vUnflippableChilds = new Transform[lNewLength];
			for (int iChild = 0; iChild < lOldChilds.Length; ++iChild)
				vUnflippableChilds[iChild] = lOldChilds[iChild];
			vUnflippableChilds[lOldChilds.Length] = pUnflippableChild;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 카메라 방향 갱신 여부를 지정합니다.
	public void EnableUpdateCameraRotation(bool pIsUpdateCameraRotation)
	{
		vIsUpdateCameraRotation = pIsUpdateCameraRotation;

		if (vIsUpdateCameraRotation &&
			!mIsUpdatingCameraRotation)
			StartCoroutine(_UpdateCameraRotation());
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 활성화합니다.
	private void _Activate()
	{
		//if (Application.isPlaying) Debug.Log("_Activate() mIsActivated=" + mIsActivated + " - " + this);
	
		if (mIsActivated)
			return;
	
		// 상태를 초기화합니다.
		mIsFlipped = false;
		mFlippingPivotOffset = vPivotOffset;

		// 실제 스프라이트가 있을 때의 활성화 처리를 합니다.
		//if (Application.isPlaying) Debug.Log("vSpriteAnimator=" + vSpriteAnimator);
		if (vSpriteAnimator != null)
		{
			//Debug.Log("mSpriteTransform=" + mSpriteTransform);

			aToneColor = mToneColor;

			if ((vPivotSpriteObject != null) &&
				!vPivotSpriteObject.mIsActivated)
				vPivotSpriteObject._Activate(); // 기반이 되는 스프라이트가 있다면 그 스프라이트를 먼저 활성화합니다.

			UpdateChange(UpdateMask.All);
		}
		
		mIsActivated = true;
		//Debug.Log("_Activate() mIsActivated=" + mIsActivated + " - " + this);

		if ((vSpriteAnimator != null) &&
			vIsUpdateCameraRotation)
			StartCoroutine(_UpdateCameraRotation());

		// 기본 쉐이더 처리(Missing shader 처리 필요)
	#if UNITY_EDITOR
		Shader lDefaultShader = GetComponent<Renderer>().sharedMaterial.shader;
		//if (lDefaultShader == null)
		if (String.Compare(lDefaultShader.name, cNullShader) == 0)
		{
			lDefaultShader = Shader.Find(SpriteLib.cDefaultShaderName);
			GetComponent<Renderer>().sharedMaterial.shader = lDefaultShader;
		}
	#endif
	}
	//-------------------------------------------------------------------------------------------------------
	// [코루틴] 카메라 방향 갱신을 계속합니다.
	private IEnumerator _UpdateCameraRotation()
	{
		Debug.Log("_UpdateCameraRotation() - " + this);

		mIsUpdatingCameraRotation = true;
		
		yield return null; // Start() 이후에 호출되도록 한 프레임 쉽니다.
	
		for (;;)
		{
			// 카메라 변화를 갱신합니다.
			UpdateChange(UpdateMask.Camera);

			// 외부에서 갱신 플래그가 꺼졌다면 갱신을 그만둡니다.
			if (!vIsUpdateCameraRotation)
			{
				mIsUpdatingCameraRotation = false;
				yield break;
			}
			
			yield return null;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 액션 클립을 지정합니다.
	private void _SetActionClip(SpriteLib.ActionClip pActionClip, bool pSetNewAction)
	{
		mIsFlipped = pActionClip.vIsFlipped;
		if (!mIsFlipped)
		{
			//Debug.Log("mSpriteTransform=" + mSpriteTransform + " - " + this);
			if (mSpriteTransform.localScale.x < 0.0f)
				mSpriteTransform.localScale = new Vector3(
					-mSpriteTransform.localScale.x,
					mSpriteTransform.localScale.y,
					mSpriteTransform.localScale.z);

			if (vUnflippableChilds != null)
				for (int iChild = 0; iChild < vUnflippableChilds.Length; ++iChild)
					if (vUnflippableChilds[iChild].localScale.x < 0.0f)
						vUnflippableChilds[iChild].localScale = new Vector3(
							-vUnflippableChilds[iChild].localScale.x,
							vUnflippableChilds[iChild].localScale.y,
							vUnflippableChilds[iChild].localScale.z);
		}
		else
		{
			if (mSpriteTransform.localScale.x > 0.0f)
				mSpriteTransform.localScale = new Vector3(
					-mSpriteTransform.localScale.x,
					mSpriteTransform.localScale.y,
					mSpriteTransform.localScale.z);

			if (vUnflippableChilds != null)
				for (int iChild = 0; iChild < vUnflippableChilds.Length; ++iChild)
					if (vUnflippableChilds[iChild].localScale.x > 0.0f)
						vUnflippableChilds[iChild].localScale = new Vector3(
							-vUnflippableChilds[iChild].localScale.x,
							vUnflippableChilds[iChild].localScale.y,
							vUnflippableChilds[iChild].localScale.z);
		}

		//Debug.Log("vSpriteAnimator=" + vSpriteAnimator);
		//if (vSpriteAnimator.Playing)
		//	Debug.Log("vSpriteAnimator.ClipTimeSeconds=" + vSpriteAnimator.ClipTimeSeconds);
		float lClipTimeSeconds
			= (!pSetNewAction && vSpriteAnimator.Playing)
			? vSpriteAnimator.ClipTimeSeconds : 0.0f;
		vSpriteAnimator.Play(pActionClip.aAnimationClip, lClipTimeSeconds, tk2dSpriteAnimator.DefaultFps);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private bool mIsActivated;
	private bool mIsStarted;
	
	private Transform mSpriteTransform;
	private SpriteLib.Action mCurrentAction;
	private Vector3 mCurrentViewOffset;
	private float mCurrentSpriteDirectionY;
	private float mCurrentLocalDirectionY;
	private float mCurrentGlobalDirectionY;
	private int mCurrentSpriteDirectionId;
	private float mCurrentPivotDirectionY;
	private Vector3 mCurrentCameraDirectionAngles;
	private bool mIsFlipped;
	private Vector3 mFlippingPivotOffset;
	private bool mIsYAxisCameraPitchCorrection;
	private Vector3 mCameraPitchCorrectionBaseScale;

	private Color mToneColor = Color.gray;
	private float mAlpha = 1.0f;
	
	private bool mIsUpdatingCameraRotation;
}
