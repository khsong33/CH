using UnityEngine;
using System.Collections;
using System;

public class SpriteScaleUtil : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public float vScale = 1.0f;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Awake()
	{
		mSprite = gameObject.GetComponent<UISprite>();
		if (mSprite == null)
			Debug.LogError("Sprite is empty Error - " + gameObject);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
// 		UIAtlas.Sprite lRealSprite = mSprite.GetAtlasSprite();
// 		mSprite.transform.localScale = new Vector3(lRealSprite.outer.width * vScale,
// 													lRealSprite.outer.height * vScale,
// 													1);
	}
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UISprite mSprite;
}
