using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PlayTime : MonoBehaviour
{
	public const String cPlayTimePath = "PlayTime";

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public float vDebugAccelTimeScale = 10.0f;
	public float vDebugSlowTimeScale = 0.1f;
	public KeyCode vAccelKeyCode = KeyCode.Space;
	public KeyCode vSlowKeyCode = KeyCode.S;
	public KeyCode vSlowAltKeyCode = KeyCode.LeftShift;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static PlayTime get
	{
		get
		{
			if (sInstance == null)
				CreateSingletonObject();
			return sInstance;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간 멈춤 여부
	public bool aIsTimePaused
	{
		get { return mIsTimePaused; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간 가속 여부
	public bool aIsTimeAcceled
	{
		get { return mIsTimeAcceled; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 일반 시간
	public float aNormalTimeScale
	{
		get { return mNormalTimeScale; }
		set
		{
			mNormalTimeScale = value;
			UpdateTimeScale();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 가속 시간
	public float aAccelTimeScale
	{
		get { return (mAccelTimeScale * (mIsDebugTimeAcceled ? mDebugAccelTimeScale : 1.0f)); }
		set
		{
			mAccelTimeScale = value;
			UpdateTimeScale();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 가속키가 눌려졌는지 여부
	public bool aIsAccelKeyPressed
	{
		get { return mIsAccelKeyPressed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 감속키가 눌려졌는지 여부
	public bool aIsSlowKeyPressed
	{
		get { return mIsSlowKeyPressed; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 감속 변형키가 눌려졌는지 여부
	public bool aIsSlowAltKeyPressed
	{
		get { return mIsSlowAltKeyPressed; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 초기화 콜백
	void Awake()
	{
		//Debug.Log("Awake() - " + this);

		// 싱글톤 처리를 합니다.
		if (sInstance == null)
		{
			OnCreate();
		}
		else if (sInstance != this)
		{
			DestroyObject(gameObject); // 기존 객체가 있으므로 이 객체는 삭제합니다.
			return;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성 콜백
	void OnCreate()
	{
		//Debug.Log("OnCreate() - " + this);

		// 싱글톤을 등록합니다.
		sInstance = this;
		if (Application.isPlaying)
			DontDestroyOnLoad(gameObject); // 이후로 파괴하지 않습니다.
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 콜백
	void Update()
	{
		//Debug.Log("Update() - " + this);
	
		// 시간 가속 입력을 처리합니다.
		if (Input.GetKeyDown(vSlowAltKeyCode))
		{
			mIsSlowAltKeyPressed = true;
			if (mIsAccelKeyPressed)
				TimeUtil.DebugAccelTime(vDebugSlowTimeScale);
		}
		else if (Input.GetKeyUp(vSlowAltKeyCode))
		{
			mIsSlowAltKeyPressed = false;
			if (mIsAccelKeyPressed)
				TimeUtil.DebugAccelTime(vDebugAccelTimeScale);
		}
		if (Input.GetKeyDown(vAccelKeyCode))

		{
			mIsAccelKeyPressed = true;
			if (mIsSlowAltKeyPressed)
				TimeUtil.DebugAccelTime(vDebugSlowTimeScale);
			else
				TimeUtil.DebugAccelTime(vDebugAccelTimeScale);
		}
		else if (Input.GetKeyUp(vAccelKeyCode))
		{
			mIsAccelKeyPressed = false;
			TimeUtil.DebugAccelTime(false);
		}

		if (Input.GetKeyDown(vSlowKeyCode))
		{
			mIsSlowKeyPressed = true;
			TimeUtil.DebugAccelTime(vDebugSlowTimeScale);
		}
		else if (Input.GetKeyUp(vSlowKeyCode))
		{
			mIsSlowKeyPressed = false;
			TimeUtil.DebugAccelTime(false);
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체를 생성합니다.
	public static void CreateSingletonObject()
	{
		if (sInstance == null)
		{
			GameObject lSingletonRoot = GameObject.Find("_Singletons");
			if (lSingletonRoot == null)
			{
				lSingletonRoot = new GameObject("_Singletons");
				DontDestroyOnLoad(lSingletonRoot);
			}
			else
				sInstance = ObjectUtil.FindChild<PlayTime>(lSingletonRoot.transform, false);

			if (sInstance == null) // ObjectUtil.FindChild<PlayTime>()로도 못찾았다면 생성합니다.
			{
				sInstance = ResourceUtil.InstantiateComponent<PlayTime>(cPlayTimePath);
				sInstance.transform.parent = lSingletonRoot.transform;
			}
			else
				sInstance.OnCreate();

// 			if (!Application.isPlaying) // Editor 실행시에는 Awake()가 아직 호출이 안되므로 명시적으로 호출합니다.
// 				sInstance.OnCreate();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 멈춥니다.
	public void PauseTime(bool pIsTimePaused)
	{
		mIsTimePaused = pIsTimePaused;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 가속합니다.
	public void AccelTime(float pAccelTimeScale)
	{
		mIsTimeAcceled = true;
		mAccelTimeScale = pAccelTimeScale;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 가속합니다.
	public void AccelTime(bool pIsTimeAcceled)
	{
		mIsTimeAcceled = pIsTimeAcceled;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 시간을 가속합니다.
	public void DebugAccelTime(float pDebugAccelTimeScale)
	{
		mIsDebugTimeAcceled = true;
		mDebugAccelTimeScale = pDebugAccelTimeScale;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 디버그 시간을 가속합니다.
	public void DebugAccelTime(bool pIsDebugTimeAcceled)
	{
		mIsDebugTimeAcceled = pIsDebugTimeAcceled;
		UpdateTimeScale();
	}
	//-------------------------------------------------------------------------------------------------------
	// 시간을 현재 상태로 갱신합니다.
	public void UpdateTimeScale()
	{
		Time.timeScale = mIsTimePaused
			? 0.0f
			: (mIsTimeAcceled ? mAccelTimeScale : mNormalTimeScale) * (mIsDebugTimeAcceled ? mDebugAccelTimeScale : 1.0f);
		//Debug.Log("Time.timeScale=" + Time.timeScale);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static PlayTime sInstance = null;

	private bool mIsTimePaused = false;
	private bool mIsTimeAcceled = false;
	private float mAccelTimeScale = 1.0f;
	private bool mIsDebugTimeAcceled = false;
	private float mDebugAccelTimeScale = 1.0f;
	private float mNormalTimeScale = 1.0f;

	private bool mIsAccelKeyPressed = false;
	private bool mIsSlowKeyPressed = false;
	private bool mIsSlowAltKeyPressed = false;
}
