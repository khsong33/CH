﻿using UnityEngine;
using System.Collections;

public class BillboardObject : MonoBehaviour
{
	public Camera vCamera;
	void Start()
	{
		transform.localScale = new Vector3(-1, 1, -1);
	}

	// Update is called once per frame
	void Update()
	{
		if (vCamera == null)
		{
			return;
		}
		transform.LookAt(vCamera.transform.position, Vector3.up);
	}
}
