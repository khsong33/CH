﻿using System;
using System.Collections;
using System.Collections.Generic;


public class CSVEnumerator : IEnumerator<CSVRow>
{
    private CSVRow[] _elements;
    int position = -1;

    public CSVEnumerator(CSVRow[] rows)
    {
        _elements = rows;
    }
    
    public bool MoveNext()
    {
        if (position < _elements.Length - 1)
        {
            position++;
            return true;
        }
        else
        {
            return false;
        }
    }
    
    // Declare the Reset method required by IEnumerator:
    public void Reset()
    {
        position = -1;
    }

    void IDisposable.Dispose() { }

    // Declare the Current property required by IEnumerator:
    object IEnumerator.Current
    {
        get { return Current; }
    }
    public CSVRow Current
    {
        get { return _elements[position]; }
    }
   
}

