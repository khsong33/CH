﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CSVRow
{
	public delegate String OnParsingLogPrefixDelegate(String pRowInfo);

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 파싱 과정 등에서 로그를 남길 필요가 있을 때 따라붙는 머리글을 생성하는 콜백
	public OnParsingLogPrefixDelegate aOnParsingLogPrefixDelegate
	{
		get { return mOnParsingLogPrefixDelegate; }
		set { mOnParsingLogPrefixDelegate = value; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄 정보
	public String aRowInfo
	{
		get { return mRowInfo; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 값 테이블
	public Dictionary<string, string> aValueDictionary
	{
		get { return mValueDictionary; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public CSVRow()
	{
		mValueDictionary = new Dictionary<string, string>();
	}
	//-------------------------------------------------------------------------------------------------------
	// IDispose 메서드
// 	public void Dispose()
// 	{
// 	}
	//-------------------------------------------------------------------------------------------------------
	// 파싱 로그 머리말 생성 규칙을 지정합니다.
	public void SetParsingLogPrefix(OnParsingLogPrefixDelegate pOnParsingLogPrefixDelegate, String pRowInfo)
	{
		mOnParsingLogPrefixDelegate = pOnParsingLogPrefixDelegate;
		mRowInfo = pRowInfo;
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 문자열 값을 얻습니다.
	public String GetStringValue(String pValueColumnName, String pDefault)
	{
		try
		{
			return TextUtil.ValidateExcelString(mValueDictionary[pValueColumnName]);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 문자열 값을 얻습니다.
	public String GetStringValue(String pValueColumnName)
	{
		try
		{
			return TextUtil.ValidateExcelString(mValueDictionary[pValueColumnName]);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return String.Empty;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 문자열 값을 얻습니다.
	public bool TryStringValue(String pValueColumnName, out String oValue, String pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				oValue = lValueString;
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 부울 값을 얻습니다.
	public bool GetBoolValue(String pValueColumnName, bool pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			bool lBoolValue;
			if (lValueString == "True")
				lBoolValue = true;
			else if (lValueString == "False")
				lBoolValue = false;
			else
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return pDefault;
			}
			return lBoolValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 부울 값을 얻습니다.
	public bool GetBoolValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			bool lBoolValue;
			if (lValueString == "True")
				lBoolValue = true;
			else if (lValueString == "False")
				lBoolValue = false;
			else
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return false;
			}
			return lBoolValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 부울 값을 얻습니다.
	public bool TryBoolValue(String pValueColumnName, out bool oValue, bool pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				if (lValueString == "True")
					oValue = true;
				else if (lValueString == "False")
					oValue = false;
				else
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = false;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 실수 값을 얻습니다.
	public float GetFloatValue(String pValueColumnName, float pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lFloatValue;
			if (!float.TryParse(lValueString, out lFloatValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return pDefault;
			}
			return lFloatValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 실수 값을 얻습니다.
	public float GetFloatValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lFloatValue;
			if (!float.TryParse(lValueString, out lFloatValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return 0.0f;
			}
			return lFloatValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return 0.0f;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 실수 값을 얻습니다.
	public bool TryFloatValue(String pValueColumnName, out float oValue, float pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				if (!float.TryParse(lValueString, out oValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = 0.0f;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 정수 값을 얻습니다.
	public int GetIntValue(String pValueColumnName, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			int lIntValue = int.Parse(lValueString);
			return lIntValue;
		}
		catch (Exception)
		{
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 정수 값을 얻습니다.
	public int GetIntValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			int lIntValue;
			if (!int.TryParse(lValueString, out lIntValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return 0;
			}
			return lIntValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 정수 값을 얻습니다.
	public bool TryIntValue(String pValueColumnName, out int oValue, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				if (!int.TryParse(lValueString, out oValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = 0;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 밀리 단위의 정수(버림) 값을 얻습니다.
	public int GetMilliIntValue(String pValueColumnName, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lValue;
			if (!float.TryParse(lValueString, out lValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return pDefault;
			}
            return Mathf.RoundToInt(lValue * 1000.0f);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 밀리 단위의 정수(버림) 값을 얻습니다.
	public int GetMilliIntValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lValue;
			if (!float.TryParse(lValueString, out lValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return 0;
			}
            return Mathf.RoundToInt(lValue * 1000.0f);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 밀리 단위의 정수(버림) 값을 얻습니다.
	public bool TryMilliIntValue(String pValueColumnName, out int oValue, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				float lIntValue;
				if (!float.TryParse(lValueString, out lIntValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = 0;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
                oValue = Mathf.RoundToInt(lIntValue * 1000.0f);
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 만분율 값을 얻습니다.
	public int GetRatioValue(String pValueColumnName, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lPercentValue;
			if (!float.TryParse(lValueString, out lPercentValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return pDefault;
			}
            return Mathf.RoundToInt(lPercentValue * 100.0f);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return pDefault;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 만분율 값을 얻습니다.
	public int GetRatioValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			float lPercentValue;
			if (!float.TryParse(lValueString, out lPercentValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return 0;
			}
            return Mathf.RoundToInt(lPercentValue * 100.0f);
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 만분율 값을 얻습니다.
	public bool TryRatioValue(String pValueColumnName, out int oValue, int pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				float lPercentValue;
				if (!float.TryParse(lValueString, out lPercentValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = 0;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
                oValue = Mathf.RoundToInt(lPercentValue * 100.0f);
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 좌표 값을 얻습니다.
	public Coord2 GetCoordValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			String[] lCoordValueStrings = lValueString.Split(',');
			if (lCoordValueStrings.Length != 2)
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Coord2.zero;
			}

			int lCoordXValue;
			int lCoordZValue;
			if (!int.TryParse(lCoordValueStrings[0], out lCoordXValue) ||
				!int.TryParse(lCoordValueStrings[1], out lCoordZValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Coord2.zero;
			}
			Coord2 lCoordValue = new Coord2(lCoordXValue, lCoordZValue);
			return lCoordValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return Coord2.zero;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 좌표 값을 얻습니다.
	public Coord2 GetCoordValue(String pValueColumnName, Vector3 pCoordScale)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			String[] lCoordValueStrings = lValueString.Split(',');
			if (lCoordValueStrings.Length != 2)
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Coord2.zero;
			}

			float lCoordXValue;
			float lCoordZValue;
			if (!float.TryParse(lCoordValueStrings[0], out lCoordXValue) ||
				!float.TryParse(lCoordValueStrings[1], out lCoordZValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Coord2.zero;
			}
			Coord2 lCoordValue = new Coord2((int)(lCoordXValue * pCoordScale.x), (int)(lCoordZValue * pCoordScale.z));
			return lCoordValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return Coord2.zero;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 좌표 값을 얻습니다.
	public bool TryCoordValue(String pValueColumnName, out Coord2 oValue, Coord2 pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				String[] lCoordValueStrings = lValueString.Split(',');
				if (lCoordValueStrings.Length != 2)
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = Coord2.zero;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}

				int lCoordXValue;
				int lCoordZValue;
				if (!int.TryParse(lCoordValueStrings[0], out lCoordXValue) ||
					!int.TryParse(lCoordValueStrings[1], out lCoordZValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = Coord2.zero;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
				oValue = new Coord2(lCoordXValue, lCoordZValue);
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 좌표 값을 얻습니다.
	public bool TryCoordValue(String pValueColumnName, Vector3 pCoordScale, out Coord2 oValue, Coord2 pDefault)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			if ((lValueString.Length > 1) ||
				(lValueString[0] != TextUtil.cEmptyExcelStringChar))
			{
				String[] lCoordValueStrings = lValueString.Split(',');
				if (lCoordValueStrings.Length != 2)
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = Coord2.zero;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}

				float lCoordXValue;
				float lCoordZValue;
				if (!float.TryParse(lCoordValueStrings[0], out lCoordXValue) ||
					!float.TryParse(lCoordValueStrings[1], out lCoordZValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					oValue = Coord2.zero;
					return false; // 값을 넣기는 했는데 잘못된 포맷
				}
				oValue = new Coord2((int)(lCoordXValue * pCoordScale.x), (int)(lCoordZValue * pCoordScale.z));
				return true;
			}
			else
			{
				oValue = pDefault;
				return false; // 빈 칸
			}
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			oValue = pDefault;
			return false; // 컬럼 못찾음
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 컬러 값을 얻습니다.
	public Color GetColorValue(String pValueColumnName)
	{
		try
		{
			String lValueString = mValueDictionary[pValueColumnName];
			String[] lColorValueStrings = lValueString.Split('^');
			if (lColorValueStrings.Length < 3)
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Color.white;
			}

			int lRedValue;
			int lGreenValue;
			int lBlueValue;
			if (!int.TryParse(lColorValueStrings[0], out lRedValue) ||
				!int.TryParse(lColorValueStrings[1], out lGreenValue) ||
				!int.TryParse(lColorValueStrings[2], out lBlueValue))
			{
				Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
				return Color.white;
			}
			int lAlphaValue = 255;
			if (lColorValueStrings.Length >= 4)
			{
				if (!int.TryParse(lColorValueStrings[3], out lAlphaValue))
				{
					Debug.LogError(String.Format("{0} invalid {1}:'{2}'", _GetParsingLogPrefix(), pValueColumnName, lValueString));
					return Color.white;
				}
			}
			Color lColorValue = ColorUtil.Color4ub(lRedValue, lGreenValue, lBlueValue, lAlphaValue);
			return lColorValue;
		}
		catch (Exception)
		{
			Debug.LogError(String.Format("{0} can't find column {1}", _GetParsingLogPrefix(), pValueColumnName));
			return Color.white;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 로그 머리글을 얻습니다.
	private String _GetParsingLogPrefix()
	{
		if (mOnParsingLogPrefixDelegate != null)
			return mOnParsingLogPrefixDelegate(mRowInfo);
		else
			return "CSVRow parsing log:";
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private Dictionary<string, string> mValueDictionary;
	private OnParsingLogPrefixDelegate mOnParsingLogPrefixDelegate;
	private String mRowInfo;
}
