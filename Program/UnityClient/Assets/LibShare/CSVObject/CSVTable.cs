﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class CSVTable
{
	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 테이블을 로딩합니다.
	public void Load(String pCSVText, String pKeyColumnName)
	{
		//Debug.Log("Load() - " + this);

		mCSVObject = new CSVObject();
		mCSVObject.LoadCSV(pCSVText, 0); // 헤더를 가지고 있다고 가정합니다.

		mCSVRowDictionary = new Dictionary<String, CSVRow>();

		for (int iRow = 0; iRow < mCSVObject.RowNum; ++iRow) 
		{
			CSVRow lCSVRow = mCSVObject.GetRow(iRow);
			String lRowKey = lCSVRow.aValueDictionary[pKeyColumnName];
			if (lRowKey.Length <= 1) // 주석 넘김
				continue;

			if (mCSVRowDictionary.ContainsKey(lRowKey))
				Debug.LogError("duplicate row key:" + lRowKey);
			mCSVRowDictionary.Add(lRowKey, lCSVRow);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키의 줄을 얻습니다.
	public CSVRow GetRow(String pRowKey)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow;
		else
			return null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 문자열 값을 얻습니다.
	public String GetStringValue(String pRowKey, String pValueColumnName, String pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetStringValue(pValueColumnName, pDefault);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 이름의 문자열 값을 얻습니다.
	public String GetStringValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetStringValue(pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return String.Empty;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 문자열 값을 얻습니다.
	public String GetStringValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetStringValue(pValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 실수 값을 얻습니다.
	public float GetFloatValue(String pRowKey, String pValueColumnName, float pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetFloatValue(pValueColumnName, pDefault);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 실수 값을 얻습니다.
	public float GetFloatValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetFloatValue(pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return 0.0f;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 실수 값을 얻습니다.
	public float GetFloatValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetFloatValue(pValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 정수 값을 얻습니다.
	public int GetIntValue(String pRowKey, String pValueColumnName, int pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetIntValue(lCSVRow, pValueColumnName);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 정수 값을 얻습니다.
	public int GetIntValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetIntValue(lCSVRow, pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 정수 값을 얻습니다.
	public int GetIntValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetIntValue(pValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 밀리 단위의 정수 값을 얻습니다.
	public int GetMilliIntValue(String pRowKey, String pValueColumnName, int pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetMilliIntValue(pValueColumnName, pDefault);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 밀리 단위의 정수 값을 얻습니다.
	public int GetMilliIntValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetMilliIntValue(lCSVRow, pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 밀리 단위의 정수(버림) 값을 얻습니다.
	public int GetMilliIntValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetMilliIntValue(pValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 만분율 값을 얻습니다.
	public int GetRatioValue(String pRowKey, String pValueColumnName, int pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return lCSVRow.GetRatioValue(pValueColumnName, pDefault);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 만분율 값을 얻습니다.
	public int GetRatioValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetRatioValue(lCSVRow, pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return 0;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 만분율 값을 얻습니다.
	public int GetRatioValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetRatioValue(pValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 정수 값을 얻습니다.
	public Color GetColorValue(String pRowKey, String pValueColumnName, Color pDefault)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetColorValue(lCSVRow, pValueColumnName);
		else
			return pDefault;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키와 이름의 정수 값을 얻습니다.
	public Color GetColorValue(String pRowKey, String pValueColumnName)
	{
		CSVRow lCSVRow;
		if (mCSVRowDictionary.TryGetValue(pRowKey, out lCSVRow))
			return GetColorValue(lCSVRow, pValueColumnName);
		else
		{
			Debug.LogError("can't find row key:" + pRowKey);
			return Color.white;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 줄에서 해당 이름의 정수 값을 얻습니다.
	public Color GetColorValue(CSVRow pCSVRow, String pValueColumnName)
	{
		return pCSVRow.GetColorValue(pValueColumnName);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private CSVObject mCSVObject;
	private Dictionary<String, CSVRow> mCSVRowDictionary;
}
