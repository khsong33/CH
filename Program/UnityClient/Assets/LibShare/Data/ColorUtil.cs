using UnityEngine;
using System.Collections;
using System;

public class ColorUtil
{
	public const float cColor256Scale = 1.0f / 255.0f;

	//-------------------------------------------------------------------------------------------------------
	// 함수
	//-------------------------------------------------------------------------------------------------------
	// 4ub 포맷으로 컬러를 생성합니다.
	public static Color Color4ub(int pRed, int pGreen, int pBlue, int pAlpha)
	{
		return new Color(pRed * cColor256Scale, pGreen * cColor256Scale, pBlue * cColor256Scale, pAlpha * cColor256Scale);
	}
	//-------------------------------------------------------------------------------------------------------
	// 3ub 포맷으로 컬러를 생성합니다.
	public static Color Color3ub(int pRed, int pGreen, int pBlue)
	{
		return new Color(pRed * cColor256Scale, pGreen * cColor256Scale, pBlue * cColor256Scale);
	}
	//-------------------------------------------------------------------------------------------------------
	// 알파를 바꿉니다.
	public static void SetAlpha(ref Color rColor, float pAlpha)
	{
		Color lColor = rColor;
		lColor.a = pAlpha;
		rColor = lColor;
	}
}
