using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class Config
{
	public const String cCSVTablePath = "ConfigCSV";
	public const String cKeyColumnName= "Key";
	public const String cValueColumnName = "Value";

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤 객체
	public static Config get
	{
		get
		{
			if (sInstance == null)
				CreateSingleton();
			return sInstance;
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 싱글톤을 생성합니다.
	public static void CreateSingleton()
	{
		if (sInstance == null)
		{
			sInstance = new Config();
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public Config()
	{
		Load();
	}
	//-------------------------------------------------------------------------------------------------------
	// 로딩합니다.
	public void Load()
	{
		mCSVTable = new CSVTable();
		mCSVTable.Load(ResourceUtil.LoadString(cCSVTablePath), cKeyColumnName);
		mValueColumnName = cValueColumnName;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키의 문자열 값을 얻습니다.
	public String GetStringValue(String pKey)
	{
		return mCSVTable.GetStringValue(pKey, mValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키의 실수 값을 얻습니다.
	public float GetFloatValue(String pKey)
	{
		return mCSVTable.GetFloatValue(pKey, mValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키의 정수 값을 얻습니다.
	public int GetIntValue(String pKey)
	{
		return mCSVTable.GetIntValue(pKey, mValueColumnName);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 키의 밀리 단위의 정수 값을 얻습니다.
	public int GetMilliIntValue(String pKey)
	{
		return mCSVTable.GetMilliIntValue(pKey, mValueColumnName);
	}
	
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static Config sInstance = null;

	private CSVTable mCSVTable;
	private String mValueColumnName;
}
