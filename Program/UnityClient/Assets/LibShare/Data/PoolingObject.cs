using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class PoolingObject : IDisposable
{
	internal _ObjectPool mObjectPool;

	public abstract void Reset();

	public void Dispose()
	{
		Reset();
		if (mObjectPool.mIsEnablePooling)
			mObjectPool.mObjectPoolingQueue.Enqueue(this);
	}
}

public class _ObjectPool
{
	internal Queue<PoolingObject> mObjectPoolingQueue;
	internal bool mIsEnablePooling;

	internal _ObjectPool(bool pIsEnablePooling)
	{
		mObjectPoolingQueue = new Queue<PoolingObject>();
		mIsEnablePooling = pIsEnablePooling;
	}
}

public class ObjectPool<T_PoolingObject> : _ObjectPool where T_PoolingObject : PoolingObject, new()
{
	public ObjectPool(bool pIsEnablePooling)
		: base(pIsEnablePooling)
	{
	}
	public T_PoolingObject Allocate()
	{
		T_PoolingObject lPoolingObject;
		if (mObjectPoolingQueue.Count <= 0)
		{
			lPoolingObject = new T_PoolingObject();
			lPoolingObject.mObjectPool = this;
		}
		else
			lPoolingObject = (T_PoolingObject)mObjectPoolingQueue.Dequeue();

		return lPoolingObject;
	}
}
