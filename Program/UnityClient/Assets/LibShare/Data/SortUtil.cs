using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class SortUtil
{
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 리스트를 정렬합니다.
	public static void SortList<T>(List<T> pList) where T : IComparable<T>
	{
		for (int iItem = 0; iItem < pList.Count; ++iItem)
		{
			for (int iChangeItem = (iItem + 1); iChangeItem < pList.Count; ++iChangeItem)
			{
				if (pList[iChangeItem].CompareTo(pList[iItem]) < 0)
				{
					T lTemp = pList[iItem];
					pList[iItem] = pList[iChangeItem];
					pList[iChangeItem] = lTemp;
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 리스트를 정렬합니다.
	public static void SortList<T>(List<T> pList, Comparison<T> pComparison)
	{
		for (int iItem = 0; iItem < pList.Count; ++iItem)
		{
			for (int iChangeItem = (iItem + 1); iChangeItem < pList.Count; ++iChangeItem)
			{
				if (pComparison(pList[iChangeItem], pList[iItem]) < 0)
				{
					T lTemp = pList[iItem];
					pList[iItem] = pList[iChangeItem];
					pList[iChangeItem] = lTemp;
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 리스트를 정렬합니다.
	public static void SortList<T>(List<T> pList, IComparer<T> pComparer)
	{
		for (int iItem = 0; iItem < pList.Count; ++iItem)
		{
			for (int iChangeItem = (iItem + 1); iChangeItem < pList.Count; ++iChangeItem)
			{
				if (pComparer.Compare(pList[iChangeItem], pList[iItem]) < 0)
				{
					T lTemp = pList[iItem];
					pList[iItem] = pList[iChangeItem];
					pList[iChangeItem] = lTemp;
				}
			}
		}
	}
}
