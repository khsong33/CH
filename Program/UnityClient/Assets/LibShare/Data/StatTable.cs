using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class StatTable<T_Stat> where T_Stat : struct, IConvertible, IComparable, IFormattable
{
	public delegate void OnStatChangeDelegate(int pStatIdx);

	private int mStatCount;

	protected int[] mStats;
	protected OnStatChangeDelegate[] mOnStatChangeDelegates;

	public int aStatCount
	{
		get { return mStatCount; }
	}

	public OnStatChangeDelegate[] aOnStatChangeDelegates
	{
		get { return mOnStatChangeDelegates; }
	}

	public StatTable()
	{
		mStatCount = Enum.GetValues(typeof(T_Stat)).Length;

		mStats = new int[mStatCount];

		mOnStatChangeDelegates = new OnStatChangeDelegate[mStatCount];
	}

	public void SetStatChangeDelegate(T_Stat pStat, OnStatChangeDelegate pOnStatChangeDelegate)
	{
		int lStatIdx = Convert.ToInt32(pStat);

		mOnStatChangeDelegates[lStatIdx] = pOnStatChangeDelegate;
	}

	public void ValidateStatChangeLink(int pStatIdx)
	{
		if (mOnStatChangeDelegates[pStatIdx] != null)
			mOnStatChangeDelegates[pStatIdx](pStatIdx);
	}

	public int Get(int pStatIdx)
	{
		return mStats[pStatIdx];
	}

	public void Set(int pStatIdx, int value)
	{
		mStats[pStatIdx] = value;

		if (mOnStatChangeDelegates[pStatIdx] != null)
			mOnStatChangeDelegates[pStatIdx](pStatIdx);
	}

	public void Set(T_Stat pStat, int value)
	{
		int lStatIdx = Convert.ToInt32(pStat);

		mStats[lStatIdx] = value;

		if (mOnStatChangeDelegates[lStatIdx] != null)
			mOnStatChangeDelegates[lStatIdx](lStatIdx);
	}

	public void Copy(int pStatIdx, StatTable<T_Stat> pSetUpTable)
	{
		mStats[pStatIdx] = pSetUpTable.mStats[pStatIdx];

		if (mOnStatChangeDelegates[pStatIdx] != null)
			mOnStatChangeDelegates[pStatIdx](pStatIdx);
	}

	public void Copy(T_Stat pStat, StatTable<T_Stat> pSetUpTable)
	{
		int lStatIdx = Convert.ToInt32(pStat);

		mStats[lStatIdx] = pSetUpTable.mStats[lStatIdx];

		if (mOnStatChangeDelegates[lStatIdx] != null)
			mOnStatChangeDelegates[lStatIdx](lStatIdx);
	}

	public void Copy(StatTable<T_Stat> pStatTable)
	{
		if (pStatTable.mStats.Length != mStats.Length)
			Debug.LogError("table length mismatch - " + this);

		for (int iStat = 0; iStat < mStats.Length; ++iStat)
		{
			mStats[iStat] = pStatTable.mStats[iStat];

			if (mOnStatChangeDelegates[iStat] != null)
				mOnStatChangeDelegates[iStat](iStat);
		}
	}
}
