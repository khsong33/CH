using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum StatModifyType
{
	None,
	Ratio,
	Add,
	Overwrite,
}

public class StatTableModifier<T_Stat> where T_Stat : struct, IConvertible, IComparable, IFormattable
{
	protected StatModifyType[] mModifyTypes;

	protected int mMaxLevel;
	protected int[][] mLevelModifyValues;
	protected int[] mModifyValues;

	protected int mModifyStatCount;
	protected int[] mModifyStatIdxs;

	public StatModifyType[] aStatModifyTypes
	{
		get { return mModifyTypes; }
	}
	public int[] aModifyValues
	{
		get { return mModifyValues; }
	}
	public int aModifyStatCount
	{
		get { return mModifyStatCount; }
	}
	public int[] aModifyStatIdxs
	{
		get { return mModifyStatIdxs; }
	}

	public StatTableModifier()
	{
		Init(1);
	}
	public StatTableModifier(int pMaxLevel)
	{
		Init(pMaxLevel);
	}
	public void Init(int pMaxLevel)
	{
		int lStatEnumCount = Enum.GetValues(typeof(T_Stat)).Length;

		mModifyTypes = new StatModifyType[lStatEnumCount];
		for (int iStat = 0; iStat < lStatEnumCount; ++iStat)
			mModifyTypes[iStat] = StatModifyType.None;

		mMaxLevel = pMaxLevel;
		if (mMaxLevel > 1)
		{
			mLevelModifyValues = new int[1 + mMaxLevel][]; // 0 레벨 포함해서 1+
			for (int iLevel = 1; iLevel <= mMaxLevel; ++iLevel)
				mLevelModifyValues[iLevel] = new int[lStatEnumCount];
			mModifyValues = mLevelModifyValues[1];
		}
		else
		{
			mModifyValues = new int[lStatEnumCount];
		}

		mModifyStatCount = 0;
		mModifyStatIdxs = new int[lStatEnumCount];
	}

	public void AddModifyIntStat(T_Stat pModifyStat, StatModifyType pModifyType, int pModifyIntValue)
	{
		int lModifyStatIdx = Convert.ToInt32(pModifyStat);

		if (mModifyTypes[lModifyStatIdx] != StatModifyType.None)
		{
			Debug.LogError("already modified stat:" + lModifyStatIdx);
			return; // 첫 적용된 변경만 효과를 줌
		}

		mModifyTypes[lModifyStatIdx] = pModifyType;

		if (mMaxLevel > 1)
		{
			mLevelModifyValues[1][lModifyStatIdx] = pModifyIntValue; 
			for (int iLevel = 2; iLevel <= mMaxLevel; ++iLevel)
				mLevelModifyValues[iLevel][lModifyStatIdx]
					= (int)((Int64)mLevelModifyValues[iLevel - 1][lModifyStatIdx] * pModifyIntValue / 10000);
		}
		else
		{
			mModifyValues[lModifyStatIdx] = pModifyIntValue;
		}

		mModifyStatIdxs[mModifyStatCount++] = lModifyStatIdx;
	}

	public void AddModifyBoolStat(T_Stat pModifyStat, StatModifyType pModifyType, bool pModifyBoolValue)
	{
		int lModifyStatIdx = Convert.ToInt32(pModifyStat);

		if (mModifyTypes[lModifyStatIdx] != StatModifyType.None)
		{
			Debug.LogError("already modified stat:" + lModifyStatIdx);
			return; // 첫 적용된 변경만 효과를 줌
		}
		if (pModifyType != StatModifyType.Overwrite)
		{
			Debug.LogError("bool stat must be overwrited:" + lModifyStatIdx);
			return; // 불 스탯은 오버라이트만 가능
		}

		mModifyTypes[lModifyStatIdx] = pModifyType;

		if (mMaxLevel > 1)
		{
			for (int iLevel = 1; iLevel <= mMaxLevel; ++iLevel)
				mLevelModifyValues[iLevel][lModifyStatIdx] = (pModifyBoolValue ? 1 : 0);
		}
		else
		{
			mModifyValues[lModifyStatIdx] = (pModifyBoolValue ? 1 : 0);
		}

		mModifyStatIdxs[mModifyStatCount++] = lModifyStatIdx;
	}

	public void ModifyStatTable(StatTable<T_Stat> pStatTable)
	{
		for (int iStat = 0; iStat < mModifyStatCount; ++iStat)
		{
			int lModifiedStatIdx = mModifyStatIdxs[iStat];

			if (mModifyTypes[lModifiedStatIdx] == StatModifyType.None)
				continue;

			switch (mModifyTypes[lModifiedStatIdx])
			{
			case StatModifyType.Ratio:
				pStatTable.Set(lModifiedStatIdx, (int)((Int64)pStatTable.Get(lModifiedStatIdx) * mModifyValues[lModifiedStatIdx] / 10000)); // Ratio는 만분율임
				break;
			case StatModifyType.Add:
				pStatTable.Set(lModifiedStatIdx, pStatTable.Get(lModifiedStatIdx) + mModifyValues[lModifiedStatIdx]);
				break;
			case StatModifyType.Overwrite:
				pStatTable.Set(lModifiedStatIdx, mModifyValues[lModifiedStatIdx]);
				break;
			default:
				Debug.LogError("not implemented yet");
				break;
			}

			// StatTable.ValidateStatChangeLink()
			if (pStatTable.aOnStatChangeDelegates[lModifiedStatIdx] != null)
				pStatTable.aOnStatChangeDelegates[lModifiedStatIdx](lModifiedStatIdx);
		}
	}

	public void ModifyLevelStatTable(StatTable<T_Stat> pStatTable, int pLevel)
	{
		for (int iStat = 0; iStat < mModifyStatCount; ++iStat)
		{
			int lModifiedStatIdx = mModifyStatIdxs[iStat];

			if (mModifyTypes[lModifiedStatIdx] == StatModifyType.None)
				continue;

			int lModifyValue = mLevelModifyValues[pLevel][lModifiedStatIdx];

			switch (mModifyTypes[lModifiedStatIdx])
			{
			case StatModifyType.Ratio:
				pStatTable.Set(lModifiedStatIdx, (int)((Int64)pStatTable.Get(lModifiedStatIdx) * lModifyValue / 10000)); // Ratio는 만분율임
				break;
			case StatModifyType.Add:
				pStatTable.Set(lModifiedStatIdx, pStatTable.Get(lModifiedStatIdx) + lModifyValue);
				break;
			case StatModifyType.Overwrite:
				pStatTable.Set(lModifiedStatIdx, lModifyValue);
				break;
			default:
				Debug.LogError("not implemented yet");
				break;
			}

			// StatTable.ValidateStatChangeLink()
			if (pStatTable.aOnStatChangeDelegates[lModifiedStatIdx] != null)
				pStatTable.aOnStatChangeDelegates[lModifiedStatIdx](lModifiedStatIdx);
		}
	}

	public void ModifyStat(StatTable<T_Stat> pStatTable, int pStatIdx)
	{
		if (mModifyTypes[pStatIdx] == StatModifyType.None)
			return;

		switch (mModifyTypes[pStatIdx])
		{
		case StatModifyType.Ratio:
			pStatTable.Set(pStatIdx, (int)((Int64)pStatTable.Get(pStatIdx) * mModifyValues[pStatIdx] / 10000)); // Ratio는 만분율임
			break;
		case StatModifyType.Add:
			pStatTable.Set(pStatIdx, pStatTable.Get(pStatIdx) + mModifyValues[pStatIdx]);
			break;
		case StatModifyType.Overwrite:
			pStatTable.Set(pStatIdx, mModifyValues[pStatIdx]);
			break;
		default:
			Debug.LogError("not implemented yet");
			break;
		}

		// StatTable.ValidateStatChangeLink()
		if (pStatTable.aOnStatChangeDelegates[pStatIdx] != null)
			pStatTable.aOnStatChangeDelegates[pStatIdx](pStatIdx);
	}

	public void ModifyLevelStat(StatTable<T_Stat> pStatTable, int pStatIdx, int pLevel)
	{
		if (mModifyTypes[pStatIdx] == StatModifyType.None)
			return;

		int lModifyValue = mLevelModifyValues[pLevel][pStatIdx];

		switch (mModifyTypes[pStatIdx])
		{
		case StatModifyType.Ratio:
			pStatTable.Set(pStatIdx, (int)((Int64)pStatTable.Get(pStatIdx) * lModifyValue / 10000)); // Ratio는 만분율임
			break;
		case StatModifyType.Add:
			pStatTable.Set(pStatIdx, pStatTable.Get(pStatIdx) + lModifyValue);
			break;
		case StatModifyType.Overwrite:
			pStatTable.Set(pStatIdx, lModifyValue);
			break;
		default:
			Debug.LogError("not implemented yet");
			break;
		}

		// StatTable.ValidateStatChangeLink()
		if (pStatTable.aOnStatChangeDelegates[pStatIdx] != null)
			pStatTable.aOnStatChangeDelegates[pStatIdx](pStatIdx);
	}
}
