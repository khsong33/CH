using UnityEngine;
using System.Collections;
using System;

public class TextUtil
{
	public static readonly char cEmptyExcelStringChar = '\"';

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 엑셀 테이블에서 의미있는 문자열을 읽어들입니다.
	public static String ValidateExcelString(String pString)
	{
		if ((pString != null) && // null 문자열 검사
			(pString.Length > 1) || (pString[0] != cEmptyExcelStringChar)) // 길이가 1보다 크거나 빈 칸 문자열이 아니라면
			return pString;
		else
			return String.Empty;
	}
	//-------------------------------------------------------------------------------------------------------
	// String형을 SByte형으로 변환합니다.
	public static void ConvertStringToSByte(String pStr, out sbyte[] pSByte)
	{
		byte[] lBytes = new byte[pStr.Length];
		System.Text.ASCIIEncoding enc = new System.Text.ASCIIEncoding();
		lBytes = enc.GetBytes(pStr);

		pSByte = new sbyte[pStr.Length];
		for (int i = 0; i < pStr.Length; i++)
		{
			pSByte[i] = Convert.ToSByte(lBytes[i] - '0');
		}
	}
}
