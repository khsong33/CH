//#define BATTLE_VERIFY_TEST

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public delegate void FrameTaskDelegate(FrameTask pTask, int pExtraFrameDelta);

public enum FrameUpdateLayer
{
	Layer0,
	Layer1,
}

public class FrameTask
{
	internal uint mGuid;

	internal GuidLink<IFrameObject> mObjectLink;
	internal FrameTaskDelegate mTaskDelegate;

	internal LinkedListNode<FrameTask> mUpdateNode; // 반복용
	internal int mReservationDelay; // 예약용

	internal FrameTask()
	{
		mObjectLink = new GuidLink<IFrameObject>();
	}

	internal void InitRepeat(IFrameObject pFrameObject, FrameTaskDelegate pRepeatTaskDelegate)
	{
		mObjectLink.Set(pFrameObject);
		mTaskDelegate = pRepeatTaskDelegate;
		if (mUpdateNode == null)
			mUpdateNode = new LinkedListNode<FrameTask>(this);

		++pFrameObject.aTaskCount;
	}
	internal void InitReservation(IFrameObject pFrameObject, FrameTaskDelegate pReservationTaskDelegate, int pReservationDelay)
	{
		mObjectLink.Set(pFrameObject);
		mTaskDelegate = pReservationTaskDelegate;
		mReservationDelay = pReservationDelay;

		++pFrameObject.aTaskCount;
	}

	public uint aGuid
	{
		get { return mGuid; }
	}

	public void Cancel()
	{
		mObjectLink.Set(null);
	}
}

public class FrameUpdater
{
	public delegate void OnFrameChangeDelegate(int pFrameIdx);
	public delegate void OnFrameSliceChangeDelegate(int pFrameIdx, int pFrameSliceIdx, int pFrameDelta);

	private class ReservationTaskSlot
	{
		internal Queue<FrameTask> mFrameTaskQueue;

		internal ReservationTaskSlot()
		{
			mFrameTaskQueue = new Queue<FrameTask>();
		}
	}

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 한 프레임의 델타 시간(밀리초)
	public int aFrameDelta
	{
		get { return mFrameDelta; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 한 프레임을 나누어서 처리하기 위한 분할 조각 개수
	public int aFrameSliceCount
	{
		get { return mFrameSliceCount; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 태스트를 예약할 수 있는 최대 프레임 딜레이
	public int aTaskFrameRange
	{
		get { return mTaskFrameRange; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 초당 프레임 수
	public float aFramePerSecond
	{
		get { return mFramePerSecond; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신 중단 여부
	public bool aIsStopped
	{
		get { return mIsStopped; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 현재의 프레임 번호(고정 간격 정수 카운터, 1부터 시작)
	public int aFrameIdx
	{
		get { return mFrameIdx; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 경과 시간
	public int aElapsedTime
	{
		get { return mElapsedTime; }
	}
	//-------------------------------------------------------------------------------------------------------
	// 이벤트 콜백
	public OnFrameChangeDelegate aOnFrameChangeDelegate { get; set; }
	public OnFrameSliceChangeDelegate aOnFrameSliceChangeDelegate { get; set; }
	//-------------------------------------------------------------------------------------------------------
	// [개발용] 갱신 리스트를 노출
	public LinkedList<FrameTask>[] aRepeatTaskList
	{
		get { return mRepeatTaskLists; }
	}
	//-------------------------------------------------------------------------------------------------------
	// [개발용] 통계
	public FrameUpdaterStatistics aStatistics
	{
		get { return mStatistics; }
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public FrameUpdater(int pFrameDelta, int pFrameSliceCount, int pTaskFrameRange)
	{
		mFrameDelta = pFrameDelta;
		mFrameSliceCount = pFrameSliceCount;
		mFrameSliceDelta = pFrameDelta / pFrameSliceCount;
		mTaskFrameRange = pTaskFrameRange;
		mFramePerSecond = 1000.0f / mFrameDelta;
		mFrameDeltaMinusOne = mFrameDelta - 1;

		mRepeatTaskLists = new LinkedList<FrameTask>[mFrameSliceCount];
		mRepeatTaskAddQueue = new Queue<FrameTask>();
		for (int iSlice = 0; iSlice < mFrameSliceCount; ++iSlice)
			mRepeatTaskLists[iSlice] = new LinkedList<FrameTask>();

		mFrameSlotItemCounts = new int[mFrameSliceCount];
		mFrameTaskPoolingQueue = new Queue<FrameTask>();

		mReservationTaskFrameIdxRange = (mTaskFrameRange + mFrameDelta + mFrameDeltaMinusOne) / mFrameDelta;
		//Debug.Log("mReservationTaskFrameIdxRange=" + mReservationTaskFrameIdxRange);
		mReservationTaskSlots = new ReservationTaskSlot[mReservationTaskFrameIdxRange][];
		for (int iFrame = 0; iFrame < mReservationTaskFrameIdxRange; ++iFrame)
		{
			mReservationTaskSlots[iFrame] = new ReservationTaskSlot[mFrameSliceCount];
			for (int iSlice = 0; iSlice < mFrameSliceCount; ++iSlice)
				mReservationTaskSlots[iFrame][iSlice] = new ReservationTaskSlot();
		}

		mStatistics = new FrameUpdaterStatistics();

		_CommonReset();
	}
	//-------------------------------------------------------------------------------------------------------
	// 기본 초기화를 합니다(내부 함수지만 코드 읽기 편하라고 이곳에 둠)
	private void _CommonReset()
	{
		mObjectGuidGenerator = 0;
		mTaskGuidGenerator = 0;

		mIsRequestStopUpdate = false;
		mIsStopped = false;
		mFrameTimer = 0;
		mFrameSliceIdx = 0;
		mFrameIdx = 0;
		for (int iSlice = 0; iSlice < mFrameSliceCount; ++iSlice)
			mFrameSlotItemCounts[iSlice] = 0;
		mElapsedTime = 0;

		mCurrentReservationTaskSlotIdx = 0;

		mStatistics.Reset();
	}
	//-------------------------------------------------------------------------------------------------------
	// 초기화합니다.
	public void Reset()
	{
		_CommonReset();

		// 태스크를 모두 비웁니다.
		while (mRepeatTaskAddQueue.Count > 0)
		{
			FrameTask lRepeatTask = mRepeatTaskAddQueue.Dequeue();
			lRepeatTask.Cancel();
			mFrameTaskPoolingQueue.Enqueue(lRepeatTask);
		}
		for (int iSlice = 0; iSlice < mFrameSliceCount; ++iSlice)
		{
			LinkedList<FrameTask> lRepeatTaskList = mRepeatTaskLists[iSlice];
			while (lRepeatTaskList.First != null)
			{
				FrameTask lRepeatTask = lRepeatTaskList.First.Value;
				lRepeatTaskList.RemoveFirst();
				if (lRepeatTask.mObjectLink.Get() != null)
				{
					Debug.LogError("lRepeatTask.mObjectLink.Get()=" + lRepeatTask.mObjectLink.Get());
					lRepeatTask.Cancel();
				}
				mFrameTaskPoolingQueue.Enqueue(lRepeatTask);
			}
			for (int iFrame = 0; iFrame < mReservationTaskFrameIdxRange; ++iFrame)
			{
				Queue<FrameTask> lFrameTaskQueue = mReservationTaskSlots[iFrame][iSlice].mFrameTaskQueue;
				while (lFrameTaskQueue.Count > 0)
				{
					FrameTask lFrameTask = lFrameTaskQueue.Dequeue();
					if (lFrameTask.mObjectLink.Get() != null)
					{
						Debug.LogError("lFrameTask.mObjectLink.Get()=" + lFrameTask.mObjectLink.Get());
						lFrameTask.Cancel();
					}
					mFrameTaskPoolingQueue.Enqueue(lFrameTask);
				}
			}
		}

		// 이벤트 콜백을 삭제합니다.
		aOnFrameChangeDelegate = null;
		aOnFrameSliceChangeDelegate = null;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임을 진행시킵니다.
	public void UpdateFrameDelta(int pFrameDelta, int pFrameIdxUpdateLimit)
	{
		if (mIsStopped)
			return;

		if ((mFrameSliceIdx == 0) && // 첫 프레임 조각이고
			(mFrameIdx >= pFrameIdxUpdateLimit)) // 갱신 제한에 도달하면 
		{
			// 아무 처리를 하지 않습니다.
			mFrameTimer = 0;
			return;
		}
		else // 갱신 제한에 걸리지 않았다면
		{
			// 프레임 조각 단위로 갱신합니다.
			mFrameTimer += pFrameDelta;
			while (mFrameTimer >= mFrameSliceDelta)
			{
				// 현재 프레임 조각만을 갱신합니다.
				_UpdateFrameSlice(mFrameSliceIdx);

				// 프레임 시간을 증가시킵니다.
				mFrameTimer -= mFrameSliceDelta;
				++mFrameSliceIdx;
				if (mFrameSliceIdx == mFrameSliceCount)
				{
					if (!mIsRequestStopUpdate)
						_UpdateFrameIdx();
					else
						mIsStopped = true;
					mFrameSliceIdx = 0;

					if (mIsStopped || // 중단되었거나
						(mFrameIdx >= pFrameIdxUpdateLimit)) // 갱신 제한에 도달하면 
					{
						// 갱신을 멈춥니다.
						mFrameTimer = 0;
						return;
					}
				}
//				mFrameTimer = 0; // 속도가 빨라도 한 프레임 이상 진행 안되게 테스트 할 때 주석을 품
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 한 프레임을 갱신합니다.
	public void UpdateFrame()
	{
		mIsStopped = mIsRequestStopUpdate; // 프레임 단위의 갱신일 경우는 요청과 중단이 바로 동기화 됨
		if (mIsStopped)
			return;

		for (int iSlice = 0; iSlice < mFrameSliceCount; ++iSlice)
			_UpdateFrameSlice(iSlice);

		_UpdateFrameIdx();
	}
	//-------------------------------------------------------------------------------------------------------
	// 갱신을 중단을 지정합니다.
	public void StopUpdate(bool pIsStopUpdate)
	{
		//Debug.Log("StopUpdate() pIsStopUpdate=" + pIsStopUpdate);

		if (pIsStopUpdate)
		{
			mIsRequestStopUpdate = true; // 이후로 모든 프레임 조각의 갱신이 끝나야 mIsStopped가 true가 됨
		}
		else
		{
			mIsRequestStopUpdate = false;
			mIsStopped = false;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// 오브젝트 GUID를 생성합니다.
	public uint GenerateObjectGuid()
	{
		return ++mObjectGuidGenerator;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 조각 인덱스를 생성합니다.
	public int GenerateFrameSliceIdx()
	{
		int lGenerationSliceIndex = 0;
		for (int iSlice = 1; iSlice < mFrameSliceCount; ++iSlice)
		{
			if (mFrameSlotItemCounts[lGenerationSliceIndex] > mFrameSlotItemCounts[iSlice])
				lGenerationSliceIndex = iSlice;
		}
		++mFrameSlotItemCounts[lGenerationSliceIndex]; // 카운터를 증가
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		String lFrameSlotItemCountsString = mFrameSlotItemCounts[0].ToString();
		for (int iSlice = 1; iSlice < mFrameSliceCount; ++iSlice)
			lFrameSlotItemCountsString += ", " + mFrameSlotItemCounts[iSlice];
		Logger.Log(String.Format("[{0}] GenerateFrameSliceIdx({1}) for Guid({2})", mFrameIdx, lFrameSlotItemCountsString, mObjectGuidGenerator - 1));
	#endif
		return lGenerationSliceIndex;
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 조각 인덱를 삭제합니다.
	public void DeleteFrameSliceIdx(int pFrameSliceIdx)
	{
		--mFrameSlotItemCounts[pFrameSliceIdx];
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		String lFrameSlotItemCountsString = mFrameSlotItemCounts[0].ToString();
		for (int iSlice = 1; iSlice < mFrameSliceCount; ++iSlice)
			lFrameSlotItemCountsString += ", " + mFrameSlotItemCounts[iSlice];
		Logger.Log(String.Format("[{0}] DeleteFrameSliceIdx({1}) for Slice({2})", mFrameIdx, lFrameSlotItemCountsString, pFrameSliceIdx));
	#endif
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 태스크를 매 프레임마다 갱신합니다.
	public FrameTask RepeatTask(IFrameObject pFrameObject, FrameTaskDelegate pRepeatTaskDelegate, FrameUpdateLayer pFrameUpdateLayer)
	{
		//Debug.Log("RepeatTask() pFrameObject=" + pFrameObject + " mFrameIdx=" + mFrameIdx);

		// 태스크 아이템을 생성합니다.
		FrameTask lFrameTask;
		if (mFrameTaskPoolingQueue.Count <= 0)
			lFrameTask = new FrameTask();
		else
			lFrameTask = mFrameTaskPoolingQueue.Dequeue();
		lFrameTask.InitRepeat(pFrameObject, pRepeatTaskDelegate);

		// Guid를 할당합니다.
		lFrameTask.mGuid = ++mTaskGuidGenerator;
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log(String.Format("[{0}] RepeatTask(Guid:{1}) pFrameObject={2}", mFrameIdx, lFrameTask.mGuid, pFrameObject));
	#endif

		// 생성한 태스크를 반복 대기 큐에 넣습니다.
		mRepeatTaskAddQueue.Enqueue(lFrameTask);

		return lFrameTask;
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 태스크를 예약합니다.
	public FrameTask ReserveTask(IFrameObject pFrameObject, FrameTaskDelegate pReservationTaskDelegate, int pReservationDelay)
	{
		//Debug.Log("ReserveTask() pFrameObject=" + pFrameObject + " pReservationDelay=" + pReservationDelay + " mFrameIdx=" + mFrameIdx);

		// 태스크 아이템을 생성합니다.
		FrameTask lFrameTask;
		if (mFrameTaskPoolingQueue.Count <= 0)
			lFrameTask = new FrameTask();
		else
			lFrameTask = mFrameTaskPoolingQueue.Dequeue();

		// Guid를 할당합니다.
		lFrameTask.mGuid = ++mTaskGuidGenerator;
	#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
		Logger.Log(String.Format("[{0}] ReserveTask(Guid:{1}) pFrameObject={2}", mFrameIdx, lFrameTask.mGuid, pFrameObject));
	#endif
		lFrameTask.InitReservation(pFrameObject, pReservationTaskDelegate, pReservationDelay);

		// 생성한 태스크를 예약 큐에 넣습니다.
		_AddReservationTask(lFrameTask, pFrameObject.aUpdateFrameSliceIdx);

		return lFrameTask;
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 예약 태스크를 슬롯에 넣습니다.
	private void _AddReservationTask(FrameTask pReservationTask, int pFrameSliceIdx)
	{
		int lFrameDelay = pReservationTask.mReservationDelay;
		if (lFrameDelay > mTaskFrameRange) // 한 번 예약으로 가능하지 않은 긴 예약의 경우
		{
			lFrameDelay = mTaskFrameRange;
			pReservationTask.mReservationDelay = pReservationTask.mReservationDelay - mTaskFrameRange; // 나중에 한 번 더 예약하기 위해서 초과 시간을 저장합니다.
		}
		else // 최대 시간을 초과하지 않은 일반 태스크는
			pReservationTask.mReservationDelay = 0; // 예약 딜레이를 0으로 해서 바로 처리하도록 합니다.

		int lFrameIdxDelay;
		if (lFrameDelay > 0)
			lFrameIdxDelay = (lFrameDelay + mFrameDeltaMinusOne) / mFrameDelta; // CeilToInt 처리
		else
			lFrameIdxDelay = 1;

		//Debug.Log("pReservationDelay=" + pReservationDelay);
		//Debug.Log("mCurrentReservationTaskSlotIdx=" + mCurrentReservationTaskSlotIdx);
		//Debug.Log("lFrameIdxDelay=" + lFrameIdxDelay);
		//Debug.Log("(mCurrentReservationTaskSlotIdx + lFrameIdxDelay) % mReservationTaskFrameIdxRange=" + ((mCurrentReservationTaskSlotIdx + lFrameIdxDelay) % mReservationTaskFrameIdxRange));
		//Debug.Log("mReservationTaskSlots.Length=" + mReservationTaskSlots.Length);

		ReservationTaskSlot lReservationTaskSlot = mReservationTaskSlots[(mCurrentReservationTaskSlotIdx + lFrameIdxDelay) % mReservationTaskFrameIdxRange][pFrameSliceIdx];
		lReservationTaskSlot.mFrameTaskQueue.Enqueue(pReservationTask);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 프레임 조각을 갱신합니다.
	private void _UpdateFrameSlice(int pFrameSliceIdx)
	{
		//Debug.Log("_UpdateFrameSlice() pFrameSliceIdx=" + pFrameSliceIdx);
		//Debug.Log("mFrameIdx=" + mFrameIdx + " mRepeatTaskLists.First=" + mRepeatTaskLists.First + " mRepeatTaskAddQueue.Count=" + mRepeatTaskAddQueue.Count);
		//Debug.Log("mFrameIdx=" + mFrameIdx + " mRepeatTaskLists.First.Value=" + mRepeatTaskLists.First.Value + " mRepeatTaskAddQueue.First.Value=" + mRepeatTaskAddQueue.First.Value);

		LinkedList<FrameTask> lRepeatTaskList = mRepeatTaskLists[pFrameSliceIdx];

		// 지금까지 추가된 노드들을 갱신 리스트에 넣습니다.
		//Debug.Log("mRepeatTaskAddQueue.Count=" + mRepeatTaskAddQueue.Count);
		while (mRepeatTaskAddQueue.Count > 0)
		{
			FrameTask lRepeatTask = mRepeatTaskAddQueue.Dequeue();
			if (lRepeatTask.mObjectLink.Get() != null)
			{
				mRepeatTaskLists[lRepeatTask.mObjectLink.Get().aUpdateFrameSliceIdx].AddLast(lRepeatTask.mUpdateNode);
			#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
				Logger.Log(String.Format("[{0}] mRepeatTaskLists[SliceIdx:{1}].AddLast(Count:{2})", mFrameIdx, lRepeatTask.mObjectLink.Get().aUpdateFrameSliceIdx, mRepeatTaskLists[lRepeatTask.mObjectLink.Get().aUpdateFrameSliceIdx].Count));
			#endif
			}
		}

		// 이번 프레임 번호에서 갱신할 아이템을 처리합니다.
		LinkedListNode<FrameTask> lRepeatTaskNode = lRepeatTaskList.First;
		while (lRepeatTaskNode != null)
		{
			FrameTask lRepeatTask = lRepeatTaskNode.Value;
			lRepeatTaskNode = lRepeatTaskNode.Next; // 삭제될 수 있으므로 노드를 미리 다음 아이템으로 옮겨놓습니다.
			//Debug.Log("_UpdateFrameSlice() lRepeatTask=" + lRepeatTask);

			// 아이템을 갱신합니다.
			if (lRepeatTask.mObjectLink.Get() != null)
			{
				lRepeatTask.mTaskDelegate(lRepeatTask, mFrameDelta); // 태스크를 수행
				mStatistics.IncrementRepeatTaskUpdateCount(); // 통계치 반영
			}
			else
			{
				lRepeatTaskList.Remove(lRepeatTask.mUpdateNode); // 태스크를 갱신 리스트에서 뺍니다.
			#if (UNITY_EDITOR || BATTLE_VERIFY) && BATTLE_VERIFY_TEST
				Logger.Log(String.Format("[{0}] mRepeatTaskLists[SliceIdx:{1}].Remove(Count:{2})", mFrameIdx, pFrameSliceIdx, lRepeatTaskList.Count));
			#endif
				mFrameTaskPoolingQueue.Enqueue(lRepeatTask); // 빼낸 태스크는 팩토리에 넣습니다.
			}
		}

		// 이번 프레임에 예약된 태스크를 처리합니다.
		Queue<FrameTask> lFrameTaskQueue = mReservationTaskSlots[mCurrentReservationTaskSlotIdx][pFrameSliceIdx].mFrameTaskQueue;
		while (lFrameTaskQueue.Count > 0)
		{
			//Debug.Log("mCurrentReservationTaskSlotIdx=" + mCurrentReservationTaskSlotIdx);
			//Debug.Log("lFrameTaskQueue.First.Value.mTaskDelegate=" + lFrameTaskQueue.First.Value.mTaskDelegate);

			FrameTask lFrameTask = lFrameTaskQueue.Dequeue();
			if (lFrameTask.mObjectLink.Get() != null)
			{
				--lFrameTask.mObjectLink.Get().aTaskCount; // 태스크 개수를 감소시키고

				if (lFrameTask.mReservationDelay <= 0)
				{
					lFrameTask.mTaskDelegate(lFrameTask, -lFrameTask.mReservationDelay); // 태스크를 수행
					mStatistics.IncrementReservationTaskUpdateCount(); // 통계치 반영
					lFrameTask.Cancel();
					mFrameTaskPoolingQueue.Enqueue(lFrameTask); // 처리 완료후 재활용 풀에 돌려놓습니다.
				}
				else
					_AddReservationTask(lFrameTask, pFrameSliceIdx); // 다시 넣습니다.
			}
			else
			{
				//Debug.Log("if (lFrameTask.mObjectLink != null)");
				mFrameTaskPoolingQueue.Enqueue(lFrameTask); // 처리는 안했지만 소용없게 되었으므로 재활용 풀에 돌려놓습니다.
			}
		}

		// 프레임 조각 변화 콜백을 호출합니다.
		if (aOnFrameSliceChangeDelegate != null)
			aOnFrameSliceChangeDelegate(mFrameIdx, pFrameSliceIdx, mFrameDelta);
	}
	//-------------------------------------------------------------------------------------------------------
	// 프레임 번호를 증가시킵니다.
	private void _UpdateFrameIdx()
	{
		++mFrameIdx;
		mElapsedTime += mFrameDelta;

		mCurrentReservationTaskSlotIdx = (mCurrentReservationTaskSlotIdx + 1) % mReservationTaskFrameIdxRange; // 슬롯 인덱스를 증가(루프)시킵니다.

		// 프레임 변화 콜백을 호출합니다.
		if (aOnFrameChangeDelegate != null)
			aOnFrameChangeDelegate(mFrameIdx);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private uint mObjectGuidGenerator;
	private uint mTaskGuidGenerator;

	private int mFrameDelta;
	private int mFrameSliceCount;
	private int mFrameSliceDelta;
	private int mTaskFrameRange;
	private float mFramePerSecond;
	private int mFrameDeltaMinusOne;

	private bool mIsRequestStopUpdate;
	private bool mIsStopped;
	private int mFrameTimer;
	private int mFrameSliceIdx;
	private int mFrameIdx;
	private int mElapsedTime;

	private int[] mFrameSlotItemCounts;
	private Queue<FrameTask> mFrameTaskPoolingQueue;

	private LinkedList<FrameTask>[] mRepeatTaskLists;
	private Queue<FrameTask> mRepeatTaskAddQueue;

	private int mReservationTaskFrameIdxRange;
	private ReservationTaskSlot[][] mReservationTaskSlots;
	private int mCurrentReservationTaskSlotIdx;

	private FrameUpdaterStatistics mStatistics;
}
