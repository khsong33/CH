using UnityEngine;
using System.Collections;
using System;

public class GuidLink<T_GuidObject> where T_GuidObject : IGuidObject
{
	// 링크를 지정합니다.
	public void Set(T_GuidObject pGuidObject)
	{
		mGuidObject = pGuidObject;
		if (pGuidObject != null)
			mGuid = pGuidObject.aGuid;
	}

	// 링크를 얻습니다.
	public T_GuidObject Get()
	{
		if (mGuidObject != null)
		{
			if ((mGuid == mGuidObject.aGuid) &&
				(mGuid > 0))
			{
				return mGuidObject;
			}
			else
			{
				//Debug.Log("mGuidObject=" + mGuidObject);
				mGuidObject = default(T_GuidObject);
				return default(T_GuidObject);
			}
		}
		else
			return default(T_GuidObject);
	}

	// 링크를 초기화합니다.
	public void Reset()
	{
		mGuidObject = default(T_GuidObject);
	}

	private T_GuidObject mGuidObject;
	private uint mGuid = 0;
}
