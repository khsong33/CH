using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public delegate void OnEnumerateGuidObjectDelegate<T_GuidObject>(T_GuidObject pGuidObject);

public class GuidObjectChainItem<T_GuidObject> where T_GuidObject : IGuidObject
{
	// 아이템에 들어있는 오브젝트
	public T_GuidObject Value
	{
		get { return mGuidLink.Get(); }
	}

	// 아이템을 생성합니다.
	public static GuidObjectChainItem<T_GuidObject> Create(T_GuidObject pGuidObject)
	{
		GuidObjectChainItem<T_GuidObject> lItem;
		if (sItemFactoryList.First == null)
		{
			lItem = new GuidObjectChainItem<T_GuidObject>();
		}
		else
		{
			LinkedListNode<GuidObjectChainItem<T_GuidObject>> lItemNode = sItemFactoryList.First;
			sItemFactoryList.RemoveFirst();
			lItem = lItemNode.Value;
		}

		lItem.mGuidLink.Set(pGuidObject);

		return lItem;
	}

	// 아이템을 비웁니다.
	public void Empty()
	{
		mGuidLink.Set(default(T_GuidObject));
	}

	internal GuidObjectChainItem()
	{
		mNode = new LinkedListNode<GuidObjectChainItem<T_GuidObject>>(this);

		mGuidLink = new GuidLink<T_GuidObject>();
	}

	internal static LinkedList<GuidObjectChainItem<T_GuidObject>> sItemFactoryList = new LinkedList<GuidObjectChainItem<T_GuidObject>>();
	internal LinkedListNode<GuidObjectChainItem<T_GuidObject>> mNode;

	internal GuidLink<T_GuidObject> mGuidLink;
}

public class GuidObjectChain<T_GuidObject> where T_GuidObject : IGuidObject
{
	// 생성자
	public GuidObjectChain()
	{
		mGuidObjectChainItemList = new LinkedList<GuidObjectChainItem<T_GuidObject>>();
		mEnumeratorCount = 0;
	}

	// 비웁니다.
	public void Clear()
	{
		if (mEnumeratorCount == 1) // 참조중인 열거자가 없다면
		{
			// 모든 노드를 삭제하고 팩토리로 돌려놓습니다.
			LinkedListNode<GuidObjectChainItem<T_GuidObject>> lNode = mGuidObjectChainItemList.First;
			while (lNode != null)
			{
				lNode.Value.Empty();
				mGuidObjectChainItemList.Remove(lNode);
				GuidObjectChainItem<T_GuidObject>.sItemFactoryList.AddFirst(lNode); // 노드를 팩토리에 반환
				lNode = mGuidObjectChainItemList.First;
			}
		}
		else
		{
			// 모든 노드를 비우기만 합니다.
			LinkedListNode<GuidObjectChainItem<T_GuidObject>> lNode = mGuidObjectChainItemList.First;
			while (lNode != null)
			{
				lNode.Value.Empty();
				lNode = lNode.Next;
			}
		}
	}

	// 오브젝트를 추가합니다.
	public GuidObjectChainItem<T_GuidObject> AddFirst(T_GuidObject pGuidObject)
	{
		GuidObjectChainItem<T_GuidObject> lGuidObjectChainItem = GuidObjectChainItem<T_GuidObject>.Create(pGuidObject);
		mGuidObjectChainItemList.AddFirst(lGuidObjectChainItem.mNode);

		return lGuidObjectChainItem;
	}

	// 오브젝트를 추가합니다.
	public GuidObjectChainItem<T_GuidObject> AddLast(T_GuidObject pGuidObject)
	{
		GuidObjectChainItem<T_GuidObject> lGuidObjectChainItem = GuidObjectChainItem<T_GuidObject>.Create(pGuidObject);
		mGuidObjectChainItemList.AddLast(lGuidObjectChainItem.mNode);

		return lGuidObjectChainItem;
	}

	// 내부적으로 열거자를 생성해서 반복합니다.
	public void Enumerate(OnEnumerateGuidObjectDelegate<T_GuidObject> pOnEnumerateGuidObjectDelegate)
	{
		using (GuidObjectChainEnumerator<T_GuidObject> lEnumerator = CreateEnumerator())
		{
			while (lEnumerator.MoveNext())
			{
				T_GuidObject lObject = lEnumerator.GetCurrent();
				pOnEnumerateGuidObjectDelegate(lObject);
			}
		}
	}

	// 열거자를 생성합니다.
	public GuidObjectChainEnumerator<T_GuidObject> CreateEnumerator()
	{
		GuidObjectChainEnumerator<T_GuidObject> lItemEnumerator;
		if (GuidObjectChainEnumerator<T_GuidObject>.sItemEnumeratorFactoryList.First == null)
		{
			lItemEnumerator = new GuidObjectChainEnumerator<T_GuidObject>();
		}
		else
		{
			LinkedListNode<GuidObjectChainEnumerator<T_GuidObject>> lItemEnumeratorNode = GuidObjectChainEnumerator<T_GuidObject>.sItemEnumeratorFactoryList.First;
			GuidObjectChainEnumerator<T_GuidObject>.sItemEnumeratorFactoryList.RemoveFirst();
			lItemEnumerator = lItemEnumeratorNode.Value;
		}

		lItemEnumerator.mChain = this;
		lItemEnumerator.mGuidObjectChainItemList = mGuidObjectChainItemList;
		lItemEnumerator.Reset();

		++mEnumeratorCount;
		//Debug.Log("CreateEnumerator() mEnumeratorCount=" + mEnumeratorCount);

		return lItemEnumerator;
	}

	// 유효한 아이템 개수를 셉니다.
	public int CountValidItem()
	{
		int lValidItemCount = 0;

		LinkedListNode<GuidObjectChainItem<T_GuidObject>> lNode = mGuidObjectChainItemList.First;
		while (lNode != null)
		{
			if (lNode.Value.mGuidLink.Get() != null)
				++lValidItemCount;

			lNode = lNode.Next;
		}

		return lValidItemCount;
	}

	// 유효한 아이템이 있는지 검사합니다.
	public bool CheckValidItem()
	{
		LinkedListNode<GuidObjectChainItem<T_GuidObject>> lNode = mGuidObjectChainItemList.First;
		while (lNode != null)
		{
			if (lNode.Value.mGuidLink.Get() != null)
				return true;

			lNode = lNode.Next;
		}

		return false;
	}

	internal LinkedList<GuidObjectChainItem<T_GuidObject>> mGuidObjectChainItemList;
	internal int mEnumeratorCount;
}

public class GuidObjectChainEnumerator<T_GuidObject> : IDisposable, IEnumerator where T_GuidObject : IGuidObject
{
	public void Dispose()
	{
		--mChain.mEnumeratorCount;
		//Debug.Log("Dispose() mEnumeratorCount=" + mChain.mEnumeratorCount);
	}

	public object Current
	{
		get { return GetCurrent(); }
	}
	public bool MoveNext()
	{
		if (mIsReset)
		{
			mIsReset = false;
			return (GetFirst() != null);
		}
		else
		{
			return (GetNext() != null);
		}
	}
	public void Reset()
	{
		mIsReset = true;
	}

	// 열거자를 초기화하고 첫 번째 오브젝트를 얻습니다.
	internal T_GuidObject GetFirst()
	{
		mCurrentGuidObjectChainItemNode = mGuidObjectChainItemList.First;
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);

		T_GuidObject lGuidObject = mCurrentGuidObjectChainItemNode.Value.mGuidLink.Get();
		if (lGuidObject != null) // 아이템에 오브젝트가 있다면
		{
			// 오브젝트를 반환합니다.
			return lGuidObject;
		}
		else // 아이템에 오브젝트가 없다면
		{
			if (mChain.mEnumeratorCount == 1) // 다른 열거자가 없다면
			{
				// 지금의 첫 노드를 삭제하고 다시 재귀 호출로 첫 노드를 반환합니다.
				//Debug.Log("GetFirst() mChain.mEnumeratorCount=" + mChain.mEnumeratorCount);
				mGuidObjectChainItemList.RemoveFirst();
				GuidObjectChainItem<T_GuidObject>.sItemFactoryList.AddFirst(mCurrentGuidObjectChainItemNode); // 노드를 팩토리에 반환
				return GetFirst();
			}
			else
			{
				// 빈 아이템을 건너뛰고 다음 아이템을 반환합니다.
				return GetNext();
			}
		}
	}

	// 열거자를 초기화하고 마지막 오브젝트를 얻습니다.
	internal T_GuidObject GetLast()
	{
		mCurrentGuidObjectChainItemNode = mGuidObjectChainItemList.Last;
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);

		T_GuidObject lGuidObject = mCurrentGuidObjectChainItemNode.Value.mGuidLink.Get();
		if (lGuidObject != null) // 아이템에 오브젝트가 있다면
		{
			// 오브젝트를 반환합니다.
			return lGuidObject;
		}
		else // 아이템에 오브젝트가 없다면
		{
			if (mChain.mEnumeratorCount == 1) // 다른 열거자가 없다면
			{
				// 지금의 마지막 노드를 삭제하고 다시 재귀 호출로 마지막 노드를 반환합니다.
				//Debug.Log("GetLast() mChain.mEnumeratorCount=" + mChain.mEnumeratorCount);
				mGuidObjectChainItemList.RemoveLast();
				GuidObjectChainItem<T_GuidObject>.sItemFactoryList.AddFirst(mCurrentGuidObjectChainItemNode); // 노드를 팩토리에 반환
				return GetLast();
			}
			else
			{
				// 빈 아이템을 건너뛰고 이전 아이템을 반환합니다.
				return GetPrevious();
			}
		}
	}

	// 열거자를 이전 오브젝트로 옮겨가서 오브젝트를 얻습니다.
	internal T_GuidObject GetPrevious()
	{
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);
		mCurrentGuidObjectChainItemNode = mCurrentGuidObjectChainItemNode.Previous;
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);

		T_GuidObject lGuidObject = mCurrentGuidObjectChainItemNode.Value.mGuidLink.Get();
		if (lGuidObject != null) // 아이템에 오브젝트가 있다면
		{
			// 오브젝트를 반환합니다.
			return lGuidObject;
		}
		else // 아이템에 오브젝트가 없다면
		{
			if (mChain.mEnumeratorCount == 1) // 다른 열거자가 없다면
			{
				// 지금의 노드를 삭제하고 커서를 지금 노드의 다음으로 옮긴 다음에 재귀적으로 다시 이전 오브젝트를 반환합니다.
				//Debug.Log("GetPrevious() mChain.mEnumeratorCount=" + mChain.mEnumeratorCount);
				LinkedListNode<GuidObjectChainItem<T_GuidObject>> lNextNode = mCurrentGuidObjectChainItemNode.Next;
				if (lNextNode == null)
					lNextNode = mGuidObjectChainItemList.Last;
				mGuidObjectChainItemList.Remove(mCurrentGuidObjectChainItemNode);
				GuidObjectChainItem<T_GuidObject>.sItemFactoryList.AddFirst(mCurrentGuidObjectChainItemNode); // 노드를 팩토리에 반환
				mCurrentGuidObjectChainItemNode = lNextNode;
				return GetPrevious();
			}
			else
			{
				// 빈 아이템을 건너뛰고 이전 아이템을 반환합니다.
				return GetPrevious();
			}
		}
	}

	// 열거자를 다음 오브젝트로 옮겨가서 오브젝트를 얻습니다.
	internal T_GuidObject GetNext()
	{
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);
		mCurrentGuidObjectChainItemNode = mCurrentGuidObjectChainItemNode.Next;
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);

		T_GuidObject lGuidObject = mCurrentGuidObjectChainItemNode.Value.mGuidLink.Get();
		if (lGuidObject != null) // 아이템에 오브젝트가 있다면
		{
			// 오브젝트를 반환합니다.
			return lGuidObject;
		}
		else // 아이템에 오브젝트가 없다면
		{
			if (mChain.mEnumeratorCount == 1) // 다른 열거자가 없다면
			{
				// 지금의 노드를 삭제하고 커서를 지금 노드의 이전으로 옮긴 다음에 재귀적으로 다시 다음 오브젝트를 반환합니다.
				//Debug.Log("GetNext() mChain.mEnumeratorCount=" + mChain.mEnumeratorCount);
				LinkedListNode<GuidObjectChainItem<T_GuidObject>> lPreviousNode = mCurrentGuidObjectChainItemNode.Previous;
				if (lPreviousNode == null)
					lPreviousNode = mGuidObjectChainItemList.First;
				mGuidObjectChainItemList.Remove(mCurrentGuidObjectChainItemNode);
				GuidObjectChainItem<T_GuidObject>.sItemFactoryList.AddLast(mCurrentGuidObjectChainItemNode); // 노드를 팩토리에 반환
				mCurrentGuidObjectChainItemNode = lPreviousNode;
				return GetNext();
			}
			else
			{
				// 빈 아이템을 건너뛰고 다음 아이템을 반환합니다.
				return GetNext();
			}
		}
	}

	// 현재 열거자의 오브젝트를 얻습니다.
	internal T_GuidObject GetCurrent()
	{
		if (mCurrentGuidObjectChainItemNode == null)
			return default(T_GuidObject);

		return mCurrentGuidObjectChainItemNode.Value.mGuidLink.Get();
	}

	internal static LinkedList<GuidObjectChainEnumerator<T_GuidObject>> sItemEnumeratorFactoryList = new LinkedList<GuidObjectChainEnumerator<T_GuidObject>>();
	//	internal LinkedListNode<GuidObjectChainEnumerator<T_GuidObject>> mNode;

	internal GuidObjectChain<T_GuidObject> mChain;
	internal LinkedList<GuidObjectChainItem<T_GuidObject>> mGuidObjectChainItemList;
	internal LinkedListNode<GuidObjectChainItem<T_GuidObject>> mCurrentGuidObjectChainItemNode;
	internal bool mIsReset;
}
