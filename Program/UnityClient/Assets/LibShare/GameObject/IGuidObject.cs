using UnityEngine;
using System.Collections;
using System;

public interface IGuidObject
{
	uint aGuid { get; }

	void ResetGuid();
}
