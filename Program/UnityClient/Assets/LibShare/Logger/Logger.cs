using UnityEngine;
using System.Collections;
using System;

public class Logger
{
	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스를 지정합니다.
	public static void SetInstance(ILogger pILogger)
	{
		sILoggerInstance = pILogger;
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void Log(String pLogMessage)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.Log(pLogMessage);
		else
			Debug.Log(pLogMessage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void LogFormat(String pLogMessage, params object[] args)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.LogFormat(pLogMessage, args);
		else
			Debug.LogFormat(pLogMessage, args);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void LogWarning(String pLogMessage)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.LogWarning(pLogMessage);
		else
			Debug.LogWarning(pLogMessage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void LogWarningFormat(String pLogMessage, params object[] args)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.LogWarningFormat(pLogMessage, args);
		else
			Debug.LogWarningFormat(pLogMessage, args);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void LogError(String pLogMessage)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.LogError(pLogMessage);
		else
			Debug.LogError(pLogMessage);
	}
	//-------------------------------------------------------------------------------------------------------
	// 전역 로거 인터페이스로 로그를 출력합니다.
	public static void LogErrorFormat(String pLogMessage, params object[] args)
	{
		if (sILoggerInstance != null)
			sILoggerInstance.LogErrorFormat(pLogMessage, args);
		else
			Debug.LogErrorFormat(pLogMessage, args);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private static ILogger sILoggerInstance;
}
