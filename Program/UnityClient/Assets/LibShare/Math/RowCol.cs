using UnityEngine;
using System.Collections;
using System.Text;
using System;

[Serializable]
public struct RowCol
{
	public const float cToRowCol = 100.0f;
	public const float cToWorld = 0.01f;

	public int row;
	public int col;

	public RowCol(int pRow, int pCol)
	{
		row = pRow;
		col = pCol;
	}

	public static RowCol operator -(RowCol pA)
	{
		return new RowCol(-pA.row, -pA.col);
	}
	public static RowCol operator -(RowCol pA, RowCol pB)
	{
		return new RowCol(pA.row - pB.row, pA.col - pB.col);
	}
	public static bool operator !=(RowCol pLhs, RowCol pRhs)
	{
		return ((pLhs.row != pRhs.row) || (pLhs.col != pRhs.col)) ? true : false;
	}
	public static RowCol operator *(int pD, RowCol pA)
	{
		return new RowCol(pD * pA.row, pD * pA.col);
	}
	public static RowCol operator *(RowCol pA, int pD)
	{
		return new RowCol(pA.row * pD, pA.col * pD);
	}
	public static RowCol operator /(RowCol pA, int pD)
	{
		return new RowCol(pA.row / pD, pA.col / pD);
	}
	public static RowCol operator +(RowCol pA, RowCol pB)
	{
		return new RowCol(pA.row + pB.row, pA.col + pB.col);
	}
	public static bool operator ==(RowCol pLhs, RowCol pRhs)
	{
		return ((pLhs.row == pRhs.row) && (pLhs.col == pRhs.col)) ? true : false;
	}

	public override bool Equals(object other)
	{
		return other.GetHashCode() == GetHashCode();
	}
	public override int GetHashCode()
	{
		// bijective algorithm
		int tmp = (row + ((col + 1) / 2));
		return col + (tmp * tmp);
	}
	public static RowCol Max(RowCol pLhs, RowCol pRhs)
	{
		return new RowCol(Mathf.Max(pLhs.row, pRhs.row), Mathf.Max(pLhs.col, pRhs.col));
	}
	public static RowCol Min(RowCol pLhs, RowCol pRhs)
	{
		return new RowCol(Mathf.Min(pLhs.row, pRhs.row), Mathf.Min(pLhs.col, pRhs.col));
	}
#if !BATTLE_VERIFY
	public static RowCol FromVector2(Vector2 pVector2)
	{
		return new RowCol((int)pVector2.y, (int)pVector2.x);
	}
	public static RowCol FromVectorXY(Vector3 pVector3)
	{
		return new RowCol((int)pVector3.y, (int)pVector3.x);
	}
	public static RowCol FromVectorXZ(Vector3 pVector3)
	{
		return new RowCol((int)pVector3.z, (int)pVector3.x);
	}
	public static Vector2 ToVector2(RowCol pRowCol)
	{
		return new Vector2(pRowCol.row, pRowCol.col);
	}
	public static Vector3 ToVectorXY(RowCol pRowCol)
	{
		return new Vector3(pRowCol.row, pRowCol.col, 0.0f);
	}
	public static Vector3 ToVectorXZ(RowCol pRowCol)
	{
		return new Vector3(pRowCol.row, 0.0f, pRowCol.col);
	}
	public static RowCol ToRowCol(Vector3 pWorld)
	{
		return new RowCol((int)(pWorld.z * cToRowCol), (int)(pWorld.x * cToRowCol));
	}
	public static Vector3 ToWorld(RowCol pRowCol)
	{
		return new Vector3(pRowCol.col * cToWorld, 0.0f, pRowCol.row * cToWorld);
	}
	public static Vector3 ToWorld(RowCol pRowCol, float pWorldY)
	{
		return new Vector3(pRowCol.col * cToWorld, pWorldY, pRowCol.row * cToWorld);
	}
#endif
	public static int SqrMagnitude(RowCol pA)
	{
		return (pA.row * pA.row + pA.col * pA.col);
	}
#if !BATTLE_VERIFY
	public static int Magnitude(RowCol pA)
	{
		return (int)Mathf.Sqrt(pA.row * pA.row + pA.col * pA.col);
	}
	public static float GetDirectionY(Quaternion pRotation)
	{
		return pRotation.eulerAngles.y;
	}
	public static float GetDirectionY(RowCol pRowColVector)
	{
		float lDirectionY = Mathf.Atan2(pRowColVector.col, pRowColVector.row) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static float GetDirectionY(Vector2 pWorldVector2)
	{
		float lDirectionY = Mathf.Atan2(pWorldVector2.x, pWorldVector2.y) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static float GetDirectionY(Vector3 pWorldVector3)
	{
		float lDirectionY = Mathf.Atan2(pWorldVector3.x, pWorldVector3.z) * Mathf.Rad2Deg;
		return lDirectionY;
	}
	public static Quaternion GetRotation(float pDirectionY)
	{
		return Quaternion.Euler(0.0f, pDirectionY, 0.0f);
	}
	public static Quaternion GetRotation(RowCol pRowColVector)
	{
		return Quaternion.LookRotation(ToWorld(pRowColVector));
	}
	public static Quaternion GetRotation(Vector2 pWorldVector2)
	{
		return Quaternion.LookRotation(new Vector3(-pWorldVector2.x, 0.0f, -pWorldVector2.y));
	}
	public static Quaternion GetRotation(Vector3 pWorldVector3)
	{
		return Quaternion.LookRotation(new Vector3(-pWorldVector3.x, 0.0f, -pWorldVector3.z));
	}
	public static RowCol GetDirectionVector(float pDirectionY, int pSize)
	{
// 		RowCol lDirectionVector = new RowCol(
// 			-(int)(pSize * Mathf.Sin(pDirectionY * Mathf.Deg2Rad)),
// 			-(int)(pSize * Mathf.Cos(pDirectionY * Mathf.Deg2Rad)));
// 		Debug.Log("pDirectionY=" + pDirectionY + " pSize=" + pSize + " lDirectionVector=" + lDirectionVector);
// 		return lDirectionVector;
		return new RowCol(
			(int)(pSize * Mathf.Sin(pDirectionY * Mathf.Deg2Rad)),
			(int)(pSize * Mathf.Cos(pDirectionY * Mathf.Deg2Rad)));
	}
#endif
	public override string ToString()
	{
		return String.Format("({0},{1})", row, col);
	}
	public string ToString(string pFormat)
	{
		return String.Empty;
	}
	
	public static RowCol zero = new RowCol(0, 0);
}
