using UnityEngine;
using System.Collections;
using System;

public class SyncRandom
{
	public const int cStateCount = 16;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	// 생성자
	public SyncRandom()
	{
		mStates = new UInt32[cStateCount];
	}
	//-------------------------------------------------------------------------------------------------------
	// 랜덤으로 초기화합니다.
	public void Randomize()
	{
		System.Random mRandom = new System.Random();

		mIndex = mRandom.Next(0, cStateCount - 1);

		for (int i = 0; i < cStateCount; i++)
			mStates[i] = (UInt32)mRandom.Next(0, 10000);
	}
	//-------------------------------------------------------------------------------------------------------
	// 동기화 정보로 초기화합니다.
	public void RandomizeSync(UInt32[] pStates, int pRandomSeed)
	{
		for (int i = 0; i < pStates.Length; i++)
			mStates[i] = pStates[i];

		mIndex = pRandomSeed;
	}
	//-------------------------------------------------------------------------------------------------------
	// 고정 정보로 초기화합니다.
	public void RandomizeStatic(int pRandomSeed)
	{
		for (int i = 0; i < mStates.Length; i++)
			mStates[i] = (UInt32)(100 * (i + 1));

		mIndex = pRandomSeed % cStateCount; // Clamp(0, cStateCount - 1) 효과
	}
	//-------------------------------------------------------------------------------------------------------
	// 랜덤 함수
	public uint Range()
	{
		uint a, b, c, d;

		a = mStates[mIndex];
		c = mStates[(mIndex + 13) & 15];
		b = a ^ c ^ (a << 16) ^ (c << 15);
		c = mStates[(mIndex + 9) & 15];
		c ^= (c >> 11);
		a = mStates[mIndex] = b ^ c;
		d = a ^ ((a << 5) & 0xda442d24U);
		mIndex = (mIndex + 15) & 15;
		a = mStates[mIndex];
		mStates[mIndex] = a ^ b ^ d ^ (a << 2) ^ (b << 18) ^ (c << 28);

		return mStates[mIndex];
	}
	//-------------------------------------------------------------------------------------------------------
	// 랜덤 범위 함수(pMin 이상 pMax '미만')
	public int Range(int pMin, int pMax)
	{
		if (pMax <= pMin)
			return pMin;

		return (int)((Range() % (pMax - pMin)) + pMin);
	}
	//-------------------------------------------------------------------------------------------------------
	// 정규 분포 랜덤 범위 함수(pMin 이상 pMax '미만')
	public int NormalDistributionRange(int pMin, int pMax)
	{
		// 차후에 구현 예정이고 임시로 균등 분포로 구현함 [3/3/2016 jhpark]
		if (pMax <= pMin)
			return pMin;

		return (int)((Range() % (pMax - pMin)) + pMin);
	}
	//-------------------------------------------------------------------------------------------------------
	// 해당 확률(%)이 성공했는지 여부를 랜덤 계산합니다.
	public bool CheckProb(int pProb)
	{
		if (pProb > 0)
			return (((Range() % 100) + 1) <= pProb);
		else
			return false;
	}
	//-------------------------------------------------------------------------------------------------------
	// 랜덤 인덱스
	public int RandomIndex(int[] pProbs)
	{
		int lRandomSum = 0;
		for (int iProb = 0; iProb < pProbs.Length; ++iProb)
			lRandomSum += pProbs[iProb];
		int lRandomValue = (int)Range(0, lRandomSum);

		for (int iProb = 0; iProb < pProbs.Length; ++iProb)
		{
			if (lRandomValue < pProbs[iProb])
				return iProb;

			lRandomValue -= pProbs[iProb];
		}

		return -1;
	}
	//-------------------------------------------------------------------------------------------------------
	//
	public void RandomNotOverlap(int pMin, int pMax, out int[] pOutRandomIndex)
	{
		int lSize = Mathf.Abs(pMax - pMin);
		pOutRandomIndex = new int[lSize];

		for (int i = 0; i < lSize; i++)
		{
			pOutRandomIndex[i] = i+pMin;
		}

		for(int i=0; i<lSize; i++)
		{
			int lRandomIndex = (int)Range(pMin, pMax);
			int lTeampValue = pOutRandomIndex[i];
			if (lRandomIndex == i)
				continue;

			pOutRandomIndex[i] = pOutRandomIndex[lRandomIndex];
			pOutRandomIndex[lRandomIndex] = lTeampValue;
		}
	}
	//-------------------------------------------------------------------------------------------------------
	//
	public void DebugState()
	{
		//for (int i = 0; i < mStates.Length; i++)
		//{
		//    Debug.Log("Sync Random State index : " + i + ":" + mStates[i]);
		//}
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
	private UInt32[] mStates;
	private int mIndex;
}
