using UnityEngine;
using System.Collections;
using System;

public class XORConvert
{
	public XORConvert(int pXorKeyValue)
	{
		vXorKeyValue = pXorKeyValue;
	}

	public int XOREncode(int _value)
	{
		IntToByteNon(vXorKeyValue, out extable);

		byte[] _changeBuf = { 0, 0, 0, 0 };
		IntToByte(_value, out _changeBuf);
		return ByteToIntNon(_changeBuf);
	}
	public int XORDecode(int _value)
	{
		IntToByteNon(vXorKeyValue, out extable);
		byte[] _changeBuf = { 0, 0, 0, 0 };
		IntToByteNon(_value, out _changeBuf);
		return ByteToInt(_changeBuf);
	}
	
	private void IntToByte(int num, out byte[] value)
	{
		value = new byte[4] { 0, 0, 0, 0 };
		value[0] = (byte)((num >> 24) ^ extable[0]);
		value[1] = (byte)((num >> 16) ^ extable[1]);
		value[2] = (byte)((num >> 8) ^ extable[2]);
		value[3] = (byte)(num ^ extable[3]);
	}
	private int ByteToIntNon(byte[] value)
	{
		int retV = 0;

		retV = (((value[0] & 0xFF)) << 24) |
		(((value[1] & 0xFF)) << 16) |
		(((value[2] & 0xFF)) << 8) |
		((value[3] & 0xFF));

		return retV;
	}
	private void IntToByteNon(int num, out byte[] value)
	{
		value = new byte[4] { 0, 0, 0, 0 };
		value[0] = ((byte)(num >> 24));
		value[1] = ((byte)(num >> 16));
		value[2] = ((byte)(num >> 8));
		value[3] = ((byte)(num));
	}
	private int ByteToInt(byte[] value)
	{
		int retV = 0;

		retV = (((value[0] & 0xFF) ^ extable[0]) << 24) |
		(((value[1] & 0xFF) ^ extable[1]) << 16) |
		(((value[2] & 0xFF) ^ extable[2]) << 8) |
		((value[3] & 0xFF) ^ extable[3]);

		return retV;
	}
	private byte[] extable = new byte[4]{ 0, 0, 0, 0 };
	private int vXorKeyValue;
}
