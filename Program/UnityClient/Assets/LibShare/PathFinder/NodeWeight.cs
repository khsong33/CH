﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class NodeWeight : AStarNode
{
	public int aRow
	{
		get { return mRow; }
	}
	public int aCol
	{
		get { return mCol; }
	}
	public int aWeight
	{
		get { return mWeight; }
		set { mWeight = value; }
	}

	public int mRow;
	public int mCol;
	public int mWeight;
	public NodeWeight()
	{
		mRow = 0;
		mCol = 0;
		mWeight = int.MaxValue;
	}
	public NodeWeight(int pRow, int pCol, int pWeight)
	{
		mRow = pRow;
		mCol = pCol;
		mWeight = pWeight;
	}
}
