﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
public class ExportStageData
{
	[MenuItem("Custom Tool/Stage Data Exporter")]
	static void GoExportStageData()
	{
		GameObject lSelectObject = null;
		foreach (GameObject obj in UnityEngine.Object.FindObjectsOfType(typeof(GameObject)))
		{
			if (obj.name.ToUpper().Equals("STAGE DATA"))
			{
				lSelectObject = obj;
				break;
			}
		}

		// Bring up save panel
		string path = EditorUtility.SaveFilePanel("Save Stage", "AssetDatas/Resources/MapInfos", "StageName", "txt");
		StreamWriter sw = new StreamWriter(path, false);
		for(int iChild = 0; iChild < lSelectObject.transform.childCount; iChild++)
		{
			Transform lTransform = lSelectObject.transform.GetChild(iChild);
			UnityEngine.Object parentObject = PrefabUtility.GetPrefabParent(lTransform.gameObject);
			string prefabPath = AssetDatabase.GetAssetPath(parentObject);
			string[] saveFilename = prefabPath.Split('\\', '/', '.');
			string foldername = saveFilename[saveFilename.Length - 3];
			string objectname = saveFilename[saveFilename.Length - 2];
			string objectPosition = string.Format("{0},{1},{2}", lTransform.position.x, lTransform.position.y, lTransform.position.z);

			string lData = string.Format("{0},{1},{2},{3}", prefabPath, foldername, objectname, objectPosition);
			sw.WriteLine(lData);
		}
		
		sw.Close();

		Debug.Log("Stage data export Complete!");

	}
}
#endif