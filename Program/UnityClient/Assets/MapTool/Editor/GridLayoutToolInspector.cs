﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


[CustomEditor(typeof(GridLayoutTool))]
[CanEditMultipleObjects]
public class GridLayoutToolInspector : Editor
{
    [MenuItem("Combat Hero/GridLayout Tool")]
    static void AddGridLayoutTool()
    {
        var tool = FindObjectOfType<GridLayoutTool>();
        if (tool != null)
            DestroyImmediate(FindObjectOfType<GridLayoutTool>().gameObject);

        var go = new GameObject();
        go.AddComponent<GridLayoutTool>();
        go.AddComponent<GridStageBattlePlay>();
        go.name = "@GridLayoutTool";
    }

    [MenuItem("Combat Hero/Export GridMesh as Obj")]
    static void ExportGridMesh()
    {
        var name = "default";
        var tool = FindObjectOfType<GridLayoutTool>();
        if (tool != null && tool.vGridLayoutName != "")
            name = tool.vGridLayoutName;

        GameObject.FindObjectOfType<MapExportOptions>().Export(Application.dataPath + @"\..\", name, true);
    }


    [MenuItem("Combat Hero/Lightmap/Build Lightmap Data")]
    static void MakePrefab()
    {
        BakeAndMakePrefabData(false);
    }

    [MenuItem("Combat Hero/Lightmap/Build Lightmap Data(Bake)")]
    static void BakeAndMakePrefab()
    {
        BakeAndMakePrefabData(true);
    }

    private static void BakeAndMakePrefabData(bool bake)
    {
        if (Selection.gameObjects.Count() != 1)
        {
            Debug.LogError("하나의 게임오브젝트만 선택해서 작업해주세요");
            return;
        }

        if (Lightmapping.giWorkflowMode != Lightmapping.GIWorkflowMode.OnDemand)
        {
            Lightmapping.giWorkflowMode = Lightmapping.GIWorkflowMode.OnDemand;
            Debug.LogError("Auto Bake mode is disabled.");
            return;
        }

        if (bake)
            Lightmapping.Bake();

        var name = EditorApplication.currentScene.Substring(EditorApplication.currentScene.LastIndexOf('/') + 1).Replace(".unity", "(prefab)");

        var oldData = Selection.gameObjects[0].GetComponent<PrefabLightmapData>();
        string saveFolder = "";
        if (oldData != null)
            saveFolder = oldData.saveFolder;
        else
        {
            string path = EditorUtility.OpenFolderPanel("저장위치를 고르세요", "", "");
            if (!string.IsNullOrEmpty(path))
            {
                saveFolder = path.Replace(Application.dataPath+"/", "");
                saveFolder += "/";
            }
        }

        var prefabLightmapDatas = Selection.gameObjects[0].GetComponentsInChildren<PrefabLightmapData>();
        foreach (var data in prefabLightmapDatas)
            DestroyImmediate(data);

        var lightMapInfos = Selection.gameObjects[0].GetComponentsInChildren<LightmapInfo>();
        foreach (var data in lightMapInfos)
            DestroyImmediate(data);

        // source에 정보입력
        Selection.gameObjects[0].AddComponent<PrefabLightmapData>();
        Selection.gameObjects[0].GetComponent<PrefabLightmapData>().GenerateLightmapInfo();
        Selection.gameObjects[0].GetComponent<PrefabLightmapData>().saveFolder = saveFolder;

        // clone 생성
        GameObject cloned = Instantiate<GameObject>(Selection.gameObjects[0]);
        cloned.name = name;

        // LightmapStatic 설정 꺼주기
        var meshFilters = cloned.GetComponentsInChildren<MeshFilter>();
        foreach (var meshfilter in meshFilters)
        {
            var staticFlags = GameObjectUtility.GetStaticEditorFlags(meshfilter.gameObject);
            staticFlags &= ~StaticEditorFlags.LightmapStatic;
            GameObjectUtility.SetStaticEditorFlags(meshfilter.gameObject, staticFlags);
        }

		String lPrefabPath = "Assets/" + saveFolder + name + ".prefab";
        PrefabUtility.CreatePrefab(lPrefabPath, cloned, ReplacePrefabOptions.ConnectToPrefab);
		Debug.Log("[OK] map prefab saved in '" + lPrefabPath + "'");
        DestroyImmediate(cloned);
    }

    string selectMode = "add";

    public void SelectTile(Ray ray, bool clearFirst)
    {
        var mapTool = target as GridLayoutTool;

        RaycastHit hitInfo;
        if (Physics.Raycast(ray, out hitInfo))
        {
            var cell = mapTool.GetCell(hitInfo.triangleIndex);
            if (cell != null)
            {
                if (mapTool.OnTileSelectionChanged(cell, selectMode, clearFirst))
                    EditorUtility.SetDirty(target);
            }
        }
    }

    int hash = "GridTileEditor".GetHashCode();
    void OnSceneGUI()
    {
        var mapTool = target as GridLayoutTool;
        if (mapTool.editable == false)
            return;

        Event current = Event.current;
        int ID = GUIUtility.GetControlID(hash, FocusType.Passive);
        EventType type = current.GetTypeForControl(ID);

        if (type == EventType.Layout)
        {
            HandleUtility.AddDefaultControl(ID);
        }

        if (current.button == 0 && current.alt == false && current.command == false)
        {

            Ray ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);

            if (type == EventType.MouseDown && HandleUtility.nearestControl == ID)
            {
                selectMode = current.control ? "remove" : "add";

                SelectTile(ray, current.shift == false && current.control == false);
                GUIUtility.hotControl = ID;
                current.Use();
            }
            if (type == EventType.MouseDrag)
            {
                if (GUIUtility.hotControl == ID)
                    SelectTile(ray, false);

                current.Use();
            }

            if (type == EventType.MouseUp)
            {
                if (GUIUtility.hotControl == ID)
                    GUIUtility.hotControl = 0;
                current.Use();
            }
        }
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GridLayoutTool tool = (GridLayoutTool)target;

        EditorGUILayout.BeginHorizontal();
        {
            if (tool.editable == false)
            {
                GUI.color = Color.green;
                if (GUILayout.Button("Start Edit", GUILayout.Height(40)))
                    tool.editable = true;
                GUI.color = Color.white;
            }
            else
            {
                GUI.color = Color.red;
                if (GUILayout.Button("Stop Edit", GUILayout.Height(40)))
                    tool.editable = false;
                GUI.color = Color.white;
            }

            EditorGUILayout.BeginVertical();
            {
                GUILayout.Label("Tile Opaque");
                float newAlpha = GUILayout.HorizontalSlider(tool.alpha, 0, 1, GUILayout.Width(100));
                if (newAlpha != tool.alpha)
                    tool.SetAlpha(newAlpha);
            }
            EditorGUILayout.EndVertical();
        }
        EditorGUILayout.EndHorizontal();


        if (GUILayout.Button("Create Default Grid"))
            tool.New();

        if (GUILayout.Button("Clear All Grid"))
            tool.Clear();

        if (GUILayout.Button("Refresh All"))
        {
            tool.RefreshAll();
        }

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Import"))
        {
            var defaultPath = Application.dataPath + @"/AssetData/Bundles/Resources/" + tool.vBundleStageFolder;

            string path = EditorUtility.OpenFilePanel("Import Grid", defaultPath, "txt");
            if (!string.IsNullOrEmpty(path))
            {
                char[] splitToken = new char[] { '/', '.' };
                String[] lCSVTextPath = path.Split(splitToken);
                tool.ImportGrid(lCSVTextPath[lCSVTextPath.Length - 3] + @"/", lCSVTextPath[lCSVTextPath.Length - 2]);
            }
        }
        if (GUILayout.Button("Export"))
        {
            var defaultPath = Application.dataPath + @"/AssetData/Bundles/Resources/" + tool.vBundleStageFolder;
            string path = EditorUtility.SaveFilePanel("Export Grid", defaultPath, tool.vGridLayoutName, "txt");
            if (path.Length > 0)
                tool.ExportGrid(path);
            AssetDatabase.Refresh();
        }
        EditorGUILayout.EndHorizontal();

        GUILayout.Space(10);
        GUILayout.Label("A* Test");
        EditorGUI.indentLevel++;
        if (GUILayout.Button("Set Default Weight"))
            tool.SetDefaultWayWeight();
        EditorGUILayout.BeginHorizontal();
        {
            if (GUILayout.Button(new GUIContent("Set A* Pos", "두 점을 선택한뒤 버튼을 누르면, 선택한 순서대로 시작점, 끝점이 됨")))
            {
                if (tool.selectedCells.Count >= 2)
                {
                    tool.vAStarStartPos.x = tool.selectedCells.First().row;
                    tool.vAStarStartPos.y = tool.selectedCells.First().col;
                    tool.vAStarTargetPos.x = tool.selectedCells.Last().row;
                    tool.vAStarTargetPos.y = tool.selectedCells.Last().col;
                }
            }
            if (GUILayout.Button("Test A*"))
                tool.SearchTest();
            if (GUILayout.Button("Remove A*"))
                tool.RemoveDebugLine();
        }
        EditorGUILayout.EndHorizontal();
        EditorGUI.indentLevel--;

        GUILayout.Space(10);


        if (tool.selectedCells.Count > 0)
        {
            GUILayout.Label("Selected Cell(s)");
            EditorGUI.indentLevel++;
            MultipleObjectsProxy selectedCellsProxy = new MultipleObjectsProxy();
            MultipleObjectsProxy.GetMultipleObjectsProxy<GridCell>(tool.selectedCells, selectedCellsProxy);

            Dictionary<string, object> valuesToChange = new Dictionary<string, object>();

            var tileType = selectedCellsProxy.nameToValueMap["vTileType"].value;
            if (tileType == null)
                tileType = MapGridTileType.MultipleValues.ToString();

            GeneralAttr(tileType.ToString(), selectedCellsProxy, valuesToChange);
            GUILayout.Label("Params");
            TileSpecificAttr(tileType.ToString(), selectedCellsProxy, valuesToChange);

            //TestAttr(selectedCellsProxy, valuesToChange);
            GUILayout.Label("Coords");
            string coords = "";
            foreach (var cell in tool.selectedCells)
                coords += "(" + cell.position.x + "," + cell.position.y + "," + cell.position.z + "), ";
            

            EditorGUI.indentLevel++;
            GUILayout.TextArea(coords.TrimEnd(new char[] { ',', ' ' }));
            EditorGUI.indentLevel--;


            foreach (var key in valuesToChange.Keys)
            {
                MultipleObjectsProxy.ApplyToSelection(tool.selectedCells, key, valuesToChange[key]);
                tool.OnCellProppertyChanged();
            }
            EditorGUI.indentLevel--;
        }
    }

    static Dictionary<string, Dictionary<string, string>> typeKeyLabelMap = new Dictionary<string, Dictionary<string, string>>();
    static Dictionary<string, string> toolTips = new Dictionary<string, string>();
    private static GUIContent GetTooltip(string key)
    {
        if (toolTips.Count == 0)
        {
            toolTips["vWayIndex"] = "해당 본부의 고유 번호(임의의 수)";
            toolTips["vTileType"] = "이 본부와 연결된 거점(처음부터 소유하게 될 거점)의 고유 번호";
            toolTips["vGroundType"] = "부대 스폰 좌표 X(cm)";
            toolTips["vWeight"] = "부대 스폰 좌표 Z(cm)";
            toolTips["vRotation"] = "부대 스폰 좌표 Z(cm)";

            toolTips["Headquarter GUID"] = "해당 본부의 고유 번호(임의의 수)";
            toolTips["LinkCheckPoint GUID"] = "이 본부와 연결된 거점(처음부터 소유하게 될 거점)의 고유 번호";
            toolTips["Spawn X"] = "부대 스폰 좌표 X(cm)";
            toolTips["Spawn Z"] = "부대 스폰 좌표 Z(cm)";

            toolTips["CheckPoint GUID"] = "해당 거점의 고유 번호(임의의 수)";
            toolTips["CheckPoint Type"] = @"거점 종류 지정 
    0 : 일반 거점
    1 : 관측소
    2 : 야전구호소
    3 : 야전 수리소
    4 : 통신탑";
            toolTips["User Idx"] = @"전투 시작시 점령 상태를 지정 
    -1 : 미점령 + 방어부대 없음
    0 : 미점령 + 방어부대 있음 
    1~8 : 유저1~8)";
            toolTips["StarId"] = @"승리 거점일 경우 몇 번째 별에 해당하는가? 
    1,2,3 : 세 개의 값 중 하나이면 승리 거점
    0 : 승리 거점이 아님)
    -1: 본부와 연결된 거점은";
            toolTips["CPExtra"] = @"체크포인트에 소환되는 지휘관 부대 Name 값을 입력. 소환하는 부대가 없을 경우 None 으로 입력
    -입력 형식 : 중립군이름^미군이름^독일군이름^소련군이름";

            toolTips["EventPoint GUID"] = @"고유 번호 -> 그룹 번호
    -전투 시작시에 같은 그룹 번호를 가진 이벤트 중에 하나를 랜덤으로 선정하여
     그 포인트에서만 지속적으로 이벤트가 발생하도록 함";
            toolTips["Start Delay"] = @"생성 시간 (전투 시작 시간 기준)";
            toolTips["Repeat Count"] = @"생성 횟수";
            toolTips["Repeat Delay"] = @"재생성 대기 시간(ms)";
            toolTips["EPExtra"] = @"Event 종류
    -종류가 1가지일 경우 지정, 여러가지일 경우 랜덤
    -[테이블] GroundEventDataTable(지형 이벤트 테이블) Name 컬럼 참조";

        }

        if (toolTips.ContainsKey(key))
            return new GUIContent(key, toolTips[key]);
        else
            return new GUIContent(key, "");
    }

    static string GetKeyLabel(string tileType, string key)
    {
        if (typeKeyLabelMap.Count == 0)
        {
            typeKeyLabelMap.Add(MapGridTileType.MultipleValues.ToString(), new Dictionary<string, string>());
            typeKeyLabelMap.Add(MapGridTileType.Way.ToString(), new Dictionary<string, string>());
            {
                typeKeyLabelMap[MapGridTileType.Way.ToString()].Add("vParam1", "");
                typeKeyLabelMap[MapGridTileType.Way.ToString()].Add("vParam2", "");
                typeKeyLabelMap[MapGridTileType.Way.ToString()].Add("vParam3", "");
                typeKeyLabelMap[MapGridTileType.Way.ToString()].Add("vParam4", "");
                typeKeyLabelMap[MapGridTileType.Way.ToString()].Add("vExtraString", "");
            }
            typeKeyLabelMap.Add(MapGridTileType.Headquarter.ToString(), new Dictionary<string, string>());
            {
                typeKeyLabelMap[MapGridTileType.Headquarter.ToString()].Add("vParam1", "Headquarter GUID");
                typeKeyLabelMap[MapGridTileType.Headquarter.ToString()].Add("vParam2", "LinkCheckPoint GUID");
                typeKeyLabelMap[MapGridTileType.Headquarter.ToString()].Add("vParam3", "Spawn X");
                typeKeyLabelMap[MapGridTileType.Headquarter.ToString()].Add("vParam4", "Spawn Z");
                typeKeyLabelMap[MapGridTileType.Headquarter.ToString()].Add("vExtraString", "");
            }
            typeKeyLabelMap.Add(MapGridTileType.CheckPoint.ToString(), new Dictionary<string, string>());
            {
                typeKeyLabelMap[MapGridTileType.CheckPoint.ToString()].Add("vParam1", "CheckPoint GUID");
                typeKeyLabelMap[MapGridTileType.CheckPoint.ToString()].Add("vParam2", "CheckPoint Type");
                typeKeyLabelMap[MapGridTileType.CheckPoint.ToString()].Add("vParam3", "User Idx");
                typeKeyLabelMap[MapGridTileType.CheckPoint.ToString()].Add("vParam4", "StarId");
                typeKeyLabelMap[MapGridTileType.CheckPoint.ToString()].Add("vExtraString", "CPExtra");
            }
            typeKeyLabelMap.Add(MapGridTileType.EventPoint.ToString(), new Dictionary<string, string>());
            {
                typeKeyLabelMap[MapGridTileType.EventPoint.ToString()].Add("vParam1", "EventPoint GUID");
                typeKeyLabelMap[MapGridTileType.EventPoint.ToString()].Add("vParam2", "Start Delay");
                typeKeyLabelMap[MapGridTileType.EventPoint.ToString()].Add("vParam3", "Repeat Count");
                typeKeyLabelMap[MapGridTileType.EventPoint.ToString()].Add("vParam4", "Repeat Delay");
                typeKeyLabelMap[MapGridTileType.EventPoint.ToString()].Add("vExtraString", "EPExtra");
            }
            typeKeyLabelMap.Add(MapGridTileType.Block.ToString(), new Dictionary<string, string>());
            {
                typeKeyLabelMap[MapGridTileType.Block.ToString()].Add("vParam1", "");
                typeKeyLabelMap[MapGridTileType.Block.ToString()].Add("vParam2", "");
                typeKeyLabelMap[MapGridTileType.Block.ToString()].Add("vParam3", "");
                typeKeyLabelMap[MapGridTileType.Block.ToString()].Add("vParam4", "");
                typeKeyLabelMap[MapGridTileType.Block.ToString()].Add("vExtraString", "");
            }

        }

        if (typeKeyLabelMap[tileType].ContainsKey(key))
            return typeKeyLabelMap[tileType][key];

        return null;
    }

    private static void TileSpecificAttr(string tileType, MultipleObjectsProxy selectedCellsProxy, Dictionary<string, object> valuesToChange)
    {
        foreach (var key in selectedCellsProxy.nameToValueMap.Keys)
        {
            string label = GetKeyLabel(tileType, key);
            if (label == null || label.Length == 0) 
                continue;

            MultipleObjectsProxy.ValueInfo valueInfo = selectedCellsProxy.nameToValueMap[key];
            object returnValue = null;

            if (valueInfo.value == null)
            {
                if (valueInfo.type == typeof(int))
                {
                    returnValue = EditorGUILayout.TextField(GetTooltip(label), "-");
                    if (returnValue.ToString() != "-")
                    {
                        int resultInt;
                        int.TryParse(returnValue.ToString(), out resultInt);
                        valuesToChange.Add(key, resultInt);
                    }
                }
                else if (valueInfo.type == typeof(string))
                {
                    EditorGUILayout.LabelField(GetTooltip(label));
                    returnValue = EditorGUILayout.TextArea("-", GUILayout.ExpandHeight(true));
                    if (returnValue.ToString() != "-")
                        valuesToChange.Add(key, returnValue);//.ToString());//.Replace("\n","^"));
                }
                else if (valueInfo.type == typeof(MapGridTileType))
                {
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(label), MapGridTileType.MultipleValues);
                    if ((MapGridTileType)returnValue != MapGridTileType.MultipleValues)
                        valuesToChange.Add(key, returnValue);
                }
                else if (valueInfo.type == typeof(MapGridGroundType))
                {
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(label), MapGridGroundType.MultipleValues);
                    if ((MapGridGroundType)returnValue != MapGridGroundType.MultipleValues)
                        valuesToChange.Add(key, returnValue);
                }
            }
            else
            {
                if (valueInfo.type == typeof(int))
                    returnValue = EditorGUILayout.IntField(GetTooltip(label), (int)valueInfo.value);
                else if (valueInfo.type == typeof(string))
                {
                    EditorGUILayout.LabelField(GetTooltip(label));
                    returnValue = EditorGUILayout.TextArea(((string)valueInfo.value), GUILayout.ExpandHeight(true));
                    //var ret = EditorGUILayout.TextArea(((string)valueInfo.value).Replace("^", "\n"), GUILayout.ExpandHeight(true));
                    //returnValue = ret.Trim(new char[] { '\n', '\t', ' ' }).Replace("\n", "^");
                }
                else if (valueInfo.type == typeof(MapGridTileType))
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(label), (MapGridTileType)valueInfo.value);
                else if (valueInfo.type == typeof(MapGridGroundType))
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(label), (MapGridGroundType)valueInfo.value);

                if (returnValue.ToString() != valueInfo.value.ToString())
                    valuesToChange.Add(key, returnValue);
            }
        }
    }

    private static void GeneralAttr(string tileType, MultipleObjectsProxy selectedCellsProxy, Dictionary<string, object> valuesToChange)
    {
        foreach (var key in selectedCellsProxy.nameToValueMap.Keys)
        {
            string label = GetKeyLabel(tileType, key);
            if (label != null)
                continue;

            MultipleObjectsProxy.ValueInfo valueInfo = selectedCellsProxy.nameToValueMap[key];
            object returnValue = null;

            if (valueInfo.value == null)
            {
                if (valueInfo.type == typeof(int))
                {
                    returnValue = EditorGUILayout.TextField(GetTooltip(key), "-");
                    if (returnValue.ToString() != "-")
                    {
                        int resultInt;
                        int.TryParse(returnValue.ToString(), out resultInt);
                        valuesToChange.Add(key, resultInt);
                    }
                }
                else if (valueInfo.type == typeof(string))
                {
                    returnValue = EditorGUILayout.TextField(GetTooltip(key), "-");
                    if (returnValue.ToString() != "-")
                        valuesToChange.Add(key, returnValue.ToString());
                }
                else if (valueInfo.type == typeof(MapGridTileType))
                {
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(key), MapGridTileType.MultipleValues);
                    if ((MapGridTileType)returnValue != MapGridTileType.MultipleValues)
                    {
                        valuesToChange.Add(key, returnValue);
                        if ((MapGridTileType)returnValue == MapGridTileType.Block)
                            valuesToChange.Add("vWeight", int.MaxValue);
                    }
                }
                else if (valueInfo.type == typeof(MapGridGroundType))
                {
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(key), MapGridGroundType.MultipleValues);
                    if ((MapGridGroundType)returnValue != MapGridGroundType.MultipleValues)
                        valuesToChange.Add(key, returnValue);
                }
            }
            else
            {
                if (valueInfo.type == typeof(int))
                    returnValue = EditorGUILayout.IntField(GetTooltip(key), (int)valueInfo.value);
                else if (valueInfo.type == typeof(string))
                    returnValue = EditorGUILayout.TextField(GetTooltip(key), (string)valueInfo.value);
                else if (valueInfo.type == typeof(MapGridTileType))
                {
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(key), (MapGridTileType)valueInfo.value);
                    if ((MapGridTileType)returnValue == MapGridTileType.Block)
                        valuesToChange.Add("vWeight", int.MaxValue);
                }
                else if (valueInfo.type == typeof(MapGridGroundType))
                    returnValue = EditorGUILayout.EnumPopup(GetTooltip(key), (MapGridGroundType)valueInfo.value);

                if (returnValue.ToString() != valueInfo.value.ToString())
                    valuesToChange.Add(key, returnValue);
            }
        }
    }
}
