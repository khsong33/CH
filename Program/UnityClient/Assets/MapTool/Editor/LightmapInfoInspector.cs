﻿using UnityEditor;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;


[CustomEditor(typeof(LightmapInfo))]
public class LightmapInfoInspector : Editor
{
    public override void OnInspectorGUI()
    {
        //DrawDefaultInspector();
        var pld = (LightmapInfo)target;
        var renderer = pld.GetComponent<MeshRenderer>();

        GUI.color = Color.yellow;
        GUILayout.Label("Real Renderer Values");
        GUILayout.Label("  LightmapIndex " + renderer.lightmapIndex);
        GUILayout.Label("  LightmapScaleOffset " + renderer.lightmapScaleOffset.ToString());

        GUI.color = Color.green;
        GUILayout.Label("Values to overwrite");
        pld.lightmapOffsetIndexForDebugging = EditorGUILayout.IntField("  IndexOffset", pld.lightmapOffsetIndexForDebugging);
        pld.localLightmapIndex = EditorGUILayout.IntField("  LocalLightmapIndex", pld.localLightmapIndex);
        GUILayout.Label("  LightmapScaleOffset " + pld.lightmapScaleOffset.ToString());

        GUI.color = Color.white;
        if (GUILayout.Button("Apply"))
        {
            renderer.lightmapIndex = pld.localLightmapIndex + pld.lightmapOffsetIndexForDebugging;
            renderer.lightmapScaleOffset = pld.lightmapScaleOffset;
        }
    }
}
