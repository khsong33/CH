﻿using UnityEngine;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Reflection;

public class MultipleObjectsProxy
{
    [AttributeUsage(AttributeTargets.Property | AttributeTargets.Field)]
    public class AddToInspectorAttribute : Attribute
    {
        public AddToInspectorAttribute()
        {

        }
    }

    public static void GetMultipleObjectsProxy<T>(IEnumerable<T> objects, MultipleObjectsProxy proxy) where T : new()
    {
        proxy.nameToValueMap.Clear();

        T type = objects.First();
        foreach (FieldInfo fi in type.GetType().GetFields())
        {
            bool show = false;
            object[] attrs = fi.GetCustomAttributes(false);
            foreach (object attr in attrs)
            {
                if (attr.GetType() == typeof(AddToInspectorAttribute))
                {
                    show = true;
                    break;
                }
            }
            if (show == false)
                continue;

            object value = null;
            foreach (T item in objects)
            {
                if (fi.GetValue(item) == null)
                    continue;

                if (value == null)
                    value = fi.GetValue(item);
                else if (value.ToString() != fi.GetValue(item).ToString())
                {
                    value = null;
                    break;
                }
            }

            proxy.nameToValueMap.Add(fi.Name, new ValueInfo() { type = fi.FieldType, value = value });
        }
    }

    public static void ApplyToSelection<T>(IEnumerable<T> objects, string name, object value) where T : new()
    {
        foreach (T obj in objects)
        {
            obj.GetType().GetField(name).SetValue(obj, value);
        }
    }

    public class ValueInfo
    {
        public Type type = null;
        public object value = null;
    }

    public Dictionary<string, ValueInfo> nameToValueMap = new Dictionary<string, ValueInfo>();
}

[System.Serializable]
public class GridCell
{
    public Vector3 position = new Vector3();
    public int row;
    public int col;

    private GridLayoutTool gridStage;
    public bool selected = false;

    public enum MaterialType
    {
        Link = 0,
        Way = 1,
        Type,
        GroundType,
        MaterialTypeMax
    }

    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
    [MultipleObjectsProxy.AddToInspector]
    public int vWayIndex = 0;
    [MultipleObjectsProxy.AddToInspector]
    public MapGridTileType vTileType = MapGridTileType.Way;
    [MultipleObjectsProxy.AddToInspector]
    public int vParam1 = 0;
    [MultipleObjectsProxy.AddToInspector]
    public int vParam2 = 0;
    [MultipleObjectsProxy.AddToInspector]
    public int vParam3 = 0;
    [MultipleObjectsProxy.AddToInspector]
    public int vParam4 = 0;
    [MultipleObjectsProxy.AddToInspector]
    public String vExtraString;
    [MultipleObjectsProxy.AddToInspector]
    public MapGridGroundType vGroundType = MapGridGroundType.Grass;
    [MultipleObjectsProxy.AddToInspector]
    public int vWeight = 0;
    [MultipleObjectsProxy.AddToInspector]
    public int vRotation = 0;


    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
    }

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
	// 격자를 지정합니다.
	public void SetGrid(GridLayoutTool pCombatGrid, int pCellRowIndex, int pCellColIndex)
	{
        gridStage = pCombatGrid;
		Coord2 mCenterCoord = pCombatGrid.GetCellCenterCoord(pCellRowIndex, pCellColIndex);
        position = Coord2.ToWorld(mCenterCoord);


        vExtraString = String.Empty; // 생성 초기화

		row = pCellRowIndex;
		col = pCellColIndex;
	}
    //-------------------------------------------------------------------------------------------------------
    // Cell 정보를 갱신합니다.
    public bool RefreshMap()
    {
		gridStage.aBlockNodeWeights[row, col].aWeight = vWeight;
        return true;
    }

	//-------------------------------------------------------------------------------------------------------
	// Cell Weight 초기화
	public void SetDefaultWayWeight()
	{
		// A* 처리를 위한 가중치 추가
		vWeight = MapGridTile.cDefaultWeights[(int)vGroundType];
	}
}
