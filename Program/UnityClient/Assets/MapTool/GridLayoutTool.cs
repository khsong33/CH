﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

[ExecuteInEditMode]
public class GridLayoutTool : MonoBehaviour
{
    public struct LayoutHeader
    {
        public enum Attribute
        {
            AStarHeuristic,
        }
    }

    public const String cGridLayoutTiles = "@GridMesh";

    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
    public String vBundleStageFolder = "GridLayouts0/";
    public String vGridLayoutName = "";

    public int vBlockColCount = 110;
    public int vBlockRowCount = 101;
    int vBlockScaleX = 250;
    int vBlockScaleZ = 290;
    public const String cTempFile = "TST";

    GameObject vDebugLineRoot;
    GameObject vDebugLinePrefab;
    GameObject mGridMesh;
    MeshFilter mMeshFilter;
    MeshCollider mMeshCollider;

    public Vector2 vAStarStartPos;
    public Vector2 vAStarTargetPos;
    public int vAStarHeuristic = 50;

    public float alpha { get; set; }
    public bool editable { get; set; }
    public HashSet<GridCell> selectedCells = new HashSet<GridCell>();
    public bool hexTile = false;
    //-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------
    // 경로 비중 테이블
    public NodeWeight[,] aBlockNodeWeights
    {
        get { return mBlockNodeWeights; }
    }

    MeshFilter meshFilter
    {
        get
        {
            if (mMeshFilter == null)
                mMeshFilter = mGridMesh.GetComponent<MeshFilter>();

            return mMeshFilter;
        }
    }

    MeshCollider meshCollider
    {
        get
        {
            if (mMeshCollider == null)
                mMeshCollider = mGridMesh.GetComponent<MeshCollider>();

            return mMeshCollider;
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
        if (vGridLayoutName == "")
            New();
        else if (ImportGrid(vBundleStageFolder, vGridLayoutName) == false)
            New();

        MapGridTile.VerifyTable(); // 스크립트 초기화 때 한 번만 합니다.
        alpha = 1;
    }

    public void New()
    {
        ClearGrid();
        CreateGridCells();
        CreateGridMesh();
    }

    public void Clear()
    {
        ClearGrid();
    }

    public void SetAlpha(float alpha)
    {
        this.alpha = alpha;

        if (mGridMesh != null)
        {
            var meshRenderer = mGridMesh.GetComponent<MeshRenderer>();
            if (meshRenderer != null)
                meshRenderer.sharedMaterial.SetFloat("_Opaque", alpha);
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // 기본 cell grid를 생성합니다.
    public void CreateGridCells()
    {
        mCellScaleX = vBlockScaleX * 2;
        mCellScaleZ = vBlockScaleZ * 2;

        mCellRowCount = (vBlockRowCount + 1) / 2;
        mCellColCount = vBlockColCount / 2;

        transform.position = Coord2.ToWorld(GetCellCenterCoord(mCellRowCount / 2, mCellColCount / 2)); ;

        mCells = new GridCell[mCellRowCount, mCellColCount];
        mBlockCells = new GridCell[vBlockRowCount, vBlockColCount];
        mBlockNodeWeights = new NodeWeight[mCellRowCount, mCellColCount];

        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                _CreateCell(iRow, iCol);
                mBlockNodeWeights[iRow, iCol] = new NodeWeight(iRow, iCol, mBlockCells[iRow, iCol].vWeight);
            }
        }


        mPathFinder = new AStarPathFinder<NodeWeight>();
        mPathFinder.InitNode(mBlockNodeWeights, mCellRowCount, mCellColCount, mCellScaleX, mCellScaleZ, vBlockScaleX, vBlockScaleZ, vAStarHeuristic);

        selectedCells.Clear();

        CreateCellMeshTemplate();
        RemoveDebugLine();
    }

    public void CreateGridMesh()
    {
        mGridMesh = GameObject.Find(cGridLayoutTiles);
        if (mGridMesh != null)
        {
            var collider = mGridMesh.GetComponent<MeshCollider>();
            if (collider != null && collider.sharedMesh != null)
                DestroyImmediate(collider.sharedMesh);

            var meshFilter = mGridMesh.GetComponent<MeshFilter>();
            if (meshFilter != null && meshFilter.sharedMesh != null)
                DestroyImmediate(meshFilter.sharedMesh);

            DestroyImmediate(mGridMesh);
        }

        mGridMesh = new GameObject(cGridLayoutTiles);
        mGridMesh.transform.position = Vector3.zero;

        mGridMesh.AddComponent<MeshFilter>();
        mMeshFilter = mGridMesh.GetComponent<MeshFilter>();

        mGridMesh.AddComponent<MeshCollider>();
        mMeshCollider = mGridMesh.GetComponent<MeshCollider>();

        mGridMesh.AddComponent<MapExportOptions>();
        mGridMesh.GetComponent<MapExportOptions>().gameObjectsToExport.Add(mGridMesh);

        mGridMesh.AddComponent<MeshRenderer>();
        mGridMesh.GetComponent<MeshRenderer>().material = Resources.Load("GridGroundType") as Material;

        // render mesh
        Mesh mesh = new Mesh();
        mesh.vertices = BuildGridMeshVertices(_hexVerticesSmall);
        mesh.uv = BuildGridMeshUV(mCells, false);   // base hexagon without angle
        mesh.uv2 = BuildGridMeshUVState();          // selection state
        mesh.uv3 = BuildGridMeshCustomData(mCells); // ground, tile types
        mesh.uv4 = BuildGridMeshUV(mCells, true);   // angle
        mesh.colors = new Color[mCellRowCount * mCellColCount * 6]; // path find result 
        mesh.triangles = BuildGridMeshTriangles();
        mMeshFilter.sharedMesh = mesh;

        // collision mesh
        Mesh colliderMesh = new Mesh();
        colliderMesh.vertices = BuildGridMeshVertices(_hexVerticesLarge);
        colliderMesh.triangles = BuildGridMeshTriangles();
        mMeshCollider.sharedMesh = colliderMesh;
    }

    private Vector2[] BuildGridMeshUVState()
    {
        var uvs = new Vector2[mCellRowCount * mCellColCount * 6];
        for (int i = 0; i < mCellRowCount * mCellColCount * 6; i++)
        {
            uvs[i].x = 0;
            uvs[i].y = 0;
        }

        return uvs;
    }

    private int[] BuildGridMeshTriangles()
    {
        var triangles = new int[mCellRowCount * mCellColCount * 4 * 3];
        for (int i = 0; i < mCellRowCount * mCellColCount; i++)
        {
            triangles[i * 4 * 3 + 0] = i * 6 + 3;
            triangles[i * 4 * 3 + 1] = i * 6 + 2;
            triangles[i * 4 * 3 + 2] = i * 6 + 1;
            triangles[i * 4 * 3 + 3] = i * 6 + 3;
            triangles[i * 4 * 3 + 4] = i * 6 + 1;
            triangles[i * 4 * 3 + 5] = i * 6 + 0;
            triangles[i * 4 * 3 + 6] = i * 6 + 3;
            triangles[i * 4 * 3 + 7] = i * 6 + 0;
            triangles[i * 4 * 3 + 8] = i * 6 + 5;
            triangles[i * 4 * 3 + 9] = i * 6 + 3;
            triangles[i * 4 * 3 + 10] = i * 6 + 5;
            triangles[i * 4 * 3 + 11] = i * 6 + 4;
        }

        return triangles;
    }

    public void UnselectAll()
    {
        selectedCells.Clear();

        var uvs = meshFilter.sharedMesh.uv2;
        int hexCount = 0;
        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                for (int i = 0; i < 6; i++)
                {
                    uvs[hexCount * 6 + i].x = 0;
                    uvs[hexCount * 6 + i].y = 0;
                }
                hexCount++;
                mCells[iRow, iCol].selected = false;
            }
        }
        meshFilter.sharedMesh.uv2 = uvs;
    }

    public void OnCellProppertyChanged()
    {
        var uvs = meshFilter.sharedMesh.uv3;
        foreach (var cell in selectedCells)
        {
            int hexCount = cell.row * mCellColCount + cell.col;

            for (int i = 0; i < 6; i++)
            {
                uvs[hexCount * 6 + i].x = (float)(int)cell.vGroundType;
                uvs[hexCount * 6 + i].y = (float)(int)cell.vTileType;
            }
        }
        meshFilter.sharedMesh.uv3 = uvs;

        uvs = meshFilter.sharedMesh.uv4;
        foreach (var cell in selectedCells)
        {
            int hexCount = cell.row * mCellColCount + cell.col;

            for (int i = 0; i < 6; i++)
            {
                if (cell.vRotation == 0)
                    uvs[hexCount * 6 + i] = Vector2.zero;
                else
                    uvs[hexCount * 6 + i] = GetRotatedUV(cell.vRotation, _hexUV[i]);
            }
        }
        meshFilter.sharedMesh.uv4 = uvs;
    }

    private static Vector2 GetRotatedUV(float angle, Vector2 uv)
    {
        angle *= Mathf.Deg2Rad;
        uv -= new Vector2(0.5f, 0.5f);

        var ouv = new Vector2();
        ouv.x = Mathf.Cos(angle) * (uv.x) - Mathf.Sin(angle) * (uv.y);
        ouv.y = Mathf.Sin(angle) * (uv.x) + Mathf.Cos(angle) * (uv.y);
        ouv += new Vector2(0.5f, 0.5f);
        return ouv;
    }

    public bool OnTileSelectionChanged(GridCell cell, string selectMode, bool clearFirst)
    {
        if (clearFirst)
            UnselectAll();

        if (selectMode == "add")
        {
            if (selectedCells.Contains(cell) == false)
                selectedCells.Add(cell);
            else
                return false;
        }
        else if (selectMode == "remove")
        {
            if (selectedCells.Contains(cell))
                selectedCells.Remove(cell);
            else
                return false;
        }

        cell.selected = selectMode == "add";

        var uvs = meshFilter.sharedMesh.uv2;
        {
            int hexCount = cell.row * mCellColCount + cell.col;
            for (int i = 0; i < 6; i++)
                uvs[hexCount * 6 + i].x = cell.selected ? 1 : 0;
        }
        meshFilter.sharedMesh.uv2 = uvs;
        return true;
    }

    public bool SetPathFindResult(GridCell cell, Color color)
    {
        var colors = meshFilter.sharedMesh.colors;
        {
            int hexCount = cell.row * mCellColCount + cell.col;
            for (int i = 0; i < 6; i++)
                colors[hexCount * 6 + i] = color;
        }
        meshFilter.sharedMesh.colors = colors;
        return true;
    }

    private Vector2[] BuildGridMeshUV(GridCell[,] cells, bool applyAngle)
    {
        Vector2[] uvs = new Vector2[mCellRowCount * mCellColCount * 6];

        int hexCount = 0;
        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                GridCell cell = cells[iRow, iCol];
                for (int i = 0; i < 6; i++)
                {
                    if (applyAngle)
                    {
                        if (cell.vRotation == 0)
                            uvs[hexCount * 6 + i] = Vector2.zero;
                        else
                            uvs[hexCount * 6 + i] = GetRotatedUV(cell.vRotation, _hexUV[i]);
                    }
                    else
                        uvs[hexCount * 6 + i] = _hexUV[i];
                }
                hexCount++;
            }
        }
        return uvs;
    }

    private Vector2[] BuildGridMeshCustomData(GridCell[,] cells)
    {
        Vector2[] uvs = new Vector2[mCellRowCount * mCellColCount * 6];

        int hexCount = 0;
        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                GridCell cell = cells[iRow, iCol];
                for (int i = 0; i < 6; i++)
                {
                    uvs[hexCount * 6 + i].x = (int)cell .vGroundType;
                    uvs[hexCount * 6 + i].y = (int)cell.vTileType;
                }
                hexCount++;
            }
        }
        return uvs;
    }

    private Vector3[] BuildGridMeshVertices(Vector3[] shape)
    {
        Vector3[] vertices = new Vector3[mCellRowCount * mCellColCount * 6];

        int hexCount = 0;
        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                for (int i = 0; i < 6; i++)
                {
                    var coord = GetCellCenterCoord(iRow, iCol);
                    Vector3 pos = Coord2.ToWorld(coord);
                    vertices[hexCount * 6 + i] = shape[i] + pos;
                }
                hexCount++;
            }
        }

        return vertices;
    }

    //-------------------------------------------------------------------------------------------------------
    // 모든 맵을 재설정합니다.
    public bool RefreshAll()
    {
        if (mCells == null)
            return false;
        for (int iCellRow = 0; iCellRow < mCellRowCount; iCellRow++)
        {
            for (int iCellCol = 0; iCellCol < mCellColCount; iCellCol++)
            {
                GridCell lCell = mCells[iCellRow, iCellCol];
                if (lCell == null)
                    continue;

                if (!lCell.RefreshMap())
                    return false;
            }
        }
        return true;
    }
    //-------------------------------------------------------------------------------------------------------
    // 기존 CSV 파일을 import합니다.
    public bool ImportGrid(string path, string name)
    {
        ClearGrid();

        vBundleStageFolder = path;
        vGridLayoutName = name;

        // 레이아웃의 첫 줄을 읽습니다.
        CSVObject lLayoutCsvObject = new CSVObject();
        TextAsset lTextAsset = ResourceUtil.Load<TextAsset>(path+name) as TextAsset;
        if (lTextAsset != null)
        {
            lLayoutCsvObject.LoadCSV(lTextAsset.text, 0); // 헤더를 가지고 있다고 가정합니다.
            return _LoadLayout(lLayoutCsvObject);// 레이아웃을 로딩합니다.
        }
        return false;
    }
    //-------------------------------------------------------------------------------------------------------
    // 기존 CSV 파일을 export합니다.
    public void ExportGrid(String pCSVFullPath)
    {
        if (!RefreshAll())
        {
            Debug.LogError("Export Failed - RefreshAll failed");
            return;
        }

        char[] splitToken = new char[] { '/', '.' };
        String[] lCSVTextPath = pCSVFullPath.Split(splitToken);
        vGridLayoutName = lCSVTextPath[lCSVTextPath.Length - 2];

        StreamWriter sw = new StreamWriter(pCSVFullPath, false);

        // 가장 윗 라인은 col의 갯수만큼 숫자를 작성 (0~Count-1)
        String lLineData = String.Empty;
        lLineData += String.Format("\"{0}\"", _GetHeaderDesc()); // 첫 컬럼은 헤더 정보를 넣습니다.
        lLineData += ",";
        for (int iBlockCount = 1; iBlockCount < vBlockColCount; iBlockCount++) // 첫 컬럼을 제외한 나머지 컬럼이라서 1부터 시작
        {
            lLineData += String.Format("\"{0}\"", iBlockCount);
            if (iBlockCount + 1 < vBlockColCount)
                lLineData += ",";
        }
        sw.WriteLine(lLineData);

        lLineData = String.Empty;
        for (int iRow = vBlockRowCount - 1; iRow >= 0; iRow--)
        {
            for (int iCol = 0; iCol < vBlockColCount; iCol++)
            {
                if (iCol % 2 == 0 && iRow % 2 == 0)
                {
                    lLineData += _GetCellInfo(iRow, iCol);
                }
                else
                {
                    lLineData += "\"\"";
                }
                if (iCol + 1 < vBlockColCount)
                    lLineData += ",";
            }
            sw.WriteLine(lLineData);
            lLineData = String.Empty;
        }

        sw.Close();
        Debug.Log("Export Done");
    }
    //-------------------------------------------------------------------------------------------------------
    // 기본 cell grid를 생성합니다.
    public void ClearGrid()
    {
        selectedCells.Clear();

        mGridMesh = GameObject.Find(cGridLayoutTiles);
        if (mGridMesh != null)
        {
            if (meshCollider != null && meshCollider.sharedMesh != null)
                DestroyImmediate(meshCollider.sharedMesh);
            mMeshCollider = null;

            if (meshFilter != null && meshFilter.sharedMesh != null)
                DestroyImmediate(meshFilter.sharedMesh);
            mMeshFilter = null;

            DestroyImmediate(mGridMesh);
            mGridMesh = null;
        }

        if (mCells != null)
        {
            mCells = null;
            mBlockCells = null;
        }

        RemoveDebugLine();
    }
    //-------------------------------------------------------------------------------------------------------
    // 모든 way에 대해 Default Weight 값을 적용합니다.
    public void SetDefaultWayWeight()
    {
        if (mCells != null)
        {
            for (int iRow = 0; iRow < mCellRowCount; iRow++)
            {
                for (int iCol = 0; iCol < mCellColCount; iCol++)
                {
                    if (mCells[iRow, iCol] != null)
                    {
                        mCells[iRow, iCol].SetDefaultWayWeight();
                    }
                }
            }
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // [test 코드]길을 탐색합니다.
    public void SearchTest()
    {
        RemoveDebugLine();
        NodeWeight[] lOutResultNodes = new NodeWeight[1000];
        int lNodeCount;

        mPathFinder.SetHeuristicValue(vAStarHeuristic);
        mPathFinder.Search(mBlockNodeWeights[(int)vAStarStartPos.x, (int)vAStarStartPos.y],
                           mBlockNodeWeights[(int)vAStarTargetPos.x, (int)vAStarTargetPos.y],
                           null, // 기본 델리게이트를 사용
                           lOutResultNodes,
                           out lNodeCount);

        DrawDebugPath(lOutResultNodes, lNodeCount);
    }
    //-------------------------------------------------------------------------------------------------------
    // [test 코드]길을 그려줍니다.
    private void DrawDebugPath(NodeWeight[] lPathNode, int lPathCount)
    {
        if (vDebugLineRoot == null)
            vDebugLineRoot = Instantiate(Resources.Load<GameObject>("Debug Line Render"));
        if (vDebugLinePrefab == null)
            vDebugLinePrefab = Instantiate(Resources.Load<GameObject>("Debug Line"));

        // 디버깅용 들렸던 길
        for (int iDebugPath = 0; iDebugPath < mPathFinder.mDebugNodePath.Count; iDebugPath++)
        {
            int lRow = mPathFinder.mDebugNodePath[iDebugPath].aRow;
            int lCol = mPathFinder.mDebugNodePath[iDebugPath].aCol;
            int lParentRow = mPathFinder.mDebugNodePath[iDebugPath].mParent.aRow;
            int lParentCol = mPathFinder.mDebugNodePath[iDebugPath].mParent.aCol;

            if (mDebugLineRootObj == null)
            {
                mDebugLineRootObj = (GameObject)Instantiate(vDebugLineRoot);
            }
            _DrawDebugLine(new Vector3(mCells[lRow, lCol].position.x, 1.0f, mCells[lRow, lCol].position.z),
                          new Vector3(mCells[lParentRow, lParentCol].position.x, 1.0f, mCells[lParentRow, lParentCol].position.z));

            SetPathFindResult(mCells[lRow, lCol], Color.yellow);
        }
        // 최적화된 길
        for (int iPath = lPathCount - 1; iPath >= 0; iPath--)
        {
            int lRow = lPathNode[iPath].mRow;
            int lCol = lPathNode[iPath].mCol;
            SetPathFindResult(mCells[lRow, lCol], Color.red);
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // [test 코드]연결된 길을 그려줍니다.
    private void _DrawDebugLine(Vector3 pStartPos, Vector3 pEndPos)
    {
        GameObject lObj = (GameObject)Instantiate(vDebugLinePrefab);
        lObj.transform.parent = mDebugLineRootObj.transform;
        LineRenderer lLine = lObj.GetComponent<LineRenderer>();
        lLine.SetPosition(0, pStartPos);
        lLine.SetPosition(1, pEndPos);
    }
    //-------------------------------------------------------------------------------------------------------
    // [test 코드]모든 디버그용 길을 삭제합니다.
    public void RemoveDebugLine()
    {
        if (mDebugLineRootObj != null)
        {
            DestroyImmediate(mDebugLineRootObj.gameObject);
            mDebugLineRootObj = null;
        }

        if (mGridMesh != null)
        {
            if (meshFilter != null && meshFilter.sharedMesh != null)
            {
                var colors = meshFilter.sharedMesh.colors;
                if (colors != null)
                {
                    for (int i = 0; i < mCellRowCount * mCellColCount * 6; i++)
                        colors[i] = Color.black;
                }
                meshFilter.sharedMesh.colors = colors;
            }
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 셀 루트를 생성합니다.
    private void CreateCellMeshTemplate()
    {
        _hexUV = new Vector2[6];
        _hexVerticesSmall = new Vector3[6];
        _hexVerticesLarge = new Vector3[6];

        if (hexTile)
        {
            for (int i = 0; i < 6; i++)
            {
                _hexUV[i].x = Mathf.Cos((Mathf.PI / 3) * i) / 2 + 0.5f;
                _hexUV[i].y = Mathf.Sin((Mathf.PI / 3) * i) / 2 + 0.5f;
            }
            for (int i = 0; i < 6; i++)
            {
                _hexVerticesSmall[i].x = Mathf.Cos((Mathf.PI / 3) * i) * 2.85f;
                _hexVerticesSmall[i].z = Mathf.Sin((Mathf.PI / 3) * i) * 2.85f;
            }
            for (int i = 0; i < 6; i++)
            {
                _hexVerticesLarge[i].x = Mathf.Cos((Mathf.PI / 3) * i) * 3.3f;
                _hexVerticesLarge[i].z = Mathf.Sin((Mathf.PI / 3) * i) * 3.3f;
            }
        }
        else
        {
            Vector3 lTileWorldScale = new Vector3(mCellScaleX * Coord2.cToWorld, 1, mCellScaleZ * Coord2.cToWorld);
            Vector3 lHalfTileWorldScale = lTileWorldScale * 0.5f;

            _hexUV[0].x = _hexUV[1].x = _hexUV[5].x = lHalfTileWorldScale.x / lHalfTileWorldScale.z /2 + 0.5f;
            _hexUV[2].x = _hexUV[3].x = _hexUV[4].x = -lHalfTileWorldScale.x / lHalfTileWorldScale.z/2 + 0.5f;
            _hexUV[1].y = _hexUV[2].y = lHalfTileWorldScale.z / lHalfTileWorldScale.z/2 + 0.5f;
            _hexUV[4].y = _hexUV[5].y = -lHalfTileWorldScale.z / lHalfTileWorldScale.z/2 + 0.5f;
            _hexUV[0].y = 0.5f;
            _hexUV[3].y = 0.5f;

            _hexVerticesSmall[0].x = _hexVerticesSmall[1].x = _hexVerticesSmall[5].x = lHalfTileWorldScale.x * 0.9f;
            _hexVerticesSmall[2].x = _hexVerticesSmall[3].x = _hexVerticesSmall[4].x = -lHalfTileWorldScale.x * 0.9f;
            _hexVerticesSmall[1].z = _hexVerticesSmall[2].z = lHalfTileWorldScale.z * 0.9f;
            _hexVerticesSmall[4].z = _hexVerticesSmall[5].z = -lHalfTileWorldScale.z * 0.9f;
            _hexVerticesSmall[0].z = 0;
            _hexVerticesSmall[3].z = 0;


            _hexVerticesLarge[0].x = _hexVerticesLarge[1].x = _hexVerticesLarge[5].x = lHalfTileWorldScale.x;
            _hexVerticesLarge[2].x = _hexVerticesLarge[3].x = _hexVerticesLarge[4].x = -lHalfTileWorldScale.x;
            _hexVerticesLarge[1].z = _hexVerticesLarge[2].z = lHalfTileWorldScale.z;
            _hexVerticesLarge[4].z = _hexVerticesLarge[5].z = -lHalfTileWorldScale.z;
            _hexVerticesLarge[0].z = 0;
            _hexVerticesLarge[3].z = 0;
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 셀을 생성합니다.
    private void _CreateCell(int pCellRowIndex, int pCellColIndex)
    {
        // 셀을 생성합니다.
        GridCell lCreatedCell = new GridCell();
        lCreatedCell.SetGrid(this, pCellRowIndex, pCellColIndex);
        lCreatedCell.SetDefaultWayWeight();

        // 셀을 테이블에 등록합니다.
        mCells[pCellRowIndex, pCellColIndex] = lCreatedCell;

        // 셀이 덮는 블럭 4칸에 대해서 연결 정보를 넣습니다.
        int lBlockRowIndex = pCellRowIndex * 2;
        if ((pCellColIndex & 0x01) != 0) // 홀수줄 셀이라면
            --lBlockRowIndex; // 블럭 영역을 한 칸 아래로(-1) 내립니다.
        int lBlockColIndex = pCellColIndex * 2;

        if (lBlockRowIndex >= 0) // 홀수줄 셀의 경우 첫 블럭은 음의 영역일 수 있습니다.
        {
            mBlockCells[lBlockRowIndex + 0, lBlockColIndex + 0] = lCreatedCell;
            mBlockCells[lBlockRowIndex + 0, lBlockColIndex + 1] = lCreatedCell;
        }
        if (lBlockRowIndex < (vBlockRowCount - 1)) // 짝수줄 셀의 경우 마지막 블럭은 블럭 영역이 아닙니다.
        {
            mBlockCells[lBlockRowIndex + 1, lBlockColIndex + 0] = lCreatedCell;
            mBlockCells[lBlockRowIndex + 1, lBlockColIndex + 1] = lCreatedCell;
        }
    }

    public GridCell GetCell(int triangleIndex)
    {
        triangleIndex = triangleIndex / 4;

        int col = triangleIndex % mCellColCount;
        int row = triangleIndex / mCellColCount;

        if (row < mCellRowCount)
            return mCells[row, col];
        else
            return null;
    }

    //-------------------------------------------------------------------------------------------------------
    // 셀의 중심 좌표를 구합니다.
    public Coord2 GetCellCenterCoord(int pCellRowIndex, int pCellColIndex)
    {
        int lCellCoordX = pCellColIndex * mCellScaleX + vBlockScaleX;
        int lCellCoordZ;
        if ((pCellColIndex & 0x01) == 0) // 짝수열이라면
            lCellCoordZ = pCellRowIndex * mCellScaleZ + vBlockScaleZ;
        else // 홀수열이라면
            lCellCoordZ = pCellRowIndex * mCellScaleZ;

        return new Coord2(lCellCoordX, lCellCoordZ);
    }
    //-------------------------------------------------------------------------------------------------------
    // 데이터를 로드 합니다.
    private bool _LoadLayout(CSVObject pLayoutCSVObject)
    {
        vBlockRowCount = pLayoutCSVObject.RowNum;
        vBlockColCount = pLayoutCSVObject.ColNum;

        CreateGridCells();
        SetDefaultWayWeight();
        // 셀을 로딩합니다.
        for (int iRow = 0; iRow < vBlockRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < vBlockColCount; ++iCol)
            {
                String lCellDesc = pLayoutCSVObject.Get(iRow, iCol);
                //Debug.Log(String.Format("[{0}{1}] lCellDesc={2}", iRow, iCol, lCellDesc));
                if ((lCellDesc != null) &&
                    (lCellDesc.Length > 1))
                {
                    int lBlockRow = vBlockRowCount - 1 - iRow; // lBlockRow는 테이블에서 밑에서부터 0이라서 반전시킴
                    int lBlockCol = iCol; // lBlockCol은 왼쪽에서부터 0이므로 iCol과 같음

                    //Debug.Log(String.Format("[{0},{1}] lCellDesc={2}", lBlockRow, lBlockCol, lCellDesc));
                    int lCellCol = lBlockCol / 2;
                    int lCellRow;
                    if ((lCellCol & 0x01) == 0) // 짝수 컬럼의 셀이라면
                        lCellRow = lBlockRow / 2; // 행은 2로 나눈 값 그대로
                    else // 홀수 컬럼의 셀이라면
                        lCellRow = (lBlockRow + 1) / 2; // 한 블록 아래로 내려가므로 1을 더하고서 2로 나누어야 행이 됨

                    String[] lObjectDescs = lCellDesc.Split(MapGridTile.cLineSplitChar);
                    for (int iObject = 0; iObject < lObjectDescs.Length; ++iObject)
                        _SetCellInfo(lCellRow, lCellCol, lObjectDescs[iObject]);
                }
            }
        }
        CreateGridMesh();
        return true;
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell에 정보를 셋팅합니다.
    private void _SetCellInfo(int pCellRow, int pCellCol, String pObjectDesc)
    {
        //if (!String.IsNullOrEmpty(pObjectDesc))
        //    Debug.Log(String.Format("{0}/{1} - {2}", pCellRow, pCellCol, pObjectDesc));

        GridCell lCell = mCells[pCellRow, pCellCol];
        if (lCell == null)
        {
            Debug.LogError("This Cell is empty - " + pCellRow + "/" + pCellCol);
            return;
        }

        // 셀 토큰을 분리합니다.
        String[] lCellTokens = pObjectDesc.Split(MapGridTile.cAttributeTokenSplitChar);
        String lAttributeToken = lCellTokens[0]; // lAttributeToken는 "WAY", "CP" 같은 것들입니다.

        // 속성 토큰을 가지고 정보를 얻습니다.
        MapGridTileAttributeType lAttributeType = MapGridTile.ParseAttributeType(lAttributeToken);
        switch (lAttributeType)
        {
            case MapGridTileAttributeType.Tile:
                {
                    lCell.vTileType = MapGridTile.ParseTileType(lCellTokens[1]); // 두 번째 토큰에서 타일 타입을 얻어옵니다.
                }
                break;
            case MapGridTileAttributeType.Param:
                {
                    _SetCellParams(lCell, lCellTokens[1]); // 두 번째 토큰에서 파라미터를 얻어옵니다.
                }
                break;
            case MapGridTileAttributeType.Ground:
                {
                    lCell.vGroundType = MapGridTile.ParseGroundType(lCellTokens[1]); // 두 번째 토큰에서 지형 타입을 얻어옵니다.
                }
                break;
            case MapGridTileAttributeType.Weight:
                {
                    lCell.vWeight = int.Parse(lCellTokens[1]); // 두 번째 토큰에서 웨이트를 얻어옵니다.

                    mBlockNodeWeights[pCellRow, pCellCol].mRow = pCellRow;
                    mBlockNodeWeights[pCellRow, pCellCol].mCol = pCellCol;
                    mBlockNodeWeights[pCellRow, pCellCol].mWeight = lCell.vWeight;
                }
                break;
            case MapGridTileAttributeType.DirectionY:
                {
                    lCell.vRotation = int.Parse(lCellTokens[1]); // 두 번째 토큰에서 방향각을 얻어옵니다.
                }
                break;
        }

        lCell.RefreshMap();
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell에 파라미터를 지정합니다.
    private void _SetCellParams(GridCell pCell, String pParamsDesc)
    {
        String[] lParamTokens = pParamsDesc.Split(MapGridTile.cParamTokenSplitChar);

        pCell.vParam1 = int.Parse(lParamTokens[0]);
        pCell.vParam2 = int.Parse(lParamTokens[1]);
        pCell.vParam3 = int.Parse(lParamTokens[2]);
        pCell.vParam4 = int.Parse(lParamTokens[3]);

        pCell.vExtraString = lParamTokens[4]; // vExtraString에 ^가 포함되어 있을 수 있으므로 Ex> USDefenderCommander1^USDefenderCommander1^GermanDefenderCommander1
        for (int iToken = 5; iToken < lParamTokens.Length; ++iToken) // 나머지 토큰을 붙입니다.
        {
            pCell.vExtraString += "^";
            pCell.vExtraString += lParamTokens[iToken];
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell의 파라미터를 파싱 가능한 문자열로 만듭니다.
    private String _GetCellParams(GridCell pCell)
    {
        String lParams = String.Empty;

        lParams += pCell.vParam1.ToString();
        lParams += MapGridTile.cParamTokenSplitChar;
        lParams += pCell.vParam2.ToString();
        lParams += MapGridTile.cParamTokenSplitChar;
        lParams += pCell.vParam3.ToString();
        lParams += MapGridTile.cParamTokenSplitChar;
        lParams += pCell.vParam4.ToString();
        lParams += MapGridTile.cParamTokenSplitChar;
        lParams += pCell.vExtraString;

        return lParams;
    }
    //-------------------------------------------------------------------------------------------------------
    // 블록정보로 Cell에 정보를 받아 저장데이터로 변환합니다.
    private String _GetCellInfo(int pBlockRow, int pBlockCol)
    {
        String lResult = String.Empty;
        int lCellCol = pBlockCol / 2;
        int lCellRow;
        if ((lCellCol & 0x01) == 0) // 짝수 컬럼의 셀이라면
            lCellRow = pBlockRow / 2; // 행은 2로 나눈 값 그대로
        else // 홀수 컬럼의 셀이라면
            lCellRow = (pBlockRow + 1) / 2; // 한 블록 아래로 내려가므로 1을 더하고서 2로 나누어야 행이 됨

        GridCell lCell = mCells[lCellRow, lCellCol];

        if (lCell == null)
            return "\"\"";

        lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Tile.ToString(), lCell.vTileType); // "Tile:Way"
        lResult += MapGridTile.cLineSplitChar;
        lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Param.ToString(), _GetCellParams(lCell)); // "Param:1,2,3,4,Extra"
        lResult += MapGridTile.cLineSplitChar;
        lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Ground.ToString(), lCell.vGroundType); // "Ground:Grass"
        lResult += MapGridTile.cLineSplitChar;
        lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Weight.ToString(), lCell.vWeight); // "Weight:1"

        if (lCell.vRotation != 0)
        {
            lResult += MapGridTile.cLineSplitChar;
            lResult += String.Format("{0}:{1}", MapGridTileAttributeType.DirectionY.ToString(), lCell.vRotation); // "DirectionY:30"
        }

        return lResult;
    }
    //-------------------------------------------------------------------------------------------------------
    // 헤더 문자열을 생성합니다.
    private String _GetHeaderDesc()
    {
        String lResult = String.Empty;

        lResult += String.Format("{0}:{1}", LayoutHeader.Attribute.AStarHeuristic.ToString(), vAStarHeuristic); // "AStarHeuristic:50"
                                                                                                                //lResult += MapGridTile.cLineSplitChar;

        return lResult;
    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    Vector2[] _hexUV;
    Vector3[] _hexVerticesSmall;
    Vector3[] _hexVerticesLarge;

    private int mCellScaleX;
    private int mCellScaleZ;

    private int mCellColCount;
    private int mCellRowCount;

    private GridCell[,] mCells;
    private GridCell[,] mBlockCells;

    private NodeWeight[,] mBlockNodeWeights;

    private AStarPathFinder<NodeWeight> mPathFinder;

    // 디버깅용
    private GameObject mDebugLineRootObj;
}
