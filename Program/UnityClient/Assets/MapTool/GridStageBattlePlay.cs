﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

public class GridStageBattlePlay : MonoBehaviour 
{
    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	//public GridStageNewScript vGridStageNewScript;
    public GridLayoutTool vGridLayoutTool;
    public int vStageNo = 1;
    public String vMapPath = "Maps0/Map - newFarm_works";

    void Start()
    {
        //// 전투 테스트로 들어갑니다.
        //if (vGridStageNewScript == null)
        //    vGridStageNewScript = GameObject.FindObjectOfType<GridStageNewScript>();

        //if (vGridStageNewScript != null)
        //{
        //    GameData.get.SetMapTestInfo(
        //    vGridStageNewScript.vMapPath,
        //    vGridStageNewScript.vGridLayoutPath);

        //    GameData.get.EnterBattleTestPhase(vGridStageNewScript.vStageNo, true);
        //}


        if (vGridLayoutTool == null)
            vGridLayoutTool = GameObject.FindObjectOfType<GridLayoutTool>();

        if (vGridLayoutTool != null)
        {
            GameData.get.SetMapTestInfo(vMapPath, vGridLayoutTool.vBundleStageFolder + vGridLayoutTool.vGridLayoutName);
            GameData.get.EnterBattleTestPhase(vStageNo, true);
        }
    }
}
