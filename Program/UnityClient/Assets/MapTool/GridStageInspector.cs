﻿#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(GridStageNewScript))]
public class GridStageNew : Editor
{
    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        GridStageNewScript lGridNewScript = (GridStageNewScript)target;

        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Create Default Grid"))
        {
            lGridNewScript.CreateDefaultGrid();
        }

        if (GUILayout.Button("Clear All Grid"))
        {
            lGridNewScript.ClearAllGrid();
            PlayerPrefs.DeleteKey(GridStageNewScript.cTempKey + UnityEditor.EditorApplication.currentScene);
        }
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Refresh All"))
        {
            lGridNewScript.RefreshAll();
        }
        if (GUILayout.Button("Import"))
        {
            string path = EditorUtility.OpenFilePanel("Import Grid", "AssetData/Bundles/Resources/GridLayout0", "txt");
            if (!string.IsNullOrEmpty(path))
            {
                char[] splitToken = new char[] { '/', '.' };
                String[] lCSVTextPath = path.Split(splitToken);
                lGridNewScript.ImportGrid(lGridNewScript.vBundleStageFolder + lCSVTextPath[lCSVTextPath.Length - 2]);
                //Debug.Log("path - " + path);
            }
        }
        if (GUILayout.Button("Export"))
        {
            string path = EditorUtility.SaveFilePanel("Export Grid", "AssetData/Bundles/Resources/GridLayout0", "GridName", "txt");
            lGridNewScript.ExportGrid(path);
            AssetDatabase.Refresh();
        }
        EditorGUILayout.BeginHorizontal();
        if (GUILayout.Button("Set Default Way Weight"))
        {
            lGridNewScript.SetDefaultWayWeight();
        }
        if (GUILayout.Button("Remove AStar"))
        {
            lGridNewScript.RemoveDebugLine();
        }
        EditorGUILayout.EndHorizontal();
        if (GUILayout.Button("Test Search AStar"))
        {
            lGridNewScript.SearchTest();
        }
    }
}

//[CanEditMultipleObjects]
//[CustomEditor(typeof(MapGridCell))]
//public class MapGridCellEditor : Editor
//{
//    public override void OnInspectorGUI()
//    {
//        DrawDefaultInspector();

//        if (GUILayout.Button("Apply"))
//        {
//            for(int iTarget = 0; iTarget < targets.Length; iTarget++)
//            {
//                MapGridCell lMapGridCell = (MapGridCell)targets[iTarget];
//                if(!lMapGridCell.RefreshMap())
//                    break;
//            }
//        }
//		if (GUILayout.Button("Reset"))
//		{
//			for (int iTarget = 0; iTarget < targets.Length; iTarget++)
//			{
//				MapGridCell lMapGridCell = (MapGridCell)targets[iTarget];
//				lMapGridCell.ResetCell();
//			}
//		}
//		GUILayout.BeginHorizontal();
//		if (GUILayout.Button("Set Starter"))
//		{
//			MapGridCell lMapGridCell = (MapGridCell)target;
//			lMapGridCell.aGridStage.vAStarStartPos = new Vector2(lMapGridCell.aRow, lMapGridCell.aCol);
//		}
//		if (GUILayout.Button("Set Target"))
//		{
//			MapGridCell lMapGridCell = (MapGridCell)target;
//			lMapGridCell.aGridStage.vAStarTargetPos = new Vector2(lMapGridCell.aRow, lMapGridCell.aCol);
//		}
//		GUILayout.EndHorizontal();
//		if (GUILayout.Button("Test Search AStar"))
//		{
//			MapGridCell lMapGridCell = (MapGridCell)target;
//			lMapGridCell.aGridStage.SearchTest();
//		}
//	}
//}
#endif