﻿#if UNITY_EDITOR
using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[ExecuteInEditMode]
public class GridStageNewScript : MonoBehaviour 
{
	public struct LayoutHeader
	{
		public enum Attribute
		{
			AStarHeuristic,
		}
	}

	public const String cCellRootObjectName = "Cell Root";

    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public int vStageNo = 1;
	public String vMapPath = "Maps/Map - Test";
	public String vGridLayoutPath = "GridLayouts/GridLayout - Test";

	public int vBlockColCount = 110;
    public int vBlockRowCount = 101;
    public int vBlockScaleX = 250; 
    public int vBlockScaleZ = 275;
	public String vBundleStageFolder = "GridLayouts0/";

	public MapGridCell vCombatGridCellPrefab;
    public List<Color> vWayColor;
    public List<Color> vTileTypeColor;
	public int[] vGroundTypeDefaultWeight = new int[(int)MapGridGroundType.Empty + 1];

	public const String cTempKey = "TST";

	public GameObject vDebugLineRoot;
	public GameObject vDebugLinePrefab;
	public Vector2 vAStarStartPos;
	public Vector2 vAStarTargetPos;
	public int vAStarHeuristic = 50;

	public AStarPathFinder<NodeWeight> aPathFinder { get { return mPathFinder; } }

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------
	// 경로 비중 테이블
	public NodeWeight[,] aBlockNodeWeights
	{
		get { return mBlockNodeWeights; }
	}

    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
		MapGridTile.VerifyTable(); // 스크립트 초기화 때 한 번만 합니다.

		if (vTileTypeColor.Count != MapGridTile.cTileTypeCount)
			Debug.LogError("vTileTypeColor.Count mismatch - " + this);

		String lTempMapPath = PlayerPrefs.GetString(cTempKey + UnityEditor.EditorApplication.currentScene);
		if (!String.IsNullOrEmpty(lTempMapPath))
		{
			ClearAllGrid();
			ImportGrid(lTempMapPath);
		}
		else
		{
			ClearAllGrid();
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
    // 기본 cell grid를 생성합니다.
    public void CreateDefaultGrid()
    {
		ClearAllGrid();

        mCellScaleX = vBlockScaleX * 2;
        mCellScaleZ = vBlockScaleZ * 2;
 
        mCellRowCount = (vBlockRowCount + 1) / 2;
        mCellColCount = vBlockColCount / 2;

        mCells = new MapGridCell[mCellRowCount, mCellColCount];
        mBlockCells = new MapGridCell[vBlockRowCount, vBlockColCount];
		mBlockNodeWeights = new NodeWeight[mCellRowCount, mCellColCount];

        for (int iRow = 0; iRow < mCellRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < mCellColCount; ++iCol)
            {
                _CreateCell(iRow, iCol);
				mBlockNodeWeights[iRow, iCol] = new NodeWeight(iRow, iCol, mBlockCells[iRow, iCol].vWeight);
            }
        }
		mPathFinder = new AStarPathFinder<NodeWeight>();
		mPathFinder.InitNode(mBlockNodeWeights, mCellRowCount, mCellColCount, mCellScaleX, mCellScaleZ, vBlockScaleX, vBlockScaleZ, vAStarHeuristic);
        //Debug.Log("Created Default Grid");
    }
    //-------------------------------------------------------------------------------------------------------
    // 모든 맵을 재설정합니다.
    public bool RefreshAll()
    {
        if (mCells == null)
            return false;
        for (int iCellRow = 0; iCellRow < mCellRowCount; iCellRow++)
        {
            for (int iCellCol = 0; iCellCol < mCellColCount; iCellCol++)
            {
                MapGridCell lCell = mCells[iCellRow, iCellCol];
                if (lCell == null)
                    continue;

                if (!lCell.RefreshMap())
                    return false;
            }
        }
        return true;
    }
    //-------------------------------------------------------------------------------------------------------
    // 기존 CSV 파일을 import합니다.
    public void ImportGrid(String pCsvText)
    {
        if (mCells != null && mBlockCells != null)
        {
            Debug.LogError("Already Set blocks. please Clear All Grid!! in Map Tool");
            return;
        }

		vGridLayoutPath = pCsvText;
		_SaveTempPath();

        // 레이아웃의 첫 줄을 읽습니다.
        CSVObject lLayoutCsvObject = new CSVObject();
		TextAsset lTextAsset = ResourceUtil.Load<TextAsset>(vGridLayoutPath) as TextAsset;
        lLayoutCsvObject.LoadCSV(lTextAsset.text, 0); // 헤더를 가지고 있다고 가정합니다.

        // 레이아웃을 로딩합니다.
        _LoadLayout(lLayoutCsvObject);
    }
    //-------------------------------------------------------------------------------------------------------
    // 기존 CSV 파일을 export합니다.
    public void ExportGrid(String pCSVFullPath)
    {
        if (!RefreshAll())
            return;

		char[] splitToken = new char[] { '/', '.' };
		String[] lCSVTextPath = pCSVFullPath.Split(splitToken);
		vGridLayoutPath = vBundleStageFolder + lCSVTextPath[lCSVTextPath.Length - 2];
		_SaveTempPath();

		Debug.Log("Full Path - " + pCSVFullPath);

        StreamWriter sw = new StreamWriter(pCSVFullPath, false);
        // 가장 윗 라인은 col의 갯수만큼 숫자를 작성 (0~Count-1)
        String lLineData = String.Empty;
		lLineData += String.Format("\"{0}\"", _GetHeaderDesc()); // 첫 컬럼은 헤더 정보를 넣습니다.
		lLineData += ","; 
        for(int iBlockCount = 1 ; iBlockCount < vBlockColCount; iBlockCount++) // 첫 컬럼을 제외한 나머지 컬럼이라서 1부터 시작
        {
            lLineData += String.Format("\"{0}\"", iBlockCount);
            if (iBlockCount + 1 < vBlockColCount)
            {
                lLineData += ","; 
            }
        }
        sw.WriteLine(lLineData);

        lLineData = String.Empty;
        for (int iRow = vBlockRowCount - 1; iRow >= 0; iRow--)
        {
            for (int iCol = 0; iCol < vBlockColCount; iCol++)
            {
                if (iCol % 2 == 0 && iRow % 2 == 0)
                {
                    lLineData += _GetCellInfo(iRow, iCol);
                }
                else
                {
                    lLineData += "\"\"";
                }
                if (iCol + 1 < vBlockColCount)
                    lLineData += ",";
            }
            sw.WriteLine(lLineData);
            lLineData = String.Empty;
        }

        sw.Close();
    }
    //-------------------------------------------------------------------------------------------------------
    // 기본 cell grid를 생성합니다.
    public void ClearAllGrid()
    {
		if (mCells != null)
		{
			for (int iRow = 0; iRow < mCellRowCount; ++iRow)
			{
				for (int iCol = 0; iCol < mCellColCount; ++iCol)
				{
					if (mCells[iRow, iCol] != null)
						mCells[iRow, iCol].ClearGrid();
				}
			}
			mCells = null;
			mBlockCells = null;
		}
		_CreateCellRoot();
		RemoveDebugLine();
	}
	//-------------------------------------------------------------------------------------------------------
	// 모든 way에 대해 Default Weight 값을 적용합니다.
	public void SetDefaultWayWeight()
	{
		if (mCells != null)
		{
			for (int iRow = 0; iRow < mCellRowCount; iRow++)
			{
				for (int iCol = 0; iCol < mCellColCount; iCol++)
				{
					if (mCells[iRow, iCol] != null) 
					{
						mCells[iRow, iCol].SetDefaultWayWeight();
					}
				}
			}
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [test 코드]길을 탐색합니다.
	public void SearchTest()
	{
		RemoveDebugLine();
		NodeWeight[] lOutResultNodes = new NodeWeight[1000];
		int lNodeCount;

		mPathFinder.SetHeuristicValue(vAStarHeuristic);
		mPathFinder.Search(mBlockNodeWeights[(int)vAStarStartPos.x, (int)vAStarStartPos.y], 
						   mBlockNodeWeights[(int)vAStarTargetPos.x, (int)vAStarTargetPos.y], 
						   null, // 기본 델리게이트를 사용
						   lOutResultNodes, 
						   out lNodeCount);

		DrawDebugPath(lOutResultNodes, lNodeCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// [test 코드]길을 그려줍니다.
	private void DrawDebugPath(NodeWeight[] lPathNode, int lPathCount)
	{
		// 디버깅용 들렸던 길
		for (int iDebugPath = 0; iDebugPath < mPathFinder.mDebugNodePath.Count; iDebugPath++)
		{
			int lRow = mPathFinder.mDebugNodePath[iDebugPath].aRow;
			int lCol = mPathFinder.mDebugNodePath[iDebugPath].aCol;
			int lParentRow = mPathFinder.mDebugNodePath[iDebugPath].mParent.aRow;
			int lParentCol = mPathFinder.mDebugNodePath[iDebugPath].mParent.aCol;

			if (mDebugLineRootObj == null)
			{
				mDebugLineRootObj = (GameObject)Instantiate(vDebugLineRoot);
			}
			_DrawDebugLine(new Vector3(mCells[lRow, lCol].aTransform.position.x, 1.0f, mCells[lRow, lCol].aTransform.position.z),
						  new Vector3(mCells[lParentRow, lParentCol].aTransform.position.x, 1.0f, mCells[lParentRow, lParentCol].aTransform.position.z));

			MapGridCell lCell = mCells[lRow, lCol];
			lCell.ShowCheckPointLink(Color.yellow);
		}
		// 최적화된 길
		for (int iPath = lPathCount - 1; iPath >= 0; iPath--)
		{
			int lRow = lPathNode[iPath].mRow;
			int lCol = lPathNode[iPath].mCol;
			MapGridCell lCell = mCells[lRow, lCol];
			lCell.ShowCheckPointLink(Color.red);
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// [test 코드]연결된 길을 그려줍니다.
	private void _DrawDebugLine(Vector3 pStartPos, Vector3 pEndPos)
	{
		GameObject lObj = (GameObject)Instantiate(vDebugLinePrefab);
		lObj.transform.parent = mDebugLineRootObj.transform;
		LineRenderer lLine = lObj.GetComponent<LineRenderer>();
		lLine.SetPosition(0, pStartPos);
		lLine.SetPosition(1, pEndPos);
	}
	//-------------------------------------------------------------------------------------------------------
	// [test 코드]모든 디버그용 길을 삭제합니다.
	public void RemoveDebugLine()
	{
		if (mDebugLineRootObj != null)
		{
			DestroyImmediate(mDebugLineRootObj.gameObject);
			mDebugLineRootObj = null;
		}
		if (mCells != null)
		{
			for (int iRow = 0; iRow < mCellRowCount; iRow++)
			{
				for (int iCol = 0; iCol < mCellColCount; iCol++)
				{
					MapGridCell lCell = mCells[iRow, iCol];
					lCell.ShowCheckPointLink(new Color(1, 1, 1, 0));
				}
			}
		}
	}

    //-------------------------------------------------------------------------------------------------------
    // Check point로 통하는 way 검색 및 표시
// 	public void GoCheckPointLinkCheck(String pCheckPointString, int pWayIndex)
// 	{
// 		for (int iCellRow = 0; iCellRow < mCellRowCount; iCellRow++)
// 		{
// 			for (int iCellCol = 0; iCellCol < mCellColCount; iCellCol++)
// 			{
// 				MapGridCell lCell = mCells[iCellRow, iCellCol];
// 				if (lCell == null)
// 					continue;
// 
// 				if (pWayIndex == lCell.vWayIndex)
// 				{
// 					if(lCell.vWayFrom.ToUpper().Equals(pCheckPointString.ToUpper())) // From 값을 갖고 있는 길를 표시.
// 						lCell.ShowCheckPointLink(new Color(0f, 1f, 0f, 1f));
// 					else if (lCell.vWayTo.ToUpper().Equals(pCheckPointString.ToUpper()))// To 값을 갖고 있는 길을 표시.
// 						lCell.ShowCheckPointLink(new Color(0f, 1f, 1f, 1f));
// 					else
// 						lCell.ShowCheckPointLink(new Color(0f, 0f, 0f, 0f)); // 길을 표시하지 않음
// 				}
// 				else
// 				{
// 					lCell.ShowCheckPointLink(new Color(0f, 0f, 0f, 0f)); // 길 번호가 틀릴 경우 표시하지 않음
// 				}
// 			}
// 		}
// 	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 셀 루트를 생성합니다.
    private void _CreateCellRoot()
    {
		GameObject lOldCellRootObject = GameObject.Find(cCellRootObjectName);
		//Debug.Log("lOldCellRootObject=" + lOldCellRootObject);
		if (lOldCellRootObject != null)
			DestroyImmediate(lOldCellRootObject);

		GameObject lNewCellRootObject = new GameObject(cCellRootObjectName);
		mCellRoot = lNewCellRootObject.transform;
		//Debug.Log("lNewCellRootObject=" + lNewCellRootObject);
	}
    //-------------------------------------------------------------------------------------------------------
    // 셀을 생성합니다.
    private void _CreateCell(int pCellRowIndex, int pCellColIndex)
    {
        // 셀을 생성합니다.
        MapGridCell lCreatedCell = ObjectUtil.InstantiateComponentObject<MapGridCell>(vCombatGridCellPrefab);
        lCreatedCell.SetGrid(this, pCellRowIndex, pCellColIndex);
		lCreatedCell.SetDefaultWayWeight();
		lCreatedCell.aTransform.parent = mCellRoot;
		lCreatedCell.gameObject.name = String.Format("MapGridCell[{0},{1}]", pCellRowIndex, pCellColIndex);

        // 셀을 테이블에 등록합니다.
        mCells[pCellRowIndex, pCellColIndex] = lCreatedCell;

        // 셀이 덮는 블럭 4칸에 대해서 연결 정보를 넣습니다.
        int lBlockRowIndex = pCellRowIndex * 2;
        if ((pCellColIndex & 0x01) != 0) // 홀수줄 셀이라면
            --lBlockRowIndex; // 블럭 영역을 한 칸 아래로(-1) 내립니다.
        int lBlockColIndex = pCellColIndex * 2;

        if (lBlockRowIndex >= 0) // 홀수줄 셀의 경우 첫 블럭은 음의 영역일 수 있습니다.
        {
            mBlockCells[lBlockRowIndex + 0, lBlockColIndex + 0] = lCreatedCell;
            mBlockCells[lBlockRowIndex + 0, lBlockColIndex + 1] = lCreatedCell;
        }
        if (lBlockRowIndex < (vBlockRowCount - 1)) // 짝수줄 셀의 경우 마지막 블럭은 블럭 영역이 아닙니다.
        {
            mBlockCells[lBlockRowIndex + 1, lBlockColIndex + 0] = lCreatedCell;
            mBlockCells[lBlockRowIndex + 1, lBlockColIndex + 1] = lCreatedCell;
        }
    }

    //-------------------------------------------------------------------------------------------------------
    // 셀의 중심 좌표를 구합니다.
    public Coord2 GetCellCenterCoord(int pCellRowIndex, int pCellColIndex)
    {
        int lCellCoordX = pCellColIndex * mCellScaleX + vBlockScaleX;
        int lCellCoordZ;
        if ((pCellColIndex & 0x01) == 0) // 짝수열이라면
            lCellCoordZ = pCellRowIndex * mCellScaleZ + vBlockScaleZ;
        else // 홀수열이라면
            lCellCoordZ = pCellRowIndex * mCellScaleZ;

        return new Coord2(lCellCoordX, lCellCoordZ);
    }
    //-------------------------------------------------------------------------------------------------------
    // 데이터를 로드 합니다.
    private void _LoadLayout(CSVObject pLayoutCSVObject)
    {
        vBlockRowCount = pLayoutCSVObject.RowNum;
        vBlockColCount = pLayoutCSVObject.ColNum;

        CreateDefaultGrid();
		SetDefaultWayWeight();
        // 셀을 로딩합니다.
        for (int iRow = 0; iRow < vBlockRowCount; ++iRow)
        {
            for (int iCol = 0; iCol < vBlockColCount; ++iCol)
            {
                String lCellDesc = pLayoutCSVObject.Get(iRow, iCol);
                //Debug.Log(String.Format("[{0}{1}] lCellDesc={2}", iRow, iCol, lCellDesc));
                if ((lCellDesc != null) &&
                    (lCellDesc.Length > 1))
                {
                    int lBlockRow = vBlockRowCount - 1 - iRow; // lBlockRow는 테이블에서 밑에서부터 0이라서 반전시킴
                    int lBlockCol = iCol; // lBlockCol은 왼쪽에서부터 0이므로 iCol과 같음

                    //Debug.Log(String.Format("[{0},{1}] lCellDesc={2}", lBlockRow, lBlockCol, lCellDesc));
                    int lCellCol = lBlockCol / 2;
                    int lCellRow;
                    if ((lCellCol & 0x01) == 0) // 짝수 컬럼의 셀이라면
                        lCellRow = lBlockRow / 2; // 행은 2로 나눈 값 그대로
                    else // 홀수 컬럼의 셀이라면
                        lCellRow = (lBlockRow + 1) / 2; // 한 블록 아래로 내려가므로 1을 더하고서 2로 나누어야 행이 됨

					String[] lObjectDescs = lCellDesc.Split(MapGridTile.cLineSplitChar);
                    for (int iObject = 0; iObject < lObjectDescs.Length; ++iObject)
                        _SetCellInfo(lCellRow, lCellCol, lObjectDescs[iObject]);
                }
            }
        }
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell에 정보를 셋팅합니다.
    private void _SetCellInfo(int pCellRow, int pCellCol, String pObjectDesc)
    {
        //if (!String.IsNullOrEmpty(pObjectDesc))
        //    Debug.Log(String.Format("{0}/{1} - {2}", pCellRow, pCellCol, pObjectDesc));

        MapGridCell lCell = mCells[pCellRow, pCellCol];
        if (lCell == null)
        {
            Debug.LogError("This Cell is empty - " + pCellRow + "/" + pCellCol);
            return;
        }

		// 셀 토큰을 분리합니다.
		String[] lCellTokens = pObjectDesc.Split(MapGridTile.cAttributeTokenSplitChar);
		String lAttributeToken = lCellTokens[0]; // lAttributeToken는 "WAY", "CP" 같은 것들입니다.

		// 속성 토큰을 가지고 정보를 얻습니다.
		MapGridTileAttributeType lAttributeType = MapGridTile.ParseAttributeType(lAttributeToken);
		switch (lAttributeType)
		{
		case MapGridTileAttributeType.Tile:
			{
				lCell.vTileType = MapGridTile.ParseTileType(lCellTokens[1]); // 두 번째 토큰에서 타일 타입을 얻어옵니다.
			}
			break;
		case MapGridTileAttributeType.Param:
			{
				_SetCellParams(lCell, lCellTokens[1]); // 두 번째 토큰에서 파라미터를 얻어옵니다.
			}
			break;
		case MapGridTileAttributeType.Ground:
			{
				lCell.vGroundType = MapGridTile.ParseGroundType(lCellTokens[1]); // 두 번째 토큰에서 지형 타입을 얻어옵니다.
			}
			break;
		case MapGridTileAttributeType.Weight:
			{
				lCell.vWeight = int.Parse(lCellTokens[1]); // 두 번째 토큰에서 웨이트를 얻어옵니다.

				mBlockNodeWeights[pCellRow, pCellCol].mRow = pCellRow;
				mBlockNodeWeights[pCellRow, pCellCol].mCol = pCellCol;
				mBlockNodeWeights[pCellRow, pCellCol].mWeight = lCell.vWeight;
			}
			break;
		case MapGridTileAttributeType.DirectionY:
			{
				int lDirectionY = int.Parse(lCellTokens[1]); // 두 번째 토큰에서 방향각을 얻어옵니다.
				lCell.aTransform.rotation = Quaternion.Euler(90.0f, lDirectionY, 0.0f);
			}
			break;
		}

        lCell.RefreshMap();
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell에 파라미터를 지정합니다.
	private void _SetCellParams(MapGridCell pCell, String pParamsDesc)
    {
		String[] lParamTokens = pParamsDesc.Split(MapGridTile.cParamTokenSplitChar);

		pCell.vParam1 = int.Parse(lParamTokens[0]);
		pCell.vParam2 = int.Parse(lParamTokens[1]);
		pCell.vParam3 = int.Parse(lParamTokens[2]);
		pCell.vParam4 = int.Parse(lParamTokens[3]);

		pCell.vExtraString = lParamTokens[4]; // vExtraString에 ^가 포함되어 있을 수 있으므로 Ex> USDefenderCommander1^USDefenderCommander1^GermanDefenderCommander1
		for (int iToken = 5; iToken < lParamTokens.Length; ++iToken) // 나머지 토큰을 붙입니다.
		{
			pCell.vExtraString += "^";
			pCell.vExtraString += lParamTokens[iToken];
		}
	}
	//-------------------------------------------------------------------------------------------------------
	// Cell의 파라미터를 파싱 가능한 문자열로 만듭니다.
	private String _GetCellParams(MapGridCell pCell)
	{
		String lParams = String.Empty;

		lParams += pCell.vParam1.ToString();
		lParams += MapGridTile.cParamTokenSplitChar;
		lParams += pCell.vParam2.ToString();
		lParams += MapGridTile.cParamTokenSplitChar;
		lParams += pCell.vParam3.ToString();
		lParams += MapGridTile.cParamTokenSplitChar;
		lParams += pCell.vParam4.ToString();
		lParams += MapGridTile.cParamTokenSplitChar;
		lParams += pCell.vExtraString;

		return lParams;
	}
	//-------------------------------------------------------------------------------------------------------
    // 블록정보로 Cell에 정보를 받아 저장데이터로 변환합니다.
    private String _GetCellInfo(int pBlockRow, int pBlockCol)
    {
        String lResult = String.Empty;
        int lCellCol = pBlockCol / 2;
        int lCellRow;
        if ((lCellCol & 0x01) == 0) // 짝수 컬럼의 셀이라면
            lCellRow = pBlockRow / 2; // 행은 2로 나눈 값 그대로
        else // 홀수 컬럼의 셀이라면
            lCellRow = (pBlockRow + 1) / 2; // 한 블록 아래로 내려가므로 1을 더하고서 2로 나누어야 행이 됨

        MapGridCell lCell = mCells[lCellRow, lCellCol];

        if (lCell == null)
            return "\"\"";

		lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Tile.ToString(), lCell.vTileType); // "Tile:Way"
		lResult += MapGridTile.cLineSplitChar;
		lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Param.ToString(), _GetCellParams(lCell)); // "Param:1,2,3,4,Extra"
		lResult += MapGridTile.cLineSplitChar;
		lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Ground.ToString(), lCell.vGroundType); // "Ground:Grass"
		lResult += MapGridTile.cLineSplitChar;
		lResult += String.Format("{0}:{1}", MapGridTileAttributeType.Weight.ToString(), lCell.vWeight); // "Weight:1"

		int lTileDirectionY = (int)lCell.aTransform.rotation.eulerAngles.y;
		if (lTileDirectionY != 0)
		{
			lResult += MapGridTile.cLineSplitChar;
			lResult += String.Format("{0}:{1}", MapGridTileAttributeType.DirectionY.ToString(), lTileDirectionY); // "DirectionY:30"
		}
		
		return lResult;
    }
	//-------------------------------------------------------------------------------------------------------
	// 헤더 문자열을 생성합니다.
	private String _GetHeaderDesc()
	{
		String lResult = String.Empty;

		lResult += String.Format("{0}:{1}", LayoutHeader.Attribute.AStarHeuristic.ToString(), vAStarHeuristic); // "AStarHeuristic:50"
		//lResult += MapGridTile.cLineSplitChar;

		return lResult;
	}
	//-------------------------------------------------------------------------------------------------------
	// 씬 전환시 불러올 맵을 임시 저장합니다.
	private void _SaveTempPath()
	{
		if(!String.IsNullOrEmpty(vGridLayoutPath))
			PlayerPrefs.SetString(cTempKey + UnityEditor.EditorApplication.currentScene, vGridLayoutPath);
	}
    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private int mCellScaleX;
    private int mCellScaleZ;
    private Vector3 mCellWorldScale;
    private Vector3 mBlockWorldScale;
      
    private int mCellRowCount;
    private int mCellColCount;

    private MapGridCell[,] mCells;
    private MapGridCell[,] mBlockCells;

	private NodeWeight[,] mBlockNodeWeights;

	private Transform mCellRoot;

	private AStarPathFinder<NodeWeight> mPathFinder;

	// 디버깅용
	private GameObject mDebugLineRootObj;
}
#endif
