﻿using UnityEngine;
using System.Collections;

public class LightmapInfo : MonoBehaviour {
    public int lightmapOffsetIndexForDebugging = 0;
    public int localLightmapIndex = -1;
    public Vector4 lightmapScaleOffset;
    

    // Use this for initialization
    public void Apply(MeshRenderer renderer, int lightmapOffsetIndex) {
        lightmapOffsetIndexForDebugging = lightmapOffsetIndex;
        renderer.lightmapIndex = localLightmapIndex + lightmapOffsetIndex;
        renderer.lightmapScaleOffset = lightmapScaleOffset;
    }

    // Update is called once per frame
    void Update () {
	
	}
}
