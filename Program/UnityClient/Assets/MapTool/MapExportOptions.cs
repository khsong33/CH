﻿using UnityEngine;
using System.Linq;
using System.Collections;
using System.Collections.Generic;

public class MapExportOptions : MonoBehaviour {
    public List<GameObject> gameObjectsToExport = new List<GameObject>();

    private string exportFolder;

    public bool exportTextures = true;
    bool overwriteTextures = true;
    bool materialInstancePercautions = true;
    bool matNameFromTextureName = false;
    SplitType splitType = SplitType.By_Mesh;

    public bool selectedObjectOnly = true;

    bool applyTransform = true;
    public bool exportNormal = false;

    public enum SplitType
    {
        By_Mesh,
        By_Material,
        By_Submesh,
        None
    }

    private string ConstructFaceString(int i1, int i2, int i3)
    {
        if (exportNormal)
            return "" + i1 + "/" + i2 + "/" + i3;
        else
            return "" + i1 + "/" + i3;
    }

    private List<string> matNameCache = new List<string>();

    private string GenerateMaterialNew(ref System.Text.StringBuilder sbR, ref Material m)
    {
        string matName = m.name;
        if (matNameFromTextureName && m.mainTexture != null)
        {
            Texture2D mT = m.mainTexture as Texture2D;
            if (mT != null)
            {
                matName = m.mainTexture.name;
            }
            else
            {
                Debug.Log("Could not generate material from texture name (" + matName + "). No texture assigned");
            }
        }
        bool instance = matName.Contains("Instance");
        if (instance && materialInstancePercautions)
        {
            matName += "_(" + m.GetInstanceID() + ")";
        }

        if (matNameCache.Contains(matName) == false)
        {
            matNameCache.Add(matName);
            sbR.AppendLine("newmtl " + matName);

            bool hasColor = m.HasProperty("_Color");
            if (hasColor)
            {
                Color matColor = m.color;
                sbR.AppendLine("Kd " + matColor.r + " " + matColor.g + " " + matColor.b);
                //float alpha = Mathf.Lerp(0, 1, matColor.a);
                sbR.AppendLine("d " + matColor.a);//alpha);
            }

            bool hasSpecular = m.HasProperty("_SpecColor");
            if (hasSpecular)
            {
                Color specColor = m.GetColor("_SpecColor");
                sbR.AppendLine("Ks " + specColor.r + " " + specColor.g + " " + specColor.b);
            }
            if (m.mainTexture != null)
            {
                Texture2D mainTex = m.mainTexture as Texture2D;

                if (mainTex != null)
                {
                    Vector2 mainTexScale = m.mainTextureScale;
                    if (mainTex.wrapMode == TextureWrapMode.Clamp)
                    {
                        sbR.AppendLine("-clamp on");
                    }
                    sbR.AppendLine("s " + mainTexScale.x + " " + mainTexScale.y);
                    sbR.AppendLine("map_Kd " + "textures/" + mainTex.name + ".png");

                    if (exportTextures)
                        ExportTexture(mainTex);
                }
            }
            sbR.AppendLine();
        }
        return matName;
    }

    private void ExportTexture(Texture2D mainTex)
    {
        if (overwriteTextures || System.IO.File.Exists(exportFolder + "/textures/" + mainTex.name + ".png") == false)
        {
            RenderTexture tmp = RenderTexture.GetTemporary(mainTex.width, mainTex.height, 0, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Linear);

            // Blit the pixels on texture to the RenderTexture
            Graphics.Blit(mainTex, tmp);
            // Backup the currently set RenderTexture
            RenderTexture previous = RenderTexture.active;
            // Set the current RenderTexture to the temporary one we created
            RenderTexture.active = tmp;
            // Create a new readable Texture2D to copy the pixels to it
            Texture2D myTexture2D = new Texture2D(mainTex.width, mainTex.height);
            myTexture2D.ReadPixels(new Rect(0, 0, tmp.width, tmp.height), 0, 0);
            myTexture2D.Apply();

            // Copy the pixels from the RenderTexture to the new Texture
            byte[] pngEx = myTexture2D.EncodeToPNG();
            System.IO.File.WriteAllBytes(exportFolder + "/textures/" + mainTex.name + ".png", pngEx);

            // Reset the active RenderTexture
            RenderTexture.active = previous;
            // Release the temporary RenderTexture
            RenderTexture.ReleaseTemporary(tmp);
        }
    }

    private Vector3 RotateAroundPoint(Vector3 point, Vector3 pivot, Quaternion angle)
    {
        return angle * (point - pivot) + pivot;
    }

    private Vector3 MultiplyVec3s(Vector3 v1, Vector3 v2)
    {
        return new Vector3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
    }

    public void Export(string baseDir, string mapName, bool showFolder)
    {
        matNameCache.Clear();

        exportFolder = baseDir;
        if (System.IO.Directory.Exists(exportFolder) == false)
            System.IO.Directory.CreateDirectory(exportFolder);

        if (System.IO.Directory.Exists(exportFolder + "/textures") == false)
            System.IO.Directory.CreateDirectory(exportFolder + "/textures");


        System.Text.StringBuilder sb = new System.Text.StringBuilder();
        System.Text.StringBuilder sb2 = new System.Text.StringBuilder();

        sb.AppendLine("mtllib " + mapName + ".mtl");

        MeshFilter[] sceneMeshes;
        if (selectedObjectOnly)
        {
            List<MeshFilter> tempMFList = new List<MeshFilter>();
            foreach (GameObject go in gameObjectsToExport)
            {
                if (go != null)
                    tempMFList.AddRange(go.GetComponentsInChildren<MeshFilter>());
            }

            sceneMeshes = tempMFList.Distinct().Where(x => x.gameObject.activeSelf == true).ToArray();
        }
        else
        {
            sceneMeshes = FindObjectsOfType(typeof(MeshFilter)) as MeshFilter[];
        }

        int cFaceIndex = 1;

        if (splitType == SplitType.None)
        {
            sb.AppendLine("g default");
        }

        for (int i = 0; i < sceneMeshes.Length; i++)
        {
            int wVerts = 0;

            if (sceneMeshes[i].name.Contains("MapGridCell["))
                continue;

            Mesh m = sceneMeshes[i].sharedMesh;
            GameObject go = sceneMeshes[i].gameObject;
            if (go.activeSelf == false || m == null)
                continue;

            if (splitType == SplitType.By_Mesh)
                sb.AppendLine("g " + go.name + "[" + go.GetInstanceID() + "]");

            foreach (Vector3 vertex in m.vertices)
            {
                Vector3 vx = vertex;
                if (applyTransform)
                {
                    vx = MultiplyVec3s(vx, go.transform.lossyScale);
                    vx = RotateAroundPoint(vx, Vector3.zero, go.transform.rotation);
                    vx += go.transform.position;
                }

                sb.AppendLine("v " + (vx.x * -1) + " " + vx.y + " " + vx.z);
                wVerts += 1;
            }

            if (exportNormal)
            {
                foreach (Vector3 vrtx in m.normals)
                {
                    Vector3 vx = vrtx;
                    if (applyTransform)
                        vx = RotateAroundPoint(vrtx, Vector3.zero, go.transform.rotation);
                    sb.AppendLine("vn " + (vx.x * -1) + " " + vx.y + " " + vx.z);
                }
            }

            //foreach (Vector2 vx in m.uv)
            //    sb.AppendLine("vt " + vx.x + " " + vx.y);
            for (int v = 0; v < m.uv.Length; v++)
            {
                float vx = (m.uv[v].x + m.uv3[v].x) / 16;
                sb.AppendLine("vt " + vx + " " + m.uv[v].y);
            }

            bool hasMeshRenderer = false;

            Renderer mr = go.GetComponent<Renderer>();
            if (mr != null)
                hasMeshRenderer = true;

            for (int j = 0; j < m.subMeshCount; j++)
            {
                if (splitType == SplitType.By_Submesh)
                    sb.AppendLine("g " + go.name + "[" + go.GetInstanceID() + ",SM" + j + "]");

                if (hasMeshRenderer)
                {
                    if (j <= (mr.sharedMaterials.Length - 1))
                    {
                        Material ml = mr.sharedMaterials[j];
                        string matName = GenerateMaterialNew(ref sb2, ref ml);
                        if (splitType == SplitType.By_Material)
                            sb.AppendLine("g " + matName);
                        sb.AppendLine("usemtl " + matName);
                    }
                }
                int[] indices = m.GetTriangles(j);

                for (int k = 0; k < indices.Length; k += 3)
                {
                    string face3 = ConstructFaceString(indices[k + 2] + cFaceIndex, indices[k + 2] + cFaceIndex, indices[k + 2] + cFaceIndex);
                    string face2 = ConstructFaceString(indices[k + 1] + cFaceIndex, indices[k + 1] + cFaceIndex, indices[k + 1] + cFaceIndex);
                    string face1 = ConstructFaceString(indices[k] + cFaceIndex, indices[k] + cFaceIndex, indices[k] + cFaceIndex);

                    sb.AppendLine("f " + face3 + " " + face2 + " " + face1);
                }
            }
            cFaceIndex += wVerts;
        }

        System.IO.File.WriteAllText(exportFolder + "/" + mapName + ".obj", sb.ToString());
        System.IO.File.WriteAllText(exportFolder + "/" + mapName + ".mtl", sb2.ToString());
        if (showFolder)
        {
            System.Diagnostics.Process.Start(exportFolder);
        }
    }

    private static string CreateToolDir()
    {
        var baseDir = Application.dataPath + @"/../../";
        if (System.IO.Directory.Exists(baseDir + "Tools") == false)
            System.IO.Directory.CreateDirectory(baseDir + "Tools");

        if (System.IO.Directory.Exists(baseDir + "Tools/TempMaps") == false)
            System.IO.Directory.CreateDirectory(baseDir + "Tools/TempMaps");
        return baseDir + "Tools/TempMaps";
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
