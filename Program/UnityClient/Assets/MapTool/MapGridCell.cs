﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;
using UnityEditor;

public class MapGridCell : MonoBehaviour 
{
    public enum MaterialType
    {
        Link = 0,
        Way = 1,
        Type,
        GroundType,
        MaterialTypeMax
    }

    //-------------------------------------------------------------------------------------------------------
    // 인스펙터
    //-------------------------------------------------------------------------------------------------------
	public int vWayIndex = 0;

	public MapGridTileType vTileType = MapGridTileType.Way;
	public int vParam1 = 0;
	public int vParam2 = 0;
	public int vParam3 = 0;
	public int vParam4 = 0;
	public String vExtraString;
    public MapGridGroundType vGroundType = MapGridGroundType.Grass;
	public int vWeight = 0;

    //-------------------------------------------------------------------------------------------------------
    // 속성
    //-------------------------------------------------------------------------------------------------------
    public Transform aTransform
    {
        get { return mTransform; }
    }
	public int aRow 
	{ 
		get { return mRow; } 
	}
	public int aCol 
	{ 
		get { return mCol; } 
	}
	public GridStageNewScript aGridStage
	{
		get { return mGridStage; }
	}
    //-------------------------------------------------------------------------------------------------------
    // 콜백
    //-------------------------------------------------------------------------------------------------------
    // 시작전 콜백
    void Awake()
    {
        mTransform = transform;
    }

    //-------------------------------------------------------------------------------------------------------
    // 메서드
    //-------------------------------------------------------------------------------------------------------
	// 격자를 지정합니다.
	public void SetGrid(GridStageNewScript pCombatGrid, int pCellRowIndex, int pCellColIndex)
	{
        if (mTransform == null)
            mTransform = transform;

        _InitMaterial();
        mGridStage = pCombatGrid;
        //mCellIndex = new RowCol(pCellRowIndex, pCellColIndex);
		Coord2 mCenterCoord = pCombatGrid.GetCellCenterCoord(pCellRowIndex, pCellColIndex);
        Vector3 lGridPosition = Coord2.ToWorld(mCenterCoord);
		mOriginPosition = mTransform.position = new Vector3(lGridPosition.x, 0.1f, lGridPosition.z);

		vExtraString = String.Empty; // 생성 초기화

		mRow = pCellRowIndex;
		mCol = pCellColIndex;

        _RefreshMapCell();
	}

    //-------------------------------------------------------------------------------------------------------
    // Cell을 삭제합니다.
    public void ClearGrid()
    {
        DestroyImmediate(gameObject);
    }
    //-------------------------------------------------------------------------------------------------------
    // Cell 정보를 갱신합니다.
    public bool RefreshMap()
    {
		//if (!_IsCheckCellData())
		//{
		//	Selection.activeGameObject = gameObject;
		//	EditorUtility.DisplayDialog("!WARNING!", "Check current select cell data please", "OK");
		//	return false;
		//}
        _RefreshMapCell();
        mTransform.position = mOriginPosition;
		mGridStage.aBlockNodeWeights[mRow, mCol].aWeight = vWeight;
        return true;
    }
// 	public bool _IsCheckCellData()
// 	{
// 		if (vTileType == MapGridTileType.Way)
// 		{
// 			if (String.IsNullOrEmpty(vWayFrom) || String.IsNullOrEmpty(vWayTo))
// 			{
// 				return false;
// 			}
// 		}
// 		else if (vTileType == MapGridTileType.CheckPoint)
// 		{
// 			if (String.IsNullOrEmpty(vCheckPointString))
// 			{
// 				return false;
// 			}
// 		}
// 		else if (vTileType == MapGridTileType.HQ)
// 		{
// 			if (vHeadQuarterIndex < 0)
// 			{
// 				return false;
// 			}
// 		}
// 		return true;
// 	}
    //-------------------------------------------------------------------------------------------------------
    // Inspector에서 전달 받은 메서드, Check point로 통하는 way 표시
// 	public void GoCheckPointLinkCheck()
// 	{
// 		if (vTileType != MapGridTileType.CheckPoint)
// 			return;
// 
// 		mGridStage.GoCheckPointLinkCheck(vCheckPointString, vWayIndex);
// 	}
    //-------------------------------------------------------------------------------------------------------
    // Check point로 통하는 way 표시
    public void ShowCheckPointLink(Color pColor)
    {
        mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.Link].SetColor("_Color", pColor);
    }
	//-------------------------------------------------------------------------------------------------------
	// Cell 초기화
	public void ResetCell()
	{
		vTileType = MapGridTileType.Way;
		vParam1 = 0;
		vParam2 = 0;
		vParam3 = 0;
		vParam4 = 0;
		vExtraString = String.Empty;
		vGroundType = MapGridGroundType.Grass;
		vWeight = 1;

		RefreshMap();
	}
	//-------------------------------------------------------------------------------------------------------
	// Cell Weight 초기화
	public void SetDefaultWayWeight()
	{
		// A* 처리를 위한 가중치 추가
		vWeight = MapGridTile.cDefaultWeights[(int)vGroundType];
	}

    //-------------------------------------------------------------------------------------------------------
    // 구현
    //-------------------------------------------------------------------------------------------------------
    // 초기화
    private void _InitMaterial()
    {
        Material[] lCellMaterials = new Material[(int)MaterialType.MaterialTypeMax];

        Material lLinkWay = new Material(Shader.Find("tk2d/BlendVertexColorAddTone2"));
        lLinkWay.name = "Link Material";
        lLinkWay.SetColor("_Color", new Color(0f, 1f, 0f, 0f));
        lLinkWay.SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridLinkTexture.png", typeof(Texture)) as Texture);
        lCellMaterials[0] = lLinkWay;


        Material lMatWay = new Material(Shader.Find("tk2d/BlendVertexColorAddTone2"));
        lMatWay.name = "Way Material";
        lMatWay.SetColor("_Color", new Color(1f, 1f, 1f, 100.0f / 255.0f));
        lMatWay.SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridWayTexture.png", typeof(Texture)) as Texture);
        lCellMaterials[1] = lMatWay;

        Material lMatType = new Material(Shader.Find("tk2d/BlendVertexColorAddTone2"));
        lMatType.name = "Type Material";
        lMatType.SetColor("_Color", new Color(1f, 1f, 1f, 200.0f / 255.0f));
        lMatType.SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridTypeTexture.png", typeof(Texture)) as Texture);
        lCellMaterials[2] = lMatType;

        Material lMatGround = new Material(Shader.Find("Unlit/Transparent"));
        lMatGround.name = "GroundMaterial";
        lMatGround.SetColor("_Color", new Color(1f, 1f, 1f, 1.0f));
        lMatGround.SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridEmptyIcon.png", typeof(Texture)) as Texture);
        lCellMaterials[3] = lMatGround;

        mTransform.GetComponent<Renderer>().sharedMaterials = lCellMaterials;
    }
    //-------------------------------------------------------------------------------------------------------
    // 셀 정보를 갱신 처리합니다.
    private void _RefreshMapCell()
    {
        if (vWayIndex >= mGridStage.vWayColor.Count)
            vWayIndex = mGridStage.vWayColor.Count - 1;

        mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.Way].SetColor("_Color", mGridStage.vWayColor[vWayIndex]);
        mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.Type].SetColor("_Color", mGridStage.vTileTypeColor[(int)vTileType]);

		if (vTileType == MapGridTileType.Block)
		{
			mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.GroundType].SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridEmptyIcon.png", typeof(Texture)) as Texture);
			vWeight = AStarPathFinder<NodeWeight>.cMaxWeight;
		}
		else if (vGroundType == MapGridGroundType.Grass)
		{
			mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.GroundType].SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridGrassIcon.png", typeof(Texture)) as Texture);
		}
		else if (vGroundType == MapGridGroundType.Road)
		{
			mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.GroundType].SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridRoadIcon.png", typeof(Texture)) as Texture);
		}
		else if (vGroundType == MapGridGroundType.Rock)
		{
			mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.GroundType].SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridRockIcon.png", typeof(Texture)) as Texture);
		}
		else
		{
			mTransform.GetComponent<Renderer>().sharedMaterials[(int)MaterialType.GroundType].SetTexture("_MainTex", AssetDatabase.LoadAssetAtPath("Assets/MapTool/Texture/GridEmptyIcon.png", typeof(Texture)) as Texture);
		}

    }

    //-------------------------------------------------------------------------------------------------------
    // 데이터
    //-------------------------------------------------------------------------------------------------------
    private Vector3 mOriginPosition;
    private Transform mTransform;
    private GridStageNewScript mGridStage;
    private Material[] mMaterials;
	private int mRow;
	private int mCol;
}
#endif
