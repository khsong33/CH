using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public enum MapGridTileType
{
    MultipleValues = -1, // 툴에서 서로 다른 값들을 선택할 경우 보여준다.
	Way,
	Headquarter,
	CheckPoint,
	EventPoint,

	Block, // [마지막] 무조건 갈수 없는 게임 외 공간
}

public enum MapGridGroundType
{
    MultipleValues = -1, // 툴에서 서로 다른 값들을 선택할 경우 보여준다.
	Grass,
    Road,
	Rock,
	Trench,
	Fence,
	Bunker,
	Mud,
	Tree,
	Bush,
	Water,
	SandBag,

	Empty, // [마지막] 무속성 지형
}

public enum MapGridTileAttributeType
{
	Tile, // 타일 타입
	Param, // 파라미터
	Ground, // 지형 타입
	Weight, // 경로 비중
	DirectionY, // 방향각

	None,
}

public class MapGridTile
{
	public const int cGrassWeight = 50;
	public const int cRoadWeight = 50;
	public const int cRockWeight = 50;
	public const int cTrenchWeight = 50;
	public const int cFenceWeight = 50;
	public const int cBunkerWeight = 50;
	public const int cMudWeight = 50;
	public const int cTreeWeight = 50;
	public const int cBushWeight = 50;
	public const int cWaterWeight = 50;
	public const int cSandBagWeight = 50;

	public const int cEmptyWeight = int.MaxValue;

	public static readonly int[] cDefaultWeights = {
		cGrassWeight,
		cRoadWeight,
		cRockWeight,
		cTrenchWeight,
		cFenceWeight,
		cBunkerWeight,
		cMudWeight,
		cTreeWeight,
		cBushWeight,
		cWaterWeight,
		cSandBagWeight,

		cEmptyWeight,
		};

	public const int cTileTypeCount = (int)(MapGridTileType.Block + 1);
	public const int cGroundTypeCount = (int)(MapGridGroundType.Empty + 1);
	public const int cAttributeTypeCount = (int)MapGridTileAttributeType.None;

	public const char cLineSplitChar = '%';
	public const char cAttributeTokenSplitChar = ':';
	public const char cParamTokenSplitChar = '^';

	//-------------------------------------------------------------------------------------------------------
	// 유틸리티 함수
	//-------------------------------------------------------------------------------------------------------
	// 테이블 정보를 검증합니다.
	public static void VerifyTable()
	{
		if (cDefaultWeights.Length != cGroundTypeCount)
			Debug.LogError("cDefaultWeights.Length mismatch");
	}
	//-------------------------------------------------------------------------------------------------------
	// 타일 타입을 얻습니다.
	public static MapGridTileType ParseTileType(String pTileToken)
	{
		for (int iToken = 0; iToken < cTileTypeCount; ++iToken)
			if (String.Compare(pTileToken, ((MapGridTileType)iToken).ToString()) == 0)
				return (MapGridTileType)iToken;
		return MapGridTileType.Block;
	}
	//-------------------------------------------------------------------------------------------------------
	// 지형 타입을 얻습니다.
	public static MapGridGroundType ParseGroundType(String pGroundToken)
	{
		for (int iToken = 0; iToken < cGroundTypeCount; ++iToken)
			if (String.Compare(pGroundToken, ((MapGridGroundType)iToken).ToString()) == 0)
				return (MapGridGroundType)iToken;
		return MapGridGroundType.Empty;
	}
	//-------------------------------------------------------------------------------------------------------
	// 속성 타입을 얻습니다.
	public static MapGridTileAttributeType ParseAttributeType(String pAttributeToken)
	{
		for (int iToken = 0; iToken < cAttributeTypeCount; ++iToken)
			if (String.Compare(pAttributeToken, ((MapGridTileAttributeType)iToken).ToString()) == 0)
				return (MapGridTileAttributeType)iToken;
		return MapGridTileAttributeType.None;
	}
}
