﻿// unlit, vertex color, 2 textures, alpha blended
// cull off

Shader "Tool/Tile"
{
	Properties
	{
		_Opaque("Tile Opaque", Range(0,1)) = 1
		//_Color ("Color",Color) = (1,1,1,1)
		_GroundTex("Ground Types (32:1)", 2D) = "white" {}
		_TileTypeTex("Tile Types (16:1)", 2D) = "black" {}
		_RotationTex("Rotation Marker (1:1)", 2D) = "black" {}
	}

			SubShader
	{
		Tags{ "Queue" = "Transparent" "IgnoreProjector" = "True" "RenderType" = "Transparent" }
		ZWrite Off Lighting Off Cull Off Fog{ Mode Off } Blend SrcAlpha OneMinusSrcAlpha
		LOD 110

		Pass
		{
			CGPROGRAM
			#pragma vertex vert_vctt
			#pragma fragment frag
			#pragma fragmentoption ARB_precision_hint_fastest
			#include "UnityCG.cginc"

			//float4 _Color;
			float _Opaque;
			sampler2D _GroundTex;
			sampler2D _TileTypeTex;
			sampler2D _RotationTex;

			struct vin_vctt
			{
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0; 
				float2 texcoord1 : TEXCOORD1; // x: 선택상태
				float2 texcoord2 : TEXCOORD2; // x: 그라운드 타입, y: 타일타입
				float2 texcoord3 : TEXCOORD3; // 회전값
			};

			struct v2f_vctt
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
				float2 texcoord1 : TEXCOORD1;// x: 선택상태, y: 길찾기 상태
				float2 texcoord2 : TEXCOORD2;// x: 그라운드 타입, y: 타일타입
				float2 texcoord3 : TEXCOORD3; // 회전값
			};

			v2f_vctt vert_vctt(vin_vctt v)
			{
				v2f_vctt o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.color = v.color;

				o.texcoord1 = v.texcoord1;
				o.texcoord.x = (v.texcoord.x + v.texcoord2.x)/16;
				o.texcoord.y = v.texcoord.y;
				o.texcoord2.x = (v.texcoord.x + v.texcoord2.y)/16;
				o.texcoord2.y = v.texcoord.y;
				
				o.texcoord3 = v.texcoord3;
				return o;
			}

			fixed4 frag(v2f_vctt i) : SV_Target
			{
				fixed4 col = tex2D(_GroundTex, i.texcoord);

				fixed4 tile;
				// rotation
				tile = tex2D(_RotationTex, i.texcoord3);
				if (tile.a > 0.4)
					col.rgb = tile.rgb;


				// path find result
				if (i.color.g > 0.1) // yellow
				{
					col.rg += 0.5;
					col.b -= 0.4;
				}
				else if (i.color.r > 0.1) // red
				{
					col.r += 0.5;
					col.gb -= 0.5;
				}

				// tile type
				tile = tex2D(_TileTypeTex, i.texcoord2);
				if (tile.a > 0.4)
					col.rgb = tile.rgb;

				// selection state effect
				if (i.texcoord1.x > 0.9)
					col.rbg = (col.r * 0.2126 + col.g * 0.7152 + col.b * 0.0722) * 1.5 + 0.2;

				col.a = _Opaque;
				return col;
			}

			ENDCG
		}
	}
}
