﻿Shader "Seraph/CHChracter" 
{
	Properties 
	{
		_EmissiveColor ("Emissive Color", Color) = (1,1,1,1)
		_AmbientColor  ("Ambient Color", Color) = (1,1,1,1)
		_RimColor ("Rim Light Color",Color) = (1,1,1,1)
		_RimPower ("RimPower", Range(0,2)) = 1
		_RimTick ("Rim Tickness", Range(0,30)) = 1
		_RampTex ("Ramp Texture", 2D) = "white"{}
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_NormalTex ("Normal Map", 2D) = "bump" {}
		_NormalIntensity ("Normal Map Intensity", Range(0,2)) = 1
	}
	
	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		CGPROGRAM
		#pragma surface surf BasicDiffuse

		float4 _EmissiveColor;
		float4 _AmbientColor;
		float4 _RimColor;
		float _RimPower;
		float _RimTick;
		sampler2D _RampTex;
		sampler2D _MainTex;
		sampler2D _NormalTex;
		float _NormalIntensity;
		
		inline float4 LightingBasicDiffuse (SurfaceOutput s, fixed3 lightDir, half3 viewDir, fixed atten)
		{
			//float difLight = max(0, dot(s.Normal, lightDir)); //MAX를 사용해서 범위 지정
			
			half3 h = normalize(lightDir + viewDir);
			float3 ahdn = (1-dot(h,normalize(s.Normal)))*_RimColor;
			ahdn = (pow(clamp(ahdn,0.0,1.0),_RimTick))*_RimPower ;
			
			float difLight = dot(s.Normal, lightDir); 
			float rimLight = dot(s.Normal, viewDir);
			
			float hLambert = difLight * 0.5 + 0.5; //하프 램버트 추가

			float3 ramp = tex2D(_RampTex, float2(hLambert, rimLight)).rgb;
			
			
			float4 col;
			col.rgb = (s.Albedo * _LightColor0.rgb * (ramp))+ahdn;
			col.a = s.Alpha;
			return col;
		} //사용자 정의 LightingBasicDiffuse . . . 방식으로 변경하는 부분.

		struct Input 
		{
			float2 uv_MainTex;
			float2 uv_NormalTex;
		};

		void surf (Input IN, inout SurfaceOutput o) 
		{
			half4 d = tex2D (_MainTex, IN.uv_MainTex);
			float3 normalMap = tex2D(_NormalTex, IN.uv_NormalTex);
			normalMap = float3(normalMap.x * _NormalIntensity, normalMap.y * _NormalIntensity, normalMap.z);
			
			float4 c;
			c = d*(_EmissiveColor + _AmbientColor);
			
			o.Normal = normalMap.rgb;
			o.Albedo = c.rgb;
			o.Alpha = c.a;
		}
		

		ENDCG
	} 
	
	FallBack "Diffuse"
}
