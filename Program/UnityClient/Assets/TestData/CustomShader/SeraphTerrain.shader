// Shader created with Shader Forge v1.04 
// Shader Forge (c) Neat Corporation / Joachim Holmer - http://www.acegikmo.com/shaderforge/
// Note: Manually altering this data may prevent you from opening it in Shader Forge
/*SF_DATA;ver:1.04;sub:START;pass:START;ps:flbk:,lico:1,lgpr:1,nrmq:1,limd:1,uamb:True,mssp:True,lmpd:False,lprd:False,rprd:False,enco:False,frtr:True,vitr:True,dbil:False,rmgx:True,rpth:0,hqsc:True,hqlp:False,tesm:0,blpr:0,bsrc:0,bdst:1,culm:0,dpts:2,wrdp:True,dith:2,ufog:False,aust:True,igpj:False,qofs:0,qpre:1,rntp:1,fgom:False,fgoc:False,fgod:False,fgor:False,fgmd:0,fgcr:0.5,fgcg:0.5,fgcb:0.5,fgca:1,fgde:0.01,fgrn:0,fgrf:300,ofsf:0,ofsu:0,f2p0:False;n:type:ShaderForge.SFN_Final,id:3196,x:33203,y:32654,varname:node_3196,prsc:2|diff-9667-OUT,amdfl-5011-OUT;n:type:ShaderForge.SFN_Tex2d,id:2422,x:32120,y:32499,ptovrint:False,ptlb:Texture1,ptin:_Texture1,varname:node_2422,prsc:2,tex:3ffd4f6c5af558344be02cf2054f1d14,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1857,x:32120,y:32683,ptovrint:False,ptlb:Texture2,ptin:_Texture2,varname:node_1857,prsc:2,tex:348bb1c038d00ad4d8f053458d49a095,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:1690,x:32120,y:32871,ptovrint:False,ptlb:Texture3,ptin:_Texture3,varname:node_1690,prsc:2,tex:f0bba9108d09ce44c9068440a755fe83,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:8185,x:32120,y:33053,ptovrint:False,ptlb:Texture4,ptin:_Texture4,varname:node_8185,prsc:2,tex:db6a0e046991beb4d8306b17503befac,ntxv:2,isnm:False;n:type:ShaderForge.SFN_Tex2d,id:5030,x:32121,y:33423,ptovrint:False,ptlb:TerrainPaint,ptin:_TerrainPaint,varname:node_5030,prsc:2,tex:c8bb1dfe243bf3f46a700dd0045e7b12,ntxv:2,isnm:False|UVIN-517-UVOUT;n:type:ShaderForge.SFN_Lerp,id:7858,x:32409,y:32504,varname:node_7858,prsc:2|A-2422-RGB,B-1857-RGB,T-5030-R;n:type:ShaderForge.SFN_Lerp,id:9101,x:32409,y:32638,varname:node_9101,prsc:2|A-7858-OUT,B-1690-RGB,T-5030-G;n:type:ShaderForge.SFN_Lerp,id:9667,x:32475,y:32853,varname:node_9667,prsc:2|A-9101-OUT,B-8185-RGB,T-5030-B;n:type:ShaderForge.SFN_Color,id:4848,x:32650,y:32661,ptovrint:False,ptlb:Ambient,ptin:_Ambient,varname:node_4848,prsc:2,glob:False,c1:0.5,c2:0.5,c3:0.5,c4:1;n:type:ShaderForge.SFN_Multiply,id:5011,x:32860,y:32661,varname:node_5011,prsc:2|A-4848-RGB,B-4030-OUT;n:type:ShaderForge.SFN_Slider,id:4030,x:32650,y:32840,ptovrint:False,ptlb:AmbientMultiply,ptin:_AmbientMultiply,varname:node_4030,prsc:2,min:0,cur:1,max:2;n:type:ShaderForge.SFN_Rotator,id:517,x:31932,y:33423,varname:node_517,prsc:2|UVIN-2981-UVOUT,ANG-9218-OUT;n:type:ShaderForge.SFN_TexCoord,id:2981,x:31760,y:33312,varname:node_2981,prsc:2,uv:0;n:type:ShaderForge.SFN_ValueProperty,id:9218,x:31760,y:33573,ptovrint:False,ptlb:UVRotate,ptin:_UVRotate,varname:node_9218,prsc:2,glob:False,v1:0;proporder:4848-4030-2422-1857-1690-8185-5030-9218;pass:END;sub:END;*/

Shader "Seraph/SeraphTerrain" {
    Properties {
        _Ambient ("Ambient", Color) = (0.5,0.5,0.5,1)
        _AmbientMultiply ("AmbientMultiply", Range(0, 2)) = 1
        _Texture1 ("Texture1", 2D) = "black" {}
        _Texture2 ("Texture2", 2D) = "black" {}
        _Texture3 ("Texture3", 2D) = "black" {}
        _Texture4 ("Texture4", 2D) = "black" {}
        _TerrainPaint ("TerrainPaint", 2D) = "black" {}
        _UVRotate ("UVRotate", Float ) = 0
    }
    SubShader {
        Tags {
            "RenderType"="Opaque"
        }
        LOD 200
        Pass {
            Name "ForwardBase"
            Tags {
                "LightMode"="ForwardBase"
            }
            
            
            Fog {Mode Off}
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag
            #define UNITY_PASS_FORWARDBASE
            #include "UnityCG.cginc"
            #include "AutoLight.cginc"
            #pragma multi_compile_fwdbase_fullshadows
            #pragma exclude_renderers d3d11 xbox360 ps3 flash d3d11_9x 
            #pragma target 3.0
            uniform float4 _LightColor0;
            uniform sampler2D _Texture1; uniform float4 _Texture1_ST;
            uniform sampler2D _Texture2; uniform float4 _Texture2_ST;
            uniform sampler2D _Texture3; uniform float4 _Texture3_ST;
            uniform sampler2D _Texture4; uniform float4 _Texture4_ST;
            uniform sampler2D _TerrainPaint; uniform float4 _TerrainPaint_ST;
            uniform float4 _Ambient;
            uniform float _AmbientMultiply;
            uniform float _UVRotate;
            struct VertexInput {
                float4 vertex : POSITION;
                float3 normal : NORMAL;
                float2 texcoord0 : TEXCOORD0;
            };
            struct VertexOutput {
                float4 pos : SV_POSITION;
                float2 uv0 : TEXCOORD0;
                float4 posWorld : TEXCOORD1;
                float3 normalDir : TEXCOORD2;
                LIGHTING_COORDS(3,4)
            };
            VertexOutput vert (VertexInput v) {
                VertexOutput o = (VertexOutput)0;
                o.uv0 = v.texcoord0;
                o.normalDir = mul(_Object2World, float4(v.normal,0)).xyz;
                o.posWorld = mul(_Object2World, v.vertex);
                float3 lightColor = _LightColor0.rgb;
                o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
                TRANSFER_VERTEX_TO_FRAGMENT(o)
                return o;
            }
            fixed4 frag(VertexOutput i) : COLOR {
                i.normalDir = normalize(i.normalDir);
/////// Vectors:
                float3 normalDirection = i.normalDir;
                float3 lightDirection = normalize(_WorldSpaceLightPos0.xyz);
                float3 lightColor = _LightColor0.rgb;
////// Lighting:
                float attenuation = LIGHT_ATTENUATION(i);
                float3 attenColor = attenuation * _LightColor0.xyz;
/////// Diffuse:
                float NdotL = max(0.0,dot( normalDirection, lightDirection ));
                float3 indirectDiffuse = float3(0,0,0);
                float3 directDiffuse = max( 0.0, NdotL) * attenColor;
                indirectDiffuse += UNITY_LIGHTMODEL_AMBIENT.rgb; // Ambient Light
                indirectDiffuse += (_Ambient.rgb*_AmbientMultiply); // Diffuse Ambient Light
                float4 _Texture1_var = tex2D(_Texture1,TRANSFORM_TEX(i.uv0, _Texture1));
                float4 _Texture2_var = tex2D(_Texture2,TRANSFORM_TEX(i.uv0, _Texture2));
                float node_517_ang = _UVRotate;
                float node_517_spd = 1.0;
                float node_517_cos = cos(node_517_spd*node_517_ang);
                float node_517_sin = sin(node_517_spd*node_517_ang);
                float2 node_517_piv = float2(0.5,0.5);
                float2 node_517 = (mul(i.uv0-node_517_piv,float2x2( node_517_cos, -node_517_sin, node_517_sin, node_517_cos))+node_517_piv);
                float4 _TerrainPaint_var = tex2D(_TerrainPaint,TRANSFORM_TEX(node_517, _TerrainPaint));
                float4 _Texture3_var = tex2D(_Texture3,TRANSFORM_TEX(i.uv0, _Texture3));
                float4 _Texture4_var = tex2D(_Texture4,TRANSFORM_TEX(i.uv0, _Texture4));
                float3 node_9667 = lerp(lerp(lerp(_Texture1_var.rgb,_Texture2_var.rgb,_TerrainPaint_var.r),_Texture3_var.rgb,_TerrainPaint_var.g),_Texture4_var.rgb,_TerrainPaint_var.b);
				node_9667 *= 2.0;
                float3 diffuse = (directDiffuse + indirectDiffuse) * node_9667;
/// Final Color:
                float3 finalColor = diffuse;
                return fixed4(finalColor,1);
            }
            ENDCG
        }
        
    }
    FallBack "Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}
