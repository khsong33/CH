Shader "CH/Transparent Cutout with Lightmap" {
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
	_LightMap("Lightmap (RGB)", 2D) = "black" {}
	_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_LightMapPow("LightMapPow", range(0,4)) = 1
	}

		SubShader{
		LOD 200
		Tags{ "Queue" = "AlphaTest" "IgnoreProjector" = "True" "RenderType" = "TransparentCutout" }
		CGPROGRAM
#pragma surface surf Lambert alphatest:_Cutoff
	struct Input {
		float2 uv_MainTex;
		float2 uv2_LightMap;
	};
	sampler2D _MainTex;
	sampler2D _LightMap;
	fixed4 _Color;
	float _LightMapPow;
	void surf(Input IN, inout SurfaceOutput o)
	{
		fixed4 Albedo = tex2D(_MainTex, IN.uv_MainTex) * _Color;
		half4 lm = tex2D(_LightMap, IN.uv2_LightMap);
		o.Emission = lm.rgb*Albedo.rgb *_LightMapPow;
		o.Alpha = Albedo.a;
	}
	ENDCG
	}
		Fallback "Legacy Shaders/Transparent/Cutout/VertexLit"
}
