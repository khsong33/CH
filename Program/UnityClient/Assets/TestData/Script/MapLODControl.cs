﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MapLODControl : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public GameObject vPlaneMap;
	public GameObject vTerrainMap;
	public GameObject vTerrainTextureMap;
	public List<GameObject> vPlaneMapHideList;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public void SetTerrainMap()
	{
		if (vPlaneMap != null)
			vPlaneMap.SetActive(false);

		if (vTerrainMap != null)
			vTerrainMap.SetActive(true);

		if(vTerrainTextureMap != null)
			vTerrainTextureMap.SetActive(true);

		for (int iObject = 0; iObject < vPlaneMapHideList.Count; iObject++)
			if (vPlaneMapHideList[iObject] != null)
				vPlaneMapHideList[iObject].SetActive(true);
	}
	public void SetPlaneMap()
	{
		if (vPlaneMap != null)
			vPlaneMap.SetActive(true);

		if (vTerrainMap != null)
			vTerrainMap.SetActive(false);

		if (vTerrainTextureMap != null)
			vTerrainTextureMap.SetActive(false);

		for (int iObject = 0; iObject < vPlaneMapHideList.Count; iObject++)
			if (vPlaneMapHideList[iObject] != null)
				vPlaneMapHideList[iObject].SetActive(false);
	}

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}