﻿using UnityEngine;
using System.Collections;

public class TestMeshTerrain : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		float lYPos = _GetMeshMapHeight(transform.position);
		Debug.Log("lYPos - " + lYPos);
		transform.position = new Vector3(transform.position.x,
										lYPos,
										transform.position.z);
	}
	protected float _GetMeshMapHeight(Vector3 pPosition)
	{
		Vector3 lStartPos = new Vector3(pPosition.x, pPosition.y + 100.0f, pPosition.z);
		Vector3 lEndPos = new Vector3(pPosition.x, pPosition.y - 100.0f, pPosition.z);

		Debug.DrawLine(lStartPos, lEndPos, Color.red);
		//Ray ray = new Ray(pPosition.transform.position, lEndPos);
		RaycastHit hitInfo;
		float lResultHeight = 0.0f;
		if (Physics.Raycast(lStartPos, lEndPos, out hitInfo))
		{
			lResultHeight = hitInfo.point.y;
			//lResultHeight = hitInfo.transform.position.y;
			if (lResultHeight > 0.0f)
				Debug.Log("Get Height - " + lResultHeight + "/Hit Transform name - " + hitInfo.transform.name);
		}
		return lResultHeight;
	}


}
