﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TestPlay : MonoBehaviour
{
	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public TouchPanel vTouchPanel;
	public QuaterViewCameraSet vCameraSet;
	public GameObject vCharacterRoot;
	public GameObject vTestCharacter;
	public UILabel vCountLabel;
	public UIInput vCountInput;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
//		mStartPosition = Vector3.zero;
		vCountLabel.text = String.Format("Character Count : {0}", mCurrentCharacterCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// 활성화 콜백
	void OnEnable()
	{
		vTouchPanel.aOnDragStartDelegate += _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate += _OnDragMove;
		vTouchPanel.aOnDragEndDelegate += _OnDragEnd;
		vTouchPanel.aOnClickDelegate += _OnClick;
		vTouchPanel.aOnZoomDelegate += _OnZoom;
	}
	//-------------------------------------------------------------------------------------------------------
	// 비활성화 콜백
	void OnDisable()
	{
		vTouchPanel.aOnDragStartDelegate -= _OnDragStart;
		vTouchPanel.aOnDragMoveDelegate -= _OnDragMove;
		vTouchPanel.aOnDragEndDelegate -= _OnDragEnd;
		vTouchPanel.aOnClickDelegate -= _OnClick;
		vTouchPanel.aOnZoomDelegate -= _OnZoom;
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------
	public void OnAddCharacter()
	{
		_AddCharacter(mCreateCount);
	}
	public void OnRemoveCharacter()
	{
		_RemoveCharacter(mCreateCount);
	}
	public void SetAddCharacter()
	{
		mCreateCount = System.Convert.ToInt32(vCountInput.value);
		Debug.Log(vCountInput.value);
	}
	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------
	// 캐릭터 생성 및 삭제
	private void _AddCharacter(int pCount)
	{
		for (int iCount = 0; iCount < pCount; iCount++)
		{
			GameObject lObj = (GameObject)Instantiate(vTestCharacter);
			lObj.transform.localPosition = new Vector3(UnityEngine.Random.Range(-50.0f, 50.0f),
													   0.0f,
													   UnityEngine.Random.Range(-40.0f, 60.0f));
			lObj.transform.parent = vCharacterRoot.transform;
		}
		mCurrentCharacterCount += pCount;
		vCountLabel.text = String.Format("Character Count : {0}", mCurrentCharacterCount);
	}

	private void _RemoveCharacter(int pCount)
	{
		int vCharacterCount = vCharacterRoot.transform.childCount;
		if (vCharacterCount < pCount)
			pCount = vCharacterCount;
		for (int iCount = 0; iCount < pCount; iCount++)
		{
			Transform lCharactTransform = vCharacterRoot.transform.GetChild(iCount);
			Destroy(lCharactTransform.gameObject);
		}
		mCurrentCharacterCount -= pCount;
		vCountLabel.text = String.Format("Character Count : {0}", mCurrentCharacterCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragStartDelegate
	private void _OnDragStart(Plane pDragPlane, Vector2 pDragStartPoint)
	{
		vCameraSet.DragStart(pDragPlane, pDragStartPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragMoveDelegate
	private void _OnDragMove(Plane pDragPlane, Vector2 pDragLastPoint, Vector2 pDragMovePoint, float pDragDelay, int pTouchCount)
	{
		vCameraSet.DragMove(pDragPlane, pDragLastPoint, pDragMovePoint, pDragDelay, pTouchCount);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnDragEndDelegate
	private void _OnDragEnd(Plane pDragPlane, Vector2 pDragEndPoint)
	{
		vCameraSet.DragEnd(pDragPlane, pDragEndPoint);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnClickDelegate
	private void _OnClick(Plane pClickPlane, Vector2 pClickPoint, bool pIsHolded)
	{
		//Debug.Log("_OnClick()");

		//Coord2 lClickCoord = Coord2.ToCoord(pClickPosition);
	}
	//-------------------------------------------------------------------------------------------------------
	// TouchPanel.OnZoomDelegate
	private void _OnZoom(Plane pZoomPlane, Vector2 pZoomCenterPoint, float pZoomDelta)
	{
		vCameraSet.Zoom(pZoomPlane, pZoomCenterPoint, pZoomDelta);
	}

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
//	private Vector3 mStartPosition;
	private int mCurrentCharacterCount = 0;
	private int mCreateCount = 1;
}