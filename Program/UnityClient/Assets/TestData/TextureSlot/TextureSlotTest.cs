﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class TextureSlotTest : MonoBehaviour
{
	public const int cProfilerMaxNumberOfSamplesPerFrame = 8000000;

	//-------------------------------------------------------------------------------------------------------
	// 인스펙터
	//-------------------------------------------------------------------------------------------------------
	public TextureSlot vTextureSlot;
	public Vector3 vTextureSlotScale = new Vector3(0.5f, 1.0f, 0.5f);
	public Vector3 vTextureSlotOffset = new Vector3(5.0f, 1.0f, -5.0f);
	public Vector3 vTextureSlotPositioningVector = new Vector3(10.0f, 0.0f, -10.0f);
	public int vColCount = 33;
	public Texture[] vTextures;

	//-------------------------------------------------------------------------------------------------------
	// 속성
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 콜백
	//-------------------------------------------------------------------------------------------------------
	// 시작 전 콜백
	void Awake()
	{
		if (vTextureSlot == null)
			Debug.LogError("vTextureSlot is empty - " + this);

		//Debug.Log("Profiler.maxNumberOfSamplesPerFrame=" + Profiler.maxNumberOfSamplesPerFrame);
		if (Profiler.maxNumberOfSamplesPerFrame < cProfilerMaxNumberOfSamplesPerFrame)
			Profiler.maxNumberOfSamplesPerFrame = Profiler.maxNumberOfSamplesPerFrame;
	}
	//-------------------------------------------------------------------------------------------------------
	// 시작 콜백
	void Start()
	{
		Transform thisTransform = transform;
		Transform lTextureSlotTransform = vTextureSlot.transform;

		Vector3 lTextureSlotPositionBase = lTextureSlotTransform.position;
		lTextureSlotPositionBase.x += vTextureSlotOffset.x * vTextureSlotScale.x;
		lTextureSlotPositionBase.y += vTextureSlotOffset.y * vTextureSlotScale.y;
		lTextureSlotPositionBase.z += vTextureSlotOffset.z * vTextureSlotScale.z;
		Quaternion lTextureSlotRotation = lTextureSlotTransform.rotation;

		int lRowCount = vTextures.Length / vColCount;

		for (int iRow = 0; iRow < lRowCount; ++iRow)
			for (int iCol = 0; iCol < vColCount; ++iCol)
		{
			int lTexturdIdx = iRow * vColCount + iCol;
			if (lTexturdIdx >= vTextures.Length)
				break;
			if (vTextures[lTexturdIdx] == null)
				continue;
			
			Vector3 lTextureSlotPosition = new Vector3(
				lTextureSlotPositionBase.x + vTextureSlotPositioningVector.x * vTextureSlotScale.x * iCol,
				lTextureSlotPositionBase.y + vTextureSlotPositioningVector.y * vTextureSlotScale.y,
				lTextureSlotPositionBase.z + vTextureSlotPositioningVector.z * vTextureSlotScale.z * iRow);
			Vector3 lTextureSlotLocalScale = new Vector3(
				lTextureSlotTransform.localScale.x * vTextureSlotScale.x,
				lTextureSlotTransform.localScale.y * vTextureSlotScale.y,
				lTextureSlotTransform.localScale.z * vTextureSlotScale.z);
			GameObject lTextureSlotObject = (GameObject)Instantiate(vTextureSlot.gameObject, lTextureSlotPosition, lTextureSlotRotation);

			lTextureSlotObject.name = String.Format("{0}:{1}", lTexturdIdx, vTextures[lTexturdIdx].name);
			lTextureSlotObject.transform.parent = thisTransform;
			lTextureSlotObject.transform.localScale = lTextureSlotLocalScale;

			TextureSlot lTextureSlot = lTextureSlotObject.GetComponent<TextureSlot>();
			lTextureSlot.LoadTexture(vTextures[lTexturdIdx]);
		}

		vTextureSlot.gameObject.SetActive(false); // 템플릿은 숨김
	}

	//-------------------------------------------------------------------------------------------------------
	// 메서드
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 구현
	//-------------------------------------------------------------------------------------------------------

	//-------------------------------------------------------------------------------------------------------
	// 데이터
	//-------------------------------------------------------------------------------------------------------
}
